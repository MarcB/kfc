import { Component, OnInit } from '@angular/core';

class Cocktail {
  constructor(name: string, ingredients: string[]) {
    this.ingredients = ingredients;
    this.name = name;
  }

  //
  name: string;
  ingredients: string[];
  //

}

@Component({
  selector: 'app-cocktail',
  templateUrl: './cocktail.component.html',
  styleUrls: ['./cocktail.component.css']
})

export class CocktailComponent implements OnInit {
  private ingredients: string[];
  private name: string;
  constructor(name: string, ingredients: string[]) {
    this.ingredients = ingredients;
    this.name = name;
  }

  cocktails: Cocktail[];
  eau = 'eau';
  vodka = 'vodka';
  fraise = 'fraise';
  citron = 'citron';
  rhum = 'rhum';
  menthe = 'menthe';
  olive = 'olive';
  cervoise = 'cervoise';
  ngOnInit(): void {
    const cock1 = new Cocktail('malibu', [ this.menthe, this.cervoise, this.citron]);
    const cock2 = new Cocktail('marcoise', [ this.eau, this.cervoise, this.citron]);
    const cock3 = new Cocktail('battrie', [ this.olive, this.rhum, this.vodka]);
    const cock4 = new Cocktail('vener', [ this.rhum, this.cervoise, this.vodka]);
    const cock5 = new Cocktail('manjtaimort', [ this.menthe, this.fraise, this.citron, this.eau, this.olive]);
    const cock6 = new Cocktail('fullfun', [ this.eau]);
    this.cocktails = [cock1 , cock2 , cock3 , cock4 , cock5 , cock6];
  }
}
