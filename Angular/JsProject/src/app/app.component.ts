import {Component, HostListener, OnInit} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'empty';
  squareCoord: string = 'no';
  boxCoord: string = 'no';
  content = 'BLABLabla BLABLabla BLABLabla BLABLabla';
  public monType = 'Texte' ;
  public maValue: string|Date = '01012015';
  public bgColor = 'red';
  public left =0;
  public top =0;
  public test = document.getElementsByTagName('div')
    public carre;
    public box;
    public square;
  public changeColor(color: string): void{
    this.bgColor = color;
  }
  public moveFromClick(): void {
    this.left += 50;
    // this.boxCoord = this.box.offsetLeft;
    // this.squareCoord =  this.square.offsetLeft;
  }
  //
  @HostListener('document:keydown', ['$event'])
  public handleKeyboardEvent(event: KeyboardEvent): void {
    //
    this.box = document.getElementsByClassName('box')[0];


    this.square = document.getElementsByClassName('square')[0];
    const key = event.key;
    console.log(this.box.offsetLeft)
    //
    this.boxCoord =  this.box.offsetLeft +';'+this.box.offsetTop;
    this.squareCoord =  this.square.offsetLeft+';'+this.square.offsetTop;
    switch ( key ) {
      case 'ArrowLeft': {
        if ( this.square.offsetLeft > this.box.offsetLeft ){
        // if (this.left > 0 ){
          this.left -= 10;
          break;
        }
        break;
      }
      case 'ArrowRight': {
        if (this.square.offsetLeft  < (this.box.offsetLeft+this.box.offsetWidth-this.square.offsetWidth) ){
          this.left += 10;
          break;
        }
        break;
      }
      case 'ArrowUp': {
        if (this.square.offsetTop > this.box.offsetTop ){
          this.top -= 10;
          break;
        }
        break;
      }
      case 'ArrowDown': {
        if (this.square.offsetTop < (this.box.offsetTop+this.box.offsetHeight-this.square.offsetHeight)) {
          this.top += 10;
          break;
        }
        break;
      }
    }
    //
    //test event -> on wait change format date and color
    //
  // constructor() {
  //   setTimeout(() => {
  //     this.monType = 'date';
  //     // @ts-ignore
  //     // this.maValue = new  Date(this.maValue.toDateString());
  //     this.maValue = new Date(2018, 10, 10) ;
  //     // this.maValue as Date   ;
  //   }, 1500);
  }
}



