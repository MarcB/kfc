import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bonjour',
  templateUrl: './bonjour.component.html',
  styleUrls: ['./bonjour.component.css']
})
export class BonjourComponent implements OnInit{
  public sourceImg = '';
  public w: number;
  public h: number;


  // tslint:disable-next-line:variable-name
  constructor() {

  }

  ngOnInit(): void {
    this.sourceImg = 'https://angular.io/assets/images/logos/angular/logo-nav@2x.png';
    this.w = 200;
    this.h = 100;
    }

  public square(nbr: number): number{
    return nbr * nbr;
  }
}
