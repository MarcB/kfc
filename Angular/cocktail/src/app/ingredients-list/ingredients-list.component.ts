import { Component, OnInit, Input } from '@angular/core';
import { Cocktail} from '../cocktail';

@Component({
  selector: 'app-ingredients-list',
  templateUrl: './ingredients-list.component.html',
  styleUrls: ['./ingredients-list.component.css']
})
export class IngredientsListComponent implements OnInit {
  @Input() cocktail: Cocktail;
  constructor() { }

  ngOnInit(): void {
  }

}
