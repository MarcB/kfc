import { Ingredient } from './ingredient';

export interface Cocktail {
  id: number;
  nom: string;
  prix: number;
  ingredients: Ingredient[];
}
