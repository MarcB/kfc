import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { COCKTAILS } from '../mock-cocktails';
import { Cocktail} from '../cocktail';

@Component({
  selector: 'app-cocktails-list',
  templateUrl: './cocktails-list.component.html',
  styleUrls: ['./cocktails-list.component.css']
})
export class CocktailsListComponent implements OnInit {
  cocktails = COCKTAILS;
  selectedCocktail: Cocktail;
  @Output() showIngredient = new EventEmitter<any>();
  constructor() {

  }
  public setSelelectCocktail(event, cocktail): void {
    this.selectedCocktail = cocktail;
    this.showIngredient.emit(this.selectedCocktail);
  }
  ngOnInit(): void {
    this.selectedCocktail = null;
  }

}
