import { Component, OnInit } from '@angular/core';
import { COCKTAILS } from './mock-cocktails';
import { Cocktail} from './cocktail';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  selectedCocktail: Cocktail;
  title: string ;
  constructor(){

  }
  setDetailIngredient(cocktail): void{
    this.selectedCocktail = cocktail;
  }
  ngOnInit(): void {
  }
}
