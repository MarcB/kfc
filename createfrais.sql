DROP TABLE IF EXISTS personne ;
-- DROP TABLE IF EXISTS vehicule ;
-- DROP TABLE IF EXISTS trajet ; 

-- p

CREATE TABLE personne (

	npersonne	int(8)  		not null AUTO_INCREMENT,-- pk_npersonne
	nom    		varchar(20) 	not null,
	prenom  	varchar(15) 	not null,
	mail    	varchar (30)	null, 					-- uk_mail
	tel     	int (10) 		null ,
	job      	varchar(8) 		not null ,
	code_pin	int(4)			null,

	CONSTRAINT 	uk_mail 	unique key 	(mail),
	CONSTRAINT 	pk_npersonne 	PRIMARY KEY 	( npersonne ) )engine=INNODB;
