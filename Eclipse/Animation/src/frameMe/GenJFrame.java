package frameMe;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GenJFrame {

    //variable init

    private static JFrame Affichafe;
    Container MaBoite;
    JPanel pushMe, mainTextPan;
    JLabel affTexte;
    Font affFont = new Font("Times New Roman", Font.PLAIN, 75);
    JButton start;
    JTextArea textMain;
    TtitlescreenHandler tsHaandler = new TtitlescreenHandler();

    //method du jframe

    public GenJFrame() {
        Affichafe = new JFrame("affiche moi");
        Affichafe.setSize(800, 600);
        Affichafe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Affichafe.getContentPane().setBackground(Color.DARK_GRAY);
        Affichafe.setLayout(null);
        Affichafe.setVisible(true);

        MaBoite = Affichafe.getContentPane();

        pushMe = new JPanel();
        //            bordurex  bordurey  largeur    hauteur
        pushMe.setBounds(100, 100, 600, 150);
        pushMe.setBackground(Color.gray);
        affTexte = new JLabel("try me");
        affTexte.setForeground(Color.red);
        affTexte.setFont(affFont);

        pushMe.add(affTexte);

        JPanel startPan = new JPanel();
        startPan.setBounds(300, 400, 200, 100);
        startPan.setBackground(Color.gray);

        //ajout boutton

        start = new JButton("start me");
        start.setBackground(Color.red);
        start.setForeground(Color.green);
        start.addActionListener(tsHaandler);

        //ajout au jframe

        startPan.add(start);
        pushMe.add(affTexte);

        //ajout container
        MaBoite.add(startPan);
        MaBoite.add(pushMe);

    }

    public void nouvelleAffichage() {
        pushMe.setVisible(false);
        start.setVisible(false);


        mainTextPan = new JPanel();
        mainTextPan.setBounds(100, 100, 600, 4000);
        mainTextPan.setBackground(Color.gray);
        textMain = new JTextArea("ohiohiohiohiohoihoihoihiohijiohoihoihohoihiohiohio");
        textMain.setBounds(100, 100, 600, 150);
        textMain.setBackground(Color.red);
        textMain.setForeground(Color.yellow);
        textMain.setFont(affFont);
        //decoupe le texte
        textMain.setLineWrap(true);


        mainTextPan.add(textMain);
        MaBoite.add(mainTextPan);

    }


    public class TtitlescreenHandler implements ActionListener {
        public void actionPerformed(ActionEvent event) {

            nouvelleAffichage();
        }
    }


    //main
    public static void main(String[] args) {
        new GenJFrame();

    }
}
