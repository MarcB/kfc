package main;

import inter.EntityA;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Player extends GameObject implements EntityA {
    //coordonnee
    private double x;
    private double y;

    //velocity
    private double velX=0;
    private double velY=0;

    private BufferedImage player;

    private Textures tex;

    public Player(double x,double y,Textures tex){

        //affichage
        super(x,y);
        this.tex=tex;
    }

    public void tick(){
        x+=velX;
        y+=velY;

        //gestion de collision avec le rebord de la fenetre

        if(x<=0){            x=0;        }
        if(x>=640-32){            x=640-32;        }

        if(y<=0){            y=0;        }
        if(y>=480-44){            y=480-44;        }
    }

    public Rectangle getBounds(){return new Rectangle((int)x,(int)y,32,32);    }

    public void render(Graphics g){
        g.drawImage(tex.player,(int)x,(int)y,null);
    }

    public  double getX(){        return x;    }
    public double getY(){        return y;    }
    public void setX(double x){        this.x=x;    }
    public  void setY(double y){        this.y=y;    }

    //set de la velocity pour un mouvement smooth
    public void setVelX(double velX){        this.velX=velX;    }
    public void setVelY(double velY){        this.velY=velY;    }
}
