package main;


import inter.EntityA;

import java.awt.*;

public class Bullet extends GameObject implements EntityA {

    

    private Game game;
    private Textures tex;

    public Bullet(double x,double y,Textures tex,Game game){
        super(x,y);
        this.tex=tex;
        this.game=game;
    }

    public void tick(){

        y=y-10;
        if(Physics.Collision(this, game.eb)){
            System.out.println("paf");
        }
    }

    public Rectangle getBounds(){return new Rectangle((int)x,(int)y,32,32);    }

    public void render(Graphics g){
        g.drawImage(tex.missile,(int)x,(int)y,null);
    }

    @Override
    public double getX() {
        return x;
    }

    public double getY(){
        return y;
    }
}
