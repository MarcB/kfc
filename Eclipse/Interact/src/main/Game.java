package main;

import inter.EntityA;
import inter.EntityB;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.LinkedList;

public class Game extends Canvas implements Runnable {

    //creation de proportion
    public static final int WIDTH =320;
    public static final int HEIGHT =WIDTH/12*9;
    public static final int SCALE =2;
    public final String TITLE ="2D Game";

    //boolean de thread
    private boolean running=false;
    private Thread thread;

    //buffered all the window
    private BufferedImage image =new BufferedImage(WIDTH,HEIGHT,BufferedImage.TYPE_INT_RGB);
    private BufferedImage spriteSheet =null;
    private BufferedImage background =null;

    //bool bloque le tir infini
    private boolean isShooting =false;

    //compteur d enemi
    private int enemy_count=1;
    private int enemy_killed=0;

    private Player p;
    private Controller c;
    private Textures tex;

    public LinkedList<EntityA> ea;
    public LinkedList<EntityB> eb;

    //temp
    //private BufferedImage player;


    public void init(){

        requestFocus();
        BufferedImageLoader loader=new BufferedImageLoader();

        try{
            spriteSheet=loader.loadImage("/Sprite.png");
            background=loader.loadImage("/images.jpg");
        }catch(IOException e){
            e.printStackTrace();
        }
     //   SpriteSheet ss=new SpriteSheet(spriteSheet);
       // player=ss.grabImage(5,5,32,32);
        addKeyListener(new KeyInput(this));

        tex=new Textures(this);

        p =new Player(200,200,tex);
        c=new Controller(tex);
        ea=c.getEntityA();
        eb=c.getEntityB();

        c.create_enemy(enemy_count);


    }


    //lanement de thread + verif
    private synchronized void start(){
        if (running)
            return;
        running=true;
        thread =new Thread(this);
        thread.start();
        System.out.println("started");
    }

    //coupure de thread
    private synchronized  void stop(){
        if(!running)
            return;

        running=false;
        try{
            thread.join();
        }catch(InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Stoped");
    }

    //boucle de run
    public void run(){

        init();

        //variable de temps
        long lastTime =System.nanoTime();
        final double amountOfTicks=60.0;
        //test pour synchro
        double ns =1000000000/amountOfTicks;
        //variable qui calcul le temps passé
        double delta=0;
        //display fps ect
        int updates=0;
        int frames =0;
        long timer= System.currentTimeMillis();

        while(running){

          long now=System.nanoTime();
          delta+=(now-lastTime)/ns;
          lastTime=now;
          if(delta >=1){
              tick();
              updates++;
              delta --;
          }
            render();
          frames++;

          if(System.currentTimeMillis()-timer>1000){
              timer+=1000;
              System.out.println(updates+" Ticks,Fps "+frames);
              updates=0;
              frames=0;
          }
        }
        stop();
    }

    private void tick(){
        p.tick();
        c.tick();
    }

    private void render(){
        //buffer strategy ,this refere au canvas
        BufferStrategy bs=this.getBufferStrategy();
        if(bs==null){
            //creation de 3 buffers
            createBufferStrategy(3);
            return;
        }
        Graphics g =bs.getDrawGraphics();
        /////////////////////////////////

        g.drawImage(image,0,0,getWidth(),getHeight(),this);

        g.drawImage(background,0,0,null);

   // g.drawImage(player,100,100,this);
        p.render(g);
        c.render(g);
        /////////////////////////////////
        g.dispose();
        bs.show();
    }

    public void keyPressed(KeyEvent e) {
        int key =e.getKeyCode();

        if(key==KeyEvent.VK_RIGHT){
            p.setVelX(5);
        }else if (key==KeyEvent.VK_LEFT){
            p.setVelX(-5);
        }else if (key==KeyEvent.VK_DOWN){
            p.setVelY(5);
        }else if (key==KeyEvent.VK_UP) {
            p.setVelY(-5);
        }else if (key==KeyEvent.VK_SPACE && !isShooting){
            isShooting=true;
            c.addEntity(new Bullet(p.getX(),p.getY(),tex,this));
        }
    }

    public void keyReleased(KeyEvent e) {
        int key =e.getKeyCode();

        if(key==KeyEvent.VK_RIGHT){
            p.setVelX(0);
        }else if (key==KeyEvent.VK_LEFT){
            p.setVelX(0);
        }else if (key==KeyEvent.VK_DOWN){
            p.setVelY(0);
        }else if (key==KeyEvent.VK_UP) {
            p.setVelY(0);
        }else if (key==KeyEvent.VK_SPACE ){
            isShooting=false;
        }
    }

    public static void main(String[] args) {
        Game game=new Game();

        game.setPreferredSize(new Dimension(WIDTH*SCALE,HEIGHT*SCALE));
        game.setMaximumSize(new Dimension(WIDTH*SCALE,HEIGHT*SCALE));
        game.setMinimumSize(new Dimension(WIDTH*SCALE,HEIGHT*SCALE));


        //init de la frame
        JFrame frame= new JFrame(game.TITLE);
        frame.add(game);
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        //lancement de la boucle
        game.start();
    }

    public BufferedImage getSpriteSheet(){
        return spriteSheet;
    }

    public int getEnemy_count() {
        return enemy_count;
    }

    public void setEnemy_count(int enemy_count) {
        this.enemy_count = enemy_count;
    }

    public int getEnemy_killed() {
        return enemy_killed;
    }

    public void setEnemy_killed(int enemy_killed) {
        this.enemy_killed = enemy_killed;
    }
}
