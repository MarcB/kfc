--drop table AFFECTATION;
--drop table PLAYER;
--drop table TEAM;
--drop table LEAGUE;


create table LEAGUE(
 leagueid  int not null auto_increment  ,
 name  varchar(25) not null  ,
 sport   varchar(20)not null,
 constraint uk_league_name unique(name),
 constraint pk_league primary key(leagueid)
);



create table TEAM(
 teamid  int not null auto_increment,
 name  varchar(25) not null,
 city  varchar(25)not null,
 leagueid int,
 constraint fk_team_leagueid foreign key (leagueid)  references LEAGUE(leagueid),
 constraint uk_team_name unique( name),
 constraint pk_team primary key(teamid)
);


create table PLAYER(
 playerId int not null auto_increment ,
 name     varchar(20) not null,

 position varchar(20) not null,
 salary   numeric(8,2) not null,
 constraint uk_player_name unique(name),
 constraint pk_player primary key (playerid)
);


create table AFFECTATION(
  id int not null auto_increment,
  playerid int  not null,
  teamid int not null,
  constraint fk_affectation_player_id foreign key(playerid) references PLAYER(playerid),
  constraint fk_affectation_team_id foreign key ( teamid) references TEAM(teamid),
  primary key(id)
  );       


INSERT INTO LEAGUE ( LEAGUEID, NAME, SPORT ) VALUES ( 
1, 'Mountain', 'Soccer'); 
INSERT INTO LEAGUE ( LEAGUEID, NAME, SPORT ) VALUES ( 
2, 'Valley', 'Basketball'); 

INSERT INTO TEAM ( TEAMID, NAME, CITY, LEAGUEID ) VALUES ( 
1, 'Honey Bees', 'Visalia', 1); 
INSERT INTO TEAM ( TEAMID, NAME, CITY, LEAGUEID ) VALUES ( 
2, 'Gophers', 'Manteca', 1); 
INSERT INTO TEAM ( TEAMID, NAME, CITY, LEAGUEID ) VALUES ( 
3, 'Deer', 'Bodie', 2); 
INSERT INTO TEAM ( TEAMID, NAME, CITY, LEAGUEID ) VALUES ( 
4, 'Trout', 'Truckee', 2); 
INSERT INTO TEAM ( TEAMID, NAME, CITY, LEAGUEID ) VALUES ( 
5, 'Crows', 'Orland', 1);

INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
1, 'Phil Jones', 'goalkeeper', 100); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
2, 'Alice Smith', 'defender', 505); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
3, 'Bob Roberts', 'midfielder', 65); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
4, 'Grace Phillips', 'forward', 100); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
5, 'Barney Bold', 'defender', 100); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
6, 'Ian Carlyle', 'goalkeeper', 555); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
7, 'Rebecca Struthers', 'midfielder', 777); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
8, 'Anne Anderson', 'forward', 65); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
9, 'Jan Wesley', 'defender', 100); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
10, 'Terry Smithson', 'midfielder', 100); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
11, 'Ben Shore', 'point guard', 188); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
12, 'Chris Farley', 'shooting guard', 577); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
13, 'Audrey Brown', 'small forward', 995); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
14, 'Jack Patterson', 'power forward', 100); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
15, 'Candace Lewis', 'point guard', 100); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
16, 'Linda Berringer', 'point guard', 844); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
17, 'Bertrand Morris', 'shooting guard', 452); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
18, 'Nancy White', 'small forward', 833); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
19, 'Billy Black', 'power forward', 444); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
20, 'Jodie James', 'point guard', 100); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
21, 'Henry Shute', 'goalkeeper', 205); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
22, 'Janice Walker', 'defender', 857); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
23, 'Wally Hendricks', 'midfielder', 748); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
24, 'Gloria Garber', 'forward', 777); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
25, 'Frank Fletcher', 'defender', 399); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
26, 'Hobie Jackson', 'pitcher', 582); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
27, 'Melinda Kendall', 'catcher', 677); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
28, 'Constance Adams', 'substitue', 966); 



INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
1, 1); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
2, 1); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
3, 1); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
4, 1); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
5, 1); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
6, 2); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
7, 2); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
8, 2); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
9, 2); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
10, 2); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
11, 3); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
12, 3); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
13, 3); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
14, 3); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
15, 3); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
16, 4); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
17, 4); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
18, 4); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
19, 4); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
20, 4); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
21, 5); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
22, 5); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
23, 5); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
24, 5); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
25, 5); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
28, 1); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
28, 3); 


