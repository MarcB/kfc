create table if league(
 leagueid int not null auto_increment  ,
 name  varchar(25) not null  ,
 sport   varchar(20)not null,
 constraint uk_league_name unique(name),
 constraint pk_league primary key(leagueid)
);​
​
​
create table if team(
 teamid  int not null auto_increment,
 name  varchar(25) not null,
 city  varchar(25)not null,
 leagueid int,
 constraint fk_team_leagueid foreign key (leagueid)  references league(leagueid),
 constraint uk_team_name unique( name),
 constraint pk_team primary key(teamid)
);
​
​
create table if player(
 playerId int not null auto_increment ,
 name     varchar(20) not null,
​
 position varchar(20) not null,
 salary   numeric(8,2) not null,
 constraint uk_player_name unique(name),
 constraint pk_player primary key (playerid)
);
​
​
create table if affectation(
  id int not null auto_increment,
  playerid int  not null,
  teamid int not null,
  constraint fk_affectation_player_id foreign key(playerid) references PLAYER(playerid),
  constraint fk_affectation_team_id foreign key ( teamid) references TEAM(teamid),
  primary key(id)
  );       
​
​
INSERT INTO league ( leagueid, name, sport ) VALUES ( 
1, 'Mountain', 'Soccer'); 
INSERT INTO league ( leagueid, name, sport ) VALUES ( 
2, 'Valley', 'Basketball'); 
​
INSERT INTO team ( teamid, name, city, leagueid ) VALUES ( 
1, 'Honey Bees', 'Visalia', 1); 
INSERT INTO team ( teamid, name, city, leagueid ) VALUES ( 
2, 'Gophers', 'Manteca', 1); 
INSERT INTO team ( teamid, name, city, leagueid ) VALUES ( 
3, 'Deer', 'Bodie', 2); 
INSERT INTO team ( teamid, name, city, leagueid ) VALUES ( 
4, 'Trout', 'Truckee', 2); 
INSERT INTO team ( teamid, name, city, leagueid ) VALUES ( 
5, 'Crows', 'Orland', 1);
​
INSERT INTO player ( playerid, name, position, salary) VALUES ( 
1, 'Phil Jones', 'goalkeeper', 100); 
INSERT INTO player ( playerid, name, position, salary) VALUES ( 
2, 'Alice Smith', 'defender', 505); 
INSERT INTO player ( playerid, name, position, salary) VALUES ( 
3, 'Bob Roberts', 'midfielder', 65); 
INSERT INTO player ( playerid, name, position, salary) VALUES ( 
4, 'Grace Phillips', 'forward', 100); 
INSERT INTO player ( playerid, name, position, salary) VALUES ( 
5, 'Barney Bold', 'defender', 100); 
INSERT INTO player ( playerid, name, position, salary) VALUES ( 
6, 'Ian Carlyle', 'goalkeeper', 555); 
INSERT INTO player ( playerid, name, position, salary) VALUES ( 
7, 'Rebecca Struthers', 'midfielder', 777); 
INSERT INTO player ( playerid, name, position, salary) VALUES ( 
8, 'Anne Anderson', 'forward', 65); 
INSERT INTO player ( playerid, name, position, salary) VALUES ( 
9, 'Jan Wesley', 'defender', 100); 
INSERT INTO player ( playerid, name, position, salary) VALUES ( 
10, 'Terry Smithson', 'midfielder', 100); 
INSERT INTO player ( playerid, name, position, salary) VALUES ( 
11, 'Ben Shore', 'point guard', 188); 
INSERT INTO player ( playerid, name, position, salary) VALUES ( 
12, 'Chris Farley', 'shooting guard', 577); 
INSERT INTO player ( playerid, name, position, salary) VALUES ( 
13, 'Audrey Brown', 'small forward', 995); 
INSERT INTO player ( playerid, name, position, salary) VALUES ( 
14, 'Jack Patterson', 'power forward', 100); 
INSERT INTO player ( playerid, name, position, salary) VALUES ( 
15, 'Candace Lewis', 'point guard', 100); 
INSERT INTO player ( playerid, name, position, salary) VALUES ( 
16, 'Linda Berringer', 'point guard', 844); 
INSERT INTO player ( playerid, name, position, salary) VALUES ( 
17, 'Bertrand Morris', 'shooting guard', 452); 
INSERT INTO player ( playerid, name, position, salary) VALUES ( 
18, 'Nancy White', 'small forward', 833); 
INSERT INTO player ( playerid, name, position, salary) VALUES ( 
19, 'Billy Black', 'power forward', 444); 
INSERT INTO player ( playerid, name, position, salary) VALUES ( 
20, 'Jodie James', 'point guard', 100); 
INSERT INTO player ( playerid, name, position, salary) VALUES ( 
21, 'Henry Shute', 'goalkeeper', 205); 
INSERT INTO player ( playerid, name, position, salary) VALUES ( 
22, 'Janice Walker', 'defender', 857); 
INSERT INTO player ( playerid, name, position, salary) VALUES ( 
23, 'Wally Hendricks', 'midfielder', 748); 
INSERT INTO player ( playerid, name, position, salary) VALUES ( 
24, 'Gloria Garber', 'forward', 777); 
INSERT INTO player ( playerid, name, position, salary) VALUES ( 
25, 'Frank Fletcher', 'defender', 399); 
INSERT INTO player ( playerid, name, position, salary) VALUES ( 
26, 'Hobie Jackson', 'pitcher', 582); 
INSERT INTO player ( playerid, name, position, salary) VALUES ( 
27, 'Melinda Kendall', 'catcher', 677); 
INSERT INTO player ( playerid, name, position, salary) VALUES ( 
28, 'Constance Adams', 'substitue', 966); 
​
​
​
INSERT INTO affectation (playerid,teamid VALUES ( 
1, 1); 
INSERT INTO affectation (playerid,teamid VALUES ( 
2, 1); 
INSERT INTO affectation (playerid,teamid VALUES ( 
3, 1); 
INSERT INTO affectation (playerid,teamid VALUES ( 
4, 1); 
INSERT INTO affectation (playerid,teamid VALUES ( 
5, 1); 
INSERT INTO affectation (playerid,teamid VALUES ( 
6, 2); 
INSERT INTO affectation (playerid,teamid VALUES ( 
7, 2); 
INSERT INTO affectation (playerid,teamid VALUES ( 
8, 2); 
INSERT INTO affectation (playerid,teamid VALUES ( 
9, 2); 
INSERT INTO affectation (playerid,teamid VALUES ( 
10, 2); 
INSERT INTO affectation (playerid,teamid VALUES ( 
11, 3); 
INSERT INTO affectation (playerid,teamid VALUES ( 
12, 3); 
INSERT INTO affectation (playerid,teamid VALUES ( 
13, 3); 
INSERT INTO affectation (playerid,teamid VALUES ( 
14, 3); 
INSERT INTO affectation (playerid,teamid VALUES ( 
15, 3); 
INSERT INTO affectation (playerid,teamid VALUES ( 
16, 4); 
INSERT INTO affectation (playerid,teamid VALUES ( 
17, 4); 
INSERT INTO affectation (playerid,teamid VALUES ( 
18, 4); 
INSERT INTO affectation (playerid,teamid VALUES ( 
19, 4); 
INSERT INTO affectation (playerid,teamid VALUES ( 
20, 4); 
INSERT INTO affectation (playerid,teamid VALUES ( 
21, 5); 
INSERT INTO affectation (playerid,teamid VALUES ( 
22, 5); 
INSERT INTO affectation (playerid,teamid VALUES ( 
23, 5); 
INSERT INTO affectation (playerid,teamid VALUES ( 
24, 5); 
INSERT INTO affectation (playerid,teamid VALUES ( 
25, 5); 
INSERT INTO affectation (playerid,teamid VALUES ( 
28, 1); 
INSERT INTO affectation (playerid,teamid VALUES ( 
28, 3); 

