drop table if exists affectation ;
drop table if exists player ;
drop table if exists team ;
drop table if exists league ;




create table LEAGUE(
 leagueID  varchar(5)
         constraint pk_league primary key,
 name	 varchar(25)
 		 constraint nn_league_name not null
         constraint uk_league_name unique,
 sport	 varchar(20)
         constraint nn_league_sport not null
);



create table TEAM(
 teamID  varchar(5)
         constraint pk_team primary key,
 name	 varchar(25)
 		 constraint nn_team_name not null
         constraint uk_team_name unique,
 city	 varchar(25)
         constraint nn_team_city not null,
 leagueid varchar(5)
         constraint fk_team_leagueid references League(leagueid)
);


create table PLAYER(
 playerId varchar(5) 
          constraint pk_player primary key,
 name     varchar(20)
 		  constraint nn_player_name not null
          constraint uk_player_name not null,
 position varchar(20)
          constraint nn_player_position not null,
 salary   numeric(8,2)
 		  constraint nn_player_salary not null
);


create table affectation(
  id serial,
  playerid varchar(5) 
           constraint nn_affectation_playerid not null
           constraint fk_affectation_playerid references Player(playerid),
  teamid varchar(5) 
	     constraint nn_affectation_teamid not null
         constraint fk_affectation_teamid references Team(teamid),
  primary key(id)
  ); 			  


INSERT INTO LEAGUE ( LEAGUEID, NAME, SPORT ) VALUES ( 
'L1', 'Mountain', 'Soccer'); 
INSERT INTO LEAGUE ( LEAGUEID, NAME, SPORT ) VALUES ( 
'L2', 'Valley', 'Basketball'); 

INSERT INTO TEAM ( TEAMID, NAME, CITY, LEAGUEID ) VALUES ( 
'T1', 'Honey Bees', 'Visalia', 'L1'); 
INSERT INTO TEAM ( TEAMID, NAME, CITY, LEAGUEID ) VALUES ( 
'T2', 'Gophers', 'Manteca', 'L1'); 
INSERT INTO TEAM ( TEAMID, NAME, CITY, LEAGUEID ) VALUES ( 
'T3', 'Deer', 'Bodie', 'L2'); 
INSERT INTO TEAM ( TEAMID, NAME, CITY, LEAGUEID ) VALUES ( 
'T4', 'Trout', 'Truckee', 'L2'); 
INSERT INTO TEAM ( TEAMID, NAME, CITY, LEAGUEID ) VALUES ( 
'T5', 'Crows', 'Orland', 'L1');

INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
'P1', 'Phil Jones', 'goalkeeper', 100); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
'P2', 'Alice Smith', 'defender', 505); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
'P3', 'Bob Roberts', 'midfielder', 65); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
'P4', 'Grace Phillips', 'forward', 100); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
'P5', 'Barney Bold', 'defender', 100); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
'P6', 'Ian Carlyle', 'goalkeeper', 555); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
'P7', 'Rebecca Struthers', 'midfielder', 777); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
'P8', 'Anne Anderson', 'forward', 65); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
'P9', 'Jan Wesley', 'defender', 100); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
'P10', 'Terry Smithson', 'midfielder', 100); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
'P11', 'Ben Shore', 'point guard', 188); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
'P12', 'Chris Farley', 'shooting guard', 577); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
'P13', 'Audrey Brown', 'small forward', 995); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
'P14', 'Jack Patterson', 'power forward', 100); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
'P15', 'Candace Lewis', 'point guard', 100); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
'P16', 'Linda Berringer', 'point guard', 844); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
'P17', 'Bertrand Morris', 'shooting guard', 452); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
'P18', 'Nancy White', 'small forward', 833); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
'P19', 'Billy Black', 'power forward', 444); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
'P20', 'Jodie James', 'point guard', 100); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
'P21', 'Henry Shute', 'goalkeeper', 205); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
'P22', 'Janice Walker', 'defender', 857); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
'P23', 'Wally Hendricks', 'midfielder', 748); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
'P24', 'Gloria Garber', 'forward', 777); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
'P25', 'Frank Fletcher', 'defender', 399); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
'P26', 'Hobie Jackson', 'pitcher', 582); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
'P27', 'Melinda Kendall', 'catcher', 677); 
INSERT INTO PLAYER ( PLAYERID, NAME, POSITION, SALARY ) VALUES ( 
'P28', 'Constance Adams', 'substitue', 966); 



INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
'P1', 'T1'); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
'P2', 'T1'); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
'P3', 'T1'); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
'P4', 'T1'); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
'P5', 'T1'); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
'P6', 'T2'); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
'P7', 'T2'); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
'P8', 'T2'); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
'P9', 'T2'); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
'P10', 'T2'); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
'P11', 'T3'); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
'P12', 'T3'); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
'P13', 'T3'); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
'P14', 'T3'); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
'P15', 'T3'); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
'P16', 'T4'); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
'P17', 'T4'); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
'P18', 'T4'); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
'P19', 'T4'); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
'P20', 'T4'); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
'P21', 'T5'); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
'P22', 'T5'); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
'P23', 'T5'); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
'P24', 'T5'); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
'P25', 'T5'); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
'P28', 'T1'); 
INSERT INTO AFFECTATION ( PLAYERID, TEAMID ) VALUES ( 
'P28', 'T3'); 


