﻿

CREATE TABLE if not exists ORGANISATION ( 
  ORG_NAME      VARCHAR(30)  ,
  DESCRIPTION   VARCHAR(30),
  THEBOSS       INT,
  CONSTRAINT PK_ORGANISATION
  PRIMARY KEY ( ORG_NAME ) ) ENGINE=InnoDB ; 
  
CREATE TABLE if not exists GANGSTER (
  GANGSTER_ID  INT  NOT NULL AUTO_INCREMENT,
  GNAME        VARCHAR(30),
  NICKNAME     VARCHAR(30),
  BADNESS      INT,
  ORG_NAME     VARCHAR(30),
  CONSTRAINT PK_GANGSTER
  PRIMARY KEY ( GANGSTER_ID ) ) ENGINE=InnoDB; 


CREATE TABLE if not exists JOB ( 
  JOB_ID     INT NOT NULL AUTO_INCREMENT,
  JOB_NAME     VARCHAR(30)  NOT NULL,
  SCORE        INT,
  SETUPCOST    FLOAT, 
  CONSTRAINT PK_JOB
  PRIMARY KEY ( JOB_ID ) )ENGINE=InnoDB ;
  
CREATE TABLE if not exists AFFECTATION (
  GANGSTER_ID   INT NOT NULL,
  JOB_ID        INT NOT NULL,
  CONSTRAINT PK_AFFECTATION
     PRIMARY KEY ( GANGSTER_ID, JOB_ID ) )ENGINE=InnoDB ; 
   
alter table ORGANISATION
   add constraINT FK_organisation_boss foreign key(THEBOSS) references GANGSTER(GANGSTER_ID);
alter table GANGSTER
   add constraINT FK_GANGSTER_ORG_NAME foreign key(ORG_NAME) references ORGANISATION(ORG_NAME);
alter table AFFECTATION
   add constraINT FK_AFFECTATION_GANGSTER foreign key(GANGSTER_ID)
     references GANGSTER(GANGSTER_ID);
alter table AFFECTATION
   add constraINT AFFECTATION_JOB_ID foreign key(JOB_ID) references JOB(JOB_ID);


/* Formatted on 2004/11/28 19:51 (Formatter Plus v4.6.6) */
INSERT INTO ORGANISATION
            (ORG_NAME, DESCRIPTION, THEBOSS)
     VALUES ('Yakuza', 'Japanese Gangsters', NULL);
INSERT INTO ORGANISATION
            (ORG_NAME, DESCRIPTION, THEBOSS)
     VALUES ('Mafia', 'Italian Bad Guys', NULL);
INSERT INTO ORGANISATION
            (ORG_NAME, DESCRIPTION, THEBOSS)
     VALUES ('Triads', 'Kung Fu Movie Extras', NULL);

INSERT INTO GANGSTER
            (GANGSTER_ID, GNAME, NICKNAME, BADNESS, ORG_NAME)
     VALUES (1, 'Yojimbo', 'Bodyguard', 7, 'Yakuza');

INSERT INTO GANGSTER
            (GANGSTER_ID, GNAME, NICKNAME, BADNESS, ORG_NAME)
     VALUES (2, 'Takeshi', 'Master', 10, 'Yakuza');

INSERT INTO GANGSTER
            (GANGSTER_ID, GNAME, NICKNAME, BADNESS, ORG_NAME)
     VALUES (3, 'Yuriko', 'Four finger', 4, 'Yakuza');

INSERT INTO GANGSTER
            (GANGSTER_ID, GNAME, NICKNAME, BADNESS, ORG_NAME)
     VALUES (4, 'Chow', 'Killer', 9, 'Triads');

INSERT INTO GANGSTER
            (GANGSTER_ID, GNAME, NICKNAME, BADNESS, ORG_NAME)
     VALUES (5, 'Shogi', 'Lightning', 8, 'Triads');

INSERT INTO GANGSTER
            (GANGSTER_ID, GNAME, NICKNAME, BADNESS, ORG_NAME)
     VALUES (6, 'Valentino', 'Pizza-Face', 4, 'Mafia');

INSERT INTO GANGSTER
            (GANGSTER_ID, GNAME, NICKNAME, BADNESS, ORG_NAME)
     VALUES (7, 'Toni', 'Toohless', 2, 'Mafia');

INSERT INTO GANGSTER
            (GANGSTER_ID, GNAME, NICKNAME, BADNESS, ORG_NAME)
     VALUES (8, 'Corleone', 'Godfather', 6, 'Mafia');

UPDATE ORGANISATION
   SET THEBOSS = 2
 WHERE ORG_NAME = 'Yakuza';
UPDATE ORGANISATION
   SET THEBOSS = 4
 WHERE ORG_NAME = 'Triads';
UPDATE ORGANISATION
   SET THEBOSS = 8
 WHERE ORG_NAME = 'Mafia';

INSERT INTO JOB
            (JOB_ID, JOB_NAME, SCORE, SETUPCOST)
     VALUES (1, '10th Street Jeweler Heist', 5000, 50);

INSERT INTO JOB
            (JOB_ID, JOB_NAME, SCORE, SETUPCOST)
     VALUES (2, 'The Great Train Robbery', 2000000, 500000);

INSERT INTO JOB
            (JOB_ID, JOB_NAME, SCORE, SETUPCOST)
     VALUES (3, 'Cheap Liquor Snatch and Grab', 50, 0);


INSERT INTO AFFECTATION
            (GANGSTER_ID, JOB_ID)
     VALUES (6, 1);

INSERT INTO AFFECTATION
            (GANGSTER_ID, JOB_ID)
     VALUES (8, 1);

INSERT INTO AFFECTATION
            (GANGSTER_ID, JOB_ID)
     VALUES (4, 3);

INSERT INTO AFFECTATION
            (GANGSTER_ID, JOB_ID)
     VALUES (4, 2);

INSERT INTO AFFECTATION
            (GANGSTER_ID, JOB_ID)
     VALUES (1, 2);