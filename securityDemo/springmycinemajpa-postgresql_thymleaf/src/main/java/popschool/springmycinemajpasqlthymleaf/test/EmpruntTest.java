package popschool.springmycinemajpasqlthymleaf.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import popschool.springmycinemajpasqlthymleaf.SpringmycinemajpasqlthymleafApplication;
import popschool.springmycinemajpasqlthymleaf.dao.ClientRepository;
import popschool.springmycinemajpasqlthymleaf.dao.EmpruntRepository;
import popschool.springmycinemajpasqlthymleaf.dao.FilmRepository;
import popschool.springmycinemajpasqlthymleaf.model.Emprunt;

@Component
public class EmpruntTest implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(SpringmycinemajpasqlthymleafApplication.class);

    @Autowired
    private EmpruntRepository empruntRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private FilmRepository filmRepository;

    private void information(Emprunt e){
        log.info(e.toString());
    }

    @Override
    public void run(String... args) throws Exception {

        //Creation d'un emprunt
        log.info("---------------------------------------------------------------------------------------------------");
        log.info("                                       Création d'un emprunt                                       ");
        log.info("---------------------------------------------------------------------------------------------------");
        /*Optional<Client> client = clientRepository.findById(4L);
        Optional<Film> film = filmRepository.findById(30L);
        try{
            empruntRepository.save(new Emprunt(client.get(), film.get(), "NON", new Date()));
        }catch (Exception ex){
            System.out.println(ex);
        }*/

        //Modification d'un emprunt
        /*log.info("---------------------------------------------------------------------------------------------------");
        log.info("                                       Modification d'un emprunt                                   ");
        log.info("---------------------------------------------------------------------------------------------------");
        Optional<Emprunt> emprunt = empruntRepository.findById(5L);
        Emprunt emp = emprunt.get();
        emp.setRetour("OUI");
        log.info(emp.toString());
        empruntRepository.save(emp);*/


        //find emprunt a modifier
        /*log.info("---------------------------------------------------------------------------------------------------");
        log.info("                                    trouver l'emprunt a rendre                                     ");
        log.info("---------------------------------------------------------------------------------------------------");
        String non = "NON";
        String nom = "TINTIN";
        String titre = "Le Parrain";
        Optional<Emprunt> emp = empruntRepository.findByClient_NomAndFilm_TitreAndRetour(nom,titre,non);
        if (emp.isPresent()){
            log.info(emp.toString());
        }else {
            log.info("Pas d'emprunt");
        }*/

        //Afficher tous les clients
        /*log.info("---------------------------------------------------------------------------------------------------");
        log.info("                                    afficher tous les emprunts                                     ");
        log.info("---------------------------------------------------------------------------------------------------");
        for (Emprunt e : empruntRepository.findAll()){
            log.info(e.toString());
        }*/


    }
}
