package popschool.springmycinemajpasqlthymleaf.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import popschool.springmycinemajpasqlthymleaf.service.VideoService;
import popschool.springmycinemajpasqlthymleaf.SpringmycinemajpasqlthymleafApplication;

@Component
public class VideoServiceTest implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(SpringmycinemajpasqlthymleafApplication.class);

    @Autowired
    private VideoService videoService;


    private void information(VideoService v) {
        log.info(v.toString());

    }


    @Override
    public void run(String... args) throws Exception {

        //info realisateur et acteur - traitement des tuples
        log.info("TROUVER les infos du realisateur et acteur d'un film demandé :");
        log.info("---------------------------------------------------------------------------------------------------");
        videoService.infoRealisateurActeur("Le Parrain").forEach(f->{
            log.info("realisateur : " + f.get("realisateur", String.class)+
                    " acteur : "+ f.get("acteurnom",String.class));
        });


    }
}
