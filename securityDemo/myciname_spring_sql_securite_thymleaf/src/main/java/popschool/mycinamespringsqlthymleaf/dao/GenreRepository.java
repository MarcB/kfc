package popschool.mycinamespringsqlthymleaf.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import popschool.mycinamespringsqlthymleaf.model.Client;
import popschool.mycinamespringsqlthymleaf.model.Film;
import popschool.mycinamespringsqlthymleaf.model.Genre;

import javax.persistence.Tuple;
import java.util.List;
import java.util.Optional;

public interface GenreRepository extends CrudRepository<Genre, Long> {



}
