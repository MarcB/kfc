package popschool.mycinamespringsqlthymleaf.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import popschool.mycinamespringsqlthymleaf.dao.ClientRepository;
import popschool.mycinamespringsqlthymleaf.dao.EmpruntRepository;
import popschool.mycinamespringsqlthymleaf.dao.FilmRepository;
import popschool.mycinamespringsqlthymleaf.model.Client;
import popschool.mycinamespringsqlthymleaf.model.Emprunt;
import popschool.mycinamespringsqlthymleaf.model.Film;

import javax.persistence.Tuple;
import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class VideoServiceImp implements VideoService {

    @Autowired
    private EmpruntRepository empruntRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private FilmRepository filmRepository;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                 Methode Table Emprunt                                                      //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public void emprunter(String nom, String titre) {
        Optional<Client> client = clientRepository.findByNomLike(nom);
        Optional<Film> film = filmRepository.findByTitreLike(titre);
        try{
            empruntRepository.save(new Emprunt(client.get(), film.get(), "NON", new Date()));
        }catch (Exception ex){
            System.out.println(ex);
        }
    }

    @Override
    public void retourEmprunt(String nom, String titre) {
        Optional<Emprunt> emprunt = empruntRepository.findByClient_NomAndFilm_TitreAndRetour(nom,titre,"NON");
        Emprunt emp = emprunt.get();
        emp.setRetour("OUI");
        empruntRepository.save(emp);

    }

    @Override
    public List<Emprunt> findAllEmprunt() {

        return (List<Emprunt>) empruntRepository.findAll();
    }

    @Override
    public List<Emprunt> empruntNonRendu() {
        return empruntRepository.findByRetour("NON");
    }

    @Override
    public Emprunt empruntById(Long id) {
        return empruntRepository.findById(id).get();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                    Methode Table Client                                                    //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public List<Client> ensClient() {
        return clientRepository.findAllByOrderByNom();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                    Methode Table Film                                                      //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public List<Film> ensFilmEmpruntable() {
        return filmRepository.findAllFilmEmpruntable();
    }

    @Override
    public List<Film> ensFilm(){
        return filmRepository.findAllByOrderByTitre();
    }

    @Override
    public List<Film> ensFilmGenre(String genre) {
        return filmRepository.findByGenre_Nature(genre);
    }

    @Override
    public List<Tuple> infoRealisateurActeur(String titre) {
        return filmRepository.findinfoRealisateurAndActeur(titre);
    }

    @Override
    public int nbrFilmDuGenre(String genre) {
        return filmRepository.findNbreFilmsByOneGenre(genre);
    }

    @Override
    public List<Film> ensFilmEmpruntesByClient(Optional<Client> client) {
        return filmRepository.findFilmEmprunteByClient(client);
    }


}
