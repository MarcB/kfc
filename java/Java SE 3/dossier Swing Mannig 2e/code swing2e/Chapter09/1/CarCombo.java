/** 
 *  Copyright 1999-2002 Matthew Robinson and Pavel Vorobiev. 
 *  All Rights Reserved. 
 * 
 *  =================================================== 
 *  This program contains code from the book "Swing" 
 *  2nd Edition by Matthew Robinson and Pavel Vorobiev 
 *  http://www.spindoczine.com/sbe 
 *  =================================================== 
 * 
 *  The above paragraph must be included in full, unmodified 
 *  and completely intact in the beginning of any source code 
 *  file that references, copies or uses (in any way, shape 
 *  or form) code contained in this file. 
 */ 

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import dl.*;

public class CarCombo extends JFrame {	// Example name was changed !

	public CarCombo() {
		super("Car Combo Example");
		getContentPane().setLayout(new BorderLayout());

		Vector cars = new Vector();
		Car maxima = new Car("Maxima", "Nissan", new ImageIcon(
			"maxima.gif"));
		maxima.addTrim("GXE", 21499, 19658, "3.0L V6 190-hp");
		maxima.addTrim("SE",  23499, 21118, "3.0L V6 190-hp");
		maxima.addTrim("GLE", 26899, 24174, "3.0L V6 190-hp");
		cars.addElement(maxima);

		Car accord = new Car("Accord", "Honda", new ImageIcon(
			"accord.gif"));
		accord.addTrim("LX Sedan", 21700, 19303, "3.0L V6 200-hp");
		accord.addTrim("EX Sedan", 24300, 21614, "3.0L V6 200-hp");
		cars.addElement(accord);

		Car camry = new Car("Camry", "Toyota", new ImageIcon(
			"camry.gif"));
		camry.addTrim("LE V6", 21888, 19163, "3.0L V6 194-hp");
		camry.addTrim("XLE V6", 24998, 21884, "3.0L V6 194-hp");
		cars.addElement(camry);

		Car lumina = new Car("Lumina", "Chevrolet", new ImageIcon(
			"lumina.gif"));
		lumina.addTrim("LS", 19920, 18227, "3.1L V6 160-hp");
		lumina.addTrim("LTZ", 20360, 18629, "3.8L V6 200-hp");
		cars.addElement(lumina);

		Car taurus = new Car("Taurus", "Ford", new ImageIcon(
			"taurus.gif"));
		taurus.addTrim("LS", 17445, 16110, "3.0L V6 145-hp");
		taurus.addTrim("SE", 18445, 16826, "3.0L V6 145-hp");
		taurus.addTrim("SHO", 29000, 26220, "3.4L V8 235-hp");
		cars.addElement(taurus);

		Car passat = new Car("Passat", "Volkswagen", new ImageIcon(
			"passat.gif"));
		passat.addTrim("GLS V6", 23190, 20855, "2.8L V6 190-hp");
		passat.addTrim("GLX", 26250, 23589, "2.8L V6 190-hp");
		cars.addElement(passat);
		
		getContentPane().setLayout(new GridLayout(1, 2, 5, 3));
		CarPanel pl = new CarPanel("Base Model", cars);
		getContentPane().add(pl);
		CarPanel pr = new CarPanel("Compare to", cars);
		getContentPane().add(pr);
		
		pl.selectCar(maxima);
		pr.selectCar(accord);
		setResizable(false);
		pack();
	}

	public static void main(String argv[]) {
		CarCombo frame = new CarCombo();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}

class Car {

	protected String m_name;
	protected String m_manufacturer;
	protected Icon   m_img;
	protected Vector m_trims;

	public Car(String name, String manufacturer, Icon img) {
		m_name = name;
		m_manufacturer = manufacturer;
		m_img = img;
		m_trims = new Vector();
	}

	public void addTrim(String name, int MSRP, int invoice, 
		String engine) {
		Trim trim = new Trim(this, name, MSRP, invoice, engine);
		m_trims.addElement(trim);
	}

	public String getName() {
		return m_name;
	}

	public String getManufacturer() {
		return m_manufacturer;
	}

	public Icon getIcon() {
		return m_img;
	}

	public Vector getTrims() {
		return m_trims;
	}

	public String toString() {
		return m_manufacturer+" "+m_name;
	}
}

class Trim {

	protected Car    m_parent;
	protected String m_name;
	protected int    m_MSRP;
	protected int    m_invoice;
	protected String m_engine;

	public Trim(Car parent, String name, int MSRP, int invoice, 
		String engine) {
		m_parent = parent;
		m_name = name;
		m_MSRP = MSRP;
		m_invoice = invoice;
		m_engine = engine;
	}

	public Car getCar() {
		return m_parent;
	}

	public String getName() {
		return m_name;
	}

	public int getMSRP() {
		return m_MSRP;
	}

	public int getInvoice() {
		return m_invoice;
	}

	public String getEngine() {
		return m_engine;
	}

	public String toString() {
		return m_name;
	}
}

class CarPanel extends JPanel {

	protected JComboBox m_cbCars;
	protected JComboBox m_cbTrims;
	protected JLabel m_lblImg;
	protected JLabel m_lblMSRP;
	protected JLabel m_lblInvoice;
	protected JLabel m_lblEngine;

	public CarPanel(String title, Vector cars) {
		super();
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		setBorder(new CompoundBorder(
			new TitledBorder(new EtchedBorder(), title),
			new EmptyBorder(5, 10, 5, 10))
		);

		JPanel p = new JPanel(new DialogLayout());
		p.add(new JLabel("Model:"));
		m_cbCars = new JComboBox(cars);
		ActionListener lst = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Car car = (Car)m_cbCars.getSelectedItem();
				if (car != null)
					showCar(car);
			}
		};
		m_cbCars.addActionListener(lst);
		p.add(m_cbCars);

		p.add(new JLabel("Trim:"));
		m_cbTrims = new JComboBox();
		lst = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Trim trim = (Trim)m_cbTrims.getSelectedItem();
				if (trim != null)
					showTrim(trim);
			}
		};
		m_cbTrims.addActionListener(lst);
		p.add(m_cbTrims);
		add(p);

		p = new JPanel();
		m_lblImg = new JLabel();
		m_lblImg.setHorizontalAlignment(JLabel.CENTER);
		m_lblImg.setPreferredSize(new Dimension(140, 80));
		m_lblImg.setBorder(new BevelBorder(BevelBorder.LOWERED));
		p.add(m_lblImg);
		add(p);

		p = new JPanel();
		p.setLayout(new GridLayout(3, 2, 10, 5));
		p.add(new JLabel("MSRP:"));
		m_lblMSRP = new JLabel();
		p.add(m_lblMSRP);

		p.add(new JLabel("Invoice:"));
		m_lblInvoice = new JLabel();
		p.add(m_lblInvoice);

		p.add(new JLabel("Engine:"));
		m_lblEngine = new JLabel();
		p.add(m_lblEngine);
		add(p);
	}

	public void selectCar(Car car) {
		m_cbCars.setSelectedItem(car);
	}

	public void showCar(Car car) {
		m_lblImg.setIcon(car.getIcon());
		if (m_cbTrims.getItemCount() > 0)
			m_cbTrims.removeAllItems();
		Vector v = car.getTrims();
		for (int k=0; k<v.size(); k++)
			m_cbTrims.addItem(v.elementAt(k));
		m_cbTrims.grabFocus();
	}

	public void showTrim(Trim trim) {
		m_lblMSRP.setText("$"+trim.getMSRP());
		m_lblInvoice.setText("$"+trim.getInvoice());
		m_lblEngine.setText(trim.getEngine());
	}

}
