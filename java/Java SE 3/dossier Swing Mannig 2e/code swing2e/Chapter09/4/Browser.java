/** 
 *  Copyright 1999-2002 Matthew Robinson and Pavel Vorobiev. 
 *  All Rights Reserved. 
 * 
 *  =================================================== 
 *  This program contains code from the book "Swing" 
 *  2nd Edition by Matthew Robinson and Pavel Vorobiev 
 *  http://www.spindoczine.com/sbe 
 *  =================================================== 
 * 
 *  The above paragraph must be included in full, unmodified 
 *  and completely intact in the beginning of any source code 
 *  file that references, copies or uses (in any way, shape 
 *  or form) code contained in this file. 
 */ 

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.text.*;
import javax.swing.text.html.*;

public class Browser extends JFrame {
	
	public static final String APP_NAME = "ComboBox With Memory";

	protected JEditorPane   m_browser;
	protected MemComboBox   m_locator;
	protected AnimatedLabel m_runner;
	protected JLabel        m_status;

	public Browser() {
		super(APP_NAME);
		setSize(500, 300);
		
		JPanel p = new JPanel();
		p.setLayout(new BoxLayout(p, BoxLayout.X_AXIS));
		p.add(new JLabel("Address"));
		p.add(Box.createRigidArea(new Dimension(10, 1)));

		m_locator = new MemComboBox();
		m_locator.load("addresses.dat");
		BrowserListener lst = new BrowserListener();
		m_locator.addActionListener(lst);

		p.add(m_locator);
		p.add(Box.createRigidArea(new Dimension(10, 1)));

		// NEW
		MemComboAgent agent = new MemComboAgent(m_locator);

		m_runner = new AnimatedLabel("clock", 8);
		p.add(m_runner);
		getContentPane().add(p, BorderLayout.NORTH);

		m_browser = new JEditorPane();
		m_browser.setEditable(false);
		m_browser.addHyperlinkListener(lst);

		JScrollPane sp = new JScrollPane();
		sp.getViewport().add(m_browser);
		getContentPane().add(sp, BorderLayout.CENTER);
		
		m_status = new JLabel("Ready");
		m_status.setForeground(Color.black);
		m_status.setBorder(new CompoundBorder(
			new EmptyBorder(2, 5, 2, 5),
			new SoftBevelBorder(SoftBevelBorder.LOWERED)));
		getContentPane().add(m_status, BorderLayout.SOUTH);
		
		// Bug fix for empty labels
		Dimension d = m_status.getPreferredSize();
		m_status.setPreferredSize(new Dimension(150, d.height));

		WindowListener wndCloser = new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				m_locator.save("addresses.dat");
				System.out.println("Saving data");
			}
		};
		addWindowListener(wndCloser);

		m_locator.grabFocus();
	}
	
	class BrowserListener 
		implements ActionListener, HyperlinkListener {
	
		public void actionPerformed(ActionEvent evt) {
			String sUrl = (String)m_locator.getSelectedItem();
			if (sUrl == null || sUrl.length() == 0 || 
				m_runner.getRunning())
				return;

			BrowserLoader loader = new BrowserLoader(sUrl);
			loader.start();
		}

		public void hyperlinkUpdate(HyperlinkEvent e) {
			URL url = e.getURL();
			if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
				if (url == null || m_runner.getRunning())
					return;
				BrowserLoader loader = new BrowserLoader(url.toString());
				loader.start();
			}
			else if (e.getEventType() == HyperlinkEvent.EventType.ENTERED) {
				m_status.setText(url == null ? "" : url.toString());
			}
			else if (e.getEventType() == HyperlinkEvent.EventType.EXITED) {
				m_status.setText("");
			}
		}
	}

	
	class BrowserLoader 
		extends Thread {
	
		protected String m_sUrl;

		public BrowserLoader(String sUrl) {
			m_sUrl = sUrl;
		}

		public void run() {
			System.out.println("Loading "+m_sUrl);
			m_browser.setCursor( Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			m_runner.setRunning(true);

			try {
				URL source = new URL(m_sUrl);
				m_browser.setPage(source);
				m_locator.add(m_sUrl);
			}
			catch (Exception e) {
				JOptionPane.showMessageDialog(Browser.this, 
					"Error: "+e.toString(),
					APP_NAME, JOptionPane.WARNING_MESSAGE);
			}

			m_runner.setRunning(false);
			m_browser.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		}
	}

	public static void main(String argv[]) {
		Browser frame = new Browser();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}

// NEW
class MemComboAgent
	extends KeyAdapter {

	protected JComboBox   m_comboBox;
	protected JTextField  m_editor;

	public MemComboAgent(JComboBox comboBox) {
		m_comboBox = comboBox;
		m_editor = (JTextField)comboBox.getEditor().
			getEditorComponent();
		m_editor.addKeyListener(this);
	}

	public void keyReleased(KeyEvent e) {
		char ch = e.getKeyChar();
		if (ch == KeyEvent.CHAR_UNDEFINED || Character.isISOControl(ch))
			return;
		int pos = m_editor.getCaretPosition();
		String str = m_editor.getText();
		if (str.length() == 0)
			return;

		for (int k=0; k<m_comboBox.getItemCount(); k++) {
			String item = m_comboBox.getItemAt(k).toString();
			if (item.startsWith(str)) {
				m_editor.setText(item);
				m_editor.setCaretPosition(item.length());
				m_editor.moveCaretPosition(pos);
				break;
			}
		}
	}
}

class MemComboBox 
	extends JComboBox {

	public static final int MAX_MEM_LEN = 30;

	public MemComboBox() {
		setEditable(true);
	}

	public void add(String item) {
		removeItem(item);
		insertItemAt(item, 0);
		setSelectedItem(item);
		if (getItemCount() > MAX_MEM_LEN)
			removeItemAt(getItemCount()-1);
	}

	public void load(String fName) {
		try {
			if (getItemCount() > 0)
				removeAllItems();
			File f = new File(fName);
			if (!f.exists())
				return;
			FileInputStream fStream = 
				new FileInputStream(f);
			ObjectInput  stream  =  
				new  ObjectInputStream(fStream);

			Object obj = stream.readObject();
			if (obj instanceof ComboBoxModel)
				setModel((ComboBoxModel)obj);

			stream.close();
			fStream.close();
		}
		catch (Exception e) {
			e.printStackTrace();
			System.err.println("Serialization error: "+e.toString());
		}
	}

	public void save(String fName) {
		try {
			FileOutputStream fStream = 
				new FileOutputStream(fName);
			ObjectOutput  stream  =  
				new  ObjectOutputStream(fStream);

			stream.writeObject(getModel());

			stream.flush();
			stream.close();
			fStream.close();
		}
		catch (Exception e) {
			e.printStackTrace();
			System.err.println("Serialization error: "+e.toString());
		}
	}
}

class AnimatedLabel 
	extends JLabel
	implements Runnable {

	protected Icon[] m_icons;
	protected int m_index = 0;
	protected boolean m_isRunning;

	public AnimatedLabel(String gifName, int numGifs) {
		m_icons = new Icon[numGifs];
		for (int k=0; k<numGifs; k++)
			m_icons[k] = new ImageIcon(gifName+k+".gif");
		setIcon(m_icons[0]);

		Thread tr = new Thread(this);
		tr.setPriority(Thread.MAX_PRIORITY);
		tr.start();
	}

	public void setRunning(boolean isRunning) {
		m_isRunning = isRunning;
	}

	public boolean getRunning() {
		return m_isRunning;
	}

	public void run() {
		while(true) {
			if (m_isRunning) {
				m_index++;
				if (m_index >= m_icons.length)
					m_index = 0;
				setIcon(m_icons[m_index]);
				Graphics g = getGraphics();
				m_icons[m_index].paintIcon(this, g, 0, 0);
			}
			else {
				if (m_index > 0) {
					m_index = 0;
					setIcon(m_icons[0]);
				}
			}
			try { Thread.sleep(500); } 
			catch (InterruptedException ex) {}
		}
	}
}
