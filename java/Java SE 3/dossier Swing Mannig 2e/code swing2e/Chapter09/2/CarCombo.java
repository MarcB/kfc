/** 
 *  Copyright 1999-2002 Matthew Robinson and Pavel Vorobiev. 
 *  All Rights Reserved. 
 * 
 *  =================================================== 
 *  This program contains code from the book "Swing" 
 *  2nd Edition by Matthew Robinson and Pavel Vorobiev 
 *  http://www.spindoczine.com/sbe 
 *  =================================================== 
 * 
 *  The above paragraph must be included in full, unmodified 
 *  and completely intact in the beginning of any source code 
 *  file that references, copies or uses (in any way, shape 
 *  or form) code contained in this file. 
 */ 

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import dl.*;

public class CarCombo extends JFrame {	// Example name was changed !

	public CarCombo() {
		super("Car Combo Example");
		getContentPane().setLayout(new BorderLayout());

		Vector cars = new Vector();
		Car maxima = new Car("Maxima", "Nissan", new ImageIcon(
			"maxima.gif"));
		maxima.addTrim("GXE", 21499, 19658, "3.0L V6 190-hp");
		maxima.addTrim("SE",  23499, 21118, "3.0L V6 190-hp");
		maxima.addTrim("GLE", 26899, 24174, "3.0L V6 190-hp");
		cars.addElement(maxima);

		Car accord = new Car("Accord", "Honda", new ImageIcon(
			"accord.gif"));
		accord.addTrim("LX Sedan", 21700, 19303, "3.0L V6 200-hp");
		accord.addTrim("EX Sedan", 24300, 21614, "3.0L V6 200-hp");
		cars.addElement(accord);

		Car camry = new Car("Camry", "Toyota", new ImageIcon(
			"camry.gif"));
		camry.addTrim("LE V6", 21888, 19163, "3.0L V6 194-hp");
		camry.addTrim("XLE V6", 24998, 21884, "3.0L V6 194-hp");
		cars.addElement(camry);

		Car lumina = new Car("Lumina", "Chevrolet", new ImageIcon(
			"lumina.gif"));
		lumina.addTrim("LS", 19920, 18227, "3.1L V6 160-hp");
		lumina.addTrim("LTZ", 20360, 18629, "3.8L V6 200-hp");
		cars.addElement(lumina);

		Car taurus = new Car("Taurus", "Ford", new ImageIcon(
			"taurus.gif"));
		taurus.addTrim("LS", 17445, 16110, "3.0L V6 145-hp");
		taurus.addTrim("SE", 18445, 16826, "3.0L V6 145-hp");
		taurus.addTrim("SHO", 29000, 26220, "3.4L V8 235-hp");
		cars.addElement(taurus);

		Car passat = new Car("Passat", "Volkswagen", new ImageIcon(
			"passat.gif"));
		passat.addTrim("GLS V6", 23190, 20855, "2.8L V6 190-hp");
		passat.addTrim("GLX", 26250, 23589, "2.8L V6 190-hp");
		cars.addElement(passat);
		
		getContentPane().setLayout(new GridLayout(1, 2, 5, 3));
		CarPanel pl = new CarPanel("Base Model", cars);
		getContentPane().add(pl);
		CarPanel pr = new CarPanel("Compare to", cars);
		getContentPane().add(pr);
		
		pl.selectCar(maxima);
		pr.selectCar(accord);
		setResizable(false);
		pack();
	}

	public static void main(String argv[]) {
		CarCombo frame = new CarCombo();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}

class Car {

	protected String m_name;
	protected String m_manufacturer;
	protected Icon   m_img;
	protected Vector m_trims;

	public Car(String name, String manufacturer, Icon img) {
		m_name = name;
		m_manufacturer = manufacturer;
		m_img = img;
		m_trims = new Vector();
	}

	public void addTrim(String name, int MSRP, int invoice, 
		String engine) {
		Trim trim = new Trim(this, name, MSRP, invoice, engine);
		m_trims.addElement(trim);
	}

	public String getName() {
		return m_name;
	}

	public String getManufacturer() {
		return m_manufacturer;
	}

	public Icon getIcon() {
		return m_img;
	}

	public Vector getTrims() {
		return m_trims;
	}

	public String toString() {
		return m_manufacturer+" "+m_name;
	}
}

class Trim {

	protected Car    m_parent;
	protected String m_name;
	protected int    m_MSRP;
	protected int    m_invoice;
	protected String m_engine;

	public Trim(Car parent, String name, int MSRP, int invoice, 
		String engine) {
		m_parent = parent;
		m_name = name;
		m_MSRP = MSRP;
		m_invoice = invoice;
		m_engine = engine;
	}

	public Car getCar() {
		return m_parent;
	}

	public String getName() {
		return m_name;
	}

	public int getMSRP() {
		return m_MSRP;
	}

	public int getInvoice() {
		return m_invoice;
	}

	public String getEngine() {
		return m_engine;
	}

	public String toString() {
		return m_name;
	}
}

class CarPanel extends JPanel {

	protected JComboBox m_cbCars;
	protected JLabel m_txtModel;	// NEW
	protected JLabel m_lblImg;
	protected JLabel m_lblMSRP;
	protected JLabel m_lblInvoice;
	protected JLabel m_lblEngine;

	public CarPanel(String title, Vector cars) {
		super();
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		setBorder(new CompoundBorder(
			new TitledBorder(new EtchedBorder(), title),
			new EmptyBorder(5, 10, 5, 10))
		);

		JPanel p = new JPanel(new DialogLayout());
		p.add(new JLabel("Model:"));
		m_txtModel = new JLabel("");	// NEW
		m_txtModel.setForeground(Color.black);
		p.add(m_txtModel);

		p.add(new JLabel("Trim:"));
		CarComboBoxModel model = new CarComboBoxModel(cars);	// NEW
		m_cbCars = new JComboBox(model);
		m_cbCars.setRenderer(new IconComboRenderer());
		ActionListener lst = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ListData data = (ListData)m_cbCars.getSelectedItem();
				Object obj = data.getObject();
				if (obj instanceof Trim)
					showTrim((Trim)obj);
			}
		};
		m_cbCars.addActionListener(lst);
		p.add(m_cbCars);
		add(p);

		p = new JPanel();
		m_lblImg = new JLabel();
		m_lblImg.setHorizontalAlignment(JLabel.CENTER);
		m_lblImg.setPreferredSize(new Dimension(140, 80));
		m_lblImg.setBorder(new BevelBorder(BevelBorder.LOWERED));
		p.add(m_lblImg);
		add(p);

		p = new JPanel();
		p.setLayout(new GridLayout(3, 2, 10, 5));
		p.add(new JLabel("MSRP:"));
		m_lblMSRP = new JLabel();
		p.add(m_lblMSRP);

		p.add(new JLabel("Invoice:"));
		m_lblInvoice = new JLabel();
		p.add(m_lblInvoice);

		p.add(new JLabel("Engine:"));
		m_lblEngine = new JLabel();
		p.add(m_lblEngine);
		add(p);
	}

	public synchronized void selectCar(Car car) {
		for (int k=0; k<m_cbCars.getItemCount(); k++) { // NEW
			ListData obj = (ListData)m_cbCars.getItemAt(k);
			if (obj.getObject() == car)
				m_cbCars.setSelectedItem(obj);
		}
	}

	public synchronized void showTrim(Trim trim) {
		Car car = trim.getCar(); // NEW
		m_txtModel.setText(car.toString());
		m_lblImg.setIcon(car.getIcon());
		m_lblMSRP.setText("$"+trim.getMSRP());
		m_lblInvoice.setText("$"+trim.getInvoice());
		m_lblEngine.setText(trim.getEngine());
	}

}

// NEW
class ListData {

	protected Icon    m_icon;
	protected int     m_index;
	protected boolean m_selectable;
	protected Object  m_data;

	public ListData(Icon icon, int index, boolean selectable, 
		Object data) {
		m_icon = icon;
		m_index = index;
		m_selectable = selectable;
		m_data = data;
	}

	public Icon getIcon() {
		return m_icon;
	}

	public int getIndex() {
		return m_index;
	}

	public boolean isSelectable() {
		return m_selectable;
	}

	public Object getObject() {
		return m_data;
	}

	public String toString() {
		return m_data.toString();
	}
}

class CarComboBoxModel
	extends DefaultComboBoxModel {

	public static final ImageIcon ICON_CAR = 
		new ImageIcon("car.gif");
	public static final ImageIcon ICON_TRIM = 
		new ImageIcon("trim.gif");

	public CarComboBoxModel(Vector cars) {
		for (int k=0; k<cars.size(); k++) {
			Car car = (Car)cars.elementAt(k);
			addElement(new ListData(ICON_CAR, 0, false, car));

			Vector v = car.getTrims();
			for (int i=0; i<v.size(); i++) {
				Trim trim = (Trim)v.elementAt(i);
				addElement(new ListData(ICON_TRIM, 1, true, trim));
			}
		}
	}

	public void setSelectedItem(Object item) {
		if (item instanceof ListData) {
			ListData ldata = (ListData)item;
			if (!ldata.isSelectable()) {
				Object newItem = null;
				int index = getIndexOf(item);
				for (int k=index+1; k<getSize(); k++) {
					Object item1 = getElementAt(k);
					if (item1 instanceof ListData) {
						ListData ldata1 = (ListData)item1;
						if (!ldata1.isSelectable())
							continue;
					}
					newItem = item1;
					break;
				}
				if (newItem==null)
					return;		// Selection failed
				item = newItem;
			}
		}
		super.setSelectedItem(item);
	}
}

class IconComboRenderer 
	extends    JLabel 
	implements ListCellRenderer {

	public static final int OFFSET = 16;

	protected Color m_textSelectionColor = Color.white;
	protected Color m_textNonSelectionColor = Color.black;
	protected Color m_textNonselectableColor = Color.gray;
	protected Color m_bkSelectionColor = new Color(0, 0, 128);
	protected Color m_bkNonSelectionColor = Color.white;
	protected Color m_borderSelectionColor = Color.yellow;

	protected Color  m_textColor;
	protected Color  m_bkColor;

	protected boolean m_hasFocus;
	protected Border[] m_borders;

	public IconComboRenderer() {
		super();
		m_textColor = m_textNonSelectionColor;
		m_bkColor = m_bkNonSelectionColor;
		m_borders = new Border[20];
		for (int k=0; k<m_borders.length; k++)
			m_borders[k] = new EmptyBorder(0, OFFSET*k, 0, 0);
		setOpaque(false);
	}

	public Component getListCellRendererComponent(JList list,
		Object obj, int row, boolean sel,
		boolean hasFocus) {
	
		if (obj == null)
			return this;
		setText(obj.toString());
		boolean selectable = true;
		if (obj instanceof ListData) {
			ListData ldata = (ListData)obj;
			selectable = ldata.isSelectable();
			setIcon(ldata.getIcon());
			int index = 0;
			if (row >= 0)	// no offset for text field (row=-1)
				index = ldata.getIndex();
			Border b = (index<m_borders.length ? m_borders[index] : 
				new EmptyBorder(0, OFFSET*index, 0, 0));
			setBorder(b);
		}
		else
			setIcon(null);

		setFont(list.getFont());
		m_textColor = (sel ? m_textSelectionColor : 
			(selectable ? m_textNonSelectionColor : 
			m_textNonselectableColor));
		m_bkColor = (sel ? m_bkSelectionColor : 
			m_bkNonSelectionColor);
		m_hasFocus = hasFocus;
		return this;
	}
    
	public void paint(Graphics g) {
		Icon icon = getIcon();
		Border b = getBorder();

		g.setColor(m_bkNonSelectionColor);
		g.fillRect(0, 0, getWidth(), getHeight());

		g.setColor(m_bkColor);
		int offset = 0;
		if(icon != null && getText() != null) {
			Insets ins = getInsets();
			offset = ins.left + icon.getIconWidth() + getIconTextGap();
		}
		g.fillRect(offset, 0, getWidth() - 1 - offset,
			getHeight() - 1);
		
		if (m_hasFocus) {
			g.setColor(m_borderSelectionColor);
			g.drawRect(offset, 0, getWidth()-1-offset, getHeight()-1);
		}

		setForeground(m_textColor);
		setBackground(m_bkColor);
		super.paint(g);
    }
}
