/** 
 *  Copyright 1999-2002 Matthew Robinson and Pavel Vorobiev. 
 *  All Rights Reserved. 
 * 
 *  =================================================== 
 *  This program contains code from the book "Swing" 
 *  2nd Edition by Matthew Robinson and Pavel Vorobiev 
 *  http://www.spindoczine.com/sbe 
 *  =================================================== 
 * 
 *  The above paragraph must be included in full, unmodified 
 *  and completely intact in the beginning of any source code 
 *  file that references, copies or uses (in any way, shape 
 *  or form) code contained in this file. 
 */

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;
import java.util.*;
import java.sql.*;	// NEW

import javax.swing.*;
import javax.swing.text.*;
import javax.swing.event.*;
import javax.swing.border.*;
import javax.swing.text.html.*;
import javax.swing.undo.*;

import dl.*;

/*
This example adds:
- spell checking dialog.
*/
public class HtmlProcessor extends JFrame {

	public static final String APP_NAME = "HTML Word Processor";

	protected JTextPane m_editor;
	protected StyleSheet m_context;
	protected MutableHTMLDocument m_doc;
	protected CustomHTMLEditorKit m_kit;
	protected SimpleFilter m_htmlFilter;
	protected JToolBar m_toolBar;

	protected JFileChooser m_chooser;
	protected File	m_currentFile;

	protected boolean m_textChanged = false;

	protected JComboBox m_cbFonts;
	protected JComboBox m_cbSizes;
	protected SmallToggleButton m_bBold;
	protected SmallToggleButton m_bItalic;

	protected String m_fontName = "";
	protected int m_fontSize = 0;
	protected boolean m_skipUpdate;

	protected int m_xStart = -1;
	protected int m_xFinish = -1;

	protected ColorMenu m_foreground;

	protected JComboBox m_cbStyles;
	public static HTML.Tag[] STYLES = {
		HTML.Tag.P, HTML.Tag.BLOCKQUOTE, HTML.Tag.CENTER, HTML.Tag.CITE, HTML.Tag.CODE,
		HTML.Tag.H1, HTML.Tag.H2, HTML.Tag.H3, HTML.Tag.H4, HTML.Tag.H5,
		HTML.Tag.H6, HTML.Tag.PRE };

	protected UndoManager m_undo = new UndoManager();
	protected Action m_undoAction;
	protected Action m_redoAction;

	protected String[] m_fontNames;
	protected String[] m_fontSizes;

	protected FindDialog m_findDialog;

	public HtmlProcessor() {
		super(APP_NAME);
		setSize(650, 400);

		m_editor = new JTextPane();
		m_kit = new CustomHTMLEditorKit();
		m_editor.setEditorKit(m_kit);

		JScrollPane ps = new JScrollPane(m_editor);
		getContentPane().add(ps, BorderLayout.CENTER);

		JMenuBar menuBar = createMenuBar();
		setJMenuBar(menuBar);

		m_chooser = new JFileChooser();
		m_htmlFilter = new SimpleFilter("html", "HTML Documents");
		m_chooser.setFileFilter(m_htmlFilter);
		try {
			File dir = (new File(".")).getCanonicalFile();
			m_chooser.setCurrentDirectory(dir);
		} catch (IOException ex) {}

		CaretListener lst = new CaretListener() {
			public void caretUpdate(CaretEvent e) {
				showAttributes(e.getDot());
			}
		};
		m_editor.addCaretListener(lst);

		FocusListener flst = new FocusListener() {
			public void focusGained(FocusEvent e) {
				int len = m_editor.getDocument().getLength();
				if (m_xStart>=0 && m_xFinish>=0 && m_xStart<len && m_xFinish<len)
					if (m_editor.getCaretPosition()==m_xStart) {
						m_editor.setCaretPosition(m_xFinish);
						m_editor.moveCaretPosition(m_xStart);
					}
					else
						m_editor.select(m_xStart, m_xFinish);
			}

			public void focusLost(FocusEvent e) {
				m_xStart = m_editor.getSelectionStart();
				m_xFinish = m_editor.getSelectionEnd();
			}
		};
		m_editor.addFocusListener(flst);

		newDocument();

		WindowListener wndCloser = new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				if (!promptToSave())
					return;
				System.exit(0);
			}
			public void windowActivated(WindowEvent e) {
				m_editor.requestFocus();
			}
		};
		addWindowListener(wndCloser);
	}

	protected JMenuBar createMenuBar() {
		JMenuBar menuBar = new JMenuBar();

		JMenu mFile = new JMenu("File");
		mFile.setMnemonic('f');

		ImageIcon iconNew = new ImageIcon("New16.gif");
		Action actionNew = new AbstractAction("New", iconNew) {
			public void actionPerformed(ActionEvent e) {
				if (!promptToSave())
					return;
				newDocument();
			}
		};
		JMenuItem item = new JMenuItem(actionNew);
		item.setMnemonic('n');
		item.setAccelerator(KeyStroke.getKeyStroke(
			KeyEvent.VK_N, InputEvent.CTRL_MASK));
		mFile.add(item);

		ImageIcon iconOpen = new ImageIcon("Open16.gif");
		Action actionOpen = new AbstractAction("Open...", iconOpen) {
			public void actionPerformed(ActionEvent e) {
				if (!promptToSave())
					return;
				openDocument();
			}
		};
		item = new JMenuItem(actionOpen);
		item.setMnemonic('o');
		item.setAccelerator(KeyStroke.getKeyStroke(
			KeyEvent.VK_O, InputEvent.CTRL_MASK));
		mFile.add(item);

		ImageIcon iconSave = new ImageIcon("Save16.gif");
		Action actionSave = new AbstractAction("Save", iconSave) {
			public void actionPerformed(ActionEvent e) {
				saveFile(false);
			}
		};
		item = new JMenuItem(actionSave);
		item.setMnemonic('s');
		item.setAccelerator(KeyStroke.getKeyStroke(
			KeyEvent.VK_S, InputEvent.CTRL_MASK));
		mFile.add(item);

		ImageIcon iconSaveAs = new ImageIcon("SaveAs16.gif");
		Action actionSaveAs = new AbstractAction("Save As...", iconSaveAs) {
			public void actionPerformed(ActionEvent e) {
				saveFile(true);
			}
		};
		item = new JMenuItem(actionSaveAs);
		item.setMnemonic('a');
		mFile.add(item);

		mFile.addSeparator();

		Action actionExit = new AbstractAction("Exit") {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		};

		item =	mFile.add(actionExit);
		item.setMnemonic('x');
		menuBar.add(mFile);

		m_toolBar = new JToolBar();
		JButton bNew = new SmallButton(actionNew, "New document");
		m_toolBar.add(bNew);

		JButton bOpen = new SmallButton(actionOpen, "Open HTML document");
		m_toolBar.add(bOpen);

		JButton bSave = new SmallButton(actionSave, "Save HTML document");
		m_toolBar.add(bSave);
		m_toolBar.add(bSave);

		JMenu mEdit = new JMenu("Edit");
		mEdit.setMnemonic('e');

		Action action = new AbstractAction("Copy",
		 new ImageIcon("Copy16.gif"))
		{
			public void actionPerformed(ActionEvent e) {
				m_editor.copy();
			}
		};
		item = mEdit.add(action);
		item.setMnemonic('c');
		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C,
			KeyEvent.CTRL_MASK));

		action = new AbstractAction("Cut",
		 new ImageIcon("Cut16.gif"))
		{
			public void actionPerformed(ActionEvent e) {
				m_editor.cut();
			}
		};
		item = mEdit.add(action);
		item.setMnemonic('t');
		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X,
			KeyEvent.CTRL_MASK));

		action = new AbstractAction("Paste",
		 new ImageIcon("Paste16.gif"))
		{
			public void actionPerformed(ActionEvent e) {
				m_editor.paste();	// Doesn't work in JDK 1.4
			}
		};
		item = mEdit.add(action);
		item.setMnemonic('p');
		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V,
			KeyEvent.CTRL_MASK));

		mEdit.addSeparator();

		m_undoAction = new AbstractAction("Undo",
		 new ImageIcon("Undo16.gif"))
		{
			public void actionPerformed(ActionEvent e) {
				try {
					m_undo.undo();
				}
				catch (CannotUndoException ex) {
					System.err.println("Unable to undo: " + ex);
				}
				updateUndo();
			}
		};
		item = mEdit.add(m_undoAction);
		item.setMnemonic('u');
		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z,
			KeyEvent.CTRL_MASK));

		m_redoAction = new AbstractAction("Redo",
		 new ImageIcon("Redo16.gif"))
		{
			public void actionPerformed(ActionEvent e) {
				try {
					m_undo.redo();
				}
				catch (CannotRedoException ex) {
					System.err.println("Unable to redo: " + ex);
				}
				updateUndo();
			}
		};
		item =	mEdit.add(m_redoAction);
		item.setMnemonic('r');
		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Y,
			KeyEvent.CTRL_MASK));

		Action findAction = new AbstractAction("Find...",
		 new ImageIcon("Find16.gif"))
		{
			public void actionPerformed(ActionEvent e) {
				if (m_findDialog==null)
					m_findDialog = new FindDialog(HtmlProcessor.this, 0);
				else
					m_findDialog.setSelectedIndex(0);
				m_findDialog.show();
			}
		};
		item = mEdit.add(findAction);
		item.setMnemonic('f');
		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F,
			KeyEvent.CTRL_MASK));

		Action replaceAction = new AbstractAction("Replace...",
		 new ImageIcon("Replace16.gif")) {
			public void actionPerformed(ActionEvent e) {
				if (m_findDialog==null)
					m_findDialog = new FindDialog(HtmlProcessor.this, 1);
				else
					m_findDialog.setSelectedIndex(1);
				m_findDialog.show();
			}
		};
		item =	mEdit.add(replaceAction);
		item.setMnemonic('l');
		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_H,
			KeyEvent.CTRL_MASK));

		menuBar.add(mEdit);

		GraphicsEnvironment ge = GraphicsEnvironment.
			getLocalGraphicsEnvironment();
		m_fontNames = ge.getAvailableFontFamilyNames();

		m_toolBar.addSeparator();
		m_cbFonts = new JComboBox(m_fontNames);
		m_cbFonts.setMaximumSize(new Dimension(200, 23));
		m_cbFonts.setEditable(true);

		ActionListener lst = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				m_fontName = m_cbFonts.getSelectedItem().toString();
				MutableAttributeSet attr = new SimpleAttributeSet();
				StyleConstants.setFontFamily(attr, m_fontName);
				setAttributeSet(attr);
				m_editor.grabFocus();
			}
		};
		m_cbFonts.addActionListener(lst);
		m_toolBar.add(m_cbFonts);

		m_toolBar.addSeparator();
		m_fontSizes = new String[] {"8", "9", "10",
			"11", "12", "14", "16", "18", "20", "22", "24", "26",
			"28", "36", "48", "72"};
		m_cbSizes = new JComboBox(m_fontSizes);
		m_cbSizes.setMaximumSize(new Dimension(50, 23));
		m_cbSizes.setEditable(true);

		lst = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int fontSize = 0;
				try {
					fontSize = Integer.parseInt(m_cbSizes.
						getSelectedItem().toString());
				}
				catch (NumberFormatException ex) { return; }

				m_fontSize = fontSize;
				MutableAttributeSet attr = new SimpleAttributeSet();
				StyleConstants.setFontSize(attr, fontSize);
				setAttributeSet(attr);
				m_editor.grabFocus();
			}
		};
		m_cbSizes.addActionListener(lst);
		m_toolBar.add(m_cbSizes);

		m_toolBar.addSeparator();
		ImageIcon img1 = new ImageIcon("Bold16.gif");
		m_bBold = new SmallToggleButton(false, img1, img1,
			"Bold font");
		lst = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MutableAttributeSet attr = new SimpleAttributeSet();
				StyleConstants.setBold(attr, m_bBold.isSelected());
				setAttributeSet(attr);
				m_editor.grabFocus();
			}
		};
		m_bBold.addActionListener(lst);
		m_toolBar.add(m_bBold);

		img1 = new ImageIcon("Italic16.gif");
		m_bItalic = new SmallToggleButton(false, img1, img1,
			"Italic font");
		lst = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MutableAttributeSet attr = new SimpleAttributeSet();
				StyleConstants.setItalic(attr, m_bItalic.isSelected());
				setAttributeSet(attr);
				m_editor.grabFocus();
			}
		};
		m_bItalic.addActionListener(lst);
		m_toolBar.add(m_bItalic);

		JMenu mInsert = new JMenu("Insert");
		mInsert.setMnemonic('i');

		item = new JMenuItem("Image...");
		item.setMnemonic('i');
		lst = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String url = inputURL("Please enter image URL:", null);
				if (url == null)
					return;
				try {
					ImageIcon icon = new ImageIcon(new URL(url));
					int w = icon.getIconWidth();
					int h = icon.getIconHeight();
					if (w<=0 || h<=0) {
						JOptionPane.showMessageDialog(HtmlProcessor.this,
							"Error reading image URL\n"+
							url, APP_NAME,
							JOptionPane.WARNING_MESSAGE);
							return;
					}
					MutableAttributeSet attr = new SimpleAttributeSet();
					attr.addAttribute(StyleConstants.NameAttribute, HTML.Tag.IMG);
					attr.addAttribute(HTML.Attribute.SRC, url);
					attr.addAttribute(HTML.Attribute.HEIGHT, Integer.toString(h));
					attr.addAttribute(HTML.Attribute.WIDTH, Integer.toString(w));
					int p = m_editor.getCaretPosition();
					m_doc.insertString(p, " ", attr);
				}
				catch (Exception ex) {
					showError(ex, "Error: "+ex);
				}
			}
		};
		item.addActionListener(lst);
		mInsert.add(item);

		item = new JMenuItem("Hyperlink...");
		item.setMnemonic('h');
		lst = new ActionListener()	{
			public void actionPerformed(ActionEvent e) {
				String oldHref = null;
				// The following code is correct, but may modify the original HTML - very strange...
				int p = m_editor.getCaretPosition();
				AttributeSet attr = m_doc.getCharacterElement(p).
					getAttributes();
				AttributeSet anchor = (AttributeSet)attr.getAttribute(HTML.Tag.A);
				if (anchor != null)
					oldHref = (String)anchor.getAttribute(HTML.Attribute.HREF);

				String newHref = inputURL("Please enter link URL:", oldHref);
				if (newHref == null)
					return;

				SimpleAttributeSet attr2 = new SimpleAttributeSet();
				attr2.addAttribute(StyleConstants.NameAttribute, HTML.Tag.A);
				attr2.addAttribute(HTML.Attribute.HREF, newHref);
				setAttributeSet(attr2, true);
				m_editor.grabFocus();
			}
		};
		item.addActionListener(lst);
		mInsert.add(item);

		item = new JMenuItem("Table...");
		item.setMnemonic('t');
		lst = new ActionListener()	{
			public void actionPerformed(ActionEvent e) {
				TableDlg dlg = new TableDlg(HtmlProcessor.this, m_doc);
				dlg.show();
				if (dlg.succeeded()) {
					String tableHtml = dlg.generateHTML();
					Element ep = m_doc.getParagraphElement(
						m_editor.getSelectionStart());
					try {
						m_doc.insertAfterEnd(ep, tableHtml);
					}
					catch (Exception ex) {
						ex.printStackTrace();
					}
					documentChanged();
				}
			}
		};
		item.addActionListener(lst);
		mInsert.add(item);

		menuBar.add(mInsert);

		JMenu mFormat = new JMenu("Format");
		mFormat.setMnemonic('o');

		m_foreground = new ColorMenu("Foreground Color");
		m_foreground.setColor(m_editor.getForeground());
		m_foreground.setMnemonic('f');
		lst = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MutableAttributeSet attr = new SimpleAttributeSet();
				StyleConstants.setForeground(attr, m_foreground.getColor());
				setAttributeSet(attr);
			}
		};
		m_foreground.addActionListener(lst);
		mFormat.add(m_foreground);

		MenuListener ml = new MenuListener() {
			public void menuSelected(MenuEvent e) {
				int p = m_editor.getCaretPosition();
				AttributeSet attr = m_doc.getCharacterElement(p).
					getAttributes();
				Color c = StyleConstants.getForeground(attr);
				m_foreground.setColor(c);
			}
			public void menuDeselected(MenuEvent e) {}
			public void menuCanceled(MenuEvent e) {}
		};
		m_foreground.addMenuListener(ml);

		item = new JMenuItem("Font...");
		item.setMnemonic('o');
		lst = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FontDialog dlg = new FontDialog(HtmlProcessor.this,
					m_fontNames, m_fontSizes);
				AttributeSet a = m_doc.getCharacterElement(
					m_editor.getCaretPosition()).getAttributes();
				dlg.setAttributes(a);
				dlg.show();
				if (dlg.succeeded()) {
					setAttributeSet(dlg.getAttributes());
					showAttributes(m_editor.getCaretPosition());
				}
			}
		};
		item.addActionListener(lst);
		mFormat.add(item);
		mFormat.addSeparator();

		item = new JMenuItem("Page Properties...");
		item.setMnemonic('p');
		lst = new ActionListener()	{
			public void actionPerformed(ActionEvent e) {
				DocumentPropsDlg dlg = new DocumentPropsDlg(HtmlProcessor.this, m_doc);
				dlg.show();
				if (dlg.succeeded())
					documentChanged();
			}
		};
		item.addActionListener(lst);
		mFormat.add(item);

		menuBar.add(mFormat);

		JMenu mTools = new JMenu("Tools");
		mTools.setMnemonic('t');

		item = new JMenuItem("HTML Source...");
		item.setMnemonic('s');
		lst = new ActionListener()	{
			public void actionPerformed(ActionEvent e) {
				try {
					StringWriter sw = new StringWriter();
					m_kit.write(sw, m_doc, 0, m_doc.getLength());
					sw.close();

					HtmlSourceDlg dlg = new HtmlSourceDlg(
						HtmlProcessor.this, sw.toString());
					dlg.show();
					if (!dlg.succeeded())
						return;

					StringReader sr = new StringReader(dlg.getSource());
					m_doc = (MutableHTMLDocument)m_kit.createDocument();
					m_context = m_doc.getStyleSheet();
					m_kit.read(sr, m_doc, 0);
					sr.close();
					m_editor.setDocument(m_doc);
					documentChanged();
				}
				catch (Exception ex) {
					showError(ex, "Error: "+ex);
				}
			}
		};
		item.addActionListener(lst);
		mTools.add(item);

		// NEW
		Action spellAction = new AbstractAction("Spelling...",
		 new ImageIcon("SpellCheck16.gif"))
		{
			public void actionPerformed(ActionEvent e) {
				SpellChecker checker = new SpellChecker(HtmlProcessor.this);
				HtmlProcessor.this.setCursor(Cursor.getPredefinedCursor(
					Cursor.WAIT_CURSOR));
				checker.start();
			}
		};
		item =	mTools.add(spellAction);
		item.setMnemonic('s');
		item.setAccelerator(KeyStroke.getKeyStroke(
			KeyEvent.VK_F7, 0));

		menuBar.add(mTools);

		m_toolBar.addSeparator();
		m_cbStyles = new JComboBox(STYLES);
		m_cbStyles.setMaximumSize(new Dimension(100, 23));
		m_cbStyles.setRequestFocusEnabled(false);
		m_toolBar.add(m_cbStyles);

		lst = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				HTML.Tag style = (HTML.Tag)m_cbStyles.getSelectedItem();
				if (style == null)
					return;
				MutableAttributeSet attr = new SimpleAttributeSet();
				attr.addAttribute(StyleConstants.NameAttribute, style);
				setAttributeSet(attr, true);
				m_editor.grabFocus();
			}
		};
		m_cbStyles.addActionListener(lst);

		getContentPane().add(m_toolBar, BorderLayout.NORTH);

		return menuBar;
	}

	public Document getDocument() {
		return m_doc;
	}

	public JTextPane getTextPane() {
		return m_editor;
	}

	public void setSelection(int xStart, int xFinish, boolean moveUp) {
		if (moveUp) {
			m_editor.setCaretPosition(xFinish);
			m_editor.moveCaretPosition(xStart);
		}
		else
			m_editor.select(xStart, xFinish);
		m_xStart = m_editor.getSelectionStart();
		m_xFinish = m_editor.getSelectionEnd();
	}

	protected String getDocumentName() {
		String title = m_doc.getTitle();
		if (title != null && title.length() > 0)
			return title;
		return m_currentFile==null ? "Untitled" :
			m_currentFile.getName();
	}

	protected void newDocument() {
		m_doc = (MutableHTMLDocument)m_kit.createDocument();
		m_context = m_doc.getStyleSheet();

		m_editor.setDocument(m_doc);
		m_currentFile = null;
		setTitle(APP_NAME+" ["+getDocumentName()+"]");

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				showAttributes(0);
				m_editor.scrollRectToVisible(new Rectangle(0,0,1,1));
				m_doc.addDocumentListener(new UpdateListener());
				m_doc.addUndoableEditListener(new Undoer());
				m_textChanged = false;
			}
		});
	}

	protected void openDocument() {
		if (m_chooser.showOpenDialog(HtmlProcessor.this) !=
			JFileChooser.APPROVE_OPTION)
			return;
		File f = m_chooser.getSelectedFile();
		if (f == null || !f.isFile())
			return;
		m_currentFile = f;

		HtmlProcessor.this.setCursor(
			Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		try {
			InputStream in = new FileInputStream(m_currentFile);
			m_doc = (MutableHTMLDocument)m_kit.createDocument();
			m_kit.read(in, m_doc, 0);
			m_context = m_doc.getStyleSheet();
			m_editor.setDocument(m_doc);
			in.close();
		}
		catch (Exception ex) {
			showError(ex, "Error reading file "+m_currentFile);
		}
		HtmlProcessor.this.setCursor(Cursor.getPredefinedCursor(
			Cursor.DEFAULT_CURSOR));

		setTitle(APP_NAME+" ["+getDocumentName()+"]");

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				m_editor.setCaretPosition(1);
				showAttributes(1);
				m_editor.scrollRectToVisible(new Rectangle(0,0,1,1));
				m_doc.addDocumentListener(new UpdateListener());
				m_doc.addUndoableEditListener(new Undoer());
				m_textChanged = false;
			}
		});
	}

	protected boolean saveFile(boolean saveAs) {
		if (!saveAs && !m_textChanged)
			return true;
		if (saveAs || m_currentFile == null) {
			if (m_chooser.showSaveDialog(HtmlProcessor.this) !=
				JFileChooser.APPROVE_OPTION)
				return false;
			File f = m_chooser.getSelectedFile();
			if (f == null)
				return false;
			m_currentFile = f;
			setTitle(APP_NAME+" ["+getDocumentName()+"]");
		}

		HtmlProcessor.this.setCursor(
			Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		try {
			OutputStream out = new FileOutputStream(m_currentFile);
			m_kit.write(out, m_doc, 0, m_doc.getLength());
			out.close();
			m_textChanged = false;
		}
		catch (Exception ex) {
			showError(ex, "Error saving file "+m_currentFile);
		}
		HtmlProcessor.this.setCursor(Cursor.getPredefinedCursor(
			Cursor.DEFAULT_CURSOR));
		return true;
	}

	protected boolean promptToSave() {
		if (!m_textChanged)
			return true;
		int result = JOptionPane.showConfirmDialog(this,
			"Save changes to "+getDocumentName()+"?",
			APP_NAME, JOptionPane.YES_NO_CANCEL_OPTION,
			JOptionPane.INFORMATION_MESSAGE);
		switch (result) {
		case JOptionPane.YES_OPTION:
			if (!saveFile(false))
				return false;
			return true;
		case JOptionPane.NO_OPTION:
			return true;
		case JOptionPane.CANCEL_OPTION:
			return false;
		}
		return true;
	}

	public void showError(Exception ex, String message) {
		ex.printStackTrace();
		JOptionPane.showMessageDialog(this,
			message, APP_NAME,
			JOptionPane.WARNING_MESSAGE);
	}

	protected void showAttributes(int p) {
		m_skipUpdate = true;
		AttributeSet attr = m_doc.getCharacterElement(p).
			getAttributes();

		String name = StyleConstants.getFontFamily(attr);
		if (!m_fontName.equals(name)) {
			m_fontName = name;
			m_cbFonts.setSelectedItem(name);
		}
		int size = StyleConstants.getFontSize(attr);
		if (m_fontSize != size) {
			m_fontSize = size;
			m_cbSizes.setSelectedItem(Integer.toString(m_fontSize));
		}
		boolean bold = StyleConstants.isBold(attr);
		if (bold != m_bBold.isSelected())
			m_bBold.setSelected(bold);
		boolean italic = StyleConstants.isItalic(attr);
		if (italic != m_bItalic.isSelected())
			m_bItalic.setSelected(italic);

		Element ep = m_doc.getParagraphElement(p);
		HTML.Tag attrName = (HTML.Tag)ep.getAttributes().
			getAttribute(StyleConstants.NameAttribute);

		int index = -1;
		if (attrName != null) {
			for (int k=0; k<STYLES.length; k++) {
				if (STYLES[k].equals(attrName)) {
					index = k;
					break;
				}
			}
		}
		m_cbStyles.setSelectedIndex(index);

		m_skipUpdate = false;
	}

	protected void setAttributeSet(AttributeSet attr) {
		setAttributeSet(attr, false);
	}

	protected void setAttributeSet(AttributeSet attr,
		boolean setParagraphAttributes) {
		if (m_skipUpdate)
			return;
		int xStart = m_editor.getSelectionStart();
		int xFinish = m_editor.getSelectionEnd();
		if (!m_editor.hasFocus()) {
			xStart = m_xStart;
			xFinish = m_xFinish;
		}

		if (setParagraphAttributes)
			m_doc.setParagraphAttributes(xStart,
				xFinish - xStart, attr, false);
		else if (xStart != xFinish)
			m_doc.setCharacterAttributes(xStart,
				xFinish - xStart, attr, false);
		else {
			MutableAttributeSet inputAttributes =
				m_kit.getInputAttributes();
			inputAttributes.addAttributes(attr);
		}
	}

	protected String inputURL(String prompt, String initialValue) {
		JPanel p = new JPanel();
		p.setLayout(new BoxLayout(p, BoxLayout.X_AXIS));
		p.add(new JLabel(prompt));
		p.add(Box.createHorizontalGlue());
		JButton bt = new JButton("Local File");
		bt.setRequestFocusEnabled(false);
		p.add(bt);

		final JOptionPane op = new JOptionPane(p,
			JOptionPane.PLAIN_MESSAGE, JOptionPane.OK_CANCEL_OPTION);
		op.setWantsInput(true);
		if (initialValue != null)
			op.setInitialSelectionValue(initialValue);

		ActionListener lst = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				if (chooser.showOpenDialog(HtmlProcessor.this) !=
					JFileChooser.APPROVE_OPTION)
					return;
				File f = chooser.getSelectedFile();
				try {
					String str = f.toURL().toString();
					op.setInitialSelectionValue(str);
				}
				catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		};
		bt.addActionListener(lst);

		JDialog dlg = op.createDialog(this, APP_NAME);
		dlg.show();
		dlg.dispose();

		Object value = op.getInputValue();	// Changed - Pavel
		if(value == JOptionPane.UNINITIALIZED_VALUE)
			return null;
		String str = (String)value;
		if (str != null && str.length() == 0)
			str = null;
		return str;
	}

	public void documentChanged() {
		m_editor.setDocument(new HTMLDocument());
		m_editor.setDocument(m_doc);
		m_editor.revalidate();
		m_editor.repaint();
		setTitle(APP_NAME+" ["+getDocumentName()+"]");
		m_textChanged = true;
	}

	protected void updateUndo() {
		if(m_undo.canUndo()) {
			m_undoAction.setEnabled(true);
			m_undoAction.putValue(Action.NAME,
			m_undo.getUndoPresentationName());
		}
		else {
			m_undoAction.setEnabled(false);
			m_undoAction.putValue(Action.NAME, "Undo");
		}
		if(m_undo.canRedo()) {
			m_redoAction.setEnabled(true);
			m_redoAction.putValue(Action.NAME,
			m_undo.getRedoPresentationName());
		}
		else {
			m_redoAction.setEnabled(false);
			m_redoAction.putValue(Action.NAME, "Redo");
		}
	}

	public static void main(String argv[]) {
		HtmlProcessor frame = new HtmlProcessor();
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.setVisible(true);
	}

	class UpdateListener implements DocumentListener {

		public void insertUpdate(DocumentEvent e) {
			m_textChanged = true;
		}

		public void removeUpdate(DocumentEvent e) {
			m_textChanged = true;
		}

		public void changedUpdate(DocumentEvent e) {
			m_textChanged = true;
		}
	}

	class Undoer implements UndoableEditListener {

		public Undoer() {
			m_undo.die();
			updateUndo();
		}

		public void undoableEditHappened(UndoableEditEvent e) {
			UndoableEdit edit = e.getEdit();
			m_undo.addEdit(e.getEdit());
			updateUndo();
		}
	}
}

// Class SmallButton unchanged from chapter 12

class SmallButton extends JButton implements MouseListener {
	protected Border m_raised = new SoftBevelBorder(BevelBorder.RAISED);
	protected Border m_lowered = new SoftBevelBorder(BevelBorder.LOWERED);
	protected Border m_inactive = new EmptyBorder(3, 3, 3, 3);
	protected Border m_border = m_inactive;
	protected Insets m_ins = new Insets(4,4,4,4);

	public SmallButton(Action act, String tip) {
		super((Icon)act.getValue(Action.SMALL_ICON));
		setBorder(m_inactive);
		setMargin(m_ins);
		setToolTipText(tip);
		setRequestFocusEnabled(false);
		addActionListener(act);
		addMouseListener(this);
	}

	public float getAlignmentY() {
		return 0.5f;
	}

	// Overridden for 1.4 bug fix
	public Border getBorder() {
		return m_border;
	}

	// Overridden for 1.4 bug fix
	public Insets getInsets() {
		return m_ins;
	}

	public void mousePressed(MouseEvent e) {
		m_border = m_lowered;
		setBorder(m_lowered);
	}

	public void mouseReleased(MouseEvent e) {
		m_border = m_inactive;
		setBorder(m_inactive);
	}

	public void mouseClicked(MouseEvent e) {}

	public void mouseEntered(MouseEvent e) {
		m_border = m_raised;
		setBorder(m_raised);
	}

	public void mouseExited(MouseEvent e) {
		m_border = m_inactive;
		setBorder(m_inactive);
	}
}

// Class SmallToggleButton unchanged from chapter 12

class SmallToggleButton extends JToggleButton
	implements ItemListener {

	protected Border m_raised = new SoftBevelBorder(BevelBorder.RAISED);
	protected Border m_lowered = new SoftBevelBorder(BevelBorder.LOWERED);
	protected Insets m_ins = new Insets(4,4,4,4);

	public SmallToggleButton(boolean selected,
		ImageIcon imgUnselected, ImageIcon imgSelected, String tip) {
		super(imgUnselected, selected);
		setHorizontalAlignment(CENTER);
		setBorder(selected ? m_lowered : m_raised);
		setMargin(m_ins);
		setToolTipText(tip);
		setRequestFocusEnabled(false);
		setSelectedIcon(imgSelected);
		addItemListener(this);
	}

	public float getAlignmentY() {
		return 0.5f;
	}

	// Overridden for 1.4 bug fix
	public Insets getInsets() {
		return m_ins;
	}

	public Border getBorder() {
		return (isSelected() ? m_lowered : m_raised);
	}

	public void itemStateChanged(ItemEvent e) {
		setBorder(isSelected() ? m_lowered : m_raised);
	}
}

// Class SimpleFilter unchanged from chapter 14

class SimpleFilter extends javax.swing.filechooser.FileFilter
{
	private String m_description = null;
	private String m_extension = null;

	public SimpleFilter(String extension, String description) {
		m_description = description;
		m_extension = "."+extension.toLowerCase();
	}

	public String getDescription() {
		return m_description;
	}

	public boolean accept(File f) {
		if (f == null)
			return false;
		if (f.isDirectory())
			return true;
		return f.getName().toLowerCase().endsWith(m_extension);
	}
}

// Class ColorMenu unchanged from chapter 12

class ColorMenu extends JMenu {

	protected Border m_unselectedBorder;
	protected Border m_selectedBorder;
	protected Border m_activeBorder;

	protected Hashtable m_panes;
	protected ColorPane m_selected;

	public ColorMenu(String name) {
		super(name);
		m_unselectedBorder = new CompoundBorder(
			new MatteBorder(1, 1, 1, 1, getBackground()),
			new BevelBorder(BevelBorder.LOWERED,
			Color.white, Color.gray));
		m_selectedBorder = new CompoundBorder(
			new MatteBorder(2, 2, 2, 2, Color.red),
			new MatteBorder(1, 1, 1, 1, getBackground()));
		m_activeBorder = new CompoundBorder(
			new MatteBorder(2, 2, 2, 2, Color.blue),
			new MatteBorder(1, 1, 1, 1, getBackground()));

		JPanel p = new JPanel();
		p.setBorder(new EmptyBorder(5, 5, 5, 5));
		p.setLayout(new GridLayout(8, 8));
		m_panes = new Hashtable();

		int[] values = new int[] { 0, 128, 192, 255 };
		for (int r=0; r<values.length; r++) {
			for (int g=0; g<values.length; g++) {
				for (int b=0; b<values.length; b++) {
					Color c = new Color(values[r], values[g], values[b]);
					ColorPane pn = new ColorPane(c);
					p.add(pn);
					m_panes.put(c, pn);
				}
			}
		}
		add(p);
	}

	public void setColor(Color c) {
		Object obj = m_panes.get(c);
		if (obj == null)
			return;
		if (m_selected != null)
			m_selected.setSelected(false);
		m_selected = (ColorPane)obj;
		m_selected.setSelected(true);
	}

	public Color getColor() {
		if (m_selected == null)
			return null;
		return m_selected.getColor();
	}

	public void doSelection() {
		fireActionPerformed(new ActionEvent(this,
			ActionEvent.ACTION_PERFORMED, getActionCommand()));
	}

	class ColorPane extends JPanel implements MouseListener {
		protected Color m_c;
		protected boolean m_selected;

		public ColorPane(Color c) {
			m_c = c;
			setBackground(c);
			setBorder(m_unselectedBorder);
			String msg = "R "+c.getRed()+", G "+c.getGreen()+
				", B "+c.getBlue();
			setToolTipText(msg);
			addMouseListener(this);
		}

		public Color getColor() {
			return m_c;
		}

		public Dimension getPreferredSize() {
			return new Dimension(15, 15);
		}

		public Dimension getMaximumSize() {
			return getPreferredSize();
		}

		public Dimension getMinimumSize() {
			return getPreferredSize();
		}

		public void setSelected(boolean selected) {
			m_selected = selected;
			if (m_selected)
				setBorder(m_selectedBorder);
			else
				setBorder(m_unselectedBorder);
		}

		public boolean isSelected() {
			return m_selected;
		}

		public void mousePressed(MouseEvent e) {}

		public void mouseClicked(MouseEvent e) {}

		public void mouseReleased(MouseEvent e) {
			setColor(m_c);
			MenuSelectionManager.defaultManager().clearSelectedPath();
			doSelection();
		}

		public void mouseEntered(MouseEvent e) {
			setBorder(m_activeBorder);
		}

		public void mouseExited(MouseEvent e) {
			setBorder(m_selected ? m_selectedBorder :
				m_unselectedBorder);
		}
	}
}

class Utils
{
	public static String colorToHex(Color color) {
		String colorstr = new String("#");

		// Red
		String str = Integer.toHexString(color.getRed());
		if (str.length() > 2)
			str = str.substring(0, 2);
		else if (str.length() < 2)
			colorstr += "0" + str;
		else
			colorstr += str;

		// Green
		str = Integer.toHexString(color.getGreen());
		if (str.length() > 2)
			str = str.substring(0, 2);
		else if (str.length() < 2)
			colorstr += "0" + str;
		else
			colorstr += str;

		// Blue
		str = Integer.toHexString(color.getBlue());
		if (str.length() > 2)
			str = str.substring(0, 2);
		else if (str.length() < 2)
			colorstr += "0" + str;
		else
			colorstr += str;

		return colorstr;
	}

	public static final char[] WORD_SEPARATORS = {' ', '\t', '\n',
		'\r', '\f', '.', ',', ':', '-', '(', ')', '[', ']', '{',
		'}', '<', '>', '/', '|', '\\', '\'', '\"'};

	public static boolean isSeparator(char ch) {
		for (int k=0; k<WORD_SEPARATORS.length; k++)
			if (ch == WORD_SEPARATORS[k])
				return true;
		return false;
	}

	// NEW
	public static String soundex(String word) {
		char[] result = new char[4];
		result[0] = word.charAt(0);
		result[1] = result[2] = result[3] = '0';
		int index = 1;

		char codeLast = '*';
		for (int k=1; k<word.length(); k++) {
			char ch = word.charAt(k);
			char code = ' ';
			switch (ch) {
				case 'b': case 'f': case 'p': case 'v':
					code = '1';
					break;
				case 'c': case 'g': case 'j': case 'k':
				case 'q': case 's': case 'x': case 'z':
					code = '2';
					break;
				case 'd': case 't':
					code = '3';
					break;
				case 'l':
					code = '4';
					break;
				case 'm': case 'n':
					code = '5';
					break;
				case 'r':
					code = '6';
					break;
				default:
					code = '*';
					break;
			}
			if (code == codeLast)
				code = '*';
			codeLast = code;
			if (code != '*') {
				result[index] = code;
				index++;
				if (index > 3)
					break;
			}
		}
		return new String(result);
	}

	public static boolean hasDigits(String word) {
		for (int k=1; k<word.length(); k++) {
			char ch = word.charAt(k);
			if (Character.isDigit(ch))
				return true;
		}
		return false;
	}

	public static String titleCase(String source) {
		return Character.toUpperCase(source.charAt(0)) +
			source.substring(1);
	}
}

class CustomHTMLEditorKit extends HTMLEditorKit {

	public Document createDocument() {
		StyleSheet styles = getStyleSheet();
		StyleSheet ss = new StyleSheet();

		ss.addStyleSheet(styles);

		MutableHTMLDocument doc = new MutableHTMLDocument(ss);
		doc.setParser(getParser());
		doc.setAsynchronousLoadPriority(4);
		doc.setTokenThreshold(100);
		return doc;
	}

}

class MutableHTMLDocument extends HTMLDocument {

	public MutableHTMLDocument(StyleSheet styles) {
		super(styles);
	}

	public Element getElementByTag(HTML.Tag tag) {
		Element root = getDefaultRootElement();
		return getElementByTag(root, tag);
	}

	public Element getElementByTag(Element parent, HTML.Tag tag) {
		if (parent == null || tag == null)
			return null;
		for (int k=0; k<parent.getElementCount(); k++) {
			Element child = parent.getElement(k);
			if (child.getAttributes().getAttribute(
					StyleConstants.NameAttribute).equals(tag))
				return child;
			Element e = getElementByTag(child, tag);
			if (e != null)
				return e;
		}
		return null;
	}

	public String getTitle() {
		return (String)getProperty(Document.TitleProperty);
	}

	// This will work only if <title> element was
	// previously created. Looks like a bug in HTML package.
	public void setTitle(String title) {
		Dictionary di = getDocumentProperties();
		di.put(Document.TitleProperty, title);
		setDocumentProperties(di);
	}

	public void addAttributes(Element e, AttributeSet attributes) {
		if (e == null || attributes == null)
			return;
		try {
			writeLock();
			MutableAttributeSet mattr =
				(MutableAttributeSet)e.getAttributes();
			mattr.addAttributes(attributes);
			fireChangedUpdate(new DefaultDocumentEvent(0, getLength(),
				DocumentEvent.EventType.CHANGE));
		}
		finally {
			writeUnlock();
		}
	}
}

class DocumentPropsDlg extends JDialog {
	protected boolean m_succeeded = false;
	protected MutableHTMLDocument m_doc;

	protected Color m_backgroundColor;
	protected Color m_textColor;
	protected Color m_linkColor;
	protected Color m_viewedColor;

	protected JTextField m_titleTxt;
	protected JTextPane m_previewPane;

	public DocumentPropsDlg(JFrame parent, MutableHTMLDocument doc) {
		super(parent, "Page Properties", true);
		m_doc = doc;

		Element body = m_doc.getElementByTag(HTML.Tag.BODY);
		if (body != null) {
			AttributeSet attr = body.getAttributes();
			StyleSheet syleSheet = m_doc.getStyleSheet();
			Object obj = attr.getAttribute(HTML.Attribute.BGCOLOR);
			if (obj != null)
				m_backgroundColor = syleSheet.stringToColor((String)obj);
			obj = attr.getAttribute(HTML.Attribute.TEXT);
			if (obj != null)
				m_textColor = syleSheet.stringToColor((String)obj);
			obj = attr.getAttribute(HTML.Attribute.LINK);
			if (obj != null)
				m_linkColor = syleSheet.stringToColor((String)obj);
			obj = attr.getAttribute(HTML.Attribute.VLINK);
			if (obj != null)
				m_viewedColor = syleSheet.stringToColor((String)obj);
		}

		ActionListener lst;
		JButton bt;

		JPanel pp = new JPanel(new DialogLayout2());
		pp.setBorder(new EmptyBorder(10, 10, 5, 10));

		pp.add(new JLabel("Page title:"));
		m_titleTxt = new JTextField(m_doc.getTitle(), 24);
		pp.add(m_titleTxt);

		JPanel pa = new JPanel(new BorderLayout(5, 5));
		Border ba = new TitledBorder(new EtchedBorder(
			EtchedBorder.RAISED), "Appearance");
		pa.setBorder(new CompoundBorder(ba, new EmptyBorder(0, 5, 5, 5)));

		JPanel pb = new JPanel(new GridLayout(4, 1, 5, 5));
		bt = new JButton("Background");
		bt.setMnemonic('b');
		lst = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				m_backgroundColor = JColorChooser.showDialog(DocumentPropsDlg.this,
					"Document Background", m_backgroundColor);
				showColors();
			}
		};
		bt.addActionListener(lst);
		pb.add(bt);

		bt = new JButton("Text");
		bt.setMnemonic('t');
		lst = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				m_textColor = JColorChooser.showDialog(DocumentPropsDlg.this,
					"Text Color", m_textColor);
				showColors();
			}
		};
		bt.addActionListener(lst);
		pb.add(bt);

		bt = new JButton("Link");
		bt.setMnemonic('l');
		lst = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				m_linkColor = JColorChooser.showDialog(DocumentPropsDlg.this,
					"Links Color", m_linkColor);
				showColors();
			}
		};
		bt.addActionListener(lst);
		pb.add(bt);

		bt = new JButton("Viewed");
		bt.setMnemonic('v');
		lst = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				m_viewedColor = JColorChooser.showDialog(DocumentPropsDlg.this,
					"Viewed Links Color", m_viewedColor);
				showColors();
			}
		};
		bt.addActionListener(lst);
		pb.add(bt);
		pa.add(pb, BorderLayout.WEST);

		m_previewPane = new JTextPane();
		m_previewPane.setBackground(Color.white);
		m_previewPane.setEditable(false);
		m_previewPane.setBorder(new CompoundBorder(
			new BevelBorder(BevelBorder.LOWERED),
			new EmptyBorder(10, 10, 10, 10)));
		showColors();
		pa.add(m_previewPane, BorderLayout.CENTER);

		pp.add(pa);

		bt = new JButton("Save");
		lst = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				saveData();
				dispose();
			}
		};
		bt.addActionListener(lst);
		pp.add(bt);

		bt = new JButton("Cancel");
		lst = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		};
		bt.addActionListener(lst);
		pp.add(bt);

		getContentPane().add(pp, BorderLayout.CENTER);
		pack();
		setResizable(false);
		setLocationRelativeTo(parent);
	}

	public boolean succeeded() {
		return m_succeeded;
	}

	protected void saveData() {
		m_doc.setTitle(m_titleTxt.getText());

		Element body = m_doc.getElementByTag(HTML.Tag.BODY);
		MutableAttributeSet attr = new SimpleAttributeSet();
		if (m_backgroundColor != null)
			attr.addAttribute(HTML.Attribute.BGCOLOR,
				Utils.colorToHex(m_backgroundColor));
		if (m_textColor != null)
			attr.addAttribute(HTML.Attribute.TEXT,
				Utils.colorToHex(m_textColor));
		if (m_linkColor != null)
			attr.addAttribute(HTML.Attribute.LINK,
				Utils.colorToHex(m_linkColor));
		if (m_viewedColor != null)
			attr.addAttribute(HTML.Attribute.VLINK,
				Utils.colorToHex(m_viewedColor));
		m_doc.addAttributes(body, attr);

		m_succeeded = true;
	}

	protected void showColors() {
		DefaultStyledDocument doc = new DefaultStyledDocument();

		SimpleAttributeSet attr = new SimpleAttributeSet();
		StyleConstants.setFontFamily(attr, "Arial");
		StyleConstants.setFontSize(attr, 14);
		if (m_backgroundColor != null) {
			StyleConstants.setBackground(attr, m_backgroundColor);
			m_previewPane.setBackground(m_backgroundColor);
		}

		try {
			StyleConstants.setForeground(attr, m_textColor!=null ?
				m_textColor : Color.black);
			doc.insertString(doc.getLength(), "Plain text preview\n\n", attr);

			StyleConstants.setForeground(attr, m_linkColor!=null ?
				m_linkColor : Color.blue);
			StyleConstants.setUnderline(attr, true);
			doc.insertString(doc.getLength(), "Link preview\n\n", attr);

			StyleConstants.setForeground(attr, m_viewedColor!=null ?
				m_viewedColor : Color.magenta);
			StyleConstants.setUnderline(attr, true);
			doc.insertString(doc.getLength(), "Viewed link preview\n", attr);
		}
		catch (BadLocationException be) {
			be.printStackTrace();
		}
		m_previewPane.setDocument(doc);
	}

}

class HtmlSourceDlg extends JDialog {
	protected boolean m_succeeded = false;

	protected JTextArea m_sourceTxt;

	public HtmlSourceDlg(JFrame parent, String source) {
		super(parent, "HTML Source", true);

		JPanel pp = new JPanel(new BorderLayout());
		pp.setBorder(new EmptyBorder(10, 10, 5, 10));

		m_sourceTxt = new JTextArea(source, 20, 60);
		m_sourceTxt.setFont(new Font("Courier", Font.PLAIN, 12));
		JScrollPane sp = new JScrollPane(m_sourceTxt);
		pp.add(sp, BorderLayout.CENTER);

		JPanel p = new JPanel(new FlowLayout());
		JPanel p1 = new JPanel(new GridLayout(1, 2, 10, 0));
		JButton bt = new JButton("Save");
		ActionListener lst = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				m_succeeded = true;
				dispose();
			}
		};
		bt.addActionListener(lst);
		p1.add(bt);

		bt = new JButton("Cancel");
		lst = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		};
		bt.addActionListener(lst);
		p1.add(bt);
		p.add(p1);
		pp.add(p, BorderLayout.SOUTH);

		getContentPane().add(pp, BorderLayout.CENTER);
		pack();
		setResizable(true);
		setLocationRelativeTo(parent);
	}

	public boolean succeeded() {
		return m_succeeded;
	}

	public String getSource() {
		return m_sourceTxt.getText();
	}
}

class TableDlg extends JDialog {
	protected boolean m_succeeded = false;
	protected MutableHTMLDocument m_doc;

	protected JSpinner m_rowsSpn;
	protected JSpinner m_colsSpn;
	protected JSpinner m_spacingSpn;
	protected JSpinner m_paddingSpn;

	protected JSpinner m_borderWidthSpn;
	protected JSpinner m_tableWidthSpn;
	protected JSpinner m_tableHeightSpn;
	protected JComboBox m_tableUnitsCb;

	protected JTextPane m_previewPane;

	protected Color m_borderColor;
	protected Color m_backgroundColor;

	protected HTMLEditorKit m_kit = new HTMLEditorKit();

	public TableDlg(JFrame parent, MutableHTMLDocument doc) {
		super(parent, "Insert Table", true);
		m_doc = doc;

		ActionListener lst;
		JButton bt;

		JPanel pp = new JPanel(new DialogLayout2());
		pp.setBorder(new EmptyBorder(10, 10, 5, 10));

		JPanel p1 = new JPanel(new DialogLayout2());
		p1.setBorder(new EmptyBorder(10, 10, 5, 10));

		p1.add(new JLabel("Rows:"));
		m_rowsSpn = new JSpinner(new SpinnerNumberModel(
			new Integer(2), new Integer(0), null, new Integer(1)));
		p1.add(m_rowsSpn);

		p1.add(new JLabel("Columns:"));
		m_colsSpn = new JSpinner(new SpinnerNumberModel(
			new Integer(2), new Integer(0), null, new Integer(1)));
		p1.add(m_colsSpn);

		p1.add(new JLabel("Cell spacing:"));
		m_spacingSpn = new JSpinner(new SpinnerNumberModel(
			new Integer(2), new Integer(0), null, new Integer(1)));
		p1.add(m_spacingSpn);

		p1.add(new JLabel("Cell padding:"));
		m_paddingSpn = new JSpinner(new SpinnerNumberModel(
			new Integer(2), new Integer(0), null, new Integer(1)));
		p1.add(m_paddingSpn);

		JPanel p2 = new JPanel(new DialogLayout2());
		p2.setBorder(new EmptyBorder(10, 10, 5, 10));

		p2.add(new JLabel("Border width:"));
		m_borderWidthSpn = new JSpinner(new SpinnerNumberModel(
			new Integer(2), new Integer(0), null, new Integer(1)));
		p2.add(m_borderWidthSpn);

		p2.add(new JLabel("Table width:"));
		m_tableWidthSpn = new JSpinner(new SpinnerNumberModel(
			new Integer(100), new Integer(0), null, new Integer(1)));
		p2.add(m_tableWidthSpn);

		p2.add(new JLabel("Table height:"));
		m_tableHeightSpn = new JSpinner(new SpinnerNumberModel(
			new Integer(0), new Integer(0), null, new Integer(1)));
		p2.add(m_tableHeightSpn);

		p2.add(new JLabel("Units:"));
		m_tableUnitsCb = new JComboBox(new String[] {"Percent", "Pixels" });
		p2.add(m_tableUnitsCb);

		JPanel p3 = new JPanel(new FlowLayout());
		p3.setBorder(new EmptyBorder(10, 10, 5, 10));
		JPanel pb = new JPanel(new GridLayout(2, 1, 5, 5));
		p3.add(pb);

		bt = new JButton("Border");
		bt.setMnemonic('b');
		lst = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				m_borderColor = JColorChooser.showDialog(TableDlg.this,
					"Border Color", m_borderColor);
			}
		};
		bt.addActionListener(lst);
		pb.add(bt);

		bt = new JButton("Background");		// Changed - Pavel
		bt.setMnemonic('c');
		lst = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				m_backgroundColor = JColorChooser.showDialog(TableDlg.this,
					"Background Color", m_backgroundColor);
			}
		};
		bt.addActionListener(lst);
		pb.add(bt);

		JPanel p4 = new JPanel(new BorderLayout());
		p4.setBorder(new EmptyBorder(10, 10, 5, 10));

		m_previewPane = new JTextPane();
		m_previewPane.setEditorKit(m_kit);
		m_previewPane.setBackground(Color.white);
		m_previewPane.setEditable(false);
		JScrollPane sp = new JScrollPane(m_previewPane);
		sp.setPreferredSize(new Dimension(200, 100));
		p4.add(sp, BorderLayout.CENTER);

		final JTabbedPane tb = new JTabbedPane();
		tb.addTab("Table", p1);
		tb.addTab("Size", p2);
		tb.addTab("Color", p3);
		tb.addTab("Preview", p4);
		pp.add(tb);

		ChangeListener chl = new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				if (tb.getSelectedIndex() != 3)
					return;
				setCursor(Cursor.getPredefinedCursor(
					Cursor.WAIT_CURSOR));
				try {
					HTMLDocument doc = (HTMLDocument)m_kit.createDefaultDocument();
					doc.setAsynchronousLoadPriority(0);
					StringReader sr = new StringReader(generateHTML());
					m_kit.read(sr, doc, 0);
					sr.close();

					m_previewPane.setDocument(doc);
					validate();
					repaint();
				}
				catch (Exception ex) {
					ex.printStackTrace();
				}
				finally {
					setCursor(Cursor.getPredefinedCursor(
						Cursor.DEFAULT_CURSOR));
				}
			}
		};
		tb.addChangeListener(chl);

		bt = new JButton("Insert");
		lst = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				m_succeeded = true;
				dispose();
			}
		};
		bt.addActionListener(lst);
		pp.add(bt);

		bt = new JButton("Cancel");
		lst = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		};
		bt.addActionListener(lst);
		pp.add(bt);

		getContentPane().add(pp, BorderLayout.CENTER);
		pack();
		setResizable(true);
		setLocationRelativeTo(parent);
	}

	public boolean succeeded() {
		return m_succeeded;
	}

	protected String generateHTML() {
		StringBuffer buff = new StringBuffer();
		buff.append("<table");

		int tableWidth = ((Integer)m_tableWidthSpn.getValue()).intValue();
		int tableHeight = ((Integer)m_tableHeightSpn.getValue()).intValue();
		String unit = "";
		if (m_tableUnitsCb.getSelectedIndex()==0)
			unit = "%";
		if (tableWidth > 0)
			buff.append(" width=\"").append(tableWidth).append(unit).append("\"");
		if (tableHeight > 0)
			buff.append(" height=\"").append(tableHeight).append(unit).append("\"");

		buff.append(" cellspacing=\"").append(m_spacingSpn.getValue()).append("\"");
		buff.append(" cellpadding=\"").append(m_paddingSpn.getValue()).append("\"");
		buff.append(" border=\"").append(m_borderWidthSpn.getValue()).append("\"");
		if (m_borderColor != null)
			buff.append(" bordercolor=\"").append(Utils.colorToHex(m_borderColor)).append("\"");
		if (m_backgroundColor != null)
			buff.append(" bgcolor=\"").append(Utils.colorToHex(m_backgroundColor)).append("\"");
		buff.append(">\n");

		int nRows = ((Integer)m_rowsSpn.getValue()).intValue();
		int nCols = ((Integer)m_colsSpn.getValue()).intValue();
		for (int k=0; k<nRows; k++) {
			buff.append("<tr>\n");
			for (int s=0; s<nCols; s++)
				buff.append("<td>&nbsp;</td>\n");
			buff.append("</tr>\n");
		}

		buff.append("</table>\n");
		return buff.toString();
	}

}

class FontDialog extends JDialog
{
	protected boolean m_succeeded = false;
	protected OpenList m_lstFontName;
	protected OpenList m_lstFontSize;
	protected MutableAttributeSet m_attributes;
	protected JCheckBox m_chkBold;
	protected JCheckBox m_chkItalic;
	protected JCheckBox m_chkUnderline;

	protected JCheckBox m_chkStrikethrough;
	protected JCheckBox m_chkSubscript;
	protected JCheckBox m_chkSuperscript;

	protected JComboBox m_cbColor;
	protected JLabel m_preview;

	public FontDialog(JFrame parent,
		String[] names, String[] sizes)
	{
		super(parent, "Font", true);
		JPanel pp = new JPanel();
		pp.setBorder(new EmptyBorder(5,5,5,5));
		pp.setLayout(new BoxLayout(pp, BoxLayout.Y_AXIS));

		JPanel p = new JPanel(new GridLayout(1, 2, 10, 2));
		p.setBorder(new TitledBorder(new EtchedBorder(), "Font"));
		m_lstFontName = new OpenList(names, "Name:");
		p.add(m_lstFontName);

		m_lstFontSize = new OpenList(sizes, "Size:");
		p.add(m_lstFontSize);
		pp.add(p);

		p = new JPanel(new GridLayout(2, 3, 10, 5));
		p.setBorder(new TitledBorder(new EtchedBorder(), "Effects"));
		m_chkBold = new JCheckBox("Bold");
		p.add(m_chkBold);
		m_chkItalic = new JCheckBox("Italic");
		p.add(m_chkItalic);
		m_chkUnderline = new JCheckBox("Underline");
		p.add(m_chkUnderline);
		m_chkStrikethrough = new JCheckBox("Strikeout");
		p.add(m_chkStrikethrough);
		m_chkSubscript = new JCheckBox("Subscript");
		p.add(m_chkSubscript);
		m_chkSuperscript = new JCheckBox("Superscript");
		p.add(m_chkSuperscript);
		pp.add(p);
		pp.add(Box.createVerticalStrut(5));

		p = new JPanel();
		p.setLayout(new BoxLayout(p, BoxLayout.X_AXIS));
		p.add(Box.createHorizontalStrut(10));
		p.add(new JLabel("Color:"));
		p.add(Box.createHorizontalStrut(20));
		m_cbColor = new JComboBox();

		int[] values = new int[] { 0, 128, 192, 255 };
		for (int r=0; r<values.length; r++) {
			for (int g=0; g<values.length; g++) {
				for (int b=0; b<values.length; b++) {
					Color c = new Color(values[r], values[g], values[b]);
					m_cbColor.addItem(c);
				}
			}
		}

		m_cbColor.setRenderer(new ColorComboRenderer());
		p.add(m_cbColor);
		p.add(Box.createHorizontalStrut(10));
		pp.add(p);

		ListSelectionListener lsel = new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				updatePreview();
			}
		};
		m_lstFontName.addListSelectionListener(lsel);
		m_lstFontSize.addListSelectionListener(lsel);

		ActionListener lst = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updatePreview();
			}
		};
		m_chkBold.addActionListener(lst);
		m_chkItalic.addActionListener(lst);
		m_cbColor.addActionListener(lst);

		p = new JPanel(new BorderLayout());
		p.setBorder(new TitledBorder(new EtchedBorder(), "Preview"));
		m_preview = new JLabel("Preview Font", JLabel.CENTER);
		m_preview.setBackground(Color.white);
		m_preview.setForeground(Color.black);
		m_preview.setOpaque(true);
		m_preview.setBorder(new LineBorder(Color.black));
		m_preview.setPreferredSize(new Dimension(120, 40));
		p.add(m_preview, BorderLayout.CENTER);
		pp.add(p);

		p = new JPanel(new FlowLayout());
		JPanel p1 = new JPanel(new GridLayout(1, 2, 10, 0));
		JButton btOK = new JButton("OK");
		lst = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				m_succeeded = true;
				dispose();
			}
		};
		btOK.addActionListener(lst);
		p1.add(btOK);

		JButton btCancel = new JButton("Cancel");
		lst = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		};
		btCancel.addActionListener(lst);
		p1.add(btCancel);
		p.add(p1);
		pp.add(p);

		getContentPane().add(pp, BorderLayout.CENTER);
		pack();
		setResizable(false);
		setLocationRelativeTo(parent);
	}

	public void setAttributes(AttributeSet a) {
		m_attributes = new SimpleAttributeSet(a);
		String name = StyleConstants.getFontFamily(a);
		m_lstFontName.setSelected(name);
		int size = StyleConstants.getFontSize(a);
		m_lstFontSize.setSelectedInt(size);
		m_chkBold.setSelected(StyleConstants.isBold(a));
		m_chkItalic.setSelected(StyleConstants.isItalic(a));
		m_chkUnderline.setSelected(StyleConstants.isUnderline(a));
		m_chkStrikethrough.setSelected(
			StyleConstants.isStrikeThrough(a));
		m_chkSubscript.setSelected(StyleConstants.isSubscript(a));
		m_chkSuperscript.setSelected(StyleConstants.isSuperscript(a));
		m_cbColor.setSelectedItem(StyleConstants.getForeground(a));
		updatePreview();
	}

	public AttributeSet getAttributes() {
		if (m_attributes == null)
			return null;
		StyleConstants.setFontFamily(m_attributes,
			m_lstFontName.getSelected());
		StyleConstants.setFontSize(m_attributes,
			m_lstFontSize.getSelectedInt());
		StyleConstants.setBold(m_attributes,
			m_chkBold.isSelected());
		StyleConstants.setItalic(m_attributes,
			m_chkItalic.isSelected());
		StyleConstants.setUnderline(m_attributes,
			m_chkUnderline.isSelected());
		StyleConstants.setStrikeThrough(m_attributes,
			m_chkStrikethrough.isSelected());
		StyleConstants.setSubscript(m_attributes,
			m_chkSubscript.isSelected());
		StyleConstants.setSuperscript(m_attributes,
			m_chkSuperscript.isSelected());
		StyleConstants.setForeground(m_attributes,
			(Color)m_cbColor.getSelectedItem());
		return m_attributes;
	}


	public boolean succeeded() {
		return m_succeeded;
	}

	protected void updatePreview() {
		String name = m_lstFontName.getSelected();
		int size = m_lstFontSize.getSelectedInt();
		if (size <= 0)
			return;
		int style = Font.PLAIN;
		if (m_chkBold.isSelected())
			style |= Font.BOLD;
		if (m_chkItalic.isSelected())
			style |= Font.ITALIC;

		// Bug Alert! This doesn't work if only style is changed.
		Font fn = new Font(name, style, size);
		m_preview.setFont(fn);

		Color c = (Color)m_cbColor.getSelectedItem();
		m_preview.setForeground(c);
		m_preview.repaint();
	}
}

class OpenList extends JPanel
	implements ListSelectionListener, ActionListener
{
	protected JLabel m_title;
	protected JTextField m_text;
	protected JList m_list;
	protected JScrollPane m_scroll;

	public OpenList(String[] data, String title) {
		setLayout(null);
		m_title = new JLabel(title, JLabel.LEFT);
		add(m_title);
		m_text = new JTextField();
		m_text.addActionListener(this);
		add(m_text);
		m_list = new JList(data);
		m_list.setVisibleRowCount(4);
		m_list.addListSelectionListener(this);
		m_list.setFont(m_text.getFont());
		m_scroll = new JScrollPane(m_list);
		add(m_scroll);
	}

	// NEW
	public OpenList(String title, int numCols) {
		setLayout(null);
		m_title = new JLabel(title, JLabel.LEFT);
		add(m_title);
		m_text = new JTextField(numCols);
		m_text.addActionListener(this);
		add(m_text);
		m_list = new JList();
		m_list.setVisibleRowCount(4);
		m_list.addListSelectionListener(this);
		m_scroll = new JScrollPane(m_list);
		add(m_scroll);
	}

	// NEW
	public void appendResultSet(ResultSet results, int index,
	 boolean toTitleCase)
	{
		m_text.setText("");
		DefaultListModel model = new DefaultListModel();
		try {
			while (results.next()) {
				String str = results.getString(index);
				if (toTitleCase)
					str = Utils.titleCase(str);
				model.addElement(str);
			}
		}
		catch (SQLException ex) {
			System.err.println("appendResultSet: "+ex.toString());
		}
		m_list.setModel(model);
		if (model.getSize() > 0)
			m_list.setSelectedIndex(0);
	}

	public void setSelected(String sel) {
		m_list.setSelectedValue(sel, true);
		m_text.setText(sel);
	}

	public String getSelected() { return m_text.getText(); }

	public void setSelectedInt(int value) {
		setSelected(Integer.toString(value));
	}

	public int getSelectedInt() {
		try {
			return Integer.parseInt(getSelected());
		}
		catch (NumberFormatException ex) { return -1; }
	}

	public void valueChanged(ListSelectionEvent e) {
		Object obj = m_list.getSelectedValue();
		if (obj != null)
			m_text.setText(obj.toString());
	}

	public void actionPerformed(ActionEvent e) {
		ListModel model = m_list.getModel();
		String key = m_text.getText().toLowerCase();
		for (int k=0; k<model.getSize(); k++) {
			String data = (String)model.getElementAt(k);
			if (data.toLowerCase().startsWith(key)) {
				m_list.setSelectedValue(data, true);
				break;
			}
		}
	}

	public void addListSelectionListener(ListSelectionListener lst) {
		m_list.addListSelectionListener(lst);
	}

	public Dimension getPreferredSize() {
		Insets ins = getInsets();
		Dimension d1 = m_title.getPreferredSize();
		Dimension d2 = m_text.getPreferredSize();
		Dimension d3 = m_scroll.getPreferredSize();
		int w = Math.max(Math.max(d1.width, d2.width), d3.width);
		int h = d1.height + d2.height + d3.height;
		return new Dimension(w+ins.left+ins.right,
			h+ins.top+ins.bottom);
	}

	public Dimension getMaximumSize() {
		Insets ins = getInsets();
		Dimension d1 = m_title.getMaximumSize();
		Dimension d2 = m_text.getMaximumSize();
		Dimension d3 = m_scroll.getMaximumSize();
		int w = Math.max(Math.max(d1.width, d2.width), d3.width);
		int h = d1.height + d2.height + d3.height;
		return new Dimension(w+ins.left+ins.right,
			h+ins.top+ins.bottom);
	}

	public Dimension getMinimumSize() {
		Insets ins = getInsets();
		Dimension d1 = m_title.getMinimumSize();
		Dimension d2 = m_text.getMinimumSize();
		Dimension d3 = m_scroll.getMinimumSize();
		int w = Math.max(Math.max(d1.width, d2.width), d3.width);
		int h = d1.height + d2.height + d3.height;
		return new Dimension(w+ins.left+ins.right,
			h+ins.top+ins.bottom);
	}

	public void doLayout() {
		Insets ins = getInsets();
		Dimension d = getSize();
		int x = ins.left;
		int y = ins.top;
		int w = d.width-ins.left-ins.right;
		int h = d.height-ins.top-ins.bottom;

		Dimension d1 = m_title.getPreferredSize();
		m_title.setBounds(x, y, w, d1.height);
		y += d1.height;
		Dimension d2 = m_text.getPreferredSize();
		m_text.setBounds(x, y, w, d2.height);
		y += d2.height;
		m_scroll.setBounds(x, y, w, h-y);
	}
}

class ColorComboRenderer extends JPanel implements ListCellRenderer
{
	protected Color m_color = Color.black;
	protected Color m_focusColor =
		(Color) UIManager.get("List.selectionBackground");
	protected Color m_nonFocusColor = Color.white;

	public Component getListCellRendererComponent(JList list,
	 Object obj, int row, boolean sel, boolean hasFocus)
	{
		if (hasFocus || sel)
			setBorder(new CompoundBorder(
				new MatteBorder(2, 10, 2, 10, m_focusColor),
				new LineBorder(Color.black)));
		else
			setBorder(new CompoundBorder(
				new MatteBorder(2, 10, 2, 10, m_nonFocusColor),
				new LineBorder(Color.black)));

		if (obj instanceof Color)
			m_color = (Color) obj;
		return this;
	}

	public void paintComponent(Graphics g) {
		setBackground(m_color);
		super.paintComponent(g);
	}
}

class FindDialog extends JDialog
{
	protected HtmlProcessor m_owner;
	protected JTabbedPane m_tb;
	protected JTextField m_txtFind1;
	protected JTextField m_txtFind2;
	protected Document m_docFind;
	protected Document m_docReplace;
	protected ButtonModel m_modelWord;
	protected ButtonModel m_modelCase;
	protected ButtonModel m_modelUp;
	protected ButtonModel m_modelDown;

	protected int m_searchIndex = -1;
	protected boolean m_searchUp = false;
	protected String	m_searchData;

	public FindDialog(HtmlProcessor owner, int index) {
		super(owner, "Find and Replace", false);
		m_owner = owner;

		m_tb = new JTabbedPane();

		// "Find" panel
		JPanel p1 = new JPanel(new BorderLayout());

		JPanel pc1 = new JPanel(new BorderLayout());

		JPanel pf = new JPanel();
		pf.setLayout(new DialogLayout2(20, 5));
		pf.setBorder(new EmptyBorder(8, 5, 8, 0));
		pf.add(new JLabel("Find what:"));

		m_txtFind1 = new JTextField();
		m_docFind = m_txtFind1.getDocument();
		pf.add(m_txtFind1);
		pc1.add(pf, BorderLayout.CENTER);

		JPanel po = new JPanel(new GridLayout(2, 2, 8, 2));
		po.setBorder(new TitledBorder(new EtchedBorder(),
			"Options"));

		JCheckBox chkWord = new JCheckBox("Whole words only");
		chkWord.setMnemonic('w');
		m_modelWord = chkWord.getModel();
		po.add(chkWord);

		ButtonGroup bg = new ButtonGroup();
		JRadioButton rdUp = new JRadioButton("Search up");
		rdUp.setMnemonic('u');
		m_modelUp = rdUp.getModel();
		bg.add(rdUp);
		po.add(rdUp);

		JCheckBox chkCase = new JCheckBox("Match case");
		chkCase.setMnemonic('c');
		m_modelCase = chkCase.getModel();
		po.add(chkCase);

		JRadioButton rdDown = new JRadioButton("Search down", true);
		rdDown.setMnemonic('d');
		m_modelDown = rdDown.getModel();
		bg.add(rdDown);
		po.add(rdDown);
		pc1.add(po, BorderLayout.SOUTH);

		p1.add(pc1, BorderLayout.CENTER);

		JPanel p01 = new JPanel(new FlowLayout());
		JPanel p = new JPanel(new GridLayout(2, 1, 2, 8));

		ActionListener findAction = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				findNext(false, true);
			}
		};
		JButton btFind = new JButton("Find Next");
		btFind.addActionListener(findAction);
		btFind.setMnemonic('f');
		p.add(btFind);

		ActionListener closeAction = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		};
		JButton btClose = new JButton("Close");
		btClose.addActionListener(closeAction);
		btClose.setDefaultCapable(true);
		p.add(btClose);
		p01.add(p);
		p1.add(p01, BorderLayout.EAST);

		m_tb.addTab("Find", p1);

		// "Replace" panel
		JPanel p2 = new JPanel(new BorderLayout());

		JPanel pc2 = new JPanel(new BorderLayout());

		JPanel pc = new JPanel();
		pc.setLayout(new DialogLayout2(20, 5));
		pc.setBorder(new EmptyBorder(8, 5, 8, 0));

		pc.add(new JLabel("Find what:"));
		m_txtFind2 = new JTextField();
		m_txtFind2.setDocument(m_docFind);
		pc.add(m_txtFind2);

		pc.add(new JLabel("Replace:"));
		JTextField txtReplace = new JTextField();
		m_docReplace = txtReplace.getDocument();
		pc.add(txtReplace);
		pc2.add(pc, BorderLayout.CENTER);

		po = new JPanel(new GridLayout(2, 2, 8, 2));
		po.setBorder(new TitledBorder(new EtchedBorder(),
			"Options"));

		chkWord = new JCheckBox("Whole words only");
		chkWord.setMnemonic('w');
		chkWord.setModel(m_modelWord);
		po.add(chkWord);

		bg = new ButtonGroup();
		rdUp = new JRadioButton("Search up");
		rdUp.setMnemonic('u');
		rdUp.setModel(m_modelUp);
		bg.add(rdUp);
		po.add(rdUp);

		chkCase = new JCheckBox("Match case");
		chkCase.setMnemonic('c');
		chkCase.setModel(m_modelCase);
		po.add(chkCase);

		rdDown = new JRadioButton("Search down", true);
		rdDown.setMnemonic('d');
		rdDown.setModel(m_modelDown);
		bg.add(rdDown);
		po.add(rdDown);
		pc2.add(po, BorderLayout.SOUTH);

		p2.add(pc2, BorderLayout.CENTER);

		JPanel p02 = new JPanel(new FlowLayout());
		p = new JPanel(new GridLayout(3, 1, 2, 8));

		ActionListener replaceAction = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				findNext(true, true);
			}
		};
		JButton btReplace = new JButton("Replace");
		btReplace.addActionListener(replaceAction);
		btReplace.setMnemonic('r');
		p.add(btReplace);

		ActionListener replaceAllAction = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int counter = 0;
				while (true) {
					int result = findNext(true, false);
					if (result < 0)		// error
						return;
					else if (result == 0)		// no more
						break;
					counter++;
				}
				JOptionPane.showMessageDialog(m_owner,
					counter+" replacement(s) have been done",
					HtmlProcessor.APP_NAME,
					JOptionPane.INFORMATION_MESSAGE);
			}
		};
		JButton btReplaceAll = new JButton("Replace All");
		btReplaceAll.addActionListener(replaceAllAction);
		btReplaceAll.setMnemonic('a');
		p.add(btReplaceAll);

		btClose = new JButton("Close");
		btClose.addActionListener(closeAction);
		btClose.setDefaultCapable(true);
		p.add(btClose);
		p02.add(p);
		p2.add(p02, BorderLayout.EAST);

		// Make button columns the same size
		p01.setPreferredSize(p02.getPreferredSize());

		m_tb.addTab("Replace", p2);

		m_tb.setSelectedIndex(index);

		JPanel pp = new JPanel(new BorderLayout());
		pp.setBorder(new EmptyBorder(5,5,5,5));
		pp.add(m_tb, BorderLayout.CENTER);
		getContentPane().add(pp, BorderLayout.CENTER);

		pack();
		setResizable(false);
		setLocationRelativeTo(owner);

		WindowListener flst = new WindowAdapter() {
			public void windowActivated(WindowEvent e) {
				m_searchIndex = -1;
			}

			public void windowDeactivated(WindowEvent e) {
				m_searchData = null;
			}
		};
		addWindowListener(flst);
	}

	public void setSelectedIndex(int index) {
		m_tb.setSelectedIndex(index);
		setVisible(true);
		m_searchIndex = -1;
	}

	public int findNext(boolean doReplace, boolean showWarnings) {
		JTextPane monitor = m_owner.getTextPane();
		int pos = monitor.getCaretPosition();
		if (m_modelUp.isSelected() != m_searchUp) {
			m_searchUp = m_modelUp.isSelected();
			m_searchIndex = -1;
		}

		if (m_searchIndex == -1) {
			try {
				Document doc = m_owner.getDocument();
				if (m_searchUp)
					m_searchData = doc.getText(0, pos);
				else
					m_searchData = doc.getText(pos, doc.getLength()-pos);
				m_searchIndex = pos;
			}
			catch (BadLocationException ex) {
				warning(ex.toString());
				return -1;
			}
		}

		String key = "";
		try {
			key = m_docFind.getText(0, m_docFind.getLength());
		}
		catch (BadLocationException ex) {}
		if (key.length()==0) {
			warning("Please enter the target to search");
			return -1;
		}
		if (!m_modelCase.isSelected()) {
			m_searchData = m_searchData.toLowerCase();
			key = key.toLowerCase();
		}
		if (m_modelWord.isSelected()) {
			for (int k=0; k<Utils.WORD_SEPARATORS.length; k++) {
				if (key.indexOf(Utils.WORD_SEPARATORS[k]) >= 0) {
					warning("The text target contains an illegal "+
						"character \'"+Utils.WORD_SEPARATORS[k]+"\'");
					return -1;
				}
			}
		}

		String replacement = "";
		if (doReplace) {
			try {
				replacement = m_docReplace.getText(0,
					m_docReplace.getLength());
			} catch (BadLocationException ex) {}
		}

		int xStart = -1;
		int xFinish = -1;
		while (true)
		{
			if (m_searchUp)
				xStart = m_searchData.lastIndexOf(key, pos-1);
			else
				xStart = m_searchData.indexOf(key, pos-m_searchIndex);
			if (xStart < 0) {
				if (showWarnings)
					warning("Text not found");
				return 0;
			}

			xFinish = xStart+key.length();

			if (m_modelWord.isSelected()) {
				boolean s1 = xStart>0;
				boolean b1 = s1 && !Utils.isSeparator(m_searchData.charAt(
					xStart-1));
				boolean s2 = xFinish<m_searchData.length();
				boolean b2 = s2 && !Utils.isSeparator(m_searchData.charAt(
					xFinish));

				if (b1 || b2)		// Not a whole word
				{
					if (m_searchUp && s1)		// Can continue up
					{
						pos = xStart;
						continue;
					}
					if (!m_searchUp && s2)		// Can continue down
					{
						pos = xFinish+1;
						continue;
					}
					// Found, but not a whole word, and we cannot continue
					if (showWarnings)
						warning("Text not found");
					return 0;
				}
			}
			break;
		}

		if (!m_searchUp) {
			xStart += m_searchIndex;
			xFinish += m_searchIndex;
		}
		if (doReplace) {
			m_owner.setSelection(xStart, xFinish, m_searchUp);
			monitor.replaceSelection(replacement);
			m_owner.setSelection(xStart, xStart+replacement.length(),
				m_searchUp);
			m_searchIndex = -1;
		}
		else
			m_owner.setSelection(xStart, xFinish, m_searchUp);
		return 1;
	}

	protected void warning(String message) {
		JOptionPane.showMessageDialog(m_owner,
			message, HtmlProcessor.APP_NAME,
			JOptionPane.INFORMATION_MESSAGE);
	}
}

// NEW
class SpellChecker extends Thread
{
	protected static String SELECT_QUERY =
		"SELECT Data.word FROM Data WHERE Data.word = ";
	protected static String SOUNDEX_QUERY =
		"SELECT Data.word FROM Data WHERE Data.soundex = ";

	protected HtmlProcessor m_owner;
	protected Connection m_conn;
	protected DocumentTokenizer m_tokenizer;
	protected Hashtable	m_ignoreAll;
	protected SpellingDialog m_dlg;

	public SpellChecker(HtmlProcessor owner) {
		m_owner = owner;
	}

	public void run() {
		JTextPane monitor = m_owner.getTextPane();
		m_owner.setEnabled(false);
		monitor.setEnabled(false);

		m_dlg = new SpellingDialog(m_owner);
		m_ignoreAll = new Hashtable();

		try {
			// Load the JDBC-ODBC bridge driver
			Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
			m_conn = DriverManager.getConnection(
				"jdbc:odbc:Shakespeare", "admin", "");
			Statement selStmt = m_conn.createStatement();

			Document doc = m_owner.getDocument();
			int pos = monitor.getCaretPosition();
			m_tokenizer = new DocumentTokenizer(doc, pos);
				String word, wordLowCase;

			while (m_tokenizer.hasMoreTokens()) {
				word = m_tokenizer.nextToken();
				if (word.equals(word.toUpperCase()))
					continue;
				if (word.length()<=1)
					continue;
				if (Utils.hasDigits(word))
					continue;
				wordLowCase = word.toLowerCase();
				if (m_ignoreAll.get(wordLowCase) != null)
					continue;

				ResultSet results = selStmt.executeQuery(
					SELECT_QUERY+"'"+wordLowCase+"'");
				if (results.next())
					continue;

				results = selStmt.executeQuery(SOUNDEX_QUERY+
					"'"+Utils.soundex(wordLowCase)+"'");
				m_owner.setSelection(m_tokenizer.getStartPos(),
					m_tokenizer.getEndPos(), false);
				if (!m_dlg.suggest(word, results))
					break;
			}

			m_conn.close();
			System.gc();
			monitor.setCaretPosition(pos);
		}
		catch (Exception ex) {
			ex.printStackTrace();
			System.err.println("SpellChecker error: "+ex.toString());
		}

		monitor.setEnabled(true);
		m_owner.setEnabled(true);
		m_owner.setCursor(Cursor.getPredefinedCursor(
			Cursor.DEFAULT_CURSOR));
	}

	protected void replaceSelection(String replacement) {
		int xStart = m_tokenizer.getStartPos();
		int xFinish = m_tokenizer.getEndPos();
		m_owner.setSelection(xStart, xFinish, false);
		m_owner.getTextPane().replaceSelection(replacement);
		xFinish = xStart+replacement.length();
		m_owner.setSelection(xStart, xFinish, false);
		m_tokenizer.setPosition(xFinish);
	}

	protected void addToDB(String word) {
		String sdx = Utils.soundex(word);
		try {
			Statement stmt = m_conn.createStatement();
			stmt.executeUpdate(
				"INSERT INTO DATA (Word, Soundex) VALUES ('"+
				word+"', '"+sdx+"')");
		}
		catch (Exception ex) {
			ex.printStackTrace();
			System.err.println("SpellChecker error: "+ex.toString());
		}
	}

	class SpellingDialog extends JDialog
	{
		protected JTextField	m_txtNotFound;
		protected OpenList		m_suggestions;

		protected String	 m_word;
		protected boolean	m_continue;

		public SpellingDialog(HtmlProcessor owner) {
			super(owner, "Spelling", true);

			JPanel p = new JPanel();
			p.setBorder(new EmptyBorder(5, 5, 5, 5));
			p.setLayout(new BoxLayout(p, BoxLayout.X_AXIS));
			p.add(new JLabel("Not in dictionary:"));
			p.add(Box.createHorizontalStrut(10));
			m_txtNotFound = new JTextField();
			m_txtNotFound.setEditable(false);
			p.add(m_txtNotFound);
			getContentPane().add(p, BorderLayout.NORTH);

			m_suggestions = new OpenList("Change to:", 12);
			m_suggestions.setBorder(new EmptyBorder(0, 5, 5, 5));
			getContentPane().add(m_suggestions, BorderLayout.CENTER);

			JPanel p1 = new JPanel();
			p1.setBorder(new EmptyBorder(20, 0, 5, 5));
			p1.setLayout(new FlowLayout());
			p = new JPanel(new GridLayout(3, 2, 8, 2));

			JButton bt = new JButton("Change");
			ActionListener lst = new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					replaceSelection(m_suggestions.getSelected());
					m_continue = true;
					setVisible(false);
				}
			};
			bt.addActionListener(lst);
			bt.setMnemonic('c');
			p.add(bt);

			bt = new JButton("Add");
			lst = new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					addToDB(m_word.toLowerCase());
					m_continue = true;
					setVisible(false);
				}
			};
			bt.addActionListener(lst);
			bt.setMnemonic('a');
			p.add(bt);

			bt = new JButton("Ignore");
			lst = new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					m_continue = true;
					setVisible(false);
				}
			};
			bt.addActionListener(lst);
			bt.setMnemonic('i');
			p.add(bt);

			bt = new JButton("Suggest");
			lst = new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						m_word = m_suggestions.getSelected();
						Statement selStmt = m_conn.createStatement();
						ResultSet results = selStmt.executeQuery(
							SELECT_QUERY+"'"+m_word.toLowerCase()+"'");
						boolean toTitleCase = Character.isUpperCase(
							m_word.charAt(0));
						m_suggestions.appendResultSet(results, 1,
							toTitleCase);
					}
					catch (Exception ex) {
						ex.printStackTrace();
						System.err.println("SpellChecker error: "+
							ex.toString());
					}
				}
			};
			bt.addActionListener(lst);
			bt.setMnemonic('s');
			p.add(bt);

			bt = new JButton("Ignore All");
			lst = new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					m_ignoreAll.put(m_word.toLowerCase(), m_word);
					m_continue = true;
					setVisible(false);
				}
			};
			bt.addActionListener(lst);
			bt.setMnemonic('g');
			p.add(bt);

			bt = new JButton("Close");
			lst = new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					m_continue = false;
					setVisible(false);
				}
			};
			bt.addActionListener(lst);
			bt.setDefaultCapable(true);
			p.add(bt);
			p1.add(p);
			getContentPane().add(p1, BorderLayout.EAST);

			pack();
			setResizable(false);
			setLocationRelativeTo(owner);
		}

		public boolean suggest(String word, ResultSet results) {
			m_continue = false;
			m_word = word;
			m_txtNotFound.setText(word);
			boolean toTitleCase = Character.isUpperCase(
				word.charAt(0));
			m_suggestions.appendResultSet(results, 1, toTitleCase);
			show();
			return m_continue;
		}
	}
}

// NEW
class DocumentTokenizer
{
	protected Document m_doc;
	protected Segment	m_seg;
	protected int m_startPos;
	protected int m_endPos;
	protected int m_currentPos;

	public DocumentTokenizer(Document doc, int offset) {
		m_doc = doc;
		m_seg = new Segment();
		setPosition(offset);
	}

	public boolean hasMoreTokens() {
		return (m_currentPos < m_doc.getLength());
	}

	public String nextToken() {
		StringBuffer s = new StringBuffer();
		try {
			// Trim leading separators
			while (hasMoreTokens()) {
				m_doc.getText(m_currentPos, 1, m_seg);
				char ch = m_seg.array[m_seg.offset];
				if (!Utils.isSeparator(ch)) {
					m_startPos = m_currentPos;
					break;
				}
				m_currentPos++;
			}

			// Append characters
			while (hasMoreTokens()) {
				m_doc.getText(m_currentPos, 1, m_seg);
				char ch = m_seg.array[m_seg.offset];
				if (Utils.isSeparator(ch)) {
					m_endPos = m_currentPos;
					break;
				}
				s.append(ch);
				m_currentPos++;
			}
		}
		catch (BadLocationException ex) {
			System.err.println("nextToken: "+ex.toString());
			m_currentPos = m_doc.getLength();
		}
		return s.toString();
	}

	public int getStartPos() { return m_startPos; }

	public int getEndPos() { return m_endPos; }

	public void setPosition(int pos) {
		m_startPos = pos;
		m_endPos = pos;
		m_currentPos = pos;
	}
}
