/** 
 *  Copyright 1999-2002 Matthew Robinson and Pavel Vorobiev. 
 *  All Rights Reserved. 
 * 
 *  =================================================== 
 *  This program contains code from the book "Swing" 
 *  2nd Edition by Matthew Robinson and Pavel Vorobiev 
 *  http://www.spindoczine.com/sbe 
 *  =================================================== 
 * 
 *  The above paragraph must be included in full, unmodified 
 *  and completely intact in the beginning of any source code 
 *  file that references, copies or uses (in any way, shape 
 *  or form) code contained in this file. 
 */ 

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;	// NEW

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.tree.*;
import javax.swing.table.*;

import javax.xml.parsers.*;
import org.w3c.dom.*;

/*
Adds editing of node's content and attributes.
Prompts to save changes before they may be lost (similar to chapter 12)
*/
public class XmlViewer
	extends JFrame {

	public static final String APP_NAME = "XML Viewer";

	protected Document m_doc;

	protected JTree  m_tree;
	protected DefaultTreeModel m_model;
	protected DefaultTreeCellEditor m_treeEditor;	// NEW
	protected Node m_editingNode = null;	// NEW

	protected JTable m_table;
	protected AttrTableModel m_tableModel;

	protected JFileChooser m_chooser;
	protected File  m_currentFile;

	protected boolean m_xmlChanged = false;

	public XmlViewer() {
		super(APP_NAME);
		setSize(800, 400);
		getContentPane().setLayout(new BorderLayout());

		JToolBar tb = createToolbar();
		getContentPane().add(tb, BorderLayout.NORTH);

		DefaultMutableTreeNode top = new DefaultMutableTreeNode(
			"No XML loaded");
		m_model = new DefaultTreeModel(top);
		m_tree = new JTree(m_model);

		m_tree.getSelectionModel().setSelectionMode(
			TreeSelectionModel.SINGLE_TREE_SELECTION);
		m_tree.setShowsRootHandles(true);
		m_tree.setEditable(false);

		DefaultTreeCellRenderer renderer = new DefaultTreeCellRenderer() {
			public Component getTreeCellRendererComponent(JTree tree,
				Object value, boolean sel, boolean expanded,
				boolean leaf, int row, boolean hasFocus) {
				Component res = super.getTreeCellRendererComponent(tree,
					value, sel, expanded, leaf, row, hasFocus);
				if (value instanceof XmlViewerNode) {
					Node node = ((XmlViewerNode)value).getXmlNode();
					if (node instanceof Element)
						setIcon(expanded ? openIcon : closedIcon);	// No difference in Metal PLAF
					else
						setIcon(leafIcon);
				}
				return res;
			}
		};
		m_tree.setCellRenderer(renderer);

		// NEW
		m_treeEditor = new DefaultTreeCellEditor(m_tree, renderer) {
			public boolean isCellEditable(EventObject event) {
				Node node = getSelectedNode();
				if (node != null && node.getNodeType() == Node.TEXT_NODE)
					return super.isCellEditable(event);
				else
					return false;
			}

			public Component getTreeCellEditorComponent(JTree tree, Object value,
				boolean isSelected, boolean expanded, boolean leaf, int row) {
				if (value instanceof XmlViewerNode)
					m_editingNode = ((XmlViewerNode)value).getXmlNode();
				return super.getTreeCellEditorComponent(tree,
					value, isSelected, expanded, leaf, row);
			}
		};
		m_treeEditor.addCellEditorListener(new XmlEditorListener());
		m_tree.setCellEditor(m_treeEditor);
		m_tree.setEditable(true);
		m_tree.setInvokesStopCellEditing(true);

		m_tableModel = new AttrTableModel();
		m_table = new JTable(m_tableModel);

		JScrollPane s1 = new JScrollPane(m_tree);
		JScrollPane s2 = new JScrollPane(m_table);
		s2.getViewport().setBackground(m_table.getBackground());
		JSplitPane sp = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, s1, s2);
		sp.setDividerLocation(400);
		sp.setDividerSize(5);
		getContentPane().add(sp, BorderLayout.CENTER);

		TreeSelectionListener lSel = new TreeSelectionListener() {
			public void valueChanged(TreeSelectionEvent e) {
				Node node = getSelectedNode();
				setNodeToTable(node);	// null is OK
			}
		};
		m_tree.addTreeSelectionListener(lSel);

		// NEW
		WindowListener wndCloser = new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				if (!promptToSave())
					return;
				System.exit(0);
			}
		};
		addWindowListener(wndCloser);

		m_chooser = new JFileChooser();
		m_chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		m_chooser.setFileFilter(new SimpleFilter("xml",
			"XML Files"));
		try {
			File dir = (new File(".")).getCanonicalFile();
			m_chooser.setCurrentDirectory(dir);
		} catch (IOException ex) {}
	}

	protected JToolBar createToolbar() {
		JToolBar tb = new JToolBar();
		tb.setFloatable(false);

		JButton bt = new JButton(new ImageIcon("Open24.gif"));
		bt.setToolTipText("Open XML file");
		ActionListener lst = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!promptToSave())	// NEW
					return;
				openDocument();
 			}
		};
		bt.addActionListener(lst);
		tb.add(bt);

		// NEW
		bt = new JButton(new ImageIcon("Save24.gif"));
		bt.setToolTipText("Save changes to current file");
		lst = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				saveFile(false);
 			}
		};
		bt.addActionListener(lst);
		tb.add(bt);

		// NEW
		bt = new JButton(new ImageIcon("SaveAs24.gif"));
		bt.setToolTipText("Save changes to another file");
		lst = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				saveFile(true);
 			}
		};
		bt.addActionListener(lst);
		tb.add(bt);

		return tb;
	}

	public String getDocumentName() {
		return m_currentFile==null ? "Untitled" :
			m_currentFile.getName();
	}

	protected void openDocument() {
		Thread runner = new Thread() {
			public void run() {
				if (m_chooser.showOpenDialog(XmlViewer.this) !=
					JFileChooser.APPROVE_OPTION)
					return;
				File f = m_chooser.getSelectedFile();
				if (f == null || !f.isFile())
					return;

				setCursor( Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR) );
				try {
					DocumentBuilderFactory docBuilderFactory =
						DocumentBuilderFactory.newInstance();
					DocumentBuilder docBuilder = docBuilderFactory.
						newDocumentBuilder();

					m_doc = docBuilder.parse(f);

					Element root = m_doc.getDocumentElement();
					root.normalize();

					DefaultMutableTreeNode top = createTreeNode(root);

					m_model.setRoot(top);
					m_tree.treeDidChange();
					expandTree(m_tree);
					setNodeToTable(null);
					m_currentFile = f;
					setTitle(APP_NAME+" ["+getDocumentName()+"]");
					m_xmlChanged = false;
				}
				catch (Exception ex) {
					showError(ex, "Error reading or parsing XML file");
				}
				finally {
					setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
				}
			}
		};
		runner.start();
	}

	// NEW
	protected boolean saveFile(boolean saveAs) {
		if (m_doc == null)
			return false;
		if (saveAs || m_currentFile == null) {
			if (m_chooser.showSaveDialog(XmlViewer.this) !=
				JFileChooser.APPROVE_OPTION)
				return false;
			File f = m_chooser.getSelectedFile();
			if (f == null)
				return false;
			m_currentFile = f;
			setTitle(APP_NAME+" ["+getDocumentName()+"]");
		}

		setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR) );
		try {
			FileWriter out = new FileWriter(m_currentFile);
			XMLRoutines.write(m_doc, out);
			out.close();
		}
		catch (Exception ex) {
			showError(ex, "Error saving XML file");
 		}
 		finally {
 			setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		}
		m_xmlChanged = false;
		return true;
	}

	// NEW
	protected boolean promptToSave() {
		if (!m_xmlChanged)
			return true;
		int result = JOptionPane.showConfirmDialog(this,
			"Save changes to "+getDocumentName()+"?",
			APP_NAME, JOptionPane.YES_NO_CANCEL_OPTION,
			JOptionPane.INFORMATION_MESSAGE);
		switch (result) {
		case JOptionPane.YES_OPTION:
			if (!saveFile(false))
				return false;
			return true;
		case JOptionPane.NO_OPTION:
			return true;
		case JOptionPane.CANCEL_OPTION:
			return false;
		}
		return true;
	}


	protected DefaultMutableTreeNode createTreeNode(Node root) {
		if (!canDisplayNode(root))
			return null;
		XmlViewerNode treeNode = new XmlViewerNode(root);
		NodeList list = root.getChildNodes();
		for (int k=0; k<list.getLength(); k++) {
			Node nd = list.item(k);
			DefaultMutableTreeNode child = createTreeNode(nd);
			if (child != null)
				treeNode.add(child);
		}
		return treeNode;
	}

	protected boolean canDisplayNode(Node node) {
		switch (node.getNodeType()) {
		case Node.ELEMENT_NODE:
			return true;
		case Node.TEXT_NODE:
			String text = node.getNodeValue().trim();
			return !(text.equals("") || text.equals("\n") || text.equals("\r\n"));
		}
		return false;
	}

	public XmlViewerNode getSelectedTreeNode() {
		TreePath path = m_tree.getSelectionPath();
		if (path == null)
			return null;
		Object obj = path.getLastPathComponent();
		if (!(obj instanceof XmlViewerNode))
			return null;
		return (XmlViewerNode)obj;
	}

	public Node getSelectedNode() {
		XmlViewerNode treeNode = getSelectedTreeNode();
		if (treeNode == null)
			return null;
		return treeNode.getXmlNode();
	}

	public void setNodeToTable(Node node) {
		m_tableModel.setNode(node);
		m_table.tableChanged(new TableModelEvent(m_tableModel));
	}

	public void showError(Exception ex, String message) {
		ex.printStackTrace();
		JOptionPane.showMessageDialog(this,
			message, APP_NAME,
			JOptionPane.WARNING_MESSAGE);
	}

    public static void expandTree(JTree tree) {
	    TreeNode root = (TreeNode)tree.getModel().getRoot();
	    TreePath path = new TreePath(root);
	    for (int k = 0; k<root.getChildCount(); k++) {
			TreeNode child = (TreeNode)root.getChildAt(k);
			expandTree(tree, path, child);
		}
	}

    public static void expandTree(JTree tree, TreePath path, TreeNode node) {
		if (path==null || node==null)
			return;
		tree.expandPath(path);
		TreePath newPath = path.pathByAddingChild(node);
	    for (int k = 0; k<node.getChildCount(); k++) {
			TreeNode child = (TreeNode)node.getChildAt(k);
			if (child != null) {
				expandTree(tree, newPath, child);
			}
		}
	}

	public static void main(String argv[]) {
		XmlViewer frame = new XmlViewer();
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);	// NEW
		frame.setVisible(true);
	}

	// NEW
	class XmlEditorListener implements CellEditorListener {
		public void editingStopped(ChangeEvent e) {
			String value = m_treeEditor.getCellEditorValue().toString();
			if (m_editingNode != null)
				m_editingNode.setNodeValue(value);
			TreePath path = m_tree.getSelectionPath();
			if (path != null) {
				DefaultMutableTreeNode treeNode =
					(DefaultMutableTreeNode)path.getLastPathComponent();
				treeNode.setUserObject(m_editingNode);
				m_model.nodeStructureChanged(treeNode);
			}
			m_xmlChanged = true;
			m_editingNode = null;
		}

		public void editingCanceled(ChangeEvent e) {
			m_editingNode = null;
		}
	}

	class AttrTableModel extends AbstractTableModel {
		public static final int NAME_COLUMN = 0;
		public static final int VALUE_COLUMN = 1;

		protected Node m_node;
		protected NamedNodeMap m_attrs;

		public void setNode(Node node) {
			m_node = node;
			m_attrs = node==null ? null : node.getAttributes();
		}

		public Node getNode() {
			return m_node;
		}

		public int getRowCount() {
			if (m_attrs == null)
				return 0;
			return m_attrs.getLength();
		}

		public int getColumnCount() {
			return 2;
		}

		public String getColumnName(int nCol) {
			return nCol==NAME_COLUMN ? "Attribute" : "Value";
		}

		public Object getValueAt(int nRow, int nCol) {
			if (m_attrs == null || nRow < 0 || nRow>=getRowCount())
				return "";
			Attr attr = (Attr)m_attrs.item(nRow);
			if (attr == null)
				return "";
			switch (nCol) {
			case NAME_COLUMN:
				return attr.getName();
			case VALUE_COLUMN:
				return attr.getValue();
			}
			return "";
		}

		// NEW
		public boolean isCellEditable(int nRow, int nCol) {
			return (nCol==VALUE_COLUMN);
		}

		public void setValueAt(Object value, int nRow, int nCol) {
			if (nRow < 0 || nRow>=getRowCount())
				return;
			if (!(m_node instanceof Element))
				return;
			String name = getValueAt(nRow, NAME_COLUMN).toString();
			((Element)m_node).setAttribute(name, value.toString());
			m_xmlChanged = true;
		}
	}
}

class XmlViewerNode extends DefaultMutableTreeNode {
	public XmlViewerNode(Node node) {
		super(node);
	}

	public Node getXmlNode() {
		Object obj = getUserObject();
		if (obj instanceof Node)
			return (Node)obj;
		return null;
	}

	public String toString () {
		Node node = getXmlNode();
		if (node == null)
			return getUserObject().toString();
		StringBuffer sb = new StringBuffer();
		switch (node.getNodeType()) {
		case Node.ELEMENT_NODE:
			sb.append('<');
			sb.append(node.getNodeName());
			sb.append('>');
			break;
		case Node.TEXT_NODE:
			sb.append(node.getNodeValue());
			break;
		}
		return sb.toString();
	}
}

class SimpleFilter
	extends javax.swing.filechooser.FileFilter {

	private String m_description = null;
	private String m_extension = null;

	public SimpleFilter(String extension, String description) {
		m_description = description;
		m_extension = "."+extension.toLowerCase();
	}

		public String getDescription() {
		return m_description;
	}

	public boolean accept(File f) {
		if (f == null)
			return false;
		if (f.isDirectory())
			return true;
		return f.getName().toLowerCase().endsWith(m_extension);
	}
}

// NEW
class XMLRoutines {

	public static void write(Document doc, Writer out) throws Exception {
		write(doc.getDocumentElement(), out);
	}

	public static void write(Node node, Writer out) throws Exception {
		if (node==null || out==null)
			return;

		int type = node.getNodeType();
		switch (type) {
		case Node.DOCUMENT_NODE:
			write(((Document)node).getDocumentElement(), out);
			out.flush();
			break;

		case Node.ELEMENT_NODE:
			out.write('<');
			out.write(node.getNodeName());
			NamedNodeMap attrs = node.getAttributes();
			for (int k = 0; k< attrs.getLength(); k++ ) {
				Node attr = attrs.item(k);
				out.write(' ');
				out.write(attr.getNodeName());
				out.write("=\"");
				out.write(attr.getNodeValue());
				out.write('"');
			}
			out.write('>');
			break;

		case Node.ENTITY_REFERENCE_NODE:
			out.write('&');
			out.write(node.getNodeName());
			out.write(';');
			break;

		// print cdata sections
		case Node.CDATA_SECTION_NODE:
			out.write("<![CDATA[");
			out.write(node.getNodeValue());
			out.write("]]>");
			break;

		// print text
		case Node.TEXT_NODE:
			out.write(node.getNodeValue());
			break;

		// print processing instruction
		case Node.PROCESSING_INSTRUCTION_NODE:
			out.write("<?");
			out.write(node.getNodeName());
			String data = node.getNodeValue();
			if ( data != null && data.length() > 0 ) {
				out.write(' ');
				out.write(data);
			}
			out.write("?>");
			break;

		default:
			out.write("<TYPE="+type);
			out.write(node.getNodeName());
			out.write("?>");
			break;
		}

		NodeList children = node.getChildNodes();
		if ( children != null ) {
	 		for ( int k = 0; k<children.getLength(); k++ ) {
				write(children.item(k), out);
			}
		}

		if (node.getNodeType() == Node.ELEMENT_NODE ) {
			out.write("</");
			out.write(node.getNodeName());
			out.write('>');
		}
		out.flush();
	}
}

