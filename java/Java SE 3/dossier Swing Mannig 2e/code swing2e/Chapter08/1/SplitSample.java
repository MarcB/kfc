/** 
 *  Copyright 1999-2002 Matthew Robinson and Pavel Vorobiev. 
 *  All Rights Reserved. 
 * 
 *  =================================================== 
 *  This program contains code from the book "Swing" 
 *  2nd Edition by Matthew Robinson and Pavel Vorobiev 
 *  http://www.spindoczine.com/sbe 
 *  =================================================== 
 * 
 *  The above paragraph must be included in full, unmodified 
 *  and completely intact in the beginning of any source code 
 *  file that references, copies or uses (in any way, shape 
 *  or form) code contained in this file. 
 */ 


import java.awt.*;
import javax.swing.*;

public class SplitSample
	extends JFrame {

	public SplitSample() {
		super("Simple SplitSample Example");
		setSize(400, 400);
		getContentPane().setLayout(new BorderLayout());

		Component c11 = new SimplePanel();
		Component c12 = new SimplePanel();
		JSplitPane spLeft = new JSplitPane(
			JSplitPane.VERTICAL_SPLIT, c11, c12);
		spLeft.setDividerSize(8);
		spLeft.setDividerLocation(150);
		spLeft.setContinuousLayout(true);

		Component c21 = new SimplePanel();
		Component c22 = new SimplePanel();
		JSplitPane spRight = new JSplitPane(
			JSplitPane.VERTICAL_SPLIT, c21, c22);
		spRight.setDividerSize(8);
		spRight.setDividerLocation(150);
		spRight.setContinuousLayout(true);

		JSplitPane sp = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
			spLeft, spRight);
		sp.setDividerSize(8);
		sp.setDividerLocation(200);
		sp.setResizeWeight(0.5);	// 1.3
		sp.setContinuousLayout(false);
		sp.setOneTouchExpandable(true);

		getContentPane().add(sp, BorderLayout.CENTER);
	}

	public static void main(String argv[]) {
		SplitSample frame = new SplitSample();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}

class SimplePanel
	extends JPanel {

	public SimplePanel() {
		setBackground(Color.white);
	}

	public Dimension getPreferredSize() {
		return new Dimension(200, 200);
	}

	public Dimension getMinimumSize() {
		return new Dimension(40, 40);
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.black);
		Dimension sz = getSize();
		g.drawLine(0, 0, sz.width, sz.height);
		g.drawLine(sz.width, 0, 0, sz.height);
	}
}
