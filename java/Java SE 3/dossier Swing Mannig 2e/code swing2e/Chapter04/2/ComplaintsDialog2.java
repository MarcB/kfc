/** 
 *  Copyright 1999-2002 Matthew Robinson and Pavel Vorobiev. 
 *  All Rights Reserved. 
 * 
 *  =================================================== 
 *  This program contains code from the book "Swing" 
 *  2nd Edition by Matthew Robinson and Pavel Vorobiev 
 *  http://www.spindoczine.com/sbe 
 *  =================================================== 
 * 
 *  The above paragraph must be included in full, unmodified 
 *  and completely intact in the beginning of any source code 
 *  file that references, copies or uses (in any way, shape 
 *  or form) code contained in this file. 
 */ 

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.*;

public class ComplaintsDialog2 extends JDialog
{
   public ComplaintsDialog2( JFrame frame )
   {
      super( frame, true );
      setTitle( "Simple Complaints Dialog" );
      setSize( 500, 300 );

      // Creates the helper class panel to hold all my components
      GriddedPanel panel = new GriddedPanel();
      // give the panel a border gap of 5 pixels
      panel.setBorder( new EmptyBorder( new Insets( 5, 5, 5, 5 ) ) );
      getContentPane().add( BorderLayout.CENTER, panel );
      
      // Define preferred sizes for my entry fields
      Dimension shortField = new Dimension( 40, 20 );
      Dimension mediumField = new Dimension( 120, 20 );
      Dimension longField = new Dimension( 240, 20 );
      Dimension hugeField = new Dimension( 240, 80 );

      // Spacing between the label and the field
      EmptyBorder border = new EmptyBorder( new Insets( 0, 0, 0, 10 ) );
      EmptyBorder border1 = new EmptyBorder( new Insets( 0, 20, 0, 10 ) );

      // Short description label and field
      JLabel lbl1 = new JLabel( "Short Description" );
      lbl1.setBorder( border ); // add some space on the right
      panel.addComponent( lbl1, 1, 1 );
      JTextField txt1 = new JTextField();
      txt1.setPreferredSize( longField );
      panel.addFilledComponent( txt1, 1, 2, 3, 1, GridBagConstraints.HORIZONTAL );

      // Description label and field
      JLabel lbl2 = new JLabel( "Description" );
      lbl2.setBorder( border );
      panel.addComponent( lbl2, 2, 1 );
      JTextArea area1 = new JTextArea();
      JScrollPane scroll = new JScrollPane( area1 );
      scroll.setPreferredSize( hugeField );
      panel.addFilledComponent( scroll, 2, 2, 3, 2, GridBagConstraints.BOTH );

      // Severity label and combo box
      JLabel lbl3 = new JLabel( "Severity" );
      lbl3.setBorder( border );
      panel.addComponent( lbl3, 4, 1 );
      JComboBox combo3 = new JComboBox();
      combo3.addItem( "A" );
      combo3.addItem( "B" );
      combo3.addItem( "C" );
      combo3.addItem( "D" );
      combo3.addItem( "E" );
      combo3.setPreferredSize( shortField );
      panel.addComponent( combo3, 4, 2 );

      // Priority label and combo box
      JLabel lbl4 = new JLabel( "Priority" );
      lbl4.setBorder( border1 );
      panel.addComponent( lbl4, 4, 3 );
      JComboBox combo4 = new JComboBox();
      combo4.addItem( "1" );
      combo4.addItem( "2" );
      combo4.addItem( "3" );
      combo4.addItem( "4" );
      combo4.addItem( "5" );
      combo4.setPreferredSize( shortField );
      panel.addComponent( combo4, 4, 4 );

      // Name label and text field
      JLabel lbl5 = new JLabel( "Name" );
      lbl5.setBorder( border );
      panel.addComponent( lbl5, 5, 1 );
      JTextField txt5 = new JTextField();
      txt5.setPreferredSize( longField );
      panel.addComponent( txt5, 5, 2, 3, 1 );

      // Telephone label and text field
      JLabel lbl6 = new JLabel( "Telephone" );
      lbl6.setBorder( border );
      panel.addComponent( lbl6, 6, 1 );
      JTextField txt6 = new JTextField();
      txt6.setPreferredSize( mediumField );
      panel.addComponent( txt6, 6, 2, 3, 1 );

      // Sex label and radio button
      JLabel lbl7 = new JLabel( "Sex" );
      lbl7.setBorder( border );
      panel.addComponent( lbl7, 7, 1 );
      JPanel radioPanel = new JPanel();
      // Creates a FlowLayout layout JPanel with 5 pixel of horizontal gaps
      // and no vertical gaps
      radioPanel.setLayout( new FlowLayout( FlowLayout.LEFT, 5, 0 ) );
      ButtonGroup group = new ButtonGroup();
      JRadioButton radio1 = new JRadioButton( "Male" );
      radio1.setSelected( true );
      group.add( radio1 );
      JRadioButton radio2 = new JRadioButton( "Female" );
      group.add( radio2 );
      radioPanel.add( radio1 );
      radioPanel.add( radio2 );
      panel.addComponent( radioPanel, 7, 2, 3, 1 );

      // ID Number label and text field
      JLabel lbl8 = new JLabel( "ID Number" );
      lbl8.setBorder( border );
      panel.addComponent( lbl8, 8, 1 );
      JTextField txt8 = new JTextField();
      txt8.setPreferredSize( mediumField );
      panel.addComponent( txt8, 8, 2, 3, 1 );

      // Okay button
      JButton submitBtn = new JButton( "Submit" );
      panel.addFilledComponent( submitBtn, 1, 5 );

      // Cancel button
      JButton cancelBtn = new JButton( "Cancel" );
      panel.addFilledComponent( cancelBtn, 2, 5 );

      // Help button
      JButton helpBtn = new JButton( "Help" );
      panel.addComponent( helpBtn, 3, 5, 1, 1, GridBagConstraints.NORTH,
                          GridBagConstraints.HORIZONTAL );

      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

      setVisible( true );
   }

   public static void main( String[] args )
   {
      new ComplaintsDialog2( new JFrame() );
   }
}
