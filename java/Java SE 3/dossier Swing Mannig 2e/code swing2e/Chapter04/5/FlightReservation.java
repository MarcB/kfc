/** 
 *  Copyright 1999-2002 Matthew Robinson and Pavel Vorobiev. 
 *  All Rights Reserved. 
 * 
 *  =================================================== 
 *  This program contains code from the book "Swing" 
 *  2nd Edition by Matthew Robinson and Pavel Vorobiev 
 *  http://www.spindoczine.com/sbe 
 *  =================================================== 
 * 
 *  The above paragraph must be included in full, unmodified 
 *  and completely intact in the beginning of any source code 
 *  file that references, copies or uses (in any way, shape 
 *  or form) code contained in this file. 
 */ 

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import dl.*;

public class FlightReservation extends JFrame {

  public FlightReservation() {
    super("Flight Reservation Dialog [Custom Layout - 2]");

    JPanel c = new JPanel(new DialogLayout2(20, 5));
    c.setBorder(new EmptyBorder(10, 10, 10, 10));

    c.add(new JLabel("Date:"));
    c.add(new JTextField());
        
    c.add(new JLabel("From:"));
    JComboBox cb1 = new JComboBox();
    cb1.addItem("New York");
    c.add(cb1);

    c.add(new JLabel("To:"));
    JComboBox cb2 = new JComboBox();
    cb2.addItem("London");
    c.add(cb2);

    c.add(new DialogSeparator("Available Flights"));
    JList list = new JList();
    JScrollPane ps = new JScrollPane(list);
    c.add(ps);
        
    c.add(new DialogSeparator("Options"));

    ButtonGroup group = new ButtonGroup();
    JRadioButton r1 = new JRadioButton("First class");
    group.add(r1);
    c.add(r1);

    JRadioButton r2 = new JRadioButton("Business");
    group.add(r2);
    c.add(r2);

    JRadioButton r3 = new JRadioButton("Coach");
    group.add(r3);
    c.add(r3);
        
    c.add(new DialogSeparator());
        
    JButton b1 = new JButton("Search");
    c.add(b1);
        
    JButton b2 = new JButton("Purchase");
    c.add(b2);
        
    JButton b3 = new JButton("Exit");
    c.add(b3);
    
    getContentPane().add(c, BorderLayout.CENTER);
    pack();	// added - Pavel
  }

  public static void main(String argv[]) {
    FlightReservation frame = new FlightReservation();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setVisible(true);
  }
}
