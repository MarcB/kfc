/** 
 *  Copyright 1999-2002 Matthew Robinson and Pavel Vorobiev. 
 *  All Rights Reserved. 
 * 
 *  =================================================== 
 *  This program contains code from the book "Swing" 
 *  2nd Edition by Matthew Robinson and Pavel Vorobiev 
 *  http://www.spindoczine.com/sbe 
 *  =================================================== 
 * 
 *  The above paragraph must be included in full, unmodified 
 *  and completely intact in the beginning of any source code 
 *  file that references, copies or uses (in any way, shape 
 *  or form) code contained in this file. 
 */ 

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;
import java.net.*;
import java.lang.reflect.*;

import sun.net.ftp.*;
import sun.net.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import dl.*;

public class FTPApp extends JFrame {

	public static int BUFFER_SIZE = 2048;

	protected JTextField m_txtUser;
	protected JPasswordField m_txtPassword;
	protected JTextField m_txtURL;
	protected JTextField m_txtFile;
	protected JTextArea	m_monitor;
	protected JProgressBar m_progress;
	protected JButton m_btPut;
	protected JButton m_btGet;
	protected JButton m_btFile;
	protected JButton m_btClose;
	protected JFileChooser m_chooser;

	protected FtpClient m_client;
	protected String m_sLocalFile;
	protected String m_sHostFile;

	public FTPApp() {
		super("FTP Client");

		JPanel p = new JPanel();
		p.setLayout(new DialogLayout2(10, 5));
		p.setBorder(new EmptyBorder(10, 10, 10, 10));

		p.add(new JLabel("User name:"));
		m_txtUser = new JTextField("anonymous", 20);
		p.add(m_txtUser);

		p.add(new JLabel("Password:"));
		m_txtPassword = new JPasswordField(20);
		p.add(m_txtPassword);

		p.add(new JLabel("URL:"));
		m_txtURL = new JTextField(20);
		p.add(m_txtURL);

		p.add(new JLabel("Destination file:"));
		m_txtFile = new JTextField(20);
		p.add(m_txtFile);

		JPanel pp = new JPanel(new DialogLayout2(10, 5));
	pp.setBorder(new CompoundBorder(
		new TitledBorder(new EtchedBorder(), "Connection Monitor"),
		new EmptyBorder(3, 5, 3, 5))
	);

		m_monitor = new JTextArea(5, 20);
		m_monitor.setEditable(false);
	m_monitor.setLineWrap(true);
	m_monitor.setWrapStyleWord(true);
		JScrollPane ps = new JScrollPane(m_monitor);
		pp.add(ps);

		m_progress = new JProgressBar();
		m_progress.setStringPainted(true);
		m_progress.setBorder(new BevelBorder(BevelBorder.LOWERED,
			Color.white, Color.gray));
		m_progress.setMinimum(0);
		JPanel p1 = new JPanel(new BorderLayout());
		p1.add(m_progress, BorderLayout.CENTER);
		pp.add(p1);
		p.add(pp);

		m_btPut = new JButton("Put");
		ActionListener lst = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					Thread uploader = new Thread() {
						public void run() {
				if (connect())
					putFile();
				disconnect();
			}
					};
					uploader.start();
			}
		};
		m_btPut.addActionListener(lst);
		m_btPut.setMnemonic('p');
		p.add(m_btPut);

		m_btGet = new JButton("Get");
		lst = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					Thread downloader = new Thread() {
						public void run() {
						if (connect())
								getFile();
							disconnect();
			}
					};
					downloader.start();
			}
		};
		m_btGet.addActionListener(lst);
		m_btGet.setMnemonic('g');
		p.add(m_btGet);

		m_btFile = new JButton("File");
		lst = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (m_chooser.showSaveDialog(FTPApp.this) !=
					JFileChooser.APPROVE_OPTION)
						return;
				File f = m_chooser.getSelectedFile();
				m_txtFile.setText(f.getPath());
			}
		};
		m_btFile.addActionListener(lst);
		m_btFile.setMnemonic('f');
		p.add(m_btFile);

		m_btClose = new JButton("Close");
		lst = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (m_client != null)
					disconnect();
				else
					System.exit(0);
			}
		};
		m_btClose.addActionListener(lst);
		m_btClose.setDefaultCapable(true);
		m_btClose.setMnemonic('g');
		p.add(m_btClose);

		getContentPane().add(p, BorderLayout.CENTER);
		pack();

		m_chooser = new JFileChooser();
		m_chooser.setDialogTitle(
			"Select File For Upload/Download");
		try {
			File dir = (new File(".")).getCanonicalFile();
			m_chooser.setCurrentDirectory(dir);
		} catch (IOException ex) {}
	}

	protected void setButtonStates(boolean state) {
		m_btPut.setEnabled(state);
		m_btGet.setEnabled(state);
		m_btFile.setEnabled(state);
	}

	protected boolean connect() {
	// Input validation
		String user = m_txtUser.getText();
		if (user.length()==0) {
			message("Please enter user name");
			return false;
		}
		String password = new String(m_txtPassword.getPassword());
		String sUrl = m_txtURL.getText();
		if (sUrl.length()==0) {
			message("Please enter URL");
			return false;
		}
		m_sLocalFile = m_txtFile.getText();
		if (m_sLocalFile.length()==0) {
			message("Please enter local file name");
			return false;
		}

		// Parse URL
		int index = sUrl.indexOf("//");
		if (index >= 0)
		sUrl = sUrl.substring(index+2);

		index = sUrl.indexOf("/");
		String host = sUrl.substring(0, index);
		sUrl = sUrl.substring(index+1);

		String sDir = "";
		index = sUrl.lastIndexOf("/");
		if (index >= 0) {
			sDir = sUrl.substring(0, index);
			sUrl = sUrl.substring(index+1);
		}
		m_sHostFile = sUrl;

		m_monitor.setText("");
		setButtonStates(false);
		m_btClose.setText("Cancel");
		setCursor(Cursor.getPredefinedCursor(
			Cursor.WAIT_CURSOR));

		try {
 		m_progress.setIndeterminate(true);	// 1.4
			message("Connecting to host "+host);
			m_client = new FtpClient(host);
			m_client.login(user, password);
			message("User "+user+" login OK");
			message(m_client.welcomeMsg);
			m_client.cd(sDir);
			message("Directory: "+sDir);
			m_client.binary();
			return true;
		}
		catch (Exception ex) {
			message("Error: "+ex.toString());
			ex.printStackTrace();
			setButtonStates(true);
			return false;
		}
		finally {
 		m_progress.setIndeterminate(false);	// 1.4
		}
	}

	protected void disconnect() {
		if (m_client != null) {
			try { m_client.closeServer(); }
			catch (IOException ex) {}
			m_client = null;
		}
	m_progress.setValue(0);
	setButtonStates(true);
	m_btClose.setText("Close");
	validate();
	setCursor(Cursor.getPredefinedCursor(
		Cursor.DEFAULT_CURSOR));
	}

	protected void getFile() {
		byte[] buffer = new byte[BUFFER_SIZE];
		try {
			int size = getFileSize(m_client, m_sHostFile);
			if (size > 0) {
				message("File " + m_sHostFile + ": " + size + " bytes");
				setProgressMaximum(size);
			}
			else
				message("File " + m_sHostFile + ": size unknown");

			File output = new File(m_sLocalFile);
			message("Output to "+output);
			FileOutputStream out = new FileOutputStream(output);

			InputStream in = m_client.get(m_sHostFile);
			int counter = 0;
			while(true) {
				int bytes = in.read(buffer);
				if (bytes < 0)
					break;

				out.write(buffer, 0, bytes);
				counter += bytes;
				if (size > 0) {
					setProgressValue(counter);
					int proc = (int) Math.round(m_progress.
						getPercentComplete() * 100);
					setProgressString(proc + " %");
				}
				else {
					int kb = counter/1024;
					setProgressString(kb + " KB");
				}
			}
			out.close();
			in.close();
		}
		catch (Exception ex) {
			message("Error: "+ex.toString());
			ex.printStackTrace();
	 }
	}

	protected void putFile() {
		byte[] buffer = new byte[BUFFER_SIZE];
		try {
			File f = new File(m_sLocalFile);
			int size = (int)f.length();
			message("File " + m_sLocalFile + ": " + size + " bytes");
			setProgressMaximum (size);

			File input = new File(m_sLocalFile);
			FileInputStream in = new
				FileInputStream(input);
			OutputStream out = m_client.put(m_sHostFile);

			int counter = 0;
			while(true) {
				int bytes = in.read(buffer);
				if (bytes < 0)
					break;
				out.write(buffer, 0, bytes);
				counter += bytes;
				setProgressValue(counter);
				int proc = (int) Math.round(m_progress.
					getPercentComplete() * 100);
				setProgressString(proc + " %");
			}

			out.close();
			in.close();
		}
		catch (Exception ex) {
			message("Error: " + ex.toString());
			ex.printStackTrace();
		}
	}

	protected void message(final String str) {
		if (str != null) {
			Runnable runner = new Runnable() {
				public void run() {
					m_monitor.append(str + '\n');
					m_monitor.repaint();
				}
			};
			SwingUtilities.invokeLater(runner);
		}
	}

	protected void setProgressValue(final int value) {
		Runnable runner = new Runnable() {
			public void run() {
				m_progress.setValue(value);
			}
		};
		SwingUtilities.invokeLater(runner);
	}

	protected void setProgressMaximum(final int value) {
		Runnable runner = new Runnable() {
			public void run() {
				m_progress.setMaximum(value);
			}
		};
		SwingUtilities.invokeLater(runner);
	}

	protected void setProgressString(final String string) {
		Runnable runner = new Runnable() {
			public void run() {
				m_progress.setString(string);
			}
		};
		SwingUtilities.invokeLater(runner);
	}

	public static void main(String argv[]) {
	FTPApp frame = new FTPApp();
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	frame.setVisible(true);
	}

	public static int getFileSize(FtpClient client, String fileName)
	 throws IOException {
		TelnetInputStream lst = client.list();
		String str = "";
		fileName = fileName.toLowerCase();
		while(true) {
			int c = lst.read();
			char ch = (char)c;
			if (c < 0 || ch == '\n') {
				str = str.toLowerCase();
				if (str.indexOf(fileName) >= 0) {
					StringTokenizer tk = new StringTokenizer(str);
					int index = 0;
					while(tk.hasMoreTokens()) {
						String token = tk.nextToken();
						if (index == 4)
							try {
								return Integer.parseInt(token);
							}
							catch (NumberFormatException ex) {
								return -1;
							}
							index++;
					}
				}
				str = "";
			}
			if (c <= 0)
				break;
			str += ch;
		}
		return -1;
	}
}
