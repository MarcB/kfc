/** 
 *  Copyright 1999-2002 Matthew Robinson and Pavel Vorobiev. 
 *  All Rights Reserved. 
 * 
 *  =================================================== 
 *  This program contains code from the book "Swing" 
 *  2nd Edition by Matthew Robinson and Pavel Vorobiev 
 *  http://www.spindoczine.com/sbe 
 *  =================================================== 
 * 
 *  The above paragraph must be included in full, unmodified 
 *  and completely intact in the beginning of any source code 
 *  file that references, copies or uses (in any way, shape 
 *  or form) code contained in this file. 
 */ 

/** 
 * Image of earth from http://earth.jsc.nasa.gov/ 
 */

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.util.*;
import java.io.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.filechooser.*;

import com.sun.image.codec.jpeg.*;

public class JPEGEditor extends JFrame {

	public final static Dimension VERTICAL_RIGID_SIZE
		= new Dimension(1,3);
	public final static Dimension HORIZONTAL_RIGID_SIZE
		= new Dimension(3,1);

	protected File m_currentDir;
	protected File m_currentFile;

	protected JFileChooser m_chooser;
	protected JPEGPanel m_panel;
	protected JSlider m_slHorzDensity;
	protected JSlider m_slVertDensity;
	protected JSlider m_slQuality;

	protected BufferedImage m_bi1, m_bi2;

	public JPEGEditor() {
		super("JPEG Editor");
		setSize(600, 400);

		m_chooser = new JFileChooser();
		m_chooser.setFileFilter(new SimpleFilter("jpg",
			"JPEG Image Files"));
		try {
			m_currentDir = (new File(".")).getCanonicalFile();
		} catch (IOException ex) {}

		m_panel = new JPEGPanel();
		JScrollPane ps = new JScrollPane(m_panel,
			JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
			JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		getContentPane().add(ps, BorderLayout.CENTER);

		JPanel p;
		JPanel p1 = new JPanel(new GridLayout(1, 2, 10, 10));

		m_slVertDensity = new JSlider(JSlider.VERTICAL,
			100, 500, 300);
		m_slVertDensity.setExtent(50);
		m_slVertDensity.setPaintLabels(true);
		m_slVertDensity.setMajorTickSpacing(100);
		m_slVertDensity.setMinorTickSpacing(50);
		m_slVertDensity.setPaintTicks(true);
		m_slVertDensity.putClientProperty("JSlider.isFilled", Boolean.TRUE);

		p = new JPanel();
		p.setBorder(new TitledBorder(new EtchedBorder(),
			"Vert. dens."));
		p.add(Box.createRigidArea(HORIZONTAL_RIGID_SIZE));
		p.add(m_slVertDensity);
		p.add(Box.createRigidArea(HORIZONTAL_RIGID_SIZE));
		getContentPane().add(p, BorderLayout.EAST);

		m_slHorzDensity = new JSlider(JSlider.HORIZONTAL,
			100, 500, 300);
		m_slHorzDensity.setExtent(50);
		m_slHorzDensity.setPaintLabels(true);
		m_slHorzDensity.setMajorTickSpacing(100);
		m_slHorzDensity.setMinorTickSpacing(50);
		m_slHorzDensity.setPaintTicks(true);
		m_slHorzDensity.putClientProperty("JSlider.isFilled", Boolean.TRUE);

		p = new JPanel();
		p.setBorder(new TitledBorder(new EtchedBorder(),
			"Horizontal density"));
		p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
		p.add(Box.createRigidArea(VERTICAL_RIGID_SIZE));
		p.add(m_slHorzDensity);
		p.add(Box.createRigidArea(VERTICAL_RIGID_SIZE));
		p.setLayout(new BoxLayout(p, BoxLayout.X_AXIS));
		p1.add(p);

		m_slQuality = new JSlider(JSlider.HORIZONTAL,
			0, 100, 100);
		Hashtable labels = new Hashtable(6);
		for (float q = 0; q <= 1.0; q += 0.2)
			labels.put(new Integer((int)(q*100)),
			new JLabel("" + q, JLabel.CENTER ));
		m_slQuality.setLabelTable(labels);
		m_slQuality.setExtent(10);
		m_slQuality.setPaintLabels(true);
		m_slQuality.setMinorTickSpacing(10);
		m_slQuality.setPaintTicks(true);
		m_slQuality.putClientProperty("JSlider.isFilled", Boolean.TRUE);

		p = new JPanel();
		p.setBorder(new TitledBorder(new EtchedBorder(),
			"Quality"));
		p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
		p.add(Box.createRigidArea(VERTICAL_RIGID_SIZE));
		p.add(m_slQuality);
		p.add(Box.createRigidArea(VERTICAL_RIGID_SIZE));
		p1.add(p);
		getContentPane().add(p1, BorderLayout.SOUTH);

	JToolBar tb = createToolbar();
	getContentPane().add(tb, BorderLayout.NORTH);
	}

	protected JToolBar createToolbar() {
	JToolBar tb = new JToolBar();
	tb.setFloatable(false);

	JButton bt = new JButton(new ImageIcon("Open24.gif"));
	bt.setToolTipText("Open JPEG file");
		ActionListener lst = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			m_chooser.setCurrentDirectory(m_currentDir);	// 1.4 bug: will not show files immediately
			m_chooser.rescanCurrentDirectory();
				int result = m_chooser.showOpenDialog(JPEGEditor.this);
				repaint();
				if (result != JFileChooser.APPROVE_OPTION)
						return;
				m_currentDir = m_chooser.getCurrentDirectory();
				File fChoosen = m_chooser.getSelectedFile();
				openFile(fChoosen);
			}
		};
		bt.addActionListener(lst);
		tb.add(bt);

	bt = new JButton(new ImageIcon("Save24.gif"));
	bt.setToolTipText("Save changes to current file");
		lst = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				saveFile(m_currentFile);
			}
		};
		bt.addActionListener(lst);
		tb.add(bt);

	bt = new JButton(new ImageIcon("SaveAs24.gif"));
	bt.setToolTipText("Save changes to another file");
		lst = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
		if (m_currentFile == null || m_panel.getBufferedImage() == null)
		 return;
			m_chooser.setCurrentDirectory(m_currentDir);
			m_chooser.rescanCurrentDirectory();
				int result = m_chooser.showSaveDialog(JPEGEditor.this);
				repaint();
				if (result != JFileChooser.APPROVE_OPTION)
						return;
				m_currentDir = m_chooser.getCurrentDirectory();
				File fChoosen = m_chooser.getSelectedFile();
				if (fChoosen!=null && fChoosen.exists()) {
					String message = "File " + fChoosen.getName()+
						" already exists. Override?";
					int result2 = JOptionPane.showConfirmDialog(
						JPEGEditor.this, message, getTitle(),
						JOptionPane.YES_NO_OPTION);
					if (result2 != JOptionPane.YES_OPTION)
						return;
				}
				setCurrentFile(fChoosen);
				saveFile(fChoosen);
			}
		};
		bt.addActionListener(lst);
		tb.add(bt);

		tb.addSeparator();
		JButton btApply = new JButton("Apply");
		lst = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				apply();
			}
		};
		btApply.addActionListener(lst);

		btApply.setMinimumSize(btApply.getPreferredSize());
	btApply.setMaximumSize(btApply.getPreferredSize());
		tb.add(btApply);

		tb.addSeparator();
		JButton btReset = new JButton("Reset");
		lst = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				reset();
			}
		};
		btReset.addActionListener(lst);

		btReset.setMinimumSize(btReset.getPreferredSize());
	btReset.setMaximumSize(btReset.getPreferredSize());
		tb.add(btReset);

		return tb;
	}

	protected void setCurrentFile(File file) {
		if (file != null) {
			m_currentFile = file;
			setTitle("JPEG Editor ["+file.getName()+"]");
		}
	}

	protected void openFile(final File file) {
		if (file == null || !file.exists())
			return;
		setCurrentFile(file);

		setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		Thread runner = new Thread() {
			public void run() {
				try {
					FileInputStream in = new FileInputStream(file);
					JPEGImageDecoder decoder =
						JPEGCodec.createJPEGDecoder(in);
					m_bi1 = decoder.decodeAsBufferedImage();
					m_bi2 = null;
					in.close();
					SwingUtilities.invokeLater( new Runnable() {
						public void run() { reset(); }
					});
				}
				catch (Exception ex) {
					ex.printStackTrace();
					System.err.println("openFile: "+ex.toString());
				}
				setCursor(Cursor.getPredefinedCursor(
					Cursor.DEFAULT_CURSOR));
			}
		};
		runner.start();
	}

	protected void saveFile(final File file) {
		if (file == null || m_panel.getBufferedImage() == null)
			return;

		setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		Thread runner = new Thread() {
			public void run() {
				try {
					FileOutputStream out = new FileOutputStream(file);
					JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
					encoder.encode(m_panel.getBufferedImage());
					out.close();
				}
				catch (Exception ex) {
					ex.printStackTrace();
					System.err.println("apply: "+ex.toString());
				}
				setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
			}
		};
		runner.start();
	}

	protected void apply() {
		if (m_bi1 == null)
			return;

		setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		Thread runner = new Thread() {
			public void run() {
				try {
					ByteArrayOutputStream out = new ByteArrayOutputStream();
					JPEGImageEncoder encoder =
						JPEGCodec.createJPEGEncoder(out);
					JPEGEncodeParam param =
						encoder.getDefaultJPEGEncodeParam(m_bi1);

					float quality = m_slQuality.getValue()/100.0f;
					param.setQuality(quality, false);

					param.setDensityUnit(
						JPEGEncodeParam.DENSITY_UNIT_DOTS_INCH);
					int xDensity = m_slHorzDensity.getValue();
					param.setXDensity(xDensity);
					int yDensity = m_slVertDensity.getValue();
					param.setYDensity(yDensity);

					encoder.setJPEGEncodeParam(param);
					encoder.encode(m_bi1);

					ByteArrayInputStream in = new ByteArrayInputStream(
						out.toByteArray());
					JPEGImageDecoder decoder =
						JPEGCodec.createJPEGDecoder(in);
					final BufferedImage bi2 = decoder.decodeAsBufferedImage();
					SwingUtilities.invokeLater( new Runnable() {
						public void run() {
							m_panel.setBufferedImage(bi2);
						}
					});
				}
				catch (Exception ex) {
					ex.printStackTrace();
					System.err.println("apply: "+ex.toString());
				}
				setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
			}
		};
		runner.start();
	}

	protected void reset() {
		if (m_bi1 != null) {
			m_panel.setBufferedImage(m_bi1);
			m_slQuality.setValue(100);
			m_slHorzDensity.setValue(300);
			m_slVertDensity.setValue(300);
		}
	}

	public static void main(String argv[]) {
	JPEGEditor frame = new JPEGEditor();
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	frame.setVisible(true);
	}
}

class JPEGPanel extends JPanel {

	protected BufferedImage m_bi = null;

	public void setBufferedImage(BufferedImage bi) {
		if (bi == null)
			return;
		m_bi = bi;
		Dimension d = new Dimension(m_bi.getWidth(this),
			m_bi.getHeight(this));
		setPreferredSize(d);
		revalidate();
		repaint();
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Dimension d = getSize();
		g.setColor(getBackground());
		g.fillRect(0, 0, d.width, d.height);
		if (m_bi != null)
			g.drawImage(m_bi, 0, 0, this);
	}

	public BufferedImage getBufferedImage() {
		return m_bi;
	}
}


class SimpleFilter
	extends javax.swing.filechooser.FileFilter {

	private String m_description = null;
	private String m_extension = null;

	public SimpleFilter(String extension, String description) {
		m_description = description;
		m_extension = "."+extension.toLowerCase();
	}

		public String getDescription() {
		return m_description;
	}

	public boolean accept(File f) {
		if (f == null)
			return false;
		if (f.isDirectory())
			return true;
		return f.getName().toLowerCase().endsWith(m_extension);
	}
}
