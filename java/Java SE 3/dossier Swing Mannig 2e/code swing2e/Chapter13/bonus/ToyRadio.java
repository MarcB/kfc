/** 
 *  Copyright 1999-2002 Matthew Robinson and Pavel Vorobiev. 
 *  All Rights Reserved. 
 * 
 *  =================================================== 
 *  This program contains code from the book "Swing" 
 *  2nd Edition by Matthew Robinson and Pavel Vorobiev 
 *  http://www.spindoczine.com/sbe 
 *  =================================================== 
 * 
 *  The above paragraph must be included in full, unmodified 
 *  and completely intact in the beginning of any source code 
 *  file that references, copies or uses (in any way, shape 
 *  or form) code contained in this file. 
 */ 

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.applet.*;
import java.net.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

public class ToyRadio 
	extends JApplet
{
	public final static Dimension VERTICAL_RIGID_SIZE = new Dimension(1,3);
	public final static Dimension HORIZONTAL_RIGID_SIZE = new Dimension(14,1);

	public final static int STATION_WIDTH = 1;

	protected static ImageIcon INACTIVE_RADIO_IMAGE = 
		new ImageIcon("radio1.gif");
	protected static ImageIcon ACTIVE_RADIO_IMAGE = 
		new ImageIcon("radio2.gif");

	protected RadioStation[] m_stations = null;
	protected RadioStation m_currentStation = null;

	protected JLabel  m_icon;
	protected JLabel  m_indicator;
	protected JSlider m_frequency;

	public ToyRadio() 
	{
	}

	public synchronized void init()
	{
		m_stations = new RadioStation[] {
			new RadioStation(83, "fired", "fired.wav"),
			new RadioStation(87, "groovy stuff", "groovy.wav"),
			new RadioStation(93, "the holdup", "momgod.wav"),
			new RadioStation(97, "wait", 
				"putogthr.wav"),
			new RadioStation(103, "star command", "starcomd.wav"),
			new RadioStation(106, "useless fact", "uslsfact.wav"),
			new RadioStation(109, "angry", "dontmake.wav")
		};

		getContentPane().setLayout(new FlowLayout());

		JPanel p0 = new JPanel();
		p0.setLayout(new BoxLayout(p0, BoxLayout.X_AXIS));

		JPanel p1 = new JPanel();
		p1.setLayout(new GridLayout(3, 1));

		JPanel p = new JPanel();
		p.setLayout(new BoxLayout(p, BoxLayout.X_AXIS));

		p.add(Box.createRigidArea(HORIZONTAL_RIGID_SIZE));
		m_icon = new JLabel(INACTIVE_RADIO_IMAGE);
		p.add(m_icon);
		p.add(Box.createRigidArea(HORIZONTAL_RIGID_SIZE));

		m_indicator = new JLabel("101.5 FM");
		m_indicator.setFont(new Font("Arial",Font.BOLD,24));
		m_indicator.setBorder(new OvalBorder());
		p.add(m_indicator);

		p.setBorder(new TitledBorder(new OvalBorder(), "Status"));
		p1.add(p);

		m_frequency = new JSlider(JSlider.HORIZONTAL, 80, 110, 101);
		m_frequency.setPaintLabels(true);
		m_frequency.setMajorTickSpacing(5);
		FreqListener lst = new FreqListener();
		m_frequency.addChangeListener(lst);

		p = new JPanel();
		p.setBorder(new TitledBorder(new OvalBorder(), 
			"Frequency"));
		p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
		p.add(Box.createRigidArea(VERTICAL_RIGID_SIZE));
		p.add(m_frequency);
		p.add(Box.createRigidArea(VERTICAL_RIGID_SIZE));
		p1.add(p);

		p = new JPanel();
		p.setBorder(new TitledBorder(new OvalBorder(), 
			"Favorites"));
		p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
		p.add(Box.createRigidArea(VERTICAL_RIGID_SIZE));

		JPanel p2 = new JPanel();
		p2.setLayout(new BoxLayout(p2, BoxLayout.X_AXIS));
		p2.add(Box.createRigidArea(HORIZONTAL_RIGID_SIZE));
		
		for (int k=1; k<=5; k++)
		{
			JButton fv = new JButton(Integer.toString(k));
			fv.setBorder(new OvalBorder(6, 6));
			fv.setToolTipText(m_stations[k].m_name);
			fv.addActionListener(new StationListener(k));
			p2.add(fv);
			p2.add(Box.createRigidArea(HORIZONTAL_RIGID_SIZE));
		}

		p.add(p2);
		p.add(Box.createRigidArea(VERTICAL_RIGID_SIZE));
		p1.add(p);
		p0.add(p1);

		p = new JPanel();
		p.setBorder(new TitledBorder(new OvalBorder(), "Volume"));
		p.setLayout(new BoxLayout(p, BoxLayout.X_AXIS));

		JSlider volume = new JSlider(JSlider.VERTICAL);
		
		p.add(Box.createRigidArea(HORIZONTAL_RIGID_SIZE));
		p.add(volume);
		p.add(Box.createRigidArea(HORIZONTAL_RIGID_SIZE));
		
		p0.add(Box.createRigidArea(HORIZONTAL_RIGID_SIZE));
		p0.add(p);

		getContentPane().add(p0);

		lst.stateChanged(new ChangeEvent(m_frequency));
	}

	public synchronized void playStation(int index, boolean setFreq)
	{
		if (index <0 || index>=m_stations.length)
			return;

		RadioStation station = m_stations[index];
		if (m_currentStation == station)
			return;
		
		stopPlay();
		station.loop();
		m_currentStation = station;

		m_icon.setIcon(ACTIVE_RADIO_IMAGE);
		if (setFreq)
			m_frequency.setValue(station.m_frequency);
	}

	public synchronized void stopPlay()
	{
		if (m_currentStation != null)
		{
			m_currentStation.stop();
			m_currentStation = null;
			m_icon.setIcon(INACTIVE_RADIO_IMAGE);
		}
	}

	class RadioStation
	{
		protected int m_frequency;
		protected String m_name;
		protected String m_file;
		protected AudioClip m_clip;

		public RadioStation(int frequency, String name, String file)
		{
			m_frequency = frequency;
			m_name = name;
			m_file = file;
			
			URL self = getCodeBase();
			m_clip = getAudioClip(self, m_file);
		}

		public boolean closeEnough(int newFreq)
		{
			return Math.abs(m_frequency-newFreq) <= STATION_WIDTH;
		}

		public synchronized void loop()
		{
			if (m_clip != null)
				m_clip.loop();
		}

		public synchronized void stop()
		{
			if (m_clip != null)
				m_clip.stop();
		}
	}

	class StationListener implements ActionListener
	{
		protected int m_index;

		StationListener(int index)
		{
			m_index = index;
		}

		public void actionPerformed(ActionEvent e)
		{
			playStation(m_index, true);
		}
	}

	// Variant 1
	class FreqListener implements ChangeListener
	{
		FreqListener()
		{
		}

		public synchronized void stateChanged(ChangeEvent e)
		{
			int frequency = m_frequency.getValue();
			m_indicator.setText(frequency+" FM");
			boolean found = false;
			for (int k=0; k<m_stations.length; k++)
			{
				if (m_stations[k].closeEnough(frequency))
				{
					playStation(k, false);
					found = true;
					break;
				}
			}
			if (!found)
				stopPlay();
		}
	}

	// Variant 2
/*	class FreqListener implements ChangeListener, Runnable
	{
		protected Vector m_stack;

		FreqListener()
		{
			m_stack = new Vector();
			(new Thread(this)).start();
		}

		public synchronized void stateChanged(ChangeEvent e)
		{
			int frequency = m_frequency.getValue();
			m_stack.addElement(new Integer(frequency));
			m_indicator.setText(frequency+" FM");
		}

		public void run()
		{
			int frequency;
			while(true)
			{
				try 
				{ 
					Thread.sleep(100); 
				} 
				catch (InterruptedException e) {}

				synchronized (m_stack)
				{
					if (m_stack.size()==0)
						continue;
					Object last = m_stack.elementAt(m_stack.size()-1);
					frequency = ((Integer)last).intValue();
					m_stack.removeAllElements();
				}
				
				boolean found = false;
				for (int k=0; k<m_stations.length; k++)
				{
					if (m_stations[k].closeEnough(frequency))
					{
						playStation(k, false);
						found = true;
						break;
					}
				}
				if (!found)
					stopPlay();
			}
		}
	}
*/
}

class OvalBorder 
	implements Border
{
	protected int m_w=6;
	protected int m_h=6;
        protected Color m_topColor = Color.white;
        protected Color m_bottomColor = Color.gray;

	public OvalBorder()
	{
		m_w=6;
		m_h=6;
	}

	public OvalBorder(int w, int h)
	{
		m_w=w;
		m_h=h;
	}

        public OvalBorder(int w, int h, Color topColor, Color bottomColor)
        {
                m_w=w;
                m_h=h;
                m_topColor = topColor;
                m_bottomColor = bottomColor;
        }

	public Insets getBorderInsets(Component c)
	{
		return new Insets(m_h, m_w, m_h, m_w);
	}

	public  boolean isBorderOpaque()
	{
		return true;
	}

	public void paintBorder(Component c, Graphics g, 
		int x, int y, int w, int h)
	{
		w--;
		h--;

		g.setColor(m_topColor);
		g.drawLine(x, y+h-m_h, x, y+m_h);
		g.drawArc(x, y, 2*m_w, 2*m_h, 180, -90);
		g.drawLine(x+m_w, y, x+w-m_w, y);
		g.drawArc(x+w-2*m_w, y, 2*m_w, 2*m_h, 90, -90);

		g.setColor(m_bottomColor);
		g.drawLine(x+w, y+m_h, x+w, y+h-m_h);
		g.drawArc(x+w-2*m_w, y+h-2*m_h, 2*m_w, 2*m_h, 0, -90);
		g.drawLine(x+m_w, y+h, x+w-m_w, y+h);
		g.drawArc(x, y+h-2*m_h, 2*m_w, 2*m_h, -90, -90);
	}
}

