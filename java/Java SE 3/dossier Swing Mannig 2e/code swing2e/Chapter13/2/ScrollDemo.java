/** 
 *  Copyright 1999-2002 Matthew Robinson and Pavel Vorobiev. 
 *  All Rights Reserved. 
 * 
 *  =================================================== 
 *  This program contains code from the book "Swing" 
 *  2nd Edition by Matthew Robinson and Pavel Vorobiev 
 *  http://www.spindoczine.com/sbe 
 *  =================================================== 
 * 
 *  The above paragraph must be included in full, unmodified 
 *  and completely intact in the beginning of any source code 
 *  file that references, copies or uses (in any way, shape 
 *  or form) code contained in this file. 
 */ 

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;

// The idea of this example: you can control scroll bars and use their estate as you pleased - Pavel

public class ScrollDemo
	extends JFrame {

	public ScrollDemo() {
		super("JScrollBar Demo");
		setSize(300,250);

		ImageIcon ii = new ImageIcon("earth.jpg");
		CustomScrollPane sp = new CustomScrollPane(new JLabel(ii));
		getContentPane().add(sp);
	}

	public static void main(String[] args) {
		ScrollDemo frame = new ScrollDemo();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}

class CustomScrollPane
	extends JPanel {

	protected JScrollBar m_vertSB;
	protected JScrollBar m_horzSB;
	protected CustomViewport m_viewport;
	protected JComponent m_comp;

	protected JButton m_btUp;
	protected JButton m_btDown;
	protected JButton m_btLeft;
	protected JButton m_btRight;

	public CustomScrollPane(JComponent comp) {
		if (comp == null)
			throw new IllegalArgumentException("Component cannot be null");

		setLayout(null);
		m_viewport = new CustomViewport();
		m_viewport.setLayout(null);
		add(m_viewport);
		m_comp = comp;
		m_viewport.add(m_comp);

		m_vertSB = new JScrollBar(JScrollBar.VERTICAL, 0, 0, 0, 0);
		m_vertSB.setUnitIncrement(5);
		add(m_vertSB);

		m_horzSB = new JScrollBar(JScrollBar.HORIZONTAL, 0, 0, 0, 0);
		m_horzSB.setUnitIncrement(5);
		add(m_horzSB);

		AdjustmentListener lst = new AdjustmentListener() {
			public void adjustmentValueChanged(AdjustmentEvent e) {
				m_viewport.doLayout();
			}
		};
		m_vertSB.addAdjustmentListener(lst);
		m_horzSB.addAdjustmentListener(lst);

		m_btUp = new JButton(new ImageIcon("Up16.gif"));
		m_btUp.setMargin(new Insets(0,0,0,0));
		m_btUp.setBorder(new EmptyBorder(1,1,1,1));
		m_btUp.setToolTipText("Go top");
		add(m_btUp);
		ActionListener l = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				m_vertSB.setValue(m_vertSB.getMinimum());
				validate();
			}
		};
		m_btUp.addActionListener(l);

		m_btDown = new JButton(new ImageIcon("Down16.gif"));
		m_btDown.setMargin(new Insets(0,0,0,0));
		m_btDown.setBorder(new EmptyBorder(1,1,1,1));
		m_btDown.setToolTipText("Go bottom");
		add(m_btDown);
		l = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				m_vertSB.setValue(m_vertSB.getMaximum());
				validate();
			}
		};
		m_btDown.addActionListener(l);

		m_btLeft = new JButton(new ImageIcon("Back16.gif"));
		m_btLeft.setMargin(new Insets(0,0,0,0));
		m_btLeft.setBorder(new EmptyBorder(1,1,1,1));
		m_btLeft.setToolTipText("Go left");
		add(m_btLeft);
		l = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				m_horzSB.setValue(m_horzSB.getMinimum());
				validate();
			}
		};
		m_btLeft.addActionListener(l);

		m_btRight = new JButton(new ImageIcon("Forward16.gif"));
		m_btRight.setMargin(new Insets(0,0,0,0));
		m_btRight.setBorder(new EmptyBorder(1,1,1,1));
		m_btRight.setToolTipText("Go right");
		add(m_btRight);
		l = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				m_horzSB.setValue(m_horzSB.getMaximum());
				validate();
			}
		};
		m_btRight.addActionListener(l);
	}

	public void doLayout() {
		Dimension d = getSize();
		Dimension d0 = m_comp.getPreferredSize();
		Dimension d1 = m_vertSB.getPreferredSize();
		Dimension d2 = m_horzSB.getPreferredSize();

		int w = Math.max(d.width - d1.width-1, 0);
		int h = Math.max(d.height - d2.height-1, 0);
		m_viewport.setBounds(0, 0, w, h);

		int btW = d1.width;
		int btH = d2.height;
		m_btUp.setBounds(w+1, 0, btW, btH);
		m_vertSB.setBounds(w+1, btH+1, btW, h-2*btH);
		m_btDown.setBounds(w+1, h-btH+1, btW, btH);

		m_btLeft.setBounds(0, h+1, btW, btH);
		m_horzSB.setBounds(btW+1, h+1, w-2*btW, btH);
		m_btRight.setBounds(w-btW+1, h+1, btW, btH);

		int xs = Math.max(d0.width - w, 0);
		m_horzSB.setMaximum(xs);
		m_horzSB.setBlockIncrement(xs/5);
		m_horzSB.setEnabled(xs > 0);

		int ys = Math.max(d0.height - h, 0);
		m_vertSB.setMaximum(ys);
		m_vertSB.setBlockIncrement(ys/5);
		m_vertSB.setEnabled(ys > 0);

        m_horzSB.setVisibleAmount(m_horzSB.getBlockIncrement());
        m_vertSB.setVisibleAmount(m_vertSB.getBlockIncrement());
	}

	public Dimension getPreferredSize() {
		Dimension d0 = m_comp.getPreferredSize();
		Dimension d1 = m_vertSB.getPreferredSize();
		Dimension d2 = m_horzSB.getPreferredSize();
		Dimension d = new Dimension(d0.width+d1.width,
			d0.height+d2.height);
		return d;
	}

	class CustomViewport
		extends JPanel {

		public void doLayout() {
			Dimension d0 = m_comp.getPreferredSize();
			int x = m_horzSB.getValue();
			int y = m_vertSB.getValue();
			m_comp.setBounds(-x, -y, d0.width, d0.height);
		}
	}
}
