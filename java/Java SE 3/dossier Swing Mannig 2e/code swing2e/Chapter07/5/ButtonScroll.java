/** 
 *  Copyright 1999-2002 Matthew Robinson and Pavel Vorobiev. 
 *  All Rights Reserved. 
 * 
 *  =================================================== 
 *  This program contains code from the book "Swing" 
 *  2nd Edition by Matthew Robinson and Pavel Vorobiev 
 *  http://www.spindoczine.com/sbe 
 *  =================================================== 
 * 
 *  The above paragraph must be included in full, unmodified 
 *  and completely intact in the beginning of any source code 
 *  file that references, copies or uses (in any way, shape 
 *  or form) code contained in this file. 
 */ 

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.*;

public class ButtonScroll 
	extends JFrame {

    protected JViewport m_viewport;
	protected JButton m_up;
	protected JButton m_down;
	protected JButton m_left;
	protected JButton m_right;
	
	protected int m_pgVert;
	protected int m_pgHorz;

	public ButtonScroll() {
		super("Scrolling Programmatically");
		setSize(400, 400);
		getContentPane().setLayout(new BorderLayout());

		ImageIcon shuttle = new ImageIcon("shuttle.gif");
		m_pgVert = shuttle.getIconHeight()/5;
		m_pgHorz = shuttle.getIconWidth()/5;
		JLabel lbl = new JLabel(shuttle);

        m_viewport = new JViewport();
        m_viewport.setView(lbl);
        m_viewport.addChangeListener(new ChangeListener() {
          public void stateChanged(ChangeEvent e) {
				enableButtons(ButtonScroll.this.m_viewport.getViewPosition());
            }
		});
       	getContentPane().add(m_viewport, BorderLayout.CENTER);

		JPanel pv = new JPanel(new BorderLayout());
		m_up = createButton("up", 'u');
		ActionListener lst = new ActionListener() { 
			public void actionPerformed(ActionEvent e) { 
				movePanel(0, -1);
			}
		};
		m_up.addActionListener(lst);
		pv.add(m_up, BorderLayout.NORTH);
		
		m_down = createButton("down", 'd');
		lst = new ActionListener() { 
			public void actionPerformed(ActionEvent e) { 
				movePanel(0, 1);
			}
		};
		m_down.addActionListener(lst);
		pv.add(m_down, BorderLayout.SOUTH);
		getContentPane().add(pv, BorderLayout.EAST);

		JPanel ph = new JPanel(new BorderLayout());
		m_left = createButton("left", 'l');
		lst = new ActionListener() { 
			public void actionPerformed(ActionEvent e) { 
				movePanel(-1, 0);
			}
		};
		m_left.addActionListener(lst);
		ph.add(m_left, BorderLayout.WEST);

		m_right = createButton("right", 'r');
		lst = new ActionListener() { 
			public void actionPerformed(ActionEvent e) { 
				movePanel(1, 0);
			}
		};
		m_right.addActionListener(lst);
		ph.add(m_right, BorderLayout.EAST);
		getContentPane().add(ph, BorderLayout.SOUTH);
		
		//setVisible(true);
		movePanel(0, 0);
	}

	protected JButton createButton(String name, char mnemonics) {
		JButton btn = new JButton(new ImageIcon(name+"1.gif"));
		btn.setPressedIcon(new ImageIcon(name+"2.gif"));
		btn.setDisabledIcon(new ImageIcon(name+"3.gif"));
		btn.setToolTipText("Move "+name);
		btn.setBorderPainted(false);
		btn.setMargin(new Insets(0, 0, 0, 0));
		btn.setContentAreaFilled(false);
		btn.setMnemonic(mnemonics);
		return btn;
	}

	protected void movePanel(int xmove, int ymove) {
		Point pt = m_viewport.getViewPosition();
		pt.x += m_pgHorz*xmove;
		pt.y += m_pgVert*ymove;

		pt.x = Math.max(0, pt.x);
		pt.x = Math.min(getMaxXExtent(), pt.x);
		pt.y = Math.max(0, pt.y);
		pt.y = Math.min(getMaxYExtent(), pt.y);

		m_viewport.setViewPosition(pt);
		enableButtons(pt);
	}

	protected void enableButtons(Point pt) {
		enableComponent(m_left, pt.x > 0);
		enableComponent(m_right, pt.x < getMaxXExtent());
		enableComponent(m_up, pt.y > 0);
		enableComponent(m_down, pt.y < getMaxYExtent());
	}

	protected void enableComponent(JComponent c, boolean b) {
		if (c.isEnabled() != b)
			c.setEnabled(b);
	}

	protected int getMaxXExtent() {
        return m_viewport.getView().getWidth()-m_viewport.getWidth();
	}

	protected int getMaxYExtent() {
        return m_viewport.getView().getHeight()-m_viewport.getHeight();
	}

	public static void main(String argv[])  {
		ButtonScroll frame = new ButtonScroll();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}
