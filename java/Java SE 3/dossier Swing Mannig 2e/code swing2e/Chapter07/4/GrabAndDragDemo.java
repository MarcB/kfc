/** 
 *  Copyright 1999-2002 Matthew Robinson and Pavel Vorobiev. 
 *  All Rights Reserved. 
 * 
 *  =================================================== 
 *  This program contains code from the book "Swing" 
 *  2nd Edition by Matthew Robinson and Pavel Vorobiev 
 *  http://www.spindoczine.com/sbe 
 *  =================================================== 
 * 
 *  The above paragraph must be included in full, unmodified 
 *  and completely intact in the beginning of any source code 
 *  file that references, copies or uses (in any way, shape 
 *  or form) code contained in this file. 
 */ 

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

public class GrabAndDragDemo
extends JFrame 
{
  public GrabAndDragDemo() {
    super("Grab-and-Drag Demo");
    ImageIcon ii = new ImageIcon("earth.jpg");
    JScrollPane jsp = new JScrollPane(new GrabAndScrollLabel(ii));
    getContentPane().add(jsp);
    setSize(300,250);
  }

  public static void main(String[] args) { 
    GrabAndDragDemo frame = new GrabAndDragDemo();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setVisible(true);
  }
}

class GrabAndScrollLabel
	extends JLabel {

  public GrabAndScrollLabel(ImageIcon i) {
    super(i);

    MouseInputAdapter mia = new MouseInputAdapter() {
      int m_XDifference, m_YDifference;
      boolean m_dragging;
      Container c;

      public void mouseDragged(MouseEvent e) {
        c = GrabAndScrollLabel.this.getParent();
        if (c instanceof JViewport) {
          JViewport jv = (JViewport) c;
          Point p = jv.getViewPosition();
          int newX = p.x - (e.getX() - m_XDifference);
          int newY = p.y - (e.getY() - m_YDifference);

          int maxX = GrabAndScrollLabel.this.getWidth() - jv.getWidth();
          int maxY = GrabAndScrollLabel.this.getHeight() - jv.getHeight();
          if (newX < 0)
            newX = 0;
          if (newX > maxX)
            newX = maxX;
          if (newY < 0)
            newY = 0;
          if (newY > maxY)
            newY = maxY;

          jv.setViewPosition(new Point(newX, newY));
        }
      }

      public void mousePressed(MouseEvent e) {
        setCursor(Cursor.getPredefinedCursor(
          Cursor.MOVE_CURSOR));
        m_XDifference = e.getX();
        m_YDifference = e.getY();
      }

      public void mouseReleased(MouseEvent e) {
        setCursor(Cursor.getPredefinedCursor(
          Cursor.DEFAULT_CURSOR));
      }        
    };
    addMouseMotionListener(mia);
    addMouseListener(mia);
  }
}
  