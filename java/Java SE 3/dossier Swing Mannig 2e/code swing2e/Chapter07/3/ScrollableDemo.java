/** 
 *  Copyright 1999-2002 Matthew Robinson and Pavel Vorobiev. 
 *  All Rights Reserved. 
 * 
 *  =================================================== 
 *  This program contains code from the book "Swing" 
 *  2nd Edition by Matthew Robinson and Pavel Vorobiev 
 *  http://www.spindoczine.com/sbe 
 *  =================================================== 
 * 
 *  The above paragraph must be included in full, unmodified 
 *  and completely intact in the beginning of any source code 
 *  file that references, copies or uses (in any way, shape 
 *  or form) code contained in this file. 
 */ 

import java.awt.*;
import javax.swing.*;

public class ScrollableDemo
extends JFrame {

  public ScrollableDemo() {
    super("Scrollable Demo");
    ImageIcon ii = new ImageIcon("earth.jpg");
    JScrollPane jsp = new JScrollPane(new ScrollableLabel(ii));
    getContentPane().add(jsp);
    setSize(300,250);
  }

  public static void main(String[] args) { 
    ScrollableDemo frame = new ScrollableDemo();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setVisible(true);
  }
}

class ScrollableLabel
	extends JLabel
	implements Scrollable {

  public ScrollableLabel(ImageIcon i){
    super(i);
  }

  public Dimension getPreferredScrollableViewportSize() {
    return getPreferredSize();
  }
  
  // Will scroll in 4 steps by block
  public int getScrollableBlockIncrement(Rectangle r, 
    int orientation, int direction) {
	  if (orientation == SwingConstants.VERTICAL)
	  	return Math.max(getHeight()-r.height, 0)/4;
	  else
		return Math.max(getWidth()-r.width, 0)/4;
  }

  // Will scroll in 8 steps by units
  public int getScrollableUnitIncrement(Rectangle r, 
    int orientation, int direction) {
	  if (orientation == SwingConstants.VERTICAL)
	  	return Math.max(getHeight()-r.height, 0)/8;
	  else
		return Math.max(getWidth()-r.width, 0)/8;
  }

  public boolean getScrollableTracksViewportHeight() {
    return false;
  }

  public boolean getScrollableTracksViewportWidth() {
    return false;
  }
}
