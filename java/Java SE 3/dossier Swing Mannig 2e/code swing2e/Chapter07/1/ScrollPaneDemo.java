/** 
 *  Copyright 1999-2002 Matthew Robinson and Pavel Vorobiev. 
 *  All Rights Reserved. 
 * 
 *  =================================================== 
 *  This program contains code from the book "Swing" 
 *  2nd Edition by Matthew Robinson and Pavel Vorobiev 
 *  http://www.spindoczine.com/sbe 
 *  =================================================== 
 * 
 *  The above paragraph must be included in full, unmodified 
 *  and completely intact in the beginning of any source code 
 *  file that references, copies or uses (in any way, shape 
 *  or form) code contained in this file. 
 */ 

import java.awt.*;
import javax.swing.*;

import java.awt.event.*;

public class ScrollPaneDemo
	extends JFrame {

  public ScrollPaneDemo() {
    super("JScrollPane Demo");
    ImageIcon ii = new ImageIcon("earth.jpg");
    JScrollPane jsp = new JScrollPane(new JLabel(ii));
    jsp.setWheelScrollingEnabled(true);

    jsp.addMouseWheelListener(new MouseWheelListener() {
      public void mouseWheelMoved(MouseWheelEvent e) {
        int scrollAmount = e.getScrollAmount();
        String scrollType = 
          e.getScrollType() == MouseWheelEvent.WHEEL_UNIT_SCROLL ? 
            "WHEEL_UNIT_SCROLL" : "WHEEL_BLOCK_SCROLL";
        int wheelRotations = e.getWheelRotation();
        System.out.println("Mouse wheel rotated. Scroll Amount = "
          + scrollAmount + ", Wheel Rotations = " + wheelRotations 
          + ", Scroll Type = " + scrollType);
      }
    });

    getContentPane().add(jsp);
    setSize(300,250);
  }

  public static void main(String[] args) { 

    try {
      UIManager.setLookAndFeel(
        UIManager.getSystemLookAndFeelClassName());
    }
    catch (Exception e) {}

    ScrollPaneDemo frame = new ScrollPaneDemo();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setVisible(true);
  }
}
