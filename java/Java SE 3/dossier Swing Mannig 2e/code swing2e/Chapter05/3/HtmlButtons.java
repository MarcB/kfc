/** 
 *  Copyright 1999-2002 Matthew Robinson and Pavel Vorobiev. 
 *  All Rights Reserved. 
 * 
 *  =================================================== 
 *  This program contains code from the book "Swing" 
 *  2nd Edition by Matthew Robinson and Pavel Vorobiev 
 *  http://www.spindoczine.com/sbe 
 *  =================================================== 
 * 
 *  The above paragraph must be included in full, unmodified 
 *  and completely intact in the beginning of any source code 
 *  file that references, copies or uses (in any way, shape 
 *  or form) code contained in this file. 
 */ 

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class HtmlButtons 
	extends JFrame {

	public HtmlButtons() {
		super("HTML Buttons and Labels");
		setSize(400, 300);

		getContentPane().setLayout(new FlowLayout());

		String htmlText = 
			"<html><p><font color=\"#800080\" "+
			"size=\"4\" face=\"Verdana\">JButton</font> </p>"+
			"<address><font size=\"2\"><em>"+
			"with HTML text</em></font>"+
			"</address>";
		JButton btn = new JButton(htmlText);
		getContentPane().add(btn);

		htmlText = 
			"<html><p><font color=\"#800080\" "+
			"size=\"4\" face=\"Verdana\">JLabel</font> </p>"+
			"<address><font size=\"2\"><em>"+
			"with HTML text</em></font>"+
			"</address>";
		JLabel lbl = new JLabel(htmlText);
		getContentPane().add(lbl);
	}

	public static void main(String args[]) {
		HtmlButtons frame = new HtmlButtons();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}
