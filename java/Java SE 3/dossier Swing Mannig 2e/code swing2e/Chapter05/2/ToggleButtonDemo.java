/** 
 *  Copyright 1999-2002 Matthew Robinson and Pavel Vorobiev. 
 *  All Rights Reserved. 
 * 
 *  =================================================== 
 *  This program contains code from the book "Swing" 
 *  2nd Edition by Matthew Robinson and Pavel Vorobiev 
 *  http://www.spindoczine.com/sbe 
 *  =================================================== 
 * 
 *  The above paragraph must be included in full, unmodified 
 *  and completely intact in the beginning of any source code 
 *  file that references, copies or uses (in any way, shape 
 *  or form) code contained in this file. 
 */ 

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

class ToggleButtonDemo extends JFrame {

  public ToggleButtonDemo () {
    super("ToggleButton Demo");
    getContentPane().setLayout(new FlowLayout());
    
    ButtonGroup buttonGroup = new ButtonGroup();
    for (int k=0; k<4; k++) {
      char ch = (char)('1' + k);
      JToggleButton button = new JToggleButton("Button "+ch, k==0);
      button.setMnemonic(ch);
      button.setEnabled(k<3);
      button.setToolTipText("This is Button " + ch);

      button.setIcon(new ImageIcon("ball_bw.gif"));
      button.setSelectedIcon(new ImageIcon("ball_red.gif"));
      button.setRolloverIcon(new ImageIcon("ball_blue.gif"));
      button.setRolloverSelectedIcon(new ImageIcon("ball_blue.gif"));

      getContentPane().add(button);
      buttonGroup.add(button);
    }

    pack();
  }

  public static void main(String args[]) {
    ToggleButtonDemo frame = new ToggleButtonDemo();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setVisible(true);
  }
}
