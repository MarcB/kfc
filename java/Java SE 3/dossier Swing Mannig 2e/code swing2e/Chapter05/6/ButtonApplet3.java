/** 
 *  Copyright 1999-2002 Matthew Robinson and Pavel Vorobiev. 
 *  All Rights Reserved. 
 * 
 *  =================================================== 
 *  This program contains code from the book "Swing" 
 *  2nd Edition by Matthew Robinson and Pavel Vorobiev 
 *  http://www.spindoczine.com/sbe 
 *  =================================================== 
 * 
 *  The above paragraph must be included in full, unmodified 
 *  and completely intact in the beginning of any source code 
 *  file that references, copies or uses (in any way, shape 
 *  or form) code contained in this file. 
 */ 

import java.applet.*;
import java.awt.*;
import java.awt.event.*;
import java.net.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

public class ButtonApplet3 
	extends JApplet {

// NEW
	protected CustomToolTipManager m_manager;	// Former MyToolTipManager

	public ButtonApplet3() {}

	public synchronized void init() {
	
		String imageName = getParameter("image");
		if (imageName == null) {
			System.err.println("Need \"image\" parameter");
			return;
		}
		URL imageUrl = null;
		try {
			imageUrl = new URL(getDocumentBase(), imageName);
		}
		catch (MalformedURLException ex) {
			ex.printStackTrace();
			return;
		}
		ImageIcon bigImage = new ImageIcon(imageUrl);
		JLabel bigLabel = new JLabel(bigImage);
		bigLabel.setLayout(null);

		int index = 1;
		while(true) {
			String paramSize = getParameter("button"+index);
			String paramName = getParameter("name"+index);
			String paramUrl = getParameter("url"+index);
			if (paramSize==null || paramName==null || paramUrl==null)
				break;
		
			Polygon p = new Polygon();
			try {
				StringTokenizer tokenizer = new StringTokenizer(
					paramSize, ",");
				while (tokenizer.hasMoreTokens()) {
					String str = tokenizer.nextToken().trim();
					int x = Integer.parseInt(str);
					str = tokenizer.nextToken().trim();
					int y = Integer.parseInt(str);
					p.addPoint(x, y);
				}
			}
			catch (Exception ex) { break; }

			PolygonButton btn = new PolygonButton(this, p, 
				paramName, paramUrl);
			bigLabel.add(btn);

			index++;
		}

		m_manager = new CustomToolTipManager(this);
		PolygonButton.m_toolTip = m_manager.m_toolTip;

		getContentPane().setLayout(null);
		getContentPane().add(bigLabel);
		bigLabel.setBounds(0, 0, bigImage.getIconWidth(), 
			bigImage.getIconHeight());
	}

	public String getAppletInfo() {
		return "Sample applet with PolygonButtons";
	}

	public String[][] getParameterInfo() {
		String pinfo[][] = {	 
			{"image",  "string",  "base image file name"},
			{"buttonX","x1,y1, x2,y2, ...", "button's bounds"},
			{"nameX",  "string",  "tooltip text"},
			{"urlX",   "url",     "link URL"} };
		return pinfo;
	}
}

class PolygonButton
	extends    JComponent
	implements MouseListener, MouseMotionListener {

	static public Color ACTIVE_COLOR = Color.red;
	static public Color INACTIVE_COLOR = Color. darkGray;

	protected JApplet m_parent;
	protected String m_text;
	protected String m_sUrl;
	protected URL    m_url;

	protected Polygon m_polygon;
	protected Rectangle m_rc;
	protected boolean m_active;

	protected static PolygonButton m_currentButton;

// NEW
	public static JToolTip m_toolTip;

	public PolygonButton(JApplet parent, Polygon p, 
		String text, String sUrl) {
	
		m_parent = parent;
		m_polygon = p;
		setText(text);
		m_sUrl = sUrl;
		try {
			m_url = new URL(sUrl);
		}
		catch(Exception ex) { m_url = null; }

		setOpaque(false);

		m_parent.addMouseListener(this);
		m_parent.addMouseMotionListener(this);

		m_rc = new Rectangle(m_polygon.getBounds()); // Bug alert!
		m_rc.grow(1, 1);

		setBounds(m_rc);
		m_polygon.translate(-m_rc.x, -m_rc.y);
	}

	public void setText(String text) {
		m_text = text;
	}

	public String getText() {
		return m_text;
	}

	public void mouseMoved(MouseEvent e) {
	
		if (!m_rc.contains(e.getX(), e.getY()) || e.isConsumed()) {
			if (m_active) {
				m_active = false;
				repaint();
			}
			return;		// quickly return, if outside our rectangle
		}
		
		int x = e.getX() - m_rc.x;
		int y = e.getY() - m_rc.y;
		boolean active = m_polygon.contains(x, y);

		if (m_active != active)
			setState(active);
		if (active)
			e.consume();
	}

	public void mouseDragged(MouseEvent e) {}
		
	protected void setState(boolean active) {
	
		m_active = active;
		repaint();
		if (active) {
			if (m_currentButton != null)
				m_currentButton.setState(false);
			m_parent.setCursor(Cursor.getPredefinedCursor(
				Cursor.HAND_CURSOR));
			m_parent.showStatus(m_sUrl);
			if (m_toolTip != null)
				m_toolTip.setTipText(m_text);	// NEW
		}
		else {
			m_currentButton = null;
			m_parent.setCursor(Cursor.getPredefinedCursor(
				Cursor.DEFAULT_CURSOR));
			m_parent.showStatus("");
			if (m_toolTip != null)
				m_toolTip.setTipText(null);		// NEW
		}
	}
	
	public void mouseClicked(MouseEvent e) {

		if (m_active && m_url != null && !e.isConsumed()) {
			AppletContext context = m_parent.getAppletContext();
			if (context != null)
				context.showDocument(m_url);
			e.consume();
		}
	}

	public void mousePressed(MouseEvent e) {}

	public void mouseReleased(MouseEvent e) {}

	public void mouseExited(MouseEvent e) {
		mouseMoved(e);
	}

	public void mouseEntered(MouseEvent e) {
		mouseMoved(e);
	}
				
	public void paint(Graphics g) {
		g.setColor(m_active ? ACTIVE_COLOR : INACTIVE_COLOR);
		g.drawPolygon(m_polygon);
	}

}

// NEW
class CustomToolTipManager
	extends MouseMotionAdapter
	implements ActionListener {

	protected javax.swing.Timer m_timer;	// Name conflict with java.util.Timer in 1.3
	protected int m_lastX = -1;
	protected int m_lastY = -1;
	protected boolean m_moved = false;
	protected int m_counter = 0;

	public JToolTip m_toolTip = new JToolTip();

	CustomToolTipManager(JApplet parent) {
		parent.addMouseMotionListener(this);

		m_toolTip.setTipText(null);
		parent.getContentPane().add(m_toolTip);

		m_toolTip.setVisible(false);
		m_timer = new javax.swing.Timer(1000, this);
		m_timer.start();
	}

	public void mouseMoved(MouseEvent e) {
		m_moved = true;
		m_counter = -1;
		m_lastX = e.getX();
		m_lastY = e.getY();
		if (m_toolTip.isVisible()) {
			m_toolTip.setVisible(false);
			m_toolTip.getParent().repaint();
		}
	}

	public void actionPerformed(ActionEvent e) {

		if (m_moved || m_counter==0 || m_toolTip.getTipText()==null) {
			if (m_toolTip.isVisible())
				m_toolTip.setVisible(false);
			m_moved = false;
			return;
		}

		if (m_counter < 0) {
			m_counter = 4;
			m_toolTip.setVisible(true);
			Dimension d = m_toolTip.getPreferredSize();
			m_toolTip.setBounds(m_lastX, m_lastY+20, 
				d.width, d.height);
		}

		m_counter--;
	}
}
