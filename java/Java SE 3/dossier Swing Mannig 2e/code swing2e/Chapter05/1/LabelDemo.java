/** 
 *  Copyright 1999-2002 Matthew Robinson and Pavel Vorobiev. 
 *  All Rights Reserved. 
 * 
 *  =================================================== 
 *  This program contains code from the book "Swing" 
 *  2nd Edition by Matthew Robinson and Pavel Vorobiev 
 *  http://www.spindoczine.com/sbe 
 *  =================================================== 
 * 
 *  The above paragraph must be included in full, unmodified 
 *  and completely intact in the beginning of any source code 
 *  file that references, copies or uses (in any way, shape 
 *  or form) code contained in this file. 
 */ 

import java.awt.*;
import javax.swing.*;

class LabelDemo
	extends JFrame {

  public LabelDemo() {
    super("JLabel Demo");
    setSize(600, 100);

    JPanel content = (JPanel) getContentPane();
    content.setLayout(new GridLayout(1, 4, 4, 4));

    JLabel label = new JLabel();
    label.setText("JLabel");
    label.setBackground(Color.white);
    content.add(label);

    label = new JLabel("JLabel",
      SwingConstants.CENTER);
    label.setOpaque(true);
    label.setBackground(Color.white);
    content.add(label);

    label = new JLabel("JLabel");
    label.setFont(new Font("Helvetica", Font.BOLD, 18));
    label.setOpaque(true);
    label.setBackground(Color.white);
    content.add(label);

    ImageIcon image = new ImageIcon("flight.gif");
    label = new JLabel("JLabel", image,
      SwingConstants.RIGHT);
    label.setVerticalTextPosition(SwingConstants.TOP);
    label.setOpaque(true);
    label.setBackground(Color.white);
    content.add(label);
  }

  public static void main(String args[]) {
    LabelDemo frame = new LabelDemo();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setVisible(true);
  }
}
