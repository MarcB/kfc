/** 
 *  Copyright 1999-2002 Matthew Robinson and Pavel Vorobiev. 
 *  All Rights Reserved. 
 * 
 *  =================================================== 
 *  This program contains code from the book "Swing" 
 *  2nd Edition by Matthew Robinson and Pavel Vorobiev 
 *  http://www.spindoczine.com/sbe 
 *  =================================================== 
 * 
 *  The above paragraph must be included in full, unmodified 
 *  and completely intact in the beginning of any source code 
 *  file that references, copies or uses (in any way, shape 
 *  or form) code contained in this file. 
 */ 

import java.applet.*;
import java.awt.*;
import java.awt.event.*;
import java.net.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

public class ButtonApplet 
	extends JApplet {

	public ButtonApplet() {}

	public synchronized void init() {
		String imageName = getParameter("image");
		if (imageName == null) {
			System.err.println("Need \"image\" parameter");
			return;
		}
		URL imageUrl = null;
		try {
			imageUrl = new URL(getDocumentBase(), imageName);
		}
		catch (MalformedURLException ex) {
			ex.printStackTrace();
			return;
		}
		ImageIcon bigImage = new ImageIcon(imageUrl);
		JLabel bigLabel = new JLabel(bigImage);
		bigLabel.setLayout(null);

		int index = 1;
		int[] q = new int[4];
		while(true) {
			String paramSize = getParameter("button"+index);
			String paramName = getParameter("name"+index);
			String paramUrl = getParameter("url"+index);
			if (paramSize==null || paramName==null || paramUrl==null)
				break;
			
			try {
				StringTokenizer tokenizer = new StringTokenizer(
					paramSize, ",");
				for (int k=0; k<4; k++) {
					String str = tokenizer.nextToken().trim();
					q[k] = Integer.parseInt(str);
				}
			}
			catch (Exception ex) { break; }

			NavigateButton btn = new NavigateButton(
				this, paramName, paramUrl);
			bigLabel.add(btn);
			btn.setBounds(q[0], q[1], q[2], q[3]);

			index++;
		}

		getContentPane().setLayout(null);
		getContentPane().add(bigLabel);
		bigLabel.setBounds(0, 0, bigImage.getIconWidth(), 
			bigImage.getIconHeight());
	}

	public String getAppletInfo() {
		return "Sample applet with NavigateButtons";
	}

	public String[][] getParameterInfo() {
		String pinfo[][] = {	 
			{"image",  "string",  "base image file name"},
			{"buttonX","x,y,w,h", "button's bounds"},
			{"nameX",  "string",  "tooltip text"},
			{"urlX",   "url",     "link URL"} };
		return pinfo;
	}
}

class NavigateButton
	extends JButton
	implements ActionListener {

	protected Border m_activeBorder;
	protected Border m_inactiveBorder;

	protected Applet m_parent;
	protected String m_text;
	protected String m_sUrl;
	protected URL    m_url;

	public NavigateButton(Applet parent, String text, String sUrl) {
		m_parent = parent;
		setText(text);
		m_sUrl = sUrl;
		try {
			m_url = new URL(sUrl);
		}
		catch(Exception ex) { m_url = null; }

		setOpaque(false);
		enableEvents(AWTEvent.MOUSE_EVENT_MASK);

		m_activeBorder = new MatteBorder(1, 1, 1, 1, Color.yellow);
		m_inactiveBorder = new EmptyBorder(1, 1, 1, 1);
		setBorder(m_inactiveBorder);

		addActionListener(this);
	}

	public void setText(String text) {
		m_text = text;
		setToolTipText(text);
	}

	public String getText() {
		return m_text;
	}

	protected void processMouseEvent(MouseEvent evt) {
		switch (evt.getID()) {
		case MouseEvent.MOUSE_ENTERED:
			setBorder(m_activeBorder);
			setCursor(Cursor.getPredefinedCursor(
				Cursor.HAND_CURSOR));
			m_parent.showStatus(m_sUrl);
			break;

		case MouseEvent.MOUSE_EXITED:
			setBorder(m_inactiveBorder);
			setCursor(Cursor.getPredefinedCursor(
				Cursor.DEFAULT_CURSOR));
			m_parent.showStatus("");
			break;
		}
		super.processMouseEvent(evt);
	}

	public void actionPerformed(ActionEvent e) {
		if (m_url != null) {
			AppletContext context = m_parent.getAppletContext();
			if (context != null)
				context.showDocument(m_url);
		}
	}
	
	public void paintComponent(Graphics g) {
		paintBorder(g);
	}
}
