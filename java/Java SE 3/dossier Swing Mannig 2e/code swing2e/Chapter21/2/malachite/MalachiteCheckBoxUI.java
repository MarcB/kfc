/** 
 *  Copyright 1999-2002 Matthew Robinson and Pavel Vorobiev. 
 *  All Rights Reserved. 
 * 
 *  =================================================== 
 *  This program contains code from the book "Swing" 
 *  2nd Edition by Matthew Robinson and Pavel Vorobiev 
 *  http://www.spindoczine.com/sbe 
 *  =================================================== 
 * 
 *  The above paragraph must be included in full, unmodified 
 *  and completely intact in the beginning of any source code 
 *  file that references, copies or uses (in any way, shape 
 *  or form) code contained in this file. 
 */ 

package malachite;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.plaf.*;
import javax.swing.plaf.basic.*;

public class MalachiteCheckBoxUI
	extends    BasicCheckBoxUI
	implements java.io.Serializable, MouseListener
{

	private final static MalachiteCheckBoxUI m_buttonUI =
		new MalachiteCheckBoxUI();

	protected Color  m_backgroundNormal = null;
	protected Color  m_foregroundNormal = null;
	protected Color  m_foregroundActive = null;
	protected Icon   m_checkedIcon = null;
	protected Icon   m_uncheckedIcon = null;
	protected Icon   m_pressedCheckedIcon = null;
	protected Icon   m_pressedUncheckedIcon = null;
	protected Color  m_focusBorder = null;
	protected int    m_textIconGap = -1;

	public MalachiteCheckBoxUI() {}

	public static ComponentUI createUI( JComponent c )
	{
		return m_buttonUI;
	}

	public void installUI(JComponent c)
	{
		super.installUI(c);

		m_backgroundNormal = UIManager.getColor(
			"CheckBox.background");

		m_foregroundNormal = UIManager.getColor(
			"CheckBox.foreground");
		m_foregroundActive = UIManager.getColor(
			"CheckBox.activeForeground");

		m_checkedIcon = UIManager.getIcon(
			"CheckBox.iconChecked");
		m_uncheckedIcon = UIManager.getIcon(
			"CheckBox.icon");
		m_pressedCheckedIcon = UIManager.getIcon(
			"CheckBox.iconPressedChecked");
		m_pressedUncheckedIcon = UIManager.getIcon(
			"CheckBox.iconPressed");

		m_focusBorder = UIManager.getColor(
			"CheckBox.focusBorder");

		m_textIconGap = UIManager.getInt(
			"CheckBox.textIconGap");

		c.setBackground(m_backgroundNormal);
		c.addMouseListener(this);
	}

	public void uninstallUI(JComponent c)
	{
		super.uninstallUI(c);
		c.removeMouseListener(this);
	}

	public void paint(Graphics g, JComponent c)
	{
		AbstractButton b = (AbstractButton)c;
		ButtonModel model = b.getModel();
		Dimension d = b.getSize();

		g.setFont(c.getFont());
		FontMetrics fm = g.getFontMetrics();

		Icon icon = m_uncheckedIcon;
		if (model.isPressed() && model.isSelected())
			icon = m_pressedCheckedIcon;
		else if (model.isPressed() && !model.isSelected())
			icon = m_pressedUncheckedIcon;
		else if (!model.isPressed() && model.isSelected())
			icon = m_checkedIcon;

		g.setColor(b.getForeground());
		int x = 0;
		int y = (d.height - icon.getIconHeight())/2;
		icon.paintIcon(c, g, x, y);

		String caption = b.getText();
		x = icon.getIconWidth() + m_textIconGap;
		y = (d.height + fm.getAscent())/2;
		g.drawString(caption, x, y);

		if (b.isFocusPainted() && b.hasFocus())
		{
			g.setColor(m_focusBorder);
			Insets bi = b.getBorder().getBorderInsets(b);
			g.drawRect(x-2, y-fm.getAscent()-2, d.width-x,
				fm.getAscent()+fm.getDescent()+4);
		}
	}

	public void mouseClicked(MouseEvent e) {}

	public void mousePressed(MouseEvent e) {}

	public void mouseReleased(MouseEvent e) {}

	public void mouseEntered(MouseEvent e)
	{
		JComponent c = (JComponent)e.getComponent();
		c.setForeground(m_foregroundActive);
		c.repaint();
	}

	public void mouseExited(MouseEvent e)
	{
		JComponent c = (JComponent)e.getComponent();
		c.setForeground(m_foregroundNormal);
		c.repaint();
	}

}
