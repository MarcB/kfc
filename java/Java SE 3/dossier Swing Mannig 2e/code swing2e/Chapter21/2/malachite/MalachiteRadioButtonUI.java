/** 
 *  Copyright 1999-2002 Matthew Robinson and Pavel Vorobiev. 
 *  All Rights Reserved. 
 * 
 *  =================================================== 
 *  This program contains code from the book "Swing" 
 *  2nd Edition by Matthew Robinson and Pavel Vorobiev 
 *  http://www.spindoczine.com/sbe 
 *  =================================================== 
 * 
 *  The above paragraph must be included in full, unmodified 
 *  and completely intact in the beginning of any source code 
 *  file that references, copies or uses (in any way, shape 
 *  or form) code contained in this file. 
 */

package malachite;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.plaf.*;
import javax.swing.plaf.basic.*;

public class MalachiteRadioButtonUI
	extends    MalachiteCheckBoxUI
	implements java.io.Serializable, MouseListener
{

	private final static MalachiteRadioButtonUI m_buttonUI =
		new MalachiteRadioButtonUI();

	public MalachiteRadioButtonUI() {}

	public static ComponentUI createUI( JComponent c )
	{
		return m_buttonUI;
	}

	public void installUI(JComponent c)
	{
		super.installUI(c);

		m_backgroundNormal = UIManager.getColor(
			"RadioButton.background");

		m_foregroundNormal = UIManager.getColor(
			"RadioButton.foreground");
		m_foregroundActive = UIManager.getColor(
			"RadioButton.activeForeground");

		m_checkedIcon = UIManager.getIcon(
			"RadioButton.iconChecked");
		m_uncheckedIcon = UIManager.getIcon(
			"RadioButton.icon");
		m_pressedCheckedIcon = UIManager.getIcon(
			"RadioButton.iconPressedChecked");
		m_pressedUncheckedIcon = UIManager.getIcon(
			"RadioButton.iconPressed");

		m_focusBorder = UIManager.getColor(
			"RadioButton.focusBorder");

		m_textIconGap = UIManager.getInt(
			"RadioButton.textIconGap");

		c.setBackground(m_backgroundNormal);
		c.addMouseListener(this);
	}
}
