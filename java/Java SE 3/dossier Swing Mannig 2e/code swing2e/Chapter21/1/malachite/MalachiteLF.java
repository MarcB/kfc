/** 
 *  Copyright 1999-2002 Matthew Robinson and Pavel Vorobiev. 
 *  All Rights Reserved. 
 * 
 *  =================================================== 
 *  This program contains code from the book "Swing" 
 *  2nd Edition by Matthew Robinson and Pavel Vorobiev 
 *  http://www.spindoczine.com/sbe 
 *  =================================================== 
 * 
 *  The above paragraph must be included in full, unmodified 
 *  and completely intact in the beginning of any source code 
 *  file that references, copies or uses (in any way, shape 
 *  or form) code contained in this file. 
 */ 

package malachite;

import java.awt.*;

import javax.swing.*;
import javax.swing.plaf.*;
import javax.swing.plaf.basic.*;

public class MalachiteLF
	extends    BasicLookAndFeel
	implements java.io.Serializable
{

	public String getID()
	{
		return "Malachite";
	}

	public String getName()
	{
		return "Malachite";
	}

	public String getDescription()
	{
		return "Sample L&F from Swing";
	}

	public boolean isNativeLookAndFeel()
	{
		return false;
	}

	public boolean isSupportedLookAndFeel()
	{
		return true;
	}

	protected void initComponentDefaults(UIDefaults table)
	{
		super.initComponentDefaults(table);

		ColorUIResource commonBackground =
			new ColorUIResource(152, 208, 128);
		ColorUIResource commonForeground =
			new ColorUIResource(0, 0, 0);
		ColorUIResource buttonBackground =
			new ColorUIResource(4, 108, 2);
		ColorUIResource buttonForeground =
			new ColorUIResource(236, 236, 0);
		ColorUIResource menuBackground =
			new ColorUIResource(128, 192, 128);

		BorderUIResource borderRaised = new
			BorderUIResource(new MalachiteBorder(
			MalachiteBorder.RAISED));
		BorderUIResource borderLowered = new
			BorderUIResource(new MalachiteBorder(
			MalachiteBorder.LOWERED));

		FontUIResource commonFont = new
			FontUIResource("Arial", Font.BOLD, 12 );

		Icon ubox = new ImageIcon("Malachite/ubox.gif");
		Icon ubull = new ImageIcon("Malachite/ubull.gif");

		Object[] defaults =
		{
			"Button.font", commonFont,
			"Button.background", buttonBackground,
			"Button.foreground", buttonForeground,
			"Button.border", borderRaised,
			"Button.margin", new InsetsUIResource(8, 8, 8, 8),
			"Button.textIconGap", new Integer(4),
			"Button.textShiftOffset", new Integer(2),

			"CheckBox.font", commonFont,
			"CheckBox.background", commonBackground,
			"CheckBox.foreground", commonForeground,
			"CheckBox.icon", new IconUIResource(ubox),

			"MenuBar.font", commonFont,
			"MenuBar.background", menuBackground,
			"MenuBar.foreground", commonForeground,

			"Menu.font", commonFont,
			"Menu.background", menuBackground,
			"Menu.foreground", commonForeground,
			"Menu.selectionBackground", buttonBackground,
			"Menu.selectionForeground", buttonForeground,

			"MenuItem.font", commonFont,
			"MenuItem.background", menuBackground,
			"MenuItem.foreground", commonForeground,
			"MenuItem.selectionBackground", buttonBackground,
			"MenuItem.selectionForeground", buttonForeground,
			"MenuItem.margin", new InsetsUIResource(2, 2, 2, 2),

			"Panel.background", commonBackground,
			"Panel.foreground", commonForeground,

			"RadioButton.font", commonFont,
			"RadioButton.background", commonBackground,
			"RadioButton.foreground", commonForeground,
			"RadioButton.icon", new IconUIResource(ubull),

			"ScrollPane.margin", new InsetsUIResource(8, 8, 8, 8),
			"ScrollPane.border", borderLowered,
			"ScrollPane.background", commonBackground,

			"ScrollBar.track", menuBackground,
			"ScrollBar.thumb", buttonBackground
		};

		table.putDefaults( defaults );
	}
}

