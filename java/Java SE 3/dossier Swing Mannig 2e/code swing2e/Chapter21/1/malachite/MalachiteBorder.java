/** 
 *  Copyright 1999-2002 Matthew Robinson and Pavel Vorobiev. 
 *  All Rights Reserved. 
 * 
 *  =================================================== 
 *  This program contains code from the book "Swing" 
 *  2nd Edition by Matthew Robinson and Pavel Vorobiev 
 *  http://www.spindoczine.com/sbe 
 *  =================================================== 
 * 
 *  The above paragraph must be included in full, unmodified 
 *  and completely intact in the beginning of any source code 
 *  file that references, copies or uses (in any way, shape 
 *  or form) code contained in this file. 
 */ 

package malachite;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

public class MalachiteBorder
	implements Border
{
	public static final int RAISED = 0;
	public static final int LOWERED = 1;

	static final String IMAGE_DIR = "malachite"+java.io.File.separator;

	static final ImageIcon IMAGE_NW = new ImageIcon(
		IMAGE_DIR+"nw.gif");
	static final ImageIcon IMAGE_N  = new ImageIcon(
		IMAGE_DIR+"n.gif");
	static final ImageIcon IMAGE_NE = new ImageIcon(
		IMAGE_DIR+"ne.gif");
	static final ImageIcon IMAGE_E  = new ImageIcon(
		IMAGE_DIR+"e.gif");
	static final ImageIcon IMAGE_SE = new ImageIcon(
		IMAGE_DIR+"se.gif");
	static final ImageIcon IMAGE_S  = new ImageIcon(
		IMAGE_DIR+"s.gif");
	static final ImageIcon IMAGE_SW = new ImageIcon(
		IMAGE_DIR+"sw.gif");
	static final ImageIcon IMAGE_W  = new ImageIcon(
		IMAGE_DIR+"w.gif");

	static final ImageIcon IMAGE_L_NW = new ImageIcon(
		IMAGE_DIR+"l_nw.gif");
	static final ImageIcon IMAGE_L_N  = new ImageIcon(
		IMAGE_DIR+"l_n.gif");
	static final ImageIcon IMAGE_L_NE = new ImageIcon(
		IMAGE_DIR+"l_ne.gif");
	static final ImageIcon IMAGE_L_E  = new ImageIcon(
		IMAGE_DIR+"l_e.gif");
	static final ImageIcon IMAGE_L_SE = new ImageIcon(
		IMAGE_DIR+"l_se.gif");
	static final ImageIcon IMAGE_L_S  = new ImageIcon(
		IMAGE_DIR+"l_s.gif");
	static final ImageIcon IMAGE_L_SW = new ImageIcon(
		IMAGE_DIR+"l_sw.gif");
	static final ImageIcon IMAGE_L_W  = new ImageIcon(
		IMAGE_DIR+"l_w.gif");

	protected int m_w = 7;
	protected int m_h = 7;

	protected boolean m_isRaised = true;

	public MalachiteBorder()
	{
	}

	public MalachiteBorder(int type)
	{
		if (type != RAISED && type != LOWERED)
			throw new IllegalArgumentException(
					"Type must be RAISED or LOWERED");
		m_isRaised = (type == RAISED);
	}

	public Insets getBorderInsets(Component c)
	{
		return new Insets(m_h, m_w, m_h, m_w);
	}

	public  boolean isBorderOpaque()
	{
		return true;
	}

	public void paintBorder(Component c, Graphics g,
		int x, int y, int w, int h)
	{
		int x1 = x+m_w;
		int x2 = x+w-m_w;
		int y1 = y+m_h;
		int y2 = y+h-m_h;
		int xx, yy;

		if (m_isRaised)
		{
			for (xx=x1; xx<=x2; xx += IMAGE_N.getIconWidth())
				g.drawImage(IMAGE_N.getImage(), xx, y, c);

			for (yy=y1; yy<=y2; yy += IMAGE_E.getIconHeight())
				g.drawImage(IMAGE_E.getImage(), x2, yy, c);

			for (xx=x1; xx<=x2; xx += IMAGE_S.getIconWidth())
				g.drawImage(IMAGE_S.getImage(), xx, y2, c);

			for (yy=y1; yy<=y2; yy += IMAGE_W.getIconHeight())
				g.drawImage(IMAGE_W.getImage(), x, yy, c);

			g.drawImage(IMAGE_NW.getImage(), x, y, c);
			g.drawImage(IMAGE_NE.getImage(), x2, y, c);
			g.drawImage(IMAGE_SE.getImage(), x2, y2, c);
			g.drawImage(IMAGE_SW.getImage(), x, y2, c);
		}
		else
		{
			for (xx=x1; xx<=x2; xx += IMAGE_L_N.getIconWidth())
				g.drawImage(IMAGE_L_N.getImage(), xx, y, c);

			for (yy=y1; yy<=y2; yy += IMAGE_L_E.getIconHeight())
				g.drawImage(IMAGE_L_E.getImage(), x2, yy, c);

			for (xx=x1; xx<=x2; xx += IMAGE_L_S.getIconWidth())
				g.drawImage(IMAGE_L_S.getImage(), xx, y2, c);

			for (yy=y1; yy<=y2; yy += IMAGE_L_W.getIconHeight())
				g.drawImage(IMAGE_L_W.getImage(), x, yy, c);

			g.drawImage(IMAGE_L_NW.getImage(), x, y, c);
			g.drawImage(IMAGE_L_NE.getImage(), x2, y, c);
			g.drawImage(IMAGE_L_SE.getImage(), x2, y2, c);
			g.drawImage(IMAGE_L_SW.getImage(), x, y2, c);
		}

	}
}

