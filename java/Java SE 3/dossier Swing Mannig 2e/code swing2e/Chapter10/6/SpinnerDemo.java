/** 
 *  Copyright 1999-2002 Matthew Robinson and Pavel Vorobiev. 
 *  All Rights Reserved. 
 * 
 *  =================================================== 
 *  This program contains code from the book "Swing" 
 *  2nd Edition by Matthew Robinson and Pavel Vorobiev 
 *  http://www.spindoczine.com/sbe 
 *  =================================================== 
 * 
 *  The above paragraph must be included in full, unmodified 
 *  and completely intact in the beginning of any source code 
 *  file that references, copies or uses (in any way, shape 
 *  or form) code contained in this file. 
 */ 

import java.awt.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;

class SpinnerDemo extends JFrame {

	public SpinnerDemo() {
		super("Spinner Demo (Dates)");

		JPanel p = new JPanel();
		p.setLayout(new BoxLayout(p, BoxLayout.X_AXIS));
		p.setBorder(new EmptyBorder(10, 10, 10, 10));
		p.add(new JLabel("Select date: "));

		SpinnerModel model = new SpinnerDateModel(
			new Date(), 	// Initial value
			null, 			// Minimum value
			null, 			// Maximum value
			Calendar.DAY_OF_MONTH		// Step
		);
		JSpinner spn = new JSpinner(model);
		p.add(spn);

		getContentPane().add(p, BorderLayout.NORTH);
		setSize(400,75);
	}

	public static void main( String args[] ) {
		SpinnerDemo mainFrame = new SpinnerDemo();
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setVisible(true);
	}
}
