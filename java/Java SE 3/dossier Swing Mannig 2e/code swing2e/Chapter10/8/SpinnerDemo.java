/** 
 *  Copyright 1999-2002 Matthew Robinson and Pavel Vorobiev. 
 *  All Rights Reserved. 
 * 
 *  =================================================== 
 *  This program contains code from the book "Swing" 
 *  2nd Edition by Matthew Robinson and Pavel Vorobiev 
 *  http://www.spindoczine.com/sbe 
 *  =================================================== 
 * 
 *  The above paragraph must be included in full, unmodified 
 *  and completely intact in the beginning of any source code 
 *  file that references, copies or uses (in any way, shape 
 *  or form) code contained in this file. 
 */ 

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.*;

class SpinnerDemo extends JFrame {

	public static final int PAGE_SIZE = 5;

	SpinnerNumberModel m_model;

	public SpinnerDemo() {
		super("Spinner Demo (Keys)");

		JPanel p = new JPanel();
		p.setLayout(new BoxLayout(p, BoxLayout.X_AXIS));
		p.setBorder(new EmptyBorder(10, 10, 10, 10));
		p.add(new JLabel("Use PgUp, PgDn, Ctrl-Home, Ctrl-End: "));

		m_model = new SpinnerNumberModel(0, 0, 100, 1);
		JSpinner spn = new JSpinner(m_model);
		p.add(spn);

		spn.registerKeyboardAction(new PgUpMover(),
			KeyStroke.getKeyStroke(KeyEvent.VK_PAGE_UP, 0),
			JComponent.WHEN_IN_FOCUSED_WINDOW);
		spn.registerKeyboardAction(new PgDnMover(),
			KeyStroke.getKeyStroke(KeyEvent.VK_PAGE_DOWN, 0),
			JComponent.WHEN_IN_FOCUSED_WINDOW);
		spn.registerKeyboardAction(new HomeMover(),
			KeyStroke.getKeyStroke(KeyEvent.VK_HOME, KeyEvent.CTRL_MASK),
			JComponent.WHEN_IN_FOCUSED_WINDOW);
		spn.registerKeyboardAction(new EndMover(),
			KeyStroke.getKeyStroke(KeyEvent.VK_END, KeyEvent.CTRL_MASK),
			JComponent.WHEN_IN_FOCUSED_WINDOW);

		getContentPane().add(p, BorderLayout.NORTH);
		setSize(400,75);
	}

	public static void main( String args[] ) {
		SpinnerDemo mainFrame = new SpinnerDemo();
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setVisible(true);
	}

	/**
	 * Moves Spinner's value PAGE_SIZE steps up
	 */
	class PgUpMover implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Integer newValue = new Integer(
				m_model.getNumber().intValue() +
				PAGE_SIZE*m_model.getStepSize().intValue());

			// Check maximum value, SpinnerNumberModel won't do it for us
			Comparable maximum = m_model.getMaximum();
			if (maximum != null && maximum.compareTo(newValue) < 0)
				return;

			m_model.setValue(newValue);
		}
	}

	/**
	 * Moves Spinner's value PAGE_SIZE steps down
	 */
	class PgDnMover implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Integer newValue = new Integer(
				m_model.getNumber().intValue() -
				PAGE_SIZE*m_model.getStepSize().intValue());

			// Check minimum value, SpinnerNumberModel won't do it for us
			Comparable minimum = m_model.getMinimum();
			if (minimum != null && minimum.compareTo(newValue) > 0)
				return;

			m_model.setValue(newValue);
		}
	}

	/**
	 * Moves Spinner's value to minimum
	 */
	class HomeMover implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Comparable minimum = m_model.getMinimum();
			if (minimum != null)
				m_model.setValue(minimum);
		}
	}

	/**
	 * Moves Spinner's value to maximum
	 */
	class EndMover implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Comparable maximum = m_model.getMaximum();
			if (maximum != null)
				m_model.setValue(maximum);
		}
	}

}
