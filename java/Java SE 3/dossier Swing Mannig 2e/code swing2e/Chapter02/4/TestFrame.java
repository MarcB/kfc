/** 
 *  Copyright 1999-2002 Matthew Robinson and Pavel Vorobiev. 
 *  All Rights Reserved. 
 * 
 *  =================================================== 
 *  This program contains code from the book "Swing" 
 *  2nd Edition by Matthew Robinson and Pavel Vorobiev 
 *  http://www.spindoczine.com/sbe 
 *  =================================================== 
 * 
 *  The above paragraph must be included in full, unmodified 
 *  and completely intact in the beginning of any source code 
 *  file that references, copies or uses (in any way, shape 
 *  or form) code contained in this file. 
 */ 

import java.awt.*;
import javax.swing.*;
import javax.swing.plaf.*;
import java.io.*;

class TestFrame extends JFrame
{
  public TestFrame() {
    super( "Graphics demo" );
    JCanvas jc = new JCanvas();
    RepaintManager.currentManager( jc ).setDoubleBufferingEnabled( false );
    jc.setDebugGraphicsOptions( DebugGraphics.LOG_OPTION |
      DebugGraphics.FLASH_OPTION);
    DebugGraphics.setFlashTime( 100 );
    DebugGraphics.setFlashCount( 2 );
    DebugGraphics.setFlashColor(new Color(0,0,0,0));
    getContentPane().add(jc);
  }

  public static void main( String args[] ) {
    TestFrame mainFrame = new TestFrame();
    mainFrame.pack();
    mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	// 1.3
    mainFrame.setVisible( true );
  }
}

class JCanvas extends JComponent
{
  private static Color m_tRed = new Color(255,0,0,150);
  private static Color m_tGreen = new Color(0,255,0,150);
  private static Color m_tBlue = new Color(0,0,255,150);

  private static Font m_biFont = new Font("Monospaced", Font.BOLD | Font.ITALIC, 36);
  private static Font m_pFont = new Font("SanSerif", Font.PLAIN, 12);
  private static Font m_bFont = new Font("Serif", Font.BOLD, 24);

  private PrintStream ps;
  
  private static ImageIcon m_flight = new ImageIcon("flight.gif");

  public JCanvas() {
    super.setUI(EmptyUI.createUI(this));
  }

  public void paintComponent(Graphics g) {
    super.paintComponent(g);

    ps = DebugGraphics.logStream();
    ps.println("\n===> paintComponent ENTERED <===");
  
    // counter
    int c = 0;

    // for use below
    int w = 0;
    int h = 0;
    int d = 0;

    // get damaged region
    Rectangle r = g.getClipBounds();
    int clipx = r.x;
    int clipy = r.y;
    int clipw = r.width;
    int cliph = r.height;

    // fill only damaged region only
    g.setColor(Color.white);
    g.fillRect(clipx,clipy,clipw,cliph);

    // filled yellow circle if bounding region has been damaged
    if (clipx <= 240 && clipy <= 240) {
      g.setColor(Color.yellow);
      g.fillOval(0,0,240,240); c++;
    }
        
    // filled magenta circle if bounding region has been damaged
    if (clipx + clipw >= 160 && clipx <= 400
        && clipy + cliph >= 160 && clipy <= 400) {
      g.setColor(Color.magenta);
      g.fillOval(160,160,240,240); c++;
    }

    w = m_flight.getIconWidth();
    h = m_flight.getIconHeight();
    // paint the icon below blue sqaure if bounding region has been damaged
    if (clipx + clipw >= 280-(w/2) && clipx <= (280+(w/2))
        && clipy + cliph >= 120-(h/2) && clipy <= (120+(h/2))) {
      m_flight.paintIcon(this,g,280-(w/2),120-(h/2)); c++;
    }

    // paint the icon below red sqaure if bounding region has been damaged
    if (clipx + clipw >= 120-(w/2) && clipx <= (120+(w/2))
        && clipy + cliph >= 280-(h/2) && clipy <= (280+(h/2))) {
      m_flight.paintIcon(this,g,120-(w/2),280-(h/2)); c++;
    }

    // filled transparent red square if bounding region has been damaged
    if (clipx + clipw >= 60 && clipx <= 180
        && clipy + cliph >= 220 && clipy <= 340) {
      g.setColor(m_tRed);
      g.fillRect(60,220,120,120); c++;
    }

    // filled transparent green circle if bounding region has been damaged
    if (clipx + clipw > 140 && clipx < 260
        && clipy + cliph > 140 && clipy < 260) {
      g.setColor(m_tGreen);
      g.fillOval(140,140,120,120); c++;
    }

    // filled transparent blue square if bounding region has been damaged
    if (clipx + clipw > 220 && clipx < 380
        && clipy + cliph > 60 && clipy < 180) {
      g.setColor(m_tBlue);
      g.fillRect(220,60,120,120); c++;
    }

    g.setColor(Color.black);

    g.setFont(m_biFont);
    FontMetrics fm = g.getFontMetrics();
    w = fm.stringWidth("Swing");
    h = fm.getAscent();
    d = fm.getDescent();
    // Bold, Italic, 36-point "Swing" if bounding region has been damaged
    if (clipx + clipw > 120-(w/2) && clipx < (120+(w/2))
        && clipy + cliph > (120+(h/4))-h && clipy < (120+(h/4))+d) {
      g.drawString("Swing",120-(w/2),120+(h/4)); c++;
    }

    g.setFont(m_pFont);
    fm = g.getFontMetrics();
    w = fm.stringWidth("is");
    h = fm.getAscent();
    d = fm.getDescent();
    // Plain, 12-point "is" if bounding region has been damaged
    if (clipx + clipw > 200-(w/2) && clipx < (200+(w/2))
        && clipy + cliph > (200+(h/4))-h && clipy < (200+(h/4))+d) {
      g.drawString("is",200-(w/2),200+(h/4)); c++;
    }

    g.setFont(m_bFont);
    fm = g.getFontMetrics();
    w = fm.stringWidth("powerful!!");
    h = fm.getAscent();
    d = fm.getDescent();
    // Bold 24-point "powerful!!" if bounding region has been damaged
    if (clipx + clipw > 280-(w/2) && clipx < (280+(w/2))
        && clipy + cliph > (280+(h/4))-h && clipy < (280+(h/4))+d) {
      g.drawString("powerful!!",280-(w/2),280+(h/4)); c++;
    }

    ps.println("\n# items repainted = " + c + "/10");
    ps.println("===> paintComponent FINISHED <===\n");
  }
  
  // Some layout managers need this information
  public Dimension getPreferredSize() {
    return new Dimension(400,400);
  }

  // Some layout managers need this information
  public Dimension getMinimumSize() {
    return getPreferredSize();
  }
}

class EmptyUI extends ComponentUI 
{
  private static final EmptyUI sharedInstance = new EmptyUI();

  public static ComponentUI createUI(JComponent c) {
    return sharedInstance;
  }
}
