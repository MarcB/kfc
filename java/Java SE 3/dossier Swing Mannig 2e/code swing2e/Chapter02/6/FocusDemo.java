/** 
 *  Copyright 1999-2002 Matthew Robinson and Pavel Vorobiev. 
 *  All Rights Reserved. 
 * 
 *  =================================================== 
 *  This program contains code from the book "Swing" 
 *  2nd Edition by Matthew Robinson and Pavel Vorobiev 
 *  http://www.spindoczine.com/sbe 
 *  =================================================== 
 * 
 *  The above paragraph must be included in full, unmodified 
 *  and completely intact in the beginning of any source code 
 *  file that references, copies or uses (in any way, shape 
 *  or form) code contained in this file. 
 */ 

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;

public class FocusDemo extends JFrame {

  public FocusDemo() {
    super("Focus Demo");

    JPanel contentPane = (JPanel) getContentPane();
    contentPane.setBorder(new TitledBorder("Focus Cycle A"));
    contentPane.add(createComponentPanel(), BorderLayout.NORTH);
    JDesktopPane desktop1 = new JDesktopPane();
    contentPane.add(desktop1, BorderLayout.CENTER);

    JInternalFrame internalFrame1 = new JInternalFrame("Focus Cycle B", true, true, true, true);
    contentPane = (JPanel) internalFrame1.getContentPane();
    contentPane.add(createComponentPanel(), BorderLayout.NORTH);
    JDesktopPane desktop2 = new JDesktopPane();
    contentPane.add(desktop2, BorderLayout.CENTER);
    desktop1.add(internalFrame1);
    internalFrame1.setBounds(20,20,500,300);
    internalFrame1.show();

    JInternalFrame internalFrame2 = new JInternalFrame("Focus Cycle C", true, true, true, true);
    contentPane = (JPanel) internalFrame2.getContentPane();
    contentPane.add(createComponentPanel(), BorderLayout.NORTH);
    JDesktopPane desktop3 = new JDesktopPane();
    contentPane.add(desktop3, BorderLayout.CENTER);
    desktop2.add(internalFrame2);
    internalFrame2.setBounds(20,20,400,200);
    internalFrame2.show();

    JInternalFrame internalFrame3 = new JInternalFrame("Focus Cycle D", false, true, true, true);
    contentPane = (JPanel) internalFrame3.getContentPane();
    contentPane.add(createComponentPanel(), BorderLayout.NORTH);
    desktop3.add(internalFrame3);
    internalFrame3.setBounds(20,20,300,100);
    internalFrame3.show();
  }

  public static void main(String[] args) {
    FocusDemo frame = new FocusDemo();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	// 1.3
    frame.setVisible(true);
    frame.setBounds(0,0,600,450);
  }

  protected JPanel createComponentPanel() {
    JPanel panel = new JPanel();
    panel.add(new JButton("Button 1"));
    panel.add(new JButton("Button 2"));
    panel.add(new JTextField(10));
    return panel;
  }
}
