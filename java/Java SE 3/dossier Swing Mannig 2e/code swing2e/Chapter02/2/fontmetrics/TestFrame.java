/** 
 *  Copyright 1999-2002 Matthew Robinson and Pavel Vorobiev. 
 *  All Rights Reserved. 
 * 
 *  =================================================== 
 *  This program contains code from the book "Swing" 
 *  2nd Edition by Matthew Robinson and Pavel Vorobiev 
 *  http://www.spindoczine.com/sbe 
 *  =================================================== 
 * 
 *  The above paragraph must be included in full, unmodified 
 *  and completely intact in the beginning of any source code 
 *  file that references, copies or uses (in any way, shape 
 *  or form) code contained in this file. 
 */ 

import java.awt.*;
import javax.swing.*;

class TestFrame extends JFrame
{
  public TestFrame() {
    super( "Lets get it straight!" );
    getContentPane().add(new JCanvas());
  }

  public static void main( String args[] ) {
    TestFrame mainFrame = new TestFrame();
    mainFrame.pack();
    mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	// 1.3
    mainFrame.setVisible( true );
  }
}

class JCanvas extends JComponent
{
  private static Font m_biFont = new Font("Monospaced", Font.BOLD, 36);

  public void paintComponent(Graphics g) {
    g.setColor(Color.black);

    // Bold 36-point "Swing"
    g.setFont(m_biFont);
    FontMetrics fm = g.getFontMetrics();
    int h = fm.getAscent();

    g.drawString("Swing",50,50); // Try these as well: � � � ^

    // draw Ascent line
    g.drawLine(10,50-h,190,50-h);

    // draw Ascent/2 line
    g.drawLine(10,50-(h/2),190,50-(h/2));
    
    // draw Ascent/4 line
    g.drawLine(10,50-(h/4),190,50-(h/4));

    // draw baseline line
    g.drawLine(10,50,190,50);

    // draw Descent line
    g.drawLine(10,50+fm.getDescent(),190,50+fm.getDescent());
  }
  
  public Dimension getPreferredSize() {
    return new Dimension(200,100);
  }
}