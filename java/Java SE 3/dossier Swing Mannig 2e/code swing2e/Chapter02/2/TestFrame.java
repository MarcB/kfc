/** 
 *  Copyright 1999-2002 Matthew Robinson and Pavel Vorobiev. 
 *  All Rights Reserved. 
 * 
 *  =================================================== 
 *  This program contains code from the book "Swing" 
 *  2nd Edition by Matthew Robinson and Pavel Vorobiev 
 *  http://www.spindoczine.com/sbe 
 *  =================================================== 
 * 
 *  The above paragraph must be included in full, unmodified 
 *  and completely intact in the beginning of any source code 
 *  file that references, copies or uses (in any way, shape 
 *  or form) code contained in this file. 
 */ 

import java.awt.*;
import javax.swing.*;

class TestFrame extends JFrame {

  public TestFrame() {
    super( "Graphics demo" );
    getContentPane().add(new JCanvas());
  }

  public static void main( String args[] ) {
    TestFrame mainFrame = new TestFrame();
    mainFrame.pack();
    mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	// 1.3
    mainFrame.setVisible(true);
  }
}

class JCanvas extends JComponent {

  private static Color m_tRed = new Color(255,0,0,150);
  private static Color m_tGreen = new Color(0,255,0,150);
  private static Color m_tBlue = new Color(0,0,255,150);

  private static Font m_biFont = new Font("Monospaced", Font.BOLD | Font.ITALIC, 36);
  private static Font m_pFont = new Font("SanSerif", Font.PLAIN, 12);
  private static Font m_bFont = new Font("Serif", Font.BOLD, 24);
  
  private static ImageIcon m_flight = new ImageIcon("flight.gif");

  public void paintComponent(Graphics g) {
    super.paintComponent(g);

    // fill entire component white
    g.setColor(Color.white);
    g.fillRect(0,0,getWidth(),getHeight());

    // filled yellow circle
    g.setColor(Color.yellow);
    g.fillOval(0,0,240,240);
        
    // filled magenta circle
    g.setColor(Color.magenta);
    g.fillOval(160,160,240,240);

    // paint the icon below blue sqaure
    int w = m_flight.getIconWidth();
    int h = m_flight.getIconHeight();
    m_flight.paintIcon(this,g,280-(w/2),120-(h/2));

    // paint the icon below red sqaure
    m_flight.paintIcon(this,g,120-(w/2),280-(h/2));

    // filled transparent red square
    g.setColor(m_tRed);
    g.fillRect(60,220,120,120);

    // filled transparent green circle
    g.setColor(m_tGreen);
    g.fillOval(140,140,120,120);

    // filled transparent blue square
    g.setColor(m_tBlue);
    g.fillRect(220,60,120,120);

    g.setColor(Color.black);

    // Bold, Italic, 36-point "Swing"
    g.setFont(m_biFont);
    FontMetrics fm = g.getFontMetrics();
    w = fm.stringWidth("Swing");
    h = fm.getAscent();
    g.drawString("Swing",120-(w/2),120+(h/4));

    // Plain, 12-point "is"
    g.setFont(m_pFont);
    fm = g.getFontMetrics();
    w = fm.stringWidth("is");
    h = fm.getAscent();
    g.drawString("is",200-(w/2),200+(h/4));

    // Bold 24-point "powerful!!"
    g.setFont(m_bFont);
    fm = g.getFontMetrics();
    w = fm.stringWidth("powerful!!");
    h = fm.getAscent();
    g.drawString("powerful!!",280-(w/2),280+(h/4));
  }
  
  // Some layout managers need this information
  public Dimension getPreferredSize() {
    return new Dimension(400,400);
  }

  // Some layout managers need this information
  public Dimension getMinimumSize() {
    return getPreferredSize();
  }
}