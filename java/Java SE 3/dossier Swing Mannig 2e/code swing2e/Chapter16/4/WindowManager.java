/** 
 *  Copyright 1999-2002 Matthew Robinson and Pavel Vorobiev. 
 *  All Rights Reserved. 
 * 
 *  =================================================== 
 *  This program contains code from the book "Swing" 
 *  2nd Edition by Matthew Robinson and Pavel Vorobiev 
 *  http://www.spindoczine.com/sbe 
 *  =================================================== 
 * 
 *  The above paragraph must be included in full, unmodified 
 *  and completely intact in the beginning of any source code 
 *  file that references, copies or uses (in any way, shape 
 *  or form) code contained in this file. 
 */ 

import javax.swing.*;
import java.io.*;
import java.awt.*;

public class WindowManager 
extends DefaultDesktopManager
{
  //ID 16 means new Frame
  protected static final int ACIVATE_ID = 1;
  protected static final int BEGINDRAG_ID = 2;
  protected static final int BEGINRESIZE_ID = 3;
  protected static final int CLOSE_ID = 4;
  protected static final int DEACTIVATE_ID = 5;
  protected static final int DEICONIFY_ID = 6;
  protected static final int DRAG_ID = 7;
  protected static final int ENDDRAG_ID = 8;
  protected static final int ENDRESIZE_ID = 9;
  protected static final int ICONIFY_ID = 10;
  protected static final int MAXIMIZE_ID = 11;
  protected static final int MINIMIZE_ID = 12;
  protected static final int OPEN_ID = 13;
  protected static final int RESIZE_ID = 14;
  protected static final int SETBOUNDS_ID = 15;
  protected WindowWatcher ww;
  protected DataOutputStream m_output;
  protected JDesktopPane m_desktop;
  protected boolean m_prop;

  public WindowManager(JDesktopPane desktop) {
    m_desktop = desktop;
    m_prop = true;
    ww = new WindowWatcher(desktop);
  } 

  public WindowWatcher getWindowWatcher() { return ww; }

  public void setOutputStream(DataOutputStream output) {
    m_output = output;
  }

  public void sendMessage(String s) {
    try {
      if (m_output != null)
        m_output.writeUTF(s);
    }
    catch (IOException e) {}
  }

  public void setPropagate(boolean b) {
    m_prop = b;
  }

  public String getStringIndex(Component f) {
    String s = f.toString();
    while (s.length() < 3)
      s = ("0").concat(s);    
    return s;
  }

  public String getString(int number) {
    String s;
    if(number < 0) 
      s = "" + (-number);
    else
      s = "" + number;
    while (s.length() < 6)
      s = ("0").concat(s);    
    if (number < 0)
      s = "-" + s.substring(1,6);
    return s;
  }

  public void activateFrame(JInternalFrame f) {
    String index = getStringIndex(f);
    super.activateFrame(f);
    ww.repaint();
    if (m_prop)
      sendMessage("01" + index + "000000000000000000000000");
  }
  public void beginDraggingFrame(JComponent f) {
    String index = getStringIndex(f);
    super.beginDraggingFrame(f);
    ww.repaint();
    if (m_prop)
      sendMessage("02" + index + "000000000000000000000000");
  }
  public void beginResizingFrame(JComponent f, int direction) {
    String index = getStringIndex(f);
    String dir = getString(direction);
    super.beginResizingFrame(f,direction);
    ww.repaint();
    if (m_prop)
      sendMessage("03" + index + dir + "000000000000000000");
  }
  public void closeFrame(JInternalFrame f) {
    String index = getStringIndex(f);
    super.closeFrame(f);
    ww.repaint();
    if (m_prop)
      sendMessage("04" + index + "000000000000000000000000");
  }
  public void deactivateFrame(JInternalFrame f) {
    super.deactivateFrame(f);
    ww.repaint();
    // ID 05 - not implemented
  }
  public void deiconifyFrame(JInternalFrame f) {
    super.deiconifyFrame(f);
    ww.repaint();
    // ID 06 - not implemented
  }
  public void dragFrame(JComponent f, int newX, int newY) {
    String index = getStringIndex(f);
    String x = getString(newX);
    String y = getString(newY);
    f.setLocation(newX, newY);
    ww.repaint();
    if (m_prop)
      sendMessage("07" + index + x + y +"000000000000");
  }
  public void endDraggingFrame(JComponent f) {
    String index = getStringIndex(f);
    super.endDraggingFrame(f);
    ww.repaint();
    if (m_prop)
      sendMessage("08" + index + "000000000000000000000000");
  }
  public void endResizingFrame(JComponent f) {
    String index = getStringIndex(f);
    super.endResizingFrame(f);
    ww.repaint();
    if (m_prop)
      sendMessage("09" + index + "000000000000000000000000");
  }
  public void iconifyFrame(JInternalFrame f) {
    super.iconifyFrame(f);
    ww.repaint();
    // ID 10 - not implemented
  }
  public void maximizeFrame(JInternalFrame f) {
    String index = getStringIndex(f);
    super.maximizeFrame(f);
    ww.repaint();
    // ID 11 - not implemented
  }
  public void minimizeFrame(JInternalFrame f) {
    super.minimizeFrame(f);
    ww.repaint();
    // ID 12 - not implemented
  }
  public void openFrame(JInternalFrame f) {
    String index = getStringIndex(f);
    super.openFrame(f);
    ww.repaint();    
    if (m_prop)
      sendMessage("13" + index + "000000000000000000000000");
  }
  public void resizeFrame(JComponent f,
   int newX, int newY, int newWidth, int newHeight) {
    String index = getStringIndex(f);
    String x = getString(newX);
    String y = getString(newY);
    String w = getString(newWidth);
    String h = getString(newHeight);
    f.setBounds(newX, newY, newWidth, newHeight);
    ww.repaint();
    if (m_prop)
      sendMessage("14" + index + x + y + w + h);
  }
  public void setBoundsForFrame(JComponent f,
   int newX, int newY, int newWidth, int newHeight) {
    String index = getStringIndex(f);
    String x = getString(newX);
    String y = getString(newY);
    String w = getString(newWidth);
    String h = getString(newHeight);
    if (newWidth > m_desktop.getWidth())
      newWidth = m_desktop.getWidth();
    if (newHeight > m_desktop.getHeight())
      newHeight = m_desktop.getHeight();
    f.setBounds(newX, newY, newWidth, newHeight);
    ww.repaint();
    if (m_prop)
      sendMessage("15" + index + x + y + w + h);
  }
}