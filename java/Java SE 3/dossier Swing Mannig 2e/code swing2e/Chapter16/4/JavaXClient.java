/** 
 *  Copyright 1999-2002 Matthew Robinson and Pavel Vorobiev. 
 *  All Rights Reserved. 
 * 
 *  =================================================== 
 *  This program contains code from the book "Swing" 
 *  2nd Edition by Matthew Robinson and Pavel Vorobiev 
 *  http://www.spindoczine.com/sbe 
 *  =================================================== 
 * 
 *  The above paragraph must be included in full, unmodified 
 *  and completely intact in the beginning of any source code 
 *  file that references, copies or uses (in any way, shape 
 *  or form) code contained in this file. 
 */ 

import java.beans.PropertyVetoException;
import javax.swing.*;
import java.awt.event.*;
import java.io.*;
import java.awt.*;
import java.net.*;

public class JavaXClient
extends JFrame
implements Runnable
{
  protected int m_count;
  protected int m_tencount;
  protected int m_wmX, m_wmY;
  protected JButton m_newFrame;
  protected JDesktopPane m_desktop;
  protected WindowManager m_wm;
  protected JViewport viewport;

  protected JTextArea m_consoletext, m_consolechat;
  protected JTextField m_text, m_chatText;
  protected boolean m_connected;
  protected JLabel m_status;
  protected DataInputStream m_input;
  protected DataOutputStream m_output;
  protected Socket m_client;
  protected Thread m_listenThread;

  protected JButton m_connect;
  
  public JavaXClient() {
    setTitle("JavaX Client");
    m_count = m_tencount = 0;
    m_desktop = new JDesktopPane();

    m_status = new JLabel("Not Connected");

    JScrollPane scroller = new JScrollPane();
    m_wm = new WindowManager(m_desktop);
    m_desktop.setDesktopManager(m_wm);
    m_desktop.add(m_wm.getWindowWatcher(),
      JLayeredPane.PALETTE_LAYER);
    m_wm.getWindowWatcher().setBounds(555,5,100,100);

    viewport = new JViewport() {
      public void setViewPosition(Point p) { 
        super.setViewPosition(p); 
        m_wm.getWindowWatcher().setLocation(
          m_wm.getWindowWatcher().getX() +
            (getViewPosition().x-m_wmX),
          m_wm.getWindowWatcher().getY() +
            (getViewPosition().y-m_wmY));
        m_wmX = getViewPosition().x;
        m_wmY = getViewPosition().y;
      }
    };
    viewport.setView(m_desktop);
    scroller.setViewport(viewport);

    ComponentAdapter ca = new ComponentAdapter() {
      JViewport view = viewport;
      public void componentResized(ComponentEvent e) {
        m_wm.getWindowWatcher().setLocation(
          view.getViewPosition().x + view.getWidth()-
            m_wm.getWindowWatcher().getWidth()-15,
          view.getViewPosition().y + 5);
      }
    };
    viewport.addComponentListener(ca);
      
    m_newFrame = new JButton("New Frame");
    m_newFrame.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        m_wm.setPropagate(false);
        newFrame();
        if (m_connected)
          m_wm.sendMessage("16000000000000000000000000000");
        m_wm.setPropagate(true);
      }
    });
    m_newFrame.setEnabled(false);

    JPanel topPanel = new JPanel(true);
    topPanel.add(m_newFrame);

    m_connect = new JButton("Connect");
    m_connect.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        if (m_listenThread == null) {
          Thread connector = new Thread() {
            public void run() {
              try 
              {
                m_consoletext.append(
                  "\nTrying " + m_text.getText() + " ...");
                m_client = new Socket(
                  InetAddress.getByName(m_text.getText()),5000);
                m_input = new DataInputStream(
                  m_client.getInputStream());
                m_output = new DataOutputStream(
                  m_client.getOutputStream());
                m_connected = true;
                m_listenThread = new Thread(JavaXClient.this);
                m_listenThread.start();
                m_wm.setOutputStream(m_output);
                m_consoletext.append("\nStreams established...");
                m_status.setText("Connected to " + m_text.getText());
                m_connect.setEnabled(false);
                m_newFrame.setEnabled(true);
              }
              catch (Exception ex) {
                m_consoletext.append("\n" + ex);
                m_newFrame.setEnabled(false);
              }
            }
          };
          connector.start();
        }
      }
    });

    JPanel XPanel = new JPanel(); 
    XPanel.setLayout(new BorderLayout());
    JLabel hl = new JLabel("Connect to: ", SwingConstants.CENTER);
    m_text = new JTextField(15);
    XPanel.add("North", hl);
    XPanel.add("Center", m_text);
    XPanel.add("East", m_connect);
    
    JPanel upperPanel = new JPanel();
    upperPanel.setLayout(new BorderLayout());
    upperPanel.add("Center", XPanel);
    upperPanel.add("East",topPanel);

    getContentPane().setLayout(new BorderLayout());
    getContentPane().add("North", upperPanel);
    getContentPane().add("Center", scroller);

    getContentPane().add("South", m_status);

    topPanel.add(m_newFrame);

    setupConsole();

    Dimension dim = getToolkit().getScreenSize();
    setSize(800,600);
    setLocation(dim.width/2-getWidth()/2,
      dim.height/2-getHeight()/2);
    m_desktop.setPreferredSize(new Dimension(1600,1200));
    WindowListener l = new WindowAdapter() {
      public void windowClosing(WindowEvent e) {
        System.exit(0);
      }
    };       
    addWindowListener(l);

    setVisible(true);
  }

  public void setupConsole() {
    JInternalFrame console = new JInternalFrame(
     "JavaX Client Console", 
     false, false, false, false) {
      int TAG = m_count;
      public String toString() {
        return "" + TAG;
      }
    }; 
    m_count++;
    console.setBounds(20, 20, 500, 300);
    console.setVisible(true);

    JPanel chatPanel = new JPanel();
    JLabel chatLabel = new JLabel(" Chat");
    chatPanel.setLayout(new BorderLayout());

    m_consoletext = new JTextArea();
    m_consoletext.setPreferredSize(new Dimension(500,50));
    m_consoletext.setLineWrap(true);

    m_consoletext.setText("Client Started...");

    m_consoletext.setEditable(false);

    m_consolechat = new JTextArea();
    m_consolechat.setLineWrap(true);
    m_consolechat.setEditable(false);

    m_chatText = new JTextField();
    m_chatText.addActionListener(new ChatAdapter());

    JButton chatSend = new JButton("Send");
    chatSend.addActionListener(new ChatAdapter());

    JPanel sendPanel = new JPanel();

    sendPanel.setLayout(new BorderLayout());
    sendPanel.add("Center", m_chatText);
    sendPanel.add("West", chatSend);

    JScrollPane cscroller1 = new JScrollPane(m_consoletext);
    JScrollPane cscroller2 = new JScrollPane(m_consolechat);

    chatPanel.add("North", chatLabel);
    chatPanel.add("Center", cscroller2);
    chatPanel.add("South", sendPanel);

    JSplitPane splitter = new JSplitPane(
      JSplitPane.VERTICAL_SPLIT, true, cscroller1, chatPanel);

    console.getContentPane().add(splitter);
    m_desktop.add(console);

    m_wm.getWindowWatcher().repaint();
  }

  public void run() {
    while (m_connected) {
      try {
        processMessage(m_input.readUTF());
      }
      catch (IOException e) {
        m_consoletext.append("\n" + e);
        m_connected = false;
      }
    }
  }

  public void newFrame() {
    JInternalFrame jif = new JInternalFrame("Frame " + m_count, 
     true, true, false, false) {
      int TAG = m_count;
      public String toString() {
        return "" + TAG;
      }
    }; 
    jif.setBounds(20*(m_count%10) + m_tencount*80, 
      20*(m_count%10), 200, 200);

    m_desktop.add(jif);
    try {            
      jif.setSelected(true);        
    }
    catch (PropertyVetoException pve) {
      System.out.println("Could not select " + jif.getTitle());
    }

    jif.setVisible(true);

    m_count++;
    if (m_count%10 == 0) {
      if (m_tencount < 3)
        m_tencount++;
      else 
        m_tencount = 0;
    }
  }

  public void processMessage(String s) {
    if (s.startsWith("cc")) {
      m_consolechat.append("SERVER: " + s.substring(2) + "\n");
      m_consolechat.setCaretPosition(
        m_consolechat.getText().length());
    }
    else {
      int id = (Integer.valueOf(s.substring(0,2))).intValue();
      m_wm.setPropagate(false);
      if (id == 16) {
        newFrame();
      }
      else {
        Component[] components = m_desktop.getComponentsInLayer(0); 
        int index = 0;
        int tag = (Integer.valueOf(s.substring(2,5))).intValue();
        int param1 = 
          (Integer.valueOf(s.substring(5,11))).intValue();
        int param2 = 
          (Integer.valueOf(s.substring(11,17))).intValue();
        int param3 = 
          (Integer.valueOf(s.substring(17,23))).intValue();
        int param4 = 
          (Integer.valueOf(s.substring(23))).intValue();
        boolean found = false;
        for (int i=components.length-1; i>-1;i--) {
          if (components[i] instanceof JInternalFrame) {
            if (Integer.valueOf(
             components[i].toString()).intValue() == tag) {
              try {            
                ((JInternalFrame) components[i]).setSelected(true);
                ((JInternalFrame) components[i]).toFront();
                index = i;
                found = true;
                break;       
              }
              catch (PropertyVetoException pve) {
                System.out.println(
                 "Could not select JInternalFrame with tag " + tag);
              }
            }
          }
        }
        if (found == false) return;
        switch (id)
        {
          case 1:
            m_wm.activateFrame((JInternalFrame) components[index]);
            break;
          case 2:
            m_wm.beginDraggingFrame((JComponent) components[index]);
            break;
          case 3:
            m_wm.beginResizingFrame(
             (JComponent) components[index], param1);
            break;
          case 4:
            m_wm.closeFrame((JInternalFrame) components[index]);
            break;
          // case 5: not implemented
          // case 6: not implemented
          case 7:
            m_wm.dragFrame(
              (JComponent)components[index], param1, param2);
            break;  
          case 8:
            m_wm.endDraggingFrame((JComponent) components[index]);
            break;
          case 9:
            m_wm.endResizingFrame((JComponent) components[index]);
            break;
          // case 10: not implemented
          // case 11: not implemented
          // case 12: not implemented
          case 13:
            m_wm.openFrame((JInternalFrame) components[index]);
            break;
          case 14:
            m_wm.resizeFrame(
              (JComponent) components[index], param1, 
                param2, param3, param4);
            break;
          case 15:
            m_wm.setBoundsForFrame(
              (JComponent) components[index], param1, 
                param2, param3, param4);
            break;
        }
      }
      m_wm.setPropagate(true);
    }
    m_desktop.repaint();
  }

  public static void main(String[] args) {
    new JavaXClient();
  }

  class ChatAdapter implements ActionListener {
    public void actionPerformed(ActionEvent e) {
      m_wm.sendMessage("cc" + m_chatText.getText());
      m_consolechat.append("CLIENT: " + m_chatText.getText() + "\n");
      m_chatText.setText("");
    }
  }
}