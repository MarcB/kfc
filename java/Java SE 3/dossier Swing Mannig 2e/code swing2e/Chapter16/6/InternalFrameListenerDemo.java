/** 
 *  Copyright 1999-2002 Matthew Robinson and Pavel Vorobiev. 
 *  All Rights Reserved. 
 * 
 *  =================================================== 
 *  This program contains code from the book "Swing" 
 *  2nd Edition by Matthew Robinson and Pavel Vorobiev 
 *  http://www.spindoczine.com/sbe 
 *  =================================================== 
 * 
 *  The above paragraph must be included in full, unmodified 
 *  and completely intact in the beginning of any source code 
 *  file that references, copies or uses (in any way, shape 
 *  or form) code contained in this file. 
 */ 

import java.beans.PropertyVetoException;
import javax.swing.plaf.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.*;

public class InternalFrameListenerDemo 
extends JFrame
implements ActionListener, InternalFrameListener
{
  protected int m_count;
  protected int m_tencount;
  private int[] m_counts = new int[7];
  private int m_open, m_closing, m_close, m_iconify, 
    m_deiconify, m_activate, m_deactivate;
  protected JButton m_newFrame;
  protected JDesktopPane m_desktop;
  protected JComboBox m_UIBox;
  protected UIManager.LookAndFeelInfo[] m_infos;
  private JLabel m_lOpened, m_lClosing, m_lClosed, 
    m_lIconified, m_lDeiconified, m_lActivated, 
    m_lDeactivated;
  protected IFEventCanvas m_ifEventCanvas;
  protected Timer m_eventTimer;
    
  public InternalFrameListenerDemo() {
    setTitle("Animated InternalFrameListener");
    m_count = m_tencount = 0;

    JPanel innerListenerPanel = new JPanel(new GridLayout(7,1));
    JPanel listenerPanel = new JPanel(new BorderLayout());
    m_ifEventCanvas = new IFEventCanvas();
        
    m_lOpened = new JLabel("internalFrameOpened");
    m_lClosing = new JLabel("internalFrameClosing");
    m_lClosed = new JLabel("internalFrameClosed");
    m_lIconified = new JLabel("internalFrameIconified");
    m_lDeiconified = new JLabel("internalFrameDeiconified");
    m_lActivated = new JLabel("internalFrameActivated");
    m_lDeactivated = new JLabel("internalFrameDeactivated");

    innerListenerPanel.add(m_lOpened);
    innerListenerPanel.add(m_lClosing);
    innerListenerPanel.add(m_lClosed);
    innerListenerPanel.add(m_lIconified);
    innerListenerPanel.add(m_lDeiconified);
    innerListenerPanel.add(m_lActivated);
    innerListenerPanel.add(m_lDeactivated);

    listenerPanel.add("Center", m_ifEventCanvas);
    listenerPanel.add("West", innerListenerPanel);
    listenerPanel.setOpaque(true);
    listenerPanel.setBackground(Color.white);

    m_desktop = new JDesktopPane();
    m_desktop.setBorder(new SoftBevelBorder(BevelBorder.LOWERED));
    m_newFrame = new JButton("New Frame");
    m_newFrame.addActionListener(this);
    m_infos = UIManager.getInstalledLookAndFeels();
    String[] LAFNames = new String[m_infos.length];
    for(int i=0; i<m_infos.length; i++) {
      LAFNames[i] = m_infos[i].getName();
    }
    m_UIBox = new JComboBox(LAFNames);
    m_UIBox.addActionListener(this);
    JPanel topPanel = new JPanel(true);
    topPanel.setLayout(new FlowLayout());
    topPanel.setBorder(new CompoundBorder(
      new SoftBevelBorder(BevelBorder.LOWERED),
      new CompoundBorder(new EmptyBorder(2,2,2,2),
        new SoftBevelBorder(BevelBorder.RAISED))));
    getContentPane().setLayout(new BorderLayout());
    getContentPane().add("North", topPanel);
    getContentPane().add("Center", m_desktop);
    getContentPane().add("South", listenerPanel);
    ((JPanel) getContentPane()).setBorder(new CompoundBorder(
      new SoftBevelBorder(BevelBorder.LOWERED),
      new CompoundBorder(new EmptyBorder(1,1,1,1),
        new SoftBevelBorder(BevelBorder.RAISED))));
    topPanel.add(m_newFrame);
    topPanel.add(new JLabel("Look & Feel:",SwingConstants.RIGHT));
    topPanel.add(m_UIBox);
    setSize(645,500);
    Dimension dim = getToolkit().getScreenSize();
    setLocation(dim.width/2-getWidth()/2,
      dim.height/2-getHeight()/2);
    setVisible(true);
    WindowListener l = new WindowAdapter() {
      public void windowClosing(WindowEvent e) {
        System.exit(0);
      }
    };       
    addWindowListener(l);
    m_eventTimer = new Timer(1000, this);
    m_eventTimer.setRepeats(true);
    m_eventTimer.start();
  }

  public void newFrame() {
    JInternalFrame jif = new JInternalFrame("Frame " + m_count, 
      true, true, true, true);
    jif.addInternalFrameListener(this);
    jif.setBounds(20*(m_count%10) + m_tencount*80, 
      20*(m_count%10), 200, 200);
    JLabel label = new JLabel();
    label.setBackground(Color.white);
    label.setOpaque(true);
    jif.getContentPane().add(label);
    m_desktop.add(jif);
    try {            
      jif.setSelected(true);        
    }
    catch (PropertyVetoException pve) {
      System.out.println("Could not select " + jif.getTitle());
    }
    jif.setVisible(true);
    m_count++;
    if (m_count%10 == 0) {
      if (m_tencount < 3)
        m_tencount++;
      else 
        m_tencount = 0;
    }
  }

  public void clearCounts() {
    for (int i=0; i<7; i++) {
      m_counts[i] = 0;
    }
  }

  public int[] getCounts() {
    return m_counts;
  }

  public void internalFrameOpened(InternalFrameEvent e) {
    m_counts[0]++;    
  }
  public void internalFrameClosing(InternalFrameEvent e) {
    m_counts[1]++;    
  }
  public void internalFrameClosed(InternalFrameEvent e) {
    m_counts[2]++;    
  }
  public void internalFrameIconified(InternalFrameEvent e) {
    m_counts[3]++;    
  }
  public void internalFrameDeiconified(InternalFrameEvent e) {
    m_counts[4]++;    
  }
  public void internalFrameActivated(InternalFrameEvent e) {
    m_counts[5]++;
  }
  public void internalFrameDeactivated(InternalFrameEvent e) {
    m_counts[6]++;    
  }

  public void actionPerformed(ActionEvent e) {
    if (e.getSource() == m_newFrame)
      newFrame();
    else if (e.getSource() == m_eventTimer) {
      m_ifEventCanvas.render(getCounts());
      clearCounts();
    }
    else if (e.getSource() == m_UIBox) {
      try {
        m_UIBox.hidePopup(); //BUG WORKAROUND
        UIManager.setLookAndFeel(m_infos[m_UIBox.getSelectedIndex()].getClassName());
        SwingUtilities.updateComponentTreeUI(this);
      }
      catch(Exception ex) {
        System.out.println("Could not load " + 
          m_infos[m_UIBox.getSelectedIndex()].getClassName());
      }
    }
  }
 
  public static void main(String[] args) {
    new InternalFrameListenerDemo();
  }
}

class IFEventCanvas
extends JComponent
{
  private Color[] m_colors = new Color[8];
  private int[][] m_arrays = new int[15][12];

  public IFEventCanvas() {
    setPreferredSize(new Dimension(505,130));
    for (int i=0; i<7; i++) {
      m_arrays[i] = new int[12];
      m_colors[i] = new Color(37+i*14, 37+i*14, 37+i*14);
    }
  }

  public void paintEventSquare(Graphics g, int value, int currwidth, 
   int currheight, int cellwidth, int cellheight, Color color) {
    if(value != 0) {
      g.setColor(color);
      g.fillRect(currwidth, currheight, cellwidth, cellheight);
      g.setColor(Color.green);
      g.drawString("" + value, currwidth + 5, currheight + 14);
    }
    g.setColor(Color.black);
    g.drawRect(currwidth, currheight, cellwidth, cellheight);
  }

  public void paintComponent(Graphics g) {
    int cellheight = 19;
    int cellwidth = 40;
    int currwidth = 0;
    int currheight = 0;
    for (int i=0; i < 12; i++) {
      for (int j=0; j < 7; j++) {
        paintEventSquare(g, m_arrays[j][i], currwidth, currheight,
          cellwidth, cellheight, m_colors[j]);
        currheight += cellheight;
      }
      currheight = 0;
      currwidth += cellwidth;
    }
  }

  public void render(int[] counts) {
    for (int i=0; i < 11; i++) {
      for (int j=0; j < 7; j++) {            
        m_arrays[j][i] = m_arrays[j][i+1];
      }
    }
    for (int k=0; k < 7; k++) {
      m_arrays[k][11] = counts[k];
    }
    paintImmediately(new Rectangle(new Dimension(505,130)));
  }
}

		