/** 
 *  Copyright 1999-2002 Matthew Robinson and Pavel Vorobiev. 
 *  All Rights Reserved. 
 * 
 *  =================================================== 
 *  This program contains code from the book "Swing" 
 *  2nd Edition by Matthew Robinson and Pavel Vorobiev 
 *  http://www.spindoczine.com/sbe 
 *  =================================================== 
 * 
 *  The above paragraph must be included in full, unmodified 
 *  and completely intact in the beginning of any source code 
 *  file that references, copies or uses (in any way, shape 
 *  or form) code contained in this file. 
 */ 

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

import resize.*;

public class WindowWatcher 
extends JPanel
{
  protected static final Color C_UNSELECTED =
    new Color(123, 123, 123);
  protected static final Color C_SELECTED =
    new Color(243, 232, 165);
  protected static final Color C_BACKGROUND =
    new Color(5,165,165);
  protected static final Color C_WWATCHER =
    new Color(203,226,0);
  protected float m_widthratio, m_heightratio;
  protected int m_width, m_height, m_XDifference, m_YDifference;
  protected JDesktopPane m_desktop;
  protected NorthResizeEdge m_northResizer;
  protected SouthResizeEdge m_southResizer;
  protected EastResizeEdge m_eastResizer;
  protected WestResizeEdge m_westResizer;
  
  public WindowWatcher(JDesktopPane desktop) {
    m_desktop = desktop;
    setOpaque(true);

    m_northResizer = new NorthResizeEdge(this);
    m_southResizer = new SouthResizeEdge(this);
    m_eastResizer = new EastResizeEdge(this);
    m_westResizer = new WestResizeEdge(this);

    setLayout(new BorderLayout());
    add(m_northResizer, BorderLayout.NORTH);
    add(m_southResizer, BorderLayout.SOUTH);
    add(m_eastResizer, BorderLayout.EAST);
    add(m_westResizer, BorderLayout.WEST);

    MouseInputAdapter ma = new MouseInputAdapter() {
      public void mousePressed(MouseEvent e) {
        m_XDifference = e.getX();
        m_YDifference = e.getY();
      }
      public void mouseDragged(MouseEvent e) {
        int vx = 0;
        int vy = 0;
        if (m_desktop.getParent() instanceof JViewport) {
          vx = ((JViewport) 
            m_desktop.getParent()).getViewPosition().x;
          vy = ((JViewport) 
            m_desktop.getParent()).getViewPosition().y;
        }
        int w = m_desktop.getParent().getWidth();
        int h = m_desktop.getParent().getHeight();
        int x = getX();
        int y = getY();
        int ex = e.getX();
        int ey = e.getY(); 
        if ((ey + y > vy && ey + y < h+vy) && 
            (ex + x > vx && ex + x < w+vx)) 
        {
          setLocation(ex-m_XDifference + x, ey-m_YDifference + y);
        }
        else if (!(ey + y > vy && ey + y < h+vy) && 
                  (ex + x > vx && ex + x < w+vx)) 
        {
          if (!(ey + y > vy) && ey + y < h+vy)
            setLocation(ex-m_XDifference + x, vy-m_YDifference);
          else if (ey + y > vy && !(ey + y < h+vy))
            setLocation(ex-m_XDifference + x, (h+vy)-m_YDifference);
        }
        else if ((ey + y >vy && ey + y < h+vy) && 
                !(ex + x > vx && ex + x < w+vx)) 
        {
          if (!(ex + x > vx) && ex + x < w+vx)
            setLocation(vx-m_XDifference, ey-m_YDifference + y);
          else if (ex + x > vx && !(ex + x < w))
            setLocation((w+vx)-m_XDifference, ey-m_YDifference + y);
        }
        else if (!(ey + y > vy) && ey + y < h+vy && 
                 !(ex + x > vx) && ex + x < w+vx)
          setLocation(vx-m_XDifference, vy-m_YDifference);
        else if (!(ey + y > vy) && ey + y < h+vy && 
                 ex + x > vx && !(ex + x < w+vx)) 
          setLocation((w+vx)-m_XDifference, vy-m_YDifference);
        else if (ey + y > vy && !(ey + y < h+vy) && 
                 !(ex + x > vx) && ex + x < w+vx) 
          setLocation(vx-m_XDifference, (h+vy)-m_YDifference);
        else if (ey + y > vy && !(ey + y < h+vy) && 
                 ex + x > vx && !(ex + x < w+vx)) 
          setLocation((w+vx)-m_XDifference, (h+vy)-m_YDifference);
      }
      public void mouseEntered(MouseEvent e) {
        setCursor(Cursor.getPredefinedCursor(
          Cursor.MOVE_CURSOR));
      }
      public void mouseExited(MouseEvent e) {
        setCursor(Cursor.getPredefinedCursor(
          Cursor.DEFAULT_CURSOR));
      }
    };
    addMouseListener(ma);
    addMouseMotionListener(ma);
  } 

  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    m_height = getHeight();
    m_width = getWidth(); 
    g.setColor(C_BACKGROUND);
    g.fillRect(0,0,m_width,m_height);
    Component[] components = m_desktop.getComponents();
    m_widthratio = ((float) 
      m_desktop.getWidth())/((float) m_width);
    m_heightratio = ((float) 
      m_desktop.getHeight())/((float) m_height);
    for (int i=components.length-1; i>-1; i--) {
      if (components[i].isVisible()) {
        g.setColor(C_UNSELECTED);
        if (components[i] instanceof JInternalFrame) {
          if (((JInternalFrame) components[i]).isSelected())
            g.setColor(C_SELECTED);
        }
        else if(components[i] instanceof WindowWatcher)
          g.setColor(C_WWATCHER);
        g.fillRect(
          (int)(((float)components[i].getX())/m_widthratio),
          (int)(((float)components[i].getY())/m_heightratio),
          (int)(((float)components[i].getWidth())/m_widthratio), 
          (int)(((float)components[i].getHeight())/m_heightratio));
        g.setColor(Color.black);
        g.drawRect(
          (int)(((float)components[i].getX())/m_widthratio), 
          (int)(((float)components[i].getY())/m_heightratio),
          (int)(((float)components[i].getWidth())/m_widthratio), 
          (int)(((float)components[i].getHeight())/m_heightratio));
      }        
    }
    g.drawLine(m_width/2,0,m_width/2,m_height);
    g.drawLine(0,m_height/2,m_width,m_height/2);
  }
}
