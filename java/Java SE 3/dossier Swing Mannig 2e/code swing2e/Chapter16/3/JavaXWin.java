/** 
 *  Copyright 1999-2002 Matthew Robinson and Pavel Vorobiev. 
 *  All Rights Reserved. 
 * 
 *  =================================================== 
 *  This program contains code from the book "Swing" 
 *  2nd Edition by Matthew Robinson and Pavel Vorobiev 
 *  http://www.spindoczine.com/sbe 
 *  =================================================== 
 * 
 *  The above paragraph must be included in full, unmodified 
 *  and completely intact in the beginning of any source code 
 *  file that references, copies or uses (in any way, shape 
 *  or form) code contained in this file. 
 */ 

import java.beans.PropertyVetoException;
import javax.swing.*;
import java.awt.event.*;
import java.io.*;
import java.awt.*;

public class JavaXWin 
extends JFrame
{
  protected int m_count;
  protected int m_tencount;
  protected int m_wmX, m_wmY;
  protected JButton m_newFrame;
  protected JDesktopPane m_desktop;
  protected WindowManager m_wm;
  protected JViewport viewport;
  
  public JavaXWin() {
    setTitle("JavaXWin");
    m_count = m_tencount = 0;
    m_desktop = new JDesktopPane();

    JScrollPane scroller = new JScrollPane();
    m_wm = new WindowManager(m_desktop);
    m_desktop.setDesktopManager(m_wm);
    m_desktop.add(m_wm.getWindowWatcher(),
      JLayeredPane.PALETTE_LAYER);
    m_wm.getWindowWatcher().setBounds(555,5,200,150);

    viewport = new JViewport() {
      public void setViewPosition(Point p) { 
        super.setViewPosition(p); 
        m_wm.getWindowWatcher().setLocation(
          m_wm.getWindowWatcher().getX() +
            (getViewPosition().x-m_wmX),
          m_wm.getWindowWatcher().getY() +
            (getViewPosition().y-m_wmY));
        m_wmX = getViewPosition().x;
        m_wmY = getViewPosition().y;
      }
    };
    viewport.setView(m_desktop);
    scroller.setViewport(viewport);

    ComponentAdapter ca = new ComponentAdapter() {
      JViewport view = viewport;
      public void componentResized(ComponentEvent e) {
        m_wm.getWindowWatcher().setLocation(
          view.getViewPosition().x + view.getWidth()-
            m_wm.getWindowWatcher().getWidth()-15,
          view.getViewPosition().y + 5);
      }
    };
    viewport.addComponentListener(ca);
      
    m_newFrame = new JButton("New Frame");
    m_newFrame.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        newFrame();
      }
    });

    JPanel topPanel = new JPanel(true);
    topPanel.setLayout(new FlowLayout());

    getContentPane().setLayout(new BorderLayout());
    getContentPane().add("North", topPanel);
    getContentPane().add("Center", scroller);

    topPanel.add(m_newFrame);

    Dimension dim = getToolkit().getScreenSize();
    setSize(800,600);
    setLocation(dim.width/2-getWidth()/2,
      dim.height/2-getHeight()/2);
    m_desktop.setPreferredSize(new Dimension(1600,1200));
    setVisible(true);
    WindowListener l = new WindowAdapter() {
      public void windowClosing(WindowEvent e) {
        System.exit(0);
      }
    };       
    addWindowListener(l);
  }

  public void newFrame() {
    JInternalFrame jif = new JInternalFrame("Frame " + m_count, 
      true, true, true, true);
    jif.setBounds(20*(m_count%10) + m_tencount*80, 
      20*(m_count%10), 200, 200);

    JTextArea text = new JTextArea();
    JScrollPane scroller = new JScrollPane();
    scroller.getViewport().add(text);
    try {
      FileReader fileStream = new FileReader("JavaLinux.txt");
      text.read(fileStream, "JavaLinux.txt");
    }
    catch (Exception e) {
      text.setText("* Could not read JavaLinux.txt *");
    }
    jif.getContentPane().add(scroller);

    m_desktop.add(jif);
    try {            
      jif.setSelected(true);        
    }
    catch (PropertyVetoException pve) {
      System.out.println("Could not select " + jif.getTitle());
    }

    jif.setVisible(true);

    m_count++;
    if (m_count%10 == 0) {
      if (m_tencount < 3)
        m_tencount++;
      else 
        m_tencount = 0;
    }
  }

  public static void main(String[] args) {
    new JavaXWin();
  }
}

class WindowManager 
extends DefaultDesktopManager
{
  protected WindowWatcher ww;

  public WindowManager(JDesktopPane desktop) {
    ww = new WindowWatcher(desktop);
  }

  public WindowWatcher getWindowWatcher() { return ww; }
    
  public void activateFrame(JInternalFrame f) {
    super.activateFrame(f);
    ww.repaint();
  }
  public void beginDraggingFrame(JComponent f) { 
    super.beginDraggingFrame(f);
    ww.repaint();
  }
  public void beginResizingFrame(JComponent f, int direction) {
    super.beginResizingFrame(f,direction);
    ww.repaint();
  }
  public void closeFrame(JInternalFrame f) {
    super.closeFrame(f);
    ww.repaint();
  }
  public void deactivateFrame(JInternalFrame f) {
    super.deactivateFrame(f);
    ww.repaint();
  }
  public void deiconifyFrame(JInternalFrame f) {
    super.deiconifyFrame(f);
    ww.repaint();
  }
  public void dragFrame(JComponent f, int newX, int newY) {
    f.setLocation(newX, newY);
    ww.repaint();
  }
  public void endDraggingFrame(JComponent f) {
    super.endDraggingFrame(f);
    ww.repaint();
  }
  public void endResizingFrame(JComponent f) {
    super.endResizingFrame(f);
    ww.repaint();
  }
  public void iconifyFrame(JInternalFrame f) {
    super.iconifyFrame(f);
    ww.repaint();
  }
  public void maximizeFrame(JInternalFrame f) {
    super.maximizeFrame(f);
    ww.repaint();
  }
  public void minimizeFrame(JInternalFrame f) {
    super.minimizeFrame(f);
    ww.repaint();
  }
  public void openFrame(JInternalFrame f) {
    super.openFrame(f);
    ww.repaint();
  }
  public void resizeFrame(JComponent f,
   int newX, int newY, int newWidth, int newHeight) {
    f.setBounds(newX, newY, newWidth, newHeight);
    ww.repaint();
  }
  public void setBoundsForFrame(JComponent f,
   int newX, int newY, int newWidth, int newHeight) {
    f.setBounds(newX, newY, newWidth, newHeight);
    ww.repaint();
  }
}