/** 
 *  Copyright 1999-2002 Matthew Robinson and Pavel Vorobiev. 
 *  All Rights Reserved. 
 * 
 *  =================================================== 
 *  This program contains code from the book "Swing" 
 *  2nd Edition by Matthew Robinson and Pavel Vorobiev 
 *  http://www.spindoczine.com/sbe 
 *  =================================================== 
 * 
 *  The above paragraph must be included in full, unmodified 
 *  and completely intact in the beginning of any source code 
 *  file that references, copies or uses (in any way, shape 
 *  or form) code contained in this file. 
 */ 

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import dl.*;

public class TextDemo extends JFrame {
	protected JTextField m_firstTxt;
	protected JTextField m_lastTxt;
	protected JPasswordField m_passwordTxt;
	protected JTextArea  m_commentsTxt;

	public TextDemo() {
		super("Text Components Demo");
		Font monospaced = new Font("Monospaced", Font.PLAIN, 12);
		JPanel pp = new JPanel(new BorderLayout());

		JPanel p = new JPanel(new DialogLayout());
		p.setBorder(new EmptyBorder(10, 10, 10, 10));
		p.add(new JLabel("First name:"));
		m_firstTxt = new JTextField(20);
		p.add(m_firstTxt);

		p.add(new JLabel("Last name:"));
		m_lastTxt = new JTextField(20);
		p.add(m_lastTxt);

		p.add(new JLabel("Login password:"));
		m_passwordTxt = new JPasswordField(20);
		m_passwordTxt.setFont(monospaced);
		p.add(m_passwordTxt);

		p.setBorder(new CompoundBorder(
			new TitledBorder(new EtchedBorder(), "Personal Data"),
			new EmptyBorder(1, 5, 3, 5))
		);
		pp.add(p, BorderLayout.NORTH);

		m_commentsTxt = new JTextArea("", 4, 30);
		m_commentsTxt.setFont(monospaced);
		m_commentsTxt.setLineWrap(true);
		m_commentsTxt.setWrapStyleWord(true);
		p = new JPanel(new BorderLayout());
		p.add(new JScrollPane(m_commentsTxt));
		p.setBorder(new CompoundBorder(
			new TitledBorder(new EtchedBorder(), "Comments"),
			new EmptyBorder(3, 5, 3, 5))
		);
		pp.add(p, BorderLayout.CENTER);

		pp.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(pp);
		pack();
	}

	public static void main(String[] args) {
		JFrame frame = new TextDemo();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}
