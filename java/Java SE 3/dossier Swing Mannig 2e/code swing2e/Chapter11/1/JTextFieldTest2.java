/** 
 *  Copyright 1999-2002 Matthew Robinson and Pavel Vorobiev. 
 *  All Rights Reserved. 
 * 
 *  =================================================== 
 *  This program contains code from the book "Swing" 
 *  2nd Edition by Matthew Robinson and Pavel Vorobiev 
 *  http://www.spindoczine.com/sbe 
 *  =================================================== 
 * 
 *  The above paragraph must be included in full, unmodified 
 *  and completely intact in the beginning of any source code 
 *  file that references, copies or uses (in any way, shape 
 *  or form) code contained in this file. 
 */ 

import javax.swing.*;
import java.awt.*;

public class JTextFieldTest2 extends JFrame 
{
  public JTextFieldTest2() {
    super("JPasswordField Test");

    getContentPane().setLayout(new FlowLayout());

    UIManager.put("PasswordField.font", new Font("Monospaced", Font.PLAIN, 12));

    JPasswordField textField1 = new JPasswordField("m",2);
    JPasswordField textField2 = new JPasswordField("mm",3);
    JPasswordField textField3 = new JPasswordField("mmm",4);
    JPasswordField textField4 = new JPasswordField("mmmm",5);
    JPasswordField textField5 = new JPasswordField("mmmmm",6);
    JPasswordField textField6 = new JPasswordField("mmmmmm",7);
    JPasswordField textField7 = new JPasswordField("mmmmmmm",8);
    JPasswordField textField8 = new JPasswordField("mmmmmmmm",9);
    JPasswordField textField9 = new JPasswordField("mmmmmmmmm",10);
    JPasswordField textField10 = new JPasswordField("mmmmmmmmmm",11);
    JPasswordField textField11 = new JPasswordField("mmmmmmmmmmm",12);
    JPasswordField textField12 = new JPasswordField("mmmmmmmmmmmm",13);
    JPasswordField textField13 = new JPasswordField("mmmmmmmmmmmmm",14);
    JPasswordField textField14 = new JPasswordField("mmmmmmmmmmmmmm",15);

    getContentPane().add(textField1);
    getContentPane().add(textField2);
    getContentPane().add(textField3);
    getContentPane().add(textField4);
    getContentPane().add(textField5);
    getContentPane().add(textField6);
    getContentPane().add(textField7);
    getContentPane().add(textField8);
    getContentPane().add(textField9);
    getContentPane().add(textField10);
    getContentPane().add(textField11);
    getContentPane().add(textField12);
    getContentPane().add(textField13);
    getContentPane().add(textField14);

    setSize(300,190);
    setVisible(true);
  }
	
  public static void main(String argv[]) {
    new JTextFieldTest2();
  }
}

