/** 
 *  Copyright 1999-2002 Matthew Robinson and Pavel Vorobiev. 
 *  All Rights Reserved. 
 * 
 *  =================================================== 
 *  This program contains code from the book "Swing" 
 *  2nd Edition by Matthew Robinson and Pavel Vorobiev 
 *  http://www.spindoczine.com/sbe 
 *  =================================================== 
 * 
 *  The above paragraph must be included in full, unmodified 
 *  and completely intact in the beginning of any source code 
 *  file that references, copies or uses (in any way, shape 
 *  or form) code contained in this file. 
 */ 

import javax.swing.*;
import java.awt.*;

public class JTextFieldTest extends JFrame 
{
  public JTextFieldTest() {
    super("JTextField Test");

    getContentPane().setLayout(new FlowLayout());

    JTextField textField1 = new JTextField("m",1);
    JTextField textField2 = new JTextField("mm",2);
    JTextField textField3 = new JTextField("mmm",3);
    JTextField textField4 = new JTextField("mmmm",4);
    JTextField textField5 = new JTextField("mmmmm",5);
    JTextField textField6 = new JTextField("mmmmmm",6);
    JTextField textField7 = new JTextField("mmmmmmm",7);
    JTextField textField8 = new JTextField("mmmmmmmm",8);
    JTextField textField9 = new JTextField("mmmmmmmmm",9);
    JTextField textField10 = new JTextField("mmmmmmmmmm",10);
    JTextField textField11 = new JTextField("mmmmmmmmmmm",11);
    JTextField textField12 = new JTextField("mmmmmmmmmmmm",12);
    JTextField textField13 = new JTextField("mmmmmmmmmmmmm",13);
    JTextField textField14 = new JTextField("mmmmmmmmmmmmmm",14);

    getContentPane().add(textField1);
    getContentPane().add(textField2);
    getContentPane().add(textField3);
    getContentPane().add(textField4);
    getContentPane().add(textField5);
    getContentPane().add(textField6);
    getContentPane().add(textField7);
    getContentPane().add(textField8);
    getContentPane().add(textField9);
    getContentPane().add(textField10);
    getContentPane().add(textField11);
    getContentPane().add(textField12);
    getContentPane().add(textField13);
    getContentPane().add(textField14);

    setSize(300,170);
    setVisible(true);
  }
	
  public static void main(String argv[]) {
    new JTextFieldTest();
  }
}

