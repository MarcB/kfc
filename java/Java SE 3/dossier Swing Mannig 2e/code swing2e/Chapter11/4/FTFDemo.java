/** 
 *  Copyright 1999-2002 Matthew Robinson and Pavel Vorobiev. 
 *  All Rights Reserved. 
 * 
 *  =================================================== 
 *  This program contains code from the book "Swing" 
 *  2nd Edition by Matthew Robinson and Pavel Vorobiev 
 *  http://www.spindoczine.com/sbe 
 *  =================================================== 
 * 
 *  The above paragraph must be included in full, unmodified 
 *  and completely intact in the beginning of any source code 
 *  file that references, copies or uses (in any way, shape 
 *  or form) code contained in this file. 
 */ 

import java.awt.*;
import java.awt.event.*;
import java.text.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;

import dl.*;

class FTFDemo extends JFrame {

	public FTFDemo() {
		super("Formatted Text Field");

		JPanel p = new JPanel(new DialogLayout2());
		p.setBorder(new EmptyBorder(10, 10, 10, 10));

		p.add(new JLabel("Dollar amount:"));
		NumberFormat formatMoney =
			NumberFormat.getCurrencyInstance(Locale.US);	// New for 1.4
		JFormattedTextField ftfMoney = new JFormattedTextField(formatMoney);
		ftfMoney.setColumns(10);
		ftfMoney.setValue(new Double(100));
 		p.add(ftfMoney);

		p.add(new JLabel("Transaction date:"));
		DateFormat formatDate = new SimpleDateFormat("MM/dd/yyyy");
		JFormattedTextField ftfDate = new JFormattedTextField(formatDate);
		ftfDate.setColumns(10);
		ftfDate.setValue(new Date());
 		p.add(ftfDate);

		JButton btn = new JButton("OK");
		p.add(btn);

		getContentPane().add(p, BorderLayout.CENTER);
		pack();
	}

	public static void main( String args[] ) {
		FTFDemo mainFrame = new FTFDemo();
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setVisible(true);
	}
}
