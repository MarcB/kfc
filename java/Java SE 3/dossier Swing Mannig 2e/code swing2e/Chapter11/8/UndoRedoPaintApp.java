/** 
 *  Copyright 1999-2002 Matthew Robinson and Pavel Vorobiev. 
 *  All Rights Reserved. 
 * 
 *  =================================================== 
 *  This program contains code from the book "Swing" 
 *  2nd Edition by Matthew Robinson and Pavel Vorobiev 
 *  http://www.spindoczine.com/sbe 
 *  =================================================== 
 * 
 *  The above paragraph must be included in full, unmodified 
 *  and completely intact in the beginning of any source code 
 *  file that references, copies or uses (in any way, shape 
 *  or form) code contained in this file. 
 */ 

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.undo.*;
import javax.swing.event.*;

public class UndoRedoPaintApp extends JFrame {

  protected Vector m_points = new Vector();
  protected PaintCanvas m_canvas;
  protected JButton m_undoButton;
  protected JButton m_redoButton;
  protected UndoManager m_undoManager = new UndoManager();

  public UndoRedoPaintApp() {
    super("Custom Undo/Redo");
    setSize(400,300);

	m_undoButton = new JButton("Undo");
    m_undoButton.setEnabled(false);
    m_redoButton = new JButton("Redo");
    m_redoButton.setEnabled(false);

    JPanel buttonPanel = new JPanel(new GridLayout());
    buttonPanel.add(m_undoButton);
    buttonPanel.add(m_redoButton);
    getContentPane().add(buttonPanel, BorderLayout.NORTH);
    
    m_canvas = new PaintCanvas(m_points);
    getContentPane().add(m_canvas, BorderLayout.CENTER);

    m_canvas.addMouseListener(new MouseAdapter() {
      public void mousePressed(MouseEvent e) {
        Point point = new Point(e.getX(), e.getY());
        m_points.addElement(point);

        m_undoManager.undoableEditHappened(new UndoableEditEvent(m_canvas,
          new UndoablePaintSquare(point, m_points)));

        m_undoButton.setText(m_undoManager.getUndoPresentationName());
        m_redoButton.setText(m_undoManager.getRedoPresentationName());
        m_undoButton.setEnabled(m_undoManager.canUndo());
        m_redoButton.setEnabled(m_undoManager.canRedo());
        m_canvas.repaint();
      }
    });

    m_undoButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try { m_undoManager.undo(); }
        catch (CannotRedoException cre) { cre.printStackTrace(); }
        m_canvas.repaint();
        m_undoButton.setEnabled(m_undoManager.canUndo());
        m_redoButton.setEnabled(m_undoManager.canRedo());
      }
    });

    m_redoButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try { m_undoManager.redo(); }
        catch (CannotRedoException cre) { cre.printStackTrace(); }
        m_canvas.repaint();
        m_undoButton.setEnabled(m_undoManager.canUndo());
        m_redoButton.setEnabled(m_undoManager.canRedo());
      }
    });
  }

  public static void main(String argv[]) { 
	UndoRedoPaintApp frame = new UndoRedoPaintApp();
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	frame.setVisible(true);
  }
}

class PaintCanvas extends JPanel
{
  Vector m_points;
  protected int width = 50;
  protected int height = 50;

  public PaintCanvas(Vector vect) {
    super();
    m_points = vect;
    setOpaque(true);
    setBackground(Color.white);
  }

  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    g.setColor(Color.black);
    Enumeration enum = m_points.elements();
    while(enum.hasMoreElements()) {
      Point point = (Point) enum.nextElement();
      g.drawRect(point.x, point.y, width, height);
    }
  }
}

class UndoablePaintSquare extends AbstractUndoableEdit 
{
  protected Vector m_points;
  protected Point m_point;

  public UndoablePaintSquare(Point point, Vector vect) {
    m_points = vect;
    m_point = point;
  }   
  
  public String getPresentationName() {
    return "Square Addition";
  }

  public void undo() {
    super.undo();
    m_points.remove(m_point);
  }

  public void redo() {
    super.redo();
    m_points.add(m_point);
  }
}


