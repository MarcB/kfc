/** 
 *  Copyright 1999-2002 Matthew Robinson and Pavel Vorobiev. 
 *  All Rights Reserved. 
 * 
 *  =================================================== 
 *  This program contains code from the book "Swing" 
 *  2nd Edition by Matthew Robinson and Pavel Vorobiev 
 *  http://www.spindoczine.com/sbe 
 *  =================================================== 
 * 
 *  The above paragraph must be included in full, unmodified 
 *  and completely intact in the beginning of any source code 
 *  file that references, copies or uses (in any way, shape 
 *  or form) code contained in this file. 
 */ 

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.undo.*;
import javax.swing.event.*;

public class UndoRedoTextApp extends JFrame {

  protected JTextArea m_editor;
  protected JButton m_undoButton;
  protected JButton m_redoButton;
  protected UndoManager m_undoManager = new UndoManager();

  public UndoRedoTextApp() {
    super("Text Undo/Redo");
    setSize(400,300);

	m_undoButton = new JButton("Undo");
    m_undoButton.setEnabled(false);
    m_redoButton = new JButton("Redo");
    m_redoButton.setEnabled(false);

    JPanel buttonPanel = new JPanel(new GridLayout());
    buttonPanel.add(m_undoButton);
    buttonPanel.add(m_redoButton);
    getContentPane().add(buttonPanel, BorderLayout.NORTH);

    m_editor = new JTextArea();
    JScrollPane scroller = new JScrollPane(m_editor);
    getContentPane().add(scroller, BorderLayout.CENTER);

    m_editor.getDocument().addUndoableEditListener(new UndoableEditListener() {
      public void undoableEditHappened(UndoableEditEvent e) {
        m_undoManager.addEdit(e.getEdit());
        updateButtons();
      }
    });

    m_undoButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try { m_undoManager.undo(); }
        catch (CannotRedoException cre) { cre.printStackTrace(); }
        updateButtons();
      }
    });

    m_redoButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try { m_undoManager.redo(); }
        catch (CannotRedoException cre) { cre.printStackTrace(); }
        updateButtons();
      }
    });
  }

  public void updateButtons() {
    m_undoButton.setText(m_undoManager.getUndoPresentationName());
    m_redoButton.setText(m_undoManager.getRedoPresentationName());
    m_undoButton.setEnabled(m_undoManager.canUndo());
    m_redoButton.setEnabled(m_undoManager.canRedo());
  }

  public static void main(String argv[]) { 
	UndoRedoTextApp frame = new UndoRedoTextApp();
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	frame.setVisible(true);
  }
}


