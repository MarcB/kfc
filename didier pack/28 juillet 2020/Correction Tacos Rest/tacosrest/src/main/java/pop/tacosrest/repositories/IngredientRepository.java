package pop.tacosrest.repositories;

import org.springframework.data.repository.CrudRepository;
import pop.tacosrest.entities.Ingredient;

public interface IngredientRepository
        extends CrudRepository<Ingredient, String> {

    Iterable<Ingredient> findByType(Ingredient.Type type);
}
