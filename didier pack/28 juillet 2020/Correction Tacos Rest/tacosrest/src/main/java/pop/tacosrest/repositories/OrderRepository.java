package pop.tacosrest.repositories;

import org.springframework.data.repository.CrudRepository;
import pop.tacosrest.entities.Order;


public interface OrderRepository
        extends CrudRepository<Order, Long> {

}
