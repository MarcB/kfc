package pop.tacosweb.controllers;

import javax.validation.Valid;

import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.client.RestTemplate;
import pop.tacosweb.model.Order;


@Controller
@RequestMapping("/orders")
@SessionAttributes("order")
public class OrderController {

    public OrderController() {
    }

    @GetMapping("/current")
    public String orderForm() {
        return "orderForm";
    }

    @PostMapping
    public String processOrder(@Valid Order order, Errors errors, SessionStatus sessionStatus) {
        if (errors.hasErrors()) {
            return "orderForm";
        }

        String URL_CREATE_ORDER = "http://localhost:8080/orders";
        RestTemplate restTemplate = new RestTemplate();

        HttpEntity<Order> requestBody = new HttpEntity<>(order);

        restTemplate.postForEntity(URL_CREATE_ORDER,requestBody,Order.class);
        sessionStatus.setComplete();  // invalisation de la session (suppression du panier)

        return "redirect:/";  // retour sur la premiere VUE (comment ???)
    }

}
