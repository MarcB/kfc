package pop.tacosweb.controllers;

import java.util.Optional;


import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import org.springframework.web.client.RestTemplate;
import pop.tacosweb.model.Ingredient;


@Component
public class IngredientByIdConverter implements Converter<String, Ingredient> {


  @Override
  public Ingredient convert(String id) {
    String URL_INGREDIENTS_BY_ID = "http://localhost:8080/ingredients/"+id;

      RestTemplate restTemplate = new RestTemplate();
      Ingredient ingredient = restTemplate
              .getForObject(URL_INGREDIENTS_BY_ID,Ingredient.class);

    return ingredient;
  }

}
