create table if not exists employee (
                                          empno varchar(4) not null,
                                          empname varchar(25) not null,
                                          position varchar(15) not null,
                                          constraint pk_employee primary key (empno)
);
