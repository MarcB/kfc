

DROP TABLE IF EXISTS DEGREE CASCADE ;
DROP TABLE IF EXISTS COLLEGE CASCADE ;
DROP TABLE IF EXISTS SALARY_HISTORY CASCADE ;
DROP TABLE IF EXISTS JOB_HISTORY CASCADE ;
DROP TABLE IF EXISTS EMPLOYEE CASCADE ;
DROP TABLE IF EXISTS JOB CASCADE ;
DROP TABLE IF EXISTS DEPT CASCADE ;

/*
REM CREATION DES TABLES
*/

CREATE TABLE DEPT ( 
  DEPTNO  NUMERIC(2)    NOT NULL, 
  DNAME   VARCHAR (14), 
  LOC     VARCHAR (13), 
  CONSTRAINT DEPT_PK
  PRIMARY KEY ( DEPTNO ) 
) ; 

CREATE TABLE JOB (
   JOB_CODE CHAR(3) 
      constraint JOB_pk primary key,
   JOB_TITLE VARCHAR(20),
   MINIMUM_SALARY NUMERIC(7,2) NOT NULL,
   MAXIMUM_SALARY NUMERIC(7,2)
);



CREATE TABLE EMPLOYEE ( 
  EMPNO     NUMERIC(4)    NOT NULL, 
  ENAME     VARCHAR (20), 
  JOB       CHAR (3) constraint fk_emp_job references JOB(job_code), 
  MGR       NUMERIC(4) constraint fk_emp_mgr references EMPLOYEE(empno), 
  HIREDATE  DATE, 
  SAL       NUMERIC(7,2), 
  COMM      NUMERIC (7,2), 
  DEPTNO    NUMERIC (2) constraint fk_emp_deptno references DEPT(deptno), 
  CONSTRAINT EMP_PK
  PRIMARY KEY ( EMPNO ) 
) ; 

INSERT INTO DEPT ( DEPTNO, DNAME, LOC ) VALUES ( 
10, 'ACCOUNTING', 'NEW YORK'); 
INSERT INTO DEPT ( DEPTNO, DNAME, LOC ) VALUES ( 
20, 'RESEARCH', 'DALLAS'); 
INSERT INTO DEPT ( DEPTNO, DNAME, LOC ) VALUES ( 
30, 'SALES', 'CHICAGO'); 
INSERT INTO DEPT ( DEPTNO, DNAME, LOC ) VALUES ( 
40, 'OPERATIONS', 'BOSTON'); 

INSERT INTO JOB VALUES
('SAL','SALESMAN',1000,10000);
INSERT INTO JOB VALUES
('CLK','CLERK',200,1500);
INSERT INTO JOB VALUES
('MGR','MANAGER',1500,15000);
INSERT INTO JOB VALUES
('ANA','ANALYST',500,7500);
INSERT INTO JOB VALUES
('PRE','PRESIDENT',4000,90000);

INSERT INTO EMPLOYEE ( EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM,
DEPTNO ) VALUES ( 
7839, 'KING', 'PRE', NULL,  TO_Date( '11/17/1981 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
, 5000, NULL, 10);
INSERT INTO EMPLOYEE ( EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM,
DEPTNO ) VALUES ( 
7698, 'BLAKE', 'MGR', 7839,  TO_Date( '05/01/1981 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
, 2850, NULL, 30); 
INSERT INTO EMPLOYEE ( EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM,
DEPTNO ) VALUES ( 
7782, 'CLARK', 'MGR', 7839,  TO_Date( '06/09/1981 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
, 2450, NULL, 10);  
INSERT INTO EMPLOYEE ( EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM,
DEPTNO ) VALUES ( 
7499, 'ALLEN', 'SAL', 7698,  TO_Date( '02/20/1981 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
, 1600, 300, 30); 
INSERT INTO EMPLOYEE ( EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM,
DEPTNO ) VALUES ( 
7521, 'WARD', 'SAL', 7698,  TO_Date( '02/22/1981 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
, 1250, 500, 30); 
INSERT INTO EMPLOYEE ( EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM,
DEPTNO ) VALUES ( 
7566, 'JONES', 'MGR', 7839,  TO_Date( '04/02/1981 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
, 2975, NULL, 20); 
INSERT INTO EMPLOYEE ( EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM,
DEPTNO ) VALUES ( 
7654, 'MARTIN', 'SAL', 7698,  TO_Date( '09/28/1981 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
, 1250, 1400, 30); 
INSERT INTO EMPLOYEE ( EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM,
DEPTNO ) VALUES ( 
7788, 'SCOTT', 'ANA', 7566,  TO_Date( '04/19/1987 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
, 3000, NULL, 20); 
INSERT INTO EMPLOYEE ( EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM,
DEPTNO ) VALUES ( 
7844, 'TURNER', 'SAL', 7698,  TO_Date( '09/08/1981 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
, 1500, 0, 30); 
INSERT INTO EMPLOYEE ( EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM,
DEPTNO ) VALUES ( 
7876, 'ADAMS', 'CLK', 7788,  TO_Date( '05/23/1987 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
, 1100, NULL, 20); 
INSERT INTO EMPLOYEE ( EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM,
DEPTNO ) VALUES ( 
7900, 'JAMES', 'CLK', 7698,  TO_Date( '12/03/1981 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
, 950, NULL, 30); 
INSERT INTO EMPLOYEE ( EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM,
DEPTNO ) VALUES ( 
7902, 'FORD', 'ANA', 7566,  TO_Date( '12/03/1981 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
, 3000, NULL, 20); 
INSERT INTO EMPLOYEE ( EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM,
DEPTNO ) VALUES ( 
7934, 'MILLER', 'CLK', 7782,  TO_Date( '01/23/1982 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
, 1300, NULL, 10); 
INSERT INTO EMPLOYEE ( EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM,
DEPTNO ) VALUES ( 
7369, 'SMITH', 'CLK', 7902,  TO_Date( '12/17/1980 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
, 800, NULL, 20); 

CREATE TABLE JOB_HISTORY (
  id serial  constraint job_history_pk primary key, 
  EMPNO NUMERIC(4) NOT NULL 
       constraint jh_empno_fk references EMPLOYEE(empno),
  JOB_CODE CHAR(4) not null 
       constraint jh_JOB_fk references JOB(job_code),
  JOB_START DATE NOT NULL,
  JOB_END DATE,
  DEPTNO NUMERIC(2) NOT NULL 
       constraint jh_dept_fk references DEPT(deptno)
);

INSERT INTO JOB_HISTORY(empno, job_code, job_start, job_end, deptno) VALUES
(7369,'CLK',To_date('17/12/1980','DD/MM/YYYY'),NULL,20);
INSERT INTO JOB_HISTORY(empno, job_code, job_start, job_end, deptno) VALUES
(7499,'CLK',To_date('20/02/1981','DD/MM/YYYY'),To_date('17/11/1981','DD/MM/YYYY'),30);
INSERT INTO JOB_HISTORY(empno, job_code, job_start, job_end, deptno) VALUES
(7499,'SAL',To_date('17/11/1981','DD/MM/YYYY'),NULL,30);
INSERT INTO JOB_HISTORY(empno, job_code, job_start, job_end, deptno) VALUES
(7521,'SAL',To_date('22/02/1981','DD/MM/YYYY'),NULL,30);
INSERT INTO JOB_HISTORY(empno, job_code, job_start, job_end, deptno) VALUES
(7566,'SAL',To_date('02/04/1981','DD/MM/YYYY'),To_date('02/04/1986','DD/MM/YYYY'),20);
INSERT INTO JOB_HISTORY(empno, job_code, job_start, job_end, deptno) VALUES
(7566,'MGR',To_date('02/04/1986','DD/MM/YYYY'),NULL,30);
INSERT INTO JOB_HISTORY(empno, job_code, job_start, job_end, deptno) VALUES
(7654,'CLK',To_date('28/09/1981','DD/MM/YYYY'),To_date('15/12/1985','DD/MM/YYYY'),10);
INSERT INTO JOB_HISTORY(empno, job_code, job_start, job_end, deptno) VALUES
(7654,'SAL',To_date('15/12/1985','DD/MM/YYYY'),NULL,30);
INSERT INTO JOB_HISTORY(empno, job_code, job_start, job_end, deptno) VALUES
(7698,'MGR',To_date('01/05/1981','DD/MM/YYYY'),NULL,30);
INSERT INTO JOB_HISTORY(empno, job_code, job_start, job_end, deptno) VALUES
(7782,'MGR',To_date('09/06/1981','DD/MM/YYYY'),NULL,10);
INSERT INTO JOB_HISTORY(empno, job_code, job_start, job_end, deptno) VALUES
(7788,'ANA',To_date('09/12/1982','DD/MM/YYYY'),NULL,20);
INSERT INTO JOB_HISTORY(empno, job_code, job_start, job_end, deptno) VALUES
(7839,'CLK',To_date('17/11/1981','DD/MM/YYYY'),To_date('17/11/1982','DD/MM/YYYY'),10);
INSERT INTO JOB_HISTORY(empno, job_code, job_start, job_end, deptno) VALUES
(7839,'ANA',To_date('17/11/1982','DD/MM/YYYY'),To_date('17/11/1983','DD/MM/YYYY'),10);
INSERT INTO JOB_HISTORY(empno, job_code, job_start, job_end, deptno) VALUES
(7839,'SAL',To_date('17/11/1983','DD/MM/YYYY'),To_date('17/11/1984','DD/MM/YYYY'),10);
INSERT INTO JOB_HISTORY(empno, job_code, job_start, job_end, deptno) VALUES
(7839,'PRE',To_date('17/11/1984','DD/MM/YYYY'),NULL,10);
INSERT INTO JOB_HISTORY(empno, job_code, job_start, job_end, deptno) VALUES
(7844,'SAL',To_date('8/09/1981','DD/MM/YYYY'),NULL,30);
INSERT INTO JOB_HISTORY(empno, job_code, job_start, job_end, deptno) VALUES
(7876,'CLK',To_date('12/01/1983','DD/MM/YYYY'),NULL,20);
INSERT INTO JOB_HISTORY(empno, job_code, job_start, job_end, deptno) VALUES
(7900,'CLK',To_date('03/12/1981','DD/MM/YYYY'),NULL,30);
INSERT INTO JOB_HISTORY(empno, job_code, job_start, job_end, deptno) VALUES
(7902,'ANA',To_date('03/12/1981','DD/MM/YYYY'),NULL,20);
INSERT INTO JOB_HISTORY(empno, job_code, job_start, job_end, deptno) VALUES
(7934,'CLK',To_date('23/01/1982','DD/MM/YYYY'),NULL,10);



CREATE TABLE SALARY_HISTORY (
   id serial 
      constraint salary_history_pk primary key,
   EMPNO NUMERIC(4) NOT NULL 
      constraint sh_empno_fk references EMPLOYEE(empno) ,
   SALARY_AMOUNT NUMERIC(7,2) NOT NULL,
   SALARY_START DATE NOT NULL,
   SALARY_END DATE
);

INSERT INTO SALARY_HISTORY(empno, salary_amount, salary_start, salary_end) VALUES
(7369,200,To_date('17/12/1980','DD/MM/YYYY'),To_date('17/12/1985','DD/MM/YYYY'));
INSERT INTO SALARY_HISTORY(empno, salary_amount, salary_start, salary_end) VALUES
(7369,800,To_date('17/12/1985','DD/MM/YYYY'),NULL);
INSERT INTO SALARY_HISTORY(empno, salary_amount, salary_start, salary_end) VALUES
(7499,500,To_date('20/02/1981','DD/MM/YYYY'),To_date('17/11/1981','DD/MM/YYYY'));
INSERT INTO SALARY_HISTORY(empno, salary_amount, salary_start, salary_end) VALUES
(7499,1000,To_date('17/11/1981','DD/MM/YYYY'),To_date('17/11/1982','DD/MM/YYYY'));
INSERT INTO SALARY_HISTORY(empno, salary_amount, salary_start, salary_end) VALUES
(7499,1300,To_date('17/11/1982','DD/MM/YYYY'),To_date('20/02/1987','DD/MM/YYYY'));
INSERT INTO SALARY_HISTORY(empno, salary_amount, salary_start, salary_end) VALUES
(7499,1600,To_date('20/02/1987','DD/MM/YYYY'),NULL);
INSERT INTO SALARY_HISTORY(empno, salary_amount, salary_start, salary_end) VALUES
(7521,1000,To_date('22/02/1981','DD/MM/YYYY'),To_date('22/02/1985','DD/MM/YYYY'));
INSERT INTO SALARY_HISTORY(empno, salary_amount, salary_start, salary_end) VALUES
(7521,1100,To_date('22/02/1985','DD/MM/YYYY'),To_date('22/02/1987','DD/MM/YYYY'));
INSERT INTO SALARY_HISTORY(empno, salary_amount, salary_start, salary_end) VALUES
(7521,1250,To_date('22/02/1987','DD/MM/YYYY'),NULL);
INSERT INTO SALARY_HISTORY(empno, salary_amount, salary_start, salary_end) VALUES
(7566,1975,To_date('02/04/1981','DD/MM/YYYY'),To_date('02/04/1986','DD/MM/YYYY'));
INSERT INTO SALARY_HISTORY(empno, salary_amount, salary_start, salary_end) VALUES
(7566,2975,To_date('02/04/1981','DD/MM/YYYY'),NULL);
INSERT INTO SALARY_HISTORY(empno, salary_amount, salary_start, salary_end) VALUES
(7654,250,To_date('28/09/1981','DD/MM/YYYY'),To_date('02/04/1982','DD/MM/YYYY'));
INSERT INTO SALARY_HISTORY(empno, salary_amount, salary_start, salary_end) VALUES
(7654,350,To_date('02/04/1982','DD/MM/YYYY'),To_date('15/12/1985','DD/MM/YYYY'));
INSERT INTO SALARY_HISTORY(empno, salary_amount, salary_start, salary_end) VALUES
(7654,1000,To_date('15/12/1985','DD/MM/YYYY'),To_date('15/12/1986','DD/MM/YYYY'));
INSERT INTO SALARY_HISTORY(empno, salary_amount, salary_start, salary_end) VALUES
(7654,1250,To_date('15/12/1986','DD/MM/YYYY'),NULL);
INSERT INTO SALARY_HISTORY(empno, salary_amount, salary_start, salary_end) VALUES
(7698,2850,To_date('01/05/1981','DD/MM/YYYY'),NULL);
INSERT INTO SALARY_HISTORY(empno, salary_amount, salary_start, salary_end) VALUES
(7782,1500,To_date('09/06/1981','DD/MM/YYYY'),To_date('09/06/1982','DD/MM/YYYY'));
INSERT INTO SALARY_HISTORY(empno, salary_amount, salary_start, salary_end) VALUES
(7782,1600,To_date('09/06/1982','DD/MM/YYYY'),To_date('09/06/1983','DD/MM/YYYY'));
INSERT INTO SALARY_HISTORY(empno, salary_amount, salary_start, salary_end) VALUES
(7782,2000,To_date('09/06/1983','DD/MM/YYYY'),To_date('09/06/1987','DD/MM/YYYY'));
INSERT INTO SALARY_HISTORY(empno, salary_amount, salary_start, salary_end) VALUES
(7782,2450,To_date('09/06/1987','DD/MM/YYYY'),NULL);
INSERT INTO SALARY_HISTORY(empno, salary_amount, salary_start, salary_end) VALUES
(7839,800,To_date('17/11/1981','DD/MM/YYYY'),To_date('17/12/1981','DD/MM/YYYY'));
INSERT INTO SALARY_HISTORY(empno, salary_amount, salary_start, salary_end) VALUES
(7839,900,To_date('17/12/1981','DD/MM/YYYY'),To_date('17/11/1982','DD/MM/YYYY'));
INSERT INTO SALARY_HISTORY(empno, salary_amount, salary_start, salary_end) VALUES
(7839,1900,To_date('17/11/1982','DD/MM/YYYY'),To_date('17/11/1983','DD/MM/YYYY'));
INSERT INTO SALARY_HISTORY(empno, salary_amount, salary_start, salary_end) VALUES
(7839,2900,To_date('17/11/1983','DD/MM/YYYY'),To_date('17/11/1984','DD/MM/YYYY'));
INSERT INTO SALARY_HISTORY(empno, salary_amount, salary_start, salary_end) VALUES
(7839,3900,To_date('17/11/1984','DD/MM/YYYY'),To_date('17/11/1985','DD/MM/YYYY'));
INSERT INTO SALARY_HISTORY(empno, salary_amount, salary_start, salary_end) VALUES
(7839,4900,To_date('17/11/1985','DD/MM/YYYY'),To_date('17/11/1986','DD/MM/YYYY'));
INSERT INTO SALARY_HISTORY(empno, salary_amount, salary_start, salary_end) VALUES
(7839,5000,To_date('17/11/1986','DD/MM/YYYY'),NULL);
INSERT INTO SALARY_HISTORY(empno, salary_amount, salary_start, salary_end) VALUES
(7844,1000,To_date('08/09/1981','DD/MM/YYYY'),To_date('08/09/1982','DD/MM/YYYY'));
INSERT INTO SALARY_HISTORY(empno, salary_amount, salary_start, salary_end) VALUES
(7844,1300,To_date('08/09/1982','DD/MM/YYYY'),To_date('08/09/1987','DD/MM/YYYY'));
INSERT INTO SALARY_HISTORY(empno, salary_amount, salary_start, salary_end) VALUES
(7844,1500,To_date('08/09/1987','DD/MM/YYYY'),NULL);
INSERT INTO SALARY_HISTORY(empno, salary_amount, salary_start, salary_end) VALUES
(7876,500,To_date('12/01/1983','DD/MM/YYYY'),To_date('12/01/1985','DD/MM/YYYY'));
INSERT INTO SALARY_HISTORY(empno, salary_amount, salary_start, salary_end) VALUES
(7876,1100,To_date('12/01/1985','DD/MM/YYYY'),NULL);
INSERT INTO SALARY_HISTORY(empno, salary_amount, salary_start, salary_end) VALUES
(7900,200,To_date('03/12/1981','DD/MM/YYYY'),To_date('03/12/1984','DD/MM/YYYY'));
INSERT INTO SALARY_HISTORY(empno, salary_amount, salary_start, salary_end) VALUES
(7900,850,To_date('03/12/1984','DD/MM/YYYY'),To_date('03/12/1986','DD/MM/YYYY'));
INSERT INTO SALARY_HISTORY(empno, salary_amount, salary_start, salary_end) VALUES
(7900,950,To_date('03/12/1986','DD/MM/YYYY'),NULL);
INSERT INTO SALARY_HISTORY(empno, salary_amount, salary_start, salary_end) VALUES
(7902,700,To_date('03/12/1981','DD/MM/YYYY'),To_date('03/12/1983','DD/MM/YYYY'));
INSERT INTO SALARY_HISTORY(empno, salary_amount, salary_start, salary_end) VALUES
(7902,1000,To_date('03/12/1983','DD/MM/YYYY'),To_date('03/12/1984','DD/MM/YYYY'));
INSERT INTO SALARY_HISTORY(empno, salary_amount, salary_start, salary_end) VALUES
(7902,2000,To_date('03/12/1984','DD/MM/YYYY'),To_date('03/12/1986','DD/MM/YYYY'));
INSERT INTO SALARY_HISTORY(empno, salary_amount, salary_start, salary_end) VALUES
(7902,3000,To_date('03/12/1986','DD/MM/YYYY'),NULL);
INSERT INTO SALARY_HISTORY(empno, salary_amount, salary_start, salary_end) VALUES
(7934,1300,To_date('23/01/1982','DD/MM/YYYY'),NULL);




CREATE TABLE COLLEGE (
  COLLEGE_CODE CHAR(4) 
     constraint COLLEGE_pk primary key,
  COLLEGE_NAME VARCHAR(25),
  CITY VARCHAR(20),
  STATE CHAR(2),
  POSTAL_CODE CHAR(5)
);

INSERT INTO COLLEGE VALUES
('PRDU','PURDUE UNIVERSITY','WEST LAFAYETTE','CT','47907');
INSERT INTO COLLEGE VALUES
('IDN','INST. IND. DU NORD','V. D''ASCQ','FR','59651');
INSERT INTO COLLEGE VALUES
('STAN','STANDFORD UNIVERSITY','BERKLEY','CA','94305');
INSERT INTO COLLEGE VALUES
('HVD','HARVARD UNIVERSITY','CAMBRIDGE','MA','02138');


CREATE TABLE DEGREE (
  id serial 
     constraint DEGREE_pk primary key,
  EMPNO NUMERIC(4) NOT NULL 
     constraint deg_empno_fk references EMPLOYEE(empno),
  COLLEGE_CODE CHAR(4) 
     constraint deg_COLLEGE_fk references COLLEGE(college_code),
  YEAR_GIVEN NUMERIC(4),
  DEGREE CHAR(3),
  DEGREE_FIELD CHAR(15)
);

INSERT INTO DEGREE(empno,college_code,year_given,degree, degree_field) VALUES
(7369,'PRDU',1979,'PHD','STATISTIC');
INSERT INTO DEGREE(empno, college_code, year_given, degree, degree_field) values
(7369,'PRDU',1973,'MA','APPLIED MATH');
INSERT INTO DEGREE(empno, college_code, year_given, degree, degree_field) values
(7499,NULL,1972,'BA','ARTS');
INSERT INTO DEGREE(empno, college_code, year_given, degree, degree_field) values
(7521,'IDN',1972,'MA','GESTION');
INSERT INTO DEGREE(empno, college_code, year_given, degree, degree_field) values
(7566,'IDN',1972,'MA','GESTION');
INSERT INTO DEGREE(empno, college_code, year_given, degree, degree_field) values
(7566,'IDN',1975,'DEA','PIPO');
INSERT INTO DEGREE(empno, college_code, year_given, degree, degree_field) values
(7566,'IDN',1978,'PHD','VENT');
INSERT INTO DEGREE(empno, college_code, year_given, degree, degree_field) values
(7654,'STAN',1976,'MA','IA');
INSERT INTO DEGREE(empno, college_code, year_given, degree, degree_field) values
(7654,'STAN',1979,'PHD','GESTION');
INSERT INTO DEGREE(empno, college_code, year_given, degree, degree_field) values
(7698,'HVD',1977,'MA','IA');
INSERT INTO DEGREE(empno, college_code, year_given, degree, degree_field) values
(7698,'HVD',1979,'PHD','GL');
INSERT INTO DEGREE(empno, college_code, year_given, degree, degree_field) values
(7782,'HVD',1979,'PHD','GL');
INSERT INTO DEGREE(empno, college_code, year_given, degree, degree_field) values
(7839,'IDN',1979,'PHD','ADA');
