



-- creation des tables
drop table if exists EMPRUNT;
drop table if exists CLIENT;
drop table if exists FILM;
drop table if exists ACTEUR;
drop table if exists GENRE;
drop table if exists PAYS ;


create table PAYS(
	NPAYS	numeric(3,0)
			constraint PK_PAYS primary key,
	NOM		varchar(20)
			constraint NN_PAYS_NOM	not null
);

create table GENRE(
	NGENRE  numeric(3,0)
			constraint PK_GENRE primary key,
	NATURE	varchar(20)
			constraint NN_GENRE_NATURE not null
);

create table ACTEUR(
	NACTEUR	numeric(3,0)
			constraint PK_ACTEUR	primary key,
	NOM		varchar(20)
			constraint NN_ACTEUR_NOM	not null,
	PRENOM	varchar(15)
			constraint NN_ACTEUR_PRENOM	not null,
	NAISSANCE	date,
	NATIONALITE	numeric(3,0)
			constraint FK_ACTEUR_NATIONALITE
			           references PAYS(npays),
	NBRE_FILMS	numeric(3,0)
			constraint CK_ACTEUR_NBRE_FILMS	
			         check( nbre_films >0),
    constraint UK_ACTEUR_NOM_PRENOM unique(NOM,PRENOM)
	);

create table FILM(
	NFILM	numeric(3,0)
		    constraint PK_FILM	primary key,
	TITRE   varchar(40)
		    constraint NN_FILM_TITRE	not null,
	NGENRE  numeric(3,0)
		    constraint FK_FILM_GENRE_NGENRE references GENRE(ngenre),
	SORTIE  date,
	NPAYS	numeric(3,0)
		    constraint FK_FILM_ACTEUR_NPAYS references PAYS(npays),
	REALISATEUR varchar(20)
		    constraint NN_FILM_REALISATEUR not null,
	NACTEUR_PRINCIPAL numeric(3,0)
		    constraint FK_FILM_ACTEUR_NACTEUR references ACTEUR(nacteur),
	ENTREES numeric(6,2)
		    constraint CH_FILM_entrees check (entrees >=0),
	OSCAR char
		  constraint CH_FILM_OSCAR check( oscar in ('O','N') ),
	constraint UK_FILM_TITRE unique( TITRE )
);


--
-- Ajout des genres
--

insert into PAYS values(1,'USA');
insert into PAYS values(2,'France');
insert into PAYS values(3,'Grande Bretagne');
insert into PAYS values(4,'Italie');
insert into PAYS values(5,'Irlande');

--
-- AJout des genres
--


insert into GENRE values (1,'Comedie musicale');
insert into GENRE values (2,'Comedie');
insert into GENRE values (3,'Western');
insert into GENRE values (4,'Fantastique');
insert into GENRE values (5,'Comedie dramatique');
insert into GENRE values (6,'Aventure');
insert into GENRE values (7,'Drame');
insert into GENRE values (8,'Guerre');
insert into GENRE values (9,'Horreur');
insert into GENRE values (10,'Policier');

--
-- Ajout des Acteurs
--


insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,	NATIONALITE,NBRE_FILMS) values
	(1,'Allen',	'Woody',	'01-DEC-1935',	1,	20);
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,	NATIONALITE,NBRE_FILMS) values		
	(2,'Barr'	,'Jean-Marc', NULL,		2, NULL);			
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,	NATIONALITE,NBRE_FILMS) values
	(3,'Beatty',	'Warren',	'30-MAR-1937',	1,	18	);	
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,	NATIONALITE,NBRE_FILMS) values
	(4,'Belmondo',	'Jean-Paul',	'09-APR-1933',	2,	68);		
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,	NATIONALITE,NBRE_FILMS) values
	(5,'Brando',	'Marlon',	'03-APR-1924',	1,	35	);	
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,	NATIONALITE,NBRE_FILMS) values
	(6,'Broderick',	'Matthew',	'21-MAR-1962',	1,	12	);	
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,	NATIONALITE,NBRE_FILMS) values
	(7,'Bronson',	'Charles',	'03-NOV-1922',	1,	43	);	
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,	NATIONALITE,NBRE_FILMS) values
	(8,'Connery',	'Sean',	'25-AUG-1930',	3,	36);		
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,	NATIONALITE,NBRE_FILMS) values
	(9,'De Niro',	'Robert',	'17-AUG-1943',	1,	30);		
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,	NATIONALITE,NBRE_FILMS) values
	(10,'Depardieu',	'Gerard',	'27-DEC-1948',	2	,66);		
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,	NATIONALITE,NBRE_FILMS) values
	(11,'Dreyfuss'	,'Richard',	'29-OCT-1947',	1,	27);		
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,	NATIONALITE,NBRE_FILMS) values
	(12,'Dufilho',	'Jacques',	'19-DEC-1914'	,2,	32);		
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,	NATIONALITE,NBRE_FILMS) values
	(13,'Dullea',	'Keir',	'30-MAY-1939',	1,	15);		
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,	NATIONALITE,NBRE_FILMS) values
	(14,'Dussolier',	'Andre',	'17-FEB-1946',	2,	23);		
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,	NATIONALITE,NBRE_FILMS) values
	(15,'Eastwood',	'Clint',	'31-MAY-1930',	1,	42);		
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,	NATIONALITE,NBRE_FILMS) values
	(16,'Farrow',	'Mia',	'09-FEB-1945',	1,	24);		
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,	NATIONALITE,NBRE_FILMS) values
	(17,'Ford',	'Harrison',	'13-JUL-1942',	1,	24);		
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,	NATIONALITE,NBRE_FILMS) values
	(18,'Gere',	'Richard',	'31-AUG-1949',	1,	17);		
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,	NATIONALITE,NBRE_FILMS) values
	(19,'Hoskins',	'Bob',	'26-OCT-1942',	3,	16);		
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,	NATIONALITE,NBRE_FILMS) values
	(20,'Karyo',	'Tcheky',	'04-OCT-1953',	2,	24);		
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,	NATIONALITE,NBRE_FILMS) values
	(21,'Kingsley',	'Ben',	'10-OCT-1945',	3,	8);		
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,	NATIONALITE,NBRE_FILMS) values
	(22,'Lambert',	'Christophe',	'29-MAR-1957',	2,	15);		
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,	NATIONALITE,NBRE_FILMS) values
	(23,'Lemmon',	'Jack',	'08-FEB-1925',	1,	28);		
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,	NATIONALITE,NBRE_FILMS) values
	(24,'Mac Gill',	'Everett',	'09-APR-1945',	1,	10);		
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,	NATIONALITE,NBRE_FILMS) values
	(25,'Matthau',	'Walter',	'01-OCT-1920',	1,	32);		
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,	NATIONALITE,NBRE_FILMS) values
	(26,'Montand',	'Yves',	'13-OCT-1921',	2,	44);		
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,	NATIONALITE,NBRE_FILMS) values
	(27,'Nicholson',	'Jack',	'22-APR-1937',	1,	31);		
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,	NATIONALITE,NBRE_FILMS) values
	(28,'O Toole',	'Peter',	'02-AUG-1932',	5,	25);		
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,	NATIONALITE,NBRE_FILMS) values
	(29,'Pacino',	'Al',	'25-APR-1940',	1,	16);		
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,	NATIONALITE,NBRE_FILMS) values
	(30,'Redford',	'Robert',	'18-AUG-1937',	1,	25);		
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,	NATIONALITE,NBRE_FILMS) values
	(31,'Rourke',	'Mickey',	'05-MAR-1955',	1,	16);		
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,	NATIONALITE,NBRE_FILMS) values
	(32,'Scheider',	'Roy',	'10-NOV-1935',	1,	25);		
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,	NATIONALITE,NBRE_FILMS) values
	(33,'Streisand',	'Barbra',	'24-APR-1942',	1,	14);		
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,	NATIONALITE,NBRE_FILMS) values
	(34,'Swayze',	'Patrick',	'18-AUG-1952',	1,	11);		
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,	NATIONALITE,NBRE_FILMS) values
	(35,'Thomas',	'Henry',	NULL,	1, NULL);			
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,	NATIONALITE,NBRE_FILMS) values
	(36,'Volonte',	'Gian Maria',	'09-APR-1933',	4,	25);		
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,	NATIONALITE,NBRE_FILMS) values
	(37,'Wood',	'Nathalie',	'20-JUN-1938',	1,	28);
			
--
-- Ajout des films
--


insert into FILM  values
	(2,'West side story',1,'01-JAN-1961',1,'Wise',37,8.60,'N');	
insert into FILM  values
	(3,'La guerre des boutons',2,'01-JAN-1961',2,'Robert',12,9.60,'N');	 
insert into FILM  values
	(4,'Pour une poignee de dollars',3,	'01-JAN-1964', 4, 'Leone', 15, 3.20, 'N');	
insert into FILM  values
(5,'Le bon, la brute et le truand' ,3,'01-JAN-1966',4,'Leone',15,6.30,'N');	 
insert into FILM  values
(6,'2001 : l Odyssee de l espace',4,'01-JAN-1968',3,'Kubrick',13,2.80,	'N'	 );
insert into FILM  values
(7,'Hello Dolly',1,'01-JAN-1969',1,'Kelly',33,1.90,'N');	 
insert into FILM  values
(8,'Il etait une fois dans l Ouest', 3,'01-JAN-1969',4,'Leone',7,14.70,'N');	 
insert into FILM  values
(9,'Le dernier tango a Paris',5,'01-JAN-1972',4,'Bertolucci',5,	3.50,	'N');	 
insert into FILM  values
(10,'Le Parrain',7,'01-JAN-1972',1,'Coppola',5,7.50,'O');
insert into FILM  values
(11,'L affaire Mattei',10,'01-JAN-1972',4,'Rosi',36,5.20,'N');	
insert into FILM  values
(12	,'Le Parrain II',7,'01-JAN-1975',1,'Coppola',29,5.30,'O');	
insert into FILM  values
(13,'Les dents de la mer',6,'01-JAN-1975',1,'Spielberg',32,6.20,	'N');	 
insert into FILM  values
(14,'Le dernier nabab',7,'01-JAN-1976',1,'Kazan',9,3.60,'N');	
insert into FILM  values
(15,'Taxi Driver',7,'01-JAN-1976',1,'Scorcese',9,2.70,'N');	 
insert into FILM  values
(16,'La guerre des Çtoiles',4,'01-JAN-1977',1,'Lucas',17,	5.50,'N');	 
insert into FILM  values
(17,'Rencontres du troisieme type', 4,'01-JAN-1977',1,'Spielberg',11,	4.20,'N');	 
insert into FILM  values
(18,'Annie Hall',2,'01-JAN-1977',1,'Allen',1,2.90,'O');	 
insert into FILM  values
(19,'Voyage au bout de l enfer',7,'01-JAN-1978',1,'Cimino',9,3.90,'O');
insert into FILM  values
(20,'Le ciel peut attendre',2,'01-JAN-1978',1,'Beatty',3,3.70,'N');	 
insert into FILM  values
(21,'Apocalypse now',8,'01-JAN-1979',1,'Coppola',5,4.30,'N');	 
insert into FILM  values
(22,'Shining',9,'01-JAN-1980',3,'Kubrick',27,2.60,'N');	
insert into FILM  values
(23,'Le dernier metro',7,'01-JAN-1980',2,'Truffaut',10,2.80,'N');	 
insert into FILM  values
(24,'Le retour de Martin Guerre',7,'01-JAN-1981',2,'Vigne',10,2.70,'N');	
insert into FILM  values
(25,'La guerre du feu',6,'01-JAN-1981',2,'Annaud',24,3.90,'N');	 
insert into FILM  values
(26,'Les aventuriers de l arche perdue',6,'01-JAN-1981',1,'Spielberg',17	,5.90,'N');	
insert into FILM  values
(27,'Gandhi',7,'01-JAN-1982',3,'Attenbourough',21,4.90,'O');	 
insert into FILM  values
(28,'Missing',7,'01-JAN-1982',1,'Gavras',23,1.90,'N');	 
insert into FILM  values
(29,'E.T.',4,'01-JAN-1982',1,'Spielberg',35,8.30,'N');	 
insert into FILM  values
(30,'Wargames',6,'01-JAN-1983',1,'Badham',6,7.40,'N');	 
insert into FILM  values
(31,'Tendres passions',5,'01-JAN-1983',1,'Brooks',27,4.30,'O');	 
insert into FILM  values
(32,'Cotton club',1,'01-JAN-1984',1,'Coppola',18	,1.50,'N');	
insert into FILM  values
(33,'L annee du dragon',10,'01-JAN-1985',1,'Cimino',31,2.80,'N');	 
insert into FILM  values
(34,'Lady Hawke',4,'01-JAN-1985',1,'Donner',6,2.30,'N');	 
insert into FILM  values
(35,'Highlander',4,'01-JAN-1985',3,'Mulcahy',22,3.50,'N');	 
insert into FILM  values
(36,'Out of Africa',6,'01-JAN-1985',1,'Pollack',30,4.70,'O');	 
insert into FILM  values
(37,'La rose pourpre du Caire',5,'01-JAN-1985',1,'Allen',16	,3.40,'N');	 
insert into FILM  values
(38,'Jean de Florette',7,'01-JAN-1986',2,'Berri',26,6.00,'N');
insert into FILM  values
(39,'Manon des Sources',7,'01-JAN-1986',2,'Berri',26,4.30,'N');	
insert into FILM  values
(40,'Le maitre de guerre',6,'01-JAN-1986',1,'Eastwood',15	,1.90,'N');	 
insert into FILM  values
(41,'Trois hommes et un couffin',2,'01-JAN-1986',2,'Serreau',14	,10.00,'N');	 
insert into FILM  values
(42,'Le nom de la rose',7,'01-JAN-1986',2,'Annaud',8,8.90,'N');	 
insert into FILM  values
(43,'Pirates',6,'01-JAN-1986',1,'Polanski',25,5.80,'N');	
insert into FILM  values
(44,'Le dernier empereur',7,'01-JAN-1987',4,'Bertolucci',28,7.50,'O');	 
insert into FILM  values
(45,'Sous le soleil de Satan',7,'01-JAN-1987',2,'Pialat',10,	2.90,'N');	
insert into FILM  values
(46,'L ours',6,'01-JAN-1988',2,'Annaud',20,7.80,'N');	 
insert into FILM  values
(47,'Le grand bleu',6,'01-JAN-1988',2,'Besson',2,5.90,'N');	 
insert into FILM  values
(48,'Qui veut la peau de Roger Rabbit',2,'01-JAN-1988',1,'Zemeckis',19	,3.60,'N');	
insert into FILM  values
(49,'Crimes et delits',5,'01-JAN-1989',1,'Allen',1	,2.40,'N');	 
insert into FILM  values
(50,'Le Parrain III',7,'01-JAN-1990',1,'Coppola',29,3.80,'N');	
insert into FILM  values
(51,'Ghost',4,'01-JAN-1990',1,'Zucker',34,5.60,'N');	 


