



-- suppression des tables
drop table if exists ANIMAL cascade;
drop table if exists MENU cascade;
drop table if exists FAMILLE cascade;

-- creation des tables
create table FAMILLE(
	race	varchar(15)
			constraint NN_FAMILLE_RACE not null
			constraint CK_FAMILLE_RACE
			  check( race = lower(race) ),
	origine	varchar(15)
			constraint NN_FAMILLE_ORIGINE	not null
			constraint CK_FAMILLE_origine
			  check( origine = initcap(origine) ),
	ami		char(4)
			constraint NN_FAMILLE_AMI	not null
			constraint CK_FAMILLE_AMI 
				check( ami in ('VRAI', 'FAUX') ),
	constraint PK_FAMILLE
		primary key (race, origine)
);


create table MENU(
	N_M		char(3)
			constraint PK_MENU primary key,
	QTE_VIANDE	numeric(4,1)
			constraint NN_MENU_QTE_VIANDE not null
			constraint CK_MENU_QTE_VIANDE
				check( QTE_VIANDE >= 0 ),
	QTE_LEGUMES	numeric(4,1)
			constraint NN_MENU_QTE_LEGUMES not null
			constraint CK_MENU_QTE_LEGUMES
				check( QTE_LEGUMES >= 0 )
);

create table ANIMAL(
	n_a		char(3)
			constraint PK_ANIMAL  primary key,
	nom		varchar(20)
			constraint NN_ANIMAL_NOM  not null
			constraint CK_ANIMAL_NOM
				check( nom = initcap(nom) ),
	age		numeric(3,1)
			constraint NN_ANIMAL_AGE  not null,
			constraint CK_ANIMAL_AGE
				check( age >= 0 ),
	race		varchar(15)
			constraint NN_ANIMAL_RACE not null,
	origine		varchar(15)
			constraint NN_ANIMAL_ORIGINE  not null,
	n_menu		char(3)
			constraint NN_ANIMAL_N_MENU  not null
			constraint FK_ANIMAL_N_MENU references MENU(N_M)
			   deferrable initially immediate ,
	n_asc		char(3)
			constraint FK_ANIMAL_N_ASC  references ANIMAL(N_A)
			  deferrable initially immediate,
	constraint FK_ANIMAL_RACE_ORIGINE
		foreign key (race,origine) references FAMILLE(race,origine) 
		   deferrable initially immediate
);


--
-- ajout des n-uplets dans la table FAMILLE
--
insert into FAMILLE values('lion', 'Afrique','FAUX');
insert into FAMILLE values('panda', 'Chine','FAUX');
insert into FAMILLE values('lama', 'Amerique','VRAI');
insert into FAMILLE values('singe', 'Europe','VRAI');
insert into FAMILLE values('elephant', 'Afrique','VRAI');
insert into FAMILLE values('kangourou', 'Australie','VRAI');
insert into FAMILLE values('tigre', 'Asie','FAUX');
insert into FAMILLE values('panthere', 'Afrique','FAUX');
insert into FAMILLE values('coq', 'Europe','VRAI');
insert into FAMILLE values('ours', 'Europe','FAUX');
insert into FAMILLE values('renard', 'Europe','FAUX');
insert into FAMILLE values('serpent', 'Afrique','FAUX');
insert into FAMILLE values('serpent', 'Asie','FAUX');
insert into FAMILLE values('araignee', 'Asie','FAUX');
insert into FAMILLE values('araignee', 'Europe','VRAI');

--
-- ajout des n-uplets dans la table MENU
--
insert into MENU values( '1M', 10, 20);
insert into MENU values( '2M',  0, 100);
insert into MENU values( '3M',  0, 50);
insert into MENU values( '4M', 20,  0);
insert into MENU values( '5M',100,  0);
insert into MENU values( '6M', 20,  0);
insert into MENU values( '7M',  0, 10);
insert into MENU values( '8M',  0,  5);
insert into MENU values( '9M',0.1,  0);

--
-- ajout des n-uplets dans la table ANIMAL
--
insert into ANIMAL values(  '3A', 'Noble',   30, 'lion', 'Afrique','4M', NULL);
insert into ANIMAL values(  '1A', 'Hector',  10, 'lion', 'Afrique','5M', '3A');
insert into ANIMAL values( '13A', 'Mignon',  10, 'ours', 'Europe','1M', NULL);
insert into ANIMAL values(  '6A', 'Baloo',    5, 'ours', 'Europe','1M', '13A');
insert into ANIMAL values( '12A', 'Mignon',   1, 'ours', 'Europe','1M',  '6A');
insert into ANIMAL values(  '2A', 'Jacko',   20, 'singe', 'Europe','7M', NULL);
insert into ANIMAL values(  '4A', 'Marsupilani', 5, 'kangourou','Australie','7M', NULL);
insert into ANIMAL values(  '5A', 'Bagheera',  15, 'panthere', 'Afrique','4M', NULL);
insert into ANIMAL values(  '7A', 'Kaa',    2, 'serpent', 'Afrique','6M', NULL);
insert into ANIMAL values(  '8A', 'Isengrin',    4, 'renard', 'Europe','6M', NULL);
insert into ANIMAL values(  '9A', 'Chanteclerc', 2, 'coq', 'Europe','8M', NULL);
insert into ANIMAL values( '10A', 'Mignon',    3, 'panda', 'Chine','7M', NULL);
insert into ANIMAL values( '11A', 'Bongros',   8, 'elephant', 'Afrique','2M', NULL);
insert into ANIMAL values( '14A', 'Terrible',  2, 'tigre', 'Asie','4M', NULL);
insert into ANIMAL values( '15A', 'Pincette',  0.2, 'araignee', 'Asie','9M', NULL);
insert into ANIMAL values( '16A', 'Dentelle',  0.2, 'araignee', 'Europe','9M', NULL);










