DROP TABLE IF EXISTS lig_liv CASCADE;
DROP TABLE IF EXISTS livraison CASCADE;

DROP TABLE IF EXISTS lig_cmd CASCADE;
DROP TABLE IF EXISTS commande CASCADE;
DROP TABLE IF EXISTS article CASCADE;

DROP TABLE IF EXISTS fournisseur CASCADE;
DROP TABLE IF EXISTS client CASCADE;
DROP TABLE IF EXISTS magasin CASCADE;

CREATE TABLE magasin
( mag_num    integer constraint PK_MAGASIN primary key,
  mag_loc    varchar(25) NOT NULL,
  mag_ger    varchar(25) NOT NULL); 
  

CREATE TABLE client
( clt_num    integer constraint PK_CLIENT primary key,
  clt_nom    varchar(25) NOT NULL,
  clt_prenom varchar(20),
  clt_pays   varchar(2) NOT NULL,
  clt_loc    varchar(20) NOT NULL,
  clt_ca  integer,
  clt_type   varchar(16)); 
  

CREATE TABLE fournisseur
( frs_num    integer constraint PK_FOURNISSEUR primary key,
  frs_nom    varchar(25) NOT NULL 
                          constraint UK_FOURNISSEUR_NOM UNIQUE);
  

CREATE TABLE article
(  art_num   integer CONSTRAINT PK_ARTICLE primary key,
   art_nom   varchar(25) NOT NULL,
   art_coul  varchar(20),
   art_poids numeric(10,3),
   art_stock integer,
   art_pa    integer NOT NULL,
   art_pv    integer NOT NULL,
   art_frs   integer CONSTRAINT FK_ARTICLE_FRS references fournisseur(frs_num)
); 
   

CREATE TABLE commande
(  cmd_num   integer CONSTRAINT PK_COMMANDE primary key,
   cmd_date  date  NOT NULL,
   cmd_clt   integer NOT NULL 
             CONSTRAINT FK_COMMANDE_CLT references client (clt_num),
   cmd_mag   integer NOT NULL 
             CONSTRAINT FK_COMMANDE_MAG references magasin(mag_num)
);

CREATE TABLE lig_cmd
(  lcd_cmd   integer NOT NULL,
   lcd_art   integer NOT NULL,
   lcd_qte   integer NOT NULL,
   lcd_liv   integer DEFAULT 0,
   lcd_pu    integer NOT NULL,
   lcd_datliv date,
   constraint pk_lig_cmd primary key (lcd_cmd, lcd_art),
   constraint fk_lig_cmd_cmd foreign key (lcd_cmd) references commande(cmd_num),
   constraint fk_lig_cmd_art foreign key (lcd_art) references article(art_num) );
   
CREATE TABLE livraison
(  liv_num   integer CONSTRAINT PK_LIVRAISON primary key,
   liv_date  DATE,
   liv_clt   integer CONSTRAINT FK_LIVRAISON_CLT references CLIENT(clt_num),
   liv_mag   integer CONSTRAINT FK_LIVRAISON_MAG references MAGASIN(mag_num)
);

CREATE TABLE lig_liv
(  llv_liv   integer NOT NULL,
   llv_art   integer NOT NULL,
   llv_qte   integer     NOT NULL,
   llv_cmd   integer NOT NULL,
   constraint PK_lig_liv PRIMARY KEY(llv_liv, llv_art),
   constraint FK_LIG_LIV_LIV foreign key (llv_liv)references livraison( liv_num ),
   constraint FK_LIG_LIV_ART foreign key (llv_ART) references article( art_num ),
   constraint FK_LIG_LIV_CMD_ART foreign key (llv_cmd, llv_art) references lig_cmd(lcd_cmd, lcd_art)
);   