package popschool.mongo_web_book.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import popschool.mongo_web_book.dao.BookDao;
import popschool.mongo_web_book.model.Book;

import java.util.List;
import java.util.Optional;

@Service
public class BookService {

    @Autowired
    private BookDao bookDao;

    public Book createBook(Book book){
       return bookDao.save(book);
    };


    public List<Book> getAllBooks(){

        return bookDao.findAll();
    }


    public Optional<Book> findBookById(String id){
        return bookDao.findById(id);
    }


    public void deleteBookById(String id){
        bookDao.deleteById(id);
    }


    public void updateBook(Book book){
        bookDao.save(book);
    }


    public void deleteAllBooks(){
        bookDao.deleteAll();
    }
}
