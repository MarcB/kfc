package popschool.mongo_web_book.dao;


import org.springframework.data.mongodb.repository.MongoRepository;
import popschool.mongo_web_book.model.Book;

public interface BookDao extends MongoRepository<Book, String> {
}
