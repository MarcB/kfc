package popschool.mongo_web_book.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import popschool.mongo_web_book.model.Book;
import popschool.mongo_web_book.service.BookService;

import java.util.List;

@RestController
public class BookController {
    @Autowired
    private BookService bookService;

    // URI:
    // /contextPath/servletPath/books
    @GetMapping(value = "/books")
    @ResponseBody
    public List<Book> getBooks() {
        return bookService.getAllBooks();

    }

    // URI:
    // /contextPath/servletPath/books/{bookId}
    @GetMapping(value = "/books/{bookId}")
    public Book getBook(@PathVariable("bookId") String bookId) {
        return bookService.findBookById(bookId).get();
    }




    // URI:
    // /contextPath/servletPath/books
    @PostMapping(value = "/books")
    public ResponseEntity<Book> addBook(@RequestBody Book book) {

        Book nouv = bookService.createBook(book);
        return ResponseEntity.status(HttpStatus.CREATED).body(nouv);

    }

}
