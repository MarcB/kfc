package popschool.mongo_web_book;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MongoWebBookApplication {

    public static void main(String[] args) {
        SpringApplication.run(MongoWebBookApplication.class, args);
    }

}
