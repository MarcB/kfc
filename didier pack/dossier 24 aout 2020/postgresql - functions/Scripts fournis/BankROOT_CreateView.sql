

-- -------------------------------------------------------------------------- --
--	                                      									       juin 2020  --
--																			  --
--      S C R I P T   D E   C R � A T I O N    D E S    V U E S               --
--																			  --
-- -------------------------------------------------------------------------- --




-- ========================================================================== --
--	CR�ATION des vues C O M P T E S  et  C A R T E S  par  T I T U L A I R E  --
-- ========================================================================== --

-- -------------------------------------------------------------------------- --
--	Comptes par titulaire                                                     --
-- -------------------------------------------------------------------------- --
DROP VIEW IF EXISTS INFOS_TITULAIRES_COMPTES_VIEW ;
CREATE VIEW INFOS_TITULAIRES_COMPTES_VIEW
AS SELECT P.ID        AS ID_PERSONNE,
          P.CATEGORIE AS TYPE_PERSONNE, 
          P.CIVILITE, 
          P.PRENOM, 
          P.NOM, 
          C.NUMERO, 
          C.CATEGORIE AS TYPE_COMPTE, 
          C.LIBELLE, 
          C.SOLDE, 
          C.BLOQUE,
          C.DECOUVERT_AUTORISE, 
          C.TAUX_ANNUEL, 
          C.DATE_MAJ
   FROM PERSONNE P
        JOIN RATTACHEMENT R ON (P.ID = R.ID_TITULAIRE)
		JOIN COMPTE_DEPOT C ON (R.ID_COMPTE = C.NUMERO)
   ORDER BY P.CATEGORIE, P.NOM
;

-- -------------------------------------------------------------------------- --
--	Carte par titulaire                                                       --
-- -------------------------------------------------------------------------- --
DROP VIEW IF EXISTS INFOS_TITULAIRES_CARTES_VIEW; 
CREATE VIEW INFOS_TITULAIRES_CARTES_VIEW 
AS SELECT P.ID        AS ID_PERSONNE,
          P.CATEGORIE AS TYPE_PERSONNE, 
          P.CIVILITE, 
          P.PRENOM, 
          P.NOM, 
          C.NUMERO, 
          C.CATEGORIE AS TYPE_COMPTE, 
          C.LIBELLE, 
          C.SOLDE, 
          CB.BLOQUEE,
          CB.DATE_EXPIRATION, 
          T.STATUT
   FROM PERSONNE P
        JOIN CARTE_BANCAIRE CB ON (P.ID = CB.ID_TITULAIRE)
		JOIN COMPTE_DEPOT C    ON (CB.ID_COMPTE = C.NUMERO)
		JOIN TYPE_CARTE T      ON (CB.ID_TYPE_CARTE = T.ID)
   ORDER BY P.CATEGORIE, P.NOM
;

-- -------------------------------------------------------------------------- --
--	Test d'affichage de la vue des comptes des titulaires                     --
-- -------------------------------------------------------------------------- --
SELECT * FROM INFOS_TITULAIRES_COMPTES_VIEW ;

-- ========================================================================== --
