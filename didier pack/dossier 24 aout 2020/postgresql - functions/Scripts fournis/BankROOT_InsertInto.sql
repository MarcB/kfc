

-- -------------------------------------------------------------------------- --
--	                    									       juin 2020  --
--																			  --
--      S C R I P T   D E   P E U P L E M E N T   D E S   T A B L E S         --
--																			  --
-- -------------------------------------------------------------------------- --



-- ========================================================================== --
--	I N S E R T I O N S   dans la table des  P E R S O N N Es                 --
-- ========================================================================== --

-- -------------------------------------------------------------------------- --
--		Cas des Particuliers                                                  --	
-- -------------------------------------------------------------------------- --
INSERT INTO PERSONNE 
		  (CATEGORIE, 	CIVILITE, PRENOM, 			NOM, 			ADRESSE) 
  VALUES  ('Particulier', 'M.',	  'S�raphin', 		'LAMPION',  	'MOULINSART'),
		  ('Particulier', 'Mme',  'Elvire', 		'MENKIBLOC',  	'LYON'),
		  ('Particulier', 'Mme',  'Sheila', 		'KRIZ',  		'LYON'),
		  ('Particulier', 'M.',   'Phil�mon', 		'BLAIS',  		'LILLE'),
--		  ('Particulier', 'M.',   'Adh�mar', 		'D�SAGIO',  	'MARSEILLE'),
--		  ('Particulier', 'M.',   'Humphrey', 		'INCRAIDIT',  	'LYON'),
		  ('Particulier', 'Mme',  'Debbie', 		'DIPHAIRET',	'MARSEILLE'),
		  ('Particulier', 'Mme',  'Jessica-Ren�e',	'DE CHAIQUE',  	'NANTES'),
		  ('Particulier', 'Mme',  'Candice', 		'PAR�-LOSEILLE','NANTES'),
		  ('Particulier', 'M.',   'Am�d�e', 		'COUV�RE',  	'PARIS'),
		  ('Particulier', 'Mme',  'Dj�mila', 		'BANCANFAHITE', 'LILLE'),
		  ('Particulier', 'Mme',  'H�l�na', 		'PERDULECODE',  'LILLE');
		 
-- -------------------------------------------------------------------------- --
--		Cas des Entreprises                                                   --
-- -------------------------------------------------------------------------- --
INSERT INTO PERSONNE 
			(CATEGORIE,  	NOM,  		 ADRESSE, 	   SIREN, 	DIRIGEANT) 
    VALUES 	('Entreprise', 'YOUPLABOUM', 'MOULINSART', 12345, 
					(
						SELECT p.ID 
						FROM personne p 
						WHERE P.nom = 'LAMPION'
					)
				);
																  
-- ========================================================================== --
--	I N S E R T I O N S   dans la table des   C O M P T Es                    --
-- ========================================================================== --

-- -------------------------------------------------------------------------- --
-- 		Cas de comptes simples                                                --
-- -------------------------------------------------------------------------- --
INSERT INTO COMPTE_DEPOT (CATEGORIE, SOLDE, LIBELLE, DECOUVERT_AUTORISE)
           VALUES ('Compte_Depot', 1000, 'M. S�raphin LAMPION',   -500),
		          ('Compte_Depot',  523, 'Mme Debbie DIPHAIRET',  -200),
		          ('Compte_Depot',  568, 'Mme MENKIBLOC',         -100),
		          ('Compte_Depot',   30, 'Mme KRIZ',                 0),
		          ('Compte_Depot',  789, 'Mme PAR�-LOSEILLE',     -150),
		          ('Compte_Depot', 1500, 'M. COUV�RE',           -1500),
		          ('Compte_Depot',  752, 'Mme PERDULECODE',       -500)
;
				  
-- -------------------------------------------------------------------------- --
--		Cas de comptes r�mun�r�s                                              --
-- -------------------------------------------------------------------------- --
INSERT INTO COMPTE_DEPOT 
		(CATEGORIE, SOLDE, LIBELLE, DECOUVERT_AUTORISE,TAUX_ANNUEL, DATE_MAJ )
 VALUES ('Compte_Remunere', 10000, 'YOUPLABOUM EIRL', 			    -1000,  0.02, 
			current_timestamp),
		('Compte_Remunere',  1000, 'Mme Debbie DIPHAIRET Livret A',     0, 0.005, 
			current_timestamp),
		('Compte_Remunere',  1000, 'Mme BANCANFAHITE', 			     -500, 0.005, 
			current_timestamp),
		('Compte_Remunere',  1000, 'M. BLAIS', 					     -500,  0.01, 
			current_timestamp)
;
				  

-- ========================================================================== --
--	I N S E R T I O N S  dans la table des  R A T T A C H E M E N Ts          --
-- ========================================================================== --
																  

-- -------------------------------------------------------------------------- --
--	compte de LAMPION                                                         --
-- -------------------------------------------------------------------------- --
INSERT INTO RATTACHEMENT 
		(ID_TITULAIRE, ID_COMPTE)
    VALUES 
		(
			(SELECT P.ID FROM PERSONNE P WHERE P.NOM = 'LAMPION'),
		    (SELECT C.NUMERO FROM COMPTE_DEPOT C WHERE C.LIBELLE = 'M. S�raphin LAMPION')
		)
; 
				   
-- -------------------------------------------------------------------------- --
--	2 comptes de Debbie DIPHAIRET                                             --
-- -------------------------------------------------------------------------- --
INSERT INTO RATTACHEMENT 
		(ID_TITULAIRE, ID_COMPTE)
    VALUES 
		(
			(SELECT P.ID FROM PERSONNE P WHERE P.NOM = 'DIPHAIRET'),
		    (SELECT C.NUMERO FROM COMPTE_DEPOT C WHERE C.LIBELLE = 'Mme Debbie DIPHAIRET')
		),
		(
			(SELECT P.ID FROM PERSONNE P WHERE P.NOM = 'DIPHAIRET'),
		    (SELECT C.NUMERO FROM COMPTE_DEPOT C WHERE C.LIBELLE = 'Mme Debbie DIPHAIRET Livret A')
		)
;

-- -------------------------------------------------------------------------- --
--	compte de Elvire MENKIBLOC                                                --
-- -------------------------------------------------------------------------- --
INSERT INTO RATTACHEMENT 
		(ID_TITULAIRE, ID_COMPTE)
    VALUES 
		(
			(SELECT P.ID FROM PERSONNE P WHERE P.NOM = 'MENKIBLOC'),
		    (SELECT C.NUMERO FROM COMPTE_DEPOT C WHERE C.LIBELLE = 'Mme MENKIBLOC')
		) 
;	
				   
-- -------------------------------------------------------------------------- --
--	compte de Sheila KRIZ                                                     --
-- -------------------------------------------------------------------------- --
INSERT INTO RATTACHEMENT 
		(ID_TITULAIRE, ID_COMPTE)
    VALUES 
		(
			(SELECT P.ID FROM PERSONNE P WHERE P.NOM = 'KRIZ'),
		    (SELECT C.NUMERO FROM COMPTE_DEPOT C WHERE C.LIBELLE = 'Mme KRIZ')
		) 
;	
				   
-- -------------------------------------------------------------------------- --
--	compte de Candice  PAR�-LOSEILLE                                          --
-- -------------------------------------------------------------------------- --
INSERT INTO RATTACHEMENT 
		(ID_TITULAIRE, ID_COMPTE)
    VALUES 
		(
			(SELECT P.ID FROM PERSONNE P WHERE P.NOM = 'PAR�-LOSEILLE'),
		    (SELECT C.NUMERO FROM COMPTE_DEPOT C WHERE C.LIBELLE = 'Mme PAR�-LOSEILLE')
		) 
;	
				   
-- -------------------------------------------------------------------------- --
--	compte de Am�d�e COUV�RE                                                  --
-- -------------------------------------------------------------------------- --
INSERT INTO RATTACHEMENT 
		(ID_TITULAIRE, ID_COMPTE)
    VALUES 
		(
			(SELECT P.ID FROM PERSONNE P WHERE P.NOM = 'COUV�RE'),
		    (SELECT C.NUMERO FROM COMPTE_DEPOT C WHERE C.LIBELLE = 'M. COUV�RE')
		) 
;
				   
-- -------------------------------------------------------------------------- --
--	compte de H�l�na PERDULECODE                                              --
-- -------------------------------------------------------------------------- --
INSERT INTO RATTACHEMENT 
		(ID_TITULAIRE, ID_COMPTE)
    VALUES 
		(
			(SELECT P.ID FROM PERSONNE P WHERE P.NOM = 'PERDULECODE'),
		    (SELECT C.NUMERO FROM COMPTE_DEPOT C WHERE C.LIBELLE = 'Mme PERDULECODE')
		) 
;	
				   
-- -------------------------------------------------------------------------- --
--	compte de Dj�mila BANCANFAHITE                                            --
-- -------------------------------------------------------------------------- --
INSERT INTO RATTACHEMENT 
		(ID_TITULAIRE, ID_COMPTE)
    VALUES 
		(
			(SELECT P.ID FROM PERSONNE P WHERE P.NOM = 'BANCANFAHITE'),
		    (SELECT C.NUMERO FROM COMPTE_DEPOT C WHERE C.LIBELLE = 'Mme BANCANFAHITE')
		) 
;	
			   
-- -------------------------------------------------------------------------- --
--	compte de Phil�mon BLAIS                                                  --
-- -------------------------------------------------------------------------- --
INSERT INTO RATTACHEMENT 
		(ID_TITULAIRE, ID_COMPTE)
    VALUES 
		(
			(SELECT P.ID FROM PERSONNE P WHERE P.NOM = 'BLAIS'),
		    (SELECT C.NUMERO FROM COMPTE_DEPOT C WHERE C.LIBELLE = 'M. BLAIS')
		) 
;

				   
-- -------------------------------------------------------------------------- --
--	compte de 'YOUPLABOUM EIRL                                                --
-- -------------------------------------------------------------------------- --
INSERT INTO RATTACHEMENT 
		(ID_TITULAIRE, ID_COMPTE)
    VALUES 
		(
			(SELECT P.ID FROM PERSONNE P WHERE P.NOM = 'YOUPLABOUM'),
		    (SELECT C.NUMERO FROM COMPTE_DEPOT C WHERE C.LIBELLE = 'YOUPLABOUM EIRL')
		)
;
				   
-- ========================================================================== --
--	I N S E R T I O N S  dans la table des  T Y P Es _ C A R T E              --
-- ========================================================================== --
INSERT INTO TYPE_CARTE 
			(STATUT,	PLAFOND_RETRAIT,	COTISATION_ANNUELLE)
    VALUES 
			('Basic', 		 1000, 				 50),
			('Bronze', 		 2000, 				100),
			('Silver', 		 4000, 				200),
			('Gold', 		10000, 				500)
;
					
-- ========================================================================== --
--	I N S E R T I O N S  dans la table des  C A R T Es _ B A N C A I R Es     --
-- ========================================================================== --

-- -------------------------------------------------------------------------- --
--	carte de LAMPION sur son compte personnel                                 --
-- -------------------------------------------------------------------------- --
INSERT INTO CARTE_BANCAIRE 
		(
			ID_TITULAIRE,
	        ID_COMPTE,
	        ID_TYPE_CARTE,
	        CODE_SECRET,
	        DATE_EXPIRATION
		)
	VALUES 
		(
			(SELECT P.ID FROM PERSONNE P WHERE P.NOM = 'LAMPION'),
		    (SELECT C.NUMERO FROM COMPTE_DEPOT C WHERE C.LIBELLE = 'M. S�raphin LAMPION'),
			(SELECT T.ID FROM TYPE_CARTE T WHERE T.STATUT = 'Basic'),
			1234,
			TO_DATE('31/01/2022', 'DD/MM/YYYY')
		)
;
										  

-- -------------------------------------------------------------------------- --
--	carte de LAMPION pour son entreprise                                      --
-- -------------------------------------------------------------------------- --
INSERT INTO CARTE_BANCAIRE 
		(
			ID_TITULAIRE,
	        ID_COMPTE,
	        ID_TYPE_CARTE,
	        CODE_SECRET,
	        DATE_EXPIRATION
		)
	VALUES 
		(
			(SELECT P.ID FROM PERSONNE P WHERE P.NOM = 'LAMPION'),
		    (SELECT C.NUMERO FROM COMPTE_DEPOT C WHERE C.LIBELLE = 'YOUPLABOUM EIRL'),
			(SELECT T.ID FROM TYPE_CARTE T WHERE T.STATUT = 'Silver'),
			1234,
			TO_DATE('31/01/2022', 'DD/MM/YYYY')
		)
;

-- -------------------------------------------------------------------------- --
--	carte de Debbie DIPHAIRET sur son compte r�mun�r�	                      --                      
-- -------------------------------------------------------------------------- --
INSERT INTO CARTE_BANCAIRE 
		(
			ID_TITULAIRE,
	        ID_COMPTE,
	        ID_TYPE_CARTE,
	        CODE_SECRET,
	        DATE_EXPIRATION
		)
	VALUES 
		(
			(SELECT P.ID FROM PERSONNE P WHERE P.NOM = 'DIPHAIRET'),
		    (SELECT C.NUMERO FROM COMPTE_DEPOT C WHERE C.LIBELLE = 'Mme Debbie DIPHAIRET Livret A'),
			(SELECT T.ID FROM TYPE_CARTE T WHERE T.STATUT = 'Silver'),
			1234,
			TO_DATE('31/01/2022', 'DD/MM/YYYY')
		)
;			

-- ========================================================================== --
