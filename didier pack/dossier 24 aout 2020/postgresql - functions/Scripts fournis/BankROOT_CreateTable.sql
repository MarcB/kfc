

--==========================================================================================

-- -------------------------------------------------------------------------- --
--	                     									       juin 2020  --
--																			  --
--      S C R I P T   D E   C R � A T I O N    D E S   T A B L E S            --
--																			  --
-- -------------------------------------------------------------------------- --



 

-- -------------------------------------------------------------------------- --
--	PAQUETAGE GesionClient : cr�ation de Personne
-- -------------------------------------------------------------------------- --
CREATE TABLE PERSONNE 
( 
    --	attributs de la classe Personne
	ID        	SERIAL , 	
	CATEGORIE 	VARCHAR(30) NOT NULL, 
	NOM       	VARCHAR(30) NOT NULL, 
	ADRESSE   	VARCHAR(30) NOT NULL,
	
	--	attributs de la classe  Particulier
	PRENOM    	VARCHAR(30),
	CIVILITE  	VARCHAR(3) 
					CONSTRAINT CK_PERSONNE_CIVILITE 
						CHECK (CIVILITE IN ('M.', 'Mme')),
						
	--	attributs de  la classe Entreprise
	SIREN     	INTEGER,
	DIRIGEANT 	INTEGER,
	--fin de d�claration des colonnes d�but des contraintes
	
	--	d�finition des contraintes
	CONSTRAINT PK_PERSONNE 
					PRIMARY KEY(ID),
	CONSTRAINT FK_PERSONNE_DIRIGEANT 
					FOREIGN KEY (DIRIGEANT) 
						REFERENCES PERSONNE(ID),
						
	/*	Contraintes sur les unicit�s pour chacune des sous classes */

		/* 	Cas d'un Particulier */
	CONSTRAINT UN_PERSONNE_PARTICULIER_CLE_SECONDAIRE 
					UNIQUE (CATEGORIE, CIVILITE, PRENOM, NOM, ADRESSE),	

		/* 	Cas d'une Entreprise */
	CONSTRAINT UN_PERSONNE_Entreprise_CLE_SECONDAIRE 
					UNIQUE (CATEGORIE, SIREN),
	
	/* 
	 *	contrainte sur la cat�gorie pour la coh�rence de la 
	 *	g�n�ralisation/sp�cialisation
	 *	inconv�nient de fa�on de v�rifier la coh�rence : 
	 *     en cas d'erreur le message n'est pas explicite sur l'erreur commise 
	 */	
	CONSTRAINT CK_PERSONNE_CATEGORIE_GS 
		CHECK (
				(
					CATEGORIE = 'Particulier' 	AND 
					PRENOM IS NOT NULL          AND 
					CIVILITE IS NOT NULL		AND 
					SIREN IS NULL				AND 
					DIRIGEANT IS NULL
				)
		        OR
				(
					CATEGORIE = 'Entreprise' 	AND 
					PRENOM IS NULL				AND 
					CIVILITE IS NULL			AND 
					SIREN IS NOT NULL			AND 
					DIRIGEANT IS NOT NULL
				)
			  )	--	fin de la contrainte CHECK
)
;

 
-- -------------------------------------------------------------------------- --
--	PAQUETAGE GestionComptes : cr�ation de Compte_Depot                       --
-- -------------------------------------------------------------------------- --

CREATE TABLE COMPTE_DEPOT 
( 
    --	attributs de la classe Compte_depot
	NUMERO             SERIAL,
	CATEGORIE          VARCHAR(30) NOT NULL, 
    DATE_CREATION      TIMESTAMP NOT NULL	
							DEFAULT current_timestamp,
	SOLDE              NUMERIC(12, 2) NOT NULL 
							DEFAULT 0,
	BLOQUE             BOOLEAN 
							DEFAULT FALSE,
	DECOUVERT_AUTORISE NUMERIC (8, 2) 
							DEFAULT 0, -- peut �tre n�gatif ou nul
	LIBELLE            VARCHAR(30) NOT NULL,
	
	--	attributs de la classe  Compte_R�munere
	TAUX_ANNUEL        NUMERIC (3, 2),
    DATE_MAJ           TIMESTAMP,
	--	fin de d�claration des colonnes d�but des contraintes
	
	--d�finition des contraintes
	CONSTRAINT PK_COMPTE_DEPOT 
						PRIMARY KEY(NUMERO),
	CONSTRAINT UN_COMPTE_DEPOT_NUMERO 
						UNIQUE (NUMERO),
	CONSTRAINT CK_COMPTE_DEPOT_DECOUVERT_AUTORISE 
						CHECK (DECOUVERT_AUTORISE <= 0),
	CONSTRAINT CK_COMPTE_DEPOT_TAUX_ANNUEL 
						CHECK (TAUX_ANNUEL > 0),
	
	/* 	contrainte sur la cat�gorie pour la coh�rence de la 
	 *	g�n�ralisation/sp�cialisation
	 *	inconv�nient de fa�on de v�rifier la coh�rence : 
	 *     en cas d'erreur le message n'est pas explicite sur l'erreur commise 
	 */	
	CONSTRAINT CK_COMPTE_DEPOT_CATEGORIE_GS 
		CHECK 	(
					(
						CATEGORIE = 'Compte_Depot' 		AND 
						TAUX_ANNUEL IS NULL				AND 
						DATE_MAJ IS NULL	
					)
		            OR
					(
						CATEGORIE = 'Compte_Remunere'	AND 
						TAUX_ANNUEL IS NOT NULL			AND 
						DATE_MAJ IS NOT NULL
					)
				)--	fin de la contrainte CHECK
)
;


-- -------------------------------------------------------------------------- --
--	PAQUETAGE GestionComptes : cr�ation des rattachements                     --
-- -------------------------------------------------------------------------- --
CREATE TABLE RATTACHEMENT 
( 
	ID              	SERIAL ,
    ID_COMPTE         	INTEGER NOT NULL,
	ID_TITULAIRE      	INTEGER NOT NULL,
	DATE_RATTACHEMENT 	TIMESTAMP NOT NULL 
							DEFAULT current_timestamp,
	--	fin de d�claration des colonnes d�but des contraintes
	
	--	d�finition des contraintes
	CONSTRAINT PK_RATTACHEMENT 
					PRIMARY KEY(ID),
	CONSTRAINT FK_RATTACHEMENT_ID_COMPTE 
					FOREIGN KEY (ID_COMPTE) 
						REFERENCES COMPTE_DEPOT(NUMERO),	
	CONSTRAINT FK_RATTACHEMENT_ID_TITULAIRE 
					FOREIGN KEY (ID_TITULAIRE) 
						REFERENCES PERSONNE(ID),
	--	absence de doublon
	CONSTRAINT UN_FK_RATTACHEMENT 
					UNIQUE (ID_COMPTE, ID_TITULAIRE) 
)
;


-- -------------------------------------------------------------------------- --
--	PAQUETAGE GestionComptes : cr�ation des types de cartes                   --
-- -------------------------------------------------------------------------- --
CREATE TABLE TYPE_CARTE 
( 
	ID             		SERIAL,
	STATUT              VARCHAR(30) NOT NULL,
	PLAFOND_RETRAIT     NUMERIC(10, 2) NOT NULL,
	COTISATION_ANNUELLE NUMERIC(8, 2) NOT NULL,
	--	fin de d�claration des colonnes d�but des contraintes
	
	--	d�finition des contraintes
	CONSTRAINT PK_TYPE_CARTE 
					PRIMARY KEY(ID),
	CONSTRAINT CK_TYPE_CARTE_PLAFOND_RETRAIT     
					CHECK (PLAFOND_RETRAIT > 0),
	CONSTRAINT CK_TYPE_CARTE_COTISATION_ANNUELLE 
					CHECK (COTISATION_ANNUELLE > 0)
)
;

-- -------------------------------------------------------------------------- --
--	PAQUETAGE GestionComptes : cr�ation des cartes                            --
-- -------------------------------------------------------------------------- --
CREATE TABLE CARTE_BANCAIRE 
( 
	NUMERO                  SERIAL,
	ID_TITULAIRE            INTEGER NOT NULL,
	ID_COMPTE               INTEGER NOT NULL,
	ID_TYPE_CARTE           INTEGER NOT NULL,
	CODE_SECRET             INTEGER NOT NULL,
	NB_ESSAIS               INTEGER NOT NULL 
								DEFAULT 0,
	BLOQUEE					BOOLEAN NOT NULL 
								DEFAULT FALSE,
	DATE_EXPIRATION	        DATE NOT NULL,
	--	fin de d�claration des colonnes d�but des contraintes
	
	--	d�finition des contraintes
	CONSTRAINT PK_CARTE_BANCAIRE 
					PRIMARY KEY(NUMERO),	
	CONSTRAINT FK_CARTE_BANCAIRE_ID_TITULAIRE 
					FOREIGN KEY (ID_TITULAIRE)  
						REFERENCES PERSONNE(ID),
	CONSTRAINT FK_CARTE_BANCAIRE_ID_COMPTE    
					FOREIGN KEY (ID_COMPTE)     
						REFERENCES COMPTE_DEPOT(NUMERO),
	CONSTRAINT FK_CARTE_BANCAIRE_TYPE_CARTE   
					FOREIGN KEY (ID_TYPE_CARTE) 
						REFERENCES TYPE_CARTE(ID),
    CONSTRAINT CK_CARTE_BANCAIRE_CODE_SECRET  
					CHECK (CODE_SECRET BETWEEN 1000 AND 9999),
    CONSTRAINT CK_CARTE_BANCAIRE_NB_ESSAIS    
					CHECK (NB_ESSAIS BETWEEN 0 AND 2)
)
;

-- -------------------------------------------------------------------------- --
--	PAQUETAGE GestionOp�rations : cr�ation des op�rations                     --
-- -------------------------------------------------------------------------- --
CREATE TABLE OPERATION 
( 
    --attributs de la classe Op�ration
	NUMERO             SERIAL,
	CATEGORIE          VARCHAR(30) NOT NULL, 
    DATE_OPERATION     TIMESTAMP NOT NULL 
							DEFAULT CURRENT_TIMESTAMP,
	ID_COMPTE          INTEGER NOT NULL,
	
	--	attributs de la classe  Operation Blocage D�blocage
	OPERATION 		   VARCHAR(30),
	
	--	attributs de la classe  Operation sur compte
	MONTANT            NUMERIC(12, 2) ,
	EFFECTUEE          BOOLEAN,
	CAUSE_REFUS        VARCHAR(30),
	
	--	attributs de la classe Operation debit_cheque
	NUMERO_CHEQUE      INTEGER,
	
	--	attributs de la classe Operation debit_carte
	ID_CARTE_BANCAIRE  INTEGER,
	CODE_SECRET_SAISI  INTEGER,
	--	fin de d�claration des colonnes d�but des contraintes
	
	--d�finition des contraintes
	CONSTRAINT PK_OPERATION 
					PRIMARY KEY(NUMERO),
	CONSTRAINT FK_OPERATION_ID_COMPTE         
					FOREIGN KEY (ID_COMPTE)         
						REFERENCES COMPTE_DEPOT(NUMERO),
	CONSTRAINT FK_OPERATION_ID_CARTE_BANCAIRE 
					FOREIGN KEY (ID_CARTE_BANCAIRE) 
						REFERENCES CARTE_BANCAIRE(NUMERO),
    CONSTRAINT CK_OPERATION_NUMERO_CHEQUE     
					CHECK (NUMERO_CHEQUE > 0),
	CONSTRAINT CK_OPERATION_CODE_SECRET_SAISI 
					CHECK (CODE_SECRET_SAISI BETWEEN 1000 and 9999),
	
	/* 
	 *	contrainte sur la categorie pour la coh�rence de la 
	 *	g�n�ralisation/sp�cialisation
	 *	inconv�nient de fa�on de v�rifier la coh�rence : 
	 *     en cas d'erreur le message n'est pas explicite sur l'erreur commise 
	 */
	CONSTRAINT CK_OPERATION_CATEGORIE_GS 
		CHECK 	(
					(
						CATEGORIE = 'Blocage-Deblocage'     	AND 
						OPERATION IN ('Blocage', 'Deblocage')  	AND 
						MONTANT IS NULL							AND 
						EFFECTUEE IS NULL						AND 
						CAUSE_REFUS IS NULL						AND 
						NUMERO_CHEQUE IS NULL					AND 
						ID_CARTE_BANCAIRE IS NULL				AND 
						CODE_SECRET_SAISI IS NULL
					)
		            OR
					(	
						CATEGORIE = 'Credit' 					AND 
						OPERATION IS NULL						AND 
						MONTANT > 0								AND 
						EFFECTUEE = True						AND 
						CAUSE_REFUS IS NULL						AND 
						NUMERO_CHEQUE IS NULL					AND 
						ID_CARTE_BANCAIRE IS NULL				AND 
						CODE_SECRET_SAISI IS NULL
					)
					OR
					(
						CATEGORIE = 'Debit' 				    AND 
						OPERATION IS NULL 						AND 
						MONTANT < 0								AND 
						(
							(EFFECTUEE = TRUE AND CAUSE_REFUS IS NULL) 
							OR 
							(EFFECTUEE = FALSE AND CAUSE_REFUS IS NOT NULL)
						)										AND 
						NUMERO_CHEQUE IS NULL					AND 
						ID_CARTE_BANCAIRE IS NULL			    AND 
						CODE_SECRET_SAISI IS NULL
					)
					OR
					(
						CATEGORIE = 'Debit Cheque'				AND 
						OPERATION IS NULL 						AND 
						MONTANT < 0								AND 
						(
							(EFFECTUEE = TRUE AND CAUSE_REFUS IS NULL) 
							OR 
							(EFFECTUEE = FALSE AND CAUSE_REFUS IS NOT NULL)
						)										AND 
						NUMERO_CHEQUE IS NOT NULL				AND 
						ID_CARTE_BANCAIRE IS NULL				AND 
						CODE_SECRET_SAISI IS NULL
					)
					OR
					(
						CATEGORIE = 'Debit Carte' 				AND 
						OPERATION IS NULL 						AND 
						MONTANT < 0								AND 
						(
							(EFFECTUEE = TRUE AND CAUSE_REFUS IS NULL) 
							OR 
							(EFFECTUEE = FALSE AND CAUSE_REFUS IS NOT NULL)
						)										AND 
						NUMERO_CHEQUE IS NULL					AND 
						ID_CARTE_BANCAIRE IS NOT NULL			AND 
						CODE_SECRET_SAISI IS NOT NULL
					)
				)	--	fin de la contrainte CHECK
)
;
-- -------------------------------------------------------------------------- --

					