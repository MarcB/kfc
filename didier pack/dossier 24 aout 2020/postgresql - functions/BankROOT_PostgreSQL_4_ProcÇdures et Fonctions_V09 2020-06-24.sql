/* *******************************************
Avant de lancer ce script 
1) Remplacez partout la cha�ne $$$$INITIALES$$$$ par vos initiales (2 ou 3 caract�res sans espace)
  (pensez � faire un "remplacer partout" car il y a 3 remplacements 
  � faire y compris celui de ce commentaire)

2) Sauvegardez le script dans un dossier (par exemple C:\remp\sin)

3) Ex�cution du script avec DBeaver
	   a) ouvrez une connexion sur la base de donn�es
	   b) ouvrez un �diteur SQL (touche F3)
	   c) copiez tout le script contenu dans ce fichier(CTRL A, CTRL C)
	   d) s'il n'est pas vide, effacer le contenu de l'�diteur SQL de DBeaver (CTRL A, Suppr)
	   e) collez le contenu du presse papier  dans l'�diteur SQL de DBeaver (CTRL V)
       f) ex�cutez la totalit� du script (Alt X)	 
	   
4) � la fin d'ex�cution, v�rifiez la pr�sence 
   des fonctions et proc�dures dans le navigateur (F5 parfois n�cessaire)
*/

-- -------------------------------------------------------------------------- --
--	SIN S�ances 13-14    									       juin 2020  --
--																			  --
--      S C R I P T   D E   C R � A T I O N    D E S    R O U T I N E S       --
--																			  --
-- -------------------------------------------------------------------------- --

-- red�finition du chemin de recherche des objets de la base 
-- pour �viter de devoir utiliser la notation point�e
-- le sch�ma BANK_ROOT est le premier schema
SET search_path TO BANK_ROOT_$$$$INITIALES$$$$, PUBLIC;
--
	
-- -------------------------------------------------------------------------- --
--	Suppression manuelle pr�alable de toutes les proc�dures, fonctions et vues cr��es  --
--	dans ce script, pour permettre un retour � l'�tat initial si n�cessaire   --
-- -------------------------------------------------------------------------- --
--
--	1)	Suppression des proc�dures initiales
--
/*
DROP PROCEDURE IF EXISTS Creer_Particulier CASCADE;
DROP PROCEDURE IF EXISTS Creer_Compte_From_Personne_Id  CASCADE;
DROP PROCEDURE IF EXISTS Crediter_Compte  CASCADE;
DROP PROCEDURE IF EXISTS Debiter_Compte CASCADE;
DROP PROCEDURE IF EXISTS Faire_Virement CASCADE;
--
--	2)	Suppression des fonctions initiales
--
DROP FUNCTION IF EXISTS Get_Id_From_Prenom_Nom_Adresse CASCADE;
DROP FUNCTION IF EXISTS Particulier_Exists CASCADE;
DROP FUNCTION IF EXISTS Personne_Exists CASCADE;
DROP FUNCTION IF EXISTS Get_Nb_Comptes_From_Id CASCADE;
DROP FUNCTION IF EXISTS Compte_Exists CASCADE;
--
--	3)	Suppression des proc�dures et fonctions compl�mentaires
--
DROP PROCEDURE IF EXISTS Bloquer_Compte CASCADE; 
DROP PROCEDURE IF EXISTS Debloquer_Compte CASCADE;
DROP PROCEDURE IF EXISTS Creer_Entreprise CASCADE;
DROP PROCEDURE IF EXISTS Creer_Carte CASCADE;
DROP FUNCTION  IF EXISTS Random_Between CASCADE;
--
--	4)	Suppression de la vue
--
DROP VIEW IF EXISTS Info_Particuliers_Vue;
*/

/*Suppression automatique (et violente) de TOUTES les procedures et fonctions du schema pass� en param�tre
Les objets d�pendants (vues, ...) sont �galement d�truits */

CREATE OR REPLACE PROCEDURE public.delete_all_routines_cascade(p_schema text)

LANGUAGE plpgsql 
AS
$$
DECLARE
    v_query text;

BEGIN
    SELECT  string_agg(format('DROP %s %s %s;'
                              , CASE prokind
                                    WHEN 'f' THEN 'FUNCTION'
                                    WHEN 'a' THEN 'AGGREGATE'
                                    WHEN 'p' THEN 'PROCEDURE'
                                    WHEN 'w' THEN 'FUNCTION'  -- window function (rarely applicable)
                             -- ELSE NULL              -- not possible in pg 11
                                END
                            , oid::regprocedure 
					        ,' CASCADE ')  --end of format
                      , E'\n') --end of string_agg
	INTO v_query
    FROM pg_proc
    WHERE pronamespace = p_schema :: regnamespace;

    IF v_query IS NOT NULL THEN
        EXECUTE v_query;
        RAISE NOTICE '************************Deleted all functions in schema: %', p_schema;
		RAISE NOTICE '%', v_query;		
		RAISE NOTICE '*************************End of deletion';
    ELSE
        RAISE NOTICE '*************************No functions to delete in schema: %', p_schema;
    END IF;
END
$$;


CALL public.delete_all_routines_cascade(p_schema =>'bank_root_$$$$INITIALES$$$$');

-- -------------------------------------------------------------------------- --
--	Question n�1 : Insertion d'un particulier dans la table des personnes     --
-- -------------------------------------------------------------------------- --
DROP procedure IF EXISTS creer_particulier cascade;
CREATE OR REPLACE PROCEDURE creer_particulier
		(
			p_civilite IN personne.civilite%TYPE,
            p_prenom   IN personne.prenom%TYPE,
            p_nom      IN personne.nom%TYPE,
            p_adresse  IN personne.adresse%TYPE
		)
LANGUAGE plpgsql
AS 
$$
	BEGIN
		INSERT INTO personne 
			(categorie, prenom,  nom, adresse,civilite)
	    VALUES 
	   		('Particulier', p_prenom, p_nom, p_adresse, p_civilite);
	END
$$
;


-- -------------------------------------------------------------------------- --
-- Question n�2 :	Get_Id_From_Prenom_Nom_Adresse	 (version 1)
--
--		Cette routine retourne l'id d'un particulier � partir triplet 
--		(nom, pr�nom, adresse).
--
--		Dans cette version on suppose que le triplet fourni correspond � un 
--		particulier pr�sent dans la base.
--		Comme il s'agit de faire une recherche � partir de param�tres fournis 
--		dont on ne conna�t pas la casse a priori, il faut faire une mise en 
--		forme. 
--		Pour m�moire :
--			�	pr�nom en minuscules sauf la premi�re lettre du pr�nom 
--				(et celle des pr�noms compos�s)
--			�	nom en majuscules
--			�	adresse en majuscules
--
-- DROP FUNCTION IF EXISTS get_Id_From_Prenom_Nom_Adresse cascade;
--
CREATE OR REPLACE function get_Id_From_Prenom_Nom_Adresse
	(
		p_prenom   IN personne.prenom%TYPE,
		p_nom      IN personne.nom%TYPE,
		p_adresse  IN personne.adresse%TYPE
    )
	RETURNS personne.id%TYPE
LANGUAGE plpgsql
AS 
$$
	DECLARE
 		v_id personne.id%TYPE;
	BEGIN
		SELECT id INTO v_id
    	FROM personne
    	WHERE 
    		categorie ='Particulier' 	AND 
    		nom = upper(p_nom) 			AND 
    		adresse =  upper(p_adresse) AND 
    		prenom = initcap(p_prenom)
    	;
     RETURN v_id;
END
$$
;


-- -------------------------------------------------------------------------- --
--	Question n�3 : 	Get_Id_From_Prenom_Nom_Adresse (version 2)
--
--	Cette routine retourne l'id d'un particulier � partir triplet 
--	(nom, pr�nom, adresse).
--
--	Dans cette version, on suppose que le triplet fourni peut �tre quelconque. 
--	Une erreur (avec message explicite) doit donc �tre d�clench�e  si le 
--	triplet ne correspond � aucun particulier ou si l'un des arguments est NULL.
--	Comme dans la question pr�c�dente, on suppose que les param�tres effectifs 
--	peuvent �tre fournis dans une casse quelconque. 


CREATE OR REPLACE function get_Id_From_Prenom_Nom_Adresse
	(
		p_prenom   IN personne.prenom%TYPE,
		p_nom      IN personne.nom%TYPE,
		p_adresse  IN personne.adresse%TYPE
    )
	RETURNS personne.id%TYPE
LANGUAGE plpgsql
AS 
$$
	DECLARE
 		v_id personne.id%TYPE;
 	
	BEGIN
		IF p_prenom IS NULL THEN 
			RAISE EXCEPTION 'Le pr�nom ne peut �tre NULL'; 
		END IF;

		IF p_nom IS NULL THEN 
			RAISE EXCEPTION 'Le nom ne peut �tre NULL'; 
		END IF;

		IF p_adresse  IS NULL THEN 
			RAISE EXCEPTION 'L''adresse ne peut �tre NULL'; 
		END IF;
		
		/* le SELECT ci-dessous est STRICT => lev�e d'exception si aucune ligne 
		   n'est retourn�e (NO_DATA_FOUND) ou plusieurs lignes sont retourn�es (TOO_MANY_ROWS)
		   On aurait pu aussi 
		     1) utiliser un SELECT INTO "simple" 
			 2) tester ensuite la nullit� de la variable v_id
			 3) lever l'exception si v_id is null
			 */

		SELECT id INTO STRICT v_id
    	FROM personne
    	WHERE 
    		categorie ='Particulier' 	 AND 
    		nom       = upper(p_nom) 	 AND 
    		adresse   = upper(p_adresse) AND 
    		prenom    = initcap(p_prenom)
    	;

		RETURN v_id;

     	EXCEPTION
    		WHEN no_data_found THEN 
    			RAISE EXCEPTION 'Aucun particulier nomm� % % � %' , 
    				initcap(p_prenom), upper(p_nom), upper(p_adresse);
END
$$
;


-- -------------------------------------------------------------------------- --
--	Question n�4 : 	Particulier_Exists (version 1)
--
--	Particulier_Exists � partir du nom, du pr�nom et de l'adresse :
--
--	Cette routine retourne true si le triplet (nom, pr�nom, adresse) 
--	correspond � un particulier et false sinon
--	Si l'un des arguments est null, la routine retourne false.
--	L� encore la casse des param�tres effectifs peut �tre quelconque.
  
CREATE OR REPLACE FUNCTION Particulier_Exists 
	(
		p_prenom   IN personne.prenom%TYPE,
		p_nom      IN personne.nom%TYPE,
		p_adresse  IN personne.adresse%TYPE
	)
	RETURNS boolean
LANGUAGE plpgsql 
AS 
$$
	DECLARE
 		v_nb_particulier integer;
	BEGIN
		IF 	p_prenom  IS NULL	OR 
			p_nom     IS NULL 	OR 
			p_adresse IS NULL 
		THEN 
			RETURN FALSE; 
		END IF;

		SELECT count(id) INTO v_nb_particulier
	    FROM personne
	    WHERE 
	    	categorie 	= 'Particulier'		AND 
	    	nom     	= upper(p_nom)  	AND 
	    	adresse 	= upper(p_adresse) 	AND 
	     	prenom  	= initcap(p_prenom)
	     ;
	     RETURN (v_nb_particulier = 1);
	END
$$
;


-- -------------------------------------------------------------------------- --
--	Question n�5 : 	 Personne_Exists
--
--	Cette routine retourne true si l'id fourni en param�tre correspond � une 
--	Personne et false sinon ou si l'id est NULL
--
CREATE OR REPLACE FUNCTION Personne_Exists 
	(
		p_id   IN personne.id%TYPE
	)
	RETURNS boolean
LANGUAGE plpgsql 
AS 
$$
	DECLARE
 		v_nb_personnes integer;
	BEGIN
		IF p_id IS NULL THEN 
			RETURN FALSE; 
		END IF;

		SELECT count(id) INTO v_nb_personnes
    	FROM personne
    	WHERE id = p_id ;
     
    	RETURN v_nb_personnes = 1;
	END
$$
;

-- -------------------------------------------------------------------------- --
--	Question n�6 : 	 Particulier_Exists 
--
--	Version qui utilise get_Id_From_Prenom_Nom_Adresse et Personne_Exists
--
CREATE OR REPLACE FUNCTION Particulier_Exists 
	(
		p_prenom   IN personne.prenom%TYPE,
		p_nom      IN personne.nom%TYPE,
		p_adresse  IN personne.adresse%TYPE
	)
	RETURNS boolean 
LANGUAGE plpgsql
AS 
$$

	DECLARE
	 v_id integer;
	BEGIN
		IF 	p_prenom  IS NULL	OR 
			p_nom     IS NULL 	OR 
			p_adresse IS NULL 
		THEN 
			RETURN FALSE; 
		END IF;

	
     RETURN personne_exists
    		(
    			p_id => get_id_from_Prenom_Nom_Adresse
    						(   
								p_prenom,							
    							p_nom,
    							p_adresse 
    						)
    		);
	END
$$
;

-- -------------------------------------------------------------------------- --
--	Question n�7 : 	 Get_Nb_Comptes_From_Id
--
--	Il s'agit de retrouver le nombre de comptes de d�p�t (r�mun�r�s ou pas) 
--	que poss�de une personne (Particulier ou Entreprise) � partir de son Id
--	Si l'id n'existe pas, la routine retourne la valeur 0.
--
CREATE OR REPLACE FUNCTION Get_Nb_Comptes_From_Id 
	(
		p_id personne.id%TYPE 
	)
	RETURNS INTEGER 
LANGUAGE plpgsql
AS 
$$
	DECLARE
 		v_nb_comptes integer;
	BEGIN
		SELECT count(*) INTO v_nb_comptes
		FROM rattachement r2 
		WHERE r2.id_titulaire = p_id;
	
     	RETURN v_nb_comptes;
	END
$$
;

-- -------------------------------------------------------------------------- --
--	Question n�8 : 	Cr�ation d'une vue
--
--	Vue qui affiche la liste des particuliers (civilit�, pr�nom, nom) et 
--	leur nombre de comptes SANS UTILISATION EXPLICITE DE LA COLONNE (id)
--
CREATE OR REPLACE VIEW Info_Particuliers_vue
	(civilite, prenom, nom, nb_comptes)
	AS 
	SELECT 	civilite, 
			prenom, 
			nom,
			Get_Nb_Comptes_From_Id 
				(
					p_Id => Get_Id_From_Prenom_Nom_Adresse
								(
									p_prenom  => prenom, 
									p_nom     => nom, 
									p_adresse => adresse
								)
				)
	FROM Personne
	WHERE
		initcap(categorie) = 'Particulier'
;


-- -------------------------------------------------------------------------- --
--	Question n�9	Creer_Compte_From_Personne_ID
--
--	Il s'agit de cr�er un compte de d�p�t "simple" partir de l'id d'un 
--	futur titulaire.
--	Si l'id est NULL ou ne correspond � aucune personne, une exception avec 
--	un message explicite doit �tre lev�e.
--
--	La date de cr�ation du compte sera automatiquement la date du jour 
--	(type timestamp)
--	Le solde n'est pas fourni en param�tre. Par d�faut, il est �gal � 0.
--	Le compte est non bloqu� par d�faut
--	Le d�couvert autoris� est �gal � 0 par d�faut.
--	Le libell� est, par d�faut, constitu� du num�ro du compte, 
--	de la civilit� , du nom du titulaire et de son adresse
--
--	Pour cr�er un compte, il est n�cessaire de faire un insert dans la 
--	table COMPTE_DEPOT et de rattacher le compte � son titulaire par un 
--	INSERT dans la table RATTACHEMENT. 
--	Il faut donc r�cup�rer le num�ro du compte cr�� pour faire l'insertion 
--	dans la table RATTACHEMENT. 
--	Or le num�ro du compte est g�n�r� automatiquement. 
--	Pour le r�cup�rer, utilisez un ... 
--	INSERT INTO COMPTE_DEPOT� �RETURNING �INTO une_variable 
--	(cf. doc et exemples ICI) qui permet d'ins�rer une ligne dans la table 
--	des comptes pour r�cup�rer, apr�s l'INSERT, par exemple le num�ro du compte 
--	g�n�r� automatiquement
CREATE OR REPLACE PROCEDURE Creer_Compte_From_Personne_ID 
	(
		p_id IN personne.id%TYPE
	)
LANGUAGE plpgsql
	AS 
$$
	DECLARE 
  		v_numero_compte    	compte_depot.numero%TYPE;
  		v_civilite_personne	personne.civilite%TYPE;
  		v_nom_personne     	personne.nom%TYPE;
  		v_adresse_personne 	personne.adresse%TYPE;
	BEGIN
	
		IF NOT personne_exists(p_id) THEN 
	  		RAISE EXCEPTION 'L''id  % ne correspond � aucune personne', p_id; 
		END IF;

  	--	1)	R�cuperation du nom et de l'adresse du titulaire
   		SELECT 
   			civilite,
   			nom, 
   			adresse 
   		INTO 
   			v_civilite_personne,
   			v_nom_personne, 
   			v_adresse_personne
   		FROM personne 
   		WHERE id  = p_id;
  
  	--	2)	insert dans la table des comptes avec r�cup�ration du num�ro g�n�r�
  		INSERT INTO compte_depot 
  				(categorie, libelle)
            VALUES 
            	(
            		'Compte_Depot', 
            		v_civilite_personne|| ' ' || v_nom_personne || ' ' || v_adresse_personne
            	) 
       	RETURNING numero INTO v_numero_compte; 
      
  	--	3)	mise � jour du libell�
  	/**/	UPDATE compte_depot
  			SET libelle = v_numero_compte::TEXT || ' ' || libelle 
  		WHERE numero = v_numero_compte;
    
 	--	4)	insert dans la table des rattachements
 		INSERT INTO rattachement 
 				(id_compte , id_titulaire )
        	VALUES 
        		(v_numero_compte, p_id );
	
	END
$$
;

-- -------------------------------------------------------------------------- --
--	Question n�10 : 	Crediter_Compte (Version 1)
--
--	Il s'agit de cr�diter le compte dont le num�ro est fourni en param�tre 
--	du montant fourni en param�tre. 
--	Dans cette version, on suppose que le num�ro existe.

CREATE OR REPLACE PROCEDURE crediter_compte 
	(
		p_numero  compte_depot.numero%TYPE, 
        p_montant compte_depot.solde%TYPE 
	)
LANGUAGE plpgsql
AS 
$$

	BEGIN
		UPDATE compte_depot 
			SET solde = solde + p_montant
		WHERE numero = p_numero;
	END
$$
;


-- -------------------------------------------------------------------------- --
--	Question n�11 : 	Crediter_Compte (Version 2)
--
--	Il s'agit de d�clencher une exception lorsque le num�ro de compte 
--	n'existe pas.
--	Quand le num�ro n'existe pas la clause WHERE de l'UPDATE est fausse et 
--	donc l'UPDATE ne fait rien mais ne plante pas. 
--	Le nombre de lignes modifi�es est donc �gal � 0.
--	Il est possible de connaitre le nombre de lignes trait�es par la derni�re 
--	commande SQL (voir la documentation ICI).
--
CREATE OR REPLACE PROCEDURE crediter_compte 
	(
		p_numero  IN compte_depot.numero%TYPE, 
        p_montant IN compte_depot.solde%TYPE 
	)
LANGUAGE plpgsql
AS 
$$
	DECLARE
		v_row_count bigint;  --cf tableau 42.1 de la doc

	BEGIN
		
	    IF p_montant < 0 THEN 
	   		RAISE EXCEPTION 'Le montant de % � � cr�diter doit �tre positif', p_montant;
	   	END IF;
		
		UPDATE compte_depot 
			SET solde = solde + p_montant
		WHERE numero = p_numero;

		GET DIAGNOSTICS v_row_count = ROW_COUNT;
   
	    IF v_row_count = 0 THEN 
	   		RAISE EXCEPTION 'Impossible de cr�diter le compte num�ro : % car il n''existe pas', p_numero;
	    END IF;
	END
$$
;
--
-- -------------------------------------------------------------------------- --
--	Question n�12 : 	Debiter_Compte 
--
--	Il s'agit de d�biter le compte dont le num�ro est fourni en param�tre,
--	d'un montant fourni en param�tre. 
--	Si le num�ro de compte n'existe pas, il faut lever une exception 
--	avec un message clair sur l'erreur du type 
--	"Impossible de d�biter le compte num�ro : 123456 car il n'existe pas".
--	On ne v�rifie pas que le solde du compte est suffisant. Cela sera fait dans la prochaine s�ance avec des d�clencheurs.

CREATE OR REPLACE PROCEDURE debiter_compte 
	(
		p_numero  IN compte_depot.numero%TYPE, 
        p_montant IN compte_depot.solde%TYPE 
    )
LANGUAGE plpgsql
AS 
$$
	DECLARE
		v_row_count bigint;  --cf tableau 42.1 de la doc
	BEGIN

	    IF p_montant < 0 THEN 
	   		RAISE EXCEPTION 'Le montant de % � � d�biter doit �tre positif', p_montant;
	   	END IF;
		
		UPDATE compte_depot 
			SET solde = solde - p_montant
		WHERE numero = p_numero;
    
		GET DIAGNOSTICS v_row_count = ROW_COUNT;
   
    	IF v_row_count = 0 THEN 
    		RAISE EXCEPTION 'Impossible de d�biter le compte num�ro % car il n''existe pas', p_numero;
    	END IF;
	END
$$
;

-------------------------------------------------------------------------- --
--	Question n�13 : 	 Compte_Exists
--
--	Cette routine retourne true si le numero fourni en param�tre correspond � un 
--	compte et false sinon ou si le numero est NULL
--
--
CREATE OR REPLACE FUNCTION Compte_Exists 
	(
		p_numero   IN compte_depot.numero%TYPE
	)
	RETURNS boolean
LANGUAGE plpgsql 
AS 
$$
	DECLARE
 		v_nb_comptes integer;
	BEGIN
		IF p_numero IS NULL THEN 
			RETURN FALSE; 
		END IF;

		SELECT count(numero) INTO v_nb_comptes
    	FROM compte_depot
    	WHERE numero  = p_numero ;   --ou v_nb_comptes := count(numero) FROM compte_depot WHERE numero  = p_numero ; 
     
    	RETURN v_nb_comptes = 1;
	END
$$
;
--
-- -------------------------------------------------------------------------- --
--	Question n�14 : 	Faire_Virement
--
--	Il s'agit de faire un virement d'un certain montant entre 2 comptes diff�rents. 
--	Si le compte � cr�diter est celui � d�biter, levez une exception avec un 
--	message clair du type : "Impossible de faire un autovirement sur le compte num�ro : 1".
--	On ne v�rifie pas que le solde du compte d�biteur est suffisant. 
--	Cela sera fait dans la prochaine s�ance avec des d�clencheurs.
--
CREATE OR REPLACE PROCEDURE Faire_Virement
	(
		p_NoCompteDebiteur	compte_depot.numero%TYPE,
		p_NoCompteCrediteur	compte_depot.numero%TYPE,
		p_montant			compte_depot.solde%TYPE
	)
LANGUAGE plpgsql
	AS
$$
	DECLARE 
	
	BEGIN 
		--	1)	le montant doit �tre d�fini et doit �tre positif
	    IF p_montant IS NULL THEN 
	   		RAISE EXCEPTION 'Le montant � virer doit �tre d�fini';
	   	ELSIF p_montant < 0 THEN 
	   		RAISE EXCEPTION 'Le montant de % � � virer doit �tre positif', p_montant;
	   	END IF;
		
	   	--	2) 	v�rification de l'existence du compte d�biteur
		IF not compte_exists(p_numero => p_NoCompteDebiteur)
		  THEN RAISE EXCEPTION 'Impossible de d�biter le compte num�ro % car il n''existe pas', p_NoCompteDebiteur;
		END IF;
		
		--	3) 	v�rification de l'existence du compte cr�diteur
		IF not compte_exists(p_numero => p_NoCompteCrediteur)
		  THEN RAISE EXCEPTION 'Impossible de cr�diter le compte num�ro % car il n''existe pas', p_NoCompteCrediteur;
		END IF;
		
    	--	4)	Les comptes d�biteur et cr�diteur doivent �tre distincts
	    IF p_NoCompteDebiteur = p_NoCompteCrediteur THEN 
	   		RAISE EXCEPTION 'Les comptes d�biteur et cr�diteur (n� %) doivent �tre distincts', p_NoCompteDebiteur;
	   	END IF;
    	
		--	5)	d�biter le compte d�biteur
		CALL debiter_compte 
			(
				p_NoCompteDebiteur, 
        		p_montant
    		)
		;
		
		--	6)	cr�diter le compte cr�diteur
		CALL Crediter_compte 
			(
				p_NoCompteCrediteur, 
        		p_montant
    		)
    	;

	END
$$
;
/* ***********************************************************************************
  ROUTINES COMPLEMENTAIRES
  
  */
--
-- -------------------------------------------------------------------------- --
--	Bloquer_Compte
--
--	Il s'agit de bloquer un compte � partir de son num�ro
--  si le num�ro ne correspond pas � un compte, une exception est lev�e
CREATE OR REPLACE PROCEDURE bloquer_compte 
	(
		p_numero  IN compte_depot.numero%TYPE
    )
LANGUAGE plpgsql
AS 
$$
	DECLARE
		v_row_count bigint;  --cf tableau 42.1 de la doc
	BEGIN
	   		
		UPDATE compte_depot 
			SET bloque = TRUE
		WHERE numero = p_numero;
    
		GET DIAGNOSTICS v_row_count = ROW_COUNT;
   
    	IF v_row_count = 0 THEN 
    		RAISE EXCEPTION 'Impossible de bloquer le compte num�ro % car il n''existe pas', p_numero;
    	END IF;
	END
$$
;

-- -------------------------------------------------------------------------- --
--	Debloquer_Compte
--
--	Il s'agit de bloquer un compte � partir de son num�ro
--  si le num�ro ne correspond pas � un compte, une exception est lev�e
CREATE OR REPLACE PROCEDURE debloquer_compte 
	(
		p_numero  IN compte_depot.numero%TYPE
    )
LANGUAGE plpgsql
AS 
$$
	DECLARE
		v_row_count bigint;  --cf tableau 42.1 de la doc
	BEGIN
	   		
		UPDATE compte_depot 
			SET bloque = FALSE
		WHERE numero = p_numero;
    
		GET DIAGNOSTICS v_row_count = ROW_COUNT;
   
    	IF v_row_count = 0 THEN 
    		RAISE EXCEPTION 'Impossible de d�bloquer le compte num�ro % car il n''existe pas', p_numero;
    	END IF;
	END
$$
;
-- ----------------------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE creer_entreprise
		(
			p_nom           IN personne.nom%TYPE,
            p_adresse       IN personne.adresse%TYPE,
			p_siren         IN personne.siren%TYPE,
			p_id_dirigeant  IN personne.dirigeant%TYPE
		)
LANGUAGE plpgsql
AS 
$$
	BEGIN
		INSERT INTO personne 
			(categorie, nom, adresse, siren, dirigeant)
	    VALUES 
	   		('Entreprise', p_nom, p_adresse, p_siren, p_id_dirigeant);
	END
$$
;

-----------------------------------------------------------------------------------------------

 CREATE OR REPLACE PROCEDURE creer_carte(p_id_titulaire      IN personne.id%TYPE,
                                         p_id_compte         IN compte_depot.numero%TYPE,
                                         p_type_carte_statut IN type_carte.statut%TYPE,
                                         p_code_secret       IN carte_bancaire.code_secret%type DEFAULT 1234)  
 LANGUAGE plpgsql
 AS $$
 DECLARE 
 v_id_type_carte type_carte.id%TYPE;
BEGIN 
	
	--recup�ration de l'id du type carte
	SELECT tc.id INTO v_id_type_carte 
	FROM type_carte tc 
	WHERE tc.statut = initcap(p_type_carte_statut);
   
    IF v_id_type_carte IS NULL THEN
       RAISE EXCEPTION 'Le type carte : %, n''existe pas', p_type_carte_statut;
    END IF;
   
    --la date d'expiration est post�rieure de 2 ans � la date de cr�ation de la carte
    INSERT INTO carte_bancaire (id_compte, 
                                id_titulaire, 
                                id_type_carte ,
                                code_secret ,
                                nb_essais ,
                                bloquee ,
                                date_expiration )
                       VALUES (p_id_compte,
                               p_id_titulaire,
                               v_id_type_carte, 
                               p_code_secret,
                               0,
                               FALSE, 
                               current_date + INTERVAL '2 years');
	
	NULL;
END;
$$;
---------------------------------------------------------------------
-- la fonction Random_Between retourne un nombre al�atoire 
-- compris entre 2 bornes enti�res

CREATE OR REPLACE FUNCTION Random_Between(low INTEGER, high INTEGER)
    RETURNS INT
    LANGUAGE plpgsql
    STRICT
       AS
$$
    BEGIN
           RETURN floor(random() * (high - low + 1) + low);
    END
$$
;