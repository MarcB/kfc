package popschool.projet_tp1_client_api_rest.test;

import org.springframework.boot.CommandLineRunner;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import popschool.projet_tp1_client_api_rest.model.Employee;

@Component
public class EmployeeTest implements CommandLineRunner {

    static final String URL_EMPLOYEES = "http://localhost:8081/employees";
    static final String URL_UPDATE_EMPLOYEE = "http://localhost:8081/employees";
    static final String URL_EMPLOYEE_PREFIX = "http://localhost:8081/employees";
    static final String URL_CREATE_EMPLOYEE = "http://localhost:8081/employees";

    @Override
    public void run(String... args) throws Exception {
        System.out.println("_________________________________lecture__________________________________");
        RestTemplate restTemplate = new RestTemplate();

        // Send request with GET method and default Headers.
        Employee[] list = restTemplate.getForObject(URL_EMPLOYEES, Employee[].class);

        if (list != null) {
            for (Employee em2 : list) {
                System.out.println("Employee: " + em2.getEmpNo() + " - " + em2.getEmpName()
                        +" - "+ em2.getPosition());
            }
        }

        System.out.println("_________________________________new__________________________________");

        Employee newEmployee = new Employee("E04","Tom", "Cleck");

        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);

        RestTemplate restTemplate3 = new RestTemplate();

        // Data attached to the request.
        HttpEntity<Employee> requestBody = new HttpEntity<>(newEmployee, headers);

        // Send request with POST method.
        Employee e = restTemplate3.postForObject(URL_CREATE_EMPLOYEE, requestBody, Employee.class);

        if (e != null && e.getEmpNo() != null) {
            System.out.println("Employee created: " + e.getEmpNo());
        } else {
            System.out.println("Something error!");
        }

       /* String empNo = "E04";
        Employee updateInfo = new Employee("E04","Tom", "Cleck");

        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);

        RestTemplate restTemplate1 = new RestTemplate();

        // Data attached to the request.
        HttpEntity<Employee> requestBody = new HttpEntity<>(updateInfo, headers);

        // Send request with PUT method.
        restTemplate1.put(URL_UPDATE_EMPLOYEE + "/" + empNo, requestBody,
                new Object[]{});

        String resourceUrl = URL_EMPLOYEE_PREFIX + "/" + empNo;

        Employee em = restTemplate1.getForObject(resourceUrl, Employee.class);

        if (em != null) {
            System.out.println("(Client side) Employee after update: ");
            System.out.println("Employee: " + em.getEmpNo() + " - " + em.getEmpName());
        }*/

        System.out.println("_________________________________lecture__________________________________");
        RestTemplate restTemplate2 = new RestTemplate();

        // Send request with GET method and default Headers.
        Employee[] list2 = restTemplate2.getForObject(URL_EMPLOYEES, Employee[].class);

        if (list != null) {
            for (Employee em1 : list2) {
                System.out.println("Employee: " + em1.getEmpNo() + " - " + em1.getEmpName()
                        +" - "+ em1.getPosition());
            }
        }


    }
}
