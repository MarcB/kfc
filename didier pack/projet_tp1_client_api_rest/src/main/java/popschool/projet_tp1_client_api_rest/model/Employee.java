package popschool.projet_tp1_client_api_rest.model;

import lombok.Data;

@Data
public class Employee {

    private String empNo;
    private String empName;
    private String position;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                            CONSTRUCTEUR
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public Employee() {
    }

    public Employee(String empNo, String empName, String position) {
        this.empNo = empNo;
        this.empName = empName;
        this.position = position;
    }

}
