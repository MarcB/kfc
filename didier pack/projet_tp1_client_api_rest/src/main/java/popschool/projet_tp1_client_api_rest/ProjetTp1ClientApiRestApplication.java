package popschool.projet_tp1_client_api_rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetTp1ClientApiRestApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjetTp1ClientApiRestApplication.class, args);
    }

}
