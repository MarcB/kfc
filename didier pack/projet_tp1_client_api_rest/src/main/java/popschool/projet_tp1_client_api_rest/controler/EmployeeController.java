package popschool.projet_tp1_client_api_rest.controler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import popschool.projet_tp1_client_api_rest.model.Employee;
import popschool.projet_tp1_client_api_rest.service.EmployeeService;

import java.util.Iterator;

@Controller
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                          INITIALISATION DE LA PAGE INDEX
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @GetMapping(value =  {"/", "/index"})
    public String index(Model model){
        Employee[] employees = employeeService.findAllEmployee();
        Employee newEmployee = new Employee();
        model.addAttribute("employees", employees);
        model.addAttribute("newEmployee", newEmployee);
        return "index";
    }

    @PostMapping("/addEmployee")
    public String addEmployee(@ModelAttribute("newEmployee") Employee newEmployee,Model model){
        employeeService.createEmployee(newEmployee);
        return"redirect:/index";
    }

    @GetMapping(value = "/update/{descriptif}")
    public String deleteProduit(@PathVariable("descriptif") String descriptif, Model model){
        Employee emp = employeeService.findEmployeeById(descriptif);
        model.addAttribute("emp",emp);
        return "pageUpdate";
    }

    @PostMapping("/modification")
    public String mofificationEmployee(@ModelAttribute("emp") Employee emp, Model model) {
        System.out.println("-------------------------------empNo---------------------------------");
        System.out.println(emp.getEmpNo());
        employeeService.updateEmployee(emp);
        return "redirect:/index";
    }
}
