package popschool.projet_tp1_client_api_rest.service;

import popschool.projet_tp1_client_api_rest.model.Employee;

import java.util.*;

public interface EmployeeService {

    public Employee[] findAllEmployee();

    public Employee findEmployeeById(String empNo);

    public void createEmployee(Employee employee);

    public void updateEmployee(Employee employee);
}
