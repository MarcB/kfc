package popschool.projet_tp1_client_api_rest.service;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.thymeleaf.expression.Arrays;
import popschool.projet_tp1_client_api_rest.model.Employee;

import java.util.List;

@Service
public class EmployeeServiceRepository implements EmployeeService{

    @Override
    public Employee[] findAllEmployee() {
        String URL_EMPLOYEES = "http://localhost:8081/employees";

        RestTemplate restTemplate = new RestTemplate();

        // Send request with GET method and default Headers.
        Employee[] list = restTemplate.getForObject(URL_EMPLOYEES, Employee[].class);

        return list;
    }

    @Override
    public Employee findEmployeeById(String empNo) {
        String URL_EMPLOYEES = "http://localhost:8081/employees/" + empNo;

        RestTemplate restTemplate = new RestTemplate();

        // Send request with GET method and default Headers.
        Employee emp = restTemplate.getForObject(URL_EMPLOYEES, Employee.class);

        return emp;
    }

    @Override
    public void createEmployee(Employee newEmployee) {

        String URL_CREATE_EMPLOYEE = "http://localhost:8081/employees";


        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);

        RestTemplate restTemplate3 = new RestTemplate();

        // Data attached to the request.
        HttpEntity<Employee> requestBody = new HttpEntity<>(newEmployee, headers);

        // Send request with POST method.
        Employee e = restTemplate3.postForObject(URL_CREATE_EMPLOYEE, requestBody, Employee.class);

        if (e != null && e.getEmpNo() != null) {
            System.out.println("Employee created: " + e.getEmpNo());
        } else {
            System.out.println("Something error!");
        }
    }

    @Override
    public void updateEmployee(Employee employee) {

        final String URL_UPDATE_EMPLOYEE = "http://localhost:8081/employees";
        final String URL_EMPLOYEE_PREFIX = "http://localhost:8081/employees";

        System.out.println("-------------------------empNo dans requete-------------------");
        System.out.println(employee.getEmpNo());
            String empNo = employee.getEmpNo();

        Employee updateInfo = employee;
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        RestTemplate restTemplate = new RestTemplate();
        // Data attached to the request.
        HttpEntity<Employee> requestBody = new HttpEntity<>(updateInfo, headers);
        // Send request with PUT method
        restTemplate.put(URL_UPDATE_EMPLOYEE + "/" + empNo, requestBody,
        new Object[]{});
        String resourceUrl = URL_EMPLOYEE_PREFIX + "/" + empNo;
        Employee e = restTemplate.getForObject(resourceUrl, Employee.class);
        if (e != null) {
            System.out.println("(Client side) Employee after update: ");
            System.out.println("Employee: " + e.getEmpNo() + " - " + e.getEmpName());
        }

    }

}
