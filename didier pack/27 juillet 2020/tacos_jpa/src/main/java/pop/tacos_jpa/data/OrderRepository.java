package pop.tacos_jpa.data;

import org.springframework.data.repository.CrudRepository;

import pop.tacos_jpa.Order;

public interface OrderRepository 
         extends CrudRepository<Order, Long> {

}
