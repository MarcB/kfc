
create table  if not exists organisation(
  orgid   		int(4) auto_increment, 
  orgname      varchar (30)
                not null
                , 
  description   varchar (30), 
  theboss       integer, 
  constraint uk_organisation_name unique(orgname),
  constraint pk_organisation
     primary key ( orgid ) 
 ) engine InnoDB; 
  
create table  if not exists gangster ( 
  gangsterid  int(4) auto_increment  , 
  gname        varchar (30) 
               not null
               , 
  nickname     varchar (30), 
  badness      integer,
  orgid       integer,
  constraint fk_gangster_orgname foreign key (orgid) references organisation(orgid) on delete set null,
  constraint uk_gansgter_name unique(gname),
  constraint pk_gangster
    primary key ( gangsterid )
 ) engine InnoDB; 


create table if not exists job ( 
  jobid	   int(4) auto_increment ,
  jobname     varchar (30) not null , 
  score        integer, 
  setupcost    float, 
  constraint uk_jobname unique(jobname),
  constraint pk_job
    primary key ( jobid ) 
) engine InnoDB ;
  
create table if not exists affectation (
  affectid     int(4) auto_increment,
  gangsterid   integer  not null,
  jobid        integer  not null,
  constraint fk_affectation_jobid foreign key (jobid) references job(jobid) on delete cascade,
  constraint fk_affectation_gangsterid foreign key(gangsterid) references gangster(gangsterid) on delete cascade,
  constraint pk_affectation
     primary key ( affectid ) ,
  constraint uk_affectation
     unique (gangsterid, jobid)
     ) ; 
	 
alter table organisation
   add constraint fk_organisation_boss foreign key(theboss) references gangster(gangsterid)
                             on delete set null;	  
 

insert into organisation
            ( orgname, description, theboss)
     values ( 'yakuza', 'japanese gangsters', null);
insert into organisation
            ( orgname, description, theboss)
     values ( 'mafia', 'italian bad guys', null);
insert into organisation
            ( orgname, description, theboss)
     values ( 'triads', 'kung fu movie extras', null);

insert into gangster
            ( gname, nickname, badness, orgid)
     values ('yojimbo', 'bodyguard', 7, (select orgid from organisation where orgname='yakuza'));

insert into gangster
            ( gname, nickname, badness, orgid)
     values ( 'takeshi', 'master', 10, (select orgid from organisation where orgname='yakuza'));

insert into gangster
            (gname, nickname, badness, orgid)
     values ( 'yuriko', 'four finger', 4, (select orgid from organisation where orgname='yakuza'));

insert into gangster
            ( gname, nickname, badness, orgid)
     values ( 'chow', 'killer', 9, (select orgid from organisation where orgname='triads'));

insert into gangster
            ( gname, nickname, badness, orgid)
     values ( 'shogi', 'lightning', 8, (select orgid from organisation where orgname='triads'));

insert into gangster
            ( gname, nickname, badness, orgid)
     values ('valentino', 'pizza-face', 4, (select orgid from organisation where orgname='mafia'));

insert into gangster
            ( gname, nickname, badness, orgid)
     values ('toni', 'toohless', 2, (select orgid from organisation where orgname='mafia'));

insert into gangster
            ( gname, nickname, badness, orgid)
     values ('corleone', 'godfather', 6, (select orgid from organisation where orgname='mafia'));

update organisation
   set theboss = (select gangsterid from gangster where gname = 'takeshi')
 where orgname = 'yakuza';
update organisation
   set theboss = (select gangsterid from gangster where gname = 'chow')
 where orgname = 'triads';
update organisation
   set theboss = (select gangsterid from gangster where gname = 'corleone')
 where orgname = 'mafia';

insert into job
            ( jobname, score, setupcost)
     values ( '10th street jeweler heist', 5000, 50);

insert into job
            ( jobname, score, setupcost)
     values ( 'the great train robbery', 2000000, 500000);

insert into job
            ( jobname, score, setupcost)
     values ('cheap liquor snatch and grab', 50, 0);


insert into affectation
            ( gangsterid, jobid)
     values (
             (select gangsterid from gangster where gname='valentino'),
             (select jobid from job where jobname='10th street jeweler heist'));

insert into affectation
            ( gangsterid, jobid)
     values (
             (select gangsterid from gangster where gname='corleone'),
             (select jobid from job where jobname='10th street jeweler heist') );

insert into affectation
            ( gangsterid, jobid)
     values (
             (select gangsterid from gangster where gname='chow'),
             (select jobid from job where jobname='cheap liquor snatch and grab'));

insert into affectation
            ( gangsterid, jobid)
     values (
             (select gangsterid from gangster where gname='chow'),
             (select jobid from job where jobname='the great train robbery'));

insert into affectation
            ( gangsterid, jobid)
     values (
             (select gangsterid from gangster where gname='yojimbo'), 
             (select jobid from job where jobname='the great train robbery'));



