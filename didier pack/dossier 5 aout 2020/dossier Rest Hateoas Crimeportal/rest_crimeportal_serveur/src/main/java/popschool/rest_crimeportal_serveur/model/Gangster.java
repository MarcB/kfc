package popschool.rest_crimeportal_serveur.model;

import javax.persistence.*;

@Entity
public class Gangster {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer gangsterid;

    private String gname;

    private String nickname;
    private Integer badness;

    @ManyToOne
    @JoinColumn(name = "orgid")
    private Organisation organisation;



    public Integer getGangsterid() {
        return gangsterid;
    }

    public void setGangsterid(Integer gangsterid) {
        this.gangsterid = gangsterid;
    }

    public String getGname() {
        return gname;
    }

    public void setGname(String gname) {
        this.gname = gname;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Integer getBadness() {
        return badness;
    }

    public void setBadness(Integer badness) {
        this.badness = badness;
    }

    public Organisation getOrganisation() {
        return organisation;
    }

    public void setOrganisation(Organisation organisation) {
        this.organisation = organisation;
    }


}

