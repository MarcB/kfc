package popschool.rest_crimeportal_serveur.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Organisation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer orgid;

    private String orgname;
    private String description;

    @OneToOne
    @JoinColumn(name="theboss" )
    private Gangster theboss;



    public Integer getOrgid() {
        return orgid;
    }

    public void setOrgid(Integer orgid) {
        this.orgid = orgid;
    }

    public String getOrgname() {
        return orgname;
    }

    public void setOrgname(String orgname) {
        this.orgname = orgname;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Gangster getTheboss() {
        return theboss;
    }

    public void setTheboss(Gangster theboss) {
        this.theboss = theboss;
    }


}
