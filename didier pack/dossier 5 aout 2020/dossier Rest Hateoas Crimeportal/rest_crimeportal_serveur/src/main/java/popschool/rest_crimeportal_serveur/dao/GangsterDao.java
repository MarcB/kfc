package popschool.rest_crimeportal_serveur.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import popschool.rest_crimeportal_serveur.model.Gangster;

import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "gangsters", path = "gangsters")
public interface GangsterDao extends JpaRepository<Gangster,Integer> {
    Optional<Gangster>  findByGname(String gname);
}
