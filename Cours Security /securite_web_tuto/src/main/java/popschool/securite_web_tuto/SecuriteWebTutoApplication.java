package popschool.securite_web_tuto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecuriteWebTutoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecuriteWebTutoApplication.class, args);
    }

}
