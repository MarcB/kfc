package modele;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "albums", schema = "myTintin", catalog = "")
public class AlbumsEntity {
    private int id;
    private String titre;
    private int annee;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "titre")
    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    @Basic
    @Column(name = "annee")
    public int getAnnee() {
        return annee;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AlbumsEntity that = (AlbumsEntity) o;
        return id == that.id &&
                annee == that.annee &&
                Objects.equals(titre, that.titre);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, titre, annee);
    }
}
