package dao;

import dto.FilmDTO;
import dto.GenreDTO;
import service.ConnectCinema;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CinemaDAO {

    // Affiche ngenre et nature de tous les genres
    private final static String SQLfindAllGenres =
            " SELECT ngenre, nature " +
                    " FROM GENRE " +
                    " ORDER BY nature ASC ";

    private final static String SQLfindGenreById =
            " SELECT ngenre, nature " +
                    " FROM GENRE " +
                    " WHERE ngenre = ? ";

    // Affiche le nombre de films d’un genre spécifié par son ngenre
    private final static String SQLfindNbFilmsByGenre =
            " SELECT COUNT(*) as nbre " +
                    " FROM FILM " +
                    " WHERE ngenre =  ?";

    // Affiche nfilm, titre, nom réalisateur, nom acteur des films d’un genre spécifié par son ngenre
    private final static String SQLfindAllFilmByGenre =
            " SELECT nfilm, titre, realisateur as nom_realisateur, a.nom as nom_acteur " +
                    " FROM FILM f " +
                    " INNER JOIN ACTEUR a	ON f.nacteurPrincipal = a.nacteur" +
                    " WHERE ngenre = ?" +
                    " ORDER BY titre";

    // Affiche nfilm, titre, nom réalisateur, nom acteur d’un film spécifié par son nfilm
    private final static String SQLfindFilmById =
            " SELECT f.nfilm, f.titre, f.realisateur as nom_realisateur, a.nom as nom_acteur" +
                    " FROM ACTEUR a" +
                    " INNER JOIN FILM f ON a.nacteur = f.nacteurPrincipal" +
                    " WHERE f.nfilm = ?";


    public List<GenreDTO> ensGenres() {
        List<GenreDTO> liste = new ArrayList<>();
        try{
            Statement instruction = ConnectCinema.getINSTANCE().createStatement();
            ResultSet rs = instruction.executeQuery(SQLfindAllGenres);

            while( rs.next() ){
                int ngenre = rs.getInt(1);
                String nature = rs.getString("NATURE");

                liste.add( new GenreDTO(ngenre, nature));

            }
        } catch (SQLException e){
            e.printStackTrace();
        }
        return liste;
    }


    public long nbreFilmsDuGenre(int unGenre) {

        try{
            Connection conn = ConnectCinema.getINSTANCE();
            PreparedStatement instruction = conn.prepareStatement(SQLfindNbFilmsByGenre);
            instruction.setInt(1,unGenre);
            ResultSet rs = instruction.executeQuery();
            if(rs.next()){
                return rs.getLong("nbre");
            }

        } catch (SQLException e){
            e.printStackTrace();
        }
        return -1L;
    }


    public List<FilmDTO> ensFilmsDuGenre(int unFilm) {
        List<FilmDTO> liste = new ArrayList<>();
        try{
            Connection conn = ConnectCinema.getINSTANCE();
            PreparedStatement instruction = conn.prepareStatement(SQLfindAllFilmByGenre);
            instruction.setInt(1,unFilm);
            ResultSet rs = instruction.executeQuery();
            // nom film    titre   real    acteur nom
            while( rs .next() ){
                int nFilm = rs.getInt("nfilm");
                String titre = rs.getString("titre");
                String nomReal = rs.getString("nom_realisateur");
                String nomActeur = rs.getString("nom_acteur");
                liste.add( new FilmDTO(nFilm, titre, nomReal, nomActeur));
            }
        } catch (SQLException e){
            e.printStackTrace();
        }
        return liste;
    }

    public FilmDTO filmParId(int unFilm) {
        FilmDTO f = null;
        try{
            Connection conn = ConnectCinema.getINSTANCE();
            PreparedStatement instruction = conn.prepareStatement(SQLfindFilmById);
            instruction.setInt(1,unFilm);
            ResultSet rs = instruction.executeQuery();

            if(rs.next()){
                int nFilm = rs.getInt("nfilm");
                String titre = rs.getString("titre");
                String nomReal = rs.getString("nom_realisateur");
                String nomActeur = rs.getString("nom_acteur");

                f = new FilmDTO(nFilm,titre,nomReal,nomActeur);
            }
        } catch (SQLException e){
            e.printStackTrace();
        }
        return f;
    }

    public GenreDTO findGenresById(int ngenre) {
        GenreDTO g = null;
        try{
            Connection conn = ConnectCinema.getINSTANCE();
            PreparedStatement instruction = conn.prepareStatement(SQLfindGenreById);
            instruction.setInt(1, ngenre);
            ResultSet rs = instruction.executeQuery();

            if(rs.next()){
                int nGenre = rs.getInt(1);
                String nature = rs.getString(2);

                g = new GenreDTO(nGenre,nature);
            }
        } catch (SQLException e){
            e.printStackTrace();
        }
        return g;
    }

    // SINGLETON PRECOCE
    private static CinemaDAO INSTANCE = new CinemaDAO();

    public static CinemaDAO getInstance() {return INSTANCE;}

    public static void main(String[] args) {
        CinemaDAO dao = new CinemaDAO();
        try{
            for(GenreDTO g : dao.ensGenres()) {
                System.out.println(g.getNature());
            }

            System.out.println(dao.nbreFilmsDuGenre(1));
            for(FilmDTO f : dao.ensFilmsDuGenre(1)) {
                System.out.println(f.getTitre());
            }

            System.out.println(dao.filmParId(34));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
