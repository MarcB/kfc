<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Films</title>
</head>
<body>
Films du genre ${genre.nature}
<br /><br />

<form action = "process?action=choix" method="post">
    <table>
        <thead>
        <tr>
            <th>Titre</th>
            <th>Realisateur</th>
            <th>Acteur Principal</th>
            <th>Choix</th>
        </tr>
        </thead>
    <tbody>
        <c:forEach var="film" items="${ensFilms}">
            <tr>
                <td>${film.titre}</td>
                <td>${film.nom_realisateur}</td>
                <td>${film.nom_acteur}</td>
                <td><input type="checkbox" name="choix" value="${film.nfilm}" /></td>
            </tr>
        </c:forEach>
    </tbody>
        <input type="submit" value="Ajouter au panier">
    </table>
</form>

</body>
</html>
