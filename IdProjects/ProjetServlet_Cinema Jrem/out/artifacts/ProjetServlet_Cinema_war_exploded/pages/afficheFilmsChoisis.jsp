<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: jeremiepop
  Date: 24/02/2020
  Time: 15:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h1>Films choisis</h1>
<ul>
<c:forEach var="choix" items="${ensChoix}">
    <li>
    ${choix.value.titre} de : ${choix.value.nom_realisateur}
    </li>
</c:forEach>
    <a href="process?action=init"> <input type="button" value="Retour à l'accueil"> </a>
</ul>
</body>
</html>
