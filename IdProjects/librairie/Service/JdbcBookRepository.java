package popschool.librairie.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import popschool.librairie.DAO.BookRepository;
import popschool.librairie.modele.Book;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Repository
public class JdbcBookRepository implements BookRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public int count() {
        int nb = jdbcTemplate.queryForObject("select count(*) from BOOKS b",Integer.class);
        return nb;
    }

    @Override
    public int save(Book book) {
        return jdbcTemplate.update("insert into BOOKS(name,price) values( ?, ?)",book.getName(),book.getPrice());
    }

    @Override
    public int update(Book book) {
        return 0;
    }

    @Override
    public int deleteById(Long id) {
        return 0;
    }

    @Override
    public List<Book> findAll() {
        List<Book> list = jdbcTemplate.query("select b.name, b.price from BOOK b",
                (rs,rowNum) -> {return new Book(rs.getString("name"),rs.getBigDecimal("price"));});
        return list;
    }

    @Override
    public List<Book> findByNameAndPrice(String name, BigDecimal price) {
        return null;
    }

    @Override
    public Optional<Book> findById(Long id) {
        return Optional.empty();
    }

    @Override
    public String getNameById(Long id) {
        return null;
    }
}
