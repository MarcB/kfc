package popschool.librairie.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;
import popschool.librairie.DAO.BookRepository;
import popschool.librairie.modele.Book;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

@Component
public class SpringJdbcTutoApplication implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(SpringJdbcTutoApplication.class);

    @Autowired
    private BookRepository bookRepository;

    @Override
    public void run(String... args) throws Exception {
        runJDBC();
    }

    private void runJDBC(){
        List<Book> books = Arrays.asList(
                new Book("Thinking in Java",new BigDecimal(46.32)),
                new Book("Mkyong in Java",new BigDecimal(1.99)),
                new Book("Getting Clojure",new BigDecimal(37.3)),
                new Book("Head First Android Developpement",new BigDecimal(41.19)));
        log.info("[SAVE]");
        books.forEach(book -> {log.info("Saving ..{}",book.getName());bookRepository.save(book);});
    }
}
