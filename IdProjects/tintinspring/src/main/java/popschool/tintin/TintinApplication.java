package popschool.tintin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;
import popschool.tintin.dao.AlbumDAO;
import popschool.tintin.dao.JdbcAlbumRepository;
import popschool.tintin.model.Album;

import java.math.BigDecimal;

@SpringBootApplication

public class TintinApplication implements CommandLineRunner {
    private static final Logger log = LoggerFactory.getLogger(TintinApplication.class);
    public static void main(String[] args) {
        SpringApplication.run(TintinApplication.class, args);
    }


    @Autowired
    //JdbcTemplate jdbcTemplate;
   // @Qualifier("jdbcAlbumRepository")              // Test JdbcTemplate
    @Qualifier("namedParameterJdbcAlbumRepository")  // Test NamedParameterJdbcTemplate
    private AlbumDAO albumRepository;

    @Override
    public void run(String... args) throws Exception {
        log.info("StartApplication...");
        runTintin();
    }

    private void runTintin() {
    // find all
        log.info("\n[FIND_ALL] {}", albumRepository.listAlbums());
    //findbyid
        log.info("\n [FIND_BY_ID] :3");
        Album album = albumRepository.findById(3).orElseThrow(IllegalArgumentException::new);
        log.info("{}", album);
    // find by titre
        log.info("\n[FIND_BY_titre] : ");
        log.info("{}", albumRepository.listByTitre());
    //liste par id
        log.info("\n[list par id et titre] : ");
        log.info("{}", albumRepository.listById());
    //listByParution
        log.info("\n[list des parutions entre 2 années] : ");
        log.info("{}", albumRepository.listByParution(1950,2020));
    //listAlbumNext
        log.info("\n[list des albums avec leur suite] : ");
        log.info("{}", albumRepository.ListAlbumNext());

    //ajout d'un album
        log.info("\n[ajout d'un album] : ");
        Album repompe =new Album(50,"on s'est craché sur mars,merci tesla",2020,17) ;
        log.info("{}", albumRepository.ajoutAlbum(repompe));
    }
}
