package popschool.tintin.dao;

import popschool.tintin.model.Album;
import popschool.tintin.model.Personnage;
import popschool.tintin.model.PersonnageDTO;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface AlbumDAO {
    List<Album> listAlbums();

    Optional<Album> findById(int id);
    List<Album> listByTitre();
    List<Album> listById();

    List<Album> listByParution(int an1, int an2);

    List<Album>ListAlbumNext();

    //relation 1 n  couple objet>>>> 1 album clef lié  n personnage(s) apparessant(s) dans l'album
    Map<Album, List<PersonnageDTO>> listPersonnagesOfAlbum();

    int ajoutAlbum(Album album);
}