package popschool.tintin.dao;

import popschool.tintin.model.Album;
import popschool.tintin.model.Personnage;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface PersonnageDAO {
    List<Personnage>listPersonnage();
    Optional<Personnage> findById(int id);
    Optional<Personnage> findByNomPrenom(String nom, String prenom); //args null possible
    List<Personnage> listPersonnagesBySexe(String sexe); //référence de méthode
    List<Personnage> listPersonnagesByJob(String job); //lamba
    List<Personnage> listPersonnagesByGenre(String genre); //RowMapper
    //relation 1 n personnage clef
    Map<Personnage, List<Album>> listAlbumsOfPersonnage();
    int ajoutPersonnage(Personnage personnage);
}
