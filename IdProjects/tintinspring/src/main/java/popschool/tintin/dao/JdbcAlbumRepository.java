package popschool.tintin.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import popschool.tintin.model.Album;
import popschool.tintin.model.PersonnageDTO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;


//list album
//list album +album associé
//album par identifiant
//albums by titre(pattern de reference methode)
//list album entre 2 années
//list albums et personnages dans l album*/


@Repository
public class JdbcAlbumRepository implements AlbumDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;


    //liste complete de tous les albums >>> requete sql
    @Override
    public List<Album> listAlbums() {
        return jdbcTemplate.query(
                "select * from albums",
                this::listAlbums
        );
    }

    //liste complete de tous les albums >>> création de la liste
    protected Album listAlbums(ResultSet rs, int row) throws SQLException {
        return new Album(
                rs.getInt("id"),
                rs.getString("titre"),
                rs.getInt("annee"),
                rs.getInt("suivant")
        );
    }


    // recherche d'album par "id" par l'intermédiaire d'un optional pour la gestion d'erreur null
    @Override
    public Optional<Album> findById(int id) {
        return jdbcTemplate.query(
                "select * from albums where id = ?",
                this::resultSetExtractorOptionalAlbum,
                id
        );
    }

    //recherche une valeur "id" pouvant ne pas existée ou être erronée
    protected Optional<Album> resultSetExtractorOptionalAlbum(ResultSet rs) throws SQLException {
        if (rs.next()) {
            return Optional.of(new Album(
                    rs.getInt("id"),
                    rs.getString("titre"),
                    rs.getInt("annee"),
                    rs.getInt("suivant")
            ));
        }
        return Optional.ofNullable(null);
    }


    //liste complete de tous les titres d'albums >>> requete sql
    @Override
    public List<Album> listByTitre() {
        return jdbcTemplate.query(
                "select titre,suivant from albums",
                this::listByTitre
        );
    }

    //liste complete de tous les titres d'albums >>> création de la liste
    protected Album listByTitre(ResultSet rs, int row) throws SQLException {
        return new Album(
                rs.getString("titre")
        );
    }

    //liste complete de tous les albums par id >>> requete sql
    @Override
    public List<Album> listById() {
        return jdbcTemplate.query(
                "select id,titre from albums",
                this::listById
        );
    }

    //liste complete de tous les albums par id >>> création de la liste
    protected Album listById(ResultSet rs, int row) throws SQLException {
        return new Album(
                rs.getInt("id"),
                rs.getString("titre")
        );
    }

    //requete sql liste des albums entre 2 années
    @Override
    public List<Album> listByParution(int an1, int an2) {
        // if (an1>1930) an1=1930;
        // if (an2<1986) an2=1986;

        return jdbcTemplate.query(
                "select * from albums where annee between ? and ? ",
                this::listAlbums,
                an1, an2
        );
    }

    //liste des albums avec le titre du suivant >>> requete sql
    @Override
    public List<Album> ListAlbumNext() {
        return jdbcTemplate.query(
                // jointure sur id =suivant et remplacement par le titre et son alias
                "select t.titre,s.titre as next from albums t left outer join albums s on t.id= s.suivant",
                this::listAlbumNext
        );
    }

    //liste complete de tous les albums >>> création de la liste
    protected Album listAlbumNext(ResultSet rs, int row) throws SQLException {
        return new Album(
                rs.getString("titre"),
                rs.getString("next")
        );
    }

    @Override
    public Map<Album, List<PersonnageDTO>> listPersonnagesOfAlbum() {
        //requête avec double jointure pour réunir personnages par album
        //lien sur albums.id >> apparait.id_album et apparait.id_personnage >> personnages.id

        return null;

       /* return jdbcTemplate.query(

                " select a.titre,p.nom,p.prenom from albums a " +
                        " inner join apparaits t on t.album_id= a.id " +
                        " inner join personnages p on p.id= t.personnage_id ",

                this::listPersonnagesOfAlbum);
    }

    //Map pour liste des personnnage par album
    protected  Map<Album, List<PersonnageDTO>>listPersonnagesOfAlbum(ResultSet rs) throws SQLException {
      Map<Album, List<PersonnageDTO>> map=new HashMap<>();
        //Map sur le couple album,personnage
        while(rs.next()){
            //tant qu'il existe un suivant pour rs
            Album cle = new Album(rs.getString("titre"));
            //clef objet (tri valeur de table personnage :nom prenom)du du couple map
            PersonnageDTO valeur = new PersonnageDTO(rs.getString("nom"), rs.getString("prenom"));
            //valeur objet de album data transfert object
            if (map.containsKey(cle)){
                map.get(cle).add(valeur);
                //si la map contient la clef,ajoute valeur au niveau de la clef
            }else{
                List<PersonnageDTO> list = new ArrayList<>();
                list.add(valeur);
                map.put(cle, list);
                //sinon ajouter le tableau liste au niveau valeur de la cle
            }
        }
        return map;*/
    }


    //requête d'ajout
    @Override
    public int ajoutAlbum(Album album) {
            return jdbcTemplate.update(
                    "insert into albums values (?,?,?,?)",
                    album.getId(),album.getTitre(),album.getAnnee(),
                    album.getSuivant()
            );
        }
}
