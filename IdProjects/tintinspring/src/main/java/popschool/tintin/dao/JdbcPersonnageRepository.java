package popschool.tintin.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import popschool.tintin.model.Album;
import popschool.tintin.model.AlbumDTO;
import popschool.tintin.model.Personnage;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class JdbcPersonnageRepository implements PersonnageDAO{

    @Autowired
    private JdbcTemplate jdbcTemplate;

    //liste complete de tous les albums >>> requete sql
    @Override
    public List<Personnage> listPersonnage() {
            return jdbcTemplate.query(
                    "select * from personnages",
                    this::listPersonnage
            );
        }
    //liste complete de tous les albums >>> création de la liste
    protected Personnage listPersonnage(ResultSet rs, int row) throws SQLException {
        return  new Personnage (
                rs.getInt("id"),
                rs.getString("nom"),
                rs.getString("prenom"),
                rs.getString("profession"),
                rs.getString("sexe"),
                rs.getString("genre")
        );
    }


    // recherche de personnage par "id" par l'intermédiaire d'un optional pour la gestion d'erreur null
    //requête sql
    @Override
    public Optional<Personnage> findById(int id) {
        return jdbcTemplate.query(
                "select * from personnage where id = ?",
                this::resultSetExtractorOptionalAlbum,
                id
        );
    }
    //recherche une valeur "personnage" pouvant ne pas existée ou être erronée
    //construction de la liste
    protected Optional<Personnage> resultSetExtractorOptionalAlbum(ResultSet rs) throws SQLException {
        if(rs.next()) {
            return Optional.of(new Personnage(
                    rs.getInt("id"),
                    rs.getString("nom"),
                    rs.getString("prenom"),
                    rs.getString("profession"),
                    rs.getString("sexe"),
                    rs.getString("genre")
            ));
        }
        return Optional.ofNullable(null);
    }

    // recherche de personnage par "nom - prenom" par l'intermédiaire d'un optional pour la gestion d'erreur null
    //requête sql
    @Override
    public Optional<Personnage> findByNomPrenom(String nom, String prenom) {
        return jdbcTemplate.query(
                "select * from personnage where nom like ? and prenom like ?",
                this::resultSetExtractorOptionalAlbum,
                nom ,prenom
        );
    }

    //liste des personnes selon le sexe(référence de méthode)
    @Override
    public List<Personnage> listPersonnagesBySexe(String sexe) {
        return jdbcTemplate.query(
                "select * from personnages where sexe like ? ",
                this::listPersonnage,
                sexe
        );
    }

    //liste des personnages selon le job (lambda)
    @Override
    public List<Personnage> listPersonnagesByJob(String job) {
        List<Personnage>listPersonnagesByJob=jdbcTemplate.query(
                "select * from personnages where job like ?",
                (rs,rownum)-> {
                    return new Personnage(
                            rs.getInt("id"),
                            rs.getString("nom"),
                            rs.getString("prenom"),
                            rs.getString("profession"),
                            rs.getString("sexe"),
                            rs.getString("genre")
                    );
                });
        return listPersonnagesByJob;
    }

    //liste des personnages par genre(rowmapper)
    @Override
    public List<Personnage> listPersonnagesByGenre(String genre) {
        PersonnageRowMapper rowMapper = new PersonnageRowMapper();

        List<Personnage> list = jdbcTemplate.query(
                "select * from personnages where genre like ? ",
                 rowMapper
        );
        return list;
    }
    //row mapper de personnage
    private static final class PersonnageRowMapper implements RowMapper<Personnage> {
        @Override
        public Personnage mapRow(ResultSet rs, int rowNum) throws SQLException{
            return new Personnage(
                    rs.getInt("id"),
                    rs.getString("nom"),
                    rs.getString("prenom"),
                    rs.getString("profession"),
                    rs.getString("sexe"),
                    rs.getString("genre")
            );
        }
    }

    @Override
    public Map<Personnage, List<Album>> listAlbumsOfPersonnage() {
        return null;
        /*
        //requête avec double jointure pour réunir les albums par personnage
        //lien sur apparait.id_personnage >> personnages.id et albums.id >> apparait.id_album

       return jdbcTemplate.query(

                " Select p.nom,p.prenom,a.titre from personnages p " +
                " inner join apparaits t on p.id= t.personnage_id "+
                " inner join albums a on  t.album_id= a.id ",

                this::listAlbumsOfPersonnage);
    }

    //Map pour liste des personnnage par album
    protected  Map<Album, List<AlbumDTO>>listAlbumsOfPersonnage(ResultSet rs) throws SQLException {
      Map<Album, List<Album>> map=new HashMap<>();
        //Map sur le couple album,personnage
        while(rs.next()){
            //tant qu'il existe un suivant pour rs
            Personnage cle = new Personnage(rs.getString("nom"), rs.getString("prenom"));
            //clef objet (tri valeur de table personnage :nom prenom)du du couple map
            AlbumDTO valeur = new AlbumDTO(rs.getString("titre"));
            //valeur objet de album data transfert object
            if (map.containsKey(cle)){
                map.get(cle).add(valeur);
                //si la map contient la clef,ajoute valeur au niveau de la clef
            }else{
                List<AlbumDTO> list = new ArrayList<>();
                list.add(valeur);
                map.put(cle, list);
                //sinon ajouter le tableau liste au niveau valeur de la cle
            }
        }
        return map; */
    }


    @Override
    public int ajoutPersonnage(Personnage personnage) {
        return jdbcTemplate.update(
                "insert into personnages values (?,?,?,?,?,?)",
                personnage.getId(),personnage.getNom(),personnage.getPrenom(),
                personnage.getProfession(),personnage.getSexe(),personnage.getGenre()
        );
    }

}
