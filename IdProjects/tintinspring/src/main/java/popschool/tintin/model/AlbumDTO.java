package popschool.tintin.model;

public class AlbumDTO {
    //loué sois tu ...ho lombok !!!


        private int id;
        private String titre;
        private int annee;
        private int suivant;
        private String next;

        //constructeur vide
    public AlbumDTO() {    }

    //constructeur par titre
    public AlbumDTO(String titre) {
        this.titre=titre;
    }

    public int getId() {
        return this.id;
    }

    public String getTitre() {
        return this.titre;
    }

    public int getAnnee() {
        return this.annee;
    }

    public int getSuivant() {
        return this.suivant;
    }

    public String getNext() {
        return this.next;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }

    public void setSuivant(int suivant) {
        this.suivant = suivant;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof AlbumDTO)) return false;
        final AlbumDTO other = (AlbumDTO) o;
        if (!other.canEqual((Object) this)) return false;
        if (this.getId() != other.getId()) return false;
        final Object this$titre = this.getTitre();
        final Object other$titre = other.getTitre();
        if (this$titre == null ? other$titre != null : !this$titre.equals(other$titre)) return false;
        if (this.getAnnee() != other.getAnnee()) return false;
        if (this.getSuivant() != other.getSuivant()) return false;
        final Object this$next = this.getNext();
        final Object other$next = other.getNext();
        if (this$next == null ? other$next != null : !this$next.equals(other$next)) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof AlbumDTO;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        result = result * PRIME + this.getId();
        final Object $titre = this.getTitre();
        result = result * PRIME + ($titre == null ? 43 : $titre.hashCode());
        result = result * PRIME + this.getAnnee();
        result = result * PRIME + this.getSuivant();
        final Object $next = this.getNext();
        result = result * PRIME + ($next == null ? 43 : $next.hashCode());
        return result;
    }

    public String toString() {
        return "AlbumDTO(id=" + this.getId() + ", titre=" + this.getTitre() + ", annee=" + this.getAnnee() + ", suivant=" + this.getSuivant() + ", next=" + this.getNext() + ")";
    }
}
