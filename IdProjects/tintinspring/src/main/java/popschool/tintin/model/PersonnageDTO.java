package popschool.tintin.model;

public class PersonnageDTO {

        private String nom;
        private String prenom;


        //constructeur vide
        public PersonnageDTO() {
        }

        public PersonnageDTO(String nom, String prenom) {
            this.nom = nom;
            this.prenom = prenom;
        }

        public boolean equals(final Object o) {
            if (o == this) return true;
            if (!(o instanceof PersonnageDTO)) return false;
            final PersonnageDTO other = (PersonnageDTO) o;
            if (!other.canEqual((Object) this)) return false;
            return true;
        }

        protected boolean canEqual(final Object other) {
            return other instanceof PersonnageDTO;
        }

        public int hashCode() {
            int result = 1;
            return result;
        }

        public String toString() {
            return "PersonnageDTO()";
        }


}
