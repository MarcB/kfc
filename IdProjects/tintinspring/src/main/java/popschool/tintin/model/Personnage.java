package popschool.tintin.model;

public class Personnage {
    private int id;
    private String nom;
    private String prenom;
    private String profession;
    private String sexe;
    private String genre;

    //constructeur vide
    public Personnage(int id, String nom,String prenom, String profession, String sexe, String genre) {    }

    //constructeur nom prenom
    public Personnage(String nom,String prenom) {
        this.nom=nom;
        this.prenom=prenom;
    }


    //getter


    public int getId() {  return id;   }

    public String getNom() {        return this.nom;    }

    public String getPrenom() {        return this.prenom;    }

    public String getProfession() {        return this.profession;    }

    public String getSexe() {        return this.sexe;    }

    public String getGenre() {        return this.genre;    }

    //setter

    public void setId(int id) {        this.id = id;    }

    public void setNom(String nom) {        this.nom = nom;    }

    public void setPrenom(String prenom) {        this.prenom = prenom;    }

    public void setProfession(String profession) {        this.profession = profession;    }

    public void setSexe(String sexe) {        this.sexe = sexe;    }

    public void setGenre(String genre) {        this.genre = genre;    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof Personnage)) return false;
        final Personnage other = (Personnage) o;
        if (!other.canEqual((Object) this)) return false;
        final Object this$nom = this.getNom();
        final Object other$nom = other.getNom();
        if (this$nom == null ? other$nom != null : !this$nom.equals(other$nom)) return false;
        final Object this$prenom = this.getPrenom();
        final Object other$prenom = other.getPrenom();
        if (this$prenom == null ? other$prenom != null : !this$prenom.equals(other$prenom)) return false;
        final Object this$profession = this.getProfession();
        final Object other$profession = other.getProfession();
        if (this$profession == null ? other$profession != null : !this$profession.equals(other$profession))
            return false;
        final Object this$sexe = this.getSexe();
        final Object other$sexe = other.getSexe();
        if (this$sexe == null ? other$sexe != null : !this$sexe.equals(other$sexe)) return false;
        final Object this$genre = this.getGenre();
        final Object other$genre = other.getGenre();
        if (this$genre == null ? other$genre != null : !this$genre.equals(other$genre)) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {        return other instanceof Personnage;    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $nom = this.getNom();
        result = result * PRIME + ($nom == null ? 43 : $nom.hashCode());
        final Object $prenom = this.getPrenom();
        result = result * PRIME + ($prenom == null ? 43 : $prenom.hashCode());
        final Object $profession = this.getProfession();
        result = result * PRIME + ($profession == null ? 43 : $profession.hashCode());
        final Object $sexe = this.getSexe();
        result = result * PRIME + ($sexe == null ? 43 : $sexe.hashCode());
        final Object $genre = this.getGenre();
        result = result * PRIME + ($genre == null ? 43 : $genre.hashCode());
        return result;
    }

    public String toString() {
        return "Personnage(nom=" + this.getNom() + ", " +
                "prenom=" + this.getPrenom() + ", profession=" +
                this.getProfession() + ", sexe=" + this.getSexe() +
                ", genre=" + this.getGenre() + ")";
    }
}
