package popschool.tintin.model;

/*liste des personnages
retrouver un personnage par son identifiant
retrouver un personnage par son nom et prénom(nullité des arg. possible)
liste des personnages d un sexe donné( référence de méthode)
list by Job
listByGenre
listPersonnageAlbumforpers
AddPersonnage


lsit album
list album +album associé
album par identifiant
albums by titre(pattern de reference methode)
list album entre 2 années
list albums et personnages dans l album*/


import java.util.Date;

//loué sois tu ...ho lombok !!!

    public class Album {
        private int id;
        private String titre;
        private int annee;
        private int suivant;
        private String next;

    //constructeur vide
    public Album() { }

    //constructeur de la table albums de la base
    public Album(int id, String titre, int annee, int suivant) {
        this.id = id;
        this.titre = titre;
        this.annee = annee;
        this.suivant = suivant;
    }

    //constructeur album et suite par alais de table
    public Album( String titre, String next ) {
        this.titre = titre;
        this.next = next;

    }


    //constructeur sur les titres d'album
    public Album(String titre) {
        this.titre = titre;
    }

    //constructeur sur les id de la table albums
    public Album(int id,String titre) {
        this.id = id;
        this.titre = titre;
    }


    //getter setter
    public int getId() {        return this.id;    }

    public String getTitre() {        return this.titre;    }

    public int getAnnee() {        return this.annee;    }

    public int getSuivant() {        return this.suivant;    }

    public void setId(int id) {        this.id = id;    }

    public void setTitre(String titre) {        this.titre = titre;    }

    public void setAnnee(int annee) {        this.annee = annee;    }

    public void setSuivant(int suivant) {        this.suivant = suivant;    }

    public String getNext() {            return next;        }

    public void setNext(String next) {            this.next = next;        }

        public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof Album)) return false;
        final Album other = (Album) o;
        if (!other.canEqual((Object) this)) return false;
        if (this.getId() != other.getId()) return false;
        final Object this$titre = this.getTitre();
        final Object other$titre = other.getTitre();
        if (this$titre == null ? other$titre != null : !this$titre.equals(other$titre)) return false;
        final Object this$annee = this.getAnnee();
        final Object other$annee = other.getAnnee();
        if (this$annee == null ? other$annee != null : !this$annee.equals(other$annee)) return false;
        if (this.getSuivant() != other.getSuivant()) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof Album;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        result = result * PRIME + this.getId();
        final Object $titre = this.getTitre();
        result = result * PRIME + ($titre == null ? 43 : $titre.hashCode());
        final Object $annee = this.getAnnee();
        result = result * PRIME + ($annee == null ? 43 : $annee.hashCode());
        result = result * PRIME + this.getSuivant();
        return result;
    }

    public String toString() {
        String stAlbum="\n Album ";
        if(this.getId()!=0){stAlbum+="N°" + this.getId()+" ";}
        if(this.getTitre()!=null){stAlbum+=":" + this.getTitre()+" ";}
        if(this.getAnnee()!=0){stAlbum+="| année=" + this.getAnnee()+" ";}
        if(this.getSuivant()!=0){stAlbum+="| suivant=" + this.getSuivant()+" ";}
        //methode string pour l alias de table next pour l album suivant
        if(this.getNext()!=null){stAlbum+="| suite : " + this.getNext()+" ";}

        return  stAlbum;
    }
}

