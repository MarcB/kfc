package popschool.mycinemaspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MycinemaspringApplication {

    public static void main(String[] args) {
        SpringApplication.run(MycinemaspringApplication.class, args);
    }

}
