package popschool.mycinemaspring.dao;

import org.springframework.data.repository.CrudRepository;
import popschool.mycinemaspring.model.Acteur;

import java.util.List;
import java.util.Optional;

public interface ActeurRepository  extends CrudRepository<Acteur, Long> {

    Optional<Acteur> findByNomLike(String nom);

}
