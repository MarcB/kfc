package popschool.mycinemaspring.dao;

import org.springframework.data.repository.CrudRepository;
import popschool.mycinemaspring.model.Acteur;
import popschool.mycinemaspring.model.Pays;
import java.util.List;
import java.util.Optional;

public interface PaysRepository  extends CrudRepository<Pays, Long> {

    Optional<Pays> findByNomLike(String nom) ;

}