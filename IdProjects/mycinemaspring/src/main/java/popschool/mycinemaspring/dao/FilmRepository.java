package popschool.mycinemaspring.dao;

import org.springframework.data.repository.CrudRepository;

import popschool.mycinemaspring.model.Film;

import java.util.Optional;

public interface FilmRepository extends CrudRepository<Film, Long> {

    Optional<Film> findByNomLike(String nom);

}