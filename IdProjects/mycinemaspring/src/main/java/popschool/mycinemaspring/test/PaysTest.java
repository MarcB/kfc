package popschool.mycinemaspring.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import popschool.mycinemaspring.MycinemaspringApplication;
import popschool.mycinemaspring.dao.ActeurRepository;
import popschool.mycinemaspring.dao.PaysRepository;
import popschool.mycinemaspring.model.Acteur;
import popschool.mycinemaspring.model.Pays;

import java.util.Optional;

@Component
public class PaysTest implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(MycinemaspringApplication.class);

    @Autowired
    private PaysRepository paysRepository;

    private void information(Acteur a){
        log.info(a.toString());
    }

    @Override
    public void run(String... args) throws Exception {
        //fetch all acteur
        log.info("All Pays found with findAll():");
        log.info("-------------------DEBUT LIST PAYS------------------------------------------------------------------");
        for (Pays pays : paysRepository.findAll()){log.info(pays.toString());}
        log.info("--------------------FIN LIST PAYS-----------------------------------------------------");


        /*
        // fetch an acteur by ID
        Optional<Acteur> acteur = acteurRepository.findById(1L);
        log.info("Trouver un acteur en fonction de son id findById(1L)");
        log.info("---------------------------------------------------------------------------------------------------");
        if(acteur.isPresent()) {
            Acteur act = acteur.get();
            log.info(acteur.get().toString());
            log.info("");
            //acteurRepository.findAll().forEach(this::information);
            //log.info("");
        }

        // trouver un acteur avec son nom
        Optional<Acteur> acteur2 = acteurRepository.findByNomLike("Ford");
        log.info("Trouver un acteur en fonction de son nom findByNomLike(Ford)");
        log.info("---------------------------------------------------------------------------------------------------");
        if(acteur2.isPresent()) {
            Acteur act = acteur2.get();
            log.info(acteur2.get().toString());
            log.info("");
            //acteurRepository.findAll().forEach(this::information);
            //log.info("");
        }

        //Trouver des acteur en fonction d'un prenom
*/

    }

}
