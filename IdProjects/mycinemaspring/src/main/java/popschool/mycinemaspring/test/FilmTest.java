package popschool.mycinemaspring.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import popschool.mycinemaspring.MycinemaspringApplication;
import popschool.mycinemaspring.dao.FilmRepository;
import popschool.mycinemaspring.model.Film;

@Component
public class FilmTest implements CommandLineRunner {

		private static final Logger log = LoggerFactory.getLogger(MycinemaspringApplication.class);

		@Autowired
		private FilmRepository filmRepository;

		private void information(Film f){
			log.info(f.toString());
		}

		@Override
		public void run(String... args) throws Exception {
			//fetch all film
			log.info("All Film found with findAll():");
			log.info("-----------------------------DEBUT LIST FILM-------------------------------------------------------------");
			for (Film film : filmRepository.findAll()){log.info(film.toString());}
			log.info("------------------------------fin LIST FILM-------------------------------------------");

		}

	}


