package popschool.mycinemaspring.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="pays")
public class Pays {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer npays;
    private String nom;

    @OneToMany(mappedBy = "nationalite")
    private List<Acteur> acteurs = new ArrayList<>();

    @OneToMany(mappedBy = "npays")
    private List<Film> films = new ArrayList<>();

    public Pays() {
    }

    @Override
    public String toString() {
        return "Pays : " +
                 nom + '\'' +
                '}';
    }

    public Integer getNpays() {
        return this.npays;
    }

    public String getNom() {
        return this.nom;
    }

    public List<Acteur> getActeurs() {
        return this.acteurs;
    }

    public void setNpays(Integer npays) {
        this.npays = npays;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setActeurs(List<Acteur> acteurs) {
        this.acteurs = acteurs;
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof Pays)) return false;
        final Pays other = (Pays) o;
        if (!other.canEqual((Object) this)) return false;
        final Object this$npays = this.getNpays();
        final Object other$npays = other.getNpays();
        if (this$npays == null ? other$npays != null : !this$npays.equals(other$npays)) return false;
        final Object this$nom = this.getNom();
        final Object other$nom = other.getNom();
        if (this$nom == null ? other$nom != null : !this$nom.equals(other$nom)) return false;
        final Object this$acteurs = this.getActeurs();
        final Object other$acteurs = other.getActeurs();
        if (this$acteurs == null ? other$acteurs != null : !this$acteurs.equals(other$acteurs)) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof Pays;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $npays = this.getNpays();
        result = result * PRIME + ($npays == null ? 43 : $npays.hashCode());
        final Object $nom = this.getNom();
        result = result * PRIME + ($nom == null ? 43 : $nom.hashCode());
        final Object $acteurs = this.getActeurs();
        result = result * PRIME + ($acteurs == null ? 43 : $acteurs.hashCode());
        return result;
    }
}