package popschool.mycinemaspring.model;

import javax.persistence.*;
import java.util.Date;

@Entity

//@Table(name="acteur")

public class Acteur {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
  //  @Column(name="nacteur")
    private Integer nacteur;

 //   @Column(name="nom")
    private String nom;

 //   @Column(name="prenom")
    private String prenom;

 //   @Column(name="naissance")
    @Temporal(TemporalType.DATE)
    private Date naissance;

    //private Long nationalite;
    @ManyToOne
  //  @Column(name="nationalite")
    private Pays nationalite;

 //   @Column(name="nbrefilms")
    private Integer nbrefilms;

    protected Acteur(){}



    @Override
    public String toString() {
        return " Acteur " +
                "n° " + nacteur+" "
                 + nom + " "
                + prenom + " "
                + naissance+" "
                + nationalite+" "
                + ", nb films :" + nbrefilms
                ;
    }

    public Integer getNacteur() {
        return this.nacteur;
    }

    public String getNom() {
        return this.nom;
    }

    public String getPrenom() {
        return this.prenom;
    }

    public Date getNaissance() {
        return this.naissance;
    }

    public Pays getNationalite() {
        return this.nationalite;
    }

    public Integer getNbrefilms() {
        return this.nbrefilms;
    }

    public void setNacteur(Integer nacteur) {
        this.nacteur = nacteur;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setNaissance(Date naissance) {
        this.naissance = naissance;
    }

    public void setNationalite(Pays nationalite) {
        this.nationalite = nationalite;
    }

    public void setNbrefilms(Integer nbrefilms) {
        this.nbrefilms = nbrefilms;
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof Acteur)) return false;
        final Acteur other = (Acteur) o;
        if (!other.canEqual((Object) this)) return false;
        final Object this$nacteur = this.getNacteur();
        final Object other$nacteur = other.getNacteur();
        if (this$nacteur == null ? other$nacteur != null : !this$nacteur.equals(other$nacteur)) return false;
        final Object this$nom = this.getNom();
        final Object other$nom = other.getNom();
        if (this$nom == null ? other$nom != null : !this$nom.equals(other$nom)) return false;
        final Object this$prenom = this.getPrenom();
        final Object other$prenom = other.getPrenom();
        if (this$prenom == null ? other$prenom != null : !this$prenom.equals(other$prenom)) return false;
        final Object this$naissance = this.getNaissance();
        final Object other$naissance = other.getNaissance();
        if (this$naissance == null ? other$naissance != null : !this$naissance.equals(other$naissance)) return false;
        final Object this$nationalite = this.getNationalite();
        final Object other$nationalite = other.getNationalite();
        if (this$nationalite == null ? other$nationalite != null : !this$nationalite.equals(other$nationalite))
            return false;
        final Object this$nbrefilms = this.getNbrefilms();
        final Object other$nbrefilms = other.getNbrefilms();
        if (this$nbrefilms == null ? other$nbrefilms != null : !this$nbrefilms.equals(other$nbrefilms)) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof Acteur;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $nacteur = this.getNacteur();
        result = result * PRIME + ($nacteur == null ? 43 : $nacteur.hashCode());
        final Object $nom = this.getNom();
        result = result * PRIME + ($nom == null ? 43 : $nom.hashCode());
        final Object $prenom = this.getPrenom();
        result = result * PRIME + ($prenom == null ? 43 : $prenom.hashCode());
        final Object $naissance = this.getNaissance();
        result = result * PRIME + ($naissance == null ? 43 : $naissance.hashCode());
        final Object $nationalite = this.getNationalite();
        result = result * PRIME + ($nationalite == null ? 43 : $nationalite.hashCode());
        final Object $nbrefilms = this.getNbrefilms();
        result = result * PRIME + ($nbrefilms == null ? 43 : $nbrefilms.hashCode());
        return result;
    }
}
