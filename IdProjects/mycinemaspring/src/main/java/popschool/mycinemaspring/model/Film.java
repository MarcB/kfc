package popschool.mycinemaspring.model;

import javax.persistence.*;
import java.util.Date;
@Entity

//@Table(name="film")

public class Film {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    //  @Column(name="nfilm")
    private Integer nfilm;

    //   @Column(name="titre")
    private String titre;

    //   @Column(name="ngenre")
    private Integer ngenre;

    //   @Column(name="sortie")
    @Temporal(TemporalType.DATE)
    private Date sortie;

    //private  npays;
    @ManyToOne
    //  @Column(name="npays")
    private Pays npays;

    //   @Column(name="realisateur")
    private String realisateur;

    //
    private Integer nacteurprincipal;

    //
    private Integer entrees;

    //
    private String oscar;

    public Film() {
    }

    public Integer getNfilm() {
        return this.nfilm;
    }

    public String getTitre() {
        return this.titre;
    }

    public Integer getNgenre() {
        return this.ngenre;
    }

    public Date getSortie() {
        return this.sortie;
    }

    public Pays getNpays() {
        return this.npays;
    }

    public String getRealisateur() {
        return this.realisateur;
    }

    public Integer getNacteurprincipal() {
        return this.nacteurprincipal;
    }

    public Integer getEntrees() {
        return this.entrees;
    }

    public String getOscar() {
        return this.oscar;
    }

    public void setNfilm(Integer nfilm) {
        this.nfilm = nfilm;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public void setNgenre(Integer ngenre) {
        this.ngenre = ngenre;
    }

    public void setSortie(Date sortie) {
        this.sortie = sortie;
    }

    public void setNpays(Pays npays) {
        this.npays = npays;
    }

    public void setRealisateur(String realisateur) {
        this.realisateur = realisateur;
    }

    public void setNacteurprincipal(Integer nacteurprincipal) {
        this.nacteurprincipal = nacteurprincipal;
    }

    public void setEntrees(Integer entrees) {
        this.entrees = entrees;
    }

    public void setOscar(String oscar) {
        this.oscar = oscar;
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof Film)) return false;
        final Film other = (Film) o;
        if (!other.canEqual((Object) this)) return false;
        final Object this$nfilm = this.getNfilm();
        final Object other$nfilm = other.getNfilm();
        if (this$nfilm == null ? other$nfilm != null : !this$nfilm.equals(other$nfilm)) return false;
        final Object this$titre = this.getTitre();
        final Object other$titre = other.getTitre();
        if (this$titre == null ? other$titre != null : !this$titre.equals(other$titre)) return false;
        final Object this$ngenre = this.getNgenre();
        final Object other$ngenre = other.getNgenre();
        if (this$ngenre == null ? other$ngenre != null : !this$ngenre.equals(other$ngenre)) return false;
        final Object this$sortie = this.getSortie();
        final Object other$sortie = other.getSortie();
        if (this$sortie == null ? other$sortie != null : !this$sortie.equals(other$sortie)) return false;
        final Object this$npays = this.getNpays();
        final Object other$npays = other.getNpays();
        if (this$npays == null ? other$npays != null : !this$npays.equals(other$npays)) return false;
        final Object this$realisateur = this.getRealisateur();
        final Object other$realisateur = other.getRealisateur();
        if (this$realisateur == null ? other$realisateur != null : !this$realisateur.equals(other$realisateur))
            return false;
        final Object this$nacteurprincipal = this.getNacteurprincipal();
        final Object other$nacteurprincipal = other.getNacteurprincipal();
        if (this$nacteurprincipal == null ? other$nacteurprincipal != null : !this$nacteurprincipal.equals(other$nacteurprincipal))
            return false;
        final Object this$entrees = this.getEntrees();
        final Object other$entrees = other.getEntrees();
        if (this$entrees == null ? other$entrees != null : !this$entrees.equals(other$entrees)) return false;
        final Object this$oscar = this.getOscar();
        final Object other$oscar = other.getOscar();
        if (this$oscar == null ? other$oscar != null : !this$oscar.equals(other$oscar)) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof Film;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $nfilm = this.getNfilm();
        result = result * PRIME + ($nfilm == null ? 43 : $nfilm.hashCode());
        final Object $titre = this.getTitre();
        result = result * PRIME + ($titre == null ? 43 : $titre.hashCode());
        final Object $ngenre = this.getNgenre();
        result = result * PRIME + ($ngenre == null ? 43 : $ngenre.hashCode());
        final Object $sortie = this.getSortie();
        result = result * PRIME + ($sortie == null ? 43 : $sortie.hashCode());
        final Object $npays = this.getNpays();
        result = result * PRIME + ($npays == null ? 43 : $npays.hashCode());
        final Object $realisateur = this.getRealisateur();
        result = result * PRIME + ($realisateur == null ? 43 : $realisateur.hashCode());
        final Object $nacteurprincipal = this.getNacteurprincipal();
        result = result * PRIME + ($nacteurprincipal == null ? 43 : $nacteurprincipal.hashCode());
        final Object $entrees = this.getEntrees();
        result = result * PRIME + ($entrees == null ? 43 : $entrees.hashCode());
        final Object $oscar = this.getOscar();
        result = result * PRIME + ($oscar == null ? 43 : $oscar.hashCode());
        return result;
    }

    public String toString() {
        return "Film(nfilm=" + this.getNfilm() + ", titre=" + this.getTitre() + ", ngenre=" + this.getNgenre() + ", sortie=" + this.getSortie() + ", npays=" + this.getNpays() + ", realisateur=" + this.getRealisateur() + ", nacteurprincipal=" + this.getNacteurprincipal() + ", entrees=" + this.getEntrees() + ", oscar=" + this.getOscar() + ")";
    }
}
