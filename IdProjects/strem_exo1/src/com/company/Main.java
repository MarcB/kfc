package com.company;

import com.sun.xml.internal.ws.util.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        List<String> words = Arrays.asList("hi", "hello", "rello","hello world");

        System.out.println("exo1****************************");
        words.stream().forEach(s->System.out.println("  "+s));
        System.out.println("exo 2***************************");
        words.stream().forEach(System.out::println);

        System.out.println("exo 3***************************");

        words.stream().forEach(s->System.out.println(s+"!"));
        List<String>existingWords =words.stream().map(s->s+"!")
                                        .collect(Collectors.toList());
        System.out.printf("Existing words : %s,%n",existingWords);

        List<String>eyeWords=words.stream().map(s->s.replace("i","eye"))
                .collect(Collectors.toList());
        System.out.printf("List without i : %s,%n",eyeWords);
//        words.stream().forEach(System.out::println);
        List<String>upperCaseWords=words.stream().map(String::toUpperCase)
                                                 .collect(Collectors.toList());
        System.out.printf("List to upper : %s,%n",upperCaseWords);

        System.out.println("exo 4***************************");


        //List<String> shortWords = StringUtils.allMatches(words, s -> s.length() < 4);
        //• List<String> wordsWithB = StringUtils.allMatches(words, s -> s.contains("b"));
        //• List<String> evenLengthWords = StringUtils.allMatches(words, s -> (s.length() % 2) == 0);
        //filter

        List<String>shortWordswords=words.stream().filter(s->s.length()<4).collect(Collectors.toList());
        System.out.printf("List to length : %s,%n",shortWordswords);
        List<String>withBwords=words.stream().filter(s->s.contains("b")).collect(Collectors.toList());
        System.out.printf("List with b : %s,%n",withBwords);
        List<String>moduloWords=words.stream().filter(s->(s.length() % 2) == 0).collect(Collectors.toList());
        System.out.printf("List with b : %s,%n",moduloWords);

        System.out.println("exo 5***************************");

        Function<String,String> toUpper=s-> {
            System.out.println("to upper "+s);
            return (s.toUpperCase());
        };
        Optional<String> tryage=words.stream()
                .map(toUpper)
                .filter(s->s.contains("E"))
                .filter(s->s.length()>4)
                .findFirst();

        System.out.printf("List filtré upper E ect : "+tryage);

        System.out.println("exo 6***************************");

        String[] existingWords2 =words.stream().map(s->s+"!")
                .toArray(String[]::new);
        System.out.printf("Existing words list as array : %s,%n",Arrays.asList(existingWords2));

        System.out.println("partie 2 // exo 1***************************");
//        Afficher chaque élément
//        Stream.of(someArray).forEach(System.out::println);
//        Remise à zéro de tous les champs
//        fieldList.stream().forEach( field-> field.setText'"") );
        String total = " ";
//        String monEach=words.stream().forEach("",concat(total+s)).collect(Collectors.toList());
        String monReduced=words.stream().reduce("",(s1,s2)->s1+s2.toUpperCase());
       String monReduced2=words.stream().reduce("",String::concat).toUpperCase();
        System.out.println("list reduce+concat "+monReduced);

        System.out.println("partie 2 // exo 2***************************");
        String monReduced3=words.stream().map(String::toUpperCase).reduce("",String::concat).toUpperCase();
        System.out.println("list reduce+concat "+monReduced3);

        System.out.println("partie 2 // exo 3***************************");
        String monReduced4=words.stream().map(String::toUpperCase).reduce("\"",(s1,s2)->s1+s2.toUpperCase()+",\"");
        System.out.println("list reduce+concat "+monReduced4);

        String monConcat=words.stream().map(String::toUpperCase).reduce("",(s1,s2)->s1+s2);
        int monCount=monConcat.length();
        System.out.println("Size : "+ monCount);
    }


}
