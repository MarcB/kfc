package modele;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "COMPTE", schema = "Picsou", catalog = "")
public class CompteEntity {
    private int id;
    private String numero;
    private int titulaire;
    private Date dateOuverture;
    private Double plafond;
    private String bloque;
    private double solde;
    private Collection<CarteEntity> cartesById;
    private ClientEntity clientByTitulaire;
    private Collection<MouvementEntity> mouvementsById;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "numero")
    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Basic
    @Column(name = "titulaire")
    public int getTitulaire() {
        return titulaire;
    }

    public void setTitulaire(int titulaire) {
        this.titulaire = titulaire;
    }

    @Basic
    @Column(name = "date_ouverture")
    public Date getDateOuverture() {
        return dateOuverture;
    }

    public void setDateOuverture(Date dateOuverture) {
        this.dateOuverture = dateOuverture;
    }

    @Basic
    @Column(name = "plafond")
    public Double getPlafond() {
        return plafond;
    }

    public void setPlafond(Double plafond) {
        this.plafond = plafond;
    }

    @Basic
    @Column(name = "bloque")
    public String getBloque() {
        return bloque;
    }

    public void setBloque(String bloque) {
        this.bloque = bloque;
    }

    @Basic
    @Column(name = "solde")
    public double getSolde() {
        return solde;
    }

    public void setSolde(double solde) {
        this.solde = solde;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompteEntity that = (CompteEntity) o;
        return id == that.id &&
                titulaire == that.titulaire &&
                Double.compare(that.solde, solde) == 0 &&
                Objects.equals(numero, that.numero) &&
                Objects.equals(dateOuverture, that.dateOuverture) &&
                Objects.equals(plafond, that.plafond) &&
                Objects.equals(bloque, that.bloque);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, numero, titulaire, dateOuverture, plafond, bloque, solde);
    }

    @OneToMany(mappedBy = "compteByIdCompte")
    public Collection<CarteEntity> getCartesById() {
        return cartesById;
    }

    public void setCartesById(Collection<CarteEntity> cartesById) {
        this.cartesById = cartesById;
    }

    @ManyToOne
    @JoinColumn(name = "titulaire", referencedColumnName = "id", nullable = false)
    public ClientEntity getClientByTitulaire() {
        return clientByTitulaire;
    }

    public void setClientByTitulaire(ClientEntity clientByTitulaire) {
        this.clientByTitulaire = clientByTitulaire;
    }

    @OneToMany(mappedBy = "compteByIdCompte")
    public Collection<MouvementEntity> getMouvementsById() {
        return mouvementsById;
    }

    public void setMouvementsById(Collection<MouvementEntity> mouvementsById) {
        this.mouvementsById = mouvementsById;
    }
}
