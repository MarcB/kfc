package modele;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "RETRAIT", schema = "Picsou", catalog = "")
public class RetraitEntity {
    private int id;
    private int idCarte;
    private Timestamp dateRetrait;
    private String adresseGab;
    private double montant;
    private String etat;
    private Integer idMouvement;
    private CarteEntity carteByIdCarte;
    private MouvementEntity mouvementByIdMouvement;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "id_carte")
    public int getIdCarte() {
        return idCarte;
    }

    public void setIdCarte(int idCarte) {
        this.idCarte = idCarte;
    }

    @Basic
    @Column(name = "date_retrait")
    public Timestamp getDateRetrait() {
        return dateRetrait;
    }

    public void setDateRetrait(Timestamp dateRetrait) {
        this.dateRetrait = dateRetrait;
    }

    @Basic
    @Column(name = "adresseGAB")
    public String getAdresseGab() {
        return adresseGab;
    }

    public void setAdresseGab(String adresseGab) {
        this.adresseGab = adresseGab;
    }

    @Basic
    @Column(name = "montant")
    public double getMontant() {
        return montant;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }

    @Basic
    @Column(name = "etat")
    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    @Basic
    @Column(name = "id_mouvement")
    public Integer getIdMouvement() {
        return idMouvement;
    }

    public void setIdMouvement(Integer idMouvement) {
        this.idMouvement = idMouvement;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RetraitEntity that = (RetraitEntity) o;
        return id == that.id &&
                idCarte == that.idCarte &&
                Double.compare(that.montant, montant) == 0 &&
                Objects.equals(dateRetrait, that.dateRetrait) &&
                Objects.equals(adresseGab, that.adresseGab) &&
                Objects.equals(etat, that.etat) &&
                Objects.equals(idMouvement, that.idMouvement);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, idCarte, dateRetrait, adresseGab, montant, etat, idMouvement);
    }

    @ManyToOne
    @JoinColumn(name = "id_carte", referencedColumnName = "id", nullable = false)
    public CarteEntity getCarteByIdCarte() {
        return carteByIdCarte;
    }

    public void setCarteByIdCarte(CarteEntity carteByIdCarte) {
        this.carteByIdCarte = carteByIdCarte;
    }

    @ManyToOne
    @JoinColumn(name = "id_mouvement", referencedColumnName = "id")
    public MouvementEntity getMouvementByIdMouvement() {
        return mouvementByIdMouvement;
    }

    public void setMouvementByIdMouvement(MouvementEntity mouvementByIdMouvement) {
        this.mouvementByIdMouvement = mouvementByIdMouvement;
    }
}
