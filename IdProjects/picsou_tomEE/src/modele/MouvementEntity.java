package modele;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "MOUVEMENT", schema = "Picsou", catalog = "")
public class MouvementEntity {
    private int id;
    private int idCompte;
    private Timestamp dateOperation;
    private double montant;
    private String nature;
    private CompteEntity compteByIdCompte;
    private Collection<RetraitEntity> retraitsById;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "id_compte")
    public int getIdCompte() {
        return idCompte;
    }

    public void setIdCompte(int idCompte) {
        this.idCompte = idCompte;
    }

    @Basic
    @Column(name = "date_operation")
    public Timestamp getDateOperation() {
        return dateOperation;
    }

    public void setDateOperation(Timestamp dateOperation) {
        this.dateOperation = dateOperation;
    }

    @Basic
    @Column(name = "montant")
    public double getMontant() {
        return montant;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }

    @Basic
    @Column(name = "nature")
    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MouvementEntity that = (MouvementEntity) o;
        return id == that.id &&
                idCompte == that.idCompte &&
                Double.compare(that.montant, montant) == 0 &&
                Objects.equals(dateOperation, that.dateOperation) &&
                Objects.equals(nature, that.nature);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, idCompte, dateOperation, montant, nature);
    }

    @ManyToOne
    @JoinColumn(name = "id_compte", referencedColumnName = "id", nullable = false)
    public CompteEntity getCompteByIdCompte() {
        return compteByIdCompte;
    }

    public void setCompteByIdCompte(CompteEntity compteByIdCompte) {
        this.compteByIdCompte = compteByIdCompte;
    }

    @OneToMany(mappedBy = "mouvementByIdMouvement")
    public Collection<RetraitEntity> getRetraitsById() {
        return retraitsById;
    }

    public void setRetraitsById(Collection<RetraitEntity> retraitsById) {
        this.retraitsById = retraitsById;
    }
}
