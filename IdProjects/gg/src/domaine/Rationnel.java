package domaine;
import static java.lang.Math.*;

public class Rationnel {
    private static int pgcd(int a, int b){
        if(a==b) {
            return a;
        }
        if(a>b) {
            return pgcd(a-b, b);
        }
        else {
            return pgcd(a,b-a);
        }
    }
    
    private int numerateur;
    private int denominateur;

    public int getNumerateur() {
        return numerateur;
    }

    public void setNumerateur(int numerateur) {
        this.numerateur = numerateur;
    }

    public int getDenominateur() {
        return denominateur;
    }

    public void setDenominateur(int denominateur) {
        assert denominateur!=0;
        this.denominateur = denominateur;
    }

    public Rationnel(int numerateur, int denominateur) {
        assert denominateur!=0 : "Le denominateur ne peut �tre nul";
        
        int sgn = (numerateur*denominateur>0?+1 : -1);
        int xnum = abs(numerateur);
        int xden = abs(denominateur);
        
        int p = pgcd(abs(xnum), abs(xden));
        
        this.numerateur = (sgn==1? xnum / p: - xnum/p);
        this.denominateur = xden / p;
    }
    
    public Rationnel(int entier){
    	this(entier,1);
    }
    
    public Rationnel add(Rationnel r){
        int denom = this.denominateur * r.denominateur;
        int numer = (this.numerateur*r.denominateur)+(r.numerateur*this.denominateur);
        
        int p = pgcd(abs(numer), abs(denom));
        
        return new Rationnel(numer/p, denom/p);
    }
    
    public Rationnel add(Integer r){
        int denom = this.denominateur;
        int numer = (this.numerateur)+(r*this.denominateur);
        
        int p = pgcd(abs(numer), denom);
        
        return new Rationnel(numer/p, denom/p);
    }
    
    public Rationnel sub(Rationnel r){
        int denom = this.denominateur * r.denominateur;
        int numer = (this.numerateur*r.denominateur)-(r.numerateur*this.denominateur);
        
        int p = pgcd(abs(numer), abs(denom));
        
        return new Rationnel(numer/p, denom/p);
    }
    
    public Rationnel sub(Integer r){
        int denom = this.denominateur;
        int numer = (this.numerateur)-(r*this.denominateur);
        
        int p = pgcd(abs(numer), denom);
        
        return new Rationnel(numer/p, denom/p);
    }
    
    

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + denominateur;
		result = prime * result + numerateur;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rationnel other = (Rationnel) obj;
		if (denominateur != other.denominateur)
			return false;
		if (numerateur != other.numerateur)
			return false;
		return true;
	}

	@Override
    public String toString() {
        String aux;
        if(denominateur==1) {
            aux=""+numerateur;
        }
        else if(numerateur==0) {
            aux=" 0 ";
        }
        else {
            aux=""+numerateur+" / "+denominateur;
        }
        return aux;
    }
    
    
    
    public static void main(String[] args) {
        Rationnel r1 = new Rationnel(2,-4);
        Rationnel r2 = new Rationnel(1,4);  
//        
        System.out.println(r1.add(4));
        
        System.out.println(r1.sub(4));
    }

}
