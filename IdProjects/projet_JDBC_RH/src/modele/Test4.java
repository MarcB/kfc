package modele;
import java.sql.*;
public class Test4 {
    private final static String SQLselectEmployOfDept =
            "SELECT ename, sal, deptno, comm from employee" + " where deptno = ? " + " order by ename ";
    private final static String SQLupdateEmployOfDept =
            " update employee " + " set sal = sal *(1.0 + ? / 100.0)"
                    + " where deptno = ? ";
    private static String SQLselctDeptHavingMaxSumSalary =
            "select deptno, sum(sal) somme " +
                    " from employee " +
                    " group by deptno " +
                    " order by somme desc";
    public static void main(String[] args)throws SQLException {
        Connection conn = null;
        PreparedStatement pstm1 = null;
        PreparedStatement pstm2 = null;
        int [][] augmentation = {{20,5},{30,8},{10,15}};

        try{
            conn = Connex.getInstance();
            conn.setAutoCommit(false);
            pstm1 = conn.prepareStatement(SQLselectEmployOfDept);
            pstm2 = conn.prepareStatement(SQLupdateEmployOfDept);


            pstm1.setInt(1,10);
            ResultSet rs = pstm1.executeQuery();

            for(int i =0; i< augmentation.length;i++){
                pstm1.setInt(1,augmentation[i][0]);
                rs = pstm1.executeQuery();
                System.out.println("*** Dept " +augmentation[i][0]+ " avant augmentation :");
                while (rs.next()){
                    System.out.println(rs.getString(1)+" gagne "+rs.getFloat(2));
                }

            }





        }
        catch(SQLException e){
            e.printStackTrace();
            conn.rollback();
        }
        finally {
            if (pstm1 != null) pstm1.close();
            if (conn != null) conn.close();
        }
    }
}