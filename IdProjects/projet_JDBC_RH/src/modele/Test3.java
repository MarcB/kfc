package modele;

import java.sql.*;
import java.io.IOException;

public class Test3 {
    private final static String SQLselectEmployeesOfDept =
            " SELECT ENAME,SAL,COMM,DEPTNO FROM employee "
                +" WHERE DEPTNO = ? "
                +" ORDER BY ENAME ";

    private final static String SQLselectEmployeesSUMOfDept =
            " SELECT DEPTNO,sum(SAL)somme "
                    +" FROM employee "
                    +" group BY DEPTNO "
                    +" ORDER BY SOMME DESC ";

    private final static String SQLupdateEmployeesOfDept=
            " UPDATE employee "
                +" SET SAL= SAL*(1.0+?/100.0) "
                +" WHERE DEPTNO=?";

    public static void main(String[] args) throws SQLException,ClassNotFoundException,java.io.IOException{


    String url = "jdbc:mysql://localhost:3306/myemploye";
    String driver = "com.mysql.jdbc.Driver";
    String user = "marc";
    String pw = "root";

        Class.forName(driver);

    Connection conn = null;
    PreparedStatement pstmSelect = null;
    PreparedStatement pstmUpdate = null;
    PreparedStatement pstmMasse=null;

        try

    {

        Class.forName(driver);
        conn = DriverManager.getConnection(url, user, pw);

        conn.setAutoCommit(false);

        pstmSelect=conn.prepareStatement(SQLselectEmployeesOfDept);
        pstmUpdate=conn.prepareStatement(SQLupdateEmployeesOfDept);

        pstmSelect.setInt(1,30);
        ResultSet rs =pstmSelect.executeQuery();
        //avant
        while( rs.next() ){
            String nom=rs.getString("ENAME");
            Double salaire=rs.getDouble("SAL");
            Double comm=rs.getDouble("COMM");
            String commentaire=(rs.wasNull()?" pas de commission ": " commission de  :"+comm);

            System.out.println(nom+" gagne "+salaire+commentaire);
        }

        //update

        pstmUpdate.setDouble(1,5);

        pstmUpdate.setInt(2,30);

        int nb= pstmUpdate.executeUpdate();

        conn.setAutoCommit(false);

        System.out.println("--------fin update -----------");

        //apres

        pstmSelect.setInt(1,30);
        rs=pstmSelect.executeQuery();

        while( rs.next() ){
            String nom=rs.getString("ENAME");
            Double salaire=rs.getDouble("SAL");
            Double comm=rs.getDouble("COMM");
            String commentaire=(rs.wasNull()?" pas de commission ": " commission de  :"+comm);

            System.out.println(nom+" gagne "+salaire+commentaire);


        }

        pstmMasse=conn.prepareStatement(SQLselectEmployeesSUMOfDept);

        ResultSet rs2=pstmMasse.executeQuery();

        System.out.println("------select summ-----");

        if (rs2.next()) {
            int numeroDept=rs2.getInt("DEPTNO");
            int somm=rs2.getInt("somme");
            System.out.println("Departement:"+numeroDept+" somme total :  "+somm +"\n");
            //insertion nuveau parametre de departement
            pstmSelect.setInt(1 , numeroDept);
            //nouveau select
            rs=pstmSelect.executeQuery();

            while(rs.next()){
                String nom=rs.getString("ENAME");
                Double salaire =rs.getDouble("SAL");

                System.out.println(nom+" gagne :"+salaire);
            }
        }

        conn.commit();
        conn.rollback();

    } catch (SQLException ex) {
        ex.printStackTrace();
        conn.rollback();

    }finally {
            if (pstmSelect !=null){
                conn.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }
}
