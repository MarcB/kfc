package modele;

import java.util.*;
import java.io.IOException;
import java.sql.*;

public class Test2 {
    private static String SQLajoutEmploye=
            " INSERT INTO employee (EMPNO,ENAME,SAL,DEPTNO) "
                +" VALUES(6789,'BELIVIER',500,10)";
    private static String SQLajoutEmploye2=
            " INSERT INTO employee (EMPNO,ENAME,SAL,DEPTNO) "
                    +" VALUES(6790,'TOLIVIER',500,10)";
    private static String SQLajoutEmploye3=
            " INSERT INTO employee (EMPNO,ENAME,SAL,DEPTNO) "
                    +" VALUES(6791,'MARCOLIV',500,10)";

    private static String SQLmodificationEmploye =
                /*"UPDATE employee e,"
                +"(select count(distinct sal) id from employee where sal < sal) "
                +" as test set sal = sal *0.9 where 2>= test.id";*/

              /* " UPDATE employee emp "
                +" SET SAL = SAL * 1.1 "
                +" WHERE SAL is not null "
                +" AND 2 >= (select count(distinct SAL) from emplpoyee where (SAL <emp.SAL) )";*/


            "UPDATE employee e,\n" +
                    "(select e2.EMPNO as id,\n" +
                    "(select count(distinct sal) FROM employee e3 where e3.sal < e2.sal)as num FROM employee e2) as test\n" +
                    "set sal = sal *1.1\n" +
                    "where 2>= test.num\n" +
                    "and test.id=e.EMPNO;\n";



    public static void main(String[] args) throws SQLException,ClassNotFoundException,java.io.IOException {
        String url = "jdbc:mysql://localhost:3306/myemploye";
        String driver = "com.mysql.jdbc.Driver";
        String user = "marc";
        String pw = "root";

        Class.forName(driver);

        Connection conn = null;
        Statement stmt = null;

        try {

            Class.forName(driver);
            conn = DriverManager.getConnection(url, user, pw);

            conn.setAutoCommit(false);

            stmt = conn.createStatement();

            int nbLignes = stmt.executeUpdate((SQLajoutEmploye));
            System.out.println(nbLignes);

            int nbLignes2 = stmt.executeUpdate((SQLajoutEmploye2));
            System.out.println(nbLignes);

            int nbLignes3 = stmt.executeUpdate((SQLajoutEmploye3));
            System.out.println(nbLignes);

            nbLignes = stmt.executeUpdate((SQLmodificationEmploye));

            conn.commit();
            conn.rollback();

        } catch (SQLException ex) {
           ex.printStackTrace();
           conn.rollback();

        }finally{
            if(stmt !=null) {
                stmt.close();
            }
            if(conn !=null){
                conn.close();
            }

        }
    }
}
