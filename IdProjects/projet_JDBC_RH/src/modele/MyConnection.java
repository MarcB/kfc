package modele;

import java.io.IOException;
import java.util.*;
import java.sql.*;

public class MyConnection {
    public void test1() throws Exception,ClassNotFoundException,java.io.IOException {

        String url = "jdbc:mysql://localhost:3306/myemploye";
        String driver = "com.mysql.jdbc.Driver";
        String user = "marc";
        String pw = "root";

        Connection conn = null;
        Statement stmt = null;

        try {

            Class.forName(driver);
            //conn=DriverManager.getConnection(url,user,pw);

            //conn.setAutoCommit(false);

            conn=Connex.getInstance();

            stmt=conn.createStatement();

            ResultSet resul=stmt.executeQuery(
                    "select dname,count(*) as nbre "
                            + " from employee e "
                            + " inner join DEPT d "
                            + " on e.deptno = d.deptno "
                            + " group by dname "
                            + " order BY dname ");


            while (resul.next()) {

                System.out.println(resul.getString("dname")+" a "+resul.getInt("nbre")+" employes");
            }
        } finally{
            if(stmt !=null) {
                stmt.close();
            }
         /*   if(conn !=null){
                conn.close();
            }           */
        }

    }

    public static void main(String[] args) throws Exception {
        MyConnection cn= new MyConnection();
        try{
            cn.test1();

        }catch(ClassNotFoundException | SQLException | IOException e){
            e.printStackTrace();
        }
    }


}
