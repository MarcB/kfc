package modele;

import java.sql.*;

import com.mysql.jdbc.jdbc2.optional.*;

public class Connex {

    private final static String SERVER_NAME = "localhost";
    private final static int PORT = 3306;
    private final static String DATABASE_NAME = "myemploye";
    private final static String USER = "marc";
    private final static String PW = "root";
    private static Connection connect;

    private Connex() {
        try {
            MysqlConnectionPoolDataSource dataSource = new MysqlConnectionPoolDataSource();
            dataSource.setUser(USER);
            dataSource.setPassword(PW);
            dataSource.setPort(PORT);
            dataSource.setServerName(SERVER_NAME);
            dataSource.setDatabaseName(DATABASE_NAME);

            connect = dataSource.getConnection();
        } catch (SQLException e){
                e.printStackTrace();
            }
        }


    public static Connection getInstance() {
        if (connect == null) {
            new Connex();
        }
        return connect;
    }
}

