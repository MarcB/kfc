package com.pop.tacoclient.web;

import com.pop.tacoclient.model.Ingredient;
import com.pop.tacoclient.model.Order;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.client.RestTemplate;


import javax.validation.Valid;

@Controller
@RequestMapping("/orders")
@SessionAttributes("order")
public class OrderController {
  
//  private OrderRepository orderRepo;
//
//  public OrderController(OrderRepository orderRepo) {
//    this.orderRepo = orderRepo;
//  }
//
    String URL_SERVER = "http://localhost:8080/";

  @GetMapping("/current")
  public String orderForm() {
    return "orderForm";
  }

  @PostMapping
  public String processOrder(@Valid Order order, Errors errors, SessionStatus sessionStatus) {
    if (errors.hasErrors()) {
      return "orderForm";
    }

        Order newOrder = new Order(order.getId(),order.getCcCVV(),order.getCcExpiration(),order.getCcNumber(),
            order.getDeliveryCity(),order.getDeliveryName(),order.getDeliveryState(),order.getDeliveryStreet(),
            order.getDeliveryZip(),order.getPlacedAt());

    RestTemplate restTemplate = new RestTemplate();

    // Data attached to the request.
    HttpEntity<Order> requestBody = new HttpEntity<>(newOrder);

    // Send request with POST method.
    ResponseEntity<Order> result
            = restTemplate.postForEntity(URL_SERVER+"/order/create", requestBody, Order.class);




    sessionStatus.setComplete();

    return "redirect:/";
  }

}
