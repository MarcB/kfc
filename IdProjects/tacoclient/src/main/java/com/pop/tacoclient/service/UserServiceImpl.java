//package com.pop.tacoclient.service;
//
//import com.pop.tacoclient.model.Ingredient;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.client.RestTemplate;
//
//public class UserServiceImpl implements UserService{
//
//    String URL_SERVER = "http://localhost:8080/";
//
//    @GetMapping
//    public String showDesignForm(Model model){
//
//        Ingredient.Type[] types=Ingredient.Type.values();
//        for(Ingredient.Type type:types) {
//            RestTemplate restTemplate = new RestTemplate();
//            Ingredient[] ingredientsByType = restTemplate.getForObject(URL_SERVER + type.toString(), Ingredient[].class);
//            model.addAttribute(type.toString().toLowerCase(), ingredientsByType);
//        }
//        return "design";
//    }
//}
