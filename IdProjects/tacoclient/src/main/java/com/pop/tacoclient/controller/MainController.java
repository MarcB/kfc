//package com.pop.tacoclient.controller;
//
//
//import com.pop.tacoclient.service.UserService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//
//
//@Controller
//public class MainController {
//
//    @Autowired
//    private UserService userService;
//
//    // Injectez (inject) via application.properties.
//    @Value("${welcome.message}")
//    private String message;
//
//    @Value("${error.message}")
//    private String errorMessage;
//
//    @RequestMapping(value = { "/", "/index" }, method = RequestMethod.GET)
//    public String index(Model model) {
//        model.addAttribute("message", message);
//        return "index";
//    }
//
//    @RequestMapping(value = { "design" }, method = RequestMethod.GET)
//    public String showDesignForm(Model model) {
//        model.addAttribute("message", message);
//        return "index";
//    }
//
//}
