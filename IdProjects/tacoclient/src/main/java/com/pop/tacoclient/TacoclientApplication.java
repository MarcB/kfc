package com.pop.tacoclient;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class TacoclientApplication {

    public static void main(String[] args) {
        SpringApplication.run(TacoclientApplication.class, args);
    }

}
