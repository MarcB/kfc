package popschool.springjdbctuto.dao;

import popschool.springjdbctuto.model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Repository
public class JdbcBookRepository implements BookRepository {

    // Spring Boot will create and configure DataSource and JdbcTemplate
    // To use it, just @Autowired

    @Autowired
    private  JdbcTemplate jdbcTemplate;

    @Override
    public int count() {
        return jdbcTemplate
                .queryForObject("select count(*) from books", Integer.class);
    }

    @Override
    public int save(Book book) {
        return jdbcTemplate.update(
                "insert into books (name, price) values(?,?)",
                book.getName(), book.getPrice());
    }

    @Override
    public int update(Book book) {
        return jdbcTemplate.update(
                "update books set price = ? where id = ?",
                book.getPrice(), book.getId());
    }


    @Override
    public int deleteById(Long id) {
        return jdbcTemplate.update(
                "delete books where id = ?",
                id);
    }

    @Override
    public List<Book> findAll() {
        return jdbcTemplate.query(
                "select * from books",
                    this::mapBook
                 );
    }

    protected Book mapBook(ResultSet rs, int row) throws SQLException {
        return  new Book(
                 rs.getLong("id"),
                 rs.getString("name"),
                 rs.getBigDecimal("price")
        );
    }

    // jdbcTemplate.queryForObject, populates a single object
    @Override
    public Optional<Book> findById(Long id) {
        return jdbcTemplate.query(
                "select * from books where id = ?",
                this::resultSetExtractorOptionalBook,
                 id
        );
    }
    protected Optional<Book> resultSetExtractorOptionalBook(ResultSet rs) throws SQLException {
        if(rs.next()) {
            return Optional.of(new Book(
                    rs.getLong("id"),
                    rs.getString("name"),
                    rs.getBigDecimal("price")
            ));
        }
        return Optional.ofNullable(null);
    }

    @Override
    public List<Book> findByNameAndPrice(String name, BigDecimal price) {
        return jdbcTemplate.query(
                "select * from books where name like ? and price <= ?",
                this::mapBook,
                "%" + name + "%", price
        );
    }

    @Override
    public String getNameById(Long id) {
        return jdbcTemplate.queryForObject(
                "select name from books where id = ?",
                new Object[]{id},
                String.class
        );
    }

}