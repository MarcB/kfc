DROP TABLE books IF EXISTS ;
CREATE TABLE books(
                id SERIAL, name VARCHAR(255), price NUMERIC(15, 2));