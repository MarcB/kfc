package modele;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "detail_cde", schema = "my_ecommerce", catalog = "")
public class DetailCdeEntity {
    private int id;
    private int qte;
    private Integer cdeId;
    private Integer itemId;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "qte")
    public int getQte() {
        return qte;
    }

    public void setQte(int qte) {
        this.qte = qte;
    }

    @Basic
    @Column(name = "cde_id")
    public Integer getCdeId() {
        return cdeId;
    }

    public void setCdeId(Integer cdeId) {
        this.cdeId = cdeId;
    }

    @Basic
    @Column(name = "item_id")
    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DetailCdeEntity that = (DetailCdeEntity) o;
        return id == that.id &&
                qte == that.qte &&
                Objects.equals(cdeId, that.cdeId) &&
                Objects.equals(itemId, that.itemId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, qte, cdeId, itemId);
    }
}
