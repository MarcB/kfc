package modele;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "payment", schema = "my_ecommerce", catalog = "")
public class PaymentEntity {
    private int id;
    private BigDecimal montant;
    private Date datePaiement;
    private Integer clientId;
    private String typePayment;
    private String creditCard;
    private Date creditCardExpiration;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "montant")
    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    @Basic
    @Column(name = "date_paiement")
    public Date getDatePaiement() {
        return datePaiement;
    }

    public void setDatePaiement(Date datePaiement) {
        this.datePaiement = datePaiement;
    }

    @Basic
    @Column(name = "client_id")
    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    @Basic
    @Column(name = "type_payment")
    public String getTypePayment() {
        return typePayment;
    }

    public void setTypePayment(String typePayment) {
        this.typePayment = typePayment;
    }

    @Basic
    @Column(name = "credit_card")
    public String getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(String creditCard) {
        this.creditCard = creditCard;
    }

    @Basic
    @Column(name = "credit_card_expiration")
    public Date getCreditCardExpiration() {
        return creditCardExpiration;
    }

    public void setCreditCardExpiration(Date creditCardExpiration) {
        this.creditCardExpiration = creditCardExpiration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PaymentEntity that = (PaymentEntity) o;
        return id == that.id &&
                Objects.equals(montant, that.montant) &&
                Objects.equals(datePaiement, that.datePaiement) &&
                Objects.equals(clientId, that.clientId) &&
                Objects.equals(typePayment, that.typePayment) &&
                Objects.equals(creditCard, that.creditCard) &&
                Objects.equals(creditCardExpiration, that.creditCardExpiration);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, montant, datePaiement, clientId, typePayment, creditCard, creditCardExpiration);
    }
}
