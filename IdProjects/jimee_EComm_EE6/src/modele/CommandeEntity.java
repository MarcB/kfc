package modele;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "commande", schema = "my_ecommerce", catalog = "")
public class CommandeEntity {
    private int id;
    private Date datecde;
    private Integer clientId;
    private Integer paymentId;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "datecde")
    public Date getDatecde() {
        return datecde;
    }

    public void setDatecde(Date datecde) {
        this.datecde = datecde;
    }

    @Basic
    @Column(name = "client_id")
    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    @Basic
    @Column(name = "payment_id")
    public Integer getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Integer paymentId) {
        this.paymentId = paymentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommandeEntity that = (CommandeEntity) o;
        return id == that.id &&
                Objects.equals(datecde, that.datecde) &&
                Objects.equals(clientId, that.clientId) &&
                Objects.equals(paymentId, that.paymentId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, datecde, clientId, paymentId);
    }
}
