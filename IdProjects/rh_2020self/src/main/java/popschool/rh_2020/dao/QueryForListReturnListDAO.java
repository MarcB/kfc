package popschool.rh_2020.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;


@Repository
// @Repository is a Spring annotation that indicates that the decorated
// class is a repository. A repository is a mechanism for encapsulating storage,
// retrieval, and search behavior which emulates a collection of objects. It is a
// specialization of the @Component annotation allowing for implementation classes to be
// autodetected through classpath scanning.
//
//@ComponentScan ensures that the classes decorated with @Component and their derivatives
// including @Repository are found and registered as Spring beans. @ComponentScan is automatically
// included with @SpringBootApplication.

    public class QueryForListReturnListDAO {
        @Autowired
                //The @Autowired annotation provides more fine-grained control
                // over where and how autowiring should be accomplished. The @Autowired
                // annotation can be used to autowire bean on the setter method just like @Required
                // annotation, constructor, a property or methods with arbitrary names and/or multiple arguments.

                JdbcTemplate jdbcTemplate;

        //The JDBC template is the main API through which we'll access most of the functionality
        // that we're interested in:
        //
        //    -creation and closing of connections
        //    -executing statements and stored procedure calls
        //    -iterating over the ResultSet and returning results

        public List<String> getDeptNames(){
            String sql ="SELECT d.dname FROM Dept d";
            return jdbcTemplate.queryForList(sql, String.class);
        }

        public List<String> getDeptNames(String searchName){
            String sql ="SELECT d.dname FROM Dept d WHERE d.name like ?";
            return jdbcTemplate.queryForList(sql, String.class, "%"+searchName+"%");
        }

    }