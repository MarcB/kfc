package popschool.rh_2020.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class QueryWithRowMapperDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private final static String BASE_SQL = "SELECT d.DEPTNO, d.DNAME, d.loc from Dept d";

    public List<Department> queryDepartmentV2(){
        String sql = BASE_SQL + "Where d.DEPTNO > ?";

        DepartmentRowMapper rowMapper = new DepartmentRowMapper();

        List<Department> list = jdbcTemplate.query(sql, rowMapper, 20);
        //String sql = "SELECT d.DEPTNO, d.DNAME, d.loc from Dept d Where d.DEPTNO > ?"
        return list;

        //RowMapper interface allows to map a row of the relations with the instance of
        // user-defined class. It iterates the ResultSet internally and adds it into the
        // collection. So we don't need to write a lot of code to fetch the records as ResultSetExtractor.
        //Advantage of RowMapper over ResultSetExtractor
        //
        //RowMapper saves a lot of code because it internally adds the data of ResultSet into the collection.

    }

    private static final class DepartmentRowMapper implements RowMapper<Department> {
        @Override
        public Department mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Department(rs.getInt("DEPTNO"),rs.getString("DNAME"),rs.getString("LOC"));
            //The Java JDBC ResultSet interface represents the result of a database query.
            // The text about queries shows how the result of a query is returned as a java.sql.ResultSet.
            // This ResultSet is then iterated to inspect the result.

        }
    }


}

