package popschool.rh_2020.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import popschool.rh_2020.dao.QueryForListReturnListDAO;

import java.util.List;

public class QueryForListReturnListDAO_test {

    @Component
    public class QueryForListReturnListDAO_test implements CommandLineRunner {
        private final QueryForListReturnListDAO dao;

        @Autowired
        public popschool.rh_2020.test.QueryForListReturnListDAO_test(
        QueryForListReturnListDAO DAO);

        {
            this.dao = dao;
        }

        @Override
        public void run(String ...args) throws Exception {
            List<String> names = dao.getDeptNames("A");

            for (String name : names) {
                System.out.println("Dept Name:" + name);
            }


        }
    }

}
