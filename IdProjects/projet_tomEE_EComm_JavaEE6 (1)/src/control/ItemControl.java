package control;

import modele.Item;
import service.ItemSS;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import java.util.*;

@ManagedBean(name = "itemCtrl")
public class ItemControl {

    @EJB
    private ItemSS facadeItem;

    public List<Item> getEnsItems(){
        return facadeItem.findAllItems();
    }
}
