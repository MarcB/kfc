package modele;

import service.ServiceItem;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@NamedQueries({
        @NamedQuery( name="Item.findAll",
                query="SELECT item FROM Item item ORDER BY item.categorie, item.titre"
        ),
        @NamedQuery( name="Item.findById",
                query="SELECT item FROM Item item WHERE item.id=:id"
        )
})
public class Item implements Serializable {
    private int id;
    private String categorie;
    private String titre;
    private Double prix;
    private int codeBarre;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "categorie")
    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    @Basic
    @Column(name = "titre")
    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    @Basic
    @Column(name = "prix")
    public Double getPrix() {
        return prix;
    }

    public void setPrix(Double prix) {
        this.prix = prix;
    }

    @Basic
    @Column(name = "code_barre")
    public int getCodeBarre() {
        return codeBarre;
    }

    public void setCodeBarre(int codeBarre) {
        this.codeBarre = codeBarre;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return id == item.id &&
                codeBarre == item.codeBarre &&
                Objects.equals(categorie, item.categorie) &&
                Objects.equals(titre, item.titre) &&
                Objects.equals(prix, item.prix);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, categorie, titre, prix, codeBarre);
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", categorie='" + categorie + '\'' +
                ", titre='" + titre + '\'' +
                ", prix=" + prix +
                ", codeBarre=" + codeBarre +
                '}';
    }

    public Item() {
    }


    public Item(String  categorie, String titre, Double prix, int codeBarre) {
        this.setCategorie(categorie);
        this.setTitre(titre);
        this.setPrix(prix);
        this.setCodeBarre(codeBarre);
    }

}
