package util;

import java.io.Serializable;

public class CardExpiredException extends Exception implements Serializable
{
    public CardExpiredException(String message)
    {
        super(message);
    }
    public CardExpiredException()
    {
        super();
    }
}
