

package popschool.rh_2020.dao;
//data access object (DAO) is a pattern that provides an abstract
// interface to some type of database or other persistence mechanism.
// By mapping application calls to the persistence layer, the DAO provides
// some specific data operations without exposing details of the database.
// This isolation supports the single responsibility principle.
// It separates what data access the application needs, in terms of domain-specific
// objects and data types (the public interface of the DAO), from how these needs can
// be satisfied with a specific DBMS, database schema, etc. (the implementation of the DAO).

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
    // @Repository is a Spring annotation that indicates that the decorated
    // class is a repository. A repository is a mechanism for encapsulating storage,
    // retrieval, and search behavior which emulates a collection of objects. It is a
    // specialization of the @Component annotation allowing for implementation classes to be
    // autodetected through classpath scanning.
    //
    //@ComponentScan ensures that the classes decorated with @Component and their derivatives
    // including @Repository are found and registered as Spring beans. @ComponentScan is automatically
    // included with @SpringBootApplication.

public class QueryForListReturnListDAO {
    @Autowired
            //The @Autowired annotation provides more fine-grained control
            // over where and how autowiring should be accomplished. The @Autowired
            // annotation can be used to autowire bean on the setter method just like @Required
            // annotation, constructor, a property or methods with arbitrary names and/or multiple arguments.

    JdbcTemplate jdbcTemplate;
        //The JDBC template is the main API through which we'll access most of the functionality
        // that we're interested in:
        //
        //    -creation and closing of connections
        //    -executing statements and stored procedure calls
        //    -iterating over the ResultSet and returning results

    //list de dept all
    public List<String> getDeptNames(){
        String sql = "Select d.dname from Dept d ";

        //QueryForList (String sql, Class<T> elementType)
        List<String> list = jdbcTemplate.queryForList(sql, String.class);

        return list;
    }

    //list de dept par mot clef
    public List<String> getDeptNames(String searchName){
        String sql = "Select d.dname from Dept d " +
                "Where d.dname like ? ";

        //queryForList(String sal, Class<T> elementType,
        List<String> list = jdbcTemplate.queryForList(sql, String.class, "%"+searchName+"%");

        return list;

    }

}
