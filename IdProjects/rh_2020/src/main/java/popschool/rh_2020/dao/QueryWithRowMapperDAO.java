package popschool.rh_2020.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import popschool.rh_2020.model.Department;
import popschool.rh_2020.model.EmpDto;
import popschool.rh_2020.model.Employee;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class QueryWithRowMapperDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    //variable string requete dept
    private static final String Base_SQL = "Select d.deptno, d.dname, d.loc from dept d " ;
    //variable string requete employe
    private static final String Base_SQL_Emp = "Select e.ename, e.job,e.sal, d.dname from employee e " +
            "JOIN dept d ON e.deptno = d.deptno " ;


    //exo 3 recupérer une liste des info des departement et la liste de leurs employees
    private static final String Base_SQL_Ex3 = "Select deptno, dname, loc, empno, ename, job, sal " +
            "from employee e " +
            "inner join dept d " +
            "on e.deptno = d.deptno ";
    //empdto
//    private int empno;
//    private String ename;
//    private String job;
//    private Double salaire;

    //Map pour liste des departement
    private Map<Department, List<EmpDto>>extractDeptInfo(ResultSet rs) throws SQLException {
        Map<Department, List<EmpDto>>map=new HashMap<>();
        //Map sur le couple dept,employe
        while(rs.next()){
            //tant que rs exist un suivant
            Department cle = new Department(rs.getInt(1), rs.getString(2), rs.getString(3));
            //clef objet (tri valeur de table dept :id nom loca)du du couple map
            EmpDto valeur = new EmpDto(rs.getInt(4), rs.getString(5), rs.getString(6),rs.getDouble(7));
            //valeur objet de employe data transfert object
            if (map.containsKey(cle)){
                map.get(cle).add(valeur);
                //si la map contient la clef,ajoute valeur au niveau de la clef
            }else{
                List<EmpDto> list = new ArrayList<>();
                list.add(valeur);
                map.put(cle, list);
                //sinon ajouter le tableau liste au niveau valeur de la cle
            }
        }
        return map;
    }
    //    setNameMap(Map<Integer, String> nameMap)
    public Map<Department,List<EmpDto>>queryDeptInfo(){
        String sql = "Select e.deptno, dname, loc, empno, ename, job, sal " +
                "from employee e " +
                "inner join dept d " +
                "on e.deptno = d.deptno ";
        Map<Department,List<EmpDto>>map=jdbcTemplate.query(sql, this::extractDeptInfo);
        //rempli la map avec le couple requete sql sur la jointure dept et employe et la classe
        // d'extraction (applique le select ensuite extract fait le tri du couple)
        return map;
        //The double colon (::) operator, also known as method reference operator in Java,
        // is used to call a method by referring to it with the help of its class directly.
        // They behave exactly as the lambda expressions. The only difference it has from
        // lambda expressions is that this uses direct reference to the method by name
        // instead of providing a delegate to the method.
        //
        //Syntax:
        //<Class name>::<method name>
    }

    //1ere méthode
    public List<Department> queryDepartmentV2(){
        String sql = Base_SQL + "Where d.deptno>?";

        DepartmentRowMapper rowMapper = new DepartmentRowMapper();

        List<Department> list = jdbcTemplate.query(sql, rowMapper, 20);
        return list;
    }

    private static final class DepartmentRowMapper implements RowMapper<Department>{
        @Override
        public Department mapRow(ResultSet rs, int rowNum) throws SQLException{
            return new Department(rs.getInt("deptno"),
                    rs.getString("dname"),
                    rs.getString("loc"));
        }
    }


    //utilisation des lambdas java8  permet de ne pas créer la class DepartmentRowMapper
    public List<Department> queryDepartmentV3(){
        String sql = Base_SQL + "Where d.deptno>?";

        List<Department> list = jdbcTemplate.query(sql,
                (rs, rowNum)->{
                    return new Department(rs.getInt("deptno"),
                            rs.getString("dname"),
                            rs.getString("loc"));
                },
                20);
        return list;
    }

    //utilisation des lambdas java8  reference de methode
    public List<Department> queryDepartment() {
        String sql = Base_SQL + "Where d.deptno>?";

        List<Department> list = jdbcTemplate.query(sql, this::mapDepartment, 20);
        return list;
    }

    private Department mapDepartment(ResultSet rs, int row) throws SQLException {
            return new Department(rs.getInt("deptno"),
                    rs.getString("dname"),
                    rs.getString("loc"));
    }

    //exercice 1 rechercher le nom, le job, le salaire et le departement de tous les employées avec la methode map row v3
    public List<Employee> queryEmployesV3(){
        String sql = Base_SQL_Emp;

        List<Employee> list = jdbcTemplate.query(sql,
                (rs, rowNum)->{
                    return new Employee(rs.getString("ename"),
                            rs.getString("job"),
                            rs.getDouble("sal"),
                            rs.getString("dname"));
                });
        return list;
    }

    //exercice 2 rechercher le nom, le job, le salaire et le departement de tous les
    // employées en fonction du nom du departement avec la methode map row v3
    public List<Employee> queryEmployesV3_2(){
        String sql = Base_SQL_Emp + " Where d.dname=?";

        List<Employee> list = jdbcTemplate.query(sql,
                (rs, rowNum)->{
                    return new Employee(rs.getString("ename"),
                            rs.getString("job"),
                            rs.getDouble("sal"),
                            rs.getString("dname"));
                }, "SALES");
        return list;
    }

    public List<Department> queryDepartmentV4(){
        String sql = Base_SQL + " Where d.deptno > ?";
        List<Department> list = jdbcTemplate.query(sql, this::mapDepartmentv4, 20);
        return list;
    }
    private Department mapDepartmentv4(ResultSet rs, int row) throws SQLException{
        return new Department(rs.getInt("deptno"),rs.getString("dname"),rs.getString("loc"));
    }

}
