package popschool.rh_2020.model;

import java.util.Objects;

//In the field of programming a data transfer object (DTO)
// is an object that carries data between processes.
// The motivation for its use is that communication between processes
// is usually done resorting to remote interfaces (e.g., web services),
// where each call is an expensive operation. Because the majority of
// the cost of each call is related to the round-trip time between
// the client and the server, one way of reducing the number of calls is
// to use an object (the DTO) that aggregates the data that would have been
// transferred by the several calls, but that is served by one call only.
public class EmpDto {
    private int empno;
    private String ename;
    private String job;
    private Double salaire;

    //    private int empno;
    //    private String ename;
    //    private String job;
    //    private int mgr;
    //    private Double sal;
    //    private int comm;
    //    private int deptno;
    //    private String dname;

    //----------------------------------- GETTER SETTER --------------------------------------------------
    public int getEmpno() {
        return empno;
    }

    public void setEmpno(int empno) {
        this.empno = empno;
    }

    public String getEname() {
        return ename;
    }

    public void setEname(String ename) {
        this.ename = ename;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public Double getSalaire() {
        return salaire;
    }

    public void setSalaire(Double salaire) {
        this.salaire = salaire;
    }

    //--------------------------------------- CONSTRUCTEUR -----------------------------------------------------
    public EmpDto(int empno, String ename, String job, Double salaire) {
        this.empno = empno;
        this.ename = ename;
        this.job = job;
        this.salaire = salaire;
    }
    //constructeur vide
    public EmpDto(){
    }

    @Override
    public String toString() {
        return "EmpDto{" +
                "empno=" + empno +
                ", ename='" + ename + '\'' +
                ", job='" + job + '\'' +
                ", salaire=" + salaire +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EmpDto)) return false;
        EmpDto empDto = (EmpDto) o;
        return empno == empDto.empno &&
                ename.equals(empDto.ename) &&
                job.equals(empDto.job) &&
                salaire.equals(empDto.salaire);
    }

    @Override
    public int hashCode() {
        return Objects.hash(empno, ename, job, salaire);
    }
}
