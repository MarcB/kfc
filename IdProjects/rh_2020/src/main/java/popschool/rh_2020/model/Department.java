package popschool.rh_2020.model;

import java.util.Objects;

public class Department {

    private int deptno;
    private String dname;
    private String loc;

    //constructeur vide
    public Department(){

    }
    //contructeur
    public Department(Integer deptno, String dname, String loc){
        this.deptno=deptno;
        this.dname=dname;
        this.loc=loc;
    }
    //getter setter
    public int getDeptno() {
        return deptno;
    }

    public void setDeptno(int deptno) {
        this.deptno = deptno;
    }

    public String getDname() {
        return dname;
    }

    public String getLoc() {
        return loc;
    }

    //string methode
    @Override
    public String toString() {
        return "Department{" +
                "deptno=" + deptno +
                ", dname='" + dname + '\'' +
                ", loc='" + loc + '\'' +
                '}';
    }

    //
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Department)) return false;
        Department that = (Department) o;
        return deptno == that.deptno &&
                dname.equals(that.dname) &&
                loc.equals(that.loc);
    }

    //
    @Override
    public int hashCode() {
        return Objects.hash(deptno, dname, loc);
    }
}
