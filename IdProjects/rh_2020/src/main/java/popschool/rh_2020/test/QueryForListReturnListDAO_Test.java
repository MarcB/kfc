package popschool.rh_2020.test;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import popschool.rh_2020.dao.QueryForListReturnListDAO;

import java.util.List;

@Component
//In most typical applications, we have distinct layers like data access,
// presentation, service, business, etc.
//
//And, in each layer, we have various beans. Simply put, to detect them automatically,
// Spring uses classpath scanning annotations.
//
//Then, it registers each bean in the ApplicationContext.
//
//Here's a quick overview of a few of these annotations:
//
//    @Component is a generic stereotype for any Spring-managed component
//    @Service annotates classes at the service layer
//    @Repository annotates classes at the persistence layer, which will act as a database repository

public class QueryForListReturnListDAO_Test implements CommandLineRunner {
    //CommandLineRunner is an interface used to indicate that a bean
    // should run when it is contained within a SpringApplication.
    // A Spring Boot application can have multiple beans implementing
    // CommandLineRunner. These can be ordered with @Order.

    // The following application demonstrates the usage of CommandLineRunner.
// It creates cities in a H2 in-memory database and later lists them.
//
//pom.xml
//src
//├───main
//│   ├───java
//│   │   └───com
//│   │       └───zetcode
//│   │           │   Application.java
//│   │           │   MyRunner.java
//│   │           ├───model
//│   │           │       City.java
//│   │           └───repository
//│   │                   CityRepository.java
//│   └───resources
//│           application.properties
//└───test
//    └───java
//
//This is the project structure.

    private final QueryForListReturnListDAO dao;

    @Autowired
    public QueryForListReturnListDAO_Test(QueryForListReturnListDAO dao){
        this.dao = dao;
    }


    @Override
    public void run(String... args) throws Exception {
        List<String> names = dao.getDeptNames("A");

        //for each
        for (String name : names){
            System.out.println("Dept Names : " + name);
        }
    }
}

