package popschool.rh_2020.test;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import popschool.rh_2020.dao.QueryForListReturnListDAO;
import popschool.rh_2020.dao.QueryWithRowMapperDAO;
import popschool.rh_2020.model.Department;
import popschool.rh_2020.model.EmpDto;
import popschool.rh_2020.model.Employee;

import java.util.List;
import java.util.Map;

@Component
public class QueryWithRowMapper_Test implements CommandLineRunner {

    @Autowired
    private QueryWithRowMapperDAO dao;



    @Override
    public void run(String... args) throws Exception {
        List<Department> list = dao.queryDepartmentV2();
        List<Department> list1 = dao.queryDepartmentV4();
        List<Employee> list2 = dao.queryEmployesV3_2();

        //for (Department dept : list){
        //    System.out.println("Deptno : " + dept.getDeptno() + "- Deptname : " + dept.getDname());
        //}
        //for (Department dept : list1){
        //    System.out.println("Deptno : " + dept.getDeptno() + "- Deptname : " + dept.getDname());
        //}

        //System.out.println("--------------------Employee------------------------------------");

        //for (Employee employee : list2){
        //    System.out.println("nom : " + employee.getEname() + "- job : " + employee.getJob() + "- salaire : " +
        //            employee.getSal() + " - dname : " + employee.getDname());
        //}

        System.out.println("---------------------- AVEC FOR ----------------------------");
        Map<Department,List<EmpDto>> map = dao.queryDeptInfo();
        for (Map.Entry<Department,List<EmpDto>>assoc:map.entrySet()){
            System.out.println(assoc.getKey());
            for (EmpDto emp:assoc.getValue()){
                System.out.println("--"+emp);
            }
        }

        System.out.println("-----------------------------------------------------------");
        System.out.println("--------------------- AVEC FOREACH ------------------------");
        System.out.println("-----------------------------------------------------------");
            Map<Department, List<EmpDto>> map2 = dao.queryDeptInfo();
            map2.forEach((key, value) -> {
                System.out.println(key);
                value.forEach(e -> System.out.println("  " + e));
            });
    }
}
