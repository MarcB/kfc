package popschool.rh_2020;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Rh2020Application {

    public static void main(String[] args) {
        SpringApplication.run(Rh2020Application.class, args);
    }

}
