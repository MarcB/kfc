package test;

import dao.ActeurRepository;
import model.Acteur;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component

public class ActeurTest implements CommandLineRunner {
    private static final Logger log = LoggerFactory.getLogger(JpacinemaApplication.class);
    @Autowired
    private ActeurRepository repository;
    private void information(Acteur a){
        log.info(a.toString());
    }
    @Override
    public void run(String... args) throws Exception {
        // fetch all customer
        log.info("Acteurs found with findAll();");
        log.info("----------------------");
        for(Acteur acteur : repository.findAll()) {
            log.info(acteur.toString());
        }
        log.info("");

        // fetch an individual customer by ID
        Optional<Acteur> acteur = repository.findById(5L);
        log.info("Act found with findById(1L)");
        log.info("----------------------");
        if(acteur.isPresent()) {
            Acteur act = acteur.get();
            log.info(acteur.get().toString());
            log.info("");
            repository.findAll().forEach(this::information);
            log.info("");
        }
    }
}




