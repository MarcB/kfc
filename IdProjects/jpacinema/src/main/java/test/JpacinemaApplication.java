package test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JpacinemaApplication {

    public static void main(String[] args) {
        SpringApplication.run(JpacinemaApplication.class, args);
    }

}
