package dao;

import model.Acteur;
import org.springframework.data.repository.CrudRepository;

public interface ActeurRepository extends CrudRepository<Acteur,Long> {
}
