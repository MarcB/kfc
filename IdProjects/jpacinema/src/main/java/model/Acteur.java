package model;

import javax.persistence.*;
import java.util.Date;


@Entity

@Table(name="ACTEUR")

public class Acteur {
    @Id
    Integer nacteur;

   String nom;
   String prenom;

   @Temporal(TemporalType.DATE)
   Date naissance;

    Integer nationalite;
    Integer nbreFilms;

    public Acteur() {
    }
    public Acteur(Integer nacteur,String nom,String prenom,Date naissance,
                  Integer nationalite,Integer nbreFilms) {
    this.nacteur=nacteur;
    this.nom=nom;
    this.prenom=prenom;
    this.naissance=naissance;
    this.nationalite=nationalite;
    this.nbreFilms=nbreFilms;
    }

    public Integer getNacteur() {return this.nacteur;    }
    public String getNom() {return this.nom;    }
    public String getPrenom() {return this.prenom;    }
    public Date getNaissance() {return this.naissance;    }
    public Integer getNationalite() {return this.nationalite;    }
    public Integer getNbreFilms() {return this.nbreFilms;    }

    public void setNacteur(Integer nacteur) {this.nacteur = nacteur;    }
    public void setNom(String nom) {this.nom = nom;    }
    public void setPrenom(String prenom) {this.prenom = prenom;    }
    public void setNaissance(Date naissance) {this.naissance = naissance;    }
    public void setNationalite(Integer nationalite) {this.nationalite = nationalite;    }
    public void setNbreFilms(Integer nbreFilms) {this.nbreFilms = nbreFilms;    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof Acteur)) return false;
        final Acteur other = (Acteur) o;
        if (!other.canEqual((Object) this)) return false;
        if (this.getNacteur() != other.getNacteur()) return false;
        final Object this$nom = this.getNom();
        final Object other$nom = other.getNom();
        if (this$nom == null ? other$nom != null : !this$nom.equals(other$nom)) return false;
        final Object this$prenom = this.getPrenom();
        final Object other$prenom = other.getPrenom();
        if (this$prenom == null ? other$prenom != null : !this$prenom.equals(other$prenom)) return false;
        final Object this$naissance = this.getNaissance();
        final Object other$naissance = other.getNaissance();
        if (this$naissance == null ? other$naissance != null : !this$naissance.equals(other$naissance)) return false;
        if (this.getNationalite() != other.getNationalite()) return false;
        if (this.getNbreFilms() != other.getNbreFilms()) return false;
        return true;
    }
    protected boolean canEqual(final Object other) {
        return other instanceof Acteur;
    }

    public String toString() {
        return "\n Acteur(nacteur=" + this.getNacteur() + ", nom=" + this.getNom() + ", prenom=" + this.getPrenom() + ", naissance=" + this.getNaissance() + ", nationalite=" + this.getNationalite() + ", nbreFilms=" + this.getNbreFilms() + ")";
    }
}
