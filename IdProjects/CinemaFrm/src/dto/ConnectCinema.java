package dto;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectCinema {
    //url de connection
    private final static String URL = "jdbc:mysql://localhost:3306/mycinema";
    //utilisatuer de connection
    private final static String USER = "marc";
    //mot de passe de connection
    private final static String PW = "root";

    //SINGLETON
    private static Connection INSTANCE;

    //constructeur privé

    private ConnectCinema() {
        try{
            INSTANCE = DriverManager.getConnection(URL,USER,PW);
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    public static Connection getInstance(){
        if (INSTANCE == null){
            new ConnectCinema();
        }
        return INSTANCE;
    }

    public static void main(String[] args) {
        ConnectCinema.getInstance();
    }
}
