package dto;

import java.io.Serializable;
import java.util.Date;

public class ActeurDTO implements Serializable {
    // ---------------------attributs d'instance-------------------------
    private static final long serialVersionUID = 1L;

    private int nacteur;
    private String nom;
    private String prenom;
    private java.util.Date naissance;
    private String nationalite;
    int nbreFilms;

    //---------------------------G E T T E R -----------------------------


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public int getNacteur() {
        return nacteur;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public Date getNaissance() {
        return naissance;
    }

    public String getNationalite() {
        return nationalite;
    }

    public int getNbreFilms() {
        return nbreFilms;
    }


    //--------------------------------------C O N S T R U C T E U R ------------------------
    public ActeurDTO(int nacteur, String nom, String prenom, Date naissance, String nationalite, int nbreFilms) {
        super();
        this.nacteur = nacteur;
        this.nom = nom;
        this.prenom = prenom;
        this.naissance = naissance;
        this.nationalite = nationalite;
        this.nbreFilms = nbreFilms;
    }

    //---------------------------------------T O S T R I N G------------------------------------------
    @Override
    public String toString() {
        return "ActeurDTO{" +
                "nacteur=" + nacteur +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", naissance=" + naissance +
                ", nationalite='" + nationalite + '\'' +
                ", nbreFilms=" + nbreFilms +
                '}';
    }
}

