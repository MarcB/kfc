package dto;

import java.io.Serializable;
import java.sql.Date;

public class EmpruntDTO implements Serializable {

    private String nom;
    private String prenom;
    private String titre;
    private Date dateEmprunt;

    //GETTER SETTER


    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public Date getDateEmprunt() {
        return dateEmprunt;
    }

    public void setDateEmprunt(Date dateEmprunt) {
        this.dateEmprunt = dateEmprunt;
    }

    //CONSTRUCTEUR


    public EmpruntDTO(String nom, String prenom, String titre, Date dateEmprunt) {
        super();
        this.nom = nom;
        this.prenom = prenom;
        this.titre = titre;
        this.dateEmprunt = dateEmprunt;
    }


}
