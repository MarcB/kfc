package dto;

public class ClientDTO {

    private static final long serialVersionUID = 1L;

    private int nacteur;
    private String nom;
    private String prenom;
    private java.util.Date naissance;
    private String nationalite;
    int nbreFilms;
    private int nfilm ;
    private String titre;
    private String nomRealisateur;
    private String nomActeur;

}
