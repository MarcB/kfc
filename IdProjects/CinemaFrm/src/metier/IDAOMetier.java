package metier;

import dto.*;

import javax.swing.*;
import java.util.*;

public interface IDAOMetier {

    List<GenreDTO> ensGenres();

    long nbreFilmDuGenre(int numGenre);

    List<FilmDTO> ensFilmsDuGenre(int numGenre);

    FilmDTO infoRealisateurEtActeur(int numFilm);

    List<ActeurDTO> ensActeurs();

    List<FilmDTO> ensTitreDunActeur(int numActeur);

    //gestion des emprunts
    List<FilmDTO> ensFilmEmpruntables();

    void createClient(String nom, String prenom, String adresse, long anciennete);

    List<Client> findAllClients();

    void emprunter(int nclient , int nfilm);
    void emprunter(String nom, String prenom, String titre);
    void emprunter(Client client, FilmDTO film);

    void restituer(int nclient, int nfilm);
    void restituer(Client client, FilmDTO film);

    void deleteClient(ClientDTO client);
    void deleteClient(int Client);
}
