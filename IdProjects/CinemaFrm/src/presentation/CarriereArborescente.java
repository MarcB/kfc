package presentation;

import com.sun.tools.javac.Main;
import dto.FilmDTO;
import dto.GenreDTO;
import metier.DAOMetier;
import metier.IDAOMetier;

import javax.swing.*;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeSelectionModel;
import java.lang.*;

public class CarriereArborescente {
    //variable d instance liaison avec la partie metier
    private IDAOMetier cinema;


    private JPanel rootPanel;
    private JTree treeGenre;
    private JTextArea textAreaInfo;

    private TreeModel creationModele() {
        DefaultMutableTreeNode racine =
                new DefaultMutableTreeNode(("Genre"));

        try {
            for (GenreDTO g : cinema.ensGenres()) {
                DefaultMutableTreeNode noeud =
                        new DefaultMutableTreeNode(g);
                racine.add(noeud);
                for (FilmDTO titre : cinema.ensFilmsDuGenre(g.getNgenre())) {
                    noeud.add(new DefaultMutableTreeNode(titre));
                }
            }
        } catch (Exception ex) {
            System.err.println(ex);
            ex.printStackTrace();
        }
        return new DefaultTreeModel(racine);
    }

    public void init() {
        try {
            cinema = DAOMetier.getInstance();
        } catch (Exception e) {
            System.err.println(e);

        }
    }

    public CarriereArborescente() {

        init();

        treeGenre.setModel((creationModele()));
        treeGenre.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        treeGenre.addTreeExpansionListener(new TreeExpansionListener() {


            @Override
            public void treeExpanded(TreeExpansionEvent event) {
                try {
                    DefaultMutableTreeNode noeud =
                            (DefaultMutableTreeNode) event.getPath().getLastPathComponent();


                    Object obj = noeud.getUserObject();
                    if (obj instanceof GenreDTO) {
                        GenreDTO genre = (GenreDTO) obj;


                        textAreaInfo.setText(("Nombre de films du genres" + genre + ": " +
                                cinema.nbreFilmDuGenre((genre.getNgenre()))));
                    }
                } catch (Exception ex) {
                    System.err.println(ex.getMessage());
                }
            }

            @Override
            public void treeCollapsed(TreeExpansionEvent treeExpansionEvent) {
            }

        });

        treeGenre.addTreeSelectionListener(new TreeSelectionListener() {
            @Override
            public void valueChanged(TreeSelectionEvent e) {
                if (treeGenre.getSelectionPath() != null && treeGenre.getSelectionPath().getPathCount() == 3) {
                    DefaultMutableTreeNode noeud = (DefaultMutableTreeNode) treeGenre.getLastSelectedPathComponent();

                    if (noeud.isLeaf()) {

                        try {
                            FilmDTO film = (FilmDTO) noeud.getUserObject();
                            textAreaInfo.setText("Information sur le film: " + film.getTitre() + "\n"
                                    + " Realisateur " + film.getNomRealisateur() + "\n"
                                    + "Acteur principal :" + film.getNomActeur());

                        } catch (Exception ex) {
                            System.err.println(ex.getMessage());
                        }
                    }
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("CarriereArborescente");
        frame.setContentPane(new CarriereArborescente().rootPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
