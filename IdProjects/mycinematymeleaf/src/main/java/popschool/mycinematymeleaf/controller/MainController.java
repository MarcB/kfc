package popschool.mycinematymeleaf.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import popschool.mycinematymeleaf.dao.*;
import popschool.mycinematymeleaf.model.Acteur;
import popschool.mycinematymeleaf.model.Client;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++
//    Index :
//        Consulter les acteurs
//        Consulter les films
//        Emprunter un film
//    Consulter les acteurs :
//        List des acteurs (nom, prenom)
//        choix -> list des films de cet acteur
//    Consulter les films
//        Liste des genres
//        choix -> liste des films de ce genre
//    Emprunter un film
//        Choisir un client et un film
//        Declencher l'emprunt
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++

@Controller
public class MainController {

    @Autowired
    private ActeurRepository acteurRepository;
    @Autowired
    private FilmRepository filmRepository;
    @Autowired
    private EmpruntRepository empruntRepository;
    @Autowired
    private GenreRepository genreRepository;
    @Autowired
    private ClientRepository clientRepository;

    private static List<Acteur> acteurs = new ArrayList<Acteur>();

    // Injectez (inject) via application.properties.
    @Value("${welcome.message}")
    private String message;

    @Value("${error.message}")
    private String errorMessage;

    //index >>  page home =====================================================
    @RequestMapping(value = { "/", "/index" }, method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("message", message);
        return "index";
    }

    //list des acteurs ========================================================
    @RequestMapping(value = { "/acteurList" }, method = RequestMethod.GET)
    public String acteurList(Model model) {
        model.addAttribute("acteurs", acteurRepository.findAll());
        return "acteurList";
    }

    //list des clients =========================================================
    @RequestMapping(value = { "/clientList" }, method = RequestMethod.GET)
    public String clientList(Model model) {
        model.addAttribute("clients", clientRepository.findAll());
        return "clientList";
    }
    //list des emprunts by client(id) ==========================================
    @PostMapping("/voirclient")
    public String emprunts(@RequestParam("clientchoisi")Long nclient, Model model){
        model.addAttribute("empruntsclient", empruntRepository.findByClient_Nclient(nclient));
        //retour list d emprunt par client
        return "empruntListClient";
    }

    //list des emprunts ========================================================
    @RequestMapping(value = { "/empruntList" }, method = RequestMethod.GET)
    public String empruntList(Model model) {
        model.addAttribute("emprunts", empruntRepository.findAll());
        return "empruntList";
    }

    //list des films + combo by genre ==========================================
    @RequestMapping(value = {"/filmList"}, method = RequestMethod.GET)
    public String selectionGenre(Model model) {
        model.addAttribute("genres", genreRepository.findAll());
        model.addAttribute("films", filmRepository.findAll());
        return "filmList";
    }

    //list des films by genre(nature) ==========================================
    @PostMapping("/voirfilm" )
    public String films(@RequestParam("genrechoisi")String nature, Model model){
        model.addAttribute("ensfilmchoisi", filmRepository.findByGenre_Nature(nature));
        return "filmListGenre";
    }
    //list des films by acteur(id) =============================================
    @PostMapping("/voiracteur" )
    public String films(@RequestParam("acteurchoisi")Long nacteur, Model model){
        model.addAttribute("filmsActeur", filmRepository.findByActeur_Nacteur(nacteur));
        return "filmListActeur";
    }



    //**************************************************************************
    //***************            BOOTSTRAP VERSION            ******************
    //**************************************************************************


    //client creation ==========================================================
    @RequestMapping(value = { "/clientCrea" }, method = RequestMethod.GET)
    public String clientCrea(Model model) {
        model.addAttribute("clients", clientRepository.findAll());
        return "clientCrea";
    }

    //bouton lancement page creatio user
    @GetMapping("/signup")
    public String showSignUpForm(Client client) {
        return "add-client";
    }

    //ajout de client
    @PostMapping("/addclient")
    public String addClient(@Valid Client client, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "add-client";
        }
        clientRepository.save(client);
        model.addAttribute("clients", clientRepository.findAll());
        return "clientCrea";
    }
    // edition de l'info client
    @GetMapping("/edit/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model) {
        Client client = clientRepository.findByNclient(id).orElseThrow(() -> new IllegalArgumentException("Invalid client Id:" + id));
            model.addAttribute("client", client);
            return "update-client";
    }

    //update du client
    @PostMapping("/update/{id}")
    public String updateClient(@PathVariable("id") long id, @Valid Client client, BindingResult result, Model model) {
        //si error renvoie l update
        if (result.hasErrors()) {
            client.setNclient(id);
            return "update-client";
        }//sinon
            //re-set du N°client pour save
            client.setNclient(id);
            clientRepository.save(client);
            model.addAttribute("clients", clientRepository.findAll());
            return "clientCrea";
    }

    //delete du client selon id
    @GetMapping("/delete/{id}")
    public String deleteClient(@PathVariable("id") long id, Model model) {
        Client client = clientRepository.findByNclient(id).orElseThrow(() -> new IllegalArgumentException("Invalid client Id:" + id));
        clientRepository.delete(client);
        model.addAttribute("clients", clientRepository.findAll());
        return "clientCrea";
    }

}
