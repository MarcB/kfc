package popschool.mycinematymeleaf.dao;

import org.springframework.data.repository.CrudRepository;

import popschool.mycinematymeleaf.model.Emprunt;

import java.util.List;
import java.util.Optional;

public interface EmpruntRepository extends CrudRepository<Emprunt, Long> {

    List<Emprunt>findByClient_Nclient(Long nclient);
    Optional<Emprunt> findByClient_NomAndFilm_TitreAndRetour(String nom, String titre, String non);
}
