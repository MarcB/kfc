package popschool.mycinematymeleaf.dao;

import org.springframework.data.repository.CrudRepository;
import popschool.mycinematymeleaf.model.Client;

import java.util.List;
import java.util.Optional;


public interface ClientRepository extends CrudRepository<Client,Long> {
    Optional<Client> findByNclient(Long id);
}
