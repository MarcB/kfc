package popschool.mycinematymeleaf.dao;

import org.springframework.data.repository.CrudRepository;
import popschool.mycinematymeleaf.model.Genre;

public interface GenreRepository extends CrudRepository<Genre, Long> {

}
