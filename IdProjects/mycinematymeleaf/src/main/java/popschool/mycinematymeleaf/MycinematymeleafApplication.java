package popschool.mycinematymeleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MycinematymeleafApplication {

    public static void main(String[] args) {
        SpringApplication.run(MycinematymeleafApplication.class, args);
    }

}
