package popschool.mycinematymeleaf.model;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Data
@Entity
@Table(name="acteur")
public class Acteur {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long nacteur;

    @OneToMany(mappedBy = "acteur")
    private List<Film> films = new ArrayList<>();

    private String nom;
    private String prenom;

    @Temporal(TemporalType.DATE)
    private Date naissance;

    //private Long nationalite;

    @ManyToOne
    @JoinColumn(name = "nationalite")
    private popschool.mycinematymeleaf.model.Pays pays;

    private Integer nbrefilms;

    public popschool.mycinematymeleaf.model.Pays getPays() {
        return pays;
    }

    public void setPays(popschool.mycinematymeleaf.model.Pays pays) {
        this.pays = pays;
    }

    protected Acteur(){}

    @Override
    public String toString() {
        return "Acteur{" +
                "nacteur=" + nacteur +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", naissance=" + naissance +
                ", pays=" + pays +
                ", nbrefilms=" + nbrefilms +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof popschool.mycinematymeleaf.model.Acteur)) return false;
        popschool.mycinematymeleaf.model.Acteur acteur = (popschool.mycinematymeleaf.model.Acteur) o;
        return nom.equals(acteur.nom) &&
                prenom.equals(acteur.prenom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nom, prenom);
    }
}
