package popschool.mycinematymeleaf.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name="film")
public class Film {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long nfilm;

    private String titre;

    @ManyToOne
    @JoinColumn(name = "ngenre")
    private popschool.mycinematymeleaf.model.Genre genre;

    @Temporal(TemporalType.DATE)
    private Date sortie;

    @ManyToOne
    @JoinColumn(name = "npays")
    private popschool.mycinematymeleaf.model.Pays pays;

    private String realisateur;

    @ManyToOne
    @JoinColumn(name ="nacteurprincipal")
    private popschool.mycinematymeleaf.model.Acteur acteur;

    private Long entrees;

    private String oscar;

    @OneToMany(mappedBy = "film")
    private List<popschool.mycinematymeleaf.model.Emprunt> emprunt;

    @Override
    public String toString() {
        return "Film{" +
                "nfilm=" + nfilm +
                ", titre='" + titre + '\'' +
                ", genre=" + genre +
                ", sortie=" + sortie +
                ", pays=" + pays +
                ", realisateur='" + realisateur + '\'' +
                ", acteur=" + acteur +
                ", entrees=" + entrees +
                ", oscar='" + oscar + '\'' +
                '}';
    }
}
