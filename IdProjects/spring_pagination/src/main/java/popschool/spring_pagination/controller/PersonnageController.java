package popschool.spring_pagination.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import popschool.spring_pagination.model.personnages;
import popschool.spring_pagination.dao.PersonnageDao;

//affiche list perso par
// trie par nom
//d un sexe donné ---
//d un genre donné ---
//d un genre donné et trie par nom

//liste albums
//trie par titre
//apparetenant a un intervalle  d année trié par nom



@Controller
public class PersonnageController {
    @Autowired
    private PersonnageDao personnageDao;

    @GetMapping(value = "/listePersonnages")
    public String listePers(
            Model model,
            @RequestParam("page") Optional<Integer> page,
            @RequestParam("size") Optional<Integer> size)
    {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);

        Pageable sansTri  = PageRequest.of(currentPage-1,pageSize);
        Pageable triParNom =
                PageRequest.of(currentPage-1,pageSize, Sort.by("nom"));

        Page<personnages> personnagePage = personnageDao.findAll(PageRequest.of(currentPage-1,pageSize));

        model.addAttribute("personnagePage", personnagePage);
        model.addAttribute("listePerso", personnagePage.getContent());

        int totalPages = personnagePage.getTotalPages();
        if (totalPages > 0) {
/*            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());*/
            List<Integer> pageNumbers = new ArrayList<>();
            for(int i=1; i<=totalPages;i++)
                pageNumbers.add(i);
            model.addAttribute("pageNumbers", pageNumbers);
        }

        return "liste";
    }




    @GetMapping(value = "/listePersonnagesSexe")
    public String listePersSex(
            Model model,
            @RequestParam("page") Optional<Integer> page,
            @RequestParam("size") Optional<Integer> size)
    {
        int curPage = page.orElse(1);
        int pageS = size.orElse(5);

        Pageable sansTri  = PageRequest.of(curPage-1,pageS);
        Pageable triNom =
                PageRequest.of(curPage-1,pageS, Sort.by("nom"));

        Page<personnages> personnagePage =  personnageDao.findBySexe("M",triNom);

        model.addAttribute("personnagePage", personnagePage);
        model.addAttribute("listePersoSex", personnagePage.getContent());

        int totalPages = personnagePage.getTotalPages();
        if (totalPages > 0) {
/*            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());*/
            List<Integer> pageNumbers = new ArrayList<>();
            for(int i=1; i<=totalPages;i++)
                pageNumbers.add(i);
            model.addAttribute("pageNumbers", pageNumbers);
        }

        return "listesexe";
    }

    @GetMapping(value = "/listePersonnagesGenre")
    public String listePersoGenre(
            Model model,
            @RequestParam("page") Optional<Integer> page,
            @RequestParam("size") Optional<Integer> size)
    {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);

        Pageable sansTri  = PageRequest.of(currentPage-1,pageSize);
        Pageable triParNom =
                PageRequest.of(currentPage-1,pageSize, Sort.by("nom"));

        Page<personnages> personnagePage =  personnageDao.findByGenre("GENTIL",triParNom);

        model.addAttribute("personnagePage", personnagePage);
        model.addAttribute("listePersoGenre", personnagePage.getContent());

        int totalPages = personnagePage.getTotalPages();
        if (totalPages > 0) {

            List<Integer> pageNumbers = new ArrayList<>();
            for(int i=1; i<=totalPages;i++)
                pageNumbers.add(i);
            model.addAttribute("pageNumbers", pageNumbers);
        }

        return "listeGenre";
    }


}
