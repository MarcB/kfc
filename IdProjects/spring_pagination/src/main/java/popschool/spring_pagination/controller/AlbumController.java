package popschool.spring_pagination.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import popschool.spring_pagination.dao.AlbumDao;
import popschool.spring_pagination.model.albums;
import popschool.spring_pagination.model.personnages;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class AlbumController {
    @Autowired
    private AlbumDao albumDao;
    @GetMapping(value = "/listeAlbums")
    public String listeAlbum(
            Model model,
            @RequestParam("page") Optional<Integer> page,
            @RequestParam("size") Optional<Integer> size)
    {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);

        Pageable sansTri  = PageRequest.of(currentPage-1,pageSize);
        Pageable triParTitre =
                PageRequest.of(currentPage-1,pageSize, Sort.by("titre"));

        Page<albums> albumPage = albumDao.findAll(PageRequest.of(currentPage-1,pageSize));

        model.addAttribute("albumPage", albumPage);
        model.addAttribute("listeAlbum", albumPage.getContent());

        int totalPages = albumPage.getTotalPages();
        if (totalPages > 0) {

            List<Integer> pageNumbers = new ArrayList<>();
            for(int i=1; i<=totalPages;i++)
                pageNumbers.add(i);
            model.addAttribute("pageNumbers", pageNumbers);
        }

        return "listeAlbums";
    }

    @GetMapping(value = "/listeAlbumsByInter")
    public String listeAlbumsDate(
            Model model,
            @RequestParam("page") Optional<Integer> page,
            @RequestParam("size") Optional<Integer> size)
    {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);

        Pageable sansTri  = PageRequest.of(currentPage-1,pageSize);
        Pageable triParTitre =
                PageRequest.of(currentPage-1,pageSize, Sort.by("titre"));

        Page<albums> albumDatePage = albumDao.findByAnneeBetween(1960,1986,triParTitre);

        model.addAttribute("albumDatePage", albumDatePage);
        model.addAttribute("listByannee", albumDatePage.getContent());

        int totalPages = albumDatePage.getTotalPages();
        if (totalPages > 0) {

            List<Integer> pageNumbers = new ArrayList<>();
            for(int i=1; i<=totalPages;i++)
                pageNumbers.add(i);
            model.addAttribute("pageNumbers", pageNumbers);
        }

        return "listeAnnees";
    }




}
