package popschool.spring_pagination.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class personnages {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String nom;
    private String prenom;
    private String profession;
    private String  sexe;
    private String  genre;

    @ManyToMany
    @JoinTable(name = "apparaits",
    joinColumns = {@JoinColumn(name = "personnage_id")},
    inverseJoinColumns={@JoinColumn(name= "album_id")})
    private List<popschool.spring_pagination.model.albums> albums = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public List<popschool.spring_pagination.model.albums> getAlbums() {
        return albums;
    }

    public void setAlbums(List<popschool.spring_pagination.model.albums> albums) {
        this.albums = albums;
    }

    @Override
    public String toString() {
        return "Personnage{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", profession='" + profession + '\'' +
                ", sexe='" + sexe + '\'' +
                ", genre='" + genre + '\'' +
                '}';
    }
}
