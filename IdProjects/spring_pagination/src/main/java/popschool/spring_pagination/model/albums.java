package popschool.spring_pagination.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class albums {
    private int id;
    private String titre;
    private int annee;
    private albums albumsBySuivant;
    private List<popschool.spring_pagination.model.personnages> personnages= new ArrayList<>();

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "titre",unique = true, nullable = false)
    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    @Basic
    @Column(name = "annee")
    public int getAnnee() {
        return annee;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof albums)) return false;
        albums albums = (albums) o;
        return titre.equals(albums.titre);
    }

    @Override
    public int hashCode() {
        return Objects.hash(titre);
    }

    @ManyToOne
    @JoinColumn(name = "suivant", referencedColumnName = "id")
    public albums getAlbumsBySuivant() {
        return albumsBySuivant;
    }

    public void setAlbumsBySuivant(albums albumsBySuivant) {
        this.albumsBySuivant = albumsBySuivant;
    }

    @ManyToMany(mappedBy = "albums")
    public List<popschool.spring_pagination.model.personnages> getPersonnages() {
        return personnages;
    }

    public void setPersonnages(List<popschool.spring_pagination.model.personnages> personnages) {
        this.personnages = personnages;
    }
}

