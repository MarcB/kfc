package popschool.spring_pagination.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import popschool.spring_pagination.model.personnages;

import java.util.List;

public interface PersonnageDao extends JpaRepository<personnages, Integer> {
    Page<personnages> findBySexe(String sexe, Pageable request);
    Page<personnages> findByGenre(String genre, Pageable request);
}
