package popschool.spring_pagination.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import popschool.spring_pagination.model.albums;
import popschool.spring_pagination.model.personnages;


public interface  AlbumDao extends JpaRepository<albums, Integer> {
    Page<albums> findByTitre(String titre, Pageable request);
    Page<albums> findByAnneeBetween(int annee1,int annee2, Pageable request);
}
