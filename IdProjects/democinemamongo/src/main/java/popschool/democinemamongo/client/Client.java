package popschool.democinemamongo.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import popschool.democinemamongo.dao.MovieDAO;
import popschool.democinemamongo.dto.GenreAndTitle;
import popschool.democinemamongo.model.Movie;

import java.util.List;

import static org.springframework.data.domain.Sort.Direction.ASC;

@Component
public class Client implements CommandLineRunner {
    private final MovieDAO movieDAO;

    @Autowired
    public Client(MovieDAO movieDAO){
        this.movieDAO=movieDAO;
    }


    @Override
    public void run(String... args) throws Exception {

        List<Movie> list = movieDAO.findByDirectorLastnameLikeOrderByYear("Eastwood");
        System.out.println("*******************************");
        System.out.println("Films dirigés par Eastwood par année: \n");
        System.out.println(list);

        List<Movie> list2 = movieDAO.findByYearBetweenOrderByYearDesc(1995,2000);
        System.out.println("*******************************");
        System.out.println("Films entre 2 années: \n");
        System.out.println(list2);

        List<GenreAndTitle> list3=movieDAO.findByYear(1996);
        System.out.println("*******************************");
        System.out.println("Films d'une année: \n");
        list3.forEach(g -> System.out.println(g.getGenre()+ " : "+g.getTitle()+" dirige par "+g.getDirector().getFullName()));


        Sort sort = Sort.by(ASC,"_id");
        System.out.println("*******************************");
        System.out.println("Liste de tous les genres de film : \n");
        System.out.println(movieDAO.findAllGenre(sort));

        System.out.println("*******************************");
        System.out.println("Liste des films par genre : \n");
        System.out.println(movieDAO.moviesGenre());

        System.out.println("*******************************");
        System.out.println("Liste des films d'un genre : \n");
        System.out.println(movieDAO.findByGenreOrderByTitleAsc("Action"));

        System.out.println("*******************************");
        System.out.println("Nombre de film d'un genre : \n");
        System.out.println(movieDAO.countByGenre("Action"));
    }
}
