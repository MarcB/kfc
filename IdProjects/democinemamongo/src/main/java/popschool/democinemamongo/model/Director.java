package popschool.democinemamongo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Director {

    private String id;

    @Field("last_name")
    private String lastname;

    @Field("first_name")
    private String firstname;
    private String birth_date;

}
