package popschool.democinemamongo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class Actor {

    private String id;

    @Field("last_name")
    private String lastname;

    @Field("first_name")
    private String firstname;

    private String birth_date;

    private String role;

}
