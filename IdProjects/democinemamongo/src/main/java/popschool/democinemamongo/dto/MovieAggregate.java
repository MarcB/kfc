package popschool.democinemamongo.dto;

import lombok.Value;
import org.springframework.data.annotation.Id;

import java.util.List;

@Value
public class MovieAggregate {

    @Id
     String genre;
     List<String> movies;
     int count;

}
