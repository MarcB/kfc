package popschool.democinemamongo.dto;

public interface GenreAndTitle {

    String getGenre();
    String getTitle();
    DirectorInfo getDirector();

    interface DirectorInfo{
        String getFirstname();
        String getLastname();

        default String getFullName(){
            return getFirstname().concat(" ").concat(getLastname());
        }
    }
}
