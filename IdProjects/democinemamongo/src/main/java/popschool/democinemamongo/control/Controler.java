package popschool.democinemamongo.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import popschool.democinemamongo.dao.MovieDAO;
import popschool.democinemamongo.model.Movie;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.data.domain.Sort.Direction.ASC;

@Controller
public class Controler {

    @Autowired
    MovieDAO movieDAO;

    @GetMapping({ "/", "/index" })
    public String index(Model model){
        model.addAttribute("movies", movieDAO.findAll());
        return "movies";
    }

    @GetMapping({ "/selectgenre" })
    public String selectGenre(Model model){
        Sort sort = Sort.by(ASC,"_id");
        List<String> genres = movieDAO.findAllGenre(sort);
        model.addAttribute("genres", genres);
        return "selectgenre";
    }

    @PostMapping("selectgenre")
    public String selectGenre(Model model, @RequestParam("genre") String genre){

        List<Movie> filmsbygenre = movieDAO.findByGenreOrderByTitleAsc(genre);
        model.addAttribute("movies", filmsbygenre);
        return "movies";
    }

    @GetMapping({ "/selectyear" })
    public String selectYear(Model model){

        List<Integer> years = new ArrayList<>();
        for(int i=1960; i<2020; i++ ){
            years.add(i);
        }
        model.addAttribute("years", years);
        return "selectyear";
    }

    @PostMapping("selectyear")
    public String selectYear(Model model, @RequestParam("max")  int max, @RequestParam("min") int min){

        List<Movie> filmsbyyear = movieDAO.findByYearBetweenOrderByYearDesc(min-1, max+1);
        model.addAttribute("movies", filmsbyyear);
        return "movies";
    }

    @GetMapping({ "/selectdirector" })
    public String selectDirector(Model model){

        return "selectdirector";
    }

    @PostMapping("selectdirector")
    public String selectDirector(Model model, @RequestParam("director") String director){

        List<Movie> filmsbydirector = movieDAO.findByDirectorLastnameLikeOrderByYear(director);
        model.addAttribute("movies", filmsbydirector);
        return "movies";
    }


}
