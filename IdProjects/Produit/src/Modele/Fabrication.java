package Modele;



public class Fabrication {

    protected int quantite=1;
    protected String lieu;

    protected Produit origine=null;
    protected Produit destination=null;

    public Fabrication(int quantite, String lieu, Produit origine, Produit destination) {
        this.quantite = (quantite<1?1:quantite);
        this.lieu = (lieu==null?"LILLE":lieu.toUpperCase());
        this.origine = origine;
        this.destination = destination;
    }
}