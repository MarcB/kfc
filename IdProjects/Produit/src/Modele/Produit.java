package Modele;
import java.util.*;

public abstract class Produit {
        public String nom;

        //associaation 1--* vers fabrication
        private Set<Fabrication>composants=new HashSet<>();
        private Set<Fabrication>composites=new HashSet<>();

        public String getNom(){
        return nom;
        }

        public void setNom(String nom) {
                this.setNom(nom);
        }
        private boolean estCompatible(Produit pr){return false;}

        public  void addComposant(Produit origine,int quantite,String lieu)
                throws ExeptionMetier
        {
                if(origine==null)
                        throw new ExeptionMetier("le composant est obligatoire");

                if(this.estCompatible((origine))){
                        throw new ExeptionMetier(("le composant n'est pas compatible"));
                }
                Fabrication fab=new Fabrication(quantite,lieu,origine,this);

                origine.composites.add(fab);
                this.composants.add(fab);
        }

        @Override
        public String toString() {
                return this.getClass().getSimpleName()+"{" +
                        "nom='" + nom + '\'' +
                        '}';
        }

        //metier
        public double getPrix(){
                double total=0.0d;

                for(Fabrication f : this.composants){
                        total=total+f.quantite*f.origine.getPrix();
                }
                return total;
        }

        public static void main(String[] args) {
                Produit roue    = new ProduitBrut("roue", 2);
                Produit chassis = new ProduitSemi"chassis");
                Produit axe     = new ProduitBrut("axe", 1);
                Produit cadre   = new ProduitSemi"cadre");
                Produit velo    = new ProduitFini("velo");
                Produit frein   = new ProduitBrut("frein", 3);
                Produit selle   = new ProduitBrut("selle", 10);
                Produit guidon  = new ProduitSemi("guidon");

                chassis.addComposant(roue, 4, "lille" );
                chassis.addComposant(axe, 2, "lille");

                cadre.addComposant(roue, 2, "ascq");
                cadre.addComposant(selle, 1, "ascq");
                guidon.addComposant(frein, 2, "ascq");
                velo.addComposant(guidon, 1, "ascq");
                velo.addComposant(cadre, 1,"ascq");

                System.out.println("prix: " + velo.getPrix());

                velo.afficherNomenclature();

                System.out.println("------------------------------------");
                frein.ruptureStock();
        }
}
