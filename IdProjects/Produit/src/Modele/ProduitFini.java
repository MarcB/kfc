package Modele;

public class ProduitFini extends  Produit{
    public static String nom;

    public String getProduitFini() {
        return nom;
    }

    public void setProduitFini(String nom) {
        this.nom =( nom==null?"velopourri":nom.toUpperCase());
    }

    public ProduitFini(String nom) {
        super(nom);

    }




}
