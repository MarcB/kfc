package modele;

public class Etoile {
    public static String etoile1(int n){
        StringBuffer resultat = new StringBuffer("");
        for(int i=1;i<=n;i++) {  // il y a N lignes

            for (int j = 0; j < n - i; j++) { // le nbre d'espace vaut (N - numeroLigne)
                resultat = resultat.append(" ");
            }

            for (int j = 0; j < i; j++) {  // le nbre d'etoile vaut numeroLigne
                resultat.append("*");
            }
            resultat.append("\n");
        }
            return resultat.toString();
    }

    private static void diamant(int hauteur){
        // La hauteur doit etre un entier positif et impair
        if (hauteur<=0 || (hauteur%2 ==0))// eliminer les nbres PAIRS
            return;

        int n= (hauteur-1)/2;
        // Trace de la pyramide superieure
        for(int i=n;i>=0;i--){  // decrementation de n à 0
            // afficher les espaces ; leur nombre == i
            for(int j=0; j<i;j++){ // incrementation de 0 à i-1
               System.out.print(" ");
            }

            int limite = 2*(n-i)+1;  // analyse : nombre d'etoiles

            for(int j=0; j<limite; j++) // incrementation de 0 à limite -
                System.out.print("*");
            System.out.println(""); // force le passage à la ligne
        }
        // Trace de la pyramide inferieure
        for(int i=1; i<=n; i++){
            for(int j=0; j<i;j++) // incrementation et affichage des espaces
                System.out.print(" ");
            int limite=(2*(n-i)+1);
            for(int j=0; j<limite; j++) // increm et affichage des etoiles
                System.out.print("*");
            System.out.println("");  // forcer le passage à la ligne
        }

    }

    public static void main(String[] args) {

        System.out.println(etoile1(10) );

        diamant(7);
    }
}
