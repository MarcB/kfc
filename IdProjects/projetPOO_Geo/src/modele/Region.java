package modele;

public class Region extends Zone {

        private String responsable;

        public String getResponsable() {
            return responsable;
        }

        public void setResponsable(String responsable) {
            this.responsable =( responsable==null?"bouboule":responsable.toUpperCase());
        }

    public Region(String nom, String responsable) {
        super(nom);
        this.setResponsable(responsable);
    }
    @Override
    public String toString() {
        return super.toString()+"{" +
                "responsable:='" + responsable + '\'' +
                '}';
    }
}

