    package modele;


    import com.sun.source.util.Trees;

    import java.util.*;
    import java.util.Vector;

    public class Zone implements Comparable<Zone>{
        //attributs de classe
        private static Map<String,Zone> ensZone=new TreeMap<>();

        //attributs d'instance
        private String nom;

        //association-->mere
        private Zone mere=null;

        //association-->filles
        private Set<Zone>filles=new TreeSet<>();

        public int compareTo(Zone autre){   //ordre naturel
            return this.nom.compareTo(autre.nom);
        }

        public String getNom() {
            return nom;
        }

        public void setNom(String nom) {
            this.nom=nom;
        }
    public Zone(String nom){
        this.setNom(nom);
    }


        //metier
        public int getPopulation() {
            int somme = 0;
            for (Zone f : filles) {
                somme=somme+f.getPopulation();
            }
            return somme;
        }
        public static void ajoutPays(String nom,String president)throws Exception{
            if(nom==null)
                throw new Exception("Zone - creation Pays - le nom est obligatoire");
            String cle=nom.toUpperCase();

            if(Zone.ensZone.containsKey(cle))
                throw new  Exception("Zone - creation Pays - le nom doit etre unique");

            Zone nouv=new Pays(cle,president);
            Zone.ensZone.put(cle,nouv);
        }

        public static void ajoutRegion(String nom,String responsable,String nomPays)throws Exception{
            if(nom==null)
                throw new Exception("Zone - creation Region - le nom est obligatoire");
            String cle=nom.toUpperCase();

            if(Zone.ensZone.containsKey(cle))
                throw new  Exception("Zone - creation Region - le nom doit etre unique");

            if(nomPays==null)
                throw new  Exception("Zone - creation Region - le nom du Pays ne peut etre nul");
            String cle2=nomPays.toUpperCase();

            if(!Zone.ensZone.containsKey(cle2))
                throw new  Exception("Zone - creation Region - le Pays est inconnu");

            Zone zonePays=Zone.ensZone.get(cle2);
            if(!(zonePays instanceof Pays))
                throw new Exception("Zone - creation Region - l'objet parent n'est pas un pays");

            Zone zoneRegion=new Region (nom,responsable);
            zoneRegion.mere=zonePays;
            zonePays.filles.add(zoneRegion);
            Zone.ensZone.put(cle,zoneRegion);
        }


        public static void ajoutDepartement(String nom,String prefet,String nomRegion)throws Exception{
            if(nom==null)
                throw new Exception("Zone - creation dept - le nom est obligatoire");
            String cle=nom.toUpperCase();

            if(Zone.ensZone.containsKey(cle))
                throw new  Exception("Zone - creation dept - le nom doit etre unique");

            if(nomRegion==null)
                throw new  Exception("Zone - creation dept - le nom du Pays ne peut etre nul");
            String cle2=nomRegion.toUpperCase();

            if(!Zone.ensZone.containsKey(cle2))
                throw new  Exception("Zone - creation dept - le Pays est inconnu");

            Zone zoneRegion=Zone.ensZone.get(cle2);
            if(!(zoneRegion instanceof Region))
                throw new Exception("Zone - creation dept - l'objet parent n'est pas une region");

            Zone zoneDepartement=new Departement (nom,prefet);
            zoneDepartement.mere=zoneRegion;
            zoneRegion.filles.add(zoneDepartement);
            Zone.ensZone.put(cle,zoneDepartement);
        }


        public static void ajoutCommune(String nom,String maire,String nomDepartement,int population)throws Exception{
            if(nom==null)
                throw new Exception("Zone - creation commune - le nom est obligatoire");
            String cle=nom.toUpperCase();

            if(Zone.ensZone.containsKey(cle))
                throw new  Exception("Zone - creation commune - le nom doit etre unique");

            if(nomDepartement==null)
                throw new  Exception("Zone - creation commune - le nom du dep ne peut etre nul");
            String cle2=nomDepartement.toUpperCase();

            if(!Zone.ensZone.containsKey(cle2))
                throw new  Exception("Zone - creation commune - le dep est inconnu");

            Zone zoneDepartement=Zone.ensZone.get(cle2);
            if(!(zoneDepartement instanceof Departement))
                throw new Exception("Zone - creation commune - l'objet parent n'est pas une region");

            Zone zoneCommune=new Commune (nom,maire,population);
            zoneDepartement.mere=zoneDepartement;
            zoneDepartement.filles.add(zoneCommune);
            Zone.ensZone.put(cle,zoneCommune);
        }

        @Override
        public String toString() {
            return this.getClass().getSimpleName()+"{" +"nom='" + nom + '\'' +'}';
        }

        private void afficherBis(int decalage){

            for(int i=0;i<decalage;i++)
                System.out.print("\t");

            System.out.println(this.getClass().getSimpleName()+" "+this.getNom()+" pop:"+this.getPopulation());

            for(Zone f:this.filles){f.afficherBis(decalage+1);}
        }

        public static Zone rechercher(String nom){

            if(nom==null)return null;

            return Zone.ensZone.get(nom.toUpperCase());
        }
        public static void affichCle() {
            for (String cle : Zone.ensZone.keySet()) {
                System.out.println(cle);
            }
        }
        public static void main(String[] args) {
            try {
                Zone.ajoutPays("fr", "nabo");
                Zone.ajoutRegion("hf", "xavir", "fr");
                Zone.ajoutDepartement("nrd", "thony", "hf");
                Zone.ajoutCommune("va", "grosnull", "nrd", 10);
                Zone.affichCle();
                Zone c=Zone.rechercher("fr");
                System.out.println(c);
               // c.afficherBis(1);

            }
            catch(Exception ex){
                System.out.println(ex);
            }
        }

    }
