package modele;

public class Departement extends Zone {
        private String prefet;

        public String getPrefet() {
            return prefet;
        }

        public void setPrefet(String prefet) {
            this.prefet =( prefet==null?"surfait":prefet.toUpperCase());
        }

    public Departement(String nom, String prefet) {
        super(nom);
        this.prefet = prefet;
    }
    @Override
    public String toString() {
        return super.toString()+"{" +
                "prefet='" + prefet + '\'' +
                '}';
    }
}