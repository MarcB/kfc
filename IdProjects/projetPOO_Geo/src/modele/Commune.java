package modele;

public class Commune extends Zone{
    private String maire;
    private int population;

    public String getMaire() {
        return maire;
    }

    public void setMaire(String maire) {
        this.maire =( maire==null?"gros":maire.toUpperCase());
    }

    public Commune(String nom, String maire) {
        super(nom);
        this.maire = maire;
    }

    @Override
    public int getPopulation() {  //termine la recursivité
        return population;
    }

    public void setPopulation(int population) {
        this.population = (population<=0?1:population);
    }

    public Commune(String nom, String maire, int population) {
        super(nom);
        this.maire = maire;
        this.population = population;
    }
    @Override
    public String toString() {
        return super.toString()+"{" +
                "maire='" + maire + '\'' +"popu"+population+
                '}';
    }
}
