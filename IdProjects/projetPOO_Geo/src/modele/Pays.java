package modele;

public class Pays extends Zone{
    private String president;

    public String getPresident() {
        return president;
    }

    public void setPresident(String president) {
        this.president =( president==null?"nabo":president.toUpperCase());
    }

    public Pays(String nom,String president) {
        super(nom);
        this.setPresident(president);
    }

    @Override
    public String toString() {
        return super.toString()+"{" +
                "president='" + president + '\'' +
                '}';
    }
}
