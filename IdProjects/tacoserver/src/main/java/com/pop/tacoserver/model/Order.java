// tag::allButDetailProperties[]
package com.pop.tacoserver.model;

import lombok.Data;
import org.hibernate.validator.constraints.CreditCardNumber;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Data
@Entity
@Table(name="Taco_Order")
public class Order implements Serializable {

  private static final long serialVersionUID = 1L;
  
  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  private Long id;
  
  private Date placedAt;
  private String deliveryName;
  private String deliveryStreet;
  private String deliveryCity;
  private String deliveryState;
  private String deliveryZip;
  private String ccNumber;
  private String ccExpiration;
  private String ccCVV;

  //Si vous redémarez le erveur entre les deux envois requetes postman
  //Faites un cascade persist sur le manytomany dans order (cascade =persist)
//tag::allButDetailProperties[]
  @ManyToMany(targetEntity= com.pop.tacoserver.model.Taco.class)
  private List<Taco> tacos = new ArrayList<>();
  
  public void addDesign(com.pop.tacoserver.model.Taco design) {
    this.tacos.add(design);
  }
  
  @PrePersist
  void placedAt() {
    this.placedAt = new Date();
  }
  
}
//end::allButDetailProperties[]
//{
//    "tacos" :[{"name" : "yolo",
//"ingredients" : [ {
//            "id": "GRBF",
//            "name": "Ground Beef",
//            "type": "PROTEIN"
//        },
//        {
//            "id": "CARN",
//            "name": "Carnitas",
//            "type": "PROTEIN"
//        }]}],"deliveryName" : "JeanJacques",
//        "deliveryStreet" : "Meryl street",
//        "deliveryCity" : "LibertyMaisPasTrop City",
//        "deliveryState" : "Massage Chaussette",
//        "deliveryZip" : "4",
//        "ccNumber" : "1111222233334444",
//        "ccExpiration" : "03/15",
//        "ccCVV" : "4"
//}

//Si vous redémarez le erveur entre les deux envois requetes postman

//Faites un cascade persist sur le manytomany dans order
