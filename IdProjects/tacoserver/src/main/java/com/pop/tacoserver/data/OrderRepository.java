package com.pop.tacoserver.data;

import org.springframework.data.repository.CrudRepository;
import com.pop.tacoserver.model.Order;

public interface OrderRepository 
         extends CrudRepository<Order, Long> {

}
