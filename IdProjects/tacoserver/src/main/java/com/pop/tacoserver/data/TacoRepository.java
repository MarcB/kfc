package com.pop.tacoserver.data;

import org.springframework.data.repository.CrudRepository;
import com.pop.tacoserver.model.Taco;

public interface TacoRepository 
         extends CrudRepository<Taco, Long> {

}
