package com.pop.tacoserver.data;

import org.springframework.data.repository.CrudRepository;
import com.pop.tacoserver.model.Ingredient;

public interface IngredientRepository 
         extends CrudRepository<Ingredient, String> {

}
