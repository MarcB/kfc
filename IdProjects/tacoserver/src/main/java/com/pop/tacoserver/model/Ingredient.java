package com.pop.tacoserver.model;

import javax.persistence.Entity;
import javax.persistence.Id;

// erreur normale car une entite ne peut avoir un constructeur sans arg. private MAIS spring l'autorise!!!
@Entity
public class Ingredient {
  
  @Id
  private  String id;
  private   String name;
  private   Type type;

  public Ingredient(String id,String name, Type type) {
    this.id=id;
    this.name = name;
    this.type = type;
  }

  public Ingredient() {

  }

  public String getId() {
    return this.id;
  }

  public String getName() {
    return this.name;
  }

  public Type getType() {
    return this.type;
  }

  public void setId(String id) {
    this.id = id;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setType(Type type) {
    this.type = type;
  }

  public boolean equals(final Object o) {
    if (o == this) return true;
    if (!(o instanceof Ingredient)) return false;
    final Ingredient other = (Ingredient) o;
    if (!other.canEqual((Object) this)) return false;
    final Object this$id = this.getId();
    final Object other$id = other.getId();
    if (this$id == null ? other$id != null : !this$id.equals(other$id)) return false;
    final Object this$name = this.getName();
    final Object other$name = other.getName();
    if (this$name == null ? other$name != null : !this$name.equals(other$name)) return false;
    final Object this$type = this.getType();
    final Object other$type = other.getType();
    if (this$type == null ? other$type != null : !this$type.equals(other$type)) return false;
    return true;
  }

  protected boolean canEqual(final Object other) {
    return other instanceof Ingredient;
  }

  public int hashCode() {
    final int PRIME = 59;
    int result = 1;
    final Object $id = this.getId();
    result = result * PRIME + ($id == null ? 43 : $id.hashCode());
    final Object $name = this.getName();
    result = result * PRIME + ($name == null ? 43 : $name.hashCode());
    final Object $type = this.getType();
    result = result * PRIME + ($type == null ? 43 : $type.hashCode());
    return result;
  }

  public String toString() {
    return "Ingredient(id=" + this.getId() + ", name=" + this.getName() + ", type=" + this.getType() + ")";
  }


  public  enum Type {
    WRAP, PROTEIN, VEGGIES, CHEESE, SAUCE
  }
}
