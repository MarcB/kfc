package com.pop.tacoserver.controller;

import com.pop.tacoserver.data.IngredientRepository;
import com.pop.tacoserver.data.OrderRepository;
import com.pop.tacoserver.data.TacoRepository;
import com.pop.tacoserver.model.Ingredient;
import com.pop.tacoserver.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class MainController {
    @Autowired
    private IngredientRepository ingredientRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private TacoRepository tacoRepository;


    @GetMapping(value = "/type/{type}")
    public Iterable<Ingredient> allIngredientByType(@PathVariable Ingredient.Type type){
//        System.out.println("la**********************************************");
        List<Ingredient>ingredients=new ArrayList<>();
        ingredientRepository.findAll().forEach(ingredients::add);
//        System.out.println(ingredients);
        return ingredients .stream()
                            .filter(x ->x.getType().equals(type))
                .collect(Collectors.toList());
    }

    @PostMapping("/order/create")
    public void addOrder(@RequestBody Order order){
        orderRepository.save(order);
    }

}
