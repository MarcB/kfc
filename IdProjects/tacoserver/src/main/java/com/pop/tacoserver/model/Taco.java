package com.pop.tacoserver.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@Entity
public class Taco {

  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  private Long id;

  @NotNull
  @Size(min=5)
  private String name;

  private Date createdAt;

  @ManyToMany(targetEntity= com.pop.tacoserver.model.Ingredient.class)
  @Size(min=1)
  private List<com.pop.tacoserver.model.Ingredient> ingredients = new ArrayList<>();
  
  @PrePersist
  void createdAt() {
    this.createdAt = new Date();
  }
}
