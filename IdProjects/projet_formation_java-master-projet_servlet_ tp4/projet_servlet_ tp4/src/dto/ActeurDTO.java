package dto;

import java.io.Serializable;

public class ActeurDTO implements Serializable {

    private static final long serialVersionUID =1l;
    private int nacteur;
    private String nom;
    private String prenom;

    public ActeurDTO(int nacteur,String nom,String prenom){
        this.nacteur=nacteur;
        this.nom=nom;
        this.prenom=prenom;
    }

    @Override
    public String toString() {
        return "ActeurDTO{" +
                "nacteur=" + nacteur +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                '}';
    }
}
