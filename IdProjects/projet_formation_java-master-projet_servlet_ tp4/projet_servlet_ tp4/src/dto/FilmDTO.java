package dto;

import java.io.Serializable;

public class FilmDTO implements Serializable {

    // ---------------------attributs d'instance-------------------------
    private static final long serialVersionUID = 1L;

    private int nfilm ;
    private String titre;
    private String nomRealisateur;
    private String nomActeur;
    private String genre;
    private String entrees;
    private String sortie;
    private String pays;



    // ---------------------------- G E T T E R --------------------------
    public int getNfilm() {
        return nfilm;
    }

    public static long getSerialVersionUID() {        return serialVersionUID;    }

    public String getGenre() {        return genre;    }

    public String getEntrees() {        return entrees;    }

    public String getSortie() {        return sortie;    }

    public String getPays() {        return pays;    }

    public String getTitre() {
        return titre;
    }

    public String getNomRealisateur() {
        return nomRealisateur;
    }

    public String getNomActeur() {
        return nomActeur;
    }

    // ----------------------- C O N S T R U C T E U R ---------------------------------
    public FilmDTO(int nfilm, String titre, String nomRealisateur, String genre) {
        super();
        this.nfilm = nfilm;
        this.titre = titre;
        this.nomRealisateur = nomRealisateur;
        this.genre = genre;
    }

    public FilmDTO(int nfilm, String titre, String nomRealisateur, String nomActeur, String genre, String entrees, String sortie, String pays) {
        this.nfilm = nfilm;
        this.titre = titre;
        this.nomRealisateur = nomRealisateur;
        this.nomActeur = nomActeur;
        this.genre = genre;
        this.entrees = entrees;
        this.sortie = sortie;
        this.pays = pays;
    }

    //--------------------------- T O S T R I N G ---------------------------------------


    @Override
    public String toString() {
        return   titre;
    }
}
