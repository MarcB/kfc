<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: michael
  Date: 24/02/2020
  Time: 15:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
Fils du genre ${genre.nature}
<br>
<form action="process?action=choix" method="POST">
    <table>
        <thead>
        <tr>
            <th>Titre</th>
            <th>Réalisateur</th>
            <th>Acteur</th>
            <th>Choix</th>

        </tr>
        </thead>
        <tbody>
        <c:forEach var="film" items="${ensFilms}">
            <tr>
                <td>${film.titre}</td>
                <td>${film.nomRealisateur}</td>
                <td>${film.nomActeur}</td>
                <td><input type="checkbox" name="choix" value="${film.nfilm}"/></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <input type="submit" value="votre panier">
</form>
</body>
</html>
