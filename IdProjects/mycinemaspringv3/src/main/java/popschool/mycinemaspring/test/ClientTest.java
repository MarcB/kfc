package popschool.mycinemaspring.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import popschool.mycinemaspring.MycinemaspringApplication;
import popschool.mycinemaspring.dao.ClientRepository;
import popschool.mycinemaspring.model.Client;

import java.util.Optional;

@Component
public class ClientTest implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(MycinemaspringApplication.class);

    @Autowired
    private ClientRepository clientRepository;

    private void information(Client c){
        log.info(c.toString());
    }

    @Override
    public void run(String... args) throws Exception {

        //Creation d'un client
      /*  clientRepository.save(new Client("Duss","Jean Paul",
               "Valenciennes 59300", 30));
        clientRepository.save(new Client("Roove","Jean Paul",
               "Trith 59300", 15));
        clientRepository.save(new Client("Sellam","Brahim",
               "108 rue de Roubaix", 0));*/

        //Update d'un client
        /*log.info("---------------------------------------------------------------------------------------------------");
        log.info("                                    Update un client                                               ");
        log.info("---------------------------------------------------------------------------------------------------");
        Optional<Client>cl=clientRepository.findById(3L);
        Client c = cl.get();
        c.setNom("TINTIN");
        log.info(c.toString());
        clientRepository.save(c);*/

        //Afficher tous les clients
        log.info("---------------------------------------------------------------------------------------------------");
        log.info("                                    DEBUT LISTING clients                                      ");
        log.info("---------------------------------------------------------------------------------------------------");
        for (Client c : clientRepository.findAll()){
            log.info(c.toString());
        }
        log.info("---------------------------------------------------------------------------------------------------");
        log.info("                                     FIN LISTING clients                                      ");
        log.info("---------------------------------------------------------------------------------------------------");



    }
}
