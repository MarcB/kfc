package popschool.mycinemaspring.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import popschool.mycinemaspring.MycinemaspringApplication;
import popschool.mycinemaspring.dao.ClientRepository;
import popschool.mycinemaspring.dao.EmpruntRepository;
import popschool.mycinemaspring.dao.FilmRepository;
import popschool.mycinemaspring.model.Client;
import popschool.mycinemaspring.model.Emprunt;
import popschool.mycinemaspring.model.Film;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

@Component
public class EmpruntTest implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(MycinemaspringApplication.class);

    @Autowired
    private EmpruntRepository empruntRepository;
    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private FilmRepository filmRepository;

    private void information(Emprunt e){
        log.info(e.toString());
    }

    @Override
    public void run(String... args) throws Exception {

        Optional<Client>  client1 =clientRepository.findById(3L);
        Optional<Film>  film1 =filmRepository.findById(2L);
        Date dateRetour = new SimpleDateFormat("yyyy-MM-dd").parse("1980-02-10");
/*
        try{
        //Creation d'un client
        empruntRepository.save(new Emprunt(client1.get(),film1.get(),"oui",dateRetour));
        }catch (Exception ex){
            System.out.println(ex);
        }

*/

        //Afficher tous les clients
        log.info("---------------------------------------------------------------------------------------------------");
        log.info("                                    afficher tous les emprunts                                     ");
        log.info("---------------------------------------------------------------------------------------------------");
        for (Emprunt e : empruntRepository.findAll()){
            log.info(e.toString());
        }

    }
}
