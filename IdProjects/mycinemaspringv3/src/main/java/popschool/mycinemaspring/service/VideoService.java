package popschool.mycinemaspring.service;

import popschool.mycinemaspring.model.Client;
import popschool.mycinemaspring.model.Film;

import javax.persistence.Tuple;
import java.util.List;

public interface VideoService  {
    void emprunter(String nom, String titre);
    void retourEmprunt(String nom, String titre);
    List<Client> ensClient();
    List<Film> ensFilmEmpruntable();
    List<Film> ensFilm();
    List<Film> ensFilmGenre(String genre);
    List<Tuple>infoRealisateurActeur(String titre);
    int nbrFilmDuGenre(String genre);
    List<Film> ensFilmEmpruntesByClient(Client client);
}
