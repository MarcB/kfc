package popschool.mycinemaspring.service;

import org.springframework.beans.factory.annotation.Autowired;
import popschool.mycinemaspring.dao.ClientRepository;
import popschool.mycinemaspring.dao.EmpruntRepository;
import popschool.mycinemaspring.dao.FilmRepository;
import popschool.mycinemaspring.model.Client;
import popschool.mycinemaspring.model.Film;

import javax.persistence.Tuple;
import java.util.List;

public class VideoServiceImp implements VideoService{

    @Autowired
    private EmpruntRepository empruntRepository;
    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private FilmRepository filmRepository;



    @Override
    public void emprunter(String nom, String titre) {

    }

    @Override
    public void retourEmprunt(String nom, String titre) {

    }

    @Override
    public List<Client> ensClient() {
        ;
        Iterable<Client> ensClient;
        return (List<Client>) (ensClient=clientRepository.findAll());
    }

    @Override
    public List<Film> ensFilmEmpruntable() {
        return null;
    }

    @Override
    public List<Film> ensFilm() {
        return null;
    }

    @Override
    public List<Film> ensFilmGenre(String genre) {
        return null;
    }

    @Override
    public List<Tuple> infoRealisateurActeur(String titre) {
        return null;
    }

    @Override
    public int nbrFilmDuGenre(String genre) {
        return 0;
    }

    @Override
    public List<Film> ensFilmEmpruntesByClient(Client client) {
        return null;
    }
}
