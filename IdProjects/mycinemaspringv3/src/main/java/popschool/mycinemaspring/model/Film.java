package popschool.mycinemaspring.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name="film")
public class Film {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long nfilm;

    private String titre;

    @ManyToOne
    @JoinColumn(name = "ngenre")
    private Genre genre;

    @Temporal(TemporalType.DATE)
    private Date sortie;

    @ManyToOne
    @JoinColumn(name = "npays")
    private Pays pays;

    private String realisateur;

    @ManyToOne
    @JoinColumn(name ="nacteurprincipal")
    private Acteur acteur;

    private Long entrees;

    private String oscar;

    @OneToMany(mappedBy = "film")
    private List<Emprunt> emprunt;


}
