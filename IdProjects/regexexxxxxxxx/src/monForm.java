import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.*;

public class monForm {
    private JPanel JPform;
    private JButton button1;
    private JFormattedTextField formattedTextField1;
    private JFormattedTextField formattedTextField2;

    public monForm() {
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String monFormat="[A-Za-z0-9-_.-]+@[A-Za-z0-9]+\\.[A-Za-z]{2,4}";
                //String monFormat="([\b])+@([\b]+\\.[\w]{2,4}";
                if(formattedTextField1.getText().matches(monFormat)){
                    formattedTextField2.setText("ok");
                }else{
                    formattedTextField2.setText("pas ok");
                }


            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("monForm");
        frame.setContentPane(new monForm().JPform);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }
}
