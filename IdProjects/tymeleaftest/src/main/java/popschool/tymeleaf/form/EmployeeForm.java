package popschool.tymeleaf.form;

public class EmployeeForm {
    private int id;
    private String name;
    private String role;

    public EmployeeForm() {    }

    public EmployeeForm(String name, String role) {
        this.name = name;
        this.role = role;
    }

    public int getId() {        return id;    }
    public String getName() {        return name;    }
    public String getRole() {        return role;    }

    public void setId(int id) {        this.id = id;    }
    public void setName(String name) {        this.name = name;    }
    public void setRole(String role) {        this.role = role;    }

}