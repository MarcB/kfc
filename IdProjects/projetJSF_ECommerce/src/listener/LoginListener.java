/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package listener;

import control.ECommerceControleur;
import javax.faces.application.Application;
import javax.faces.application.ViewHandler;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

/**
 *
 * @author didier
 */
public class LoginListener implements PhaseListener{

    @Override
    public void afterPhase(PhaseEvent event) {
        
    }

    @Override
    public void beforePhase(PhaseEvent event) {
        FacesContext ctx = FacesContext.getCurrentInstance();
       // String idVue=ctx.getViewRoot().getViewId();
        Application application = ctx.getApplication();
        
        ECommerceControleur itemBean = (ECommerceControleur)application.evaluateExpressionGet(ctx, 
                "#{itemBean}", ECommerceControleur.class);
        
        // System.out.println("Valeur authentification :"+idVue);

        if(! itemBean.isAuthentifie()){
            ViewHandler viewHandler =application.getViewHandler();
            
            UIViewRoot viewRoot = viewHandler.createView(ctx, "/index.xhtml");

            ctx.setViewRoot(viewRoot);

        }
    }

    @Override
    public PhaseId getPhaseId() {
        return PhaseId.RENDER_RESPONSE;
    }

}
