/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package metier;

import javax.ejb.*;
import javax.persistence.*;
import modele.*;
import java.util.*;
import util.*;



/**
 *
 * @author didier
 */
@Stateful
@LocalBean
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class CartBean {

    // INJECTION des ressources (EJB, PU) par le serveur applicatif
    @EJB
    private ProcessPayment processPaymentBean;

    @PersistenceContext(unitName="ECommercePU", type = PersistenceContextType.EXTENDED)
    private EntityManager em;

	@Remove
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void checkout() {
      em.flush();
	}
	
	@Remove
	public void cancel() {

	}
    
    private Client client;
    private List<Achat> items = new ArrayList<>();
   // private String nomPropCarte;
    private String numeroCarte;
    private Date dateExpirationCarte;

    public void setInfoCB(String numeroCarte, Date dateExpirationCarte) {
        // Gestion des informations financieres
        this.numeroCarte = numeroCarte;
        this.dateExpirationCarte = dateExpirationCarte;
    }

    public void addItem(int code, int quantite) {
        // ajout d'un article dans le panier
        this.items.add(new Achat(code, quantite));
    }

    public void removeItem(int code) throws ItemNotFoundException {
        // Suppression d'un article du panier
        for (Achat current : this.items) {
            if (current.getCode() == code) {
                this.items.remove(current);
                return;
            }
        }
        throw new ItemNotFoundException();  // Produit inconnu
    }
    
    public void updateItem(int code, int quantite) throws ItemNotFoundException {
        // mise à jour d'un article du panier

        for (Achat current : this.items) {
            if (current.getCode() == code) {
                current.setQuantite(quantite);
                return;
            }
        }
        throw new ItemNotFoundException();  // Produit inconnu
    }

    public float getTotalPrice() {
        // Evaluation du panier
        float totalPrice = 0f;

        for (Achat achat : this.items) {
            Item current = this.em.createNamedQuery("Item.findByCodeBarre", Item.class)
                               .setParameter("codeBarre", achat.getCode())
                               .getSingleResult();
            totalPrice = (float) (totalPrice
                    + current.getPrix() * achat.getQuantite());
        }
        // arrondit au centime le plus bas
        return ( (long) (totalPrice * 100) )/ 100f;
    }

    public void purchase() throws CardExpiredException {
        try {
            Payment paiement = processPaymentBean
            		.byCredit(client,
            				  this.numeroCarte, 
            				  this.dateExpirationCarte,
            				  this.getTotalPrice());
            // termine le processus dachat
            // déclenche PurchaseProblemException si une erreur se produit

            Commande commande = new Commande();
            commande.setClient(client);
            commande.setDatecde(new java.util.Date());
            commande.setPayment(paiement);

            for (Achat achat : this.items) {
                DetailCde detailCde = new DetailCde();
                //commande.addDetailCde(detailCde);
                detailCde.setCommande(commande);

                Item item = this.em.createNamedQuery("Item.findByCodeBarre", Item.class)
                		      .setParameter("codeBarre", achat.getCode())
                		      .getSingleResult();
                detailCde.setItem(item);
                detailCde.setQte(achat.getQuantite());
                em.persist(detailCde);
            }
            em.persist(commande);
        } catch (Exception ex) {
        	System.out.println("---------------");
            System.out.println(ex.getMessage());
            throw new CardExpiredException();
        }

    }

    public List<Achat> getContents() {
    	// le panier items ne contient que des couples <codeBarre, qte)
    	// ens contient les 4 infos <codeBarre, TITRE, PRIX, qte>
        List<Achat> ens = new ArrayList<Achat>();
        for (Achat achat : this.items) {
            Item item = this.em.createNamedQuery("Item.findByCodeBarre", Item.class)
            		.setParameter("codeBarre", achat.getCode())
            		.getSingleResult();

            Achat copie = new Achat(item.getCodeBarre(), achat.getQuantite());
            copie.setTitle(item.getTitre());
            copie.setPrice(item.getPrix());

            ens.add(copie);
        }
        return ens;
    }
    
    public boolean isPanierVide(){
        return this.items.isEmpty();
    }

    public Client findOrCreateClient(String nom, String prenom) {
        try {
            this.client = em.createQuery("select c from Client c "
                    + " where upper(c.nom)=:nom and upper(c.prenom)=:prenom", Client.class)
                    .setParameter("nom", nom.toUpperCase())
                    .setParameter("prenom", prenom.toUpperCase())
                    .getSingleResult();
        } catch (NoResultException notFound) {
            this.client = new Client();
            this.client.setNom(nom);
            this.client.setPrenom(prenom);
            em.persist(client);
        }
        return this.client;
    }


}
