/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package metier;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
//import modele.Book;
//import modele.CDDisk;
import modele.Item;

/**
 *
 * @author didier
 */
@Stateless
public class ItemFacade extends AbstractFacade<Item> {
    @PersistenceContext(unitName = "ECommercePU")
    private EntityManager em;
//    @Resource
//    private javax.transaction.UserTransaction utx;

    protected EntityManager getEntityManager() {
        return em;
    }

    public ItemFacade() {
        super(Item.class);
    }
    
    
    public Item findByCodeBarre(int codeBarre) {
        Item aux = null;
        try {
            aux =  em.createQuery("select i from Item i where i.codeBarre = :codeBarre",Item.class)
                                  .setParameter("codeBarre", codeBarre)
                                  .getSingleResult();
        } catch (Exception ex) {
            aux = null;
        }

        return aux;
    }

//    public List<Book> findAllBooks() {
//        List<Book> aux = null;
//        try {
//            aux =  em.createQuery("select i from Book i order by i.titre ", Book.class)
//                                  .getResultList();
//        } catch (Exception ex) {
//            aux = new ArrayList<Book>();
//        }
//
//        return aux;
//    }
//
//
//    public List<CDDisk> findAllCDDisks() {
//        List<CDDisk> aux = null;
//        try {
//            aux =  em.createQuery("select i from CDDisk i order by i.titre ", CDDisk.class)
//                                  .getResultList();
//        } catch (Exception ex) {
//            aux = new ArrayList<CDDisk>();
//        }
//
//        return aux;
//    }

}
