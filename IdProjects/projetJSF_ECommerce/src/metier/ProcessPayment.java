/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package metier;



import java.util.Calendar;
import java.util.Date;
import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import modele.Client;
import modele.Payment;
import util.CardExpiredException;

/**
 *
 * @author didier
 */
@Stateful
@LocalBean
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class ProcessPayment {


    // INJECTION de la ressource par le serveur applicatif
    @PersistenceContext(type = PersistenceContextType.EXTENDED)
    private EntityManager em;

    @Remove
    public Payment byCredit(Client client, String creditCard,
            Date creditCardExpiration, double montant)
            throws CardExpiredException
    {
        Date today = Calendar.getInstance().getTime();
        if (creditCardExpiration.before(today)) {
            throw new CardExpiredException("Date dexpiration : " +
                    creditCardExpiration);
        } else {

            return process(client, montant, "CB", creditCard,
                           new java.util.Date(creditCardExpiration.getTime()));
        }

    }

    private Payment process(Client client, double montant, String type,
            String creditCard, java.util.Date creditCardExpiration)

    {
        Payment payment = new Payment();
        payment.setClient(client);
        payment.setMontant(montant);
        payment.setDatePaiement(new java.util.Date());
        payment.setTypePayment(type);
        payment.setCreditCard(creditCard);
        payment.setCreditCardExpiration(creditCardExpiration);
        // La Mise à jour de la BD est différée !!!
        em.persist(payment);

        return payment;
    }
}

