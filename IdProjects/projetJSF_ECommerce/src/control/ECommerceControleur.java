/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package control;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.bean.*;
import javax.faces.event.ValueChangeEvent;
import metier.CartBean;
import metier.ItemFacade;
import modele.*;
import util.Achat;
import util.CardExpiredException;
import util.ItemNotFoundException;
import javax.validation.constraints.*;

/**
 *
 * @author didier
 */
@ManagedBean(name="itemBean")
@SessionScoped
public class ECommerceControleur implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	// injection de dependance
	// EJB stateful
    @EJB
    private CartBean cartBean;
    
    // EJB stateless  (CRUD sur l'entite donc la table ITEM)
    @EJB
    private ItemFacade itemFacade;
    
  


    
    private String categorieChoisie="livre";
    private int codeBarre;
    private String titre;
    private double prix=9.99;
    private Item itemSelectionne=null;
    private Achat achatSelectionne=null;
    private int id;
    
    
    private String nom="";
    private String prenom="";
    private boolean authentifie = false;
    
    
    private String numeroCB;
    private java.util.Date dateExpirationCB;
    
    private Client client=null;
    
    private int nouvQte=1;


    public String getCategorieChoisie() {
        return categorieChoisie;
    }

    public void setCategorieChoisie(String categorieChoisie) {
        this.categorieChoisie = categorieChoisie;
    }

    public int getCodeBarre() {
        return codeBarre;
    }

    public void setCodeBarre(int codeBarre) {
        this.codeBarre = codeBarre;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public Item getItemSelectionne() {
        return itemSelectionne;
    }

    public void setItemSelectionne(Item itemSelectionne) {
        this.itemSelectionne=itemSelectionne;
    }

    public Achat getAchatSelectionne() {
        return achatSelectionne;
    }

    public void setAchatSelectionne(Achat achatSelectionne) {
        this.achatSelectionne = achatSelectionne;
    }

    public boolean isAuthentifie() {
        return authentifie;
    }

    public void setAuthentifie(boolean authentifie) {
        this.authentifie = authentifie;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Date getDateExpirationCB() {
        return dateExpirationCB;
    }

    public void setDateExpirationCB(Date dateExpirationCB) {
        this.dateExpirationCB = dateExpirationCB;
    }

    public String getNumeroCB() {
        return numeroCB;
    }

    public void setNumeroCB(String numeroCB) {
        this.numeroCB = numeroCB;
    }
    
    
    
    
    
    public List<Item> getEnsItems(){
        return itemFacade.findAll();
    }
    
    public List<Achat> getContenuPanier(){
        return this.cartBean.getContents();
    }
    
    public double getValeurPanier(){
        return this.cartBean.getTotalPrice();
    }
    
    

    public String doListeItems(){
        categorieChoisie="produit";
        return "showItems";
    }


    // ajout d'un article dans le panier
    public String doSelectItem(Item item){
        this.itemSelectionne = item;
        this.cartBean.addItem(item.getCodeBarre(), 1);
        return "showPanier";
    }
    
    // retrait d'un article du panier
    public String doSupprimerAchat(Achat achat){
        //this.achatSelectionne = achat;
        try {
               this.cartBean.removeItem(achat.getCode());
  
        } catch (ItemNotFoundException ex) {
            Logger.getLogger(ECommerceControleur.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(this.cartBean.isPanierVide()){
            return "showItems";
        }
        return "showPanier";
    }
    
    
    // modification d'un article du panier
    public String doModifierAchat(Achat achat){
       // this.achatSelectionne = achat;
        try {
               this.cartBean.updateItem(achat.getCode(),nouvQte); 
                                          //nouvQte mis à jour par l'event
        } catch (ItemNotFoundException ex) {
            Logger.getLogger(ECommerceControleur.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    public String doPaiement(){
    	return "paiement";
    }
    
    public String doLogin(){
        this.client = this.cartBean.findOrCreateClient(this.nom, this.prenom);
        this.authentifie=true;
        
        return "showItems";
    }
    
    public String doHome(){
        this.client=null;
        this.authentifie=false;
        return "index";
    }


    
    public String doValidatation(){
        this.cartBean.setInfoCB(numeroCB, dateExpirationCB);
        try {
            this.cartBean.purchase(); //achat
            this.cartBean.checkout(); 
        } catch (CardExpiredException ex) {
            return "erreurCB";
        }
        return "logout";
    }
    
    
    // Traitement d'un evenement
    // methode void ayant un argument ValueChangeEvent
    public void updateQte(ValueChangeEvent event){
        nouvQte = (Integer) event.getNewValue();
    }

}
