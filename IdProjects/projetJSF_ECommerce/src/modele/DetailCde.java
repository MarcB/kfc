package modele;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the detail_cde database table.
 * 
 */
@Entity
@Table(name="detail_cde")
@NamedQuery(name="DetailCde.findAll", query="SELECT d FROM DetailCde d")
public class DetailCde implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private int qte;

	//bi-directional many-to-one association to Item
	@ManyToOne
	private Item item;

	//bi-directional many-to-one association to Commande
	@ManyToOne
	@JoinColumn(name="cde_id")
	private Commande commande;

	public DetailCde() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getQte() {
		return this.qte;
	}

	public void setQte(int qte) {
		this.qte = qte;
	}

	public Item getItem() {
		return this.item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public Commande getCommande() {
		return this.commande;
	}

	public void setCommande(Commande commande) {
		this.commande = commande;
	}

}