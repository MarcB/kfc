package modele;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;


/**
 * The persistent class for the commande database table.
 * 
 */
@Entity
@NamedQuery(name="Commande.findAll", query="SELECT c FROM Commande c")
public class Commande implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Temporal(TemporalType.DATE)
	private Date datecde;

	//bi-directional many-to-one association to Payment
	@ManyToOne
	private Payment payment;

	//bi-directional many-to-one association to Client
	@ManyToOne
	private Client client;

	//bi-directional many-to-one association to DetailCde
	@OneToMany(mappedBy="commande")
	private Set<DetailCde> detailCdes = new HashSet<>();

	public Commande() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDatecde() {
		return this.datecde;
	}

	public void setDatecde(Date datecde) {
		this.datecde = datecde;
	}

	public Payment getPayment() {
		return this.payment;
	}

	public void setPayment(Payment payment) {
		this.payment = payment;
	}

	public Client getClient() {
		return this.client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Set<DetailCde> getDetailCdes() {
		return this.detailCdes;
	}

	public void setDetailCdes(Set<DetailCde> detailCdes) {
		this.detailCdes = detailCdes;
	}

	public DetailCde addDetailCde(DetailCde detailCde) {
		getDetailCdes().add(detailCde);
		detailCde.setCommande(this);

		return detailCde;
	}

	public DetailCde removeDetailCde(DetailCde detailCde) {
		getDetailCdes().remove(detailCde);
		detailCde.setCommande(null);

		return detailCde;
	}

}