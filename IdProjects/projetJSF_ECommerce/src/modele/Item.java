package modele;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the item database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Item.findAll", query="SELECT i FROM Item i"),
	@NamedQuery(name="Item.findByCodeBarre",
	            query="SELECT i FROM Item i WHERE i.codeBarre=:codeBarre")      
})
public class Item implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String categorie;

	@Column(name="code_barre")
	private int codeBarre;

	private double prix;

	private String titre;

	//bi-directional many-to-one association to DetailCde
	@OneToMany(mappedBy="item")
	private Set<DetailCde> detailCdes;

	public Item() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCategorie() {
		return this.categorie;
	}

	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}

	public int getCodeBarre() {
		return this.codeBarre;
	}

	public void setCodeBarre(int codeBarre) {
		this.codeBarre = codeBarre;
	}

	public double getPrix() {
		return this.prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public String getTitre() {
		return this.titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public Set<DetailCde> getDetailCdes() {
		return this.detailCdes;
	}

	public void setDetailCdes(Set<DetailCde> detailCdes) {
		this.detailCdes = detailCdes;
	}

	public DetailCde addDetailCde(DetailCde detailCde) {
		getDetailCdes().add(detailCde);
		detailCde.setItem(this);

		return detailCde;
	}

	public DetailCde removeDetailCde(DetailCde detailCde) {
		getDetailCdes().remove(detailCde);
		detailCde.setItem(null);

		return detailCde;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codeBarre;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Item))
			return false;
		Item other = (Item) obj;
		if (codeBarre != other.codeBarre)
			return false;
		return true;
	}
	
	

}