package util;

import java.io.Serializable;

/**
 *
 * @author didier
 */
public class Achat implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int code;
    private String title;
    private double price=0.0d;
    private int quantite=1;

    public Achat(int code, int quantite){
        this.setCode(code);
        this.setQuantite(quantite);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

	@Override
	public String toString() {
		return "Achat [code=" + code + ", title=" + title + ", price=" + price + ", quantite=" + quantite + "]";
	}
    
    

}
