package util;

import java.io.Serializable;

/**
 *
 * @author didier
 */
public class CardExpiredException extends Exception implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CardExpiredException(String message) {
		super(message);
	}

	public CardExpiredException() {
		super();
	}
}
