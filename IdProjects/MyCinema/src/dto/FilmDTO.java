package dto;
import java.io.Serializable;
public class FilmDTO implements  Serializable{

    private static  final long serialVersionUID =1l;

    private int nfilm;
    private String titre;
    private String nom_realisateur;
    private String nom_acteur;

    public int getNfilm() {return nfilm;}
    public String getTitre() {return titre;}
    public String getNom_realisateur() {return  nom_realisateur;}
    public String getNom_acteur() {return nom_acteur;}

    public FilmDTO(int nfilm,String titre,String nom_realisateur,String nom_acteur){
        super();
        this.nfilm=nfilm;
        this.titre=titre;
        this.nom_realisateur=nom_realisateur;
        this.nom_acteur=nom_acteur;
    }

    @Override
    public String toString() {
        return "nfilm=" + nfilm +
                ", titre='" + titre + '\'' +
                ", nom_realisateur='" + nom_realisateur + '\'' +
                ", nom_acteur='" + nom_acteur+"\n"  ;

    }
}
