package dto;

import java.util.*;

import java.io.Serializable;
public class ActeurDTO implements  Serializable{

    private static  final long serialVersionUID =1l;

    private int nacteurs;

    private String nom;
    private String prenom;
    private Date naissance;
    private String nationalite;
    private int nbreFilms;

    public int getNacteurs() {
        return nacteurs;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public Date getNaissance() {
        return naissance;
    }

    public String getNationalite() {
        return nationalite;
    }

    public int getNbreFilms() {
        return nbreFilms;
    }
}
