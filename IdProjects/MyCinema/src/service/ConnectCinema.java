package service;

import java.sql.*;

public class ConnectCinema {
    private final static String URL = "jdbc:mysql://localhost:3306/mycinema";
    private final static String USER = "marc";
    private final static String PW = "root";

    private static Connection INSTANCE;


    private ConnectCinema() {
        try {
            INSTANCE = DriverManager.getConnection(URL, USER, PW);
        } catch (SQLException e) {
            e.printStackTrace();

        }
    }

    public static  Connection getInstance(){
        if(INSTANCE==null){
            new ConnectCinema();
        }
        return INSTANCE;
    }
    public static void main(String[] args) {
        ConnectCinema.getInstance();
    }
}
