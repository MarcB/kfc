package metier;

import dto.ActeurDTO;
import service.ConnectCinema;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Navigation_Acteur {

    private Connection conn=null;
    private Statement stmt =null;
    private ResultSet acteurRS;


    private final static String SQLselectActeurs=
            " select nacteur,a.nom as nom,prenom, "
            +" naissance,p.nom as nationalite,nbreFilms "
            +" from acteur a "
            +" inner join Pays p "
            +" on  nationalite = npays "
            +" order by a.nom,prenom ";



    public Navigation_Acteur(){

    }

    public  void ouvrirConnection() throws SQLException{
        this.conn= ConnectCinema.getInstance();

        this.stmt=conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
        this.acteurRS=stmt.executeQuery(SQLselectActeurs);
    }

    public void fermerConnection() throws  SQLException{
        if(this.stmt!=null){
            stmt.close();
        }
        if(this.conn!=null){
            conn.close();
        }
    }

    public ActeurDTO getFirstActeur() throws  SQLException {
        if(acteurRS==null) return null;

        acteurRS.first();

        int nacteur= acteurRS.getInt(1);
        String nom=acteurRS.getString(2);
        String prenom=acteurRS.getString(3);
        java.util.Date naissance=acteurRS.getDate((4));
        String nationalite=acteurRS.getString((5));
        int nbreFilms=acteurRS.getInt(6);
        return new ActeurDTO(nacteur,nom,prenom,naissance,nationalite,nbreFilms);
    }

    public ActeurDTO getNextActeur() throws SQLException{
        if(this.acteurRS==null) return null;
        if(this.acteurRS.isLast())return null;





    }
}
