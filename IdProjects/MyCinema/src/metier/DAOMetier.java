package metier;

import dto.ActeurDTO;
import dto.FilmDTO;
import dto.GenreDTO;
import service.ConnectCinema;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.sql.*;
import java.io.IOException;

public class DAOMetier implements IDAOMetier {
    private final static String SQLfindAllGenres =
            "select NGENRE,NATURE from GENRE order by NATURE";

    private final static String SQLfindNbFilmsByGenre =
            "select count(*) as NB from FILM where NGENRE=?";

    private final static String SQLfindAllFilmsByGenre =
            " select NFILM,TITRE,REALISATEUR as NOM_REALISATEUR, NOM as NOM_ACTEUR "
                    + " from FILM "
                    + " INNER JOIN ACTEUR "
                    + " on NACTEURPRINCIPAL = NACTEUR "
                    +" where ngenre=? "
                    + " order by TITRE ";

    private final static String SQLfindFilmById =
            " select NFILM,TITRE,REALISATEUR as NOM_REALISATEUR ,NOM as NOM_ACTEUR "
                    + " from FILM "
                    + " inner join ACTEUR "
                    + " on NACTEURPRINCIPAL = NACTEUR "
                    + " WHERE NFILM = ? ";

   /* private final static String SQLfindALLActeurs=
            " select nacteur.a.nom"*/


    @Override
    public List<GenreDTO> ensGenres() {
        List<GenreDTO> liste = new ArrayList<>();

        try {
            Statement instr =
                    ConnectCinema.getInstance().createStatement();
            ResultSet rs = instr.executeQuery(SQLfindAllGenres);

            while (rs.next()) {
                int ngenre = rs.getInt(1);
                String nature = rs.getString("NATURE");

                liste.add(new GenreDTO(ngenre, nature));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return liste;
    }

    @Override
    public long nbreFilmsDuGenre(int unGenre) {
        try {
            PreparedStatement ps =
                    ConnectCinema.getInstance().prepareStatement(SQLfindNbFilmsByGenre);

            ps.setInt(1, unGenre);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                long nb = rs.getLong("NB");

                return nb;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1L;
    }

    @Override
    public List<FilmDTO> ensFilmsDuGenre(int unGenre) {
        List<FilmDTO> liste = new ArrayList<>();
        try {
            PreparedStatement ps =
                    ConnectCinema.getInstance().prepareStatement(SQLfindAllFilmsByGenre);

            ps.setInt(1, unGenre);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                int nfilm = rs.getInt("nfilm");
                String titre = rs.getString("titre");
                String nom_realisateur = rs.getString(3);
                String nom_acteur = rs.getString(4);

                FilmDTO f = new FilmDTO(nfilm, titre, nom_realisateur, nom_acteur);
                liste.add(f);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return liste;

    }


    @Override
    public FilmDTO infoRealisateurEtActeur(int unFilm){
        FilmDTO f=null;
        try{
            PreparedStatement ps= ConnectCinema.getInstance().prepareStatement(SQLfindFilmById);

            ps.setInt(1,unFilm);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                int nfilm = rs.getInt("nfilm");
                String titre = rs.getString("titre");
                String nom_realisateur = rs.getString(3);
                String nom_acteur = rs.getString(4);

                f = new FilmDTO(nfilm, titre, nom_realisateur, nom_acteur);
            }
            }catch(SQLException e){e.printStackTrace();}
        return f;
        }



    //singleton précoce

    private static DAOMetier INSTANCE =new DAOMetier();
    public  static IDAOMetier getInstance(){return INSTANCE;}

    //-----------------

    @Override
    public List<ActeurDTO> ensActeurs() {
        List<ActeurDTO> liste =new ArrayList <>();


    }

    @Override
    public List<FilmDTO> ensTitresDunActeur(int unActeur) {
        return null;
    }

    public static void main(String[] args) {
        DAOMetier dao = new DAOMetier();
        System.out.println(dao.ensGenres()+"\n nombre de film du genre :"+dao.nbreFilmsDuGenre(1));
        System.out.println( dao.ensFilmsDuGenre(6));
        FilmDTO f=dao.infoRealisateurEtActeur(6);
        System.out.println(f.getTitre()+"\n");

    }
}
