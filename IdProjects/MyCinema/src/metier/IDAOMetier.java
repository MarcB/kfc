package metier;

import dto.ActeurDTO;
import dto.FilmDTO;
import dto.GenreDTO;

import java.util.List;

public interface IDAOMetier {
    List<GenreDTO> ensGenres();

    long nbreFilmsDuGenre(int unGenre);

    List<FilmDTO> ensFilmsDuGenre(int unGenre);

    List<ActeurDTO> ensActeurs();

    List<FilmDTO> ensTitresDunActeur(int unActeur);
}
