package popschool.mycinamespringsqlthymleaf.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import popschool.mycinamespringsqlthymleaf.dao.*;
import popschool.mycinamespringsqlthymleaf.model.Emprunt;
import popschool.mycinamespringsqlthymleaf.service.VideoService;

@Controller
public class MainController {

    @Autowired
    private GenreRepository genreRepository;
    @Autowired
    private FilmRepository filmRepository;
    @Autowired
    private ActeurRepository acteurRepository;
    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private EmpruntRepository empruntRepository;
    @Autowired
    private VideoService videoService;

    //----------------------------------------------------------------------------------------------------------------//
    //TODO                                      AFFICHER LES FILMS D'UN GENRE
    //----------------------------------------------------------------------------------------------------------------//
    //renvoie à la page selection genre et recupère la liste des genres
    @RequestMapping(value = {"/selectionGenre"}, method = RequestMethod.GET)
    public String selectionGenre(Model model) {

        model.addAttribute("listegenre", genreRepository.findAll());

        return "selectionGenre";
    }

    //renvoie à la page listfilm et recupère la liste des film du genre selectionné
    @PostMapping("/voirfilm" )
    public String films(@RequestParam("genrechoisi")String nature, Model model){
        model.addAttribute("ensfilmchoisi", filmRepository.findByGenre_Nature(nature));
        return "listfilms";
    }

    //----------------------------------------------------------------------------------------------------------------//
    //TODO                       AFFICHER LES ACTEUR ET LA LISTYE DE FILM D'UN ACTEUR CHOISI
    //----------------------------------------------------------------------------------------------------------------//
    @RequestMapping(value = {"/listeacteur"}, method = RequestMethod.GET)
    public String listeacteur(Model model) {

        model.addAttribute("ensacteur", acteurRepository.findAll());

        return "listacteur";
    }

    //renvoie à la page listfilm et recupère la liste des film du genre selectionné
    @PostMapping("/voirfilmbyacteur" )
    public String voirfilmbyacteur(@RequestParam("acteurchoisi")String nomacteur, Model model){
        model.addAttribute("ensfilmchoisi", filmRepository.findByActeur_Nom(nomacteur));
        return "listfilms";
    }

    //----------------------------------------------------------------------------------------------------------------//
    //TODO                                          EMPRUNTER UN FILM
    //----------------------------------------------------------------------------------------------------------------//
    @RequestMapping(value = {"/emprunter"}, method = RequestMethod.GET)
    public String listclientandfilm(Model model) {

        model.addAttribute("ensclient", clientRepository.findAll());
        model.addAttribute("ensfilm", videoService.ensFilmEmpruntable());

        return "emprunter";
    }
    @PostMapping(value = {"/empruntersave"})
    public String emprunter(@RequestParam("clientchoisi")String clientchoisi, @RequestParam("filmchoisi")String filmchoisi, Model model) {

        videoService.emprunter(clientchoisi, filmchoisi);
        model.addAttribute("ensemprunt", videoService.findAllEmprunt());
        return "listemmprunt";

    }

    @RequestMapping(value = {"/listemprunter"}, method = RequestMethod.GET)
    public String listemprunt(Model model) {

        model.addAttribute("ensemprunt", videoService.findAllEmprunt());

        return "listemmprunt";
    }

    //----------------------------------------------------------------------------------------------------------------//
    //TODO                                          LISTE DES EMRPRUNT
    //----------------------------------------------------------------------------------------------------------------//
    @RequestMapping(value = {"/listdesemprunt"}, method = RequestMethod.GET)
    public String listdesemprunt(Model model) {

        model.addAttribute("ensemprunt", videoService.empruntNonRendu());

        return "retouremprunt";
    }
    //----------------------------------------------------------------------------------------------------------------//
    //TODO                                          RENDRE UN FILM
    //----------------------------------------------------------------------------------------------------------------//
    @PostMapping(value = {"/retouremprunt"})
    public String retouremprunter(@RequestParam("empruntchoisi")Long empruntchoisi, Model model) {

        Emprunt emprunt = videoService.empruntById(empruntchoisi);
        videoService.retourEmprunt(emprunt.getClient().getNom(),emprunt.getFilm().getTitre());
        model.addAttribute("ensemprunt", videoService.findAllEmprunt());
        return "listemmprunt";

    }

    //----------------------------------------------------------------------------------------------------------------//
    //TODO                                          GESTION CLIENT
    //----------------------------------------------------------------------------------------------------------------//
    @RequestMapping(value = {"/gererclient"}, method = RequestMethod.GET)
    public String gestionclient(Model model) {

        model.addAttribute("ensclient", videoService.ensClient());

        return "gestionclient";
    }

}
