package popschool.mycinamespringsqlthymleaf.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import popschool.mycinamespringsqlthymleaf.model.Acteur;

import java.util.Date;
import java.util.List;
import java.util.Optional;


public interface ActeurRepository  extends CrudRepository<Acteur, Long> {

    Optional<Acteur> findByNomLike(String nom);

    List<Acteur> findByPrenomLike(String prenom);

    List<Acteur> findByNaissanceAfter(Date date);

    List<Acteur> findByNaissanceBefore(Date date);


    List<Acteur> findByNbrefilmsGreaterThan(int nbrefilms);

    List<Acteur> findByNomContaining(String nom);

    List<Acteur> findByPays_Nom(String pays);

    //Methode dto
    @Query(value = "select new popschool.mycinamespringsqlthymleaf.model.FilmDto(a.pays.nom, count(a)) " +
            " from Acteur a " +
            " group by a.pays.nom")
    List<popschool.mycinamespringsqlthymleaf.model.FilmDto> findNbreActeurByPays();

}
