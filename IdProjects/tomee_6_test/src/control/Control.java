package control;

import javax.annotation.ManagedBean;

@ManagedBean
public class Control {
    private  String nom="tomee";

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}
