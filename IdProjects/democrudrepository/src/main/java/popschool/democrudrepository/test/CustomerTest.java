package popschool.democrudrepository.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import popschool.democrudrepository.DemocrudrepositoryApplication;
import popschool.democrudrepository.dao.CustomerRepository;
import popschool.democrudrepository.model.Customer;

import java.util.Optional;

@Component
public class CustomerTest implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(DemocrudrepositoryApplication.class);

    @Autowired
    private CustomerRepository customerRepository;


    private void information(Customer c){
        log.info(c.toString());
    }

    @Override
    public void run(String... args) throws Exception {
        //Save a few customer
        customerRepository.save(new Customer("Jack","Bauer"));
        customerRepository.save(new Customer("Chloe","O'Brian"));
        customerRepository.save(new Customer("Kim","Bauer"));
        customerRepository.save(new Customer("David","Palmer"));
        customerRepository.save(new Customer("Michelle","Berte"));

        //fetch all customer
        log.info("Customer found with findAll():");
        log.info("--------------------------");
        for (Customer customer : customerRepository.findAll()){
            log.info(customer.toString());
        }
        log.info("");

        //fetch an individual customer By id
        Optional<Customer> customer = customerRepository.findById(1L);
        log.info("Customer found with findById()");
        log.info("-----------------------------");
        if (customer.isPresent()){
            Customer cust = customer.get();
            log.info(customer.get().toString());
            log.info("");
            customerRepository.delete(cust);
            customerRepository.findAll().forEach(this::information);
            log.info("");
        }

        //fetch customers by lastname
        log.info("Customer found with findByLastName (Bauer)");
        log.info("--------------------");
        customerRepository.findByLastName("Bauer").forEach(bauer -> {
            log.info(bauer.toString());
        });
        log.info("");
    }
}
