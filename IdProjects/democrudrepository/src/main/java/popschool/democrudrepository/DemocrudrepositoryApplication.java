package popschool.democrudrepository;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemocrudrepositoryApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemocrudrepositoryApplication.class, args);
    }

}
