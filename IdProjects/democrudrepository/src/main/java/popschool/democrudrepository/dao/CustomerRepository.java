package popschool.democrudrepository.dao;

import org.springframework.data.repository.CrudRepository;
import popschool.democrudrepository.model.Customer;

import java.util.List;


public interface CustomerRepository extends CrudRepository<Customer, Long> {

    List<Customer> findByLastName(String lastName);

}
