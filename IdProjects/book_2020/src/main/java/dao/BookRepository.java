package dao;

import org.springframework.stereotype.Repository;

import popschool.book_2020.model.Book;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

//creation d'une interface pour communiquer les listes
public interface BookRepository {
    int count();

    int save(Book book);
    int update(Book book);
    int deleteById(Long id);

    List<Book> findAll();
    List<Book>findAllByNameAndPrice(String name, BigDecimal price);
    Optional<Book> findById(Long id);

    String getNameById(Long id);


}
