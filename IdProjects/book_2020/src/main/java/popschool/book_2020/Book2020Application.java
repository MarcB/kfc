package popschool.book_2020;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import popschool.book_2020.dao.BookRepository;
import popschool.book_2020.model.Book;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootApplication
public class Book2020Application implements CommandLineRunner {
    //permet de chois ou va s'ecrire le log par defaut dans la console avec pintln
    private static final Logger log = LoggerFactory.getLogger(Book2020Application.class);

    public static void main(String[] args) {

        SpringApplication.run(Book2020Application.class, args);
    }
    @Autowired
    @Qualifier("namedParameterJdbcBookRepository") //nom de la classe avec une minuscule en premier
    private BookRepository bookRepository;

    @Override
    public void run(String... args) throws Exception {
        log.info("StartApplication...");
        runJDBC();
    }

    private void runJDBC(){

        //ajout des livre dans la base de données
        List<Book> books= Arrays.asList(
        new Book("1_java",new BigDecimal("46.50")),
        new Book("2_java",new BigDecimal("19.90")),
        new Book("3_test",new BigDecimal("15.90")),
        new Book("4_test",new BigDecimal("30.00"))
        );
        log.info("[SAVE]");
        books.forEach(book -> {log.info("Saving ..{}",book.getName());
        bookRepository.save(book);
        });

        System.out.println("avant modif");
        List<Book> list = bookRepository.findAll();
        System.out.println(list);

        Book bookUpdate = new Book(new Long("1"),new BigDecimal("16.32"));
        bookRepository.update(bookUpdate);

        System.out.println("apres modif");
        List<Book> list1 = bookRepository.findAll();
        System.out.println(list1);

        //bookRepository.delete(bookUpdate);

        System.out.println(" - -------------------------apres suppression -------------------------------------------");
        List<Book> list2 = bookRepository.findAll();
        System.out.println(list2);

        System.out.println("-------------------------- find by id ---------------------------------------------------");
        Optional<Book> livre4 = bookRepository.findById(4L);
        System.out.println(livre4.isPresent()? livre4.get() : "Absent");

        Optional<Book> livre5 = bookRepository.findById(5L);
        System.out.println(livre5.isPresent()? livre5.get() : "Absent");

        System.out.println("------------------------- find by name and price ----------------------------------------");
        List<Book> list3 = bookRepository.findByNameAndPrice("jav" , new BigDecimal(10));
        System.out.println(list3);

        System.out.println("-------------------------------- find name by id ----------------------------------------");
        System.out.println(bookRepository.getNameById(5L));
    }


}
