package popschool.book_2020.dao;

import popschool.book_2020.model.Book;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface BookRepository {
    int count();

    int save(Book book);
    int update(Book book);
    int delete(Book book);

    List<Book>findAll();
    List<Book>findByNameAndPrice(String name, BigDecimal price);

    Optional<Book> findById(Long id);

    String getNameById(Long id);

}

