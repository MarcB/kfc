package popschool.book_2020.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import popschool.book_2020.model.Book;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Repository
public class JdbcBookRepository implements BookRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    //------------------------------------------------------------------------------------------------------------------
    //--------------------------------------- Methode count ------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------
    @Override
    public int count() {
        return 0;
    }

    @Override
    public int save(Book book) {
        return jdbcTemplate.update(
                "insert into books (name,price) values (?,?)",
                book.getName(),
                book.getPrice()
        );
    }

    //------------------------------------------------------------------------------------------------------------------
    //------------------------------------------------ Méthode update --------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------
    @Override
    public int update(Book book) {
        return jdbcTemplate.update("update books set price = ? where id = ? ",
                book.getPrice(),
                book.getId());
    }

    //------------------------------------------------------------------------------------------------------------------
    //------------------------------------------------ Méthode update --------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------
    @Override
    public int delete(Book book) {
        return jdbcTemplate.update("delete from books where id=?",
                book.getId());
    }
    //------------------------------------------------------------------------------------------------------------------
    //------------------------ surcharge du findAll de l'interface -----------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------
    @Override
    public List<Book> findAll(){
        String sql = "select * from books";
        List<Book> list = jdbcTemplate.query(
                sql,
                //avec reference de methode, il faut créer la méthode mapBook
                this::mapBook);
                //a la place du this::mapBook); en lamda expression
                //(rs, num) ->{return new Book(
                                //rs.getLong("id"),
                                //rs.getString("name"),
                                //rs.getBigDecimal("price")});
        return list;
    }

    //Creation de la methode mapBook qui récupere les donnée pour le findAll pour la reference de methode
    protected Book mapBook(ResultSet rs, int row)throws SQLException{
        return new Book(
                rs.getLong("id"),
                rs.getString("name"),
                rs.getBigDecimal("price")
        );
    }


    //------------------------------------------------------------------------------------------------------------------
    //----------------------------------- Methode find by name and price -----------------------------------------------
    //------------------------------------------------------------------------------------------------------------------
    @Override
    public List<Book> findByNameAndPrice(String name, BigDecimal price) {
        return jdbcTemplate.query("select * from books where name like ? and price <= ?",
                this::mapBook,
                "%" + name + "%",
                price);
    }

    //------------------------------------------------------------------------------------------------------------------
    //-------------------------------------- Methode find by id --------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------
    //jdbcTemplate.queryForList, populates a single object
    @Override
    public Optional<Book> findById(Long id) {
        return jdbcTemplate.query("select * from books where id = ?",
                this::resultSetExtractorOptionalBook,   //on utilise le resultSet extractor car il faut pouvoir
                id);                                    // controler dans la methode que le reslutSet n'est pas null
    }

    protected Optional<Book> resultSetExtractorOptionalBook(ResultSet rs) throws SQLException {
        if (rs.next()){
            return Optional.ofNullable(new Book(rs.getLong("id"),
                    rs.getString("name"),
                    rs.getBigDecimal("price")));}
        return Optional.ofNullable(null);
    }

    @Override
    public String getNameById(Long id) {
        try{
            return jdbcTemplate.queryForObject(
                "select name from books where id = ?",
                String.class,
                id
        );
        }catch (EmptyResultDataAccessException ex){
            return "N'existe pas";
        }
    }


}
