package popschool.book_2020.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import popschool.book_2020.model.Book;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
@Repository
public class NamedParameterJdbcBookRepository extends JdbcBookRepository{

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    public int update(Book book) {
        return namedParameterJdbcTemplate.update(
                "update books set price = :price where id = :id ",
                new BeanPropertySqlParameterSource(book));
    }

    @Override
    public Optional<Book> findById(Long id) {
        return namedParameterJdbcTemplate.query(
                "select * from books where id= :id",
                new MapSqlParameterSource("id",id),
                super::resultSetExtractorOptionalBook
        );
    }

    @Override
    public List<Book> findByNameAndPrice(String name, BigDecimal price) {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("name", "%" + name + "%");
        mapSqlParameterSource.addValue("price", price);

        return namedParameterJdbcTemplate.query("select * from books where name like :name and price <= :price",
                mapSqlParameterSource,
                (rs, rowNum)->new Book(
                        rs.getLong("id"),
                        rs.getString("name"),
                        rs.getBigDecimal("price")
                ));
    }
}
