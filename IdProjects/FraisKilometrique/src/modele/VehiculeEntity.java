package modele;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;

@Entity
@NamedQueries({
        //liste v admin
        @NamedQuery(name="vehicule.findAll",query="SELECT v FROM VehiculeEntity v"),
        //link combo tableau
        @NamedQuery(name="vehicule.byImma",
                query="SELECT v FROM VehiculeEntity v WHERE upper(v.immatriculation)=upper(:immatriculaion)"),
        //link ou tableau
        @NamedQuery(name="vehicule.findById",
                query="SELECT v FROM VehiculeEntity v WHERE v.nvehicule= ?1")

})


@Table(name = "vehicule", schema = "test")
public class VehiculeEntity {
    private int nvehicule;
    private String immatriculation;
    private String puissance;
    private String marque;
    private String possession;
    private byte[] carte;
    private int vehpers;
   // private Collection<TrajetEntity> trajetsByNvehicule;
    private PersonneEntity personneByVehpers;

    @Id
    @Column(name = "nvehicule")
    public int getNvehicule() {
        return nvehicule;
    }

    public void setNvehicule(int nvehicule) {
        this.nvehicule = nvehicule;
    }

    @Basic
    @Column(name = "immatriculation")
    public String getImmatriculation() {
        return immatriculation;
    }

    public void setImmatriculation(String immatriculation) {
        this.immatriculation = immatriculation;
    }

    @Basic
    @Column(name = "puissance")
    public String getPuissance() {
        return puissance;
    }

    public void setPuissance(String puissance) {
        this.puissance = puissance;
    }

    @Basic
    @Column(name = "marque")
    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    @Basic
    @Column(name = "possession")
    public String getPossession() {
        return possession;
    }

    public void setPossession(String possession) {
        this.possession = possession;
    }

    @Basic
    @Column(name = "carte")
    public byte[] getCarte() {
        return carte;
    }

    public void setCarte(byte[] carte) {
        this.carte = carte;
    }

    @Basic
    @Column(name = "vehpers")
    public int getVehpers() {
        return vehpers;
    }

    public void setVehpers(int vehpers) {
        this.vehpers = vehpers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VehiculeEntity that = (VehiculeEntity) o;
        return nvehicule == that.nvehicule &&
                vehpers == that.vehpers &&
                Objects.equals(immatriculation, that.immatriculation) &&
                Objects.equals(puissance, that.puissance) &&
                Objects.equals(marque, that.marque) &&
                Objects.equals(possession, that.possession) &&
                Arrays.equals(carte, that.carte);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(nvehicule, immatriculation, puissance, marque, possession, vehpers);
        result = 31 * result + Arrays.hashCode(carte);
        return result;
    }

    /*
    @OneToMany(mappedBy = "vehiculeByImmatrajet")
    public Collection<TrajetEntity> getTrajetsByNvehicule() {
        return trajetsByNvehicule;
    }

    public void setTrajetsByNvehicule(Collection<TrajetEntity> trajetsByNvehicule) {
        this.trajetsByNvehicule = trajetsByNvehicule;
    }
*/

    /*
    @ManyToOne
    @JoinColumn(name = "vehpers", referencedColumnName = "npersonne", nullable = false)
    public PersonneEntity getPersonneByVehpers() {
        return personneByVehpers;
    }

    public void setPersonneByVehpers(PersonneEntity personneByVehpers) {
        this.personneByVehpers = personneByVehpers;
    }*/

    @Override
    public String toString() {
        return "VehiculeEntity{" +
                "nvehicule=" + nvehicule +
                ", immatriculation='" + immatriculation + '\'' +
                ", puissance='" + puissance + '\'' +
                ", marque='" + marque + '\'' +
                ", possession='" + possession + '\'' +
                ", carte=" + Arrays.toString(carte) +
                ", vehpers=" + vehpers +
                ", trajetsByNvehicule="  +
                ", personneByVehpers=" + personneByVehpers +
                '}';
    }
}
