package modele;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "personne", schema = "test" )
@NamedQueries({
        //le login
        @NamedQuery(name="user.findByMail",
                query=" SELECT p FROM PersonneEntity p  WHERE lower(p.mail)=lower(:mail)") ,

        //pour le combo admin
        @NamedQuery(name="user.findAll",
                query=" SELECT p FROM PersonneEntity p") ,

        //link ou tableau
         @NamedQuery(name="user.findById",
                query=" SELECT p FROM PersonneEntity p WHERE p.npersonne= ?1")
})

public class PersonneEntity {
    private int npersonne;
    private String mail;
    private String nom;
    private String prenom;
    private String pass;
    private byte[] permis;
    private String duty;
    //private Collection<VehiculeEntity> vehiculesByNpersonne;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "npersonne")
    public int getNpersonne() {
        return npersonne;
    }

    public void setNpersonne(int npersonne) {
        this.npersonne = npersonne;
    }

    @Basic
    @Column(name = "mail")
    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    @Basic
    @Column(name = "nom")
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Basic
    @Column(name = "prenom")
    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @Basic
    @Column(name = "pass")
    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    @Basic
    @Column(name = "permis")
    public byte[] getPermis() {
        return permis;
    }

    public void setPermis(byte[] permis) {
        this.permis = permis;
    }

    @Basic
    @Column(name = "duty")
    public String getDuty() {
        return duty;
    }

    public void setDuty(String duty) {
        this.duty = duty;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonneEntity that = (PersonneEntity) o;
        return npersonne == that.npersonne &&
                Objects.equals(mail, that.mail) &&
                Objects.equals(nom, that.nom) &&
                Objects.equals(prenom, that.prenom) &&
                Objects.equals(pass, that.pass) &&
                Arrays.equals(permis, that.permis) &&
                Objects.equals(duty, that.duty);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(npersonne, mail, nom, prenom, pass, duty);
        result = 31 * result + Arrays.hashCode(permis);
        return result;
    }

  /*  @OneToMany(mappedBy = "personneByVehpers")
    public Collection<VehiculeEntity> getVehiculesByNpersonne() {
        return vehiculesByNpersonne;
    }

    public void setVehiculesByNpersonne(Collection<VehiculeEntity> vehiculesByNpersonne) {
        this.vehiculesByNpersonne = vehiculesByNpersonne;
    }*/


    @Override
    public String toString() {
        return "PersonneEntity{" +
                "npersonne=" + npersonne +
                ", mail='" + mail + '\'' +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", pass='" + pass + '\'' +
                ", permis=" + Arrays.toString(permis) +
                ", duty='" + duty + '\'' +
                ", vehiculesByNpersonne=" +
                '}';
    }
}
