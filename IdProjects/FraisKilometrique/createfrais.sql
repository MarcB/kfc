DROP TABLE IF EXISTS personne ;
DROP TABLE IF EXISTS vehicule ;
DROP TABLE IF EXISTS trajet ; 

-- p

CREATE TABLE personne (
	npersonne int(8)  not null AUTO_INCREMENT,
	mail     varchar (30) NOT NULL ,
	nom    varchar(20) not null,
	prenom  varchar(15) not null,
	pass      varchar(8) not null ,
	permis LONGBLOB ,
	duty varchar(5) default "user" NOT NULL,
	-- immapers varchar(10) ,  
	-- constraint fk_imma foreign key (immapers) references vehicule(immatriculation),
	constraint uk_mail unique key (mail),
	CONSTRAINT pk_npersonne  PRIMARY KEY ( npersonne ) )engine=INNODB;

-- v

CREATE TABLE vehicule (
	nvehicule int(8)  not null AUTO_INCREMENT,
	immatriculation varchar(10) not null,
	puissance varchar(10) not null,
	marque varchar(20) NOT NULL,
	possession varchar(5) not null,
	carte LONGBLOB ,
	vehpers int (8) NOT NULL ,
	-- ntrajet int(8)  not null,
	constraint fk_vehpers foreign key (vehpers) references personne(npersonne),
	constraint uk_imma unique key (immatriculation),
	-- constraint fk_ntrajet foreign key (ntrajet) references trajet(ntrajet),
	CONSTRAINT pk_nvehicule	PRIMARY KEY ( nvehicule ) )engine=INNODB;

-- t

CREATE TABLE trajet (
	ntrajet int(8)  not null AUTO_INCREMENT,
	description varchar(30),
	debdate  timestamp DEFAULT CURRENT_TIMESTAMP not null,
	debadress varchar(30) not null,
	findate  timestamp DEFAULT CURRENT_TIMESTAMP not null,
	finadress varchar(30) not null,
	immatrajet int(8) NOT NULL,
	accepte varchar(3) default "non" not null,
	distance int(5) default 0 not null,
	montant float(10) default 0.00,
	mailtrajet  varchar (30) NOT NULL ,
	constraint fk_imma foreign key (immatrajet) references vehicule(nvehicule),
	constraint fk_mail foreign key (mailtrajet) references personne(mail),
	constraint pk_ntrajet primary key (ntrajet) )engine=INNODB;

