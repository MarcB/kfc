package controleur;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/process")

public class process extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request,reponse);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request,reponse);
    }

    //navigation
    private void forward(String url,HttpServletRequest request,HttpServletResponse reponse)throws  ServletException,IOException{
        RequestDispatcher rd = request.getRequestDispatcher(url);
        rd.forward(request,reponse);
    }

    //controleur
    protected void forward(String url,HttpServletRequest request,HttpServletResponse reponse)throws  ServletException,IOException {
        try {
            //recup de l action
            String action = request.getParameter("action");

            //execution du traitement associe a l action
            if (action.equals("authentification")) {
                doAuthentificate(request, reponse);
            } else if (action.equals("login")) {
                doLogin(request, reponse);
            } else if (action.equals("afficherEnrClient")) {
                doAfficherFormEnrClient(request, reponse);
            } else if (action.equals("enrClient")) {
                doEnrClient(request, reponse);
            }catch(Exception ex){
                ex.printStackTrace();
            }

        }

        protected void doLogin (HttpServletRequest request, HttpServletResponse response) throws
        ServletExeption, IOException {
            String url = "pages/login.jsp";
            forward(url, request, response);
        }

        protected void doEnrClient (HttpServletRequest request, HttpServletResponse response) throws
        ServletExeption, IOException {
            String url = "pages/enregistrement.jsp";
            forward(url, request, response);
        }
        protected void doAuthentification (HttpServletRequest request, HttpServletResponse response) throws
        ServletExeption, IOException {
            try {
                //recup des donnees du formulaire
                String alia = request.getParameter("alias");
                String passard = request.getParameter("password");

                Client client = ClientDao.getSingleton().find(alias, password);
                if (client != null) {
                    request.setAttribute("client",client);
                }

                String url = (client == null ? "pages/inconnu.jsp" : "pages/connu.jsp");
                forward(url, request, response);

            } catch (Exception ex) {
                System.err.println(ex.getMessage());
            }


        }


    }
}
