package modele;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "vehicule", schema = "test")
public class VehiculeEntity {
    private String immatriculation;
    private String puissance;
    private String marque;
    private String possession;
    private byte[] carte;
    private String vehmail;
    private Collection<TrajetEntity> trajetsByImmatriculation;
    private PersonneEntity personneByVehmail;

    @Id
    @Column(name = "immatriculation")
    public String getImmatriculation() {
        return immatriculation;
    }

    public void setImmatriculation(String immatriculation) {
        this.immatriculation = immatriculation;
    }

    @Basic
    @Column(name = "puissance")
    public String getPuissance() {
        return puissance;
    }

    public void setPuissance(String puissance) {
        this.puissance = puissance;
    }

    @Basic
    @Column(name = "marque")
    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    @Basic
    @Column(name = "possession")
    public String getPossession() {
        return possession;
    }

    public void setPossession(String possession) {
        this.possession = possession;
    }

    @Basic
    @Column(name = "carte")
    public byte[] getCarte() {
        return carte;
    }

    public void setCarte(byte[] carte) {
        this.carte = carte;
    }

    @Basic
    @Column(name = "vehmail")
    public String getVehmail() {
        return vehmail;
    }

    public void setVehmail(String vehmail) {
        this.vehmail = vehmail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VehiculeEntity that = (VehiculeEntity) o;
        return Objects.equals(immatriculation, that.immatriculation) &&
                Objects.equals(puissance, that.puissance) &&
                Objects.equals(marque, that.marque) &&
                Objects.equals(possession, that.possession) &&
                Arrays.equals(carte, that.carte) &&
                Objects.equals(vehmail, that.vehmail);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(immatriculation, puissance, marque, possession, vehmail);
        result = 31 * result + Arrays.hashCode(carte);
        return result;
    }

    @OneToMany(mappedBy = "vehiculeByImmatrajet")
    public Collection<TrajetEntity> getTrajetsByImmatriculation() {
        return trajetsByImmatriculation;
    }

    public void setTrajetsByImmatriculation(Collection<TrajetEntity> trajetsByImmatriculation) {
        this.trajetsByImmatriculation = trajetsByImmatriculation;
    }

    @ManyToOne
    @JoinColumn(name = "vehmail", referencedColumnName = "mail", nullable = false)
    public PersonneEntity getPersonneByVehmail() {
        return personneByVehmail;
    }

    public void setPersonneByVehmail(PersonneEntity personneByVehmail) {
        this.personneByVehmail = personneByVehmail;
    }
}
