package modele;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "trajet", schema = "test", catalog = "")
public class TrajetEntity {
    private int ntrajet;
    private String description;
    private Timestamp debdate;
    private String debadress;
    private Timestamp findate;
    private String finadress;
    private String immatrajet;
    private String mailtrajet;
    private VehiculeEntity vehiculeByImmatrajet;

    @Id
    @Column(name = "ntrajet")
    public int getNtrajet() {
        return ntrajet;
    }

    public void setNtrajet(int ntrajet) {
        this.ntrajet = ntrajet;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "debdate")
    public Timestamp getDebdate() {
        return debdate;
    }

    public void setDebdate(Timestamp debdate) {
        this.debdate = debdate;
    }

    @Basic
    @Column(name = "debadress")
    public String getDebadress() {
        return debadress;
    }

    public void setDebadress(String debadress) {
        this.debadress = debadress;
    }

    @Basic
    @Column(name = "findate")
    public Timestamp getFindate() {
        return findate;
    }

    public void setFindate(Timestamp findate) {
        this.findate = findate;
    }

    @Basic
    @Column(name = "finadress")
    public String getFinadress() {
        return finadress;
    }

    public void setFinadress(String finadress) {
        this.finadress = finadress;
    }

    @Basic
    @Column(name = "immatrajet")
    public String getImmatrajet() {
        return immatrajet;
    }

    public void setImmatrajet(String immatrajet) {
        this.immatrajet = immatrajet;
    }

    @Basic
    @Column(name = "mailtrajet")
    public String getMailtrajet() {
        return mailtrajet;
    }

    public void setMailtrajet(String mailtrajet) {
        this.mailtrajet = mailtrajet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TrajetEntity that = (TrajetEntity) o;
        return ntrajet == that.ntrajet &&
                Objects.equals(description, that.description) &&
                Objects.equals(debdate, that.debdate) &&
                Objects.equals(debadress, that.debadress) &&
                Objects.equals(findate, that.findate) &&
                Objects.equals(finadress, that.finadress) &&
                Objects.equals(immatrajet, that.immatrajet) &&
                Objects.equals(mailtrajet, that.mailtrajet);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ntrajet, description, debdate, debadress, findate, finadress, immatrajet, mailtrajet);
    }

    @ManyToOne
    @JoinColumn(name = "immatrajet", referencedColumnName = "immatriculation", nullable = false)
    public VehiculeEntity getVehiculeByImmatrajet() {
        return vehiculeByImmatrajet;
    }

    public void setVehiculeByImmatrajet(VehiculeEntity vehiculeByImmatrajet) {
        this.vehiculeByImmatrajet = vehiculeByImmatrajet;
    }
}
