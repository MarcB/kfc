package modele;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "personne", schema = "test", catalog = "")
public class PersonneEntity {
    private String mail;
    private String nom;
    private String prenom;
    private String pass;
    private byte[] permis;
    private String duty;
    private String immapers;
    private Collection<VehiculeEntity> vehiculesByMail;

    @Id
    @Column(name = "mail")
    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    @Basic
    @Column(name = "nom")
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Basic
    @Column(name = "prenom")
    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @Basic
    @Column(name = "pass")
    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    @Basic
    @Column(name = "permis")
    public byte[] getPermis() {
        return permis;
    }

    public void setPermis(byte[] permis) {
        this.permis = permis;
    }

    @Basic
    @Column(name = "duty")
    public String getDuty() {
        return duty;
    }

    public void setDuty(String duty) {
        this.duty = duty;
    }

    @Basic
    @Column(name = "immapers")
    public String getImmapers() {
        return immapers;
    }

    public void setImmapers(String immapers) {
        this.immapers = immapers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonneEntity that = (PersonneEntity) o;
        return Objects.equals(mail, that.mail) &&
                Objects.equals(nom, that.nom) &&
                Objects.equals(prenom, that.prenom) &&
                Objects.equals(pass, that.pass) &&
                Arrays.equals(permis, that.permis) &&
                Objects.equals(duty, that.duty) &&
                Objects.equals(immapers, that.immapers);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(mail, nom, prenom, pass, duty, immapers);
        result = 31 * result + Arrays.hashCode(permis);
        return result;
    }

    @OneToMany(mappedBy = "personneByVehmail")
    public Collection<VehiculeEntity> getVehiculesByMail() {
        return vehiculesByMail;
    }

    public void setVehiculesByMail(Collection<VehiculeEntity> vehiculesByMail) {
        this.vehiculesByMail = vehiculesByMail;
    }
}
