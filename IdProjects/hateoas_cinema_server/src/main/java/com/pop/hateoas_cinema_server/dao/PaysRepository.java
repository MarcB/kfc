package com.pop.hateoas_cinema_server.dao;

import com.pop.hateoas_cinema_server.model.Pays;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel="pays",path="pays")
public interface PaysRepository extends PagingAndSortingRepository<Pays, Long> {

}
