package com.pop.hateoas_cinema_server.dao;

import com.pop.hateoas_cinema_server.model.Genre;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel="genres",path="genres")
public interface GenreRepository extends PagingAndSortingRepository<Genre, Long> {

}
