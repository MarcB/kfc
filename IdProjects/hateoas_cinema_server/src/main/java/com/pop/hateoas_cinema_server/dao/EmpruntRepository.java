package com.pop.hateoas_cinema_server.dao;

import com.pop.hateoas_cinema_server.model.Emprunt;
import com.pop.hateoas_cinema_server.model.Film;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;
import java.util.Optional;

@RepositoryRestResource(collectionResourceRel="emprunt",path="emprunt")
public interface EmpruntRepository extends PagingAndSortingRepository<Emprunt, Long> {


    List<Emprunt>findByClient_Nclient(Long nclient);
    Optional<Emprunt> findByClient_NomAndFilm_TitreAndRetour(String nom, String titre, String non);
}
