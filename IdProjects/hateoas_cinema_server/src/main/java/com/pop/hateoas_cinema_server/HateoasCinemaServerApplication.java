package com.pop.hateoas_cinema_server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HateoasCinemaServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(HateoasCinemaServerApplication.class, args);
    }

}
