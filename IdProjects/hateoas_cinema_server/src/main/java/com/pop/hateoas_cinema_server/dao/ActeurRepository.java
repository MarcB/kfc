package com.pop.hateoas_cinema_server.dao;

import com.pop.hateoas_cinema_server.model.Acteur;
import com.pop.hateoas_cinema_server.model.Client;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel="acteur",path="acteur")
public interface ActeurRepository extends PagingAndSortingRepository<Acteur, Long> {

    List<Acteur> findAll();

    /*
    Optional<Acteur> findActeurByNom(String nom);
    List<Acteur> findByPrenomIsLike(String prenom);
    List<Acteur> findActeurByNaissanceAfter(Date date);
    List<Acteur> findActeurByNaissanceBefore(Date date);
    List<Acteur> findActeurByNomContaining(String nom);
    @Query("Select a from Acteur a where a.nbrefilms =(select max(a2.nbrefilms) from Acteur a2)")
    List<Acteur> findActeurByMaxFilms ();
    List<Acteur> findActeurByNationalite_Nom(String nom);
    List<Acteur> findActeurByNationalite_Npays(Long npays);
    List<Acteur> findActeurByNationalite_NomContaining(String nom);
    */

}