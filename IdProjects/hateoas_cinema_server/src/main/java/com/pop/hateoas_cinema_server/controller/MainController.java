package com.pop.hateoas_cinema_server.controller;


/*
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++
//    Index :
//        Consulter les acteurs
//        Consulter les films
//        Emprunter un film
//    Consulter les acteurs :
//        List des acteurs (nom, prenom)
//        choix -> list des films de cet acteur
//    Consulter les films
//        Liste des genres
//        choix -> liste des films de ce genre
//    Emprunter un film
//        Choisir un client et un film
//        Declencher l'emprunt
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++

@RestController
public class MainController {

    @Autowired
    private ActeurRepository acteurRepository;
    @Autowired
    private FilmRepository filmRepository;
    @Autowired
    private EmpruntRepository empruntRepository;
    @Autowired
    private GenreRepository genreRepository;
    @Autowired
    private ClientRepository clientRepository;



    // Injectez (inject) via application.properties.

    //list des acteurs ========================================================
    @RequestMapping(value = { "/acteur" }, method = RequestMethod.GET)
    public List<Acteur>actorList() {return acteurRepository.findAll();}

    //list des clients =========================================================
    @RequestMapping(value = { "/client" }, method = RequestMethod.GET)
    public List<Client> clientList() {return (List<Client>) clientRepository.findAll();}

    //list des emprunts by client(id) ==========================================
    @GetMapping("/emprunt/{id}")
    public List<Emprunt> emprunts(@PathVariable("id") long id){return empruntRepository.findByClient_Nclient(id);}

    //list des emprunts ========================================================
    @RequestMapping(value = { "/emprunt" }, method = RequestMethod.GET)
    public List<Emprunt> empruntList() {return (List<Emprunt>) empruntRepository.findAll();    }

    //list des films ===========================================================
    @RequestMapping(value = {"/film"}, method = RequestMethod.GET)
    public List<Film> filmList() {return (List<Film>) filmRepository.findAll();}

    //+ combo by genre ==========================================
    @RequestMapping(value = {"/genre"}, method = RequestMethod.GET)
    public Iterable<Genre> genreList() {return genreRepository.findAll();}

    //list des films by genre(nature) ==========================================
    @PostMapping("/film/{nature}" )
    public List<Film> filmListByNature(@PathVariable("nature") String nature){
        return filmRepository.findByGenre_Nature(nature);
    }
    //list des films by acteur(id) =============================================
    @PostMapping("/acteur/{id}" )
    public List<Film> filmByActeur(@PathVariable("id")Long nacteur){
       return filmRepository.findByActeur_Nacteur(nacteur);
    }

    //*****************************************
    //*------------- Modif client ------------*
    //*****************************************

    //client(id) =============================================
    @GetMapping("/client/{id}" )
    public Optional<Client> clientById(@PathVariable("id")Long nclient){
        return clientRepository.findById(nclient);
    }
    //ajout de client
    @PostMapping("/client/create")
    public void  addClient(@RequestBody Client client,
                    @PathVariable("id") long id){
        client.setNclient(id);
        clientRepository.save(client);
    }

    // update client
    @PutMapping(value = "/client/{id}")
    public void clientUpdate(@RequestBody Client client,@PathVariable("id") long id){
        client.setNclient(id);
        clientRepository.save(client);
    }

    //delete du client selon id
    @DeleteMapping("/client/{id}")
    public void deleteClient(@PathVariable("id") long id) {

        clientReposbitory.deleteById(id);
    }

}
*/