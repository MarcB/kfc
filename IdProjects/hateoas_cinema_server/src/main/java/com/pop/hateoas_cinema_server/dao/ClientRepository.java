package com.pop.hateoas_cinema_server.dao;

import com.pop.hateoas_cinema_server.model.Client;
import com.pop.hateoas_cinema_server.model.Emprunt;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;

@RepositoryRestResource(collectionResourceRel="client",path="client")
public interface ClientRepository extends PagingAndSortingRepository<Client, Long> {

    Optional<Client> findByNclient(Long id);
}
