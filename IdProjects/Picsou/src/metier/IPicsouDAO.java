package metier;

import dto.CompteDTO;
import exception.ExceptionMetier;
import java.util.*;

public interface IPicsouDAO {
    //gestion des comptes

    public List<CompteDTO> findCompteByClient(int idClient);
    public CompteDTO findCompteById(int idCompte) throws ExceptionMetier;

    //exception du pauvre :renvoyer un status

    void credit(int idCompte, double montant)throws ExceptionMetier;
    void debit(int idCompte, double montant)throws ExceptionMetier;
    void transfert(int idCompteOrigine, int idCompteDestination, double montant)throws ExceptionMetier;

    void retrait (int idCarte,String code,double montant,String adr) throws ExceptionMetier;
}
