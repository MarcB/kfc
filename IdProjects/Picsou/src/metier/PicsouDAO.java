package metier;

import dto.CompteDTO;
import exception.ExceptionMetier;
import service.ConnectPicsou;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PicsouDAO implements IPicsouDAO {
    //Singleton
    private static IPicsouDAO dao = new PicsouDAO();

    private PicsouDAO() {
    }

    public static IPicsouDAO getDAO() {
        return dao;
    }


    // SQL

    private final static String SQLfindAllComptesbyClient =
            " select c.id as id,c.numero,c.titulaire,cl.nom,cl.prenom,c.date_ouverture,c.plafond,c.bloque,c.solde" +
                    " from COMPTE c" +
                    " inner join CLIENT cl" +
                    " on cl.id = titulaire " +
                    " where cl.id= ? ";

    private final static String SQLfindAllComptesbyId =
            " select c.id as id,numero,titulaire,date_ouverture,plafond,bloque,solde" +
                    " from COMPTE c" +
                    " inner join CLIENT cl" +
                    " on cl.id = titulaire " +
                    " where cl.id= ? ";

    private final String SQLupdateCrediterCompte =
            " update compte " +
                    " set solde = solde + ? " +
                    " where id = ? ";

    private final String SQLinsertMvt =
            " insert into mouvement (idCompte,date_operation,montant,nature)   " +
                    "  values ( ? , current_timestamp,?,? ) ";


    private final String SQLupdateDebiterCompte =
            " update compte " +
                    " set solde = solde - ? " +
                    " where id = ? ";

    private final String SQLfindCarteById =
            " select id,idCompte,bloque,ndEssai,codeSecret,date_expiration " +
                    " from carte " +
                    " where id = ?  ";
    private final String SQLinsertRetrait =
            " insert into retrait(idcarte, date_retrait, " +
                    " adresseGAB,montant,etat,idMouvement) " +
                    " values( ?,current_timestamp,?,?,'succes',?)   ";
    private final String SQLinsertRetraitAnomalie =
            " insert into retrait(idcarte,carte_retrait, " +
                    " adresseGAB,montant,etat,idMovement) " +
                    " values ( ?, current_timestamp ,?,?,?,null )";
    private final String SQLSommeJourByCarte =
            " select count(*) as cpt,sum(montan) as somme " +
                    "   FROM retrait " +
                    "WHERE current_date - 10 <=date_retrait " +
                    " AND idcarte =? " +
                    " AND etat = 'succes' ";

    //FIN SQL

    @Override
    public List<CompteDTO> findCompteByClient(int idClient) {
        List<CompteDTO> liste = new ArrayList<>();
        Connection conn = service.ConnectPicsou.getInstance();
        try {
            PreparedStatement pstmt = conn.prepareStatement(SQLfindAllComptesbyClient);
            pstmt.setInt(1, idClient);

            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                CompteDTO cpte = new CompteDTO(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getInt(3),
                        rs.getString(4) + " " + rs.getString(5),
                        rs.getDate(6),
                        rs.getDouble(7),
                        rs.getString(8),
                        rs.getDouble(9));
                liste.add(cpte);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return liste;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public CompteDTO findCompteById(int idCompte) throws ExceptionMetier {
        CompteDTO cpte = null;

        Connection conn = service.ConnectPicsou.getInstance();

        try {
            PreparedStatement pstmt = conn.prepareStatement(SQLfindAllComptesbyId);
            pstmt.setInt(1, idCompte);

            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                return new CompteDTO(
                        rs.getInt("id"),
                        rs.getString("numero"),
                        rs.getInt("titulaire"),
                        rs.getString(4) + " " + rs.getString(5),
                        rs.getDate("date_ouverture"),
                        rs.getDouble("plafond"),
                        rs.getString("plafond"),
                        rs.getDouble("solde"));
            } else {
                throw new ExceptionMetier("findCompteById  ; idCompte inconnu");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public void credit(int idCompte, double montant) throws ExceptionMetier {
        if (montant <= 0)
            throw new ExceptionMetier("Credit : montant negatif ");


        Connection conn = service.ConnectPicsou.getInstance();

        try {

            conn.setAutoCommit(false);

            PreparedStatement psUpdate = conn.prepareStatement(SQLupdateCrediterCompte);

            psUpdate.setInt(2, idCompte);
            psUpdate.setDouble(1, montant);

            int nb = psUpdate.executeUpdate();

            if (nb == 0) {
                throw new ExceptionMetier("Credit : id compte inconnu");
            }
            PreparedStatement psInsert = conn.prepareStatement(SQLinsertMvt);

            psInsert.setInt(1, idCompte);
            psInsert.setDouble(2, montant);
            psInsert.setString(3, "CR");

            nb = psInsert.executeUpdate();

            conn.commit();

        } catch (SQLException e) {
            try {
                System.err.println(e.getMessage());
                conn.rollback();
            } catch (SQLException e1) {
                throw new ExceptionMetier("bizarre");
            }
            e.printStackTrace();
        }

    }

    @Override
    public void debit(int idCompte, double montant) throws ExceptionMetier {
        if (montant <= 0)
            throw new ExceptionMetier("Credit : montant negatif ");


        Connection conn = service.ConnectPicsou.getInstance();

        try {

            conn.setAutoCommit(false);

            PreparedStatement psSelectCompte = conn.prepareStatement(SQLfindAllComptesbyId);
            PreparedStatement psUpdateCompte = conn.prepareStatement(SQLupdateDebiterCompte);
            PreparedStatement psDebiteCompte = conn.prepareStatement(SQLinsertMvt);

            psSelectCompte.setInt(1, idCompte);
            ResultSet rs = psSelectCompte.executeQuery();

            if (!rs.next()) {
                throw new ExceptionMetier("DEbit : id compte inconnu");
            }

            double solde = rs.getDouble("solde");
            double plafond = rs.getDouble("plafond");

            if (solde + plafond < montant) {
                throw new ExceptionMetier("Debit : solde insuffisant ");
            }

            psUpdateCompte.setInt(2, idCompte);
            psUpdateCompte.setDouble(1, montant);

            int nb = psUpdateCompte.executeUpdate();
            psDebiteCompte.setInt(1, idCompte);
            psDebiteCompte.setDouble(2, montant);
            psDebiteCompte.setString(3, "DB");

            nb = psDebiteCompte.executeUpdate();


            conn.commit();

        } catch (SQLException e) {
            try {
                System.err.println(e.getMessage());
                conn.rollback();
            } catch (SQLException e1) {
                throw new ExceptionMetier("bizarre");
            }
            e.printStackTrace();
        }

    }

    @Override
    public void transfert(int idCompteOrigine, int idCompteDestination, double montant) throws ExceptionMetier {
        if (montant <= 0)
            throw new ExceptionMetier("Credit : montant negatif ");

        if (idCompteOrigine == idCompteDestination) return;
        Connection conn = service.ConnectPicsou.getInstance();

        try {
            conn.setAutoCommit(false);

            PreparedStatement psSelectCompte = conn.prepareStatement(SQLfindAllComptesbyId);
            PreparedStatement psUpdateCompte = conn.prepareStatement(SQLupdateDebiterCompte);
            PreparedStatement psDebiteCompte = conn.prepareStatement(SQLinsertMvt);

            psSelectCompte.setInt(1, idCompte);
            ResultSet rs = psSelectCompte.executeQuery();

            if (!rs.next()) {
                throw new ExceptionMetier("DEbit : id compte inconnu");
            }

            double solde = rs.getDouble("solde");
            double plafond = rs.getDouble("plafond");

            if (solde + plafond < montant) {
                throw new ExceptionMetier("Debit : solde insuffisant ");
            }

            psUpdateCompte.setInt(2, idCompte);
            psUpdateCompte.setDouble(1, montant);

            int nb = psUpdateCompte.executeUpdate();
            psDebiteCompte.setInt(1, idCompte);
            psDebiteCompte.setDouble(2, montant);
            psDebiteCompte.setString(3, "DB");

            nb = psDebiteCompte.executeUpdate();


            conn.commit();

        } catch (SQLException e) {
            try {
                System.err.println(e.getMessage());
                conn.rollback();
            } catch (SQLException e1) {
                throw new ExceptionMetier("bizarre");
            }
            e.printStackTrace();
        }
    }

    @Override
    public void retrait(int idCarte, String code, double montant, String adr) throws ExceptionMetier {
        if (montant <= 0)
            throw new ExceptionMetier(("Retrait : montant negatif "));

        Connection conn = ConnectPicsou.getInstance();

        try {
            conn.setAutoCommit(false);
            //enregistrement des preparedstatement

            PreparedStatement psFindCarteById = conn.prepareStatement(SQLfindCarteById);
            PreparedStatement psInsertRetraitAnomalie = conn.prepareStatement(SQLinsertRetraitAnomalie);

            psFindCarteById.setInt(1,idCarte);

            ResultSet rs = psFindCarteById.executeQuery();

            if(! rs.next()){
                psInsertRetraitAnomalie.setInt(1,idCarte);
                psInsertRetraitAnomalie.setDouble(3,montant);
                psInsertRetraitAnomalie.setString(2,adr);
                psInsertRetraitAnomalie.setString(4,"CB inconnue");

                psInsertRetraitAnomalie.executeUpdate();

                throw  new ExceptionMetier("Retrait : carte inconnue");
            }





            //control de la cb (inconnu?,bloquee?,.....)




            //controle code

            conn.commit();
        } catch (SQLException esql) {
            esql.printStackTrace();
            try {
                conn.rollback();
            } catch (SQLException e1) {
                throw new ExceptionMetier("bizarre");
            }

        }
    }

        public static void main (String[]args){
            IPicsouDAO dao = PicsouDAO.getDAO();

            for (CompteDTO c : dao.findCompteByClient(1)) {
                System.out.println(c);
            }

            try {
                System.out.println("_________________");
                System.out.println(dao.findCompteById(3));
                dao.credit(3, 1500);
            } catch (ExceptionMetier exceptionMetier) {
                System.err.println();
            }

        }
    }
