package modele;
import java.util.Objects;
import java.util.*;

public class Patronyme implements Comparable<Patronyme>{

    private static String nom;
    private static String prenom;

    public static String getNom() {
        return nom;
    }

    public  void setNom(String nom) {
        this.nom = (nom==null?"DUPOND":nom.toUpperCase());
    }

    public static String getPrenom() {
        return prenom;
    }

    public  void setPrenom(String prenom) {
        this.prenom =(prenom==null?"JEAN":prenom.toUpperCase());
    }



    @Override
    public String toString() {
        return "Patronyme{" +
                "nom=" + nom +
                ", prenom=" + prenom +

                '}';
    }

    @Override
    public boolean equals(Object o) {
        if(this==o)return true;
        if(!(o instanceof  Patronyme))return false;
        Patronyme patronyme=(Patronyme)o;
        return nom.equals(patronyme.nom)&&prenom.equals(patronyme.prenom);
    }

    //constructeur
    public Patronyme(String nom, String prenom) {
        this.setNom(nom);
        this.setPrenom(prenom);
    }
    public Patronyme(){
        this.setNom(null);
        this.setPrenom(null);
    }
    public int hashCode(){
        return Objects.hash(nom,prenom);
    }

    @Override
    public int compareTo(Patronyme autre) {
        int aux=this.nom.compareTo((autre.nom));
        if(aux!=0)
            return aux;
        return this.prenom.compareTo((autre.prenom));
    }

    public static void main(String[] args) {
        Patronyme p1 = new Patronyme("afreux", "pierre");
        Patronyme p2 = new Patronyme("xabiste", "jean");
        Patronyme p3 = new Patronyme("gator", "nathalie");
        Patronyme p4 = new Patronyme("gator", "magalie");
        Set<Patronyme> ens = new TreeSet<>();
        ens.add(p1);
        ens.add(p2);
        ens.add(p3);
        ens.add(p4);
        System.out.println(ens);
        Map<Patronyme, Integer> ensdico = new TreeMap<>();
        ensdico.put(p1, 15);
        ensdico.put(p2, 18);
        ensdico.put(p3, 7);
        ensdico.put(p4, 56);
        System.out.println(ensdico.keySet());
        List<Integer> liste = new LinkedList<>(ensdico.values());
        Collections.sort(liste);
        System.out.println(liste);

/*
        1. List<Album> listeAlbumParTitre()
        2. List<Album> listeAlbumParTitreDecroissant()
        3. List<Album> listeAlbumParAnnee()
                - tri croissant par année et titre (utilisation d’une classe locale interne)
        4. List<Album> listeAlbumParAnneeDecroissant()
                - tri décroissant par année et titre (utilisation d’une classe anonyme)


        */
    }
}
