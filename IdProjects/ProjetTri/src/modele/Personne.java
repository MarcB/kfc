package modele;

import java.util.Objects;
import java.util.*;


public class Personne implements Comparable<Personne>{
    private  static int denierMatricule=0;

    private int matricule;
    private Patronyme patronyme;
    private Double salaire;

    public int getMatricule() {
        return matricule;
    }

    private void setMatricule() {
        Personne.denierMatricule++;
        this.matricule = Personne.denierMatricule;
    }

    public Patronyme getPatronyme() {
        return patronyme;
    }

    public Double getSalaire() {
        return salaire;
    }

    public void setSalaire(Double salaire) {
        this.salaire = (salaire==null?1000.0:(salaire<1000.0?1000:salaire));
    }

    @Override
    public int compareTo(Personne autre){
        return patronyme.compareTo(autre.patronyme);
    }

    public Personne(String nom,String prenom,Double salaire){
        this.setMatricule();
        this.patronyme=new Patronyme(nom,prenom);
        this.setSalaire(salaire);
    }

    @Override
    public String toString() {
        return "Personne{" +
                "matricule=" + matricule +
                ", patronyme=" + patronyme +
                ", salaire=" + salaire +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(matricule, patronyme, salaire);
    }

    public Personne(){
        this(null,null,null);
    }


    public static void main(String[] args) {
        Personne p1=new Personne();
        Personne p2=new Personne("oj","jji",0.0);
        Set<Personne>ens=new TreeSet<>();
        ens.add(p1);
        ens.add(p2);
        System.out.println(ens);
    }
}
