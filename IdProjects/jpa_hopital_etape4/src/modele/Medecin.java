package modele;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
@DiscriminatorValue(value="Med")

public class Medecin extends Personne implements Serializable {
    private static final long serialVersion = 1L;

    private float salaire;

    @ManyToOne
    @JoinColumn(name="service_id")
    private Service service;

    @ManyToOne
    @JoinColumn(name = "manager_id")
    private Medecin mgr;

    @OneToMany(mappedBy = "mgr")
    private Set<Medecin> subs = new HashSet<>();





    public float getSalaire() {
        return salaire;
    }

    public void setSalaire(float salaire) {
        this.salaire = salaire;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    protected Medecin(){
        super("dupond", "jean");
    }

    public Medecin(String nom, String prenom, float salaire){
        super(nom, prenom);
        this.salaire=salaire;
    }

    public Medecin getMgr() {
        return mgr;
    }

    public Set<Medecin> getSubs() {
        return subs;
    }

    public void setSubs(Set<Medecin> subs) {
        this.subs = subs;
    }

    public void setMgr(Medecin mgr){
        if (this.getMgr() !=null ){
            if (this.getMgr().equals(mgr))
                return;
            else
                this.getMgr().getSubs().remove(this);
        }
        //gestion des deux extremites
        if (mgr!=null) {
            mgr.subs.add(this);
        }
        this.mgr = mgr;
    }


    @Override
    public String toString() {
        return super.toString() + '\'' +
                ", salaire=" + salaire +
                ", service=" + (service==null?", sans service" : service.getNom()) +
                ", mgr=" + (mgr==null?"sans manager":mgr.getNom()) +
                '}';
    }
}
