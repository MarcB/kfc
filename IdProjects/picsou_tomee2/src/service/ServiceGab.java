package service;

import modele.ClientEntity;
//import util.Achat;
//import util.CardExpiredException;
//import util.InfoCB;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ServiceGab {
    // Gestion d'un Singleton
    private static ServiceGab service = new ServiceGab();
    public static ServiceGab getSingleton() {
        return service;
    }

    private EntityManagerFactory emf = JPAUtil.getEmf();

    //----------------------------------------- creation ou localisation du client
    public ClientEntity findOrCreate(String nom, String prenom){
        ClientEntity client = null;
        EntityManager em = emf.createEntityManager();

            try {
                client = em.createNamedQuery("Client.findByNomAndPrenom", ClientEntity.class)
                           .setParameter("nom",nom)
                            .setParameter("prenom",prenom)
                           .getSingleResult();
            }catch(Exception ex){

                client = new ClientEntity();
                em.getTransaction().begin();
                em.persist(client);
                em.getTransaction().commit();
            }

        em.close();
        return client;
    }

    public List<ClientEntity> findAllClient(){
        EntityManager em = emf.createEntityManager();
        return em.createNamedQuery("Client.findAll", ClientEntity.class).getResultList();
    }



/*
    public Map<Item, Integer> contenuDuPanierValue(List<Achat> panier){
        Map<Item, Integer> contenu = new HashMap<>();

        EntityManager em = emf.createEntityManager();
        for(Achat achat : panier){
            Item item = em.find(Item.class, achat.getCode());
            contenu.put(item, achat.getQuantite());
        }
        em.close();
        return contenu;
    }

    public Double valeurDuPanier(List<Achat> panier){
        Double total = 0.0d;

        EntityManager em = emf.createEntityManager();
        for(Achat achat : panier){
            Item item = em.find(Item.class, achat.getCode());
            total = total+ achat.getQuantite()*item.getPrix();
        }
        em.close();
        return total;
    }

    public void validerPanier(Client client, List<Achat> panier, InfoCB infoCB)
        throws CardExpiredException
    {
        if(infoCB.getDateExpirationCarte().before( new Date()))
            throw new CardExpiredException("date de validité dépassée");

        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
           client = em.merge(client);

           java.sql.Date dateDuJour = new java.sql.Date(System.currentTimeMillis());

           Commande commande = new Commande(dateDuJour);
           em.persist(commande);
           commande.setClient(client);
           client.getCommandes().add(commande);

           Double total = 0.0d;
           for(Achat achat : panier){
            Item item = em.find(Item.class, achat.getCode());
            total = total+ achat.getQuantite()*item.getPrix();
           }

           Payment payment = new Payment(total,dateDuJour,"CB",infoCB.getNumeroCarte(),  new java.sql.Date(infoCB.getDateExpirationCarte().getTime()));
           payment.setClient(client);
           payment.setCommande(commande);
           em.persist(payment);

           client.getPayments().add(payment);
           commande.setPayment(payment);

           for(Achat achat : panier){
               Item item = em.find(Item.class, achat.getCode());

               DetailCde detail = new DetailCde();
               detail.setItem(item);
               detail.setCommande(commande);
               detail.setQte(achat.getQuantite());

               em.persist(detail);

               commande.getDetails().add(detail);
           }
         em.getTransaction().commit();
           em.close();

    }*/
}
