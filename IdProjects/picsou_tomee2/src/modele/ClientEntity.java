package modele;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;

@Entity
@NamedQueries({
        @NamedQuery( name="Client.findAll",
                query="SELECT cl FROM ClientEntity cl ORDER BY cl.nom, cl.prenom"
        ),
        @NamedQuery( name="Client.findByNomAndPrenom",
                query="SELECT cl FROM ClientEntity cl WHERE UPPER(cl.nom)    = UPPER(:nom) and " +
                        "    UPPER(cl.prenom) = UPPER(:prenom)"
        )
})


@Table(name = "CLIENT", schema = "Picsou")
public class ClientEntity implements Serializable {
    private int id;
    private String nom;
    private String prenom;
    private String adresse;
    private Collection<CompteEntity> comptesById;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nom")
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Basic
    @Column(name = "prenom")
    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @Basic
    @Column(name = "adresse")
    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClientEntity that = (ClientEntity) o;
        return id == that.id &&
                Objects.equals(nom, that.nom) &&
                Objects.equals(prenom, that.prenom) &&
                Objects.equals(adresse, that.adresse);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nom, prenom, adresse);
    }

    @OneToMany(mappedBy = "clientByTitulaire")
    public Collection<CompteEntity> getComptesById() {
        return comptesById;
    }

    public void setComptesById(Collection<CompteEntity> comptesById) {
        this.comptesById = comptesById;
    }

    @Override
    public String toString() {
        return "ClientEntity{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", adresse='" + adresse + '\'' +
                ", comptesById=" + comptesById +
                '}';
    }
}
