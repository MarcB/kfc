package com.pop.jsontintin.controller;

import com.pop.jsontintin.dao.AlbumDAO;
import com.pop.jsontintin.dao.PersonnageDAO;
import com.pop.jsontintin.model.AlbumsEntity;
import com.pop.jsontintin.model.PersonnagesEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class MainController {
    @Autowired
    private AlbumDAO albumDAO;

    @Autowired
    private PersonnageDAO personnageDAO;

    //************************************
    //              Album
    //************************************

    //list des albums
    @GetMapping("/albums")
    List<AlbumsEntity> allAlbum(){
        return albumDAO.findAll();
    }

    //par id
    @GetMapping("/albums/{id}")
    public Optional<AlbumsEntity> getAlbum(@PathVariable("id") long id){
        Optional<AlbumsEntity> album= albumDAO.findById(id);
        return album;
    }

    //entre 2 années
    @GetMapping("/albums/{annee1}/{annee2}")
    public List<AlbumsEntity> getAnneeAlbum(@PathVariable("annee1") int annee1,@PathVariable("annee2") int annee2){
        List<AlbumsEntity>list=albumDAO.findByAnneeBetween(annee1,annee2);
        return list;
    }

    //************************************
    //             Personnage
    //************************************

    //list des personnages
    @GetMapping("/personnages")
    List<PersonnagesEntity> allPerso(){
        return personnageDAO.findAll();
    }

    //par id
    @GetMapping("/personnages/{id}")
    public Optional<PersonnagesEntity> getPerso(@PathVariable("id") long id){
        return personnageDAO.findById(id);
    }


    //personnage creation ==========================================================


//    //ajout de client
//    @PostMapping("/personnages/add")
//    public String addClient(@Valid PersonnagesEntity personnagesEntity, BindingResult result, Model model) {
//        if (result.hasErrors()) {
//            return "add-personnage";
//        }
//        personnageDAO.save(personnagesEntity);
//        model.addAttribute("Personnage", personnageDAO.findAll());
//        return "persoCrea";
//    }

    // creation d'un personnage
    @PostMapping(value = "/personnages/create")
    public void addPerso(@RequestBody PersonnagesEntity perso) {
        personnageDAO.save(perso);
    }

    // update personnage
    @PutMapping(value = "/personnages/update/{persoId}")
    public PersonnagesEntity updatePerso(
            @RequestBody PersonnagesEntity perso,
            @PathVariable("persoId") String persoId)
    {
        return personnageDAO.save(perso);
    }

    //delete personnage
    @DeleteMapping(value = "/personnages/delete/{persoId}")
    public void deletePerso(
            @PathVariable("persoId") long persoId)
    {
        personnageDAO.deleteById(persoId);
    }


}
