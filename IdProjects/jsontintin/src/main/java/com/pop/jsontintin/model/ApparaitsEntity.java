package com.pop.jsontintin.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "apparaits", schema = "myTintin", catalog = "")
public class ApparaitsEntity {
    private int id;
    private AlbumsEntity albumsByAlbumId;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ApparaitsEntity that = (ApparaitsEntity) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @ManyToOne
    @JoinColumn(name = "album_id", referencedColumnName = "id", nullable = false)
    public AlbumsEntity getAlbumsByAlbumId() {
        return albumsByAlbumId;
    }

    public void setAlbumsByAlbumId(AlbumsEntity albumsByAlbumId) {
        this.albumsByAlbumId = albumsByAlbumId;
    }
}
