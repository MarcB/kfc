package com.pop.jsontintin.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Objects;

@Data

@Entity

@Table(name = "albums")
public class AlbumsEntity {
    private long id;
    private String titre;
    private int annee;
    private AlbumsEntity albumsBySuivant;

    @Id
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "titre", nullable = false, length = 30)
    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    @Basic
    @Column(name = "annee", nullable = false)
    public int getAnnee() {
        return annee;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AlbumsEntity that = (AlbumsEntity) o;
        return id == that.id &&
                annee == that.annee &&
                Objects.equals(titre, that.titre);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, titre, annee);
    }

    @ManyToOne
    @JoinColumn(name = "suivant", referencedColumnName = "id")
    public AlbumsEntity getAlbumsBySuivant() {
        return albumsBySuivant;
    }

    public void setAlbumsBySuivant(AlbumsEntity albumsBySuivant) {
        this.albumsBySuivant = albumsBySuivant;
    }

    public AlbumsEntity() {
    }

    public AlbumsEntity(    long id) {this.id=id;
    }

    public AlbumsEntity(String titre, int annee, AlbumsEntity albumsBySuivant) {
        this.titre = titre;
        this.annee = annee;
        this.albumsBySuivant = albumsBySuivant;
    }

    @Override
    public String toString() {
        return "AlbumsEntity{" +
                "id=" + id +
                ", titre='" + titre + '\'' +
                ", annee=" + annee +
                ", albumsBySuivant=" + albumsBySuivant +
                '}';
    }
}
