package com.pop.jsontintin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JsontintinApplication {

    public static void main(String[] args) {
        SpringApplication.run(JsontintinApplication.class, args);
    }

}
