package main;

import inter.EntityB;

import java.awt.*;
import java.util.Random;


public class Enemy extends GameObject implements EntityB {

    Random r=new Random();

    private Textures tex ;
    //vitesse variable
    private int speed=r.nextInt(10)+1;

    public Enemy(double x,double y ,Textures tex){
       super(x,y);
        this.tex=tex;

    }

    public void tick(){
        y+=speed;
        if(y>(Game.HEIGHT*Game.SCALE)){
            y=-10;
            x=r.nextInt(Game.WIDTH*Game.SCALE);
        }
    }
    public void render(Graphics g){
        g.drawImage(tex.enemy,(int)x,(int)y,null);
    }

    public Rectangle getBounds(){return new Rectangle((int)x,(int)y,32,32);    }

    @Override
    public double getX() {        return x;    }
    public  double getY(){        return y;    }

    public void setY(double y){this.y=y;}
}
