package main;

import javax.imageio.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class BufferedImageLoader {

    public BufferedImage loadImage(String path) throws IOException {
        return ImageIO.read(getClass().getResource(path));
    }
}
