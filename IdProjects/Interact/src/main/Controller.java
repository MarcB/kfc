package main;

import inter.EntityA;
import inter.EntityB;

import java.awt.*;
import java.util.LinkedList;
import java.util.Random;

public class Controller {
    private LinkedList<EntityB> eb = new LinkedList<EntityB>();
    private LinkedList<EntityA> ea = new LinkedList<EntityA>();


    EntityB entb;
    EntityA enta;
    Bullet TempBullet;
    Enemy TempEnemy;
    Game game;
    Textures tex;
    Random r = new Random();

    public Controller(Textures tex) {

        this.tex = tex;
        /* addBullet(new Bullet(100,100,game));  */
        /*for(int x=0;x<(Game.WIDTH*Game.SCALE);x+=64){
            addEnemy(new Enemy(x,20,tex));
        }*/
     /*   for(int i=0;i<20;i++) {
            addEntity(new Enemy(r.nextInt(Game.WIDTH), 10, tex));
        }*/
    }

    //generateur d enemy
    public void create_enemy(int enemy_count) {
        for (int i = 0; i < enemy_count; i++) {
            addEntity(new Enemy(r.nextInt(640), -10, tex));
        }
    }

    public void tick() {
        for (int i = 0; i < ea.size(); i++) {
            enta = ea.get(i);
            enta.tick();
        }
        for (int i = 0; i < eb.size(); i++) {
            entb = eb.get(i);
            entb.tick();
        }
    }

    public void render(Graphics g) {
        for (int i = 0; i < ea.size(); i++) {
            enta = ea.get(i);
            enta.render(g);
        }
        for (int i = 0; i < eb.size(); i++) {
            entb = eb.get(i);
            entb.render(g);
        }
    }

    public void addEntity(EntityB block) {
        eb.add(block);
    }

    public void removeEntity(EntityB block) {
        eb.remove(block);
    }

    public void addEntity(EntityA block) {
        ea.add(block);
    }

    public void removeEntity(EntityA block) {
        ea.remove(block);
    }

    public LinkedList<EntityA> getEntityA(){        return ea;    }
    public LinkedList<EntityB> getEntityB(){        return eb;    }
}

