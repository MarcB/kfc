package inter;

import java.awt.*;

//class generique
public interface EntityB {
    public void tick();
    public void render(Graphics g);
    public Rectangle getBounds();
    public double getX();
    public double getY();


}
