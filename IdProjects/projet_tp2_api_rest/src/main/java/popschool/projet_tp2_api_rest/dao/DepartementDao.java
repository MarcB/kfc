package popschool.projet_tp2_api_rest.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import popschool.projet_tp2_api_rest.model.Dept;


public interface DepartementDao extends JpaRepository<Dept, String> {

}
