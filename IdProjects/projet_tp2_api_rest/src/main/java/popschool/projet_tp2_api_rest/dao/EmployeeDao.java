package popschool.projet_tp2_api_rest.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import popschool.projet_tp2_api_rest.model.Dept;
import popschool.projet_tp2_api_rest.model.Employee;

import java.util.*;

public interface EmployeeDao extends JpaRepository<Employee, String> {
    List<Employee> findByDepartement(Dept dept);

    @Query(value = "SELECT sum(e.salary) from Employee e where e.departement.deptNo=?1")
    Double findSumSalaire(String deptno);
}
