package popschool.projet_tp2_api_rest.controler;

import ch.qos.logback.core.boolex.EvaluationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import popschool.projet_tp2_api_rest.dao.DepartementDao;
import popschool.projet_tp2_api_rest.dao.EmployeeDao;
import popschool.projet_tp2_api_rest.model.Dept;
import popschool.projet_tp2_api_rest.model.Employee;


import java.util.List;
import java.util.Optional;

@RestController
public class EmployeeControler {

    @Autowired
    private EmployeeDao employeeDao;

    @Autowired
    private DepartementDao departementDao;


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                  METHODE POST
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //pour recevoir le code 201
    //URI :
    //methode créer un employee et avec le responseEntity on puet changer l'erreur pour avoir l'eereur 201
    @PostMapping(value = "/employees")
    public ResponseEntity<Employee> addEmployee(@RequestBody Employee emp){
        Employee newEmployee = employeeDao.save(emp);
        return ResponseEntity.status(HttpStatus.CREATED).body(newEmployee);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                  METHODE GET
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //URI :
    //contextPath.servletPath/employees
    @GetMapping(value = "/employees")
    public List<Employee> getEmployees(){
        List<Employee> list = employeeDao.findAll();
        return list;
    }

    @GetMapping(value = "/employees/{empNo}")
    public Employee getEmployee(@PathVariable("empNo") String empNo){
        return employeeDao.findById(empNo).get();
    }

    @GetMapping(value = "employees/dept/{deptNo}")
    public List<Employee> getEmployeeByDept(@PathVariable("deptNo") String deptNo){
        Dept dept = departementDao.findById(deptNo).get();
        return employeeDao.findByDepartement(dept);
    }

    @GetMapping(value = "employees/somme/salaire/dept/{deptNo}" )
    public Double getSommeSalaire(@PathVariable("deptNo") String deptno){
       return employeeDao.findSumSalaire(deptno);

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                  METHODE PUT
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @PutMapping(value = "/employees/{empNo}")
    public Employee updateEmployee(@RequestBody Employee emp,
                                @PathVariable("empNo") String empNo){
        return employeeDao.save(emp);
    }

    @PutMapping(value = "/employees/{empNo}/affecte/{deptNo}")
    public Employee affectDeptNo(@RequestBody Employee emp,
                                   @PathVariable("empNo") String empNo,
                                 @PathVariable("deptNo") String deptNo){
        Dept dept = departementDao.findById(deptNo).get();
        emp.setDepartement(dept);
        return employeeDao.save(emp);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                  METHODE DELETE
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @DeleteMapping(value = "/employees/{empNo}")
    public void deleteEmployee(@PathVariable("empNo") String empNo){
        employeeDao.deleteById(empNo);
    }
}
