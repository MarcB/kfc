package popschool.projet_tp2_api_rest.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.*;

@Data
@Entity
public class Dept {

    @Id
    @Column(name = "deptno")
    private String deptNo;

    @Column(name = "deptname")
    private String deptName;

    private String localisation;

    @OneToMany(mappedBy = "departement")
    @JsonIgnore
    private List<Employee> employees = new ArrayList<>();

    public Dept() {
    }

    public Dept(String deptNo, String deptName, String localisation) {
        this.deptNo=deptNo;
        this.deptName=deptName;
        this.localisation=localisation;
    }

}
