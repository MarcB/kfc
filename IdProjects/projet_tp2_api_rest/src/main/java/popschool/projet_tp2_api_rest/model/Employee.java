package popschool.projet_tp2_api_rest.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Employee {
    @Id
    @Column(name = "empno")
    private String empNo;

    @Column(name = "empname")
    private String empName;

    private String position;

    private Double salary;

    @ManyToOne
    @JoinColumn(name = "deptno")
    private Dept departement;

    public String getEmpNo() {
        return empNo;
    }

    public void setEmpNo(String empNo) {
        this.empNo = empNo;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public Dept getDepartement() {
        return departement;
    }

    public void setDepartement(Dept departement) {
        this.departement = departement;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                            CONSTRUCTEUR
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public Employee() {
    }

    public Employee(String empNo, String empName, String position, Double salary, Dept departement) {
        this.empNo = empNo;
        this.empName = empName;
        this.position = position;
        this.salary = salary;
        this.departement = departement;
    }
}
