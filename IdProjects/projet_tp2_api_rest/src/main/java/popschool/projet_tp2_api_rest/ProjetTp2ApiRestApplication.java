package popschool.projet_tp2_api_rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetTp2ApiRestApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjetTp2ApiRestApplication.class, args);
    }

}
