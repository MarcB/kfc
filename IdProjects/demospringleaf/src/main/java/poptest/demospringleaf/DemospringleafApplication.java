package poptest.demospringleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemospringleafApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemospringleafApplication.class, args);
    }

}
