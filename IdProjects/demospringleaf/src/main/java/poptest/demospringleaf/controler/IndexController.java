package poptest.demospringleaf.controler;


import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
    @RequestMapping("/home")
    public class IndexController {

    @RequestMapping(method = RequestMethod.GET)
    String get() {
        return "Hello from get";
    }

    @RequestMapping(method = RequestMethod.DELETE)
    String delete() {
        return "Hello from delete";
    }

//    @RequestMapping(method = RequestMethod.POST)
//    String post() {
//        return "Hello from post";
//    }

    @RequestMapping(method = RequestMethod.PUT)
    String put() {
        return "Hello from put";
    }

    @RequestMapping(method = RequestMethod.PATCH)
    String patch() {
        return "Hello from patch";
    }

    @RequestMapping(value = "/head", headers = {
            "content-type=text/plain",
            "content-type=text/html"
    }) String post() {
        return "Mapping applied along with headers";
    }
    //Using @RequestMapping With Dynamic URIs
    @RequestMapping(value = "/fetch/{id}", method = RequestMethod.GET)
    String getDynamicUriValue(@PathVariable String id) {
        System.out.println("ID is " + id);
        return "Dynamic URI parameter fetched";
    }
    @RequestMapping(value = "/fetch/{id:[a-z]+}/{name}", method = RequestMethod.GET)
    String getDynamicUriValueRegex(@PathVariable("name") String name) {
        System.out.println("Name is " + name);
        return "Dynamic URI parameter fetched using regex";
    }


}
