package popschool.ecommercethymeleaf.dao;

import org.springframework.data.repository.CrudRepository;
import popschool.ecommercethymeleaf.model.Produit;

import java.util.List;

public interface ProduitRepository extends CrudRepository<Produit,Integer> {
    List<Produit>findProduitByDisponible(String diponible);

}
