package popschool.ecommercethymeleaf.dao;

import org.springframework.data.repository.CrudRepository;
import popschool.ecommercethymeleaf.model.Client;

import java.util.Optional;

public interface ClientRepository extends CrudRepository<Client,String> {

    Optional<Client> findByNom(String s);
}
