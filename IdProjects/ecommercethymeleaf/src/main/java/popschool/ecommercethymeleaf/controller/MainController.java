package popschool.ecommercethymeleaf.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import popschool.ecommercethymeleaf.dao.ClientRepository;
import popschool.ecommercethymeleaf.dao.ProduitRepository;
import popschool.ecommercethymeleaf.model.Client;

import javax.validation.Valid;


//@Controller
//@SessionAttributes(value="commandeSession", types = {Commande.class})
//public class MainController {

@Controller
public class MainController {

//*******************************************************************************************
//Spring Bean Scopes  |
//--------------------
//There are five types of spring bean scopes:
//
//    singleton
//    -----------only one instance of the spring bean will be created for the spring container.
//              This is the default spring bean scope. While using this scope, make sure bean
//              doesn’t have shared instance variables otherwise it might lead to data inconsistency issues.
//
//    prototype
//    -----------A new instance will be created every time the bean is requested from the spring container.
//
//    request
//    -----------This is same as prototype scope, however it’s meant to be used for web applications.
//               A new instance of the bean will be created for each HTTP request.
//
//    session
//    -----------A new bean will be created for each HTTP session by the container.
//
//    global-session
//    -----------This is used to create global session beans for Portlet applications.
//
// *******************************************************************************************



    @Autowired
    private ProduitRepository produitRepository;
    @Autowired
    private ClientRepository clientRepository;

    @Value("${error.message}")
    private String errorMessage;

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                    CREATION DE LA COMMANDE EN SESSION
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //@ModelAttribute("commandeSession")
    //public Commande addMyBean1ToSessionScope() {
    //    return new Commande();
    //}

    //@PostMapping(value = "/Authentificate")
    //public String authentification(@ModelAttribute("client") Client client, @ModelAttribute("commandeSession") Commande commandeSession, Model model){
    //    Client verifClient = serviceEcommerce.authentification(client.getNom(), client.getMotdepasse());
    //    if (verifClient!=null){
    //        commandeSession.setClient(verifClient);
    //        verifClient.getCommandes().add(commandeSession);
    //        List<Produit> catalog = serviceEcommerce.trouverTous();
    //        model.addAttribute("catalog" , catalog);
    //        System.out.println("------------------session commande a l'authentification--------------------------");
    //        System.out.println(commandeSession);
    //        return "catalog";
    //    }else {
    //        model.addAttribute("client", client);
    //        return "index";
    //    }
    //}



    //index >>  page home =====================================================
    @RequestMapping(value = {  "/","/index" }, method = RequestMethod.GET)
    public String afficheIndex(Model model) {
        String erreur="Remplissez les champs vides";
        Client client=new Client();
        model.addAttribute("erreur",erreur);
        model.addAttribute("testClient",client);
        return "index";
        //    @RequestMapping(value = { "" , "/page" ," page*" , "view/*,**/msg" })
        //        localhost:8080/home
        //        localhost:8080/home/
        //        localhost:8080/home/page
        //        localhost:8080/home/pageabc
        //        localhost:8080/home/view/
        //        localhost:8080/home/view/view
    }

    //test liste produits =====================================================
    @RequestMapping(value = {"/listProduit"}, method = RequestMethod.GET)
    public String listingProduit(Model model) {
        model.addAttribute("products",produitRepository.findAll());
        return "listProduit";
    }

    @RequestMapping(value = {"/login"}, method = RequestMethod.POST)
    public String verifclient(
                              @Valid Client testClient,String erreur, Model model ){
        erreur="";
        // validation du login+pass
        if(testClient.getNom().equals("didier") && testClient.getMotdepasse().equals("tigger")) {
            model.addAttribute("products", produitRepository.findProduitByDisponible("vrai"));
            erreur="commande vide";
            model.addAttribute("erreur",erreur);
            return "Catalogue";
        }
            //gestion retour d'erreur-----------
        if(!(testClient.getNom().equals("didier"))) {
            erreur = "nom de client invalide";
        }
        if (testClient.getNom().equals("didier") &&
                !(testClient.getMotdepasse().equals("tigger"))) {
            erreur = "mot de passe invalide";
        }
            //----------------------------------
        model.addAttribute("erreur",erreur);
        model.addAttribute("testClient",testClient);
        return "index";
        }



    }


