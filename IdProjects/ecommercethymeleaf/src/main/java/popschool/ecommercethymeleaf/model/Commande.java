package popschool.ecommercethymeleaf.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "COMMANDE", schema = "ecommerce", catalog = "")
public class Commande {
    private int ncommande;
    private Date datecde;
    private String nomclient;
    private Client clientByNomclient;
    private Collection<DetailCde> detailCdesByNcommande;

    @Id
    @Column(name = "NCOMMANDE", nullable = false)
    public int getNcommande() {
        return ncommande;
    }

    public void setNcommande(int ncommande) {
        this.ncommande = ncommande;
    }

    @Basic
    @Column(name = "DATECDE", nullable = true)
    public Date getDatecde() {
        return datecde;
    }

    public void setDatecde(Date datecde) {
        this.datecde = datecde;
    }

//    @Basic
//    @Column(name = "NOMCLIENT", nullable = false, length = 20)
//    public String getNomclient() {
//        return nomclient;
//    }

    public void setNomclient(String nomclient) {
        this.nomclient = nomclient;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Commande that = (Commande) o;
        return ncommande == that.ncommande &&
                Objects.equals(datecde, that.datecde) &&
                Objects.equals(nomclient, that.nomclient);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ncommande, datecde, nomclient);
    }

    @ManyToOne
    @JoinColumn(name = "NOMCLIENT", referencedColumnName = "NOM", nullable = false)
    public Client getClientByNomclient() {
        return clientByNomclient;
    }

    public void setClientByNomclient(Client clientByNomclient) {
        this.clientByNomclient = clientByNomclient;
    }

    @OneToMany(mappedBy = "commandeByNcommande")
    public Collection<DetailCde> getDetailCdesByNcommande() {
        return detailCdesByNcommande;
    }

    public void setDetailCdesByNcommande(Collection<DetailCde> detailCdesByNcommande) {
        this.detailCdesByNcommande = detailCdesByNcommande;
    }
}
