package popschool.ecommercethymeleaf.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;
@Data
@Entity
@Table(name = "CLIENT", schema = "ecommerce", catalog = "")
public class Client {
    private String nom;
    private String motdepasse;
    private Collection<Commande> commandesByNom;

    @Id
    @Column(name = "NOM", nullable = false, length = 20)
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Basic
    @Column(name = "MOTDEPASSE", nullable = false, length = 8)
    public String getMotdepasse() {
        return motdepasse;
    }

    public void setMotdepasse(String motdepasse) {
        this.motdepasse = motdepasse;
    }

    public Client() {    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client that = (Client) o;
        return Objects.equals(nom, that.nom) &&
                Objects.equals(motdepasse, that.motdepasse);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nom, motdepasse);
    }

    @OneToMany(mappedBy = "clientByNomclient")
    public Collection<Commande> getCommandesByNom() {
        return commandesByNom;
    }

    public void setCommandesByNom(Collection<Commande> commandesByNom) {
        this.commandesByNom = commandesByNom;
    }
}
