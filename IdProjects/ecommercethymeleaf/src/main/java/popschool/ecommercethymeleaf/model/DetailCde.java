package popschool.ecommercethymeleaf.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "DETAIL_CDE", schema = "ecommerce", catalog = "")
public class DetailCde {
    private int ndetail;
    private int ncommande;
    private int quantite;
    private int nproduit;
    private Commande commandeByNcommande;
    private Produit produitByNproduit;

    @Id
    @Column(name = "NDETAIL", nullable = false)
    public int getNdetail() {
        return ndetail;
    }

    public void setNdetail(int ndetail) {
        this.ndetail = ndetail;
    }

//    @Basic
//    @Column(name = "NCOMMANDE", nullable = false)
//    public int getNcommande() {
//        return ncommande;
//    }

    public void setNcommande(int ncommande) {
        this.ncommande = ncommande;
    }

    @Basic
    @Column(name = "QUANTITE", nullable = false)
    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

//    @Basic
//    @Column(name = "NPRODUIT", nullable = false)
//    public int getNproduit() {
//        return nproduit;
//    }

    public void setNproduit(int nproduit) {
        this.nproduit = nproduit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DetailCde that = (DetailCde) o;
        return ndetail == that.ndetail &&
                ncommande == that.ncommande &&
                quantite == that.quantite &&
                nproduit == that.nproduit;
    }

    @Override
    public int hashCode() {
        return Objects.hash(ndetail, ncommande, quantite, nproduit);
    }

    @ManyToOne
    @JoinColumn(name = "NCOMMANDE", referencedColumnName = "NCOMMANDE", nullable = false)
    public Commande getCommandeByNcommande() {
        return commandeByNcommande;
    }

    public void setCommandeByNcommande(Commande commandeByNcommande) {
        this.commandeByNcommande = commandeByNcommande;
    }

    @ManyToOne
    @JoinColumn(name = "NPRODUIT", referencedColumnName = "NPRODUIT", nullable = false)
    public Produit getProduitByNproduit() {
        return produitByNproduit;
    }

    public void setProduitByNproduit(Produit produitByNproduit) {
        this.produitByNproduit = produitByNproduit;
    }
}
