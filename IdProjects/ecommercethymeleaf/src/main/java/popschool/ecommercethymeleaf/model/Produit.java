package popschool.ecommercethymeleaf.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "PRODUIT")
public class Produit {
    private Integer nproduit;
    private String descriptif;
    private BigDecimal prix;
    private String disponible;
    private int qteenstock;
    private Collection<DetailCde> detailCdesByNproduit;

    @Id
    @Column(name = "NPRODUIT", nullable = false)
    public int getNproduit() {
        return nproduit;
    }

    public void setNproduit(int nproduit) {
        this.nproduit = nproduit;
    }

    @Basic
    @Column(name = "DESCRIPTIF", nullable = false, length = 20)
    public String getDescriptif() {
        return descriptif;
    }

    public void setDescriptif(String descriptif) {
        this.descriptif = descriptif;
    }

    @Basic
    @Column(name = "PRIX", nullable = false, precision = 2)
    public BigDecimal getPrix() {
        return prix;
    }

    public void setPrix(BigDecimal prix) {
        this.prix = prix;
    }

    @Basic
    @Column(name = "DISPONIBLE", nullable = false, length = 4)
    public String getDisponible() {
        return disponible;
    }

    public void setDisponible(String disponible) {
        this.disponible = disponible;
    }

    @Basic
    @Column(name = "QTEENSTOCK", nullable = false)
    public int getQteenstock() {
        return qteenstock;
    }

    public void setQteenstock(int qteenstock) {
        this.qteenstock = qteenstock;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Produit that = (Produit) o;
        return nproduit == that.nproduit &&
                qteenstock == that.qteenstock &&
                Objects.equals(descriptif, that.descriptif) &&
                Objects.equals(prix, that.prix) &&
                Objects.equals(disponible, that.disponible);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nproduit, descriptif, prix, disponible, qteenstock);
    }

    @OneToMany(mappedBy = "produitByNproduit")
    public Collection<DetailCde> getDetailCdesByNproduit() {
        return detailCdesByNproduit;
    }

    public void setDetailCdesByNproduit(Collection<DetailCde> detailCdesByNproduit) {
        this.detailCdesByNproduit = detailCdesByNproduit;
    }
}
