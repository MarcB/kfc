package popschool.ecommercethymeleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EcommercethymeleafApplication {

    public static void main(String[] args) {
        SpringApplication.run(EcommercethymeleafApplication.class, args);
    }

}
