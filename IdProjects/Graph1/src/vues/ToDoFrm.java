package vues;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ToDoFrm {
    private JButton ajouterButton;
    private JButton supprimerButton;
    private JList<String> toDoList;
    private JTextField toDotextField1;
    private JPanel rootPanel;



    public ToDoFrm() {
        DefaultListModel<String>modele =new DefaultListModel<>();
        toDoList.setModel(modele);

        ajouterButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
            String aux= toDotextField1.getText();
            if(aux.isEmpty()||aux.isBlank())
            return;
            modele.addElement(aux);
            toDotextField1.setText("");
            supprimerButton.setEnabled(false);
            }
        });
        toDoList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent listSelectionEvent) {
                supprimerButton.setEnabled(true);
            }
        });
        supprimerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String item=toDoList.getSelectedValue();
                modele.removeElement(item);
                supprimerButton.setEnabled((false));
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Ma ToDo List");
        frame.setContentPane(new ToDoFrm().rootPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
