package modele;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


@Entity
public class Service implements Serializable {

    private static final long serialVersionUID=1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;


    @Column(nullable=false,unique=true,length=30)
    private String nom;

    @Column(nullable=false,length=40)
    private String situation;


    public Service() {

    }


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getSituation() {
        return situation;
    }

    public void setSituation(String situation) {
        this.situation = situation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Service service = (Service) o;
        return nom.equals(service.nom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nom);
    }

    @Override
    public String toString() {
        return "Service{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", situation='" + situation + '\'' +
                '}';
    }



    @OneToMany(mappedBy="service")
    private List<Medecin> medecins=new ArrayList<>();

    @ManyToOne
    @JoinColumn(name="responsable_id")
    private Medecin responsable;

    public Service(String nom,String situation){
        this.nom="Nouveau service";
        this.situation="Nouveau lieu";

    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Medecin> getMedecins() {
        return medecins;
    }

    public void setMedecins(List<Medecin> medecins) {
        this.medecins = medecins;
    }

    public void addMedecin(Medecin med) {
        if (med == null)
            return;
        if (med.getService() != null) {


            if (med.getService().equals(this))
                return;
            else med.getService().getMedecins().remove(med);
        }
        medecins.add(med);
        med.setService(this);
    }
    public Medecin getResponsable() {
        return responsable;
    }

    public void setResponsable(Medecin responsable) {
        this.responsable = responsable;
    }
}
