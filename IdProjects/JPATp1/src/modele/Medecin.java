package modele;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

@Entity

@DiscriminatorValue(   )



public class Medecin extends Personne implements Serializable {
    private static final long SerialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false, length = 30)
    private String nom;
    @Column(nullable = false, length = 30)
    private String prenom;

    private float salaire;

    @ManyToOne
    @JoinColumn(name = "service_id")
    private Service service;

    @ManyToOne
    @JoinColumn(name = "manager_id")
    private Medecin mgr;

    @OneToMany(mappedBy = "mgr")
    private Collection<Medecin> subs = new ArrayList<>();

    protected Medecin() {
        nom = "Dupond";
        prenom = "Paul";
    }

    public Medecin(String nom, String prenom, float salaire) {
        this.nom = nom;
        this.prenom = prenom;
        this.salaire = salaire;
       // this.service = service;
    }

    public static long getSerialVersionUID() {
        return SerialVersionUID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public float getSalaire() {
        return salaire;
    }

    public void setSalaire(float salaire) {
        this.salaire = salaire;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public Medecin getMgr() {
        return mgr;
    }

    public void setMgr(Medecin mgr) {
        this.mgr = mgr;
    }

    public Collection<Medecin> getSubs() {
        return subs;
    }

    public void setSubs(Collection<Medecin> subs) {
        this.subs = subs;
    }


    public void getMgr(Medecin mgr) {
        if (mgr == null) return;
        if (this.getMgr() != null) {
            if (this.getMgr().equals(mgr))
                return;
            else
                this.getMgr().getSubs().remove(this);
        }

        if(mgr!=null){
            mgr.subs.add(this);
        }

        this.mgr = mgr;
    }


    @Override
    public String toString() {
        return "Medecin{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", salaire=" + salaire +
                ", service=" + service +
                ", mgr=" + mgr +
                '}';
    }
}