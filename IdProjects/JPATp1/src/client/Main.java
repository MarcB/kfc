package client;
import modele.Medecin;
import modele.Service;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        //Creer et sauvegarder des medecins attribués à un service avec herarchie entre medecins
        String query = "SELECT s FROM Service s ORDER BY s.nom DESC";
        String query1 = "SELECT m FROM Medecin m ORDER BY m.nom";
        Service serv1 = new Service("Cardiologie", "Bat A, 1er étage");
        Service serv2 = new Service("Gériatrie", "Bat B rez-de-chaussée");
        Service serv3 = new Service("Urgence", "Bat A");
        Medecin med1 = new Medecin("Marc","Jean", 1800);
        Medecin med2 = new Medecin("Poivre","Olivier", 2000);
        Medecin med3 = new Medecin("Gator","Ali", 1900);
        Medecin med4 = new Medecin("Enfayitte","Melusine", 2100);
        Medecin med5 = new Medecin("Pilaf","Henri", 1900);
        Medecin med6 = new Medecin("Trancenne","Jean", 2100);
        serv1.addMedecin(med1);
        serv1.addMedecin(med2);
        serv2.addMedecin(med3);
        serv2.addMedecin(med4);
        serv3.addMedecin(med5);
        serv3.addMedecin(med6);
        med2.setMgr(med1);
        med4.setMgr(med3);
        med6.setMgr(med5);
        serv1.setResponsable(med2);
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("JavaJPA_HopitalPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        try{
            em.persist(serv1);
            em.persist(serv2);
            em.persist(serv3);
            em.persist(med1);
            em.persist(med2);
            em.persist(med3);
            em.persist(med4);
            em.persist(med5);
            em.persist(med6);
            em.getTransaction().commit();
            List<Service> liste = em.createQuery(query, Service.class).getResultList();
            for (Service s : liste){
                System.out.println(s);
            }
            List<Medecin> liste1 = em.createQuery(query1, Medecin.class).getResultList();
            for (Medecin m : liste1){
                System.out.println(m);
            }
        }
        catch (Exception e){
            e.printStackTrace();
            em.getTransaction().rollback();
        }
        finally {
            em.close();
        }
    }
}