package com.pop.reactive_mongo_book;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReactiveMongoBookApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReactiveMongoBookApplication.class, args);
    }

}
