package modele;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.Objects;

@javax.persistence.Entity
@javax.persistence.Table(name = "payment", schema = "my_ecommerce", catalog = "")
public class PaymentEntity {
    private int id;
    private BigDecimal montant;
    private Date datePaiement;
    private String typePayment;
    private String creditCard;
    private Date creditCardExpiration;

    @javax.persistence.Id
    @javax.persistence.Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "montant")
    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "date_paiement")
    public Date getDatePaiement() {
        return datePaiement;
    }

    public void setDatePaiement(Date datePaiement) {
        this.datePaiement = datePaiement;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "type_payment")
    public String getTypePayment() {
        return typePayment;
    }

    public void setTypePayment(String typePayment) {
        this.typePayment = typePayment;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "credit_card")
    public String getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(String creditCard) {
        this.creditCard = creditCard;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "credit_card_expiration")
    public Date getCreditCardExpiration() {
        return creditCardExpiration;
    }

    public void setCreditCardExpiration(Date creditCardExpiration) {
        this.creditCardExpiration = creditCardExpiration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PaymentEntity that = (PaymentEntity) o;
        return id == that.id &&
                Objects.equals(montant, that.montant) &&
                Objects.equals(datePaiement, that.datePaiement) &&
                Objects.equals(typePayment, that.typePayment) &&
                Objects.equals(creditCard, that.creditCard) &&
                Objects.equals(creditCardExpiration, that.creditCardExpiration);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, montant, datePaiement, typePayment, creditCard, creditCardExpiration);
    }
}
