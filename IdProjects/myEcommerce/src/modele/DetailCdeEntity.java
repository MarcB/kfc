package modele;

import java.util.Objects;

@javax.persistence.Entity
@javax.persistence.Table(name = "detail_cde", schema = "my_ecommerce", catalog = "")
public class DetailCdeEntity {
    private int id;
    private int qte;

    @javax.persistence.Id
    @javax.persistence.Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "qte")
    public int getQte() {
        return qte;
    }

    public void setQte(int qte) {
        this.qte = qte;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DetailCdeEntity that = (DetailCdeEntity) o;
        return id == that.id &&
                qte == that.qte;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, qte);
    }
}
