package modele;

import java.math.BigDecimal;
import java.util.Objects;

@javax.persistence.Entity
@javax.persistence.Table(name = "item", schema = "my_ecommerce", catalog = "")
public class ItemEntity {
    private int id;
    private String categorie;
    private String titre;
    private BigDecimal prix;
    private int codeBarre;

    @javax.persistence.Id
    @javax.persistence.Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "categorie")
    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "titre")
    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "prix")
    public BigDecimal getPrix() {
        return prix;
    }

    public void setPrix(BigDecimal prix) {
        this.prix = prix;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "code_barre")
    public int getCodeBarre() {
        return codeBarre;
    }

    public void setCodeBarre(int codeBarre) {
        this.codeBarre = codeBarre;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ItemEntity that = (ItemEntity) o;
        return id == that.id &&
                codeBarre == that.codeBarre &&
                Objects.equals(categorie, that.categorie) &&
                Objects.equals(titre, that.titre) &&
                Objects.equals(prix, that.prix);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, categorie, titre, prix, codeBarre);
    }
}
