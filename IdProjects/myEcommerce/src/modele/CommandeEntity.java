package modele;

import java.sql.Date;
import java.util.Objects;

@javax.persistence.Entity
@javax.persistence.Table(name = "commande", schema = "my_ecommerce", catalog = "")
public class CommandeEntity {
    private int id;
    private Date datecde;

    @javax.persistence.Id
    @javax.persistence.Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "datecde")
    public Date getDatecde() {
        return datecde;
    }

    public void setDatecde(Date datecde) {
        this.datecde = datecde;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommandeEntity that = (CommandeEntity) o;
        return id == that.id &&
                Objects.equals(datecde, that.datecde);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, datecde);
    }
}
