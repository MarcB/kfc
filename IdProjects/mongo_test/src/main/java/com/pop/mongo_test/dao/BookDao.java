package com.pop.mongo_test.dao;

import com.pop.mongo_test.model.Book;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface BookDao extends MongoRepository<Book,String> {
}
