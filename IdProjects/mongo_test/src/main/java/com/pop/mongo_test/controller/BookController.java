package com.pop.mongo_test.controller;

import com.pop.mongo_test.model.Book;
import com.pop.mongo_test.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Optional;

@RestController
public class BookController {
    @Autowired
    private BookService bookService;

    //book(id) =============================================
    @GetMapping("/book/{id}" )
    public Optional<Book> findById(@PathVariable("id")String id){
        return bookService.findById(id);
    }

    @GetMapping("/book" )
    public Collection<Book> findBAll(){

        return bookService.findAll();
    }

    @PostMapping("/book/create")
    public void  addBook(@RequestBody Book book){

        bookService.createBook(book);
    }

    // update book
    @PutMapping(value = "/book/{id}")
    public void bookUpdate(@RequestBody Book book,@PathVariable("id") String id){
        book.setId(id);
        bookService.updateBook(book);
    }

    //delete du client selon id
    @DeleteMapping("/book/{id}")
    public void deleteBook(@PathVariable("id") String id) {
        bookService.deleteBook(id);
    }

}
