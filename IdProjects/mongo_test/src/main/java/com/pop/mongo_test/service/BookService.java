package com.pop.mongo_test.service;

import com.pop.mongo_test.dao.BookDao;
import com.pop.mongo_test.model.Book;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BookService {

    BookDao bookDao;

    public void createBook(Book book) {
        bookDao.save(book);
    }

    public List<Book> findAll(){
        return bookDao.findAll();
    }

    public Optional<Book> findById(String id){
        return bookDao.findById(id);//Optional.ofNullable(null);
    }

    public void deleteBook(String id){
        bookDao.deleteById(id);
    }

    public void updateBook(Book book){
        bookDao.save(book);
    }

    public void deleteAllBook(){
        bookDao.deleteAll();
    }

}
