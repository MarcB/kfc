package com.pop.hateoas_cinema_client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HateoasCinemaClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(HateoasCinemaClientApplication.class, args);
    }

}
