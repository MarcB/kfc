package com.pop.hateoas_cinema_client.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;
import uk.co.blackpepper.bowman.annotation.LinkedResource;
import uk.co.blackpepper.bowman.annotation.RemoteResource;
import uk.co.blackpepper.bowman.annotation.ResourceId;

import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Data
@RemoteResource("/acteur")
public class Acteur {

    private URI id;
    private List<Film> films = new ArrayList<>();
    private String nom;
    private String prenom;
    private Date naissance;
    private Pays pays;
    private Integer nbrefilms;

    @LinkedResource
    public Pays getPays() {        return pays;    }
    public void setPays(Pays pays) {        this.pays = pays;    }

    @ResourceId
    public URI getId(){        return id;    }

    protected Acteur(){}

    @Override
    public String toString() {
        return "Acteur{" +
                "nacteur=" + getId() +
                ", nom='" + getNom() + '\'' +
                ", prenom='" + getPrenom() + '\'' +
                ", naissance=" + getNaissance() +
                ", pays=" + getPays() +
                ", nbrefilms=" + getNbrefilms() +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Acteur)) return false;
        Acteur acteur = (Acteur) o;
        return nom.equals(acteur.nom) &&
                prenom.equals(acteur.prenom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nom, prenom);
    }
}
