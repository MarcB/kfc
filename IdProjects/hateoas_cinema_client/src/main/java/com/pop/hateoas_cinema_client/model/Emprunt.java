package com.pop.hateoas_cinema_client.model;

import lombok.Data;

import java.util.Date;

@Data

public class Emprunt {

    private Long nemprunt;
    private Client client;
    private Film film;
    private String retour;
    private Date dateemprunt;

    public Emprunt() {
    }

    public Emprunt(Client client, Film film, String retour, Date dateemprunt){
        this.client=client;
        this.film=film;
        this.retour=retour;
        this.dateemprunt=dateemprunt;
    }

    @Override
    public String toString() {
        return "Emprunt{" +
                "nemprunt=" + nemprunt +
                ", client=" + client.getNom() +
                ", film=" + film.getTitre() +
                ", retour='" + retour + '\'' +
                ", dateemprunt=" + dateemprunt +
                '}';
    }
}
