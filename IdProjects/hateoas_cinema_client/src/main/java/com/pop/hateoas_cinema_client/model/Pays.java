package com.pop.hateoas_cinema_client.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;
import uk.co.blackpepper.bowman.InlineAssociationDeserializer;
import uk.co.blackpepper.bowman.annotation.LinkedResource;
import uk.co.blackpepper.bowman.annotation.RemoteResource;
import uk.co.blackpepper.bowman.annotation.ResourceId;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@Data
@RemoteResource("/pays")
public class Pays {

    private URI id;

    private String nom;

    private List<Acteur> acteurs = new ArrayList<>();

    //permet de regler le problème des many to one et one to many du coté client
    @JsonDeserialize(contentUsing = InlineAssociationDeserializer.class)
//    @LinkedResource
    public List<Acteur> getActeurs(){ return acteurs;}

    @ResourceId
    public URI getId(){
        return id;
    }

    public Pays(String nom) {
        this.nom = nom;
    }

    public Pays() {
    }


    public Pays(URI id, String nom, List<Acteur> acteurs) {
        this.id = id;
        this.nom = nom;
        this.acteurs = acteurs;
    }

    @Override
    public String toString() {
        return "Pays{" +
                "id=" + getId() +
                ", nom='" + getNom() + '\'' +
                ","+getActeurs()+
                '}';
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}