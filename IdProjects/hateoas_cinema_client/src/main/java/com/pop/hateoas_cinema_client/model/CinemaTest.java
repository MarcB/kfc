package com.pop.hateoas_cinema_client.model;

import net.minidev.json.JSONUtil;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.client.Traverson;
import org.springframework.hateoas.server.core.TypeReferences;
import uk.co.blackpepper.bowman.Client;
import uk.co.blackpepper.bowman.ClientFactory;
import uk.co.blackpepper.bowman.Configuration;
import com.pop.hateoas_cinema_client.model.Pays;

import java.net.URI;
import java.util.Date;


public class CinemaTest {
    public static void main(String[] args) {
        //bowman
        //list acteurs
        ClientFactory clientFactory = Configuration.build().buildClientFactory();
        Client<Acteur> client = clientFactory.create(Acteur.class);
        Iterable<Acteur> acteur = client.getAll(URI.create("http://localhost:8080/acteur"));
        //affichage
        System.out.println("*******************************************************************************");
        System.out.println(acteur);
        System.out.println("*******************************************************************************");

        //list de pays
        // construction de l url avec une base uri
        ClientFactory clientFactory1 = Configuration.builder().setBaseUri("http://localhost:8080").build().buildClientFactory();
        Client<Pays> clientPays = clientFactory1.create(Pays.class);

        Iterable<Pays> pays = clientPays.getAll(URI.create("http://localhost:8080/pays"));
        //affichage
        System.out.println("*******************************************************************************");
        pays.forEach(p -> System.out.println(p));
        System.out.println("*******************************************************************************");

        //************************************************************
        //version Traverson
//        Traverson client2 = new Traverson(URI.create("http://localhost:8080/acteur"), MediaTypes.HAL_JSON);
//        CollectionModel<EntityModel<Acteur>> acteurs = client2.follow("acteurs")
//                .toObject(new TypeReferences.CollectionModelType<EntityModel<Acteur>>() {
//                });
//        acteurs.forEach(r -> {
//            System.out.println(r.getContent());
//        });

        //bowman post
//        URI u1=client.post(new Pays((long) 50,"JsonLand"));
//        URI u2=clientActeur.post(new Acteur("Finlande");


// ****************************************************************
//                      creation d'un acteur                      *
// ***************************    +    ****************************
//                     creation d'une nationalité                 *
// ****************************************************************

        String URL_LOCAL = "http://localhost:8080/";
        ClientFactory clientFactoryPost = Configuration.builder().setBaseUri(URL_LOCAL).build().buildClientFactory();
        Client<Pays> clientPaysPost = clientFactory.create(Pays.class);
        Client<Acteur> clientActeurPost = clientFactory.create(Acteur.class);

        //create nationality before actor
        Pays nationalityCreate = new Pays();
        nationalityCreate.setNom("Vinland");
        URI id = clientPays.post(nationalityCreate);
//        nationalityCreate.setId(URI.create("http://localhost:8080/pays/24"));
        nationalityCreate.setId(id);

        //create actor
        Acteur acteurCreate = new Acteur();
        acteurCreate.setPays(nationalityCreate);
        acteurCreate.setNom("Clavier");
        acteurCreate.setPrenom("Christian");
        Date date = new Date(1986, 02, 24);


        acteurCreate.setNaissance(date);

        URI uri2 = clientActeurPost.post(acteurCreate);


    }
}
