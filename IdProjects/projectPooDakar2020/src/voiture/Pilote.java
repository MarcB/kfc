package voiture;

public class Pilote  extends  Personne {
        //attribute s'instance
        private String sponsor;

        //getter setter


        public String getSponsor() {
            return sponsor;
        }

        public void setSponsor(String specialite) {
            this.sponsor = (specialite==null ? "tesla" :sponsor.toLowerCase());
        }

        public Pilote(String nom, String prenom, String sponsor) {
            super(nom, prenom);
            this.setSponsor(sponsor);
        }
        public Pilote(String nom, String prenom) {

            this(nom,prenom,null);
        }


        public Pilote() {
            this.setSponsor( "tesla");
        }

        @Override
        protected boolean estCompatible(Voiture v){
            if(v==null) return false;
            //renvoie la sous class
            return (v instanceof Rallye);

        }


        @Override
        public String toString() {
            return super.toString()+"\n\t\t"+
                    "sponsor='" + sponsor + '\'' +
                    '}';
        }

        public static void main(String[] args) {
            Personne t=new voiture.Pilote("Afreux","Pierre");
            System.out.println(t);

        }
    }

}
