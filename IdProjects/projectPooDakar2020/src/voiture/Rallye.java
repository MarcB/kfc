package voiture;

public class Rallye  extends Voiture {
    private int gain;

    public int getGain() {
        return gain;
    }

    public void setGain(int gain) {
        this.gain = (gain <5? 5 : gain);
    }
    //constructors

    public Rallye(String marque, int puissance, char carburant, int gain) {
        super(marque, puissance, carburant);
        this.setGain(gain);
    }

    public Rallye(int gain) {
        super();
        this.gain = gain;
    }

    public Rallye(String marque, Moteur moteur, int gain) {
        super(marque, moteur);
        this.gain = gain;
    }

    @Override
    public String toString() {
        return super.toString()+"\n\t\t"+
                "gain=" + gain +
                '}';
    }

    public static void main(String[] args) {
        Rallye c= new Rallye(0);
        System.out.println(c);
    }
}
