package voiture;

public class Technicien extends  Personne {
    //attribute s'instance
    private String specialite;

    //getter setter


    public String getSpecialite() {
        return specialite;
    }

    public void setSpecialite(String specialite) {
        this.specialite = (specialite==null ? "mecanicien" :specialite.toLowerCase());
    }

    public Technicien(String nom, String prenom, String specialite) {
        super(nom, prenom);
        this.setSpecialite(specialite);
    }
    public Technicien(String nom, String prenom) {

        this(nom,prenom,null);
    }


    public Technicien() {
        this.setSpecialite( "mecanicien");
    }

    @Override
    protected boolean estCompatible(Voiture v){
        if(v==null) return false;
        //renvoie la sous class
        return v.getClass()==Camion.class;

    }


    @Override
    public String toString() {
        return super.toString()+"\n\t\t"+
                "specialite='" + specialite + '\'' +
                '}';
    }

    public static void main(String[] args) {
        Personne t=new Technicien("Afreux","Pierre");
        System.out.println(t);

    }
}
