package voiture;

public class Moteur {
    //variable d'instance
    private int puissance;
    private char carburant;

    //getter setter


    public int getPuissance() {
        return puissance;
    }

    public void setPuissance(int puissance) {
        this.puissance = (puissance<=1000? 1000 :puissance);
    }

    public char getCarburant() {
        return carburant;
    }

    public void setCarburant(char carburant) {
        this.carburant = (carburant != 'E' && carburant != 'D' ? 'E' :carburant);
    }

    public Moteur(){
        setCarburant('E');
        setPuissance(1000);

    }
    public Moteur(int puissance,char carburant){
        this.setPuissance(puissance);
        this.setCarburant(carburant);
    }

    @Override
    public String toString() {
        return "Moteur{" +
                "puissance=" + puissance +
                ", carburant=" + carburant +
                '}';
    }

    //test unitaire
    public static void main(String[] args){
        Moteur m1=new Moteur(  1200,  'D');
        Moteur m2=new Moteur(  1600,  'E');

        System.out.println(m1);
        System.out.println(m2);
    }


}
   class Voiture {
        private static int derniereImma=1;
        private int imma;
        private String marque;

        //composition
       private Moteur moteur= new Moteur();

       public int getImmam(){
           return imma;
       }
       public int getDerniereImma(){
           return derniereImma;
       }

        private void setDerniereImma(){
            Voiture.derniereImma++;
       }

        private void setImma(int derniereImma){
          Voiture.setDerniereImma();
           this.imma=Voiture.derniereImma;

        }

            private void setMarque(String marque){
                 this.marque=(marque==null ||marque.trim().equals("")? "FIAT": marque.toUpperCase());
                }


             public Voiture(String marque,int puissance,char carburant){

           this.setMarque(marque);
           this.moteur=new Moteur(puissance,carburant);
           this.setImma();
             }

             public Voiture(){
            this(null,100,'E');
             }

             public Voiture(String marque,Moteur moteur){
                if (moteur!=null)
                    this.moteur= new Moteur(moteur.getPuissance(), moteur.getCarburant());
                this.setImma();
                this.setMarque(marque);

             }
       public boolean estDisponible(){
           return(this.conducteur==null);
       }

       @Override
       public String toString() {
           return this.getClass().getSimpleName()+"{" +
                   "imma=" + imma +
                   ", marque='" + marque + '\'' +
                   ", moteur=" + moteur +
                   ", conducteur=" +(conducteur==null? "Aucun"
                                :"je suis conduite par"+conducteur.getNom())+
                   '}';
       }

       public static void main(String[] args) {
           Voiture v1=new Voiture();

           System.out.println(v1);
       }




   }

