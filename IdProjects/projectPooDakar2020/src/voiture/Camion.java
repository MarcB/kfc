package voiture;

public class Camion extends Voiture {
    private int tonnage;

    public int getTonnage() {
        return tonnage;
    }

    public void setTonnage(int tonnage) {
        this.tonnage = (tonnage<5? 5 :tonnage);
    }
    //constructors

    public Camion(String marque, int puissance, char carburant, int tonnage) {
        super(marque, puissance, carburant);
        this.setTonnage(tonnage);
    }

    public Camion(int tonnage) {
        super();
        this.tonnage = tonnage;
    }

    public Camion(String marque, Moteur moteur, int tonnage) {
        super(marque, moteur);
        this.tonnage = tonnage;
    }

    @Override
    public String toString() {
        return super.toString()+"\n\t\t"+
                "tonnage=" + tonnage +
                '}';
    }

    public static void main(String[] args) {
        Camion c= new Camion(0);
        System.out.println(c);
    }
}
