    package voiture;


    import javax.imageio.stream.ImageInputStream;

    public abstract class Personne{
        private static int dernierMatricule=0;

        private int matricule;
        private String nom;
        private String prenom;

        protected boolean estCompatible(Voiture v){
            return false;
        }
        //association
        private Voiture voiture =null; //je nais "piéton"

        //composition
        private Personne conducteur =null;//je nais "libre"

        public Personne( String nom, String prenom) {
            Personne.dernierMatricule++;
            this.matricule = Personne.dernierMatricule;
            this.setNom(nom);
            this.setPrenom(prenom);
        }

        public int getMatricule() {
            return matricule;
        }

        public String getNom() {
            return nom;
        }

        public Voiture getVoiture() {
            return voiture;
        }

        public void setVoiture(Voiture voiture) {
            this.voiture = voiture;
        }


        public Personne getConducteur() {
            return conducteur;
        }

        public void setConducteur(Personne conducteur) {
            this.conducteur = conducteur;
        }

        public void affecter(Voiture v){
            //cas 0 la voiture doit exister
            if(v==null)return;
            //cas 1 la voiture doit existe
            if(! this.estCompatible(v)) return;
            //cas 2: la voiture n'est pas dispo
            if(! v.estDisponible()) return;
            //cas 3 je ne suis pas pieton
            if(! this.estPieton()) return;
            //action 1 la voiture a un conducteur
            v.setConducteur(this);
            //action 2 je suis conducteur de v
            this.setVoiture(v);

        }

        public boolean estPieton(){
            return(this.voiture==null);
    }

        @Override
        public String toString() {
            return this.getClass().getSimpleName()+"{" +
                    "matricule=" + matricule +
                    ", nom='" + nom + '\'' +
                    ", prenom='" + prenom + '\'' +
                    ", voiture=" +(voiture==null? "je suis piéton"
                    :"je conduis une"+voiture.getMarque())+
                    '}';
        }

        public void setNom(String nom) {
            this.nom = (nom==null||nom.trim().equals("") ?"ENFAYITTE" :nom.toUpperCase());
        }

        public void setPrenom(String prenom) {
            this.prenom = (nom==null||prenom.trim().equals("") ?"MELUSINE" :nom.toUpperCase());
        }

        public Personne() {
            this(null,null);







        /*
        public static int dernierMatricule =1;
        public int matricule;
        public String nom;
        public String adresse;

        public static int getDernierMatricule() {
            return dernierMatricule;
        }
    /*public int getDernierMatricule() {
            return dernierMatricule;
        }

        public int getMatricule() {
            return matricule;
        }

        public String getNom() {
            return nom;
        }

        public String getAdresse() {
            return adresse;
        }

        public void setDernierMatricule() {
            Personne.dernierMatricule++;
        }

        public void setMatricule() {
            this.matricule = Personne.getDernierMatricule();
        }

        public void setNom(String nom) {
            this.nom = nom;
        }

        public void setAdresse(String adresse) {
            this.adresse = adresse;
        }*/
        }
        public void restituer(){
            //cas 1 : je dois être conducteur
            if(this.estPieton()) return;
            //la voiture redevient disponible
            this.getVoiture().setConducteur(null);
            //je suis pieton
            this.setVoiture(null);

        }


    }
