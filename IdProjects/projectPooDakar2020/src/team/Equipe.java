package team;
import voiture.*;

import java.util.Arrays;

public class Equipe {

    //attribut d instance
    private String nom;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = (nom==null ?"FRANCE":nom.toUpperCase());
    }

    //association 1..5
    private int nbPersonnes=0;
    private int nbVoitures=0;

    private Personne[] personnes=new Personne[6];
    private Voiture[] parc = new Voiture[6];



    //constructeur

    public Equipe(String nom){
            this.setNom(nom);
    }
    //gestion des personnes """"""""""""""

    public void embauche(Personne pers){
        //control d existence
        if(pers==null) return;
        //atteinte de la limite de 5 personnes
        if(this.nbPersonnes==5) return;
        //controle d absence de la nouvelle personne

        //mise à jour de personnel
        this.nbPersonnes++;
        this.personnes[this.nbPersonnes]=pers;
    }
    public void acheter(Voiture voit){
        //control d existence
        if(voit==null) return;
        //atteinte de la limite de 5 personnes
        if(this.nbVoitures==5) return;
        //controle d absence de la nouvelle personne

        //mise à jour de personnel
        this.nbVoitures++;
        this.parc[this.nbPersonnes]=pers;
    }





    private int localiser(Personne pers){
        if(pers==null)return 0;

        //utiliser une sentinnelle  à l'emplacement vide pour comparaison degressive
        this.personnes[0]=pers;
        int i=this.nbPersonnes;
        int cle=pers.getMatricule();
        while(this.personnes[i].getMatricule()!=cle) i=i-1;
        return i;
    }
    public boolean personnePresent(Personne pers){
        return(this.localiser(pers)>0)  ;
    }

    private int localiser(Voiture voit){
        if(voit==null)return 0;

        //utiliser une sentinnelle  à l'emplacement vide pour comparaison degressive
        this.parc[0]=voit;
        int i=this.nbVoitures;
        int cle=voit.getImma();
        while(this.parc[i].getImma()!=cle) i=i-1;
        return i;
    }
    public boolean estPresent(Voiture voit){
        return(this.localiser(voit)>0)  ;
    }
    //regles metier
    public void affecter(Personne pers,Voiture voit){

        int positionP=this.localiser(pers);
        if(positionP==0) return;

        int positionV=this.localiser(voit);
        if(positionV==0) return;

        this.personnes[positionP].affecter(parc[positionV]);
    }

    public void restituer(Personne pers){
        int positionP=this.localiser(pers);
        if(positionP==0) return;

        this.personnes[positionP].restituer();
    }

    @Override
    public String toString() {
        String trame="Equipe:" +"nom='" + nom + '\'' ;
                for(int i=1;i<=nbPersonnes;i++) {
                    trame= trame+" nom=" + personnes[i];
                }
        for(int i=1;i<=nbVoitures;i++) {
            trame= trame+"voiture" + parc[i];
        }
                return trame;
    }

    public static void main(String[] args) {
        Equipe monday =new Equipe("MAY");
        System.out.println(monday);
    }
}
