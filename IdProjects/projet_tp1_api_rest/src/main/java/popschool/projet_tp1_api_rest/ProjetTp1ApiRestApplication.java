package popschool.projet_tp1_api_rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetTp1ApiRestApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjetTp1ApiRestApplication.class, args);
    }

}
