package popschool.projet_tp1_api_rest.controler;

import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import popschool.projet_tp1_api_rest.dao.EmployeeDao;
import popschool.projet_tp1_api_rest.model.Employee;


import java.util.*;

@RestController
public class EmployeeControler {

    @Autowired
    private EmployeeDao employeeDao;

    //URI :
    //contextPath.servletPath/employees/{empNo}
    @GetMapping(value = "/employees")
    public List<Employee> getEmployees(){
        List<Employee> list = employeeDao.findAll();
        return list;
    }

    @GetMapping(value = "/employees/{empNo}")
    public Employee getEmployee(@PathVariable("empNo") String empNo){
        return employeeDao.findById(empNo).get();
    }

    @PostMapping(value = "/employees")
    public void addEmployee(@RequestBody Employee emp){
        employeeDao.save(emp);
    }

    @PutMapping(value = "/employees/{empNo}")
    public Employee updateEmployee(@RequestBody Employee emp,
                                @PathVariable("empNo") String empNo){
        return employeeDao.save(emp);
    }

    @DeleteMapping(value = "/employees/{empNo}")
    public void deleteEmployee(@PathVariable("empNo") String empNo){
        employeeDao.deleteById(empNo);
    }
}
