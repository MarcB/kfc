package popschool.projet_tp1_api_rest.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import popschool.projet_tp1_api_rest.model.Employee;

public interface EmployeeDao extends JpaRepository<Employee, String> {

}
