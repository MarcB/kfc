package popschool.projet_tp1_api_rest.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity
public class Employee {
    @Id
    @Column(name = "empno")
    private String empNo;

    @Column(name = "empname")
    private String empName;

    private String position;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                            CONSTRUCTEUR
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public Employee() {
    }

    public Employee(String empNo, String empName, String position) {
        this.empNo = empNo;
        this.empName = empName;
        this.position = position;
    }

}
