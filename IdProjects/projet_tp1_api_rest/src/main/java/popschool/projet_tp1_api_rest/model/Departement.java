package popschool.projet_tp1_api_rest.model;

import javax.persistence.Column;
import javax.persistence.Id;

public class Departement {

    @Id
    @Column(name = "deptno")
    private String deptNo;

    @Column(name = "deptname")
    private String deptName;

    private String localisation;



}
