package control;

import javax.inject.Named;

@Named
public class Control {
    private String nom="bobbee";

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}
