package db_utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class MyConnection {
    // identifications de la BdD
//    private final static String URL = "jdbc:mysql://localhost:3306/tech_support";
//    private final static String USER = "scott";
//    private final static String PW = "tiger";

    // Singleton : declaration
    private static Connection INSTANCE = null;

    // Singleton : initialisation
    static {
        // Liaison avec la Base de donnees
        Properties propBD = new Properties();
        String filename="propBD.properties" ;
        ClassLoader classLoader = MyConnection.class.getClassLoader();

        try{

            File file = new File(classLoader.getResource(filename).getFile());

            FileInputStream entree =
                    new FileInputStream( file);
            propBD.load( entree );
            entree.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }


        String DRIVER = propBD.getProperty("DRIVER");
        String URL = propBD.getProperty("URL");
        String USER = propBD.getProperty("USER");
        String PW = propBD.getProperty("PW");

        try {
            // ouvrir la connection a la BD
            INSTANCE = DriverManager.getConnection(URL,USER, PW);
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // Singleton : accesseur
    public static Connection getINSTANCE() {
        return INSTANCE;
    }
}
