package beans;

import db_utils.MyConnection;
import java.io.Serializable;
import java.sql.*;

// Composant Métier
public class TechSupportBean implements Serializable{
    // conseillé pour le gestion de version de la sérialisation
	private static final long serialVersionUID = 1L;

	// Correspond aux parametres des deux formulaires HTML
    // formulaire enregistrement incident
    private String email;
    private String software;
    private String os;
    private String problem;

    // formualire enregistrement Client
    private String firstName;
    private String lastName;
    private String phoneNumber;

	
	// Requetes
    private static String insertStatementStr =
        "INSERT INTO customers VALUES(?, ?, ?, ?)";
    private static String selectCustomerStr =
        " SELECT fname, lname, phone " +
        " FROM customers " +
        " WHERE LOWER(email) = ?";
    private static String insertSupp_request_Str =
        " INSERT INTO supp_requests(email,software,os,problem) "+
        " VALUES( ?, ?, ?, ?)";
    
    public TechSupportBean() {
    }

    public void setEmail(String email) {
        this.email = email.toLowerCase();
    }

    public void setSoftware(String software) {
        this.software = software;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public void setProblem(String problem) {
        this.problem = problem;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public String getSoftware() {
        return software;
    }

    public String getOs() {
        return os;
    }

    public String getProblem() {
        return problem;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }


    // Regles métiers
    // acces à la base de données
    public void registerSupportRequest() throws SQLException {
            PreparedStatement insertStatement =
                    MyConnection.getINSTANCE().prepareStatement(insertSupp_request_Str);
            
            insertStatement.setString(1, email.toLowerCase());
            insertStatement.setString(2, software);
            insertStatement.setString(3, os);
            insertStatement.setString(4, problem);

            insertStatement.executeUpdate();

            // On verifie si le client est enregistre ou non
            PreparedStatement selectStatement =
                    MyConnection.getINSTANCE().prepareStatement(selectCustomerStr);
            selectStatement.setString(1, this.email.toLowerCase());

            ResultSet rs = selectStatement.executeQuery();

            if (rs.next()) {
                System.out.println("Le client est enregistre");
                this.setFirstName(rs.getString("fname"));
                this.setLastName(rs.getString("lname"));
                this.setPhoneNumber(rs.getString("phone"));

                // Le client est enregistre - On va a la page "response.jsp"
                this.registered = true;
            } else {
                // Le client n'est pas enregistre. On doit aller au formulaire d'enregistrement
                this.registered = false;
            }
        
    }


    private boolean registered;
    
    public boolean isRegistered() {
        return registered;
    }

    public void registerCustomer() throws SQLException {
        PreparedStatement insertStatement = null;
        
        try {
			MyConnection.getINSTANCE().setAutoCommit(false);
            insertStatement =
                    MyConnection.getINSTANCE().prepareStatement(insertStatementStr);

            insertStatement.setString(1, email.toLowerCase());

            insertStatement.setString(2, firstName);
            insertStatement.setString(3, lastName);
            insertStatement.setString(4, phoneNumber);

            insertStatement.executeUpdate();
        	MyConnection.getINSTANCE().commit();
		} catch (SQLException ex) {
			MyConnection.getINSTANCE().rollback();
			throw ex;
        } 
    }
    
    public static void main(String[] args) {	
		TechSupportBean tech = new TechSupportBean();
		tech.setEmail("ngator@gmail.com");
        tech.setOs("W95");
        tech.setSoftware("Excel");
        tech.setProblem("complexe");
        try {
            tech.registerSupportRequest();

            System.out.println("Connu? :"+tech.isRegistered());
        } catch (SQLException e) {
            e.printStackTrace();
        }

//		tech.setEmail("ngator@gmail.com");
//		tech.setLastName("GATOR");
//		tech.setFirstName("Nathalie");
//		tech.setPhoneNumber("1236");
//
//		try {
//			tech.registerCustomer();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}

		
	}
}
