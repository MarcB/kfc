import java.awt.Dimension;
import java.awt.event.*;
import javax.swing.JFrame;

public class Fenetre extends JFrame {

    public static void main(String[] args) {
        new Fenetre();
    }

    private int timer=50;

    private Panneau pan = new Panneau();

    public Fenetre() {
        this.setTitle("Animation");
        this.setSize(300, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setContentPane(pan);
        this.setVisible(true);

        class Eventlistener implements MouseListener {
            //where initialization occurs:
            //Register for mouse events on blankArea and the panel.


            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                timer=5;
                System.out.println("uuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu");
            }

            @Override
            public void mousePressed(MouseEvent mouseEvent) {
                timer=5; System.out.println("uuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu");
            }

            @Override
            public void mouseReleased(MouseEvent mouseEvent) {
                timer=5; System.out.println("uuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu");
            }

            @Override
            public void mouseEntered(MouseEvent mouseEvent) {
                timer=5; System.out.println("uuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu");
            }

            @Override
            public void mouseExited(MouseEvent mouseEvent) {
                timer=5; System.out.println("uuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu");
            }


        }
        go();




    }

    private void go() {
        // Les coordonnées de départ de notre rond
        int x = pan.getPosX(), y = pan.getPosY();
        // Le booléen pour savoir si l'on recule ou non sur l'axe x
        boolean backX = false;
        // Le booléen pour savoir si l'on recule ou non sur l'axe y
        boolean backY = false;

        // Dans cet exemple, j'utilise une boucle while
        // Vous verrez qu'elle fonctionne très bien
        while (true) {
            // Si la coordonnée x est inférieure à 1, on avance
            if (x < 1)
                backX = false;
            // Si la coordonnée x est supérieure à la taille du Panneau moins la taille du rond, on recule
            if (x > pan.getWidth() - 50)
                backX = true;
            // Idem pour l'axe y
            if (y < 1)
                backY = false;
            if (y > pan.getHeight() - 50)
                backY = true;

            // Si on avance, on incrémente la coordonnée
            // backX est un booléen, donc !backX revient à écrire
            // if (backX == false)
            if (!backX)
                pan.setPosX(++x);
                // Sinon, on décrémente
            else
                pan.setPosX(--x);
            // Idem pour l'axe Y
            if (!backY)
                pan.setPosY(++y);
            else
                pan.setPosY(--y);

            // On redessine notre Panneau
            pan.repaint();
            // Comme on dit : la pause s'impose ! Ici, trois millièmes de seconde
            try {
                Thread.sleep(timer);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

      /*  public class Eventlistener implements MouseListener {
            //where initialization occurs:
            //Register for mouse events on blankArea and the panel.
        Fenetre.addMouseListener(this);
            addMouseListener(this);



            public void mouseClicked(MouseEvent e) {
                this.timer=5;
            }

            public void mouseRelease(MouseEvent e) {
                this.timer=5;
            }
        }*/

    }



}