package Voiture;

public class Technicien extends Personne{
    //attributs d'instance
    private String specialite;

    //getter setter


    public String getSpécialite() {
        return specialite;
    }

    private void setSpécialite(String specialite) {
        this.specialite = (specialite==null
                            ? "mécanicien"
                            : specialite.toLowerCase());
    }

    //constructeur

    public Technicien(String nom, String prenom, String specialite) {
        super(nom,prenom);
        this.setSpécialite(specialite);
    }

    public Technicien(String nom, String prenom) {
        this(nom,prenom, null);
        this.specialite = null;
    }

    public Technicien() {
        this.specialite = "mecanicien";
    }

    @Override
    protected boolean estCompatible(Voiture v){
        if (v==null) return false;
        //return (v instanceof Camion);
        return v.getClass()== Camion.class;
    }

    //to string

    @Override
    public String toString() {
        return super.toString() +"\n\t\t" +
                "spécialite='" + specialite + '\'' +
                '}';
    }

    public static void main(String[] args) {
        Technicien t1 = new Technicien("tutu", "thomas", "nettoyeur");
        System.out.println(t1);
        Voiture v1 = new Camion("volvo", 5000, 'D', 5);
        t1.affecter(v1);
        System.out.println(t1);
    }
}
