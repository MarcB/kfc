package team;

import Voiture.*;

public class Equipe {
    //attributs d'instance
    private String nom;

    //getter setter


    public String getNom() {
        return nom;
    }

    private void setNom(String nom) {
        this.nom = (nom==null?"FRANCE":nom.toUpperCase());
    }

    //association 1<-->5
    private int nbPersonnes = 0;
    private Personne[] personnes = new Personne[6];

    private int nbVoiture = 0;
    private Voiture[] parc = new Voiture[6];

    //constructeur

    public Equipe(String nom) {
        this.setNom(nom);
    }

    //GESTION des personnes
    public void embauche(Personne pers){
        //controle d'existance
        if(pers==null) return;

        //limlite de 5 atteinte?
        if(nbPersonnes==5)return;

        //controle d'absence
        if(this.estPresente(pers)) return;

        //Mise à jour du personnel
        this.nbPersonnes++;
        this.personnes[this.nbPersonnes]= pers;
    }

    private int localiser(Personne pers){
        if(pers==null) return 0;
        this.personnes[0]=pers;
        int i = this.nbPersonnes;
        int cle=pers.getMatricule();

        while (this.personnes[i].getMatricule()!=cle) i=i-1;
        return i;
    }

    private boolean estPresente(Personne pers){
        return (this.localiser(pers)>0);
    }


    //GESTION des véhicules
    public void achat(Voiture v){
        //controle existance
        if(v==null) return;

        //limlite de 5 atteinte?
        if(nbVoiture==5)return;
         //controle d'absence
        if(this.estPresente(v)) return;

         //Mise à jour du personnel
        this.nbVoiture++;
        this.parc[this.nbVoiture]= v;
    }
    private int localiser(Voiture v){
        if(v==null) return 0;
        this.parc[0]=v;
        int i = this.nbVoiture;
        int cle=v.getImma();

        while (this.parc[i].getImma()!=cle) i=i-1;
        return i;
    }

    private boolean estPresente(Voiture v){
        return (this.localiser(v)>0);
    }

    //regle metier

    public void affecter(Personne pers,Voiture v){
        int positionP = localiser(pers);
        if(positionP == 0)return;
        int positionV = localiser(v);
        if(positionV==0)return;
        this.personnes[positionP].affecter(parc[positionV]);
    }

    public void restituer(Personne pers){
        int positionP = localiser(pers);
        if(positionP==0)return;
        this.personnes[positionP].restituer();
    }

    @Override
    public String toString() {
        String str = "Equipe{" +
                "nom='" + nom + "\n";

        str= str + "personnel";
        for (int i=1 ; i<=this.nbPersonnes; i++){
            str = str + "\n\t"+ personnes[i];
        }
        str= str + "parc";
        for (int i=1 ; i<=this.nbVoiture; i++){
            str = str + "\n\t"+ parc[i];
        }

        return str + "\n";
    }

    public static void main(String[] args) {
        Personne p1 = new Pilote("Lefebvre", "Michael", "Monster");
        Personne p2 = new Technicien("Bouchez", "Marc", "Mécanicien");
        Voiture v1 = new Rallye("Nissan",3000,'E', 9);
        Voiture v2 = new Camion("Volvo",5000,'D', 11);
        Equipe e1 = new Equipe("ptitBras");
        e1.achat(v1);
        e1.achat(v2);
        e1.embauche(p1);
        e1.embauche(p2);
        p1.affecter(v1);
        p2.affecter(v2);
        System.out.println(e1);
        System.out.println(p1);
        System.out.println(p2);

    }

}
