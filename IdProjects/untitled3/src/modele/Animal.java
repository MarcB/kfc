package modele;

class Animal  extends Thread {
    int maVitesse;
    String monNom;

    public Animal(int laVitesse, String leNom) {
        monNom = leNom;
        maVitesse = laVitesse;
    }

    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println(monNom);
            try {
                sleep(1000 / maVitesse);
            } catch (Exception e) {

            }

        }
        System.out.println("\n" + monNom + " est arrivé");
    }
}