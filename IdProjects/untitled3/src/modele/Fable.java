package modele;

class Fable {
    static Animal leLievre;
    static Animal laTortue;

    public static void main(String arg[]) throws
            InterruptedException {
        leLievre = new Animal(5, "L");
        laTortue = new Animal(1, "T");
        laTortue.start(); // lancement du thread
        leLievre.start();
        laTortue.join();// attente de la fin des deux threads
        leLievre.join();// avant la sortie du programme
    }
}