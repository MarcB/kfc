package com.pop.testreactif;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.util.ArrayList;
import java.util.List;


public class FluxCreationTest {

    @Test
    public void createFlux_just() {
        Flux<String> fruitFlux = Flux.just("Apple", "Fraise", "Chaussette");

        StepVerifier.create(fruitFlux)
                .expectNext("Apple")
                .expectNext("Fraise")
                .expectNext("Chaussette")
                .verifyComplete();
    }
        @Test
        public void createFlux_justFromIterable(){
            List<String> fruitList=new ArrayList<>();
                    fruitList.add("Apple");
                    fruitList.add("Fraise");
                    fruitList.add("Chaussette");

            StepVerifier.create(fruitList)
                    .expectNext("Apple")
                    .expectNext("Fraise")
                    .expectNext("Chaussette")
                    .verifyComplete();
    }


}
