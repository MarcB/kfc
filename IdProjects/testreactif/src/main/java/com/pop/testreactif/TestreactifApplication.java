package com.pop.testreactif;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import reactor.core.publisher.Mono;

@SpringBootApplication
public class TestreactifApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestreactifApplication.class, args);


        Mono.just("Cartman")
                .map(n -> n.toUpperCase())
                .map(cn -> "Hello, " + cn + "!")
                .subscribe(System.out::println);
    }
}
