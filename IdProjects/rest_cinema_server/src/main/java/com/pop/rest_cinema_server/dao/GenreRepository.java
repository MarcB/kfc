package com.pop.rest_cinema_server.dao;

import org.springframework.data.repository.CrudRepository;
import com.pop.rest_cinema_server.model.Genre;

public interface GenreRepository extends CrudRepository<Genre, Long> {

}
