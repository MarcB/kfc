package com.pop.rest_cinema_server.dao;
import org.springframework.data.repository.CrudRepository;
import com.pop.rest_cinema_server.model.Acteur;

import java.util.List;

public interface ActeurRepository extends CrudRepository<Acteur,Long> {
    List<Acteur> findAll();

    /*
    Optional<Acteur> findActeurByNom(String nom);
    List<Acteur> findByPrenomIsLike(String prenom);
    List<Acteur> findActeurByNaissanceAfter(Date date);
    List<Acteur> findActeurByNaissanceBefore(Date date);
    List<Acteur> findActeurByNomContaining(String nom);
    @Query("Select a from Acteur a where a.nbrefilms =(select max(a2.nbrefilms) from Acteur a2)")
    List<Acteur> findActeurByMaxFilms ();
    List<Acteur> findActeurByNationalite_Nom(String nom);
    List<Acteur> findActeurByNationalite_Npays(Long npays);
    List<Acteur> findActeurByNationalite_NomContaining(String nom);
    */

}