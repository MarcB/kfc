package com.pop.rest_cinema_server.dao;

import org.springframework.data.repository.CrudRepository;
import com.pop.rest_cinema_server.model.Client;

import java.util.Optional;


public interface ClientRepository extends CrudRepository<Client,Long> {
    Optional<Client> findByNclient(Long id);
}
