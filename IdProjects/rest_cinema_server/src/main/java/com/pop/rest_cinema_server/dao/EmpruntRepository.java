package com.pop.rest_cinema_server.dao;

import org.springframework.data.repository.CrudRepository;
import com.pop.rest_cinema_server.model.Emprunt;

import java.util.List;
import java.util.Optional;

public interface EmpruntRepository extends CrudRepository<Emprunt, Long> {

    List<Emprunt>findByClient_Nclient(Long nclient);
    Optional<Emprunt> findByClient_NomAndFilm_TitreAndRetour(String nom, String titre, String non);
}
