package com.pop.rest_cinema_server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestCinemaServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestCinemaServerApplication.class, args);
    }

}
