package com.pop.rest_cinema_server.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="genre")
public class Genre {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long ngenre;

    private String nature;

    @OneToMany(mappedBy = "genre")
    @JsonIgnore
    private List<Film> films = new ArrayList<>();

    public Genre() {
    }

    @Override
    public String toString() {
        return
                "{ nature='" + nature + '\'' +
                '}';

    }

    public Long getNgenre() {
        return this.ngenre;
    }

    public String getNature() {
        return this.nature;
    }

    public List<Film> getFilms() {
        return this.films;
    }

    public void setNgenre(Long ngenre) {
        this.ngenre = ngenre;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public void setFilms(List<Film> films) {
        this.films = films;
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof Genre)) return false;
        final Genre other = (Genre) o;
        if (!other.canEqual((Object) this)) return false;
        final Object this$ngenre = this.getNgenre();
        final Object other$ngenre = other.getNgenre();
        if (this$ngenre == null ? other$ngenre != null : !this$ngenre.equals(other$ngenre)) return false;
        final Object this$nature = this.getNature();
        final Object other$nature = other.getNature();
        if (this$nature == null ? other$nature != null : !this$nature.equals(other$nature)) return false;
        final Object this$films = this.getFilms();
        final Object other$films = other.getFilms();
        if (this$films == null ? other$films != null : !this$films.equals(other$films)) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof Genre;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $ngenre = this.getNgenre();
        result = result * PRIME + ($ngenre == null ? 43 : $ngenre.hashCode());
        final Object $nature = this.getNature();
        result = result * PRIME + ($nature == null ? 43 : $nature.hashCode());
        final Object $films = this.getFilms();
        result = result * PRIME + ($films == null ? 43 : $films.hashCode());
        return result;
    }
}
