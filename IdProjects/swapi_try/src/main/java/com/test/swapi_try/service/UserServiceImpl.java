package com.test.swapi_try.service;

import com.test.swapi_try.model.Films;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.client.Traverson;
import org.springframework.hateoas.server.core.TypeReferences;
import org.springframework.stereotype.Service;
import uk.co.blackpepper.bowman.Client;
import uk.co.blackpepper.bowman.ClientFactory;
import uk.co.blackpepper.bowman.Configuration;

import java.net.URI;
import java.util.List;

@Service
public class UserServiceImpl implements UserService{
    String URL_BASE ="https://swapi.dev/api/";
    @Override
    public List<Films> findAllFilms() {

//        ClientFactory clientFactory = Configuration.builder().setBaseUri(URL_BASE).build().buildClientFactory();
        ClientFactory clientFactory = Configuration.build().buildClientFactory();
        Client<Films> client = clientFactory.create(Films.class);
        Iterable<Films> film =  client.getAll(URI.create(URL_BASE+"films"));
        System.out.println("*******************************************************************************");
        System.out.println(film);
        return (List<Films>) film;
    }

}
