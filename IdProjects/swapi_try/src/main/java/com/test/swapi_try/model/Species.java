package com.test.swapi_try.model;

import lombok.Data;

@Data
public class Species {
    //*******************************************************************************
    //              Attributes:

    private String name ;               // -- The name of this species.
    private String classification ;     //  -- The classification of this species, such as "mammal" or "reptile".
    private String designation ;        //  -- The designation of this species, such as "sentient".
    private String average_height ;     //  -- The average height of this species in centimeters.
    private String average_lifespan ;   //  -- The average lifespan of this species in years.
    private String eye_colors ;         //  -- A comma-separated string of common eye colors for this species,
                                        // "none" if this species does not typically have eyes.
    private String hair_colors ;        //  -- A comma-separated string of common hair colors for this species,
                                        // "none" if this species does not typically have hair.
    private String skin_colors ;        //  -- A comma-separated string of common skin colors for this species,
                                        // "none" if this species does not typically have skin.
    private String language ;           //  -- The language commonly spoken by this species.
    private String homeworld ;          //  -- The URL of a planet resource, a planet that this species originates from.

    //*******************************************************************************
    //              link part

    private String people[] ;           //  -- An array of People URL Resources that are a part of this species.
    private String films[] ;            //  -- An array of Film URL Resources that this species has appeared in.


    //*******************************************************************************
    //             info part

    private String url ;                //  -- the hypermedia URL of this resource.
    private String created ;            //  -- the ISO 8601 date format of the time that this resource was created.
    private String edited ;             //  -- the ISO 8601 date format of the time that this resource was edited.

    /* Search Fields:   -name  */

}
