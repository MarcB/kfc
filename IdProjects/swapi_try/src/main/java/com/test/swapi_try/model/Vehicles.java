package com.test.swapi_try.model;

import lombok.Data;

@Data
public class Vehicles {

    //*******************************************************************************
    //              Attributes:

    private String name ;                   // -- The name of this vehicle. The common name, such as "Sand Crawler"
                                            // or "Speeder bike".
    private String model ;                  // -- The model or official name of this vehicle. Such as "All-Terrain
                                            // Attack Transport".
    private String vehicle_class ;          // -- The class of this vehicle, such as "Wheeled" or "Repulsorcraft".
    private String manufacturer ;           // -- The manufacturer of this vehicle. Comma separated if more than one.
    private String length ;                 // -- The length of this vehicle in meters.
    private String cost_in_credits ;        // -- The cost of this vehicle new, in Galactic Credits.
    private String crew ;                   // -- The number of personnel needed to run or pilot this vehicle.
    private String passengers ;             // -- The number of non-essential people this vehicle can transport.
    private String max_atmosphering_speed ; // -- The maximum speed of this vehicle in the atmosphere.
    private String cargo_capacity ;         // -- The maximum number of kilograms that this vehicle can transport.
    private String consumables ;            // The maximum length of time that this vehicle can provide consumables
                                            // for its entire crew without having to resupply.


    //*******************************************************************************
    //              link part

    private String films[] ;                // -- An array of Film URL Resources that this vehicle has appeared in.
    private String pilots[] ;               // -- An array of People URL Resources that this vehicle has been piloted by.


    //*******************************************************************************
    //             info part

    private String url ;                    // -- the hypermedia URL of this resource.
    private String created ;                // -- the ISO 8601 date format of the time that this resource was created.
    private String edited ;                 // -- the ISO 8601 date format of the time that this resource was edited.
/*
    Search Fields:  -name
                    -model
*/
}
