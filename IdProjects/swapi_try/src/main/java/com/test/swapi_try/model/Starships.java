package com.test.swapi_try.model;

import lombok.Data;

@Data
public class Starships {
    //*******************************************************************************
    //              Attributes:

    private String name ;                   // -- The name of this starship. The common name, such as "Death Star".
    private String model ;                  //  -- The model or official name of this starship. Such as "T-65 X-wing"
                                            // or "DS-1 Orbital Battle Station".
    private String starship_class ;         //  -- The class of this starship, such as "Starfighter" or "Deep
                                            // Space Mobile Battlestation"
    private String manufacturer ;           //  -- The manufacturer of this starship. Comma separated if more
                                            // than one.
    private String cost_in_credits ;        //  -- The cost of this starship new, in galactic credits.
    private String length ;                 //  -- The length of this starship in meters.
    private String crew ;                   //  -- The number of personnel needed to run or pilot this starship.
    private String passengers ;             //  -- The number of non-essential people this starship can transport.
    private String max_atmosphering_speed ; //  -- The maximum speed of this starship in the atmosphere.
                                            // "N/A" if this starship is incapable of atmospheric flight.
    private String hyperdrive_rating ;      //  -- The class of this starships hyperdrive.
    private String MGLT ;                   //  -- The Maximum number of Megalights this starship can travel in a
                                            // standard hour. A "Megalight" is a standard unit of distance and has
                                            // never been defined before within the Star Wars universe. This figure
                                            // is only really useful for measuring the difference in speed of starships.
                                            // We can assume it is similar to AU, the distance between our Sun (Sol)
                                            // and Earth.
    private String cargo_capacity ;         //  -- The maximum number of kilograms that this starship can transport.
    private String consumables ;            // The maximum length of time that this starship can provide consumables
                                            // for its entire crew without having to resupply.

    //*******************************************************************************
    //              link part

    private String films[] ;                //  -- An array of Film URL Resources that this starship has appeared in.
    private String pilots[] ;               //  -- An array of People URL Resources that this starship has been piloted by.

    //*******************************************************************************
    //             info part

    private String url ;                    // -- the hypermedia URL of this resource.
    private String created ;                //  -- the ISO 8601 date format of the time that this resource was created.
    private String edited ;                 //  -- the ISO 8601 date format of the time that this resource was edited.

  /*  Search Fields:      -name
                          -model
*/

}
