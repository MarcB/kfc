package com.test.swapi_try.controller;

import com.test.swapi_try.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MainController {

    @Autowired
    private UserService userService;

    // Injectez (inject) via application.properties.
    @Value("${welcome.message}")
    private String message;

    @Value("${error.message}")
    private String errorMessage;

    //index >>  page home =====================================================
    @RequestMapping(value = { "/", "/index" }, method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("message", message);
        model.addAttribute("films", userService.findAllFilms());
        return "index";
    }

    @RequestMapping(value = { "comboFilms" }, method = RequestMethod.POST)
    public String testCombo(Model model) {
        model.addAttribute("message", message);
//        model.addAttribute("films", userService.findAllFilms());
        return "index";
    }


}
