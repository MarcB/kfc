package com.test.swapi_try;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SwapiTryApplication {

    public static void main(String[] args) {
        SpringApplication.run(SwapiTryApplication.class, args);
    }

}
