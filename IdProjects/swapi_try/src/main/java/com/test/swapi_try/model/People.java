package com.test.swapi_try.model;

import lombok.Data;

@Data
public class People {
    //*******************************************************************************
    //              Attributes:

    private String name ;       //-- The name of this person.
    private String birth_year ; // -- The birth year of the person, using the in-universe
                                // standard of BBY or ABY - Before the Battle of Yavin or
                                // After the Battle of Yavin.The Battle of Yavin is a
                                // battle that occurs at the end of Star Wars episode IV: A New Hope.
    private String eye_color ;  // -- The eye color of this person. Will be "unknown" if not known
                                // or "n/a" if the person does not have an eye.
    private String gender ;     // -- The gender of this person. Either "Male", "Female" or "unknown",
                                // "n/a" if the person does not have a gender.
    private String hair_color;  // -- The hair color of this person. Will be "unknown" if not known
                                // or "n/a" if the person does not have hair.
    private String height ;     // -- The height of the person in centimeters.
    private String mass ;       // -- The mass of the person in kilograms.
    private String skin_color ; // -- The skin color of this person.
    private String homeworld ;  // -- The URL of a planet resource, a planet that this person was
                                // born on or inhabits.

    //*******************************************************************************
    //              link part

    private String films[] ;    // -- An array of film resource URLs that this person has been in.
    private String species[] ;  // -- An array of species resource URLs that this person belongs to.
    private String starships[] ;// -- An array of starship resource URLs that this person has piloted.
    private String vehicles [] ;// -- An array of vehicle resource URLs that this person has piloted.

    //*******************************************************************************
    //             info part

    private String url ;        // -- the hypermedia URL of this resource.
    private String created ;    // -- the ISO 8601 date format of the time that this resource was created.
    private String edited ;     // -- the ISO 8601 date format of the time that this resource was edited.

   /* Search Fields:  name */

}
