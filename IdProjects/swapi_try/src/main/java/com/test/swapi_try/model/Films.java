package com.test.swapi_try.model;

import lombok.Data;
import uk.co.blackpepper.bowman.annotation.RemoteResource;
import uk.co.blackpepper.bowman.annotation.ResourceId;

import java.net.URI;
import java.util.Date;

@Data
@RemoteResource("/films")
public class Films {
    //*******************************************************************************
    //              Attributes:

    private String title ;          // -- The title of this film
    private Integer episode_id ;    // -- The episode number of this film.
    private String opening_crawl;   // -- The opening paragraphs at the beginning of this film.
    private String director ;       // -- The name of the director of this film.
    private String producer ;       // -- The name(s) of the producer(s) of this film. Comma separated.
    private Date release_date ;     // -- The ISO 8601 date format of film release at original creator country.


    //*******************************************************************************
    //              link part

    private String species[];       // -- An array of species resource URLs that are in this film.
    private String starships[];     // -- An array of starship resource URLs that are in this film.
    private String vehicles[];      // -- An array of vehicle resource URLs that are in this film.
    private String characters[];    // -- An array of people resource URLs that are in this film.
    private String planets[];       // -- An array of planet resource URLs that are in this film.

    //*******************************************************************************
    //             info part

    private String url ;            // -- the hypermedia URL of this resource.
    private String created ;        // -- the ISO 8601 date format of the time that this resource was created.
    private String edited ;         // -- the ISO 8601 date format of the time that this resource was edited.

   /* Search Fields:    title*/


}
