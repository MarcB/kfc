package com.test.swapi_try.model;

import lombok.Data;

@Data
public class Planets {

    //*******************************************************************************
    //              Attributes:

    private String name ;               // -- The name of this planet.
    private String diameter ;           //  -- The diameter of this planet in kilometers.
    private String rotation_period ;    //  -- The number of standard hours it takes for this planet
                                        // to complete a single rotation on its axis.
    private String orbital_period ;     //  -- The number of standard days it takes for this planet
                                        // to complete a single orbit of its local star.
    private String gravity ;            //  -- A number denoting the gravity of this planet, where "1"
                                        // is normal or 1 standard G. "2" is twice or 2 standard Gs. "0.5" is half
                                        // or 0.5 standard Gs.
    private String population ;         //  -- The average population of sentient beings inhabiting this planet.
    private String climate ;            //  -- The climate of this planet. Comma separated if diverse.
    private String terrain ;            //  -- The terrain of this planet. Comma separated if diverse.
    private String surface_water ;      //  -- The percentage of the planet surface that is naturally occurring
                                        // water or bodies of water.

    //*******************************************************************************
    //              link part

    private String residents[] ;        //  -- An array of People URL Resources that live on this planet.
    private String films[] ;            //  -- An array of Film URL Resources that this planet has appeared in.

    //*******************************************************************************
    //             info part

    private String url ;                //  -- the hypermedia URL of this resource.
    private String created ;            //  -- the ISO 8601 date format of the time that this resource was created.
    private String edited ;             //  -- the ISO 8601 date format of the time that this resource was edited.

  /*  Search Fields:     -name */


}
