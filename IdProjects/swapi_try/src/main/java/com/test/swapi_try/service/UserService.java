package com.test.swapi_try.service;

import com.test.swapi_try.model.Films;

import java.util.List;

public interface UserService {
    List<Films> findAllFilms();
}
