package modele;

import java.util.Hashtable;
import java.util.Vector;

public class Societe {
    //attribut de classe

    private String nom;

    //association Societe 1<-->* Client
    private Vector<Client> ens = new Vector<>();

    public Vector<Client> getEns() {
        return ens;
    }


    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom=(nom==null?"POLE":nom.toUpperCase());


    }

    protected  Societe(String nom) {
        this.setNom(nom);
    }
    protected  Societe() {
        this(null);
    }

    @Override
    public String toString() {
        String str="Societe{" +"nom='" + nom + '\n' ;
                for(Client cl : this.ens){
                  str= str+"\n\t" + cl.toString();
                }
                return str+'}';
    }

    //regle metier
    public void ajout(Client client){
        //controle1 existence du client
        if(client==null)return;
        //controle2 le client doit etre inconnu de la societe
        if(this.ens.contains(client)) return;
        //action:ajout du client
        this.ens.addElement((client));

        if(this.nom.equals(client.getNomSociete()))
        client.setNomSociete(this.nom);
    }

    public static void main(String[] args) {
        Client jp = new Client();
        Client jc = new Client("henry","dess","cimetiere");

        Societe grte=new Societe("grte");
        Societe pole=new Societe("POLE");

        System.out.println(jc);
        System.out.println(pole);
    }
}
