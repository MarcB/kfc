package modele;

import java.util.Objects;
import java.util.Vector;

public class Client {
    private String nom;//immuable
    private String prenom;//immuable
    private String nomSociete;//muable

    public String getNom() {
        return nom;
    }

    private void setNom(String nom) {
        this.nom = (nom==null?"NOM":nom.toUpperCase());
    }

    public String getPrenom() {
        return prenom;
    }

    private void setPrenom(String prenom) {
        this.prenom = (prenom==null?"PRENOM":prenom.toUpperCase());

    }

    public String getNomSociete() {
        return nomSociete;
    }

    public void setNomSociete(String nomSociete) {
        this.nomSociete = (nomSociete==null?"POLE":nomSociete.toUpperCase());
    }

    public Client(String nom, String prenom, String nomSociete) {
        this.setNom(nom);
        this.setPrenom(prenom);
        this.setNomSociete (nomSociete);
    }
    protected Client(){
        this(null,null,null);
    }
    //equals et hashcode


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return nom.equals(client.nom) &&
                prenom.equals(client.prenom) &&
                nomSociete.equals(client.nomSociete);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nom, prenom, nomSociete);
    }

    @Override
    public String toString() {
        return "client{" +
                "nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", nomSociete='" + nomSociete + '\'' +
                '}';
    }

    public static void main(String[] args) {
        Client chom =new Client();
        Client notchom =new Client();
        Client cl1 =new Client("enfayitte","melusine","goodyear");
        Client cl2 =new Client("enfayitte","melusine","goodyear");
        cl1=cl2;
        System.out.println(chom);

        // egalite des objets
        Vector<Client> ens=new Vector<>();
        ens.addElement(chom);
        ens.addElement(cl1);

        //if (ens.contains(chom)) System.out.println("Yes");
        if (ens.contains(cl2)) System.out.println("Yes"); else System.out.println("plooof");

        System.out.println(cl1.getClass());

    }
}
