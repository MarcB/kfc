    package modele;

    import java.util.Enumeration;
    import java.util.Hashtable;
    import java.util.Vector;

    public class EnsContacts {

        private Hashtable<String, Societe> enSoc=new Hashtable<>();

        public void  ajout(String nomClient,String prenom,String nomSoc){
            Client cl = new Client(nomClient,prenom,nomSoc);
            String cle=cl.getNomSociete();

            creerSociete(cle); //creation conditionnelle

            Societe soc=this.enSoc.get(cle);
            soc.ajout(cl);
        }

        private void creerSociete(String nom){
            String cle=(nom==null?"cora":nom.toUpperCase());
            if(this.enSoc.containsKey(cle)) return;

            this.enSoc.put(cle, new Societe(cle));
        }
        //afficher les clients

        public void afficherNomSociete(){
            for(String nomSoc:this.enSoc.keySet()){
                System.out.println(nomSoc);
            }
        }

        public void afficherNomSocieteBis(){
            Enumeration<String>enumcle=this.enSoc.keys();

            while (enumcle.hasMoreElements()){

                String cle=enumcle.nextElement();
                System.out.println(cle);

                Societe soc=this.enSoc.get(cle);
                Enumeration<Client>enumcl=soc.getEns().elements();

                while (enumcl.hasMoreElements()){

                    Client cl=enumcl.nextElement();
                    System.out.println("\t"+cl.toString());
                }
            }

        }




        public static void main(String[] args) {
            EnsContacts ag=new EnsContacts();
            ag.afficherNomSociete();
            ag.afficherNomSocieteBis();
        }
    }
