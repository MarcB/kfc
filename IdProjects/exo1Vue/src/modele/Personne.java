    package modele;
    import java.io.Serializable;
    import java.util.*;

    public class Personne implements  Comparable<Personne>, Serializable {
        private String nom;
        private String prenom;
        private String region;

        public String getNom() {
            return nom;
        }

        public void setNom(String nom) {
            this.nom=(nom==null?"gataor":nom.toUpperCase());
        }

        public String getPrenom() {
            return prenom;
        }

        public void setPrenom(String prenom) {
            this.prenom=(prenom==null?"navi":prenom.toUpperCase());
        }

        public String getRegion() {
            return region;
        }

        public void setRegion(String region) {
            this.region=(region==null?"pourri":region.toUpperCase());
        }

        public Personne(String nom, String prenom, String region) {
            this.setNom(nom);
            this.setPrenom (prenom);
            this.setRegion (region);
        }

        @Override
        public int compareTo(Personne o) {
            int aux =this.nom.compareTo(o.nom);
              if (aux!=0)
               return aux;
             this.prenom.compareTo(o.prenom);
            return 0;
        }

        @Override
        public String toString() {
            return  "nom='" + nom ;
        }

        @Override
        public boolean equals(Object o) {
            if(this==o) return true;
            if(!(o instanceof Personne)) return false;
            Personne personne =(Personne)o;


            return nom.equals(personne.nom);
        }


    }
