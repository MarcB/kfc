package Vue;
import modele.Personne;

import javax.swing.*;
import java.awt.event.*;

public class Jdialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField textFieldNom;
    private JTextField textFieldPrenom;
    private JTextField textFieldRegion;

    public Jdialog() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
        this.status=true;
        this.setVisible(false);
    }

    private void onCancel() {
        // add your code here if necessary
        this.status=false;
        this.setVisible(false);
    }

    Personne pers=null;
    boolean status=true;

    public boolean ouvriDlg(Personne pers, boolean isAjout){
        this.pers=pers;
       if (isAjout) {
           this.setTitle("ajout de personne");
           this.textFieldNom.setEditable(true);
           this.textFieldNom.setText("");
           this.textFieldPrenom.setText("");
           this.textFieldRegion.setText("Nord");
       }else{
           this.setTitle("modification de :"+pers.getNom());
           this.textFieldNom.setEditable(false);
           this.textFieldNom.setText(pers.getNom());
           this.textFieldPrenom.setText(pers.getPrenom());
           this.textFieldRegion.setText(pers.getRegion());
       }
       //this.status=true;
       this.setVisible(true);
       if(status){
           this.pers.setNom(this.textFieldNom.getText());
           this.pers.setPrenom(this.textFieldPrenom.getText());
           this.pers.setRegion(this.textFieldRegion.getText());
       }
       return status;
    }

    public static void main(String[] args) {
        Jdialog dialog = new Jdialog();
        dialog.pack();
        //dialog.setVisible(true);
        Personne p=new Personne(null,null,null);

        if(dialog.ouvriDlg(p,false)) {
            System.out.println(p);
            System.out.println(p.getRegion());
      //  }else if(dialog.ouvriDlg(p,true)){

        }
        System.exit(0);


    }
}
