package Vue;

import modele.Personne;

import java.io.*;
import java.util.*;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

public class MailingDlg extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JTextArea textAreaInfo;
    private JRadioButton nomRadioButton;
    private JRadioButton regionRadioButton;
    private JCheckBox ascCheckBox;
    private JButton apercuButton;
    private JButton fichierButton;
    private JButton serializationButton;


    private List<Personne> liste=new LinkedList<Personne>();
    private DefaultListModel<Personne> modele;

    public MailingDlg() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });


        apercuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                trier();
                textAreaInfo.setText("");
                String format = "";

                if (nomRadioButton.isSelected()) {
                    format = "Nom    : %s\nPrénom : %s\nRégion : %s\n";
                    for (Personne p : liste) {
                        textAreaInfo.append(String.format(format, p.getNom(), p.getPrenom(), p.getRegion()) + "\n");
                    }
                } else {
                    format = "Région : %s\nNom    : %s\nPrénom : %s\n";
                    for (Personne p : liste) {
                        textAreaInfo.append(String.format(format, p.getRegion(), p.getNom(), p.getPrenom()) + "\n");
                    }
                }
                textAreaInfo.select(0, 0);
            }
        });
        fichierButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                PrintWriter sortie = null;
                textAreaInfo.setText("");
                try {
                    JFileChooser fileChooser = new JFileChooser("D:/POP_java/");
                    int status = fileChooser.showSaveDialog(null);
                    if (status == JFileChooser.APPROVE_OPTION) {
                        File selectedFile = fileChooser.getSelectedFile();
                        sortie = new PrintWriter(new FileWriter(selectedFile));

                        trier();
                        String format = "";

                        if (nomRadioButton.isSelected()) {
                            format = "Nom    : %s\nPrénom : %s\nRégion : %s\n";
                            for (Personne p : liste) {
                                sortie.println(String.format(format, p.getNom(), p.getPrenom(), p.getRegion()) + "\n");
                            }
                        } else {
                            format = "Région : %s\nNom    : %s\nPrénom : %s\n";
                            for (Personne p : liste) {
                                sortie.println(String.format(format, p.getRegion(), p.getNom(), p.getPrenom()) + "\n");
                            }
                        }
                        sortie.close();
                    }

                } catch (Exception ioe) {
                    ioe.printStackTrace();
                }
            }
        });
        serializationButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    JFileChooser fileChooser = new JFileChooser("D:/POP_java/");
                    int status = fileChooser.showSaveDialog(null);
                    if (status == JFileChooser.APPROVE_OPTION) {
                        File selectedFile = fileChooser.getSelectedFile();
                        FileOutputStream f=new FileOutputStream(selectedFile);
                        ObjectOutputStream out = new ObjectOutputStream(f);

                        out.writeObject(modele);
                        out.close();
                    }
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }
        });
    }


    //gestion de la conversation
    public void ouvrir(DefaultListModel<Personne>modele){
        this.modele=modele;
        this.setVisible(true);
    }
    //
    private void onOK() {
        // add your code here
        this.setVisible(false);
    }

    private void onApercu(){
        trier();
        textAreaInfo.setText("");
        String format ="";

        if(nomRadioButton.isSelected()){
            format="Nom    : %s\nPrénom : %s\nRégion : %s\n";
            for(Personne p:liste){
                textAreaInfo.append(String.format(format,p.getNom(),p.getPrenom(),p.getRegion())+"\n");
            }
        }else{
            format="Région : %s\nNom    : %s\nPrénom : %s\n";
            for(Personne p:liste){
                textAreaInfo.append(String.format(format,p.getRegion(),p.getNom(),p.getPrenom())+"\n");
            }
        }
        textAreaInfo.select(0,0);
    }


    private void trier() {
        //classe auxilliaire;tri  différent de l'ordre naturel
        class ComparaisonRegionCroissante implements Comparator<Personne>{
            public int compare(Personne pa,Personne pb){
                int etat=pa.getRegion().compareTo((pb.getRegion()));
                if(etat!=0){
                    return etat;
                }
                return pa.getNom().compareTo(pb.getNom());
            }
        }
        class ComparaisonRegionDecroissante implements Comparator<Personne>{
            public int compare(Personne pa,Personne pb){
                int etat=pa.getRegion().compareTo((pb.getRegion()));
                if(etat!=0){
                    return -etat;
                }
                return pa.getNom().compareTo(pb.getNom());
            }
        }

            liste=new LinkedList<Personne>();
            Enumeration<Personne> iter = modele.elements();
        while(iter.hasMoreElements())

            {
                liste.add(iter.nextElement());
            }
        if(this.nomRadioButton.isSelected())

            {
                if (this.ascCheckBox.isSelected()) {
                    Collections.sort(liste);
                } else {
                    Collections.sort(liste, Collections.reverseOrder());
                }
            }else if(this.ascCheckBox.isSelected())

            {
                Collections.sort(liste, new ComparaisonRegionCroissante());
            }else

            {
                Collections.sort(liste, new ComparaisonRegionDecroissante());
            }
        }

    public static void main(String[] args) {
        MailingDlg dialog = new MailingDlg();
        dialog.pack();

        DefaultListModel<Personne> modele=new DefaultListModel<>();
        modele.addElement(new Personne("gator","nathalie","nord"));
        modele.addElement(new Personne("gator","navi","sud"));
        modele.addElement(new Personne("iator","jean-paul","est"));

        dialog.ouvrir(modele);
        System.exit(0);
    }
}
