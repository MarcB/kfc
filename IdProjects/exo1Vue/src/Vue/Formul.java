package Vue;

import modele.Personne;


import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.*;

public class Formul {
    private JPanel rootPanel;
    private JButton ajouterButton;
    private JButton modifierButton;
    private JButton mailingButton;
    private JList<Personne> listPersonnes;
    private JTextArea textAreaInfoPersonne;
    private MailingDlg mailingdlg=null;

    private DefaultListModel<Personne> modele =new DefaultListModel<Personne>();
    //private EnsPersonneListModel modele= new EnsPersonneListModel();

    private Jdialog ajoutDlg=null;


    public Formul() {
        initModele();
        try {
            JFileChooser fileChooser = new JFileChooser("D:/POP_java/");
            int status = fileChooser.showSaveDialog(null);
            if (status == JFileChooser.APPROVE_OPTION) {
                File selectedFile = fileChooser.getSelectedFile();
                FileInputStream f=new FileInputStream(selectedFile);
                ObjectInputStream out = new ObjectInputStream(f);

                this.modele=(DefaultListModel) out.readObject();
                out.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        this.listPersonnes.setModel((modele));
        ajouterButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(ajoutDlg==null){
                    ajoutDlg=new Jdialog();
                    ajoutDlg.pack();
                }
                Personne pers=new Personne(null,null,null);
                if(ajoutDlg.ouvriDlg(pers,true)){
                    if(!modele.contains(pers))
                    modele.addElement(pers);
                }
                textAreaInfoPersonne.setText("");
                modifierButton.setEnabled(false);
            }
        });
        listPersonnes.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                Personne pers =listPersonnes.getSelectedValue();
                textAreaInfoPersonne.setText("");
                textAreaInfoPersonne.setText("nom:"+pers.getNom());
                textAreaInfoPersonne.append("\nprenom:"+pers.getPrenom());
                textAreaInfoPersonne.append("\nregion:"+pers.getRegion());

                modifierButton.setEnabled(true);
            }
        });
        modifierButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

                    if(ajoutDlg==null){
                        ajoutDlg=new Jdialog();
                        ajoutDlg.pack();
                    }
                    Personne pers=listPersonnes.getSelectedValue();
                    Personne nouv =new Personne(pers.getNom(),pers.getPrenom(),pers.getRegion());
                    if(ajoutDlg.ouvriDlg(pers,true)){
                        if(!modele.contains(pers))
                            modele.removeElement(pers);
                          //  modele.addElement(pers);
                    }
                    textAreaInfoPersonne.setText("");
                    modifierButton.setEnabled(false);


            }
        });
        mailingButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(mailingdlg==null){
                    mailingdlg=new MailingDlg();
                    mailingdlg.pack();
                }
                mailingdlg.ouvrir(modele);
            }
        });
    }

    private void initModele(){
        this.modele=new DefaultListModel<>();/*
        modele.addElement(new Personne("gator","nathalie","nord"));
        modele.addElement(new Personne("gator","navi","sud"));
        modele.addElement(new Personne("iator","jean-paul","est"));
        modele.addElement(new Personne("ips","jean-luc","ouest"));*/
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Formul");
        frame.setContentPane(new Formul().rootPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }



}
