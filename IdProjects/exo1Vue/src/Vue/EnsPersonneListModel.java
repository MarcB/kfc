package Vue;

import modele.Personne;
import javax.swing.*;
import java.util.*;

public class EnsPersonneListModel extends AbstractListModel<Personne> {
    private List<Personne> ensPersonne= new ArrayList<>();


   /* @Override
    public void addElement(Personne element){
        if(! this.contains(element));
    }*/
    @Override
    public int getSize() {
        return ensPersonne.size();
    }

    @Override
    public Personne getElementAt(int index) {
        return ensPersonne.get(index);
    }
    public void addElement(Personne pers){
        if(pers==null)return;
        if(ensPersonne.contains(pers)) return;
        ensPersonne.add(pers);

    }

}
