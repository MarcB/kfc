package com.pop.rest_cinema_client.service;

import com.pop.rest_cinema_client.model.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class UserServiceImpl implements UserService  {
    String URL_SERVER = "http://localhost:8080/";

    @Override
    public Acteur[] findAllActeur() {
        RestTemplate restTemplate = new RestTemplate();
        Acteur[] list=restTemplate.getForObject(URL_SERVER+"acteur", Acteur[].class);
       return list;
    }

    @Override
    public Client[] findAllClient() {
        RestTemplate restTemplate = new RestTemplate();
        Client[] list=restTemplate.getForObject(URL_SERVER+"client", Client[].class);
        return list;
    }

    @Override
    public Client findClientById(Long nclient) {

        RestTemplate restTemplate=new RestTemplate();
        return restTemplate.getForObject(URL_SERVER+"client/"+nclient,Client.class);
    }

    @Override
    public Emprunt[] findAllEmprunt() {
        RestTemplate restTemplate = new RestTemplate();
        Emprunt[] list=restTemplate.getForObject(URL_SERVER+"emprunt", Emprunt[].class);
        return list;
    }

    @Override
    public Genre[] findAllGenre() {
        RestTemplate restTemplate = new RestTemplate();
        Genre[] list=restTemplate.getForObject(URL_SERVER+"genre", Genre[].class);
        return list;
    }

    @Override
    public Film[] findAllFilm() {
        RestTemplate restTemplate = new RestTemplate();
        Film[] list=restTemplate.getForObject(URL_SERVER+"film", Film[].class);
        return list;
    }

    @Override
    public List<Genre> findGenreByNature(String nature) {
        RestTemplate restTemplate=new RestTemplate();
        return (List<Genre>) restTemplate.getForObject(URL_SERVER+"genre/"+nature,Genre.class);
    }

    @Override
    public List<Acteur> findActeurById(Long nacteur) {
        RestTemplate restTemplate=new RestTemplate();
        return (List<Acteur>) restTemplate.getForObject(URL_SERVER+"acteur/"+nacteur,Acteur.class);
    }

    @Override
    public void saveClient(Client client) {

    }

    @Override
    public Client findClientByID(long id) {
        RestTemplate restTemplate=new RestTemplate();
        return restTemplate.getForObject(URL_SERVER+"client/"+id,Client.class);
    }

    @Override
    public void deleteClient(Client client) {

    }
}
