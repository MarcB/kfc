package com.pop.rest_cinema_client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestCinemaClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestCinemaClientApplication.class, args);
    }

}
