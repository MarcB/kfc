package com.pop.rest_cinema_client.service;

import com.pop.rest_cinema_client.model.*;

import java.util.List;

public interface UserService {
    Acteur[] findAllActeur();

    Client[] findAllClient();

    Client findClientById(Long nclient);

    Emprunt[] findAllEmprunt();

    Genre[] findAllGenre();

    Film[] findAllFilm();

    List<Genre> findGenreByNature(String nature);

    List<Acteur> findActeurById(Long nacteur);

    void saveClient(Client client);

    Client findClientByID(long id);

    void deleteClient(Client client);
}
