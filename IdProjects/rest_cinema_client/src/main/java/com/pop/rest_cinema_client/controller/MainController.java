package com.pop.rest_cinema_client.controller;

import com.pop.rest_cinema_client.model.Client;
import com.pop.rest_cinema_client.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import com.pop.rest_cinema_client.service.UserService;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@Controller
    public class MainController {

    @Autowired
        private UserService userService;

    @RequestMapping(value = { "/", "/index" }, method = RequestMethod.GET)
    public String index(Model model) {
        return "index";
    }

    //list des acteurs ========================================================
    @RequestMapping(value = { "/acteurList" }, method = RequestMethod.GET)
    public String acteurList(Model model) {
        model.addAttribute("acteurs", userService.findAllActeur());
        return "acteurList";
    }

    //list des clients =========================================================
    @RequestMapping(value = { "/clientList" }, method = RequestMethod.GET)
    public String clientList(Model model) {
        model.addAttribute("clients", userService.findAllClient());
        return "clientList";
    }
    //list des emprunts by client(id) ==========================================
    @PostMapping("/voirclient")
    public String emprunts(@RequestParam("clientchoisi")Long nclient, Model model){
        model.addAttribute("empruntsclient", userService.findClientById(nclient));
        //retour list d emprunt par client
        return "empruntListClient";
    }

    //list des emprunts ========================================================
    @RequestMapping(value = { "/empruntList" }, method = RequestMethod.GET)
    public String empruntList(Model model) {
        model.addAttribute("emprunts", userService.findAllEmprunt());
        return "empruntList";
    }

    //list des films + combo by genre ==========================================
    @RequestMapping(value = {"/filmList"}, method = RequestMethod.GET)
    public String selectionGenre(Model model) {
        model.addAttribute("genres", userService.findAllGenre());
        model.addAttribute("films", userService.findAllFilm());
        return "filmList";
    }

    //list des films by genre(nature) ==========================================
    @PostMapping("/voirfilm" )
    public String films(@RequestParam("genrechoisi")String nature, Model model){
        model.addAttribute("ensfilmchoisi", userService.findGenreByNature(nature));
        return "filmListGenre";
    }
    //list des films by acteur(id) =============================================
    @PostMapping("/voiracteur" )
    public String films(@RequestParam("acteurchoisi")Long nacteur, Model model){
        model.addAttribute("filmsActeur", userService.findActeurById(nacteur));
        return "filmListActeur";
    }



    //**************************************************************************
    //***************            BOOTSTRAP VERSION            ******************
    //**************************************************************************


    //client creation ==========================================================
    @RequestMapping(value = { "/clientCrea" }, method = RequestMethod.GET)
    public String clientCrea(Model model) {
        model.addAttribute("clients", userService.findAllClient());
        return "clientCrea";
    }

    //bouton lancement page creatio user
    @GetMapping("/signup")
    public String showSignUpForm(Client client) {
        return "add-client";
    }

    //ajout de client
    @PostMapping("/addclient")
    public String addClient(@Valid Client client, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "add-client";
        }
        userService.saveClient(client);
        model.addAttribute("clients", userService.findAllClient());
        return "clientCrea";
    }
    // edition de l'info client
    @GetMapping("/edit/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model) {
        Client client = (Client) userService.findClientByID(id)/*.orElseThrow(() -> new IllegalArgumentException("Invalid client Id:" + id))*/;

        model.addAttribute("client", client);
        return "update-client";
    }

    //update du client
    @PostMapping("/update/{id}")
    public String updateClient(@PathVariable("id") long id, @Valid Client client, BindingResult result, Model model) {
        //si error renvoie l update
        if (result.hasErrors()) {
            client.setNclient(id);
            return "update-client";
        }//sinon
        //re-set du N°client pour save
        client.setNclient(id);
        userService.saveClient(client);
        model.addAttribute("clients", userService.findAllClient());
        return "clientCrea";
    }

    //delete du client selon id
    @GetMapping("/delete/{id}")
    public String deleteClient(@PathVariable("id") long id, Model model) {
        Client client = (Client) userService.findClientByID(id)/*.orElseThrow(() -> new IllegalArgumentException("Invalid client Id:" + id))*/;
        userService.deleteClient(client);
        model.addAttribute("clients", userService.findAllClient());
        return "clientCrea";
    }

}
