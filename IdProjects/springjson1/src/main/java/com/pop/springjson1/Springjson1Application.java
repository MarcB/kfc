package com.pop.springjson1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springjson1Application {

    public static void main(String[] args) {
        SpringApplication.run(Springjson1Application.class, args);
    }

}
