package com.pop.springjson1.model;





import java.util.Objects;

public class Dept {

    private int deptno;
    private String dname;
    private String loc;

    //constructeur vide
    public Dept(){

    }
    //contructeur
    public Dept(Integer deptno, String dname, String loc){
        this.deptno=deptno;
        this.dname=dname;
        this.loc=loc;
    }
    //getter setter
    public int getDeptno() {
        return deptno;
    }

    public void setDeptno(int deptno) {
        this.deptno = deptno;
    }

    public String getDname() {
        return dname;
    }

    public String getLoc() {
        return loc;
    }

    //string methode
    @Override
    public String toString() {
        return "Department{" +
                "deptno=" + deptno +
                ", dname='" + dname + '\'' +
                ", loc='" + loc + '\'' +
                '}';
    }

    //
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Dept)) return false;
        Dept that = (Dept) o;
        return deptno == that.deptno &&
                dname.equals(that.dname) &&
                loc.equals(that.loc);
    }

    //
    @Override
    public int hashCode() {
        return Objects.hash(deptno, dname, loc);
    }
}
