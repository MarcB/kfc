package com.pop.springjson1.model;



public class Employee {
    private int empno;
    private String ename;
    private String job;
    private int mgr;
    private Double sal;
    private int comm;
    private int deptno;
    private String dname;


    //constructeur vide
    public Employee(){

    }
    //constructeur employe
    public Employee(String ename, String job, Double sal, int deptno){
        this.ename=ename;
        this.job=job;
        this.sal=sal;
        this.deptno=deptno;
    }
    public Employee(String ename, String job, Double sal, String dname){
        this.ename=ename;
        this.job=job;
        this.sal=sal;
        this.dname=dname;
    }

    // getter setter
    public int getEmpno() {
        return empno;
    }

    public void setEmpno(int empno) {
        this.empno = empno;
    }

    public String getEname() {
        return ename;
    }

    public void setEname(String ename) {
        this.ename = ename;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public int getMgr() {
        return mgr;
    }

    public void setMgr(int mgr) {
        this.mgr = mgr;
    }

    public Double getSal() {
        return sal;
    }

    public void setSal(Double sal) {
        this.sal = sal;
    }

    public int getComm() {
        return comm;
    }

    public void setComm(int comm) {
        this.comm = comm;
    }

    public int getDeptno() {
        return deptno;
    }

    public void setDeptno(int deptno) {
        this.deptno = deptno;
    }

    public String getDname() {
        return dname;
    }

    public void setDname(String dname) {
        this.dname = dname;
    }
}
