package popschool.sportspringjpa.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import popschool.sportspringjpa.SportspringjpaApplication;
import popschool.sportspringjpa.dao.AffectationDAO;
import popschool.sportspringjpa.dao.LeagueDAO;
import popschool.sportspringjpa.dao.PlayerDAO;
import popschool.sportspringjpa.dao.TeamDAO;
import popschool.sportspringjpa.modele.Affectation;
import popschool.sportspringjpa.modele.League;
import popschool.sportspringjpa.modele.Player;
import popschool.sportspringjpa.modele.Team;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;


@Component
    public class Test implements CommandLineRunner {

        private static final Logger log = LoggerFactory.getLogger(SportspringjpaApplication.class);

    @Autowired
    private LeagueDAO leag;

    @Autowired
    private AffectationDAO affec ;

    @Autowired
    private PlayerDAO play;
    @Autowired
    private TeamDAO tea;



    private void information(Player p, League l, Team t){
        log.info(p.toString());
        log.info(l.toString());
        log.info(t.toString());
    }

    @Override
    public void run(String... args) throws Exception {

       /*

        log.info("-------------Debut list all leagues---------------");
        for(League l : leag.findAll()){
            log.info(l.toString());
        }
        log.info("--------------fin list all leagues---------------");


        log.info("-------------Debut list all TEAMs---------------");
        for(Team t: tea.findAllByOrderByLeagueByLeagueidNameDescNameDesc()){
            log.info(t.toString());
        }
        log.info("--------------fin list all TEAMs---------------");


        //selecteur sur un nom de league
        log.info("-------------Debut list all TEAMs for mountain---------------");
        for(Team t: tea.findNameByLeagueByLeagueidName("Mountain")){
            log.info(t.toString());
        }
        log.info("--------------fin list all TEAMs for mountain---------------");


        //selecteur sur un nom de league
        log.info("-------------Debut list all player by deer team---------------");
        for(Affectation a: affec.findByTeamByTeamidName( "Deer")){
            log.info(a.toString());
        }

        //Recherche d'un joueur et modification de son salaire
        Optional<Player>pl=play.findById((int) 1L);
        Player p=pl.get();
        log.info("-----Player info avant :");
        log.info(p.toString());
        //passage par des valeurs intermédiaire pour le calcul du pourcentage et du pb de bigdecimal
        //(i valeur de base du salaire j valeur du set)
        BigDecimal i=p.getSalary();
        BigDecimal j=(i.multiply(BigDecimal.valueOf(5)).divide(BigDecimal.valueOf(100))).add(i);
        log.info("-----valeur j :");
        log.info(j.toString());
        p.setSalary(j);
        log.info("-----Player info apres :");
        log.info(p.toString());
        play.save(p);
        log.info("-----Player info saved ");

        //selecteur sur un nom de league
        log.info("-------------Debut list all player by null team---------------");
        for(Affectation a: affec.findByTeamByTeamidName( null)){
            log.info(a.toString());
        }


        */

    }


}
