package popschool.sportspringjpa.modele;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name="league")
public class League {
    private Integer leagueid;
    private String name;
    private String sport;
    private Collection<Team> teamsByLeagueid;

    @Id
    @Column(name = "leagueid", nullable = false)
    public Integer getLeagueid() {
        return leagueid;
    }

    public void setLeagueid(Integer leagueid) {
        this.leagueid = leagueid;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 25)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "sport", nullable = false, length = 20)
    public String getSport() {
        return sport;
    }

    public void setSport(String sport) {
        this.sport = sport;
    }

    public League() {    }

    public League(  Integer leagueid, String name, String sport, Collection<Team> teamsByLeagueid) {
        this.leagueid=leagueid;
        this.name = name;
        this.sport = sport;
        this.teamsByLeagueid = teamsByLeagueid;
    }
    public League(  Integer leagueid, String name, String sport) {
        this.leagueid=leagueid;
        this.name = name;
        this.sport = sport;

    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        League league = (League) o;
        return leagueid == league.leagueid &&
                Objects.equals(name, league.name) &&
                Objects.equals(sport, league.sport);
    }

    @Override
    public int hashCode() {
        return Objects.hash(leagueid, name, sport);
    }

    @OneToMany(mappedBy = "leagueByLeagueid")
    public Collection<Team> getTeamsByLeagueid() {
        return teamsByLeagueid;
    }

    public void setTeamsByLeagueid(Collection<Team> teamsByLeagueid) {
        this.teamsByLeagueid = teamsByLeagueid;
    }

    @Override
    public String toString() {
        return "League{" +
                "leagueid=" + leagueid +
                ", name='" + name + '\'' +
                ", sport='" + sport + '\'' +
             //   ", teamsByLeagueid=" + teamsByLeagueid +
                '}';
    }
}
