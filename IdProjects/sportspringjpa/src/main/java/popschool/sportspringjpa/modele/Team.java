package popschool.sportspringjpa.modele;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "team", schema = "mySport", catalog = "")
public class Team {
    private Integer teamid;
    private String name;
    private String city;
    private Integer leagueid;
    private Collection<Affectation> affectationsByTeamid;

    private League leagueByLeagueid;

    @Id
    @Column(name = "teamid", nullable = false)
    public int getTeamid() {
        return teamid;
    }

    public void setTeamid(Integer teamid) {
        this.teamid = teamid;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 25)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "city", nullable = false, length = 25)
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

/*
    public Integer getLeagueid() {
        return leagueid;
    }

    public void setLeagueid(Integer leagueid) {
        this.leagueid = leagueid;
    }
*/


    public Team() {

    }

    public Team(int teamid, String name, String city, Integer leagueid, Collection<Affectation> affectationsByTeamid, League leagueByLeagueid) {

        this.name = name;
        this.city = city;
        this.leagueid = leagueid;
        this.affectationsByTeamid = affectationsByTeamid;
        this.leagueByLeagueid = leagueByLeagueid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Team that = (Team) o;
        return teamid == that.teamid &&
                Objects.equals(name, that.name) &&
                Objects.equals(city, that.city) &&
                Objects.equals(leagueid, that.leagueid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(teamid, name, city, leagueid);
    }

    @OneToMany(mappedBy = "teamByTeamid")
    public Collection<Affectation> getAffectationsByTeamid() {
        return affectationsByTeamid;
    }

    public void setAffectationsByTeamid(Collection<Affectation> affectationsByTeamid) {
        this.affectationsByTeamid = affectationsByTeamid;
    }

    @ManyToOne
    @JoinColumn(name = "leagueid", referencedColumnName = "leagueid")
    public League getLeagueByLeagueid() {
        return leagueByLeagueid;
    }

    public void setLeagueByLeagueid(League leagueByLeagueid) {
        this.leagueByLeagueid = leagueByLeagueid;
    }

    @Override
    public String toString() {
        return "TeamEntity{"
                + "teamid=" + teamid
                +", name='" + name + '\''
                +  ", city='" + city + '\''
                + ", leagueid=" + leagueid
    //            +", affectationsByTeamid=" + affectationsByTeamid
             + ", leagueByLeagueid=" + leagueByLeagueid
                +'}';
    }
}
