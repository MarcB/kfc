package popschool.sportspringjpa.modele;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "player")
public class Player {
    private Integer playerId;
    private String name;
    private String position;
    private BigDecimal salary;
    private Collection<Affectation> affectationsByPlayerId;

    @Id
    @Column(name = "playerId", nullable = false)
    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 20)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "position", nullable = false, length = 20)
    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Basic
    @Column(name = "salary", nullable = false, precision = 2)
    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    public Player () {}

    public Player(Integer playerId ,String name, String position, BigDecimal salary, Collection<Affectation> affectationsByPlayerId) {
        this.playerId=playerId;
        this.name = name;
        this.position = position;
        this.salary = salary;
        this.affectationsByPlayerId = affectationsByPlayerId;

    }

    public Player(Integer playerId ,String name, String position, BigDecimal salary) {
        this.playerId=playerId;
        this.name = name;
        this.position = position;
        this.salary = salary;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return playerId == player.playerId &&
                Objects.equals(name, player.name) &&
                Objects.equals(position, player.position) &&
                Objects.equals(salary, player.salary);
    }

    @Override
    public int hashCode() {
        return Objects.hash(playerId, name, position, salary);
    }

    @OneToMany(mappedBy = "playerByPlayerid")
    public Collection<Affectation> getAffectationsByPlayerId() {
        return affectationsByPlayerId;
    }

    public void setAffectationsByPlayerId(Collection<Affectation> affectationsByPlayerId) {
        this.affectationsByPlayerId = affectationsByPlayerId;
    }

    @Override
    public String toString() {
        return "Player "
               + "N°" + playerId
                + " " + name
                +" | position= " + position
                + " | salary=" + salary
             //   + ", affectationsByPlayerId=" + affectationsByPlayerId
                ;
    }


}
