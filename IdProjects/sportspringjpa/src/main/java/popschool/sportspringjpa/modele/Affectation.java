package popschool.sportspringjpa.modele;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name="affectation")
public class Affectation {
    private Integer id;
    private Integer playerid;
    private Integer teamid;
    private Player playerByPlayerid;
    private Team teamByTeamid;

    @Id
    @Column(name = "id", nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
/*
    @Basic
    @Column(name = "playerid", nullable = false)
    public Integer getPlayerid() {
        return playerid;
    }

    public void setPlayerid(Integer playerid) {
        this.playerid = playerid;
    }

    @Basic
    @Column(name = "teamid", nullable = false)
    public Integer getTeamid() {
        return teamid;
    }

    public void setTeamid(int teamid) {
        this.teamid = teamid;
    }
*/
    public Affectation(Integer id,Integer playerid, Integer teamid, Player playerByPlayerid, Team teamByTeamid) {
        this.id=id;
        this.playerid = playerid;
        this.teamid = teamid;
        this.playerByPlayerid = playerByPlayerid;
        this.teamByTeamid = teamByTeamid;
    }

    public Affectation(Integer id,Integer playerid, Integer teamid, Player playerByPlayerid) {
        this.id=id;
        this.playerid = playerid;
        this.teamid = teamid;
        this.playerByPlayerid = playerByPlayerid;
    }

    public Affectation() {    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Affectation that = (Affectation) o;
        return id == that.id &&
                playerid == that.playerid &&
                teamid == that.teamid;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, playerid, teamid);
    }

    @ManyToOne
    @JoinColumn(name = "playerid", referencedColumnName = "playerId", nullable = false)
    public Player getPlayerByPlayerid() {
        return playerByPlayerid;
    }

    public void setPlayerByPlayerid(Player playerByPlayerid) {
        this.playerByPlayerid = playerByPlayerid;
    }

    @ManyToOne
    @JoinColumn(name = "teamid", referencedColumnName = "teamid", nullable = false)
    public Team getTeamByTeamid() {
        return teamByTeamid;
    }

    public void setTeamByTeamid(Team teamByTeamid) {
        this.teamByTeamid = teamByTeamid;
    }

    @Override
    public String toString() {
        return "Affectation{" +/*
                "id=" + id +
                ", playerid=" + playerid +
                ", teamid=" + teamid +*/
                ", playerByPlayerid=" + playerByPlayerid +
                ", teamByTeamid=" + teamByTeamid +

                '}';
    }
}
