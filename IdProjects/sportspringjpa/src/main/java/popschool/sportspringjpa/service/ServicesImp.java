package popschool.sportspringjpa.service;

import org.springframework.beans.factory.annotation.Autowired;
import popschool.sportspringjpa.dao.AffectationDAO;
import popschool.sportspringjpa.dao.LeagueDAO;
import popschool.sportspringjpa.dao.PlayerDAO;
import popschool.sportspringjpa.dao.TeamDAO;
import popschool.sportspringjpa.modele.Affectation;
import popschool.sportspringjpa.modele.League;
import popschool.sportspringjpa.modele.Player;
import popschool.sportspringjpa.modele.Team;

import java.math.BigDecimal;
import java.util.List;


public class ServicesImp implements Services {

    @Autowired
    private LeagueDAO leag;
    @Autowired
    private AffectationDAO affec ;
    @Autowired
    private PlayerDAO play;
    @Autowired
    private TeamDAO tea;

    @Override
    public  List<League> allLeague() {
        Iterable<League> allLeague;
        return (List<League>) leag.findAll();
        // Repository.findByNull().forEach(System.out::println)
    }

    @Override
    public League ajoutLeague(int leagueid, String lname, String sport) {
        return null;
    }

    @Override
    public Team ajoutTeam(int teamid, String tname, String city) {
        return null;
    }

    @Override
    public Team teamToLeague(String tname, String lname) {
        return null;
    }

    @Override
    public List<Player> allPlayer() {
        return null;
    }

    @Override
    public Player ajoutPlayer(int playerid, String position, BigDecimal salary) {
        return null;
    }

    @Override
    public Affectation playerToLeague(String pname, String tname) {
        return null;
    }


}
