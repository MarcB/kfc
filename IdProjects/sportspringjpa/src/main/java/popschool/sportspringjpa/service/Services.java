package popschool.sportspringjpa.service;

import popschool.sportspringjpa.modele.Affectation;
import popschool.sportspringjpa.modele.League;
import popschool.sportspringjpa.modele.Player;
import popschool.sportspringjpa.modele.Team;

import java.math.BigDecimal;
import java.util.List;

public interface Services {
   List<League>allLeague();
   League ajoutLeague(int leagueid,String lname,String sport);
   Team ajoutTeam(int teamid,String tname,String city);
   Team teamToLeague(String tname,String lname);
   List<Player>allPlayer();
   Player ajoutPlayer(int playerid, String position, BigDecimal salary);
   Affectation playerToLeague(String pname,String tname);

    //List<League>findByLeagueid(int i);
  //  List<Team>findByTeamid(int i);
  //  List<Player>findAll();




}
