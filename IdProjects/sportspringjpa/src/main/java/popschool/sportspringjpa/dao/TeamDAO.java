package popschool.sportspringjpa.dao;

import org.springframework.data.repository.CrudRepository;
import popschool.sportspringjpa.modele.League;
import popschool.sportspringjpa.modele.Team;

import java.util.List;

public interface TeamDAO extends CrudRepository<Team, Integer> {
    //selection des equipes par ordre descendant selon la league et ensuite par nom
    List<Team> findAllByOrderByLeagueByLeagueidNameDescNameDesc();
    //selection des equipes selon un nom de league
    List<Team> findNameByLeagueByLeagueidName(String LeagueByLeagueidName );


}
