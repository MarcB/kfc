package popschool.sportspringjpa.dao;

import org.springframework.data.repository.CrudRepository;
import popschool.sportspringjpa.modele.Affectation;
import popschool.sportspringjpa.modele.League;

import java.util.List;

public interface AffectationDAO extends CrudRepository<Affectation, Integer> {
    //list
    List<Affectation>findByTeamByTeamidName(String TeamByTeamidName);

}
