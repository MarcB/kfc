package popschool.sportspringjpa.dao;

import org.springframework.data.repository.CrudRepository;
import popschool.sportspringjpa.modele.League;


public interface LeagueDAO extends CrudRepository<League, Integer> {


}
