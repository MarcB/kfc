package popschool.sportspringjpa.dao;

import org.springframework.data.repository.CrudRepository;
import popschool.sportspringjpa.modele.Player;
import popschool.sportspringjpa.modele.Team;

import java.util.List;

public interface PlayerDAO extends CrudRepository<Player,Integer> {
    //List<Player>

    List<Player>findByName(String name);



}
