package popschool.sportspringjpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Service;
import popschool.sportspringjpa.modele.League;
import popschool.sportspringjpa.service.Services;

@SpringBootApplication
public class SportspringjpaApplication {

    public static void main(String[] args) {
        SpringApplication.run(SportspringjpaApplication.class, args);

    }

}
