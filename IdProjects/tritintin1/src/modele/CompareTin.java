package modele;
import java.util.*;

import java.util.Comparator;





    public class CompareTin implements Comparator<Album> {

        @Override
        public int compare(Album un , Album deux){
            int aux=un.getNom().compareTo((deux.getNom()));
            if(aux!=0)
                return aux;
            else
                return (un.getYear()).compareTo(deux.getYear());
        }
    }




/*
        1. List<Album> listeAlbumParTitre()
        2. List<Album> listeAlbumParTitreDecroissant()
        3. List<Album> listeAlbumParAnnee()
                - tri croissant par année et titre (utilisation d’une classe locale interne)
        4. List<Album> listeAlbumParAnneeDecroissant()
                - tri décroissant par année et titre (utilisation d’une classe anonyme)


        */