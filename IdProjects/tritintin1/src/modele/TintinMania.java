package modele;
import java.sql.SQLOutput;
import java.util.*;


public class TintinMania {

    private List<Album> mesTintins = new ArrayList<Album>();

    public List<Album> getMesTintins() {
        return mesTintins;
    }
    public int compareName(Album a,Album b){
       return a.getNom().compareTo(b.getNom());
    }

    public int compareYear(Album a,Album b){
      return a.getYear().compareTo(b.getYear());
    }

    public  void chargerAlbums(){
        Album a1 = new Album("Tintin au pays des Soviets", 1930);
        Album a2 = new Album("Tintin au Congo", 1931);
        Album a3 = new Album("Tintin en Amérique", 1932);
        Album a4 = new Album("Les Cigares du pharaon", 1934);
        Album a5 = new Album("Le Lotus bleu", 1936);
        Album a6 = new Album("L'Oreille cassée", 1937);
        Album a7 = new Album("L'Ile noire", 1938);
        Album a8 = new Album("Le Sceptre d'Ottokar", 1939);
        Album a9 = new Album("Le Crabe aux pinces d'or", 1941);
        Album a10 = new Album("L'Étoile mystérieuse", 1942);
        Album a11= new Album("Le Secret de la Licorne", 1943);
        Album a12= new Album("Le Trésor de Rackham le Rouge", 1944);
        Album a13= new Album("Les 7 boules de cristal", 1948);
        Album a14= new Album("Le Temple du soleil", 1950);

        mesTintins.add(a1);
        mesTintins.add(a2);
        mesTintins.add(a3);
        mesTintins.add(a4);
        mesTintins.add(a5);
        mesTintins.add(a6);
        mesTintins.add(a7);
        mesTintins.add(a8);
        mesTintins.add(a9);
        mesTintins.add(a10);
        mesTintins.add(a11);
        mesTintins.add(a12);
        mesTintins.add(a13);
        mesTintins.add(a14);

    }

    @Override
    public String toString() {
        return "TintinMania{" +
                "mesTintins=" + mesTintins +
                '}';
    }
    public void afficherCroissant() {
        List<Album> aux = new LinkedList<>(this.mesTintins);
        Collections.sort(aux);
        System.out.println("Croissant");
        for (Album p : aux) {
            System.out.print(p+"\n\t");
        }
    }

    public void afficherDeroissant() {
        List<Album> aux = new LinkedList<>(this.mesTintins);
        Collections.sort(aux,Collections.reverseOrder());
        System.out.println("Decroissant");
        for (Album p : aux) {
            System.out.print(p+"\n\t");
        }

    }
    public void aficheYearDecroissant(){
        List<Album> aux = new LinkedList<>(this.mesTintins);
        Collections.sort(aux, new CompareTin());
        System.out.println("Décroissant");
        for (Album p : aux){
            System.out.println(p);
        }
    }

    public void afficherYear() {
        //----------- C L A S S E  L O C A L -----------------
        class AlbumComparator implements Comparator<Album> {
            @Override
            public int compare(Album un, Album deux) {
                int aux = un.getYear().compareTo((deux.getYear()));
                if (aux != 0)
                    return aux;                                                //pour decroissant on met -
                else
                    return un.getNom().compareTo(deux.getNom());   //pour decroissant on met -
            }

        }
        List<Album> aux = new LinkedList<>(this.mesTintins);
        Collections.sort(aux, new AlbumComparator());
        System.out.println("Année");
        for (Album p : aux){
            System.out.println(p+"\t");
        }
    }




    public static void main(String[] args) {
        TintinMania uu= new TintinMania();
        uu.chargerAlbums();
        uu.afficherCroissant();
       // System.out.println("rrrrrrrrrrr"+uu);
        //uu.afficherCroissant();
        uu.afficherDeroissant();
        uu.afficherYear();
        uu.aficheYearDecroissant();
    }



}
