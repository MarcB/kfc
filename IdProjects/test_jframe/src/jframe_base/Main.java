package jframe_base;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Main  {

  private static  JTextField tf;
  private static   JLabel l;
  private static   JButton b;

    Main() {
        tf = new JTextField();
        tf.setBounds(50, 50, 150, 20);
        l = new JLabel();
        l.setBounds(50, 100, 250, 20);
        b = new JButton("Find IP");
        b.setBounds(50, 150, 95, 30);


    }






    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.add(b);
        frame.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                b.setText("Pressed");
                
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                System.exit(1);
            }
        });


        frame.addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                System.out.println("Moved");
            }
        });

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setSize(100, 100);
                frame.setVisible(true);
            }
        });

    }
}