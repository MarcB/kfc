package control;

import dto.Catalogue;
import dto.ClientDTO;
import dto.Panier;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

import static dao.EcommerceDAO.*;

@WebServlet("/process")
public class EcommerceServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRquest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRquest(request, response);
    }

    //navigation
    private void forward(String url, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd = request.getRequestDispatcher(url);
        rd.forward(request, response);
    }

    //routage
    private void processRquest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            //recupertaion de l'action
            String action = request.getParameter("action");

            //execution du traitement
            if (action.equals("init")) {
                doInit(request, response);
            } else if (action.equals("authenticate")) {
                doAuthenticate(request, response);
            } else if (action.equals("addItems")) {
                doAddItems(request, response);
            } else if (action.equals("validateCommande")) {
                doValidateCommande(request, response);
            } else if (action.equals("logout")) {
                doLogout(request, response);
            } else if (action.equals("showCatalogue")) {
                doShowCatalogue(request, response);
            } else if (action.equals("showDetails")) {
                doShowDetails(request, response);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void doShowDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String url = "pages/showDetails.jsp";
        forward(url, request, response);
    }

    private void doShowCatalogue(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String url = "pages/showCatalogue.jsp";
        forward(url, request, response);
    }

    private void doLogout(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        if (session != null) {
            ClientDTO client = (ClientDTO) session.getAttribute("client");
            if (client != null) {
                session.invalidate();
            }
        }
        session = request.getSession(true);
        response.sendRedirect("index.jsp");
    }

    private void doValidateCommande(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Panier panier = (Panier) request.getSession().getAttribute("anOrder");
        Catalogue catalogue = (Catalogue) getServletContext().getAttribute("catalogue");

        System.out.println("panier : " + panier);
        try {
            //enregistrement de la commande
            //et mise à jour du cache
            INSTANCE.createPanier(panier, catalogue);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        String url = "pages/receipt.jsp";
        forward(url, request, response);
    }

    private void doAddItems(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //il s'agit soit d'une nouvelle commande vue show catalogue
        //ou d'une commande modifier vue showDetails

        //recuperation, des produit choisi par le clietn
        String itemIds[] = request.getParameterValues("itemId");

        //Récuperation du scope sesseion du client
        ClientDTO client = (ClientDTO) request.getSession().getAttribute("client");
        String url = "";

        //traitement du panier cvréation de la commande et sauvegarde de celle ci dans la session
        if (itemIds != null && itemIds.length != 0) {
            Panier panier = new Panier(client);
            panier.setItemIds(itemIds);
            request.getSession().setAttribute("anOrder", panier);
            url = "pages/showDetails.jsp";
        } else {
            //panier vide
            url = "pages/emptyCart.jsp";
        }
        //cinematique
        forward(url, request, response);
    }

    private void doAuthenticate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            //recuperation des donnée du formauliare
            String nom = request.getParameter("nom");
            String motDePasse = request.getParameter("motDePasse");

            //1 recherche du client
            ClientDTO client = INSTANCE.findClientsByNom(nom);
            if (client != null) {
                if (client.getPass().equals(motDePasse)) {
                    request.getSession().setAttribute("client", client);
                } else {
                    request.getSession().removeAttribute("client");
                    client = null;
                }
            }

            // cinematique
            String url = (client == null ? "pages/showLogin.jsp" : "pages/showCatalogue.jsp");
            forward(url, request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void doInit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //gestion du cache application
        if (this.getServletContext().getAttribute("catalogue") == null) {
            //si pas de catalogue on le creer
            Catalogue catalogue = INSTANCE.getCatalogue();
            //sauvegarde du catalogue dans l'appilication
            this.getServletContext().setAttribute("catalogue", catalogue);
        }
        //prochaine vue
        String url = "pages/showLogin.jsp";
        forward(url, request, response);

    }
}
