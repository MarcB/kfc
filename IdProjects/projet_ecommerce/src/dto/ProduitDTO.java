package dto;

import java.io.Serializable;

public class ProduitDTO implements Serializable {

    private String nProd;
    private String descript;
    private float prix;
    private boolean dispo;
    private int stock;

    //GETTER SETTER
    public String getnProd() {
        return nProd;
    }

    public void setnProd(String nProd) {
        this.nProd = nProd;
    }

    public String getDescript() {
        return descript;
    }

    public void setDescript(String descript) {
        this.descript = descript;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }

    public boolean getDispo() {
        return dispo;
    }

    public void setDispo(boolean dispo) {
        this.dispo = dispo;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    //constructeur
    public ProduitDTO(String nProd, String descript, float prix, boolean dispo, int stock) {
        this.nProd = nProd;
        this.descript = descript;
        this.prix = prix;
        this.dispo = dispo;
        this.stock = stock;
    }

    //tostring
    @Override
    public String toString() {
        return "produitDTO{" +
                "nProd=" + nProd +
                ", descript='" + descript + '\'' +
                ", prix=" + prix +
                ", dispo='" + dispo + '\'' +
                ", stock=" + stock +
                '}';
    }
}
