<%--
  Created by IntelliJ IDEA.
  User: michael
  Date: 26/02/2020
  Time: 12:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Accusé de reception de la commande</title>
</head>
<body>
<h2><font face="Verdana, Arial">Merci</font></h2>
<p>
    ${client.nom} votre commande à été prise en compte.
</p>
<a href="process/action=showCatalogue">continuer a parcourir le catalogue</a>
<br>

<%@include file="footer.jsp" %>
</body>
</html>
