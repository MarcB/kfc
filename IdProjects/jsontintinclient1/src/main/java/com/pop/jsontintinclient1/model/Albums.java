package com.pop.jsontintinclient1.model;

import lombok.Data;

@Data
public class Albums {
    private long id;
    private String titre;
    private int annee;
    private Albums albumsBySuivant;

}
