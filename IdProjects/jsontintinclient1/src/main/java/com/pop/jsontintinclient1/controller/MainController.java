package com.pop.jsontintinclient1.controller;

import com.pop.jsontintinclient1.model.Personnages;
import com.pop.jsontintinclient1.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class MainController {

    @Autowired
    private UserService userService;

    // Injectez (inject) via application.properties.
    @Value("${welcome.message}")
    private String message;

    @Value("${error.message}")
    private String errorMessage;

    //------------------------------
    //      index >>  page home    -
    //------------------------------

    @RequestMapping(value = { "/", "/index" }, method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("message", message);
        return "index";
    }


    //************************************
    //              Album
    //************************************

    //list des albums ========================================================
    @RequestMapping(value = { "/albums" }, method = RequestMethod.GET)
    public String albumList(Model model ) {
        model.addAttribute("albums", userService.findAllAlb());
        return "albums";
    }

    //list des albums ========================================================
    @RequestMapping(value = { "/albumsID" }, method = RequestMethod.POST)
    public String albumById(@RequestParam("monid")int monid ,Model model ) {
        model.addAttribute( "albums",userService.findAlbById(monid) );
        return "albums";
    }

    //list des albums ========================================================
    @RequestMapping(value = {"/albumsannee"}, method = RequestMethod.POST)
    public String albumAnneeList(@RequestParam("annee1")Integer annee1,
                                 @RequestParam("annee2") Integer annee2, Model model) {
        model.addAttribute("albums", userService.findByAnneeBetween(annee1,annee2));
        return "albums";
    }

    //************************************
    //             Personnage
    //************************************

    //list des personnages
    @RequestMapping(value = { "/personnages" }, method = RequestMethod.GET)
    public String personnagesList(Model model ) {
        model.addAttribute("personnages", userService.findAllPers() );
        return "personnages";
    }

    //un personnage
    @RequestMapping(value = { "/persoId" }, method = RequestMethod.POST)
    public String persoById(@RequestParam("monid")int id ,Model model ) {
        model.addAttribute("personnages", userService.findPersById(id) );
        return "personnages";
//        return "persoById";
    }
    //*****************************************************************************
    // Bloc de creation
    //lancement de la page d'entrée
    @RequestMapping(value = { "/personnagesCreate" }, method = RequestMethod.GET)
    public String createPerso(Model model) {
        Personnages perso=new Personnages();
        model.addAttribute("perso",  perso);
        return "personnagesCreate";
    }
    //lancement de la requête pour la création
    @RequestMapping(value={"/created"},method = RequestMethod.POST)
    public String createPers(Model model, @Valid Personnages perso, BindingResult result){
//        model.addAttribute("personnages",userService.createPerso(perso ));
        if(result.hasErrors()){
            return "personnagesCreate";
        }
        userService.createPerso(perso );
        model.addAttribute("personnages", userService.findAllPers());
        return "personnages";
    }
    //*****************************************************************************
    // Bloc d'update
    //lancement de la page d'entrée
    @RequestMapping(value = { "/personnagesUpdate/{id}" }, method = RequestMethod.GET)
    public String updatePerso(@PathVariable("id") int persoId,Model model) {
        Personnages perso= userService.findPersById(persoId);
        model.addAttribute("perso",  perso);
        System.out.println("***********************up******************");
        return "personnagesUpdate";
    }
    //lancement de la requête pour la mise à jour
    @RequestMapping(value={"/updated"},method = RequestMethod.POST)
    public String updatePerso(Model model, @Valid Personnages perso, BindingResult result){
//        model.addAttribute("personnages",userService.createPerso(perso ));
        if(result.hasErrors()){
            return "personnagesUpdate";
        }
        userService.createPerso(perso );
        model.addAttribute("personnages", userService.findAllPers());
        return "personnages";
    }
    //**************************************************************************
    //delete du client selon id
    @GetMapping("/delete/{id}")
    public String deletePerso(@PathVariable("id") int id, Model model) {
        Personnages perso= userService.findPersById(id)/*.orElseThrow(() -> new IllegalArgumentException("Invalid Personnage Id:" + id))*/;
        userService.deletePerso(perso);
        model.addAttribute("personnages", userService.findAllPers());
        return "personnages";
    }

}
