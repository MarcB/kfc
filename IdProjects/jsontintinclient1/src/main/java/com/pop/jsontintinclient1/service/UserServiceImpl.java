package com.pop.jsontintinclient1.service;

import com.pop.jsontintinclient1.model.Albums;
import com.pop.jsontintinclient1.model.Personnages;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service

public class UserServiceImpl implements UserService {

     String URL_SERVER = "http://localhost:8080/";

    //************************************
    //              Album
    //************************************
    @Override
    public Albums[] findAllAlb(){
        RestTemplate restTemplate = new RestTemplate();
        Albums[] list=restTemplate.getForObject(URL_SERVER+"albums", Albums[].class);
        return list;
    }
    @Override
    public Albums findAlbById(int id){
        RestTemplate restTemplate=new RestTemplate();
        return restTemplate.getForObject(URL_SERVER+"albums/"+id,Albums.class);
    }
    @Override
    public Albums[] findByAnneeBetween(Integer annee1, Integer annee2) {
        RestTemplate restTemplate = new RestTemplate();
       Albums[] annesAlbums=restTemplate.getForObject(URL_SERVER+"albums/"+annee1+"/"+annee2, Albums[].class);
    return annesAlbums;
    }
    @Override
    public void createAlbum(Albums album) {

    }
    @Override
    public void updateAlbum(Albums album) {

    }
    @Override
    public void deleteAlbum(Albums album) {

    }

    //************************************
    //             Personnage
    //************************************

    @Override
    public Personnages[] findAllPers(){
        RestTemplate restTemplate = new RestTemplate();
        Personnages[] list=restTemplate.getForObject(URL_SERVER+"personnages", Personnages[].class);
        return list;
    }
    @Override
    public Personnages findPersById(int id) {
        RestTemplate restTemplate=new RestTemplate();
        Personnages pers = restTemplate.getForObject(URL_SERVER+"personnages/"+id,Personnages.class);
        return pers;
    }
    @Override
    public void createPerso(Personnages perso) {
        Personnages newPersonnages = new Personnages(perso.getId(),perso.getNom(),perso.getPrenom(),
                perso.getProfession(),perso.getSexe(),perso.getGenre() );

        RestTemplate restTemplate = new RestTemplate();

        // Data attached to the request.
        HttpEntity<Personnages> requestBody = new HttpEntity<>(newPersonnages);

        // Send request with POST method.
        ResponseEntity<Personnages> result
                = restTemplate.postForEntity(URL_SERVER+"/personnages/create", requestBody, Personnages.class);

        System.out.println("Status code:" + result.getStatusCode());

        // Code = 200.
        if (result.getStatusCode() == HttpStatus.OK) {
            Personnages pers = result.getBody();
            System.out.println("(Client Side) Personnages Created: "/* + pers.getId()*/);
        }
    }
    @Override
    public void deletePerso(Personnages personnages) {
        // Send request with DELETE method.
        RestTemplate restTemplate = new RestTemplate();
        System.out.println(personnages.getId()+"********************");
        restTemplate.delete(URL_SERVER+"/personnages/delete/"+personnages.getId());

        // Get
//        Personnages perso = restTemplate.getForObject(URL_SERVER+"/personnages/delete/"+personnages.getId(), Personnages.class);
//
//        if (perso != null) {
//            System.out.println("(Client side) Personnage after delete: ");
//            System.out.println("Personnage: " + perso.getNom() + " - " + perso.getPrenom());
//        } else {
//        if(perso==null){
//            System.out.println("Personnage not found!");
//        }
    }
    @Override
    public void updatePerso(Personnages personnages) {

    }
}







