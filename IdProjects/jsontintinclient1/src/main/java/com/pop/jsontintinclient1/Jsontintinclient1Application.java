package com.pop.jsontintinclient1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jsontintinclient1Application {

    public static void main(String[] args) {
        SpringApplication.run(Jsontintinclient1Application.class, args);
    }

}
