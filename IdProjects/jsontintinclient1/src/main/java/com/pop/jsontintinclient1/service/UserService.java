package com.pop.jsontintinclient1.service;

import com.pop.jsontintinclient1.model.Albums;
import com.pop.jsontintinclient1.model.Personnages;

public interface UserService {
    Albums[] findAllAlb();
    Albums[] findByAnneeBetween(Integer annee1, Integer annee2);
    Albums findAlbById(int id);

    public void createAlbum(Albums album);
    public void updateAlbum(Albums album);
    public void deleteAlbum(Albums album);

    Personnages[] findAllPers();
    Personnages findPersById (int id);
    public void createPerso(Personnages personnages);
    public void deletePerso(Personnages personnages);
    public void updatePerso(Personnages personnages);

    //   User findById(long id);
    //    User findByName(String name);
    //    void saveUser(User user);
    //    void updateUser(User user);
    //    void deleteUserById(long id);
    //    List<User> findAllUsers();
    //    void deleteAllUsers();
    //    public boolean isUserExist(User user);
}
