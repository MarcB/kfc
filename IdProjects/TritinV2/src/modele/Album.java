package modele;


public class Album implements Comparable<Album> {
    private String nom;
    private Integer year;
    private String suite;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = (nom==null?"None":nom.toUpperCase());
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = (year==0?1930:year);
    }

    public String getSuite() {return suite; }

    public void setSuite(String suite) { this.suite = suite; }

    public Album(String nom, int year,String suite) {
        this.setNom( nom);
        this.setYear( year);
        this.setSuite(suite);
    }
    public Album() {
        this.setNom( null);
        this.setYear( 0);
        this.setSuite(null);
    }

    @Override
    public String toString() {
        return "Album{" +
                "nom='" + nom + '\'' +
                ", year=" + year +
                " , suite"+suite+
                '}';
    }

    public static void main(String[] args) {
   //    Album me=new Album(null,0);
     //   System.out.println(me);
    }

    @Override
    public int compareTo(Album album) {
        //return this.nom.compareTo(album.nom);
        return this.nom.compareTo(album.nom)  ;


    }

 /* @Override
    public int compareTo(Album album) {
        return this.compareTo(album);

    }*/


}
