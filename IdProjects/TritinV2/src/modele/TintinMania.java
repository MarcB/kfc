package modele;

import static modele.EnumNature.*;
import static modele.EnumSexe.*;

public class TintinMania {


    private  void chargerAlbumsETPersonnages(){
        Album a1 = new Album("Tintin au pays des Soviets", 1930,null);
        Album a2 = new Album("Tintin au Congo", 1931,null);
        Album a3 = new Album("Tintin en Amrique", 1932,null);
        Album a4 = new Album("Les Cigares du pharaon", 1934,null);
        Album a5 = new Album("Le Lotus bleu", 1936,a4);
        Album a6 = new Album("L'Oreille cassee", 1937,null);
        Album a7 = new Album("L'Ile noire", 1938,null);
        Album a8 = new Album("Le Sceptre d'Ottokar", 1939,null);
        Album a9 = new Album("Le Crabe aux pinces d'or", 1941,null);
        Album a10 = new Album("L'Etoile mysterieuse", 1942,null);
        Album a11= new Album("Le Secret de la Licorne", 1943,null);
        Album a12= new Album("Le Tresor de Rackham le Rouge", 1944,a11);
        Album a13= new Album("Les 7 boules de cristal", 1948,null);
        Album a14= new Album("Le Temple du soleil", 1950,a13);

        Personnage p1 = new Personnage("Tintin","","Reporter",HOMME, GENTIL);
        Personnage p2 = new Personnage("Milou","",null,HOMME,GENTIL);
        Personnage p3 = new Personnage("Haddock","Archibald","Capitaine",HOMME,GENTIL);
        Personnage p4 = new Personnage("Tournesol","Tryphon","Professeur",HOMME,GENTIL);
        Personnage p5 = new Personnage("Dupond","","Detective",HOMME,GENTIL);
        Personnage p6 = new Personnage("Dupont","","Detective",HOMME,GENTIL);
        Personnage p7 = new Personnage("CASTAFIORE","Bianca","Cantatrice",FEMME,GENTIL);

        p1.participe(a1);
        p1.participe(a2);
        p1.participe(a3);
        p1.participe(a4);
        p1.participe(a5);
        p1.participe(a6);
        p1.participe(a7);
        p1.participe(a8);
        p1.participe(a9);
        p1.participe(a10);
        p1.participe(a11);
        p1.participe(a12);
        p1.participe(a13);
        p1.participe(a14);

        p2.participe(a1);
        p2.participe(a2);
        p2.participe(a3);
        p2.participe(a4);
        p2.participe(a5);
        p2.participe(a6);
        p2.participe(a7);
        p2.participe(a8);
        p2.participe(a9);
        p2.participe(a10);
        p2.participe(a11);
        p2.participe(a12);
        p2.participe(a13);
        p2.participe(a14);

        p3.participe(a9);
        p3.participe(a10);
        p3.participe(a11);
        p3.participe(a12);
        p3.participe(a13);
        p3.participe(a14);

        p4.participe(a12);
        p4.participe(a13);
        p4.participe(a14);

        p5.participe(a4);
        p5.participe(a5);

        p5.participe(a7);
        p5.participe(a8);
        p5.participe(a9);

        p5.participe(a11);
        p5.participe(a12);
        p5.participe(a13);
        p5.participe(a14);

        p6.participe(a4);
        p6.participe(a5);

        p6.participe(a7);
        p6.participe(a8);
        p6.participe(a9);

        p6.participe(a11);
        p6.participe(a12);
        p6.participe(a13);
        p6.participe(a14);


        mesTintins.add(a1);
        mesTintins.add(a2);
        mesTintins.add(a3);
        mesTintins.add(a4);
        mesTintins.add(a5);
        mesTintins.add(a6);
        mesTintins.add(a7);
        mesTintins.add(a8);
        mesTintins.add(a9);
        mesTintins.add(a10);
        mesTintins.add(a11);
        mesTintins.add(a12);
        mesTintins.add(a13);
        mesTintins.add(a14);

        mesPersonnages.add(p1);
        mesPersonnages.add(p2);
        mesPersonnages.add(p3);
        mesPersonnages.add(p4);
        mesPersonnages.add(p5);
        mesPersonnages.add(p6);
        mesPersonnages.add(p7);
    }




}