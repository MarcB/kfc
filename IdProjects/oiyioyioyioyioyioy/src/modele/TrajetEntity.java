package modele;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

@Entity

@NamedQueries({
        //pour le tableau
        @NamedQuery(name = "trajet.findAll", query = "SELECT t FROM TrajetEntity t"),
        //link ou tableau
        @NamedQuery(name = "trajet.findById",
                query = "SELECT t FROM TrajetEntity t WHERE t.ntrajet= ?1")

})

@Table(name = "trajet", schema = "test", catalog = "")
public class TrajetEntity {
    private int ntrajet;
    private String description;
    private Timestamp debdate;
    private String debadress;
    private Timestamp findate;
    private String finadress;
    private String accepte;
    private int distance;
    private Double montant;
    private VehiculeEntity vehiculeByImmatrajet;
    private Collection<PersonneEntity> listTrajets;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "ntrajet")
    public int getNtrajet() {
        return ntrajet;
    }

    public void setNtrajet(int ntrajet) {
        this.ntrajet = ntrajet;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "debdate")
    public Timestamp getDebdate() {
        return debdate;
    }

    public void setDebdate(Timestamp debdate) {
        this.debdate = debdate;
    }

    @Basic
    @Column(name = "debadress")
    public String getDebadress() {
        return debadress;
    }

    public void setDebadress(String debadress) {
        this.debadress = debadress;
    }

    @Basic
    @Column(name = "findate")
    public Timestamp getFindate() {
        return findate;
    }

    public void setFindate(Timestamp findate) {
        this.findate = findate;
    }

    @Basic
    @Column(name = "finadress")
    public String getFinadress() {
        return finadress;
    }

    public void setFinadress(String finadress) {
        this.finadress = finadress;
    }

    @Basic
    @Column(name = "accepte")
    public String getAccepte() {
        return accepte;
    }

    public void setAccepte(String accepte) {
        this.accepte = accepte;
    }

    @Basic
    @Column(name = "distance")
    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    @Basic
    @Column(name = "montant")
    public Double getMontant() {
        return montant;
    }

    public void setMontant(Double montant) {
        this.montant = montant;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TrajetEntity that = (TrajetEntity) o;
        return ntrajet == that.ntrajet &&
                distance == that.distance &&
                Objects.equals(description, that.description) &&
                Objects.equals(debdate, that.debdate) &&
                Objects.equals(debadress, that.debadress) &&
                Objects.equals(findate, that.findate) &&
                Objects.equals(finadress, that.finadress) &&
                Objects.equals(accepte, that.accepte) &&
                Objects.equals(montant, that.montant);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ntrajet, description, debdate, debadress, findate, finadress, accepte, distance, montant);
    }

    @ManyToOne
    @JoinColumn(name = "immatrajet", referencedColumnName = "nvehicule", nullable = false)
    public VehiculeEntity getVehiculeByImmatrajet() {
        return vehiculeByImmatrajet;
    }

    public void setVehiculeByImmatrajet(VehiculeEntity vehiculeByImmatrajet) {
        this.vehiculeByImmatrajet = vehiculeByImmatrajet;
    }

    @OneToMany(mappedBy = "trajetByMail")
    public Collection<PersonneEntity> getListTrajets() {
        return listTrajets;
    }

    public void setListTrajets(Collection<PersonneEntity> listTrajets) {
        this.listTrajets = listTrajets;
    }
}
