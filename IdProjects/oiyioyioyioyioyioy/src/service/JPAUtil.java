package service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {
    //singleton

    private static EntityManagerFactory emf= Persistence.createEntityManagerFactory("testPU");

    public static EntityManagerFactory getEmf() {
        return emf;
    }
}
