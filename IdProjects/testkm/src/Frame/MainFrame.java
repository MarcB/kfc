package Frame;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

public class MainFrame extends JDialog {
    private JPanel rootMain;
    private JTabbedPane TabAction;
    private JComboBox comboPersonne;
    private JPanel FieldInfo;
    private JPanel FieldTrajet;
    private JPanel FVehicule;
    private JFormattedTextField prenomTextField;
    private JFormattedTextField nomTextField;
    private JTable table1;
    private JButton retirerButton;
    private JButton ajouterButton;
    private JFormattedTextField mailTextField;
    private JPasswordField passwordField;
    private JFormattedTextField VoitureFormattedTextField;
    private JComboBox cvBox;
    private JButton CarteGriseAjouterButton;
    private JButton CarteGriseConsulterButton;
    private JButton PermisConsulterButton;
    private JButton PermisAjouterButton;
    private JButton ImprimerButton;
    private JLabel ErrorLabel;
    private JPanel TabPan;
    private JLabel lbImmatriculation;
    private JFormattedTextField formattedTextField1;
    private JFormattedTextField RembouseTextField;
    private JFormattedTextField DistanceTextField;
    private JComboBox comboVoiture;
    private String[] loger={"pop@gmail.com","visiteur@gmail.com"};
    private String[] pf={"3cv","4cv","5cv","6cv","7cv et +"};
    public Image img ;


    public MainFrame(){

        JFrame frame = new JFrame("MainFrame");
        frame.setContentPane(rootMain);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);


        initCombo(loger,comboPersonne);
        initCombo(pf,cvBox);


        CarteGriseConsulterButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                JDialog pop=new JDialog();
                JPanel popPan=new JPanel();
                pop.setBounds(0,0,200,500);
                popPan.setBounds(50,50,200,100);
                popPan.setBackground(Color.black);
                pop.add(popPan);
                pop.setVisible(true);
                pop.setTitle("Frais");

                ///////////

/*
                BufferedImage img = null;
                try {
                    img = ImageIO.read(new File("permis.jpg"));
                } catch (IOException e) {
                }
                Graphics.drawImage(img, 50, 50, null);


         */


         //       pop.getGraphics().drawImage(frame.img,0,0, label);


            }
        });
        ajouterButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
               // new Mapping();
            }
        });
        retirerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
               // new Mapping();
            }
        });
        CarteGriseConsulterButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
             //   new CarteGFrm();
            }
        });
        CarteGriseAjouterButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
              //  new CarteGFrm();
            }
        });
        PermisConsulterButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new PermisFrm();
            }
        });
        PermisAjouterButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new PermisFrm();
            }
        });
    }



    public static void main(String[] args) {
       /* JFrame frame = new JFrame("MainFrame");
        frame.setContentPane(new MainFrame().broot);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);*/
    }

    private void  initCombo(String[] maTable , JComboBox test) {
        for (String item : maTable)
        {
           test.addItem(item);
        }

    }




    public void init(Graphics g) {

        try {
            String path= "examples/strawberry.jpg";
            img = ImageIO.read(getClass().getResource(path));

        } catch (IOException e) {
        }
        g.drawImage(img, 50, 50, null);
    }



}
