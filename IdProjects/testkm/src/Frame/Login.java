package Frame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Login {

    public JPanel rootLogin;
    ///////////////////////////////////////////////////
    private JTabbedPane tabbedPane1;

    //////////////////////////////////////////////////////
    private JPanel loginTab;
    /////////////////////////////////////////////////////
    private JPasswordField passwordField;
    private JFormattedTextField mailTextField;
    private JLabel lbVerifMail;
    private JLabel lbVerifPass;
    private JLabel lbVerifLogin;
    private JButton loginButton;
    private JCheckBox RememberLoginCheckBox;

    //////////////////////////////////////////////////////
    private JPanel createTab;
    ////////////////////////////////////////////////////
    private JLabel LMail;
    private JLabel lbName;
    private JLabel lbSurname;
    private JLabel lbPass1;
    private JLabel lbMail1;

    private JLabel lbConfirmePass;
    private JLabel lbConfirmeMail;
    private JLabel lbMailCompar;
    private JLabel lbPassCompar;

    private JFormattedTextField mailTxTCrea1;
    private JFormattedTextField mailTxtCrea2;
    private JPasswordField passwordFieldCrea1;
    private JPasswordField passwordFieldCrea2;
    private JFormattedTextField nameTXTCrea;
    private JFormattedTextField surnameTxtCrea;

///////////////////////////////.60 pour 11 char
    ///return size sur titre //display


    private JButton registerButton;

    private boolean boolm;
    private boolean boolp;
    public boolean affi = true;


    public Login() {

        JFrame myFrame = new JFrame();

        myFrame.setContentPane(rootLogin);
        myFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        myFrame.pack();
        myFrame.setVisible(true);
        myFrame.setTitle("Login");

        mailTextField.setText("pop@gmail.com");
        passwordField.setText("School");

        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String mail = mailTextField.getText();
                String pass = lbVerifMail.getText();


                if (mailTextField.getText().equals("pop@gmail.com")) {
                    lbVerifMail.setText("mail ok");
                    lbVerifMail.setForeground(Color.green);
                    boolm = true;
                } else {
                    lbVerifMail.setText("mail invalide");
                    lbVerifMail.setForeground(Color.red);
                    boolm = false;
                }

                if (passwordField.getText().equals("School")) {
                    lbVerifPass.setText("password ok");
                    lbVerifPass.setForeground(Color.green);
                    boolp = true;
                } else {
                    lbVerifPass.setText("password invalide");
                    lbVerifPass.setForeground(Color.red);
                    boolp = false;
                }

                if (!boolm || !boolp) {
                    lbVerifLogin.setText("erreur de connexion");
                    lbVerifLogin.setForeground(Color.red);

                } else if (boolp && boolm) {
                    lbVerifLogin.setText("Connexion en cours");
                    lbVerifLogin.setForeground(Color.green);

                    int i = 0;
                    do {
                        i++;
                        System.out.println(i);
                    } while (i < 500);

                    myFrame.dispose();
                    new MainFrame();
                }
            }
        });
        mailTextField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                super.focusGained(e);

                ////////////////////////test/////////////////////////




//////////////////////////////////////////////////////////////

            }

            @Override
            public void focusLost(FocusEvent e) {
                super.focusLost(e);

            }
        });
        passwordField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                super.focusGained(e);
            }

            @Override
            public void focusLost(FocusEvent e) {
                super.focusLost(e);

            }
        });
        loginTab.addComponentListener(new ComponentAdapter() {
        });
        passwordField.addInputMethodListener(new InputMethodListener() {
            @Override
            public void inputMethodTextChanged(InputMethodEvent inputMethodEvent) {

            }

            @Override
            public void caretPositionChanged(InputMethodEvent inputMethodEvent) {

            }
        });
        mailTextField.addInputMethodListener(new InputMethodListener() {
            @Override
            public void inputMethodTextChanged(InputMethodEvent inputMethodEvent) {

            }

            @Override
            public void caretPositionChanged(InputMethodEvent inputMethodEvent) {

            }
        });
    }

    public static void main(String[] args) {

    }

    public void initTxtLog(){
        lbVerifMail.setText(verif(mailTextField.getText(),"pop@gmail.com","mail"));
        lbVerifPass.setText(verif(passwordField.getText(),"School","pass"));

        if (lbVerifMail.getText().equals("ok")) {
            lbVerifMail.setForeground(Color.green);
            boolm = true;
        } else {
            lbVerifMail.setForeground(Color.red);
            boolm = false;
        }

        if (lbVerifPass.getText().equals("ok")) {
            lbVerifPass.setForeground(Color.green);
            boolp = true;
        } else {
            lbVerifPass.setForeground(Color.red);
            boolp = false;
        }
    }

    public void initTxtCreat(){

    }


              ///////////////////////////////////////////////////
////////////////////////////   method verif     //////////////////////////////
             ///////////////////////////////////////////////////

    public String verif(String monChamps, String maVariable, String typage) {

        String result = "Requiert :";

        String formatName = "[A-Za-z]";
        String formatRichName = "[A-Za-z-\\S_-.]";
        String formatMail = "[A-Za-z0-9-_.-]+@[A-Za-z0-9]+\\.[A-Za-z]{2,4}";
        String formatPass = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$";

///////////////////// verif format

        if (typage.equals("mail")) {
            if (!monChamps.matches(formatMail)) {
                result = " Format non valide  ";
            }


        } else if (typage.equals("pass")) {
            if (!monChamps.matches("^.{8,}$ " )&& (monChamps.length()<8) ) {
                result = " 8 cracateres";
            } else if (!monChamps.matches("^(?=.*[0-9]).{8,}$")&& (monChamps.length()<8) ) {
                result = " un chiffre ";
            } else if (!monChamps.matches("^(?=.*[0-9])(?=.*[a-z]).{8,}$")&& (monChamps.length()<8) ) {
                result = " une minuscule";
            } else if (!monChamps.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,}$")&& (monChamps.length()<8) ) {
                result = " une majuscule";
            } else if (!monChamps.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).{8,}$")&& (monChamps.length()<8) ) {
                result = " un caractère special";
            } else if (!monChamps.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$")&& (monChamps.length()<8) ) {
                result = "les espaces sont interdits";
            }

        } else if (typage.equals("name")) {
            if (!monChamps.matches(formatName)) {
                result = "Seuls les lettres sont autorisées";
            }

        }

///////////////////// verif valeur

        if (monChamps.equals("")) {
            result = "champs vide";
        } else if (monChamps.equals(maVariable)) {
            result = "ok";
       // } else if ((!monChamps.equals(maVariable))&&(maVariable!="")) {
        //    result = "non valide";
        }

        return result;
    }

/////////////////////////////////////////////////////////////////////



}
