<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page contentType="text/html" pageEncoding="UTF-8"  import="beans.*" %>

<jsp:useBean id="ens" scope="application" class="beans.CatalogueBean"/>
<jsp:useBean id="panier" scope="session" class="beans.PanierBean"/>

<html>
    <head>
        <title>Title</title>
    </head>

    <body>
        <%--genere une lsite de livre --%>
        <ul>
             <c:forEach items="${ens.catalogue}" var="association">
                <li>
                    ${association.key} - ${associaton.value.nom}
                </li>
             </c:forEach>
        </ul>

    <c:if test="${not empty panier.listeProduits}">
        votre panier contient :
    </c:if>
        <table>
            <c:forEach items="${panier.listeProduits}" var="produits">
                <tr>
                    <td>
                        ${produits.nom}

                    </td>
                    <fmt:formatNumber value="${produit.prix}" type ="currency"/>
                </tr>
            </c:forEach>
        </table>
    </body>
</html>
