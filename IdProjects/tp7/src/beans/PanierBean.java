package beans;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class PanierBean implements Serializable {

    private static final long serialVersionUID =2L;
    //attribut d instance
    private Set<ProduitBean> listeProduits=new HashSet<>();

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Set<ProduitBean> getListeProduits() {
        return listeProduits;
    }

    public void setListeProduits(Set<ProduitBean> listeProduits) {
        this.listeProduits = listeProduits;
    }

    public PanierBean(){
        ProduitBean prod =new ProduitBean();
        prod.setId("1000");
        prod.setNom("JavaServer Pages");
        prod.setDescr("Learn how to developjsp based on web appi");
        prod.setPrix(32.95f);
        listeProduits.add(prod);
    }



}
