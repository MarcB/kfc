package beans;

import java.io.Serializable;
import java.util.Objects;

public class ProduitBean implements Serializable {

    private static final long serialVersionUID =1L;

    private static String id ;
    private static String nom;
    private static String descr;
    private float prix;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public static String getId() {
        return id;
    }

    public static void setId(String id) {
        ProduitBean.id = id;
    }

    public static String getNom() {
        return nom;
    }

    public static void setNom(String nom) {
        ProduitBean.nom = nom;
    }

    public static String getDescr() {
        return descr;
    }

    public static void setDescr(String descr) {
        ProduitBean.descr = descr;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }

    @Override
    public String toString() {
        return "ProduitBean{" +
                "prix=" + prix +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProduitBean that = (ProduitBean) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPrix());
    }
}
