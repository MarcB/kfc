package popschool.mycinemaspring.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import popschool.mycinemaspring.MycinemaspringApplication;
import popschool.mycinemaspring.dao.ClientRepository;
import popschool.mycinemaspring.model.Client;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

@Component
public class ClientTest implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(MycinemaspringApplication.class);

    @Autowired
    private ClientRepository clientRepository;

    private void information(Client c) {        log.info(c.toString());    }

    @Override
    public void run(String... args) throws Exception {
        //creation de client
       // List<Client> clients = new Arrays.asList(
                new Client("Duss", "Jean Paul", "123 rue bidon", 10);
                new Client("Roove", "Jean Paul", "132 rue bidon", 10);
                new Client("Sellam", "Brahim", "108 rue de Roubaix", 0);
     //   );

    }
}