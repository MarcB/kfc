package popschool.mycinemaspring.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import popschool.mycinemaspring.MycinemaspringApplication;
import popschool.mycinemaspring.dao.FilmRepository;
import popschool.mycinemaspring.model.Film;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

@Component
public class FilmTest implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(MycinemaspringApplication.class);

    @Autowired
    private FilmRepository filmRepository;

    private void information(Film f){
        log.info(f.toString());
    }

    @Override
    public void run(String... args) throws Exception {
        //fetch all films
        log.info("TROUVER TOUS LES FILMS findAll() :");
        log.info("---------------------------------------------------------------------------------------------------");
        for (Film film : filmRepository.findAll()){
            log.info(film.toString());
        }
        log.info("");

        //liste de films d'un genre
        log.info("TROUVER la liste des films d'un genre donné -Drame- :");
        log.info("---------------------------------------------------------------------------------------------------");
        for (Film film : filmRepository.findByGenre_Nature("Drame")){
            log.info(film.toString());
        }
        log.info("");

        //liste de films d'un acteur en fonction de son nom
        log.info("TROUVER la liste des films d'un acteur donné en fonction de son nom -Depardieu- :");
        log.info("---------------------------------------------------------------------------------------------------");
        for (Film film : filmRepository.findByActeur_Nom("Depardieu")){
            log.info(film.toString());
        }
        log.info("");

        //liste de films d'un pays en fonction de son nom
        log.info("TROUVER la liste des films d'un pays donné en fonction de son nom -France- :");
        log.info("---------------------------------------------------------------------------------------------------");
        for (Film film : filmRepository.findByPays_Nom("France")){
            log.info(film.toString());
        }
        log.info("");

        //nombre de film par genre traitement des tuple
        log.info("TROUVER le nombre de films par genre :");
        log.info("---------------------------------------------------------------------------------------------------");
        filmRepository.findNbreFilmsByGenre().forEach(f->{
            log.info("genre : " + f.get("nature", String.class)+
                    " : "+
                    f.get("nombre",Long.class));
        });

    }

}
