package popschool.mycinemaspring.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import popschool.mycinemaspring.model.Client;


import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface ClientRepository  extends CrudRepository<Client, Long> {

   // Integer save(Client client);
 //   int update(Book book);
//    int deleteById(Long id);
}