package popschool.mycinemaspring.dao;

import jdk.internal.org.objectweb.asm.tree.analysis.Value;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import popschool.mycinemaspring.model.Acteur;
import popschool.mycinemaspring.model.Film;

import javax.persistence.Tuple;
import java.util.List;

public interface FilmRepository extends CrudRepository<Film, Long> {

    List<Film> findByGenre_Nature(String drame);

    List<Film> findByActeur_Nom(String depardieu);

    List<Film> findByPays_Nom(String france);


    //methode Tuple
    @Query(value = "Select f.genre.nature as nature, count(f.genre.nature) as nombre" +
            " from Film f " +
            " group by f.genre.nature")
    List<Tuple> findNbreFilmsByGenre();


}
