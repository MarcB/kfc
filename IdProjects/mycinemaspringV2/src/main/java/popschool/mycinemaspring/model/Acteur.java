package popschool.mycinemaspring.model;

import lombok.Data;

import javax.persistence.*;
import java.util.*;

@Data
@Entity
@Table(name="acteur")
public class Acteur {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int nacteur;
    @OneToMany(mappedBy = "acteur")
    private List<Film> films = new ArrayList<>();
    private String nom;
    private String prenom;
    @Temporal(TemporalType.DATE)
    private Date naissance;
    @ManyToOne
    @JoinColumn(name = "nationalite")
    private Pays pays;
    private Integer nbrefilms;
    private Integer nationalite;
    private Long nationnalite;
    private Pays paysByNationalite;
    private Collection<Film> filmsByNacteur;

    protected Acteur(){}

    @Id
    @Column(name = "nacteur", nullable = false)
    public int getNacteur() {
        return nacteur;
    }

    //private Long nationalite;

    public void setNacteur(int nacteur) {
        this.nacteur = nacteur;
    }

    @Basic
    @Column(name = "nom", nullable = false, length = 20)
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Basic
    @Column(name = "prenom", nullable = false, length = 15)
    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @Basic
    @Column(name = "naissance", nullable = true)
    public java.sql.Date getNaissance() {
        return (java.sql.Date) naissance;
    }

    public void setNaissance(java.sql.Date naissance) {
        this.naissance = naissance;
    }

    @Basic
    @Column(name = "nbrefilms", nullable = true, precision = 0)
    public Integer getNbrefilms() {
        return nbrefilms;
    }

    public void setNbrefilms(Integer nbrefilms) {
        this.nbrefilms = nbrefilms;
    }

    public Pays getPays() {
        return pays;
    }

    public void setPays(Pays pays) {
        this.pays = pays;
    }

    @Override
    public String toString() {
        return "Acteur{" +
                "nacteur=" + nacteur +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", naissance=" + naissance +
                ", pays=" + pays +
                ", nbrefilms=" + nbrefilms +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Acteur)) return false;
        Acteur acteur = (Acteur) o;
        return nom.equals(acteur.nom) &&
                prenom.equals(acteur.prenom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nom, prenom);
    }

    @Basic
    @Column(name = "nationalite", nullable = true)
    public Integer getNationalite() {
        return nationalite;
    }

    public void setNationalite(Integer nationalite) {
        this.nationalite = nationalite;
    }

    @Basic
    @Column(name = "nationnalite", nullable = true)
    public Long getNationnalite() {
        return nationnalite;
    }

    public void setNationnalite(Long nationnalite) {
        this.nationnalite = nationnalite;
    }

    @ManyToOne
    @JoinColumn(name = "nationalite", referencedColumnName = "npays")
    public Pays getPaysByNationalite() {
        return paysByNationalite;
    }

    public void setPaysByNationalite(Pays paysByNationalite) {
        this.paysByNationalite = paysByNationalite;
    }

    @OneToMany(mappedBy = "acteurByNacteurprincipal")
    public Collection<Film> getFilmsByNacteur() {
        return filmsByNacteur;
    }

    public void setFilmsByNacteur(Collection<Film> filmsByNacteur) {
        this.filmsByNacteur = filmsByNacteur;
    }
}
