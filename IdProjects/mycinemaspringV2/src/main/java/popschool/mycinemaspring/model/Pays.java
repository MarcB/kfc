package popschool.mycinemaspring.model;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Data
@Entity
@Table(name="pays")
public class Pays {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int npays;
    private String nom;
    @OneToMany(mappedBy = "pays")
    private List<Acteur> acteurs = new ArrayList<>();
    @OneToMany(mappedBy = "pays")
    private List<Film> films = new ArrayList<>();
    private Collection<Acteur> acteursByNpays;
    private Collection<Film> filmsByNpays;

    @Id
    @Column(name = "npays", nullable = false)
    public int getNpays() {
        return npays;
    }

    public void setNpays(int npays) {
        this.npays = npays;
    }

    @Basic
    @Column(name = "nom", nullable = false, length = 20)
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Pays{" +
                ", nom='" + nom + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pays pays = (Pays) o;
        return Objects.equals(npays, pays.npays) &&
                Objects.equals(nom, pays.nom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(npays, nom);
    }

    @OneToMany(mappedBy = "paysByNationalite")
    public Collection<Acteur> getActeursByNpays() {
        return acteursByNpays;
    }

    public void setActeursByNpays(Collection<Acteur> acteursByNpays) {
        this.acteursByNpays = acteursByNpays;
    }

    @OneToMany(mappedBy = "paysByNpays")
    public Collection<Film> getFilmsByNpays() {
        return filmsByNpays;
    }

    public void setFilmsByNpays(Collection<Film> filmsByNpays) {
        this.filmsByNpays = filmsByNpays;
    }
}
