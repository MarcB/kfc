package popschool.mycinemaspring.model;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;

@Data
@Entity
@Table(name="film")
public class Film {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long nfilm;
    private String titre;
    @ManyToOne
    @JoinColumn(name = "ngenre")
    private Genre genre;
    @Temporal(TemporalType.DATE)
    private Date sortie;
    @ManyToOne
    @JoinColumn(name = "npays")
    private Pays pays;
    private String realisateur;
    @ManyToOne
    @JoinColumn(name ="nacteurprincipal")
    private Acteur acteur;
    private BigDecimal entrees;
    private String oscar;
    private int ngenre;
    private int npays;
    private int nacteurprincipal;
    private Collection<Emprunt> empruntsByNfilm;
    private Genre genreByNgenre;
    private Pays paysByNpays;
    private Acteur acteurByNacteurprincipal;

    @Id
    @Column(name = "nfilm", nullable = false)
    public Long getNfilm() {
        return nfilm;
    }

    public void setNfilm(Long nfilm) {
        this.nfilm = nfilm;
    }

    @Basic
    @Column(name = "titre", nullable = false, length = 40)
    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    @Basic
    @Column(name = "sortie", nullable = true)
    public java.sql.Date getSortie() {
        return (java.sql.Date) sortie;
    }

    public void setSortie(java.sql.Date sortie) {
        this.sortie = sortie;
    }

    @Basic
    @Column(name = "realisateur", nullable = false, length = 20)
    public String getRealisateur() {
        return realisateur;
    }

    public void setRealisateur(String realisateur) {
        this.realisateur = realisateur;
    }

    @Basic
    @Column(name = "entrees", nullable = true, precision = 2)
    public BigDecimal getEntrees() {
        return entrees;
    }

    public void setEntrees(BigDecimal entrees) {
        this.entrees = entrees;
    }

    @Basic
    @Column(name = "oscar", nullable = false, length = 1)
    public String getOscar() {
        return oscar;
    }

    public void setOscar(String oscar) {
        this.oscar = oscar;
    }

    @Basic
    @Column(name = "ngenre", nullable = false)
    public int getNgenre() {
        return ngenre;
    }

    public void setNgenre(int ngenre) {
        this.ngenre = ngenre;
    }

    @Basic
    @Column(name = "npays", nullable = false)
    public int getNpays() {
        return npays;
    }

    public void setNpays(int npays) {
        this.npays = npays;
    }

    @Basic
    @Column(name = "nacteurprincipal", nullable = false)
    public int getNacteurprincipal() {
        return nacteurprincipal;
    }

    public void setNacteurprincipal(int nacteurprincipal) {
        this.nacteurprincipal = nacteurprincipal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Film film = (Film) o;
        return ngenre == film.ngenre &&
                npays == film.npays &&
                nacteurprincipal == film.nacteurprincipal &&
                Objects.equals(nfilm, film.nfilm) &&
                Objects.equals(titre, film.titre) &&
                Objects.equals(sortie, film.sortie) &&
                Objects.equals(realisateur, film.realisateur) &&
                Objects.equals(entrees, film.entrees) &&
                Objects.equals(oscar, film.oscar);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nfilm, titre, ngenre, sortie, npays, realisateur, nacteurprincipal, entrees, oscar);
    }

    @OneToMany(mappedBy = "filmByNfilm")
    public Collection<Emprunt> getEmpruntsByNfilm() {
        return empruntsByNfilm;
    }

    public void setEmpruntsByNfilm(Collection<Emprunt> empruntsByNfilm) {
        this.empruntsByNfilm = empruntsByNfilm;
    }

    @ManyToOne
    @JoinColumn(name = "ngenre", referencedColumnName = "ngenre", nullable = false)
    public Genre getGenreByNgenre() {
        return genreByNgenre;
    }

    public void setGenreByNgenre(Genre genreByNgenre) {
        this.genreByNgenre = genreByNgenre;
    }

    @ManyToOne
    @JoinColumn(name = "npays", referencedColumnName = "npays", nullable = false)
    public Pays getPaysByNpays() {
        return paysByNpays;
    }

    public void setPaysByNpays(Pays paysByNpays) {
        this.paysByNpays = paysByNpays;
    }

    @ManyToOne
    @JoinColumn(name = "nacteurprincipal", referencedColumnName = "nacteur", nullable = false)
    public Acteur getActeurByNacteurprincipal() {
        return acteurByNacteurprincipal;
    }

    public void setActeurByNacteurprincipal(Acteur acteurByNacteurprincipal) {
        this.acteurByNacteurprincipal = acteurByNacteurprincipal;
    }
}
