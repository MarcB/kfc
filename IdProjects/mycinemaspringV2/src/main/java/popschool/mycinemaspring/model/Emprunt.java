package popschool.mycinemaspring.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
public class Emprunt {
    private int nemprunt;
    private int nclient;
    private int nfilm;
    private String retour;
    private Date dateEmprunt;
    private Client clientByNclient;
    private Film filmByNfilm;

    @Id
    @Column(name = "nemprunt", nullable = false)
    public int getNemprunt() {
        return nemprunt;
    }

    public void setNemprunt(int nemprunt) {
        this.nemprunt = nemprunt;
    }

    @Basic
    @Column(name = "nclient", nullable = false)
    public int getNclient() {
        return nclient;
    }

    public void setNclient(int nclient) {
        this.nclient = nclient;
    }

    @Basic
    @Column(name = "nfilm", nullable = false)
    public int getNfilm() {
        return nfilm;
    }

    public void setNfilm(int nfilm) {
        this.nfilm = nfilm;
    }

    @Basic
    @Column(name = "retour", nullable = true, length = 3)
    public String getRetour() {
        return retour;
    }

    public void setRetour(String retour) {
        this.retour = retour;
    }

    @Basic
    @Column(name = "dateEmprunt", nullable = false)
    public Date getDateEmprunt() {
        return dateEmprunt;
    }

    public void setDateEmprunt(Date dateEmprunt) {
        this.dateEmprunt = dateEmprunt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Emprunt emprunt = (Emprunt) o;
        return nemprunt == emprunt.nemprunt &&
                nclient == emprunt.nclient &&
                nfilm == emprunt.nfilm &&
                Objects.equals(retour, emprunt.retour) &&
                Objects.equals(dateEmprunt, emprunt.dateEmprunt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nemprunt, nclient, nfilm, retour, dateEmprunt);
    }

    @ManyToOne
    @JoinColumn(name = "nclient", referencedColumnName = "nclient", nullable = false)
    public Client getClientByNclient() {
        return clientByNclient;
    }

    public void setClientByNclient(Client clientByNclient) {
        this.clientByNclient = clientByNclient;
    }

    @ManyToOne
    @JoinColumn(name = "nfilm", referencedColumnName = "nfilm", nullable = false)
    public Film getFilmByNfilm() {
        return filmByNfilm;
    }

    public void setFilmByNfilm(Film filmByNfilm) {
        this.filmByNfilm = filmByNfilm;
    }
}
