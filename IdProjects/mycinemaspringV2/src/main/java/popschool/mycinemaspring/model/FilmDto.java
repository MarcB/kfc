package popschool.mycinemaspring.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;


public class FilmDto {

    private String nom;
    private Long nombre;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Long getNombre() {
        return nombre;
    }

    public void setNombre(Long nombre) {
        this.nombre = nombre;
    }

    public FilmDto(String nom, Long nombre) {
        this.nom = nom;
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "FilmDto{" +
                "nom='" + nom + '\'' +
                ", nombre=" + nombre +
                '}';
    }
}
