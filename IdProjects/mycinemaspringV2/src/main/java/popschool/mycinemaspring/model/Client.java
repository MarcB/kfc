package popschool.mycinemaspring.model;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
public class Client {
    private int nclient;
    private String nom;
    private String prenom;
    private String adresse;
    private int anciennete;
    private Collection<Emprunt> empruntsByNclient;

    @Id
    @Column(name = "nclient", nullable = false)
    public int getNclient() {
        return nclient;
    }

    public void setNclient(int nclient) {
        this.nclient = nclient;
    }

    @Basic
    @Column(name = "nom", nullable = false, length = 30)
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Basic
    @Column(name = "prenom", nullable = false, length = 30)
    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @Basic
    @Column(name = "adresse", nullable = false, length = 30)
    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    @Basic
    @Column(name = "anciennete", nullable = false, precision = 0)
    public int getAnciennete() {
        return anciennete;
    }

    public Client() {    }

    public Client(String nom, String prenom, String adresse,
                  int anciennete, Collection<Emprunt> empruntsByNclient) {
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.anciennete = anciennete;
        this.empruntsByNclient = empruntsByNclient;
    }    public Client(String nom, String prenom, String adresse,
                  int anciennete) {
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.anciennete = anciennete;

    }

    public Client(int nclient) { this.nclient=nclient;   }

    public void setAnciennete(int anciennete) {
        this.anciennete = anciennete;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return nclient == client.nclient &&
                anciennete == client.anciennete &&
                Objects.equals(nom, client.nom) &&
                Objects.equals(prenom, client.prenom) &&
                Objects.equals(adresse, client.adresse);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nclient, nom, prenom, adresse, anciennete);
    }

    @OneToMany(mappedBy = "clientByNclient")
    public Collection<Emprunt> getEmpruntsByNclient() {
        return empruntsByNclient;
    }

    @Override
    public String toString() {
        return "Client{" +
                "nclient=" + nclient +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", adresse='" + adresse + '\'' +
                ", anciennete=" + anciennete +
                ", empruntsByNclient=" + empruntsByNclient +
                '}';
    }

    public void setEmpruntsByNclient(Collection<Emprunt> empruntsByNclient) {
        this.empruntsByNclient = empruntsByNclient;


    }
}
