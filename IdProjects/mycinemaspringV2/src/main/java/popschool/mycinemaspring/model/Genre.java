package popschool.mycinemaspring.model;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Data
@Entity
@Table(name="genre")
public class Genre {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long ngenre;
    private String nature;
    @OneToMany(mappedBy = "genre")
    private List<Film> films = new ArrayList<>();
    private Collection<Film> filmsByNgenre;

    @Id
    @Column(name = "ngenre", nullable = false)
    public Long getNgenre() {
        return ngenre;
    }

    public void setNgenre(Long ngenre) {
        this.ngenre = ngenre;
    }

    @Basic
    @Column(name = "nature", nullable = false, length = 20)
    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    @Override
    public String toString() {
        return
                "{ nature='" + nature + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Genre genre = (Genre) o;
        return Objects.equals(ngenre, genre.ngenre) &&
                Objects.equals(nature, genre.nature);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ngenre, nature);
    }

    @OneToMany(mappedBy = "genreByNgenre")
    public Collection<Film> getFilmsByNgenre() {
        return filmsByNgenre;
    }

    public void setFilmsByNgenre(Collection<Film> filmsByNgenre) {
        this.filmsByNgenre = filmsByNgenre;
    }
}
