package popschool.tintinspringjpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TintinspringjpaApplication {

    public static void main(String[] args) {
        SpringApplication.run(TintinspringjpaApplication.class, args);
    }

}
