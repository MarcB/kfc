package popschool.tintinspringjpa.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "personnages")
public class PersonnagesEntity {
    private int id;
    private String nom;
    private String prenom;
    private String profession;
    private String sexe;
    private String genre;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public PersonnagesEntity() {    }

    public PersonnagesEntity(String nom, String prenom,
                             String profession, String sexe, String genre) {
        this.nom = nom;
        this.prenom = prenom;
        this.profession = profession;
        this.sexe = sexe;
        this.genre = genre;
    }

    public PersonnagesEntity(int id) {  this.id=id;  }

    @Basic
    @Column(name = "nom", nullable = true, length = 30)
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Basic
    @Column(name = "prenom", nullable = true, length = 20)
    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @Basic
    @Column(name = "profession", nullable = true, length = 20)
    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    @Basic
    @Column(name = "sexe", nullable = false, length = 1)
    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    @Basic
    @Column(name = "genre", nullable = false, length = 20)
    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonnagesEntity that = (PersonnagesEntity) o;
        return id == that.id &&
                Objects.equals(nom, that.nom) &&
                Objects.equals(prenom, that.prenom) &&
                Objects.equals(profession, that.profession) &&
                Objects.equals(sexe, that.sexe) &&
                Objects.equals(genre, that.genre);
    }

    @Override
    public String toString() {
        return "PersonnagesEntity{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", profession='" + profession + '\'' +
                ", sexe='" + sexe + '\'' +
                ", genre='" + genre + '\'' +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nom, prenom, profession, sexe, genre);
    }
}
