package popschool.tintinspringjpa.dao;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import popschool.tintinspringjpa.model.AlbumsEntity;
import popschool.tintinspringjpa.model.PersonnagesEntity;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface AlbumDAO extends CrudRepository<AlbumsEntity, Long>{
//@Query("select a from AlbumsEntity  a")

    List<AlbumsEntity> findAll();
    List<AlbumsEntity> findByTitre(String titre);


    Optional<AlbumsEntity>findByalbumsBySuivant_TitreLike(String titre);
    List<AlbumsEntity>findByAnneeLessThan(Integer annee);
    List<AlbumsEntity>findByAnneeBetween(Integer annee1,Integer annee2);

/*
    List<AlbumsEntity> listAlbums();

    Optional<AlbumsEntity> findById(int id);
    List<AlbumsEntity> listByTitre();
    List<AlbumsEntity> listById();

    List<AlbumsEntity> listByParution(int an1, int an2);

    List<AlbumsEntity>ListAlbumNext();

    //relation 1 n  couple objet>>>> 1 album clef lié  n personnage(s) apparessant(s) dans l'album
    Map<AlbumsEntity, List<PersonnagesEntity>> listPersonnagesOfAlbum();

    int ajoutAlbum(AlbumsEntity album);*/
}