package popschool.tintinspringjpa.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import popschool.tintinspringjpa.model.AlbumsEntity;
import popschool.tintinspringjpa.model.PersonnagesEntity;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface PersonnageDAO extends CrudRepository<PersonnagesEntity, Long>   {

    List<PersonnagesEntity> findAll();
    List<PersonnagesEntity> findByNom(String nom);
    List<PersonnagesEntity> findByPrenom(String prenom);
    @Query("select p from PersonnagesEntity p where p.prenom is null  ")
    List<PersonnagesEntity> findByNull();
    @Query("select p from PersonnagesEntity p where p.genre not like 'gentil' ")
    List<PersonnagesEntity> findByOpGenre();

    List<PersonnagesEntity> findByPrenomLike(String prenom);
    List<PersonnagesEntity> findByPrenomContaining(String prenom);

    List<PersonnagesEntity> findByNomAndPrenom(String nom,String prenom);
    List<PersonnagesEntity> findByGenreNotOrSexeLike(String genre,String sexe);



   /* List<PersonnagesEntity>listPersonnage();
    Optional<PersonnagesEntity> findById(int id);
    Optional<PersonnagesEntity> findByNomPrenom(String nom, String prenom); //args null possible
    List<PersonnagesEntity> listPersonnagesBySexe(String sexe); //référence de méthode
    List<PersonnagesEntity> listPersonnagesByJob(String job); //lamba
    List<PersonnagesEntity> listPersonnagesByGenre(String genre); //RowMapper
    //relation 1 n personnage clef
    Map<PersonnagesEntity, List<AlbumsEntity>> listAlbumsOfPersonnage();
    int ajoutPersonnage(PersonnagesEntity personnage);*/
}
