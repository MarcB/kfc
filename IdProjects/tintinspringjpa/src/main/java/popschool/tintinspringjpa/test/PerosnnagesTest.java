package popschool.tintinspringjpa.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import popschool.tintinspringjpa.dao.AlbumDAO;
import popschool.tintinspringjpa.dao.PersonnageDAO;
import popschool.tintinspringjpa.model.AlbumsEntity;
import popschool.tintinspringjpa.model.PersonnagesEntity;

import java.util.List;


@Component
public class PerosnnagesTest implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(TintinApplication.class);

    @Autowired
    private PersonnageDAO Repository;

    private void information(PersonnagesEntity p){
        log.info(p.toString());
    }

    @Override
    public void run(String... args) throws Exception {
        //fetch all albums
      /*  log.info("albums found with findAll():");
        log.info("---------------------------------------------------------------------------------------------------");
        for (PersonnagesEntity personnagesEntity : Repository.findAll()){
            log.info(personnagesEntity.toString());
        }
        log.info("");*/



        Repository.findByNom("TiNTin".toUpperCase()).forEach(System.out::println);
        Repository.findByOpGenre().forEach(System.out::println);
       // Repository.findByNull().forEach(System.out::println);
        log.info("-----------------------------DEBUT LIKE-------------------------------------------------------------");
        for (PersonnagesEntity personnagesEntity : Repository.findByPrenomContaining("al")){
            log.info(personnagesEntity.toString());
        }
        log.info("-----------------------------DEBUT nom et prenom-------------------------------------------------------------");

        Repository.findByNomAndPrenom("capone","al").forEach(System.out::println);
        log.info("-----------------------------DEBUT //gentil sexe-------------------------------------------------------------");

        Repository.findByGenreNotOrSexeLike("mechant","homme").forEach(System.out::println);


    }

}
