package popschool.tintinspringjpa.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import popschool.tintinspringjpa.dao.AlbumDAO;
import popschool.tintinspringjpa.model.AlbumsEntity;

import java.util.Optional;


@Component
public class AlbumsTest implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(TintinApplication.class);

    @Autowired
    private AlbumDAO Repository;

    private void information(AlbumsEntity a){
        log.info(a.toString());
    }

    @Override
    public void run(String... args) throws Exception {
        //fetch all albums
        log.info("albums found with findAll():");
        log.info("---------------------------------------------------------------------------------------------------");
        for (AlbumsEntity albumsEntity : Repository.findAll()){
            log.info(albumsEntity.toString());
        }
        log.info("");



       // Repository.findByTitre("Les Cigares du pharaon").forEach(System.out::println);

        log.info("-------------------------------------suite-----------------------------------------------------");
       Repository.findByalbumsBySuivant_TitreLike("Les Cigares du pharaon");
       Optional<AlbumsEntity>alb = Repository.findByalbumsBySuivant_TitreLike("Le Lotus bleu");
        log.info(alb.toString());
        log.info("------------------less than---------------");
        Repository.findByAnneeLessThan(2020).forEach(System.out::println);
        log.info("------------------between---------------");
        Repository.findByAnneeBetween(1960,2020).forEach(System.out::println);
        log.info("--------------------------------------");



    }

}
