package popschool.tintinspringjpa.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TintinApplication {

    public static void main(String[] args) {
        SpringApplication.run(TintinApplication.class, args);
    }

}
