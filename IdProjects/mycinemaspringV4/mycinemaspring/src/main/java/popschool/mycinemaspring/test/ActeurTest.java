package popschool.mycinemaspring.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import popschool.mycinemaspring.MycinemaspringApplication;
import popschool.mycinemaspring.dao.ActeurRepository;
import popschool.mycinemaspring.model.Acteur;
import popschool.mycinemaspring.model.FilmDto;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.Optional;

@Component
public class ActeurTest implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(MycinemaspringApplication.class);

    @Autowired
    private ActeurRepository acteurRepository;

    private void information(Acteur a){
        log.info(a.toString());
    }

    @Override
    public void run(String... args) throws Exception {
        /*//fetch all acteur
        log.info("Acteur found with findAll():");
        log.info("---------------------------------------------------------------------------------------------------");
        for (Acteur acteur : acteurRepository.findAll()){
            log.info(acteur.toString());
        }
        log.info("");

        // fetch an acteur by ID
        Optional<Acteur> acteur = acteurRepository.findById(1L);
        log.info("Trouver un acteur en fonction de son id findById(1L)");
        log.info("---------------------------------------------------------------------------------------------------");
        if(acteur.isPresent()) {
            Acteur act = acteur.get();
            log.info(acteur.get().toString());
            log.info("");
            //acteurRepository.findAll().forEach(this::information);
            //log.info("");
        }

        // trouver un acteur avec son nom
        Optional<Acteur> acteur2 = acteurRepository.findByNomLike("Michael");
        log.info("Trouver un acteur en fonction de son nom findByNomLike(Ford)");
        log.info("---------------------------------------------------------------------------------------------------");
        if(acteur2.isPresent()) {
            Acteur act = acteur2.get();
            log.info(acteur2.get().toString());
            log.info("");
            //acteurRepository.findAll().forEach(this::information);
            //log.info("");
        }
        else{
            log.info("Pas d'acteurs qui portent ce nom!");
        }

        //Trouver des acteur en fonction d'un prenom
        log.info("Trouver des acteurs en fonction d'un prenom :");
        log.info("---------------------------------------------------------------------------------------------------");
        for (Acteur acteur3 : acteurRepository.findByPrenomLike("Robert")){
            log.info(acteur3.toString());
        }
        log.info("");

        //Trouver les acteur nés après une date
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        String dateInString = "7-06-1950";
        try {
            Date date = formatter.parse(dateInString);
            log.info("Trouver des acteurs nés après une date :");
            log.info("---------------------------------------------------------------------------------------------------");
            for (Acteur acteur3 : acteurRepository.findByNaissanceAfter(date)){
                log.info(acteur3.toString());
            }
            log.info("");
        }catch (Exception ex){
            System.out.println(ex);
        }

        //Trouver les acteur nés avant une date
        String dateInString1 = "7-06-2013";
        try {
            Date date = formatter.parse(dateInString1);
            log.info("Trouver des acteurs nés avant une date :");
            log.info("---------------------------------------------------------------------------------------------------");
            for (Acteur acteur4 : acteurRepository.findByNaissanceBefore(date)){
                log.info(acteur4.toString());
            }
            log.info("");
        }catch (Exception ex){
            System.out.println(ex);
        }

        //trouver les acteurs ayant jouer dans plus de x films
        log.info("Trouver des acteurs ayant joué dans pllus de x films :");
        log.info("---------------------------------------------------------------------------------------------------");
        for (Acteur acteur5 : acteurRepository.findByNbrefilmsGreaterThan(20)){
            log.info(acteur5.toString());
        }
        log.info("");

        //nom contient un patttern donné
        log.info("Trouver des acteurs dont le nom contient ... :");
        log.info("---------------------------------------------------------------------------------------------------");
        for (Acteur acteur5 : acteurRepository.findByNomContaining("Br")){
            log.info(acteur5.toString());
        }
        log.info("");

        //nom contient un patttern donné
        log.info("Trouver des acteurs d'un pays' ... : findByPays_Nom permet de faire une recherche avec un join");
        log.info("---------------------------------------------------------------------------------------------------");
        for (Acteur acteur6 : acteurRepository.findByPays_Nom("USA")){
            log.info(acteur6.toString());
        }
        log.info("");


*/

        /*//trouver le nombre d'acteur par pays avec la méthode dto
        //nombre de film par genre traitement des tuple
        log.info("TROUVER le nombre d'acteur par pays :");
        log.info("---------------------------------------------------------------------------------------------------");
        for(FilmDto film : acteurRepository.findNbreActeurByPays()){
            log.info(film.toString());
        }*/

    }

}
