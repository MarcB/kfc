package popschool.mycinemaspring.dao;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import popschool.mycinemaspring.model.Acteur;
import popschool.mycinemaspring.model.Client;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public interface ClientRepository extends CrudRepository<Client, Long> {

    //Trouver un client en fonction du nom
    Optional<Client>findByNomLike(String nom);

    //Trouver tous les client ordonnés par nom
    List<Client> findAllByOrderByNom();



}
