package popschool.mycinemaspring.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import popschool.mycinemaspring.MycinemaspringApplication;
import popschool.mycinemaspring.dao.ClientRepository;
import popschool.mycinemaspring.dao.FilmRepository;
import popschool.mycinemaspring.model.Film;
import popschool.mycinemaspring.service.VideoService;

@Component
public class VideoServiceTest implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(MycinemaspringApplication.class);

    @Autowired
    private VideoService videoService;


    private void information(VideoService v) {
        log.info(v.toString());

    }


    @Override
    public void run(String... args) throws Exception {

        //info realisateur et acteur - traitement des tuples
        log.info("TROUVER les infos du realisateur et acteur d'un film demandé :");
        log.info("---------------------------------------------------------------------------------------------------");
        videoService.infoRealisateurActeur("Le Parrain").forEach(f->{
            log.info("realisateur : " + f.get("realisateur", String.class)+
                    " acteur : "+ f.get("acteurnom",String.class));
        });


    }
}
