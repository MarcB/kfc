package popschool.mycinemaspring.service;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import popschool.mycinemaspring.model.Acteur;
import popschool.mycinemaspring.model.Client;
import popschool.mycinemaspring.model.Film;

import javax.persistence.Tuple;
import java.util.List;
import java.util.Optional;


public interface VideoService{
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                          EMPRUNT                                                           //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    void emprunter(String nom, String titre);
    void retourEmprunt(String nom, String titre);

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                           CLIENT                                                           //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    List<Client> ensClient();

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                         FILM                                                               //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    List<Film> ensFilmEmpruntable();
    List<Film> ensFilm();
    List<Film> ensFilmGenre(String genre);
    List<Tuple>infoRealisateurActeur(String titre);
    int nbrFilmDuGenre(String genre);
    List<Film> ensFilmEmpruntesByClient(Optional<Client> client);

}
