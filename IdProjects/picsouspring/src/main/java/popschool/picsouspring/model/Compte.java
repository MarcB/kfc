package popschool.picsouspring.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "COMPTE", schema = "Picsou", catalog = "")
public class Compte {
    private int id;
    private String numero;
    private int titulaire;
    private Date dateOuverture;
    private Double plafond;
    private String bloque;
    private double solde;
    private Collection<Carte> cartesById;
    private Client clientByTitulaire;
    private Collection<Mouvement> mouvementsById;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "numero", nullable = false, length = 10)
    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

//    @Basic
//    @Column(name = "titulaire", nullable = false)
//    public int getTitulaire() {
//        return titulaire;
//    }

    public void setTitulaire(int titulaire) {
        this.titulaire = titulaire;
    }

    @Basic
    @Column(name = "date_ouverture", nullable = false)
    public Date getDateOuverture() {
        return dateOuverture;
    }

    public void setDateOuverture(Date dateOuverture) {
        this.dateOuverture = dateOuverture;
    }

    @Basic
    @Column(name = "plafond", nullable = true, precision = 0)
    public Double getPlafond() {
        return plafond;
    }

    public void setPlafond(Double plafond) {
        this.plafond = plafond;
    }

    @Basic
    @Column(name = "bloque", nullable = true, length = 3)
    public String getBloque() {
        return bloque;
    }

    public void setBloque(String bloque) {
        this.bloque = bloque;
    }

    @Basic
    @Column(name = "solde", nullable = false, precision = 0)
    public double getSolde() {
        return solde;
    }

    public void setSolde(double solde) {
        this.solde = solde;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Compte that = (Compte) o;
        return id == that.id &&
                titulaire == that.titulaire &&
                Double.compare(that.solde, solde) == 0 &&
                Objects.equals(numero, that.numero) &&
                Objects.equals(dateOuverture, that.dateOuverture) &&
                Objects.equals(plafond, that.plafond) &&
                Objects.equals(bloque, that.bloque);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, numero, titulaire, dateOuverture, plafond, bloque, solde);
    }

    @OneToMany(mappedBy = "compteByIdCompte")
    public Collection<Carte> getCartesById() {
        return cartesById;
    }

    public void setCartesById(Collection<Carte> cartesById) {
        this.cartesById = cartesById;
    }

    @ManyToOne
    @JoinColumn(name = "titulaire", referencedColumnName = "id", nullable = false)
    public Client getClientByTitulaire() {
        return clientByTitulaire;
    }

    public void setClientByTitulaire(Client clientByTitulaire) {
        this.clientByTitulaire = clientByTitulaire;
    }

    @OneToMany(mappedBy = "compteByIdCompte")
    public Collection<Mouvement> getMouvementsById() {
        return mouvementsById;
    }

    public void setMouvementsById(Collection<Mouvement> mouvementsById) {
        this.mouvementsById = mouvementsById;
    }
}
