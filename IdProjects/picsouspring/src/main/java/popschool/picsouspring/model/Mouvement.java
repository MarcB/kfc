package popschool.picsouspring.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "MOUVEMENT", schema = "Picsou", catalog = "")
public class Mouvement {
    private int id;
    private int idCompte;
    private Timestamp dateOperation;
    private double montant;
    private String nature;
    private Compte compteByIdCompte;
    private Collection<Retrait> retraitsById;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

//    @Basic
//    @Column(name = "id_compte", nullable = false)
//    public int getIdCompte() {
//        return idCompte;
//    }

    public void setIdCompte(int idCompte) {
        this.idCompte = idCompte;
    }

    @Basic
    @Column(name = "date_operation", nullable = false)
    public Timestamp getDateOperation() {
        return dateOperation;
    }

    public void setDateOperation(Timestamp dateOperation) {
        this.dateOperation = dateOperation;
    }

    @Basic
    @Column(name = "montant", nullable = false, precision = 0)
    public double getMontant() {
        return montant;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }

    @Basic
    @Column(name = "nature", nullable = true, length = 2)
    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Mouvement that = (Mouvement) o;
        return id == that.id &&
                idCompte == that.idCompte &&
                Double.compare(that.montant, montant) == 0 &&
                Objects.equals(dateOperation, that.dateOperation) &&
                Objects.equals(nature, that.nature);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, idCompte, dateOperation, montant, nature);
    }

    @ManyToOne
    @JoinColumn(name = "id_compte", referencedColumnName = "id", nullable = false)
    public Compte getCompteByIdCompte() {
        return compteByIdCompte;
    }

    public void setCompteByIdCompte(Compte compteByIdCompte) {
        this.compteByIdCompte = compteByIdCompte;
    }

    @OneToMany(mappedBy = "mouvementByIdMouvement")
    public Collection<Retrait> getRetraitsById() {
        return retraitsById;
    }

    public void setRetraitsById(Collection<Retrait> retraitsById) {
        this.retraitsById = retraitsById;
    }
}
