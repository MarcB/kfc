package popschool.picsouspring.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "RETRAIT", schema = "Picsou", catalog = "")
public class Retrait {
    private int id;
    private int idCarte;
    private Timestamp dateRetrait;
    private String adresseGab;
    private double montant;
    private String etat;
    private Integer idMouvement;
    private Carte carteByIdCarte;
    private Mouvement mouvementByIdMouvement;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

//    @Basic
//    @Column(name = "id_carte", nullable = false )
//    public int getIdCarte() {
//        return idCarte;
//    }

    public void setIdCarte(int idCarte) {
        this.idCarte = idCarte;
    }

    @Basic
    @Column(name = "date_retrait", nullable = false)
    public Timestamp getDateRetrait() {
        return dateRetrait;
    }

    public void setDateRetrait(Timestamp dateRetrait) {
        this.dateRetrait = dateRetrait;
    }

    @Basic
    @Column(name = "adresseGAB", nullable = false, length = 20)
    public String getAdresseGab() {
        return adresseGab;
    }

    public void setAdresseGab(String adresseGab) {
        this.adresseGab = adresseGab;
    }

    @Basic
    @Column(name = "montant", nullable = false, precision = 0)
    public double getMontant() {
        return montant;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }

    @Basic
    @Column(name = "etat", nullable = false, length = 10)
    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

//    @Basic
//    @Column(name = "id_mouvement", nullable = true)
//    public Integer getIdMouvement() {
//        return idMouvement;
//    }

    public void setIdMouvement(Integer idMouvement) {
        this.idMouvement = idMouvement;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Retrait that = (Retrait) o;
        return id == that.id &&
                idCarte == that.idCarte &&
                Double.compare(that.montant, montant) == 0 &&
                Objects.equals(dateRetrait, that.dateRetrait) &&
                Objects.equals(adresseGab, that.adresseGab) &&
                Objects.equals(etat, that.etat) &&
                Objects.equals(idMouvement, that.idMouvement);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, idCarte, dateRetrait, adresseGab, montant, etat, idMouvement);
    }

    @ManyToOne
    @JoinColumn(name = "id_carte", referencedColumnName = "id", nullable = false)
    public Carte getCarteByIdCarte() {
        return carteByIdCarte;
    }

    public void setCarteByIdCarte(Carte carteByIdCarte) {
        this.carteByIdCarte = carteByIdCarte;
    }

    @ManyToOne
    @JoinColumn(name = "id_mouvement", referencedColumnName = "id")
    public Mouvement getMouvementByIdMouvement() {
        return mouvementByIdMouvement;
    }

    public void setMouvementByIdMouvement(Mouvement mouvementByIdMouvement) {
        this.mouvementByIdMouvement = mouvementByIdMouvement;
    }
}
