package popschool.picsouspring.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "CARTE", schema = "Picsou", catalog = "")
public class Carte {
    private int id;
    private String numero;
    private int idCompte;
    private Date dateExpiration;
    private String codesecret;
    private String bloque;
    private Integer nbEssai;
    private Compte compteByIdCompte;
    private Collection<Retrait> retraitsById;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "numero", nullable = false, length = 40)
    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

//    @Basic
//    @Column(name = "id_compte", nullable = false)
//    public int getIdCompte() {
//        return idCompte;
//    }

    public void setIdCompte(int idCompte) {
        this.idCompte = idCompte;
    }

    @Basic
    @Column(name = "date_expiration", nullable = false)
    public Date getDateExpiration() {
        return dateExpiration;
    }

    public void setDateExpiration(Date dateExpiration) {
        this.dateExpiration = dateExpiration;
    }

    @Basic
    @Column(name = "codesecret", nullable = false, length = 4)
    public String getCodesecret() {
        return codesecret;
    }

    public void setCodesecret(String codesecret) {
        this.codesecret = codesecret;
    }

    @Basic
    @Column(name = "bloque", nullable = true, length = 3)
    public String getBloque() {
        return bloque;
    }

    public void setBloque(String bloque) {
        this.bloque = bloque;
    }

    @Basic
    @Column(name = "nb_essai", nullable = true)
    public Integer getNbEssai() {
        return nbEssai;
    }

    public void setNbEssai(Integer nbEssai) {
        this.nbEssai = nbEssai;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Carte that = (Carte) o;
        return id == that.id &&
                idCompte == that.idCompte &&
                Objects.equals(numero, that.numero) &&
                Objects.equals(dateExpiration, that.dateExpiration) &&
                Objects.equals(codesecret, that.codesecret) &&
                Objects.equals(bloque, that.bloque) &&
                Objects.equals(nbEssai, that.nbEssai);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, numero, idCompte, dateExpiration, codesecret, bloque, nbEssai);
    }

    @ManyToOne
    @JoinColumn(name = "id_compte", referencedColumnName = "id", nullable = false)
    public Compte getCompteByIdCompte() {
        return compteByIdCompte;
    }

    public void setCompteByIdCompte(Compte compteByIdCompte) {
        this.compteByIdCompte = compteByIdCompte;
    }

    @OneToMany(mappedBy = "carteByIdCarte")
    public Collection<Retrait> getRetraitsById() {
        return retraitsById;
    }

    public void setRetraitsById(Collection<Retrait> retraitsById) {
        this.retraitsById = retraitsById;
    }
}
