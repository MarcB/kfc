package popschool.picsouspring.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import popschool.picsouspring.dao.*;

@Controller
public class MainController {

    //bean liaison
    @Autowired
    private CarteRepository carteRepository;
    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private CompteRepository compteRepository;
    @Autowired
    private MouvementRepository mouvementRepository;
    @Autowired
    private RetraitRepository retraitRepository;

    //index
    @Value("${welcome.message}")
    private String message;
    //index >>  page home =====================================================
    @RequestMapping(value = { "/", "/index" }, method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("message", message);
        return "index";
    }

}
