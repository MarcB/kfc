package popschool.picsouspring.dao;

import org.springframework.data.repository.CrudRepository;
import popschool.picsouspring.model.Client;

public interface ClientRepository extends CrudRepository<Client,Integer> {


}
