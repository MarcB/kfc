package popschool.picsouspring.dao;

import org.springframework.data.repository.CrudRepository;
import popschool.picsouspring.model.Mouvement;

import java.util.List;
import java.util.Optional;

public interface MouvementRepository extends CrudRepository<Mouvement,Integer> {
    Optional<Mouvement> findMouvementByCompteByIdCompte(int idCompte);

}
