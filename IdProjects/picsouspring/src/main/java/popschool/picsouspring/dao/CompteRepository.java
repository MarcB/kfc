package popschool.picsouspring.dao;

import org.springframework.data.repository.CrudRepository;
import popschool.picsouspring.model.Compte;

import java.util.List;
import java.util.Optional;

public interface CompteRepository extends CrudRepository<Compte,Integer> {
    List<Compte>findCompteByClientByTitulaire(int idClient);
    Optional<Compte> findCompteById(int idCompte);

    //info sur le compte
  //  int credit(int idCompte, double montant);
   // int debit(int idCompte, double montant);
  //  int transfert(int idOrigine, int idDestination, double montant);

}
