package popschool.picsouspring.dao;

import org.springframework.data.repository.CrudRepository;
import popschool.picsouspring.model.Carte;

public interface CarteRepository extends CrudRepository<Carte,Integer> {



}
