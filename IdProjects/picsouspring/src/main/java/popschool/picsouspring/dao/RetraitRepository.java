package popschool.picsouspring.dao;

import org.springframework.data.repository.CrudRepository;
import popschool.picsouspring.model.Retrait;

import java.util.List;
import java.util.Optional;

public interface RetraitRepository extends CrudRepository<Retrait,Integer> {
  //  int retrait(int idCarte, String code, String adrGAB, double montant);
    Optional<Retrait> findRetraitByCarteByIdCarte(int idCarte);

}
