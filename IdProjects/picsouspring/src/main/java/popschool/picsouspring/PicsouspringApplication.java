package popschool.picsouspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PicsouspringApplication {

    public static void main(String[] args) {
        SpringApplication.run(PicsouspringApplication.class, args);
    }

}
