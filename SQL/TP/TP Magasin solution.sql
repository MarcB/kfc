-- Q1 Rechercher tous les articles dont le poids est inf�rieur
--    au poids de l'article num�ro '2' (requ�te imbriqu�e)
select art_num, art_poids
from article
where art_poids < (
    select art_poids
	from article
	where art_num='A02')
order by 2;

-- Q2
select b.art_num, b.art_poids
from article a
join article b on a.art_poids > b.art_poids
where a.art_num='A02'
order by b.art_poids;


-- Q3 Rechercher dans la table article, les tuples de m�me couleur
--    que l'article '10', et dont le poids est sup�rieur ou �gal 
--    au poids moyen de tous les articles
select art_num, art_nom
from article
where art_coul =
  (select art_coul
   from article
   where art_num = 'A10')
and art_poids >=
  (select avg(art_poids)
   from   article);
   
-- Q4 Donner la liste des fournisseurs qui vendent
--    au moins un article de couleur 'NOIR'   
select frs_nom
from fournisseur
where frs_num in
  (select art_frs
   from article
   where art_coul = 'NOIR');

-- Q5 Donner le nom des g�rants des magasins 
--    qui ont vendu au moins un article '2'  
select distinct mag_num, mag_ger
from magasin m
where 'A02' in
    (select lcd_art
	 from lig_cmd join commande
	 on cmd_num=lcd_cmd
	 where cmd_mag = m.mag_num );

-- Q6 Donner la liste des articles dont le prix de vente
--    est sup�rieur au prix de vente de l'article de couleur
--    blanche le moins cher (utiliser ANY)	 
select art_nom
from article
where art_pv > any
  (select art_pv
   from article
   where art_coul = 'BLANC');
 
-- Q7 Donner la liste des articles dont le prix de vente
--    est sup�rieur au prix de vente de l'article de couleur
--    blanche le moins cher (utiliser MIN)	 
select art_nom
from article
where art_pv >
  (select MIN(art_pv)
   from article
   where art_coul = 'BLANC');
   
-- Q8 Donner la liste des articles dont le prix de vente
--    est sup�rieur au prix de vente de l'article de couleur
--    blanche le moins cher (EXISTS)	 
select art_nom
from article a
where exists
  (select * 
   from article b
   where b.art_coul = 'BLANC'
   AND   a.art_pv > b.art_pv);

   
-- Q9 Donner la liste des clients qui ont achet� plus
--    d'une fois sur la semaine du 6 au 10 juin 1994   
select clt_num, clt_nom
from client 
where 1< (select count(*)
          from  commande
	      where cmd_date between '1994-06-06' and '1994-06-10'
	            and cmd_clt = clt_num);
	  
-- Q10 Donner la liste des articles dont la somme des ventes 
--     est sup�rieure � la moyenne de la somme des ventes
--     de tous les articles
select lcd_art, sum(lcd_pu * lcd_qte) as somme
from lig_cmd
group by lcd_art
having sum(lcd_pu * lcd_qte) >  
       (select avg(lcd_pu*lcd_qte)
	    from lig_cmd
	   );
	
-- Q11 Donner le num�ro et le nom des clients 
--     ayant achet� un article
--     achet� par un des clients 
--     qui ont achet� un article de couleur rouge	
select distinct clt_num, clt_nom
from client, commande, lig_cmd
where clt_num = cmd_clt
and cmd_num = lcd_cmd
and lcd_art in
  (select lcd_art
   from lig_cmd, commande
   where lcd_cmd = cmd_num
   and cmd_clt in
      (select cmd_clt
	   from commande, lig_cmd, article
	   where cmd_num = lcd_cmd
	   and lcd_art = art_num
	   and art_coul='ROUGE'));
	   
-- Q12 Donner le num�ro et la date d'achat des clients 
--     n'ayant achet� qu'une fois
select cmd_clt, 
       cmd_date as "Date d'achat"
from commande
where cmd_clt in
	 (select cmd_clt
	  from commande
	  group by cmd_clt
	  having count(*) = 1)
order by cmd_clt;
 
 
select cmd_clt, max(cmd_date) as date_achat
from commande
group by cmd_clt
having count(*)=1;

-- Q13 Donner le num�ro et le nom des clients qui
--     ont achet� dans au moins deux magasins diff�rents
select clt_num, clt_nom
from client
where clt_num in
  (select cmd_clt
   from commande
   group by cmd_clt
   having count(distinct cmd_mag) > 1);

-- Q14 Donner le num�ro des articles dont la moyenne des quantit�s
--     vendus en une fois est sup�rieure � la moyenne des quantit�s
--     vendus en une fois tous articles confondus   
select lcd_art
from lig_cmd
group by lcd_art
having avg(lcd_qte) >
     (select avg(lcd_qte)
	  from lig_cmd);
	  

 

-- 
-- Q15 liste des clients qui ont achete un article ROUGE
--
select clt_num, clt_nom, clt_loc
from client
where exists
    (select *
	 from commande, lig_cmd, article
	 where cmd_num =lcd_cmd
	 and   lcd_art = art_num
	 and   cmd_clt = clt_num
	 and   art_coul = 'ROUGE');
	 
-- Q15 op�rateur EXISTENTIELLE (corr�lation)
select clt_num, clt_nom, clt_loc
from client
where exists
    (select *
	 from commande join lig_cmd on cmd_num=lcd_cmd
	      join  article on lcd_art = art_num
	 where cmd_clt = clt_num
	 and   art_coul = 'ROUGE');
	 
-- Q15 Jointure des 4 relations PUIS restriction
select distinct clt_num, clt_nom,clt_loc
from commande join lig_cmd on cmd_num=lcd_cmd
	 join  article on lcd_art = art_num
	 join client on cmd_clt = clt_num
where art_coul='ROUGE';

--
-- Q16 Liste des clients qui n'ont pas achet� � Paris
--
select clt_num, clt_nom, clt_loc
from client
where not exists
    (select *
	 from commande , magasin
	 where cmd_mag = mag_num
	 and   cmd_clt = clt_num
	 and   mag_loc like 'Paris%');

select clt_num, clt_nom, clt_loc
from client
where clt_num not in
    (select cmd_clt
	 from commande , magasin
	 where cmd_mag = mag_num
	 and   mag_loc like 'Paris%');
	
--
-- Q17 Liste des articles qui ont �t� vendus dans tous les magasins
--     quantificateur U N I  V E R S E L     (absent de SQL v3)
select art_num, art_nom
from article
where not exists
  (select *
   from magasin
   where mag_loc='Paris%'
        and not exists
	        (select *
			 from commande, lig_cmd
			 where cmd_num = lcd_cmd
			 and lcd_art = art_num
			 and cmd_mag = mag_num));

--
-- Q18 liste des clients qui n'achetent qu'au magasin de Lyon
--     operateur d'U N I C I T E     (absent de SQL v3)
select cmd_clt
from commande  C1
where not exists
    (select *
	 from commande  C2
	 where C1.cmd_clt = C2.cmd_clt
	 and cmd_mag in
	     (select mag_num
		  from magasin
		  where mag_loc <> 'Lyon'));


--
-- Q18 bis Pourquoi cette solution est-elle incorrecte et
--     corrigez la ?
--
select clt_num
from client
where
  (select count(*)
   from commande
   where cmd_clt = clt_num)
   =
  (select count(*)
   from commande
   where cmd_clt = clt_num
   and cmd_mag =
       (select mag_num
	    from magasin
		where mag_loc = 'Lyon'));
		 		  		 	 
select clt_num
from client
where
  (select count(*)
   from commande
   where cmd_clt = clt_num
   having count(*)>0)
   =
  (select count(*)
   from commande
   where cmd_clt = clt_num
   and cmd_mag =
       (select mag_num
	    from magasin
		where mag_loc = 'Lyon'));
		 		  		 	 							 
-- Q19 Donner la liste des clients qui ont achet� au moins
--     tous les articles achet�s par le client 5	
select clt_num, clt_nom, clt_loc
from client
where clt_num <> 'C05'
and not exists
   (select lcd_art
    from commande c1, lig_cmd  l1
	where cmd_num=lcd_cmd
	and cmd_clt='C05'
	and not exists
	    (select * 
		 from commande c2, lig_cmd l2
		 where c2.cmd_num = l2.lcd_cmd
		 and l2.lcd_art = l1.lcd_art
		 and c2.cmd_clt = clt_num));


-- Q20 Dresser la liste des articles vendus au client
--     qui a le plus gros chiffre d'affaires 
--     dans tous les magasins de Paris	
select distinct lcd_art
from lig_cmd, commande
where lcd_cmd = cmd_num
and cmd_clt IN
   (select cl
    from (select cmd_clt as cl, sum(lcd_qte*lcd_pu) as ca
	      from commande, lig_cmd,magasin
		  where cmd_num=lcd_cmd
		  and cmd_mag=mag_num
		  and mag_loc like 'Paris%'
		  group by cmd_clt) A
    where ca =
	    (select MAX(ca)
		 from 	(select sum(lcd_qte*lcd_pu) as ca
			      from commande, lig_cmd,magasin
				  where cmd_num=lcd_cmd
				  and cmd_mag=mag_num
				  and mag_loc like 'Paris%'
				  group by cmd_clt) B )
	)	;		
	
											
-- ------------------------------
select extract(week from cmd_date) as semaine, 
       count(*) as nombre_ventes
from commande
group by semaine


--
-- Q21 Construire une vue (clients_paris )qui donne � l'utilisateur l'acc�s aux seuls clients de PARIS	
--
create or replace view CLIENTS_PARIS( NUM, NOM, PRENOM, PAYS,LOC, CA, TYPE)
as select CLT_NUM, CLT_NOM, CLT_PRENOM, CLT_PAYS, CLT_LOC, CLT_CA, CLT_TYPE
   from   CLIENT
   where  CLT_LOC = 'Paris';

--
-- Q22 Construire une vue (ca_clients ) qui donne la r�f�rence du client et son chiffre d'affaire	
--
create or replace view CA_CLIENTS( NUM, CA )
  as select CLT_NUM, CLT_CA
     from   CLIENT;

--
-- Q23 Construire une vue (commandes_articles ) qui r�alise 
--     un r�sum� des commandes d'une semaine par article. 
--     On retiendra le nombre de commandes et le nombre d'articles vendus.
--
create or replace view COMMANDES_ARTICLES( semaine, 
	                                       lcd_art, NB_ART , QTE)
       as select extract(week from cmd_date) as semaine, 
                 lcd_art ,count(lcd_cmd), sum(lcd_qte)
       from   COMMANDE A
         join LIG_CMD B
         on   A.CMD_NUM = B.LCD_CMD
       group by semaine, lcd_art
       order by semaine, lcd_art;

--
-- Q24 Construire une vue (ventes_en_clair ) qui montre tr�s clairement 
--     toute information sur les ventes d'une semaine dans les magasins
--     de PARIS. On extrait les donn�es des tables client, commande, 
--     magasin et lig_cmd pour remplacer les r�f�rences par des donn�es 
--     plus "parlantes".
--

create or replace view VENTES_EN_CLAIR(semaine,  MAG_NUM, MAG_GER, CMD_NUM, CMD_DATE, CLT_NOM,CLT_LOC, CLT_TYPE, CA	)
	as select extract(week from cmd_date) as semaine,A.MAG_NUM, A.MAG_GER, B.CMD_NUM, B.CMD_DATE, C.CLT_NOM, C.CLT_LOC, C.CLT_TYPE, sum(D.LCD_QTE*D.LCD_PU)
	   from   MAGASIN A
	   join   COMMANDE B
	     on     A.MAG_NUM = B.CMD_MAG
	   join   CLIENT C
	     on   B.CMD_CLT = C.CLT_NUM
	   join   LIG_CMD D
    	 on   D.LCD_CMD = B.CMD_NUM
	   group by semaine, A.MAG_NUM, A.MAG_GER, B.CMD_NUM, B.CMD_DATE, C.CLT_NOM, C.CLT_LOC, C.CLT_TYPE
	   order by semaine, A.MAG_NUM, A.MAG_GER, B.CMD_NUM;
	
--
-- Q25 Construire une vue (ventes_importantes ) qui affiche le montant 
--     et le lieu de chaque vente importante (>100) dans les magasins 
--     de Paris sur une semaine.; la construction se fera � partir de 
--     la vue ventes_en_clair.


create or replace view VENTES_IMPORTANTES( MAG_NUM, MAG_GER, CMD_NUM,
                                           CMD_DATE, CLT_NOM, CLT_LOC, CLT_TYPE, CA)
    as select MAG_NUM, MAG_GER, CMD_NUM, CMD_DATE, CLT_NOM, CLT_LOC, CLT_TYPE, CA
       from   VENTES_EN_CLAIR
       where CA > 100
       order by MAG_NUM, MAG_GER, CMD_NUM;



  