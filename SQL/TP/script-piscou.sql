


-- creation des tables
drop table if exists retrait;
drop table if exists mouvement;
drop table if exists carte;
drop table if exists compte;
drop table if exists client;


create table client(
id int not null auto_increment,
nom    varchar(20) not null,
prenom  varchar(20) not null,
adresse varchar(20) not null,
constraint uk_client_nom_prenom unique(nom,prenom),
constraint PK_client primary key (id)
)engine=INNODB auto_increment=3;

lock tables client write;
insert into client values
(1,'GATOR','NAT','LILLE'),
(2,'AFFREUX','PIERRE','LENS');
unlock tables;

create table compte(
id int not null auto_increment,
numero varchar(10) not null,
titulaire int not null,
date_ouverture date not null,
plafond real default 0.0 ,
bloque char(3) default 'non',
solde real  not null,

constraint PK_id primary key (id),
constraint uk_compte unique(numero),
constraint FK_titulaire foreign key (titulaire) references client(id) 
)engine=INNODB auto_increment=5;

lock tables compte write;
insert into compte values
(1,'1234',1,current_date,10000.0,'non',200.0),
(2,'1234',1,current_date,0.0,'non',100.0),
(3,'1234',2,current_date,15000.0,'non',300.0),
(4,'1234',1,current_date,0.0,'non',100.0);
unlock tables;

create table carte(
id int(20) not null auto_increment,
numero varchar(10) not null,
idcompte int not null,
codesecret char(4) not null,
nbEssai int(1) not null,
bloque char(3)default 'non',
date_expiration date not null,
constraint uk_carte_numero unique(numero),
constraint FK_idcompte foreign key (idcompte) references compte(id), 
constraint PK_id primary key (id))engine=INNODB auto_increment=3;

lock tables carte write;
insert into carte values
(1,'gatorCD01',1,'2021-05-07','1234','non',0),
(2,'afreuxCD01',3,'2019-05-07','5678','non',0);
unlock tables;

create table mouvement(
id int not null auto_increment,
idcompte int not null,
date_operation timestamp not null ,
montant real not null,
nature char(2) not null,
constraint FK_idcompte foreign key (idcompte) references compte(id), 
constraint PK_id primary key (id))engine=INNODB auto_increment=4;


lock tables mouvement write;
insert into mouvement values
(1,1,current_timestamp,60.0,'CB'),
(2,3,current_timestamp,60.0,'DB'),
(3,3,current_timestamp,100.0,'CB');
unlock tables;


create table retrait(
id int not null auto_increment,
idcarte int not null,
date_retrait timestamp not null,
adresseGAB varchar(20) not null, 
montant real not null,
etat varchar(10) not null,
idMouvement int,
constraint pk_retrait primary key (id),
constraint fk_retrait_idcarte foreign key(idcarte) references carte(id),
constraint fk_retrait_idmvt foreign key (idMouvement) references mouvement(id)
)engine=InnoDB auto_increment=2;

lock tables retrait write;
insert into retrait values
(1,2,current_timestamp,'BP LILLE',100.0,'SUCCES',3);
unlock tables;



