﻿drop table  if exists lig_liv ;
drop table  if exists livraison;
drop table  if exists lig_cmd;
drop table  if exists commande;
drop table  if exists article;
drop table  if exists  fournisseur;
drop table  if exists client;
drop table  if exists  magasin;

CREATE TABLE magasin
( mag_num    varchar(8) ,
  mag_loc    varchar(25) NOT NULL,
  mag_ger    varchar(25) NOT NULL,
  constraint PK_MAGASIN primary key(mag_num))ENGINE=InnoDB;

  

CREATE TABLE client
( clt_num    varchar(8) ,
  clt_nom    varchar(25) NOT NULL,
  clt_prenom varchar(20),
  clt_pays   varchar(2) NOT NULL,
  clt_loc    varchar(20) NOT NULL,
  clt_ca  integer,
  clt_type   varchar(16),
  constraint PK_CLIENT primary key(clt_num))ENGINE=InnoDB;
  

CREATE TABLE fournisseur
( frs_num    varchar(8) ,
  frs_nom    varchar(25) NOT NULL ,
                          constraint UK_FOURNISSEUR_NOM UNIQUE(frs_nom),
 constraint PK_FOURNISSEUR primary key(frs_num))ENGINE=InnoDB;
  

CREATE TABLE article
(  art_num   varchar(8),
   art_nom   varchar(25) NOT NULL,
   art_coul  varchar(20),
   art_poids numeric(10,3),
   art_stock integer,
   art_pa    integer NOT NULL,
   art_pv    integer NOT NULL,
   art_frs   varchar(8) ,
   CONSTRAINT FK_ARTICLE_FRS foreign key(art_frs) references fournisseur(frs_num)  ,
   CONSTRAINT PK_ARTICLE primary key(art_num))ENGINE=InnoDB;

   

CREATE TABLE commande
(  cmd_num   varchar(8) ,
   cmd_date  date  NOT NULL,
   cmd_clt   varchar(8) NOT NULL,
   cmd_mag   varchar(8) NOT NULL,
  CONSTRAINT FK_COMMANDE_MAG foreign key(cmd_mag) references magasin(mag_num)  ,
  CONSTRAINT FK_COMMANDE_CLT foreign key(cmd_clt) references client (clt_num),
  CONSTRAINT PK_COMMANDE primary key(cmd_num))ENGINE=InnoDB;

CREATE TABLE lig_cmd
(  lcd_cmd   varchar(8) NOT NULL,
   lcd_art   varchar(8) NOT NULL,
   lcd_qte   integer NOT NULL,
   lcd_liv   integer DEFAULT 0,
   lcd_pu    integer NOT NULL,
   lcd_datliv date,
   constraint pk_lig_cmd primary key (lcd_cmd, lcd_art),
   constraint fk_lig_cmd_cmd foreign key (lcd_cmd) references commande(cmd_num),
   constraint fk_lig_cmd_art foreign key (lcd_art) references article(art_num) )ENGINE=InnoDB;
   
CREATE TABLE livraison
(  liv_num   varchar(8) ,
   liv_date  DATE,
   liv_clt   varchar(8) ,
   liv_mag   varchar(8) ,
   CONSTRAINT FK_LIVRAISON_MAG foreign key(liv_mag) references magasin(mag_num) ,
   CONSTRAINT FK_LIVRAISON_CLT foreign key(liv_clt) references client(clt_num),
   CONSTRAINT PK_LIVRAISON primary key(liv_num))ENGINE=InnoDB;

CREATE TABLE lig_liv
(  llv_liv   varchar(8) NOT NULL,
   llv_art   varchar(8) NOT NULL,
   llv_qte   INTEGER     NOT NULL,
   llv_cmd   varchar(8) NOT NULL,
   constraint PK_lig_liv PRIMARY KEY(llv_liv, llv_art),
   constraint FK_LIG_LIV_LIV foreign key (llv_liv)references livraison( liv_num ),
   constraint FK_LIG_LIV_ART foreign key (llv_ART) references article( art_num ),
   constraint FK_LIG_LIV_CMD_ART foreign key (llv_cmd, llv_art) references lig_cmd(lcd_cmd, lcd_art)
)ENGINE=InnoDB;