/*Rechercher tous les articles dont le poids est inférieur au poids de l'article numéro '2' (requête imbriquée)*/
SELECT art_nom FROM article WHERE art_poids >(select art_poids from article where art_num ="A02");

/*Même question en utilisant une auto-jointure (a02)*/
SELECT a.art_nom FROM article a
LEFT OUTER JOIN article b ON a.art_poids =b.art_poids 
where (a.art_poids>150);

/*Rechercher dans la table article, les tuples de même couleur que l'article '10', et dont le poids 
 * est supérieur ou égal au poids moyen de tous les articles*/
SELECT art_nom FROM article WHERE (art_coul >(SELECT art_coul FROM article WHERE art_num ="A10")) AND
	 (art_poids >(SELECT AVG(art_poids) FROM article) );

/*Donner la liste des fournisseurs qui vendent au moins un article de couleur 'NOIR'*/
	SELECT frs_nom FROM fournisseur f 
		LEFT OUTER JOIN article a ON a.art_frs=f.frs_num 
			WHERE art_coul="NOIR" ;
	
/*Donner le nom des gérants des magasins qui ont vendu au moins un article 'a02'*/
	SELECT mag_ger FROM magasin mag
				LEFT OUTER JOIN livraison liv ON mag.mag_num=liv.liv_mag 
				LEFT OUTER JOIN lig_liv lig ON liv.liv_num = lig.llv_liv 
					WHERE llv_art="A02" ;
				
/*Donner la liste des articles dont le prix de vente est supérieur au prix de vente
 de l'article de couleur blanche le moins cher (utiliser ANY)	*/			
	SELECT art_nom,art_pv FROM article 
					WHERE art_pv>(SELECT MIN( art_pv) FROM article where art_coul LIKE "BLANC");
					WHERE art_pv > ANY ( SELECT art_pv FROM article where art_coul LIKE "BLANC" );
/*Même question avec une requête existentielle*/			
	SELECT art_nom,art_pv FROM article a
			WHERE  EXISTS (SELECT * FROM article b WHERE b.art_coul = "Blanc" AND a.art_pv > b.art_pv);
	
	
/*Donner la liste des clients qui ont acheté plus d'une fois sur la semaine du 6 au 10 juin 1994*/
SELECT clt_nom,clt_num FROM client cl
		LEFT OUTER JOIN commande c ON c.cmd_clt=cl.clt_num
	WHERE (cmd_date BETWEEN "1994-06-06" AND "1994-06-10") AND
		ORDER BY clt_nom ASC ;




