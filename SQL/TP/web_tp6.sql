-- creation des tables
drop table if exists CUSTOMERS;
drop table if exists SUPP_REQUESTS;

create table CUSTOMERS(
  email varchar(40) not null,
  fname varchar(15) not null,
  lname varchar(15) not null,
  phone varchar(12) not null,
  constraint PK_CUSTOMERS primary key (email)
)engine=INNODB;

create table SUPP_REQUESTS(
  request_id INT(11) NOT NULL AUTO_INCREMENT,
  email varchar(40) not null,
  software varchar(40) not null,
  os varchar(40) not null,
  problem varchar(256) not null,
  constraint PK_GENRE primary key (request_id)
)engine=INNODB;
