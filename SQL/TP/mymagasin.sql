﻿INSERT INTO magasin ( mag_num, mag_loc, mag_ger ) VALUES (
'M01', 'Paris 5', 'BERTON Louis'); 
INSERT INTO magasin ( mag_num, mag_loc, mag_ger ) VALUES ( 
'M02', 'Paris 10', 'JANNEAU Luc'); 
INSERT INTO magasin ( mag_num, mag_loc, mag_ger ) VALUES ( 
'M03', 'Lyon', 'MOUILLARD Marcel'); 
INSERT INTO magasin ( mag_num, mag_loc, mag_ger ) VALUES ( 
'M04', 'Marseille', 'CAMUS Marius'); 
INSERT INTO magasin ( mag_num, mag_loc, mag_ger ) VALUES ( 
'M05', 'Montpellier', 'BAIJOT Marc'); 
INSERT INTO magasin ( mag_num, mag_loc, mag_ger ) VALUES ( 
'M06', 'Bordeaux', 'DETIENNE Nicole'); 
INSERT INTO magasin ( mag_num, mag_loc, mag_ger ) VALUES ( 
'M07', 'Nantes', 'DUMONT Henri'); 
INSERT INTO magasin ( mag_num, mag_loc, mag_ger ) VALUES ( 
'M08', 'Tours', 'DEMARTEAU Renee'); 
INSERT INTO magasin ( mag_num, mag_loc, mag_ger ) VALUES ( 
'M09', 'Rouen', 'NOSSENT Daniel'); 
INSERT INTO magasin ( mag_num, mag_loc, mag_ger ) VALUES ( 
'M10', 'Lille', 'NIZET Jean-Luc'); 
INSERT INTO magasin ( mag_num, mag_loc, mag_ger ) VALUES ( 
'M11', 'Bruxelles', 'VANDERSMISSEN Ferand'); 
INSERT INTO magasin ( mag_num, mag_loc, mag_ger ) VALUES ( 
'M12', 'Liege', 'HANNEAU Vincent'); 
commit;

--
-- insertion table CLIENT
--
INSERT INTO client ( clt_num, clt_nom, clt_prenom, clt_pays, clt_loc, clt_ca,
clt_type ) VALUES ( 
'C01', 'DEFRERE', 'Marc', 'F', 'Paris', 806, 'Particulier'); 
INSERT INTO client ( clt_num, clt_nom, clt_prenom, clt_pays, clt_loc, clt_ca,
clt_type ) VALUES ( 
'C02', 'DECERF', 'Jean', 'F', 'Paris', 0, 'Particulier'); 
INSERT INTO client ( clt_num, clt_nom, clt_prenom, clt_pays, clt_loc, clt_ca,
clt_type ) VALUES ( 
'C03', 'DEFAWE', 'Leon', 'B', 'Liege', 0, 'Particulier'); 
INSERT INTO client ( clt_num, clt_nom, clt_prenom, clt_pays, clt_loc, clt_ca,
clt_type ) VALUES ( 
'C04', 'NOSSENT', 'Serge', 'B', 'Bruxelles', 149, 'Particulier'); 
INSERT INTO client ( clt_num, clt_nom, clt_prenom, clt_pays, clt_loc, clt_ca,
clt_type ) VALUES ( 
'C05', 'JACOB', 'Marthe', 'F', 'Marseille', 31, 'Administration'); 
INSERT INTO client ( clt_num, clt_nom, clt_prenom, clt_pays, clt_loc, clt_ca,
clt_type ) VALUES ( 
'C06', 'JAMAR', 'Pierre', 'B', 'Liege', 21, 'Administration'); 
INSERT INTO client ( clt_num, clt_nom, clt_prenom, clt_pays, clt_loc, clt_ca,
clt_type ) VALUES ( 
'C07', 'DECKERS', 'William', 'F', 'Lyon', 140, 'Grand Compte'); 
INSERT INTO client ( clt_num, clt_nom, clt_prenom, clt_pays, clt_loc, clt_ca,
clt_type ) VALUES ( 
'C08', 'DECLERCQ', 'Lucien', 'B', 'Bruxelles', 349, 'Grand Compte'); 
INSERT INTO client ( clt_num, clt_nom, clt_prenom, clt_pays, clt_loc, clt_ca,
clt_type ) VALUES ( 
'C09', 'DEFYZ', 'Maurice', 'F', 'Bordeaux', 0, 'Particulier'); 
INSERT INTO client ( clt_num, clt_nom, clt_prenom, clt_pays, clt_loc, clt_ca,
clt_type ) VALUES ( 
'C10', 'DEFOOZ', 'Francis', 'F', 'Lille', 60, 'Particulier'); 
INSERT INTO client ( clt_num, clt_nom, clt_prenom, clt_pays, clt_loc, clt_ca,
clt_type ) VALUES ( 
'C11', 'RAMJOIE', 'Victor', 'F', 'Nantes', 0, 'Particulier'); 
INSERT INTO client ( clt_num, clt_nom, clt_prenom, clt_pays, clt_loc, clt_ca,
clt_type ) VALUES ( 
'C12', 'RENARDY', 'Jean', 'F', 'Marseille', 275, 'Grand Compte'); 
INSERT INTO client ( clt_num, clt_nom, clt_prenom, clt_pays, clt_loc, clt_ca,
clt_type ) VALUES ( 
'C13', 'MANTEAU', 'Yvan', 'F', 'Paris', 320, 'Grand Compte'); 
INSERT INTO client ( clt_num, clt_nom, clt_prenom, clt_pays, clt_loc, clt_ca,
clt_type ) VALUES ( 
'C14', 'JONAS', 'Henri', 'F', 'Paris', 0, 'PME'); 
INSERT INTO client ( clt_num, clt_nom, clt_prenom, clt_pays, clt_loc, clt_ca,
clt_type ) VALUES ( 
'C15', 'DELVENNE', 'Christian', 'F', 'Lyon', 0, 'Grand Compte'); 
INSERT INTO client ( clt_num, clt_nom, clt_prenom, clt_pays, clt_loc, clt_ca,
clt_type ) VALUES ( 
'C16', 'DEFEEZ', 'Andre', 'F', 'Lyon', 0, 'Particulier'); 
commit;

--
-- insertion dans la table FOURNISSEUR
--
INSERT INTO fournisseur ( frs_num, frs_nom ) VALUES ( 
'F01', 'CATIO ELECTRONIC'); 
INSERT INTO fournisseur ( frs_num, frs_nom ) VALUES ( 
'F02', 'LES STYLOS REUNIS'); 
INSERT INTO fournisseur ( frs_num, frs_nom ) VALUES ( 
'F03', 'MECANIQUE DE PRECISION'); 
INSERT INTO fournisseur ( frs_num, frs_nom ) VALUES ( 
'F04', 'SARL ROULAND'); 
INSERT INTO fournisseur ( frs_num, frs_nom ) VALUES ( 
'F05', 'ELECTROLAMP'); 
commit;

--
-- insertion dans la table ARTICLE
--
INSERT INTO article ( art_num, art_nom, art_coul, art_poids, art_stock, art_pa, art_pv,
art_frs ) VALUES ( 
'A01', 'AGRAFEUSE', 'ROUGE', 150, 10, 20, 29, 'F04'); 
INSERT INTO article ( art_num, art_nom, art_coul, art_poids, art_stock, art_pa, art_pv,
art_frs ) VALUES ( 
'A02', 'CALCULATRICE', 'NOIR', 150, 5, 200, 235, 'F01'); 
INSERT INTO article ( art_num, art_nom, art_coul, art_poids, art_stock, art_pa, art_pv,
art_frs ) VALUES ( 
'A03', 'CACHET-DATEUR', 'BLANC', 100, 3, 21, 30, 'F04'); 
INSERT INTO article ( art_num, art_nom, art_coul, art_poids, art_stock, art_pa, art_pv,
art_frs ) VALUES ( 
'A04', 'LAMPE', 'ROUGE', 550, 3, 105, 149, 'F05'); 
INSERT INTO article ( art_num, art_nom, art_coul, art_poids, art_stock, art_pa, art_pv,
art_frs ) VALUES ( 
'A05', 'LAMPE', 'BLANC', 550, 3, 105, 149, 'F05'); 
INSERT INTO article ( art_num, art_nom, art_coul, art_poids, art_stock, art_pa, art_pv,
art_frs ) VALUES ( 
'A06', 'LAMPE', 'BLEU', 550, 3, 105, 149, 'F05'); 
INSERT INTO article ( art_num, art_nom, art_coul, art_poids, art_stock, art_pa, art_pv,
art_frs ) VALUES ( 
'A07', 'LAMPE', 'VERT', 550, 3, 105, 149, 'F05'); 
INSERT INTO article ( art_num, art_nom, art_coul, art_poids, art_stock, art_pa, art_pv,
art_frs ) VALUES ( 
'A08', 'PESE-LETTRE 1-500', NULL, NULL, 2, 120, 200, 'F03'); 
INSERT INTO article ( art_num, art_nom, art_coul, art_poids, art_stock, art_pa, art_pv,
art_frs ) VALUES ( 
'A09', 'PESE-LETTRE 1-1000', NULL, NULL, 2, 150, 250, 'F03'); 
INSERT INTO article ( art_num, art_nom, art_coul, art_poids, art_stock, art_pa, art_pv,
art_frs ) VALUES ( 
'A10', 'CRAYON', 'ROUGE', 20, 210, 1, 2, 'F02'); 
INSERT INTO article ( art_num, art_nom, art_coul, art_poids, art_stock, art_pa, art_pv,
art_frs ) VALUES ( 
'A11', 'CRAYON', 'BLEU', 20, 190, 1, 2, 'F02'); 
INSERT INTO article ( art_num, art_nom, art_coul, art_poids, art_stock, art_pa, art_pv,
art_frs ) VALUES ( 
'A12', 'CRAYON LUXE', 'ROUGE', 20, 95, 3, 5, 'F02'); 
INSERT INTO article ( art_num, art_nom, art_coul, art_poids, art_stock, art_pa, art_pv,
art_frs ) VALUES ( 
'A13', 'CRAYON LUXE', 'VERT', 20, 90, 3, 5, 'F02'); 
INSERT INTO article ( art_num, art_nom, art_coul, art_poids, art_stock, art_pa, art_pv,
art_frs ) VALUES ( 
'A14', 'CRAYON LUXE', 'BLEU', 20, 80, 3, 5, 'F02'); 
INSERT INTO article ( art_num, art_nom, art_coul, art_poids, art_stock, art_pa, art_pv,
art_frs ) VALUES ( 
'A15', 'CRAYON LUXE', 'NOIR', 20, 450, 3, 5, 'F02'); 
commit;
 


 
--
-- insertion dans la table COMMANDE
--
INSERT INTO commande ( cmd_num, cmd_date, cmd_clt, cmd_mag ) VALUES ( 
'C9401',  '1994-06-06', 'C07'
, 'M03'); 
INSERT INTO commande ( cmd_num, cmd_date, cmd_clt, cmd_mag )  VALUES (
'C9402',  '1994-06-06', 'C06'
, 'M12'); 
INSERT INTO commande ( cmd_num, cmd_date, cmd_clt, cmd_mag ) VALUES (
'C9403',  '1994-06-06', 'C13'
, 'M01'); 
INSERT INTO commande ( cmd_num, cmd_date, cmd_clt, cmd_mag ) VALUES (
'C9404',  '1994-06-06', 'C01'
, 'M02');
INSERT INTO commande ( cmd_num, cmd_date, cmd_clt, cmd_mag ) VALUES (
'C9405',  '1994-06-06', 'C08'
, 'M11'); 
INSERT INTO commande ( cmd_num, cmd_date, cmd_clt, cmd_mag ) VALUES (
'C9406',  '1994-06-07', 'C05'
, 'M04'); 
INSERT INTO commande ( cmd_num, cmd_date, cmd_clt, cmd_mag ) VALUES (
'C9407',  '1994-06-07', 'C04'
, 'M11');
INSERT INTO commande ( cmd_num, cmd_date, cmd_clt, cmd_mag ) VALUES (
'C9408',  '1994-06-08', 'C03'
, 'M12'); 
INSERT INTO commande ( cmd_num, cmd_date, cmd_clt, cmd_mag ) VALUES (
'C9409',  '1994-06-08', 'C10'
, 'M11');
INSERT INTO commande ( cmd_num, cmd_date, cmd_clt, cmd_mag ) VALUES (
'C9410',  '1994-06-08', 'C01'
, 'M11'); 
INSERT INTO commande ( cmd_num, cmd_date, cmd_clt, cmd_mag ) VALUES (
'C9411',  '1994-06-09', 'C12'
, 'M01');
INSERT INTO commande ( cmd_num, cmd_date, cmd_clt, cmd_mag ) VALUES (
'C9412',  '1994-06-10', 'C01'
, 'M11');
commit;
 

--
-- insertion dans la table LIG_CMD
--
INSERT INTO lig_cmd ( lcd_cmd, lcd_art, lcd_qte, lcd_liv, lcd_pu,
lcd_datliv ) VALUES (
'C9401', 'A04', 1, 1, 140,  '1994-06-06');
INSERT INTO lig_cmd ( lcd_cmd, lcd_art, lcd_qte, lcd_liv, lcd_pu,
lcd_datliv ) VALUES (
'C9402', 'A10', 1, 1, 2,  '1994-06-06');
INSERT INTO lig_cmd ( lcd_cmd, lcd_art, lcd_qte, lcd_liv, lcd_pu,
lcd_datliv ) VALUES (
'C9402', 'A11', 2, 2, 2,  '1994-06-06');
INSERT INTO lig_cmd ( lcd_cmd, lcd_art, lcd_qte, lcd_liv, lcd_pu,
lcd_datliv ) VALUES ( 
'C9402', 'A14', 3, 3, 5,  '1994-06-06');
INSERT INTO lig_cmd ( lcd_cmd, lcd_art, lcd_qte, lcd_liv, lcd_pu,
lcd_datliv ) VALUES (
'C9403', 'A02', 1, 1, 230, '1994-06-06');
INSERT INTO lig_cmd ( lcd_cmd, lcd_art, lcd_qte, lcd_liv, lcd_pu,
lcd_datliv ) VALUES ( 
'C9403', 'A03', 2, 2, 30,  '1994-06-06');
INSERT INTO lig_cmd ( lcd_cmd, lcd_art, lcd_qte, lcd_liv, lcd_pu,
lcd_datliv ) VALUES (
'C9403', 'A15', 5, 5, 4,  '1994-06-06');
INSERT INTO lig_cmd ( lcd_cmd, lcd_art, lcd_qte, lcd_liv, lcd_pu,
lcd_datliv ) VALUES (
'C9403', 'A14', 1, 1, 5, '1994-06-06');
INSERT INTO lig_cmd ( lcd_cmd, lcd_art, lcd_qte, lcd_liv, lcd_pu,
lcd_datliv ) VALUES ( 
'C9403', 'A13', 1, 1, 5,  '1994-06-06');
INSERT INTO lig_cmd ( lcd_cmd, lcd_art, lcd_qte, lcd_liv, lcd_pu,
lcd_datliv ) VALUES (
'C9404', 'A02', 2, 0, 235,  '1994-06-10');
INSERT INTO lig_cmd ( lcd_cmd, lcd_art, lcd_qte, lcd_liv, lcd_pu,
lcd_datliv ) VALUES (
'C9404', 'A12', 1, 0, 5, '1994-06-10');
INSERT INTO lig_cmd ( lcd_cmd, lcd_art, lcd_qte, lcd_liv, lcd_pu,
lcd_datliv ) VALUES (
'C9404', 'A13', 10, 0, 4,  '1994-06-10');
INSERT INTO lig_cmd ( lcd_cmd, lcd_art, lcd_qte, lcd_liv, lcd_pu,
lcd_datliv ) VALUES (
'C9404', 'A15', 8, 0, 4, '1994-06-10');
INSERT INTO lig_cmd ( lcd_cmd, lcd_art, lcd_qte, lcd_liv, lcd_pu,
lcd_datliv ) VALUES (
'C9404', 'A05', 1, 0, 149,  '1994-06-10');
INSERT INTO lig_cmd ( lcd_cmd, lcd_art, lcd_qte, lcd_liv, lcd_pu,
lcd_datliv ) VALUES (
'C9405', 'A06', 1, 1, 149,  '1994-06-10');
INSERT INTO lig_cmd ( lcd_cmd, lcd_art, lcd_qte, lcd_liv, lcd_pu,
lcd_datliv ) VALUES (
'C9406', 'A10', 1, 1, 2, '1994-06-10');
INSERT INTO lig_cmd ( lcd_cmd, lcd_art, lcd_qte, lcd_liv, lcd_pu,
lcd_datliv ) VALUES (
'C9407', 'A07', 1, 0, 149, '1994-06-10');
INSERT INTO lig_cmd ( lcd_cmd, lcd_art, lcd_qte, lcd_liv, lcd_pu,
lcd_datliv ) VALUES (
'C9405', 'A08', 1, 1, 200,  '1994-06-06');
INSERT INTO lig_cmd ( lcd_cmd, lcd_art, lcd_qte, lcd_liv, lcd_pu,
lcd_datliv ) VALUES (
'C9408', 'A01', 1, 1, 29,  '1994-06-08');
INSERT INTO lig_cmd ( lcd_cmd, lcd_art, lcd_qte, lcd_liv, lcd_pu,
lcd_datliv ) VALUES (
'C9409', 'A12', 3, 1, 5,  '1994-06-08');
INSERT INTO lig_cmd ( lcd_cmd, lcd_art, lcd_qte, lcd_liv, lcd_pu,
lcd_datliv ) VALUES (
'C9409', 'A13', 3, 1, 5,  '1994-06-08');
INSERT INTO lig_cmd ( lcd_cmd, lcd_art, lcd_qte, lcd_liv, lcd_pu,
lcd_datliv ) VALUES (
'C9409', 'A14', 3, 1, 5, '1994-06-08');
INSERT INTO lig_cmd ( lcd_cmd, lcd_art, lcd_qte, lcd_liv, lcd_pu,
lcd_datliv ) VALUES (
'C9409', 'A15', 3, 1, 5,  '1994-06-08');
INSERT INTO lig_cmd ( lcd_cmd, lcd_art, lcd_qte, lcd_liv, lcd_pu,
lcd_datliv ) VALUES (
'C9410', 'A12', 8, 8, 5,  '1994-06-08');
INSERT INTO lig_cmd ( lcd_cmd, lcd_art, lcd_qte, lcd_liv, lcd_pu,
lcd_datliv ) VALUES (
'C9410', 'A14', 8, 8, 5,  '1994-06-08');
INSERT INTO lig_cmd ( lcd_cmd, lcd_art, lcd_qte, lcd_liv, lcd_pu,
lcd_datliv ) VALUES (
'C9411', 'A09', 1, 0, 250,  '1994-06-14');
INSERT INTO lig_cmd ( lcd_cmd, lcd_art, lcd_qte, lcd_liv, lcd_pu,
lcd_datliv ) VALUES (
'C9411', 'A15', 5, 5, 5,  '1994-06-14');
INSERT INTO lig_cmd ( lcd_cmd, lcd_art, lcd_qte, lcd_liv, lcd_pu,
lcd_datliv ) VALUES ( 
'C9412', 'A03', 1, 0, 30,  '1994-06-15');
commit;

--
-- insertion dans la table LIVRAISON
--

INSERT INTO livraison ( liv_num, liv_date, liv_clt, liv_mag ) VALUES (
'L9401',  '1994-06-06', 'C07', 'M03');
INSERT INTO livraison ( liv_num, liv_date, liv_clt, liv_mag ) VALUES (
'L9402',  '1994-06-06', 'C06', 'M12');
INSERT INTO livraison ( liv_num, liv_date, liv_clt, liv_mag ) VALUES (
'L9403',   '1994-06-06', 'C13', 'M01');
INSERT INTO livraison ( liv_num, liv_date, liv_clt, liv_mag ) VALUES (
'L9404',   '1994-06-06', 'C08', 'M11');
INSERT INTO livraison ( liv_num, liv_date, liv_clt, liv_mag ) VALUES ( 
'L9405',   '1994-06-06', 'C05', 'M04');
INSERT INTO livraison ( liv_num, liv_date, liv_clt, liv_mag ) VALUES ( 
'L9406',   '1994-06-06', 'C03', 'M12');
INSERT INTO livraison ( liv_num, liv_date, liv_clt, liv_mag ) VALUES (
'L9407',   '1994-06-08', 'C10', 'M11');
INSERT INTO livraison ( liv_num, liv_date, liv_clt, liv_mag ) VALUES (
'L9408',   '1994-06-08', 'C01', 'M11');
INSERT INTO livraison ( liv_num, liv_date, liv_clt, liv_mag ) VALUES (
'L9409',   '1994-06-09', 'C12', 'M01');


--
-- insertion dans la table LIG_LIV
--

 INSERT INTO lig_liv ( llv_liv, llv_art, llv_qte, llv_cmd ) VALUES ( 
 'L9401', 'A04', 1, 'C9401'); 
 INSERT INTO lig_liv ( llv_liv, llv_art, llv_qte, llv_cmd ) VALUES ( 
 'L9402', 'A10', 1, 'C9402'); 
 INSERT INTO lig_liv ( llv_liv, llv_art, llv_qte, llv_cmd ) VALUES ( 
 'L9402', 'A11', 2, 'C9402'); 
 INSERT INTO lig_liv ( llv_liv, llv_art, llv_qte, llv_cmd ) VALUES ( 
 'L9402', 'A14', 3, 'C9402'); 
 INSERT INTO lig_liv ( llv_liv, llv_art, llv_qte, llv_cmd ) VALUES ( 
 'L9403', 'A02', 1, 'C9403'); 
 INSERT INTO lig_liv ( llv_liv, llv_art, llv_qte, llv_cmd ) VALUES ( 
 'L9403', 'A03', 2, 'C9403'); 
 INSERT INTO lig_liv ( llv_liv, llv_art, llv_qte, llv_cmd ) VALUES ( 
 'L9403', 'A15', 5, 'C9403'); 
 INSERT INTO lig_liv ( llv_liv, llv_art, llv_qte, llv_cmd ) VALUES ( 
 'L9403', 'A14', 1, 'C9403'); 
 INSERT INTO lig_liv ( llv_liv, llv_art, llv_qte, llv_cmd ) VALUES ( 
 'L9403', 'A13', 1, 'C9403'); 
 INSERT INTO lig_liv ( llv_liv, llv_art, llv_qte, llv_cmd ) VALUES ( 
 'L9404', 'A06', 1, 'C9405'); 
 INSERT INTO lig_liv ( llv_liv, llv_art, llv_qte, llv_cmd ) VALUES ( 
 'L9404', 'A08', 1, 'C9405'); 
 INSERT INTO lig_liv ( llv_liv, llv_art, llv_qte, llv_cmd ) VALUES ( 
 'L9405', 'A10', 1, 'C9406'); 
 INSERT INTO lig_liv ( llv_liv, llv_art, llv_qte, llv_cmd ) VALUES ( 
 'L9406', 'A01', 1, 'C9408'); 
 INSERT INTO lig_liv ( llv_liv, llv_art, llv_qte, llv_cmd ) VALUES ( 
 'L9407', 'A12', 1, 'C9409'); 
 INSERT INTO lig_liv ( llv_liv, llv_art, llv_qte, llv_cmd ) VALUES ( 
 'L9407', 'A13', 1, 'C9409'); 
 INSERT INTO lig_liv ( llv_liv, llv_art, llv_qte, llv_cmd ) VALUES ( 
 'L9407', 'A14', 1, 'C9409'); 
 INSERT INTO lig_liv ( llv_liv, llv_art, llv_qte, llv_cmd ) VALUES ( 
 'L9407', 'A15', 1, 'C9409'); 
 INSERT INTO lig_liv ( llv_liv, llv_art, llv_qte, llv_cmd ) VALUES ( 
 'L9408', 'A12', 8, 'C9410'); 
 INSERT INTO lig_liv ( llv_liv, llv_art, llv_qte, llv_cmd ) VALUES ( 
 'L9408', 'A14', 8, 'C9410'); 
 INSERT INTO lig_liv ( llv_liv, llv_art, llv_qte, llv_cmd ) VALUES ( 
 'L9409', 'A15', 5, 'C9411'); 
