

-- creation des tables
drop table if exists EMPRUNT;
drop table if exists CLIENT ;
drop table if exists FILM;
drop table if exists ACTEUR;
drop table if exists GENRE;
drop table if exists PAYS ;


create table PAYS(
  npays   INT(11) NOT NULL AUTO_INCREMENT,
  nom    varchar(20) not null,
  constraint PK_PAYS primary key (npays)
)engine=INNODB;

create table GENRE(
  ngenre INT(11) NOT NULL AUTO_INCREMENT,
  nature  varchar(20) not null,
  constraint PK_GENRE primary key (ngenre)
)engine=INNODB;

create table ACTEUR(
  nacteur  INT(11) NOT NULL AUTO_INCREMENT,
  nom    varchar(20) not null,
  prenom  varchar(15) not null,
  naissance  date,
  nationalite  INT(11),
  nbreFilms  numeric(3,0) ,
  constraint FK_ACTEUR_PAYS foreign key (nationalite) references PAYS(npays) ,
  constraint PK_ACTEUR primary key (nacteur)
  )engine=INNODB;

create table FILM(
  nfilm  INT(11) NOT NULL AUTO_INCREMENT,
  titre varchar(40) not null,
  ngenre INT(11) not null,
  sortie date,
  npays  INT(11) not null,
  realisateur varchar(20) not null,
  nacteurPrincipal INT(11) not null,
  entrees numeric(6,2),
  oscar char  not null,
  constraint FK_FILM_GENRE foreign key (ngenre) references GENRE(ngenre),
  constraint FK_FILM_ACTEUR foreign key (nacteurPrincipal) references ACTEUR(nacteur),
  constraint FK_FILM_PAYS foreign key (npays) references PAYS(npays),
  constraint PK_FILM primary key (nfilm)  
)engine=INNODB;



create table CLIENT(
  nclient INT(11) NOT NULL AUTO_INCREMENT,
  nom varchar(30) not null,
  prenom varchar(30) not null,
  adresse varchar(30) not null,
  anciennete numeric(3,0) not null,
  constraint PK_CLIENT primary key (nclient)
  )engine=INNODB;
  
create table EMPRUNT(
  nemprunt INT(11) NOT NULL AUTO_INCREMENT,
  nclient INT(11) not null,
  nfilm INT(11) not null,
  retour CHAR(3),
  dateEmprunt date not null,
  constraint FK_EMPRUNT_CLIENT foreign key (nclient) references CLIENT(nclient),
  constraint FK_EMPRUNT_FILM foreign key (nfilm) references FILM(nfilm),
  constraint PK_EMPRUNT primary key (nemprunt)
  )engine=INNODB;


//creation de tables logiques

create view Info_film as
	select Nfilm,Titre,Realisateur as nom_realisateur,nom as nom_acteur
	from film
	inner join Acteur
	on NacteurPrincipal = nacteur order by titre;

--
-- Ajout des genres
--

insert into PAYS values(1,'USA');
insert into PAYS values(2,'France');
insert into PAYS values(3,'Grande Bretagne');
insert into PAYS values(4,'Italie');
insert into PAYS values(5,'Irlande');

--
-- AJout des genres
--


insert into GENRE values (1,'Comedie musicale');
insert into GENRE values (2,'Comedie');
insert into GENRE values (3,'Western');
insert into GENRE values (4,'Fantastique');
insert into GENRE values (5,'Comedie dramatique');
insert into GENRE values (6,'Aventure');
insert into GENRE values (7,'Drame');
insert into GENRE values (8,'Guerre');
insert into GENRE values (9,'Horreur');
insert into GENRE values (10,'Policier');

--
-- Ajout des Acteurs
--


insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,  NATIONALITE,nbreFilms) values
  (1,'Allen',  'Woody',  '1935-12-01',  1,  20);
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,  NATIONALITE,nbreFilms) values    
  (2,'Barr'  ,'Jean-Marc', NULL,    2, NULL);      
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,  NATIONALITE,nbreFilms) values
  (3,'Beatty',  'Warren',  '1937-03-30',  1,  18  );  
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,  NATIONALITE,nbreFilms) values
  (4,'Belmondo',  'Jean-Paul',  '1933-04-09',  2,  68);    
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,  NATIONALITE,nbreFilms) values
  (5,'Brando',  'Marlon',  '1924-04-03',  1,  35  );  
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,  NATIONALITE,nbreFilms) values
  (6,'Broderick',  'Matthew',  '1962-03-21',  1,  12  );  
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,  NATIONALITE,nbreFilms) values
  (7,'Bronson',  'Charles',  '1922-11-03',  1,  43  );  
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,  NATIONALITE,nbreFilms) values
  (8,'Connery',  'Sean',  '1930-08-25',  3,  36);    
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,  NATIONALITE,nbreFilms) values
  (9,'De Niro',  'Robert',  '1943-08-17',  1,  30);    
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,  NATIONALITE,nbreFilms) values
  (10,'Depardieu',  'Gerard',  '1948-12-27',  2  ,66);    
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,  NATIONALITE,nbreFilms) values
  (11,'Dreyfuss'  ,'Richard',  '1947-10-29',  1,  27);    
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,  NATIONALITE,nbreFilms) values
  (12,'Dufilho',  'Jacques',  '1914-12-19'  ,2,  32);    
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,  NATIONALITE,nbreFilms) values
  (13,'Dullea',  'Keir',  '1939-05-30',  1,  15);    
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,  NATIONALITE,nbreFilms) values
  (14,'Dussolier',  'Andre',  '1946-02-17',  2,  23);    
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,  NATIONALITE,nbreFilms) values
  (15,'Eastwood',  'Clint',  '1930-05-31',  1,  42);    
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,  NATIONALITE,nbreFilms) values
  (16,'Farrow',  'Mia',  '1945-02-09',  1,  24);    
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,  NATIONALITE,nbreFilms) values
  (17,'Ford',  'Harrison',  '1942-07-13',  1,  24);    
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,  NATIONALITE,nbreFilms) values
  (18,'Gere',  'Richard',  '1949-08-31',  1,  17);    
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,  NATIONALITE,nbreFilms) values
  (19,'Hoskins',  'Bob',  '1942-10-26',  3,  16);    
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,  NATIONALITE,nbreFilms) values
  (20,'Karyo',  'Tcheky',  '1953-10-04',  2,  24);    
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,  NATIONALITE,nbreFilms) values
  (21,'Kingsley',  'Ben',  '1945-10-10',  3,  8);    
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,  NATIONALITE,nbreFilms) values
  (22,'Lambert',  'Christophe',  '1957-03-29',  2,  15);    
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,  NATIONALITE,nbreFilms) values
  (23,'Lemmon',  'Jack',  '1925-02-08',  1,  28);    
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,  NATIONALITE,nbreFilms) values
  (24,'Mac Gill',  'Everett',  '1945-04-09',  1,  10);    
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,  NATIONALITE,nbreFilms) values
  (25,'Matthau',  'Walter',  '1920-10-01',  1,  32);    
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,  NATIONALITE,nbreFilms) values
  (26,'Montand',  'Yves',  '1921-10-13',  2,  44);    
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,  NATIONALITE,nbreFilms) values
  (27,'Nicholson',  'Jack',  '1937-04-22',  1,  31);    
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,  NATIONALITE,nbreFilms) values
  (28,'O Toole',  'Peter',  '1932-08-02',  5,  25);    
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,  NATIONALITE,nbreFilms) values
  (29,'Pacino',  'Al',  '1940-04-25',  1,  16);    
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,  NATIONALITE,nbreFilms) values
  (30,'Redford',  'Robert',  '1937-08-18',  1,  25);    
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,  NATIONALITE,nbreFilms) values
  (31,'Rourke',  'Mickey',  '1955-03-05',  1,  16);    
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,  NATIONALITE,nbreFilms) values
  (32,'Scheider',  'Roy',  '1935-11-10',  1,  25);    
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,  NATIONALITE,nbreFilms) values
  (33,'Streisand',  'Barbra',  '1942-04-24',  1,  14);    
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,  NATIONALITE,nbreFilms) values
  (34,'Swayze',  'Patrick',  '1952-08-18',  1,  11);    
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,  NATIONALITE,nbreFilms) values
  (35,'Thomas',  'Henry',  NULL,  1, NULL);      
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,  NATIONALITE,nbreFilms) values
  (36,'Volonte',  'Gian Maria',  '1933-04-09',  4,  25);    
insert into ACTEUR(NACTEUR,NOM,PRENOM,NAISSANCE,  NATIONALITE,nbreFilms) values
  (37,'Wood',  'Nathalie',  '1938-06-20',  1,  28);
      
--
-- Ajout des films
--


insert into FILM  values
  (2,'West side story',1,'1961-01-01',1,'Wise',37,8.60,'N');  
insert into FILM  values
  (3,'La guerre des boutons',2,'1961-01-01',2,'Robert',12,9.60,'N');   
insert into FILM  values
  (4,'Pour une poignee de dollars',3,  '1964-01-01', 4, 'Leone', 15, 3.20, 'N');  
insert into FILM  values
(5,'Le bon, la brute et le truand' ,3,'1966-01-01',4,'Leone',15,6.30,'N');   
insert into FILM  values
(6,'2001 : l Odyssee de l espace',4,'1968-01-01',3,'Kubrick',13,2.80,  'N'   );
insert into FILM  values
(7,'Hello Dolly',1,'1969-01-01',1,'Kelly',33,1.90,'N');   
insert into FILM  values
(8,'Il etait une fois dans l Ouest', 3,'1969-01-01',4,'Leone',7,14.70,'N');   
insert into FILM  values
(9,'Le dernier tango a Paris',5,'1972-01-01',4,'Bertolucci',5,  3.50,  'N');   
insert into FILM  values
(10,'Le Parrain',7,'1972-01-01',1,'Coppola',5,7.50,'O');
insert into FILM  values
(11,'L affaire Mattei',10,'1972-01-01',4,'Rosi',36,5.20,'N');  
insert into FILM  values
(12  ,'Le Parrain II',7,'1975-01-01',1,'Coppola',29,5.30,'O');  
insert into FILM  values
(13,'Les dents de la mer',6,'1975-01-01',1,'Spielberg',32,6.20,  'N');   
insert into FILM  values
(14,'Le dernier nabab',7,'1976-01-01',1,'Kazan',9,3.60,'N');  
insert into FILM  values
(15,'Taxi Driver',7,'1976-01-01',1,'Scorcese',9,2.70,'N');   
insert into FILM  values
(16,'La guerre des toiles',4,'1977-01-01',1,'Lucas',17,  5.50,'N');   
insert into FILM  values
(17,'Rencontres du troisieme type', 4,'1977-01-01',1,'Spielberg',11,  4.20,'N');   
insert into FILM  values
(18,'Annie Hall',2,'1977-01-01',1,'Allen',1,2.90,'O');   
insert into FILM  values
(19,'Voyage au bout de l enfer',7,'1978-01-01',1,'Cimino',9,3.90,'O');
insert into FILM  values
(20,'Le ciel peut attendre',2,'1978-01-01',1,'Beatty',3,3.70,'N');   
insert into FILM  values
(21,'Apocalypse now',8,'1979-01-01',1,'Coppola',5,4.30,'N');   
insert into FILM  values
(22,'Shining',9,'1980-01-01',3,'Kubrick',27,2.60,'N');  
insert into FILM  values
(23,'Le dernier metro',7,'1980-01-01',2,'Truffaut',10,2.80,'N');   
insert into FILM  values
(24,'Le retour de Martin Guerre',7,'1981-01-01',2,'Vigne',10,2.70,'N');  
insert into FILM  values
(25,'La guerre du feu',6,'1981-01-01',2,'Annaud',24,3.90,'N');   
insert into FILM  values
(26,'Les aventuriers de l arche perdue',6,'1981-01-01',1,'Spielberg',17  ,5.90,'N');  
insert into FILM  values
(27,'Gandhi',7,'1982-01-01',3,'Attenbourough',21,4.90,'O');   
insert into FILM  values
(28,'Missing',7,'1982-01-01',1,'Gavras',23,1.90,'N');   
insert into FILM  values
(29,'E.T.',4,'1982-01-01',1,'Spielberg',35,8.30,'N');   
insert into FILM  values
(30,'Wargames',6,'1983-01-01',1,'Badham',6,7.40,'N');   
insert into FILM  values
(31,'Tendres passions',5,'1983-01-01',1,'Brooks',27,4.30,'O');   
insert into FILM  values
(32,'Cotton club',1,'1984-01-01',1,'Coppola',18  ,1.50,'N');  
insert into FILM  values
(33,'L annee du dragon',10,'1985-01-01',1,'Cimino',31,2.80,'N');   
insert into FILM  values
(34,'Lady Hawke',4,'1985-01-01',1,'Donner',6,2.30,'N');   
insert into FILM  values
(35,'Highlander',4,'1985-01-01',3,'Mulcahy',22,3.50,'N');   
insert into FILM  values
(36,'Out of Africa',6,'1985-01-01',1,'Pollack',30,4.70,'O');   
insert into FILM  values
(37,'La rose pourpre du Caire',5,'1985-01-01',1,'Allen',16  ,3.40,'N');   
insert into FILM  values
(38,'Jean de Florette',7,'1986-01-01',2,'Berri',26,6.00,'N');
insert into FILM  values
(39,'Manon des Sources',7,'1986-01-01',2,'Berri',26,4.30,'N');  
insert into FILM  values
(40,'Le maitre de guerre',6,'1986-01-01',1,'Eastwood',15  ,1.90,'N');   
insert into FILM  values
(41,'Trois hommes et un couffin',2,'1986-01-01',2,'Serreau',14  ,10.00,'N');   
insert into FILM  values
(42,'Le nom de la rose',7,'1986-01-01',2,'Annaud',8,8.90,'N');   
insert into FILM  values
(43,'Pirates',6,'1986-01-01',1,'Polanski',25,5.80,'N');  
insert into FILM  values
(44,'Le dernier empereur',7,'1987-01-01',4,'Bertolucci',28,7.50,'O');   
insert into FILM  values
(45,'Sous le soleil de Satan',7,'1987-01-01',2,'Pialat',10,  2.90,'N');  
insert into FILM  values
(46,'L ours',6,'1988-01-01',2,'Annaud',20,7.80,'N');   
insert into FILM  values
(47,'Le grand bleu',6,'1988-01-01',2,'Besson',2,5.90,'N');   
insert into FILM  values
(48,'Qui veut la peau de Roger Rabbit',2,'1988-01-01',1,'Zemeckis',19  ,3.60,'N');  
insert into FILM  values
(49,'Crimes et delits',5,'1989-01-01',1,'Allen',1  ,2.40,'N');   
insert into FILM  values
(50,'Le Parrain III',7,'1990-01-01',1,'Coppola',29,3.80,'N');  
insert into FILM  values
(51,'Ghost',4,'1990-01-01',1,'Zucker',34,5.60,'N');
