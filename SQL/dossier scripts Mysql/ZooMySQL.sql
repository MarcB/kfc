# Connection: Cinema
# Host: localhost
# Saved: 2004-11-18 19:50:44
# 


-- creation des tables
drop table  if exists ANIMAL;
drop table  if exists MENU;
drop table  if exists FAMILLE;

create table FAMILLE(
	race		varchar(15)      not null,
	origine		varchar(15)	not null,
	ami		char(4)	not null,
	constraint PK_FAMILLE_RACE_ORIGINE
		primary key (race, origine)
) engine=innodb;


create table MENU(
	N_M		char(3) not null,
	QTE_VIANDE	numeric(4,1) not null,
	QTE_LEGUMES	numeric(4,1)
			 not null,
             primary key(n_m)

) engine=innodb;

create table ANIMAL(
	n_a		char(3) not null,
	nom		varchar(20)  not null,
	age		numeric(3,1) not null,
	race		varchar(15) not null,
	origine		varchar(15) not null,
	n_menu		char(3) not null,
	n_asc		char(3),
	constraint FK_ANIMAL_N_ASC  foreign key(n_asc) references ANIMAL(N_A),
	constraint FK_ANIMAL_RACE_ORIGINE
		foreign key (race,origine) references FAMILLE(race,origine),
	constraint FK_ANIMAL_N_MENU 
                           foreign key(n_menu) references MENU(N_M),
              primary key(n_a)
) engine=innodb;


--
-- ajout des n-uplets dans la table FAMILLE
--
insert into FAMILLE values('lion', 'Afrique','FAUX');
insert into FAMILLE values('panda', 'Chine','FAUX');
insert into FAMILLE values('lama', 'Amerique','VRAI');
insert into FAMILLE values('singe', 'Europe','VRAI');
insert into FAMILLE values('elephant', 'Afrique','VRAI');
insert into FAMILLE values('kangourou', 'Australie','VRAI');
insert into FAMILLE values('tigre', 'Asie','FAUX');
insert into FAMILLE values('panthere', 'Afrique','FAUX');
insert into FAMILLE values('coq', 'Europe','VRAI');
insert into FAMILLE values('ours', 'Europe','FAUX');
insert into FAMILLE values('renard', 'Europe','FAUX');
insert into FAMILLE values('serpent', 'Afrique','FAUX');
insert into FAMILLE values('serpent', 'Asie','FAUX');
insert into FAMILLE values('araignee', 'Asie','FAUX');
insert into FAMILLE values('araignee', 'Europe','VRAI');

--
-- ajout des n-uplets dans la table MENU
--
insert into MENU values( '1M', 10, 20);
insert into MENU values( '2M',  0, 100);
insert into MENU values( '3M',  0, 50);
insert into MENU values( '4M', 20,  0);
insert into MENU values( '5M',100,  0);
insert into MENU values( '6M', 20,  0);
insert into MENU values( '7M',  0, 10);
insert into MENU values( '8M',  0,  5);
insert into MENU values( '9M',0.1,  0);

--
-- ajout des n-uplets dans la table ANIMAL
--
insert into ANIMAL values(  '3A', 'Noble',   30, 'lion', 'Afrique','4M', NULL);
insert into ANIMAL values(  '1A', 'Hector',  10, 'lion', 'Afrique','5M', '3A');
insert into ANIMAL values( '13A', 'Mignon',  10, 'ours', 'Europe','1M', NULL);
insert into ANIMAL values(  '6A', 'Baloo',    5, 'ours', 'Europe','1M', '13A');
insert into ANIMAL values( '12A', 'Mignon',   1, 'ours', 'Europe','1M',  '6A');
insert into ANIMAL values(  '2A', 'Jacko',   20, 'singe', 'Europe','7M', NULL);
insert into ANIMAL values(  '4A', 'Marsupilani', 5, 'kangourou','Australie','7M', NULL);
insert into ANIMAL values(  '5A', 'Bagheera',  15, 'panthere', 'Afrique','4M', NULL);
insert into ANIMAL values(  '7A', 'Kaa',    2, 'serpent', 'Afrique','6M', NULL);
insert into ANIMAL values(  '8A', 'Isengrin',    4, 'renard', 'Europe','6M', NULL);
insert into ANIMAL values(  '9A', 'Chanteclerc', 2, 'coq', 'Europe','8M', NULL);
insert into ANIMAL values( '10A', 'Mignon',    3, 'panda', 'Chine','7M', NULL);
insert into ANIMAL values( '11A', 'Bongros',   8, 'elephant', 'Afrique','2M', NULL);
insert into ANIMAL values( '14A', 'Terrible',  2, 'tigre', 'Asie','4M', NULL);
insert into ANIMAL values( '15A', 'Pincette',  0.2, 'araignee', 'Asie','9M', NULL);
insert into ANIMAL values( '16A', 'Dentelle',  0.2, 'araignee', 'Europe','9M', NULL);
