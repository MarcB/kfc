﻿drop table if exists apparaits;
drop table if exists personnages;
drop table if exists albums;


create table if not exists albums (
  id      integer  NOT NULL AUTO_INCREMENT,
  titre       varchar(30)       not null,
  annee       integer           not null,
  suivant     integer           ,
  constraint pk_albumss primary key(id),
  constraint fk_albumss_suivant   foreign key(suivant)
                                    references albums(id)
) ENGINE=InnoDB;

create table if not exists personnages
(
  id integer   NOT NULL AUTO_INCREMENT,
  nom         varchar(30 )     ,
  prenom      varchar(20 )    ,
  profession  varchar(20 )   ,
  sexe        char(1)           not null    ,
  genre       varchar(20 )     not null,
  constraint pk_personnages   primary key(id)
) ENGINE=InnoDB;

create table if not exists apparaits
(
   id int(11) not null auto_increment, 
   personnage_id int(11) not null ,
   album_id      integer not null,
   constraint  pk_apparait
        primary key(id),
   constraint  fk_apparait_npersonnages
        foreign key (personnage_id)
    references personnages(id),
   constraint  fk_apparait_nalbums
        foreign key (album_id)
    references albums(id)
) ENGINE=InnoDB;

insert into albums values (1,'Tintin au pays des Soviets', 1930,NULL);
insert into albums values (2,'Tintin au Congo', 1931,NULL);
insert into albums values (3,'Tintin en Amerique', 1932,NULL);
insert into albums values (4,'Les Cigares du pharaon', 1934,NULL);
insert into albums values (5,'Le Lotus bleu', 1936,NULL);
insert into albums values (6,'L''Oreille cassee', 1937,NULL);
insert into albums values (7,'L''Ile noire', 1938,NULL);
insert into albums values (8,'Le Sceptre d''Ottokar', 1939,NULL);
insert into albums values (9,'Le Crabe aux pinces d''or',1941,NULL);
insert into albums values (10,'L''Etoile mysterieuse',1942,NULL);
insert into albums values (11,'Le Secret de la Licorne',1943,NULL);
insert into albums values (12,'Le Tresor de Rackham le Rouge',1944,NULL);
insert into albums values (13,'Les 7 boules de cristal',1948,NULL);
insert into albums values (14,'Le Temple du soleil',1950,NULL);
insert into albums values (15,'Au pays de l''or noir', 1950,NULL);
insert into albums values (16,'Objectif Lune',1953,NULL);
insert into albums values (17,'On a marche sur la Lune',1954,NULL);
insert into albums values (18,'L''Affaire Tournesol',1956,NULL);
insert into albums values (19,'Coke en stock',1958,NULL);
insert into albums values (20,'Tintin au Tibet',1960,NULL);
insert into albums values (21,'Les Bijoux de la Castafiore',1963,NULL);
insert into albums values (22,'Vol 714 pour Sydney',1968,NULL);
insert into albums values (23,'Tintin et les Picaros',1976,NULL);
insert into albums values (24,'Tintin et l''Alph-Art',1986,NULL);
	
	UPDATE albums
	SET suivant = 5
	WHERE id=4;

	UPDATE albums
	SET suivant = 12
	WHERE id=11;
	
	UPDATE albums
	SET suivant = 14
	WHERE id=13;
	
	UPDATE albums
	SET suivant = 17
	WHERE id=16;
	
insert into personnages values( 1,'TINTIN',null	,'Reporter','M','GENTIL');
insert into personnages values( 2,'MILOU',null,null,'M','GENTIL');
insert into personnages values( 3,'HADDOCK','Archibald','Capitaine','M','GENTIL');
insert into personnages values( 4,'TOURNESOL','Tryphon','Professeur','M','GENTIL');
insert into personnages values( 5,'DUPOND',null,'Détective','M','GENTIL');
insert into personnages values( 6,'DUPONT',null,'Détective','M','GENTIL');
insert into personnages values( 7,'CASTAFIORE','Bianca','Cantatrice','F','GENTIL');
insert into personnages values( 8,null,'Nestor','Domestique','M','GENTIL');
insert into personnages values( 9,'LAMPION','Séraphin','Agent D''Assurance','M','GENTIL');
insert into personnages values(10,'TCHONG-JEN','Tchang',null,'M','GENTIL');
insert into personnages values(11,'ALCAZAR',null,'Général','M','GENTIL');
insert into personnages values(12,'BEN KALISH EZAB','Mohammed','Emir','M','GENTIL');
insert into personnages values(13,'DA FIGUERA','Oliveira','Brocanteur','M','GENTIL');
insert into personnages values(14,'SZUT','Piotr','Pilote','M','GENTIL');
insert into personnages values(15,'RASTAPOPOULOS','Roberto','Milliadaire','M','MECHANT');
insert into personnages values(16,'THOMPSON','Allan','Lieutenant','M','MECHANT');
insert into personnages values(17,'MÜLLER','J.W.','Docteur','M','MECHANT');
insert into personnages values(18,'SPONSZ',null,'Colonnel','M','MECHANT');
insert into personnages values(19,'BABAORO''M',null,'Roi','M','GENTIL');
insert into personnages values(20,'CAPONE','Al','Gangster','M','MECHANT');
insert into personnages values(21,'SMILES','Bobby','Gangster','M','MECHANT');
insert into personnages values(22,'MAC ADAM','Mike','Détective','M','GENTIL');
insert into personnages values(23,'SICLONE','Philémon','Egyptologue','M','GENTIL');
insert into personnages values(24,'CIPAÇALOUVISHNI'	,null	,'Fakir','M','GENTIL');
insert into personnages values(25,'MITSUHIRATO',null,'Agent Secret','M','MECHANT');
insert into personnages values(26,'DAWSON',null	,'Chef De Police','M','MECHANT');
insert into personnages values(27,'GIBBONS',null	,'Industriel','M','MECHANT');
insert into personnages values(28,'PEREZ','Alonso','Gangster','M','MECHANT');
insert into personnages values(29,'BADA','Ramon','Gangster','M','MECHANT');
insert into personnages values(30,'RIDGEWELL',null	,'Explorateur','M','GENTIL');
insert into personnages values(31,'WRONZOFF',null	,'Gangster','M','MECHANT');
insert into personnages values(32,null,'Ivan','Chauffeur','M','MECHANT');
insert into personnages values(33,'HALAMBIQUE','Alfred',null	,'M','MECHANT');
insert into personnages values(34,'HALAMBIQUE','Nestor','Professeur','M','GENTIL');
insert into personnages values(35,'MUSKAR XII',null	,'Roi','M','GENTIL');
insert into personnages values(36,'JORGEN','Boris','Colonnel','M','MECHANT');
insert into personnages values(37,'BEN SALAAD','Omar','Négociant','M','MECHANT');
insert into personnages values(38,'CALYS','Hyppolyte','Professeur','M','GENTIL');
insert into personnages values(39,'BJÖRGENSKJÖLD','Erik','Professeur','M','GENTIL');
insert into personnages values(40,'SCHLUZE','Otto','Professeur','M','GENTIL');
insert into personnages values(41,'BOLERO Y CALAMARES','Porfirio','Professeur','M','GENTIL');
insert into personnages values(42,'CANTONNEAU','Paul','Professeur','M','GENTIL');
insert into personnages values(43,'JONÃS DOS SANTOS','Pedro','Professeur','M','GENTIL');
insert into personnages values(44,'BOHLWINKEL',null,  'Financier','M','MECHANT');
insert into personnages values(45,'CHESTER',null,'Capitaine','M','GENTIL');
insert into personnages values(46,'SAKARINE','Ivan Ivanovitch','Collectionneur','M','GENTIL');
insert into personnages values(47,'LOISEAU','Maxime','Antiquaire','M','MECHANT');
insert into personnages values(48,'BERGAMOTTE','Hyppolyte','Professeur','M','GENTIL');
insert into personnages values(49,'CHARLET','Marc','Professeur','M','GENTIL');
insert into personnages values(50,'SANDERS-HARDMUTH',null,'Professeur','M','GENTIL');
insert into personnages values(51,'CLAIRMONT',null,'Cinéaste','M','GENTIL');
insert into personnages values(52,'LAUBÉPIN',null,'Professeur','M','GENTIL');
insert into personnages values(53,'HORNET',null,'Professeur','M','GENTIL');
insert into personnages values(54,null,'Zorrino','Vendeur','M','GENTIL');
insert into personnages values(55,null,'Chiquito',null,'M','MECHANT');
insert into personnages values(56,'WOLFF','Frank','Ingénieur','M','GENTIL');
insert into personnages values(57,'BAXTER',null,'Directeur','M','GENTIL');
insert into personnages values(58,'ROTULE',null,'Docteur','M','GENTIL');
insert into personnages values(59,'TOPOLINO','Alfredo','Professeur','M','GENTIL');
insert into personnages values(60,'THARKEY',null,'Guide','M','GENTIL');
insert into personnages values(61,'BEN KALISH EZAB','Abdallah',null,'M','GENTIL');
insert into personnages values(62,'CARREIDAS','Laszlo','Milliardaire','M','GENTIL');
insert into personnages values(63,'SPALDING',null,'Secrétaire','M','MECHANT');
insert into personnages values(64,null,'Gino','Stewart','M','GENTIL');
insert into personnages values(65,'KROLSPELL',null,'Docteur','M','MECHANT');
insert into personnages values(66,'COLOMBANI','Paolo','Pilote','M','MECHANT');
insert into personnages values(67,'BOEHM','Hans','Pilote','M','MECHANT');
insert into personnages values(68,'WAGNER','Igor','Pianiste','M','GENTIL');
insert into personnages values(69,null,'Irma','Habilleuse','F','GENTIL');
insert into personnages values(70,'BOULLU',null,'Carreleur','M','GENTIL');
insert into personnages values(71,'TAPIOCA',null,'Général','M','MECHANT');
insert into personnages values(72,'ALCAZAR','Peggy',null,'F','GENTIL');

INSERT INTO apparaits(personnage_id, album_id) values(1,1);
INSERT INTO apparaits(personnage_id, album_id) values(1,2);
INSERT INTO apparaits(personnage_id, album_id) values(1,3);
INSERT INTO apparaits(personnage_id, album_id) values(1,4);
INSERT INTO apparaits(personnage_id, album_id) values(1,5);
INSERT INTO apparaits(personnage_id, album_id) values(1,6);
INSERT INTO apparaits(personnage_id, album_id) values(1,7);
INSERT INTO apparaits(personnage_id, album_id) values(1,8);
INSERT INTO apparaits(personnage_id, album_id) values(1,9);
INSERT INTO apparaits(personnage_id, album_id) values(1,10);
INSERT INTO apparaits(personnage_id, album_id) values(1,11);
INSERT INTO apparaits(personnage_id, album_id) values(1,12);
INSERT INTO apparaits(personnage_id, album_id) values(1,13);
INSERT INTO apparaits(personnage_id, album_id) values(1,14);
INSERT INTO apparaits(personnage_id, album_id) values(1,15);
INSERT INTO apparaits(personnage_id, album_id) values(1,16);
INSERT INTO apparaits(personnage_id, album_id) values(1,17);
INSERT INTO apparaits(personnage_id, album_id) values(1,18);
INSERT INTO apparaits(personnage_id, album_id) values(1,19);
INSERT INTO apparaits(personnage_id, album_id) values(1,20);
INSERT INTO apparaits(personnage_id, album_id) values(1,21);
INSERT INTO apparaits(personnage_id, album_id) values(1,22);
INSERT INTO apparaits(personnage_id, album_id) values(1,23);
INSERT INTO apparaits(personnage_id, album_id) values(1,24);

INSERT INTO apparaits(personnage_id, album_id) values(2,1);
INSERT INTO apparaits(personnage_id, album_id) values(2,2);
INSERT INTO apparaits(personnage_id, album_id) values(2,3);
INSERT INTO apparaits(personnage_id, album_id) values(2,4);
INSERT INTO apparaits(personnage_id, album_id) values(2,5);
INSERT INTO apparaits(personnage_id, album_id) values(2,6);
INSERT INTO apparaits(personnage_id, album_id) values(2,7);
INSERT INTO apparaits(personnage_id, album_id) values(2,8);
INSERT INTO apparaits(personnage_id, album_id) values(2,9);
INSERT INTO apparaits(personnage_id, album_id) values(2,10);
INSERT INTO apparaits(personnage_id, album_id) values(2,11);
INSERT INTO apparaits(personnage_id, album_id) values(2,12);
INSERT INTO apparaits(personnage_id, album_id) values(2,13);
INSERT INTO apparaits(personnage_id, album_id) values(2,14);
INSERT INTO apparaits(personnage_id, album_id) values(2,15);
INSERT INTO apparaits(personnage_id, album_id) values(2,16);
INSERT INTO apparaits(personnage_id, album_id) values(2,17);
INSERT INTO apparaits(personnage_id, album_id) values(2,18);
INSERT INTO apparaits(personnage_id, album_id) values(2,19);
INSERT INTO apparaits(personnage_id, album_id) values(2,20);
INSERT INTO apparaits(personnage_id, album_id) values(2,21);
INSERT INTO apparaits(personnage_id, album_id) values(2,22);
INSERT INTO apparaits(personnage_id, album_id) values(2,23);
INSERT INTO apparaits(personnage_id, album_id) values(2,24);

INSERT INTO apparaits(personnage_id, album_id) values(3,9);
INSERT INTO apparaits(personnage_id, album_id) values(3,10);
INSERT INTO apparaits(personnage_id, album_id) values(3,11);
INSERT INTO apparaits(personnage_id, album_id) values(3,12);
INSERT INTO apparaits(personnage_id, album_id) values(3,13);
INSERT INTO apparaits(personnage_id, album_id) values(3,14);
INSERT INTO apparaits(personnage_id, album_id) values(3,15);
INSERT INTO apparaits(personnage_id, album_id) values(3,16);
INSERT INTO apparaits(personnage_id, album_id) values(3,17);
INSERT INTO apparaits(personnage_id, album_id) values(3,18);
INSERT INTO apparaits(personnage_id, album_id) values(3,19);
INSERT INTO apparaits(personnage_id, album_id) values(3,20);
INSERT INTO apparaits(personnage_id, album_id) values(3,21);
INSERT INTO apparaits(personnage_id, album_id) values(3,22);
INSERT INTO apparaits(personnage_id, album_id) values(3,23);

INSERT INTO apparaits(personnage_id, album_id) values(4,12);
INSERT INTO apparaits(personnage_id, album_id) values(4,13);
INSERT INTO apparaits(personnage_id, album_id) values(4,14);
INSERT INTO apparaits(personnage_id, album_id) values(4,16);
INSERT INTO apparaits(personnage_id, album_id) values(4,17);
INSERT INTO apparaits(personnage_id, album_id) values(4,18);
INSERT INTO apparaits(personnage_id, album_id) values(4,19);
INSERT INTO apparaits(personnage_id, album_id) values(4,20);
INSERT INTO apparaits(personnage_id, album_id) values(4,21);
INSERT INTO apparaits(personnage_id, album_id) values(4,22);
INSERT INTO apparaits(personnage_id, album_id) values(4,23);

INSERT INTO apparaits(personnage_id, album_id) values(5,4);
INSERT INTO apparaits(personnage_id, album_id) values(5,5);
INSERT INTO apparaits(personnage_id, album_id) values(5,7);
INSERT INTO apparaits(personnage_id, album_id) values(5,8);
INSERT INTO apparaits(personnage_id, album_id) values(5,9);
INSERT INTO apparaits(personnage_id, album_id) values(5,11);
INSERT INTO apparaits(personnage_id, album_id) values(5,12);
INSERT INTO apparaits(personnage_id, album_id) values(5,13);
INSERT INTO apparaits(personnage_id, album_id) values(5,14);
INSERT INTO apparaits(personnage_id, album_id) values(5,15);
INSERT INTO apparaits(personnage_id, album_id) values(5,16);
INSERT INTO apparaits(personnage_id, album_id) values(5,17);
INSERT INTO apparaits(personnage_id, album_id) values(5,18);
INSERT INTO apparaits(personnage_id, album_id) values(5,19);
INSERT INTO apparaits(personnage_id, album_id) values(5,21);
INSERT INTO apparaits(personnage_id, album_id) values(5,22);
INSERT INTO apparaits(personnage_id, album_id) values(5,23);

INSERT INTO apparaits(personnage_id, album_id) values(6,4);
INSERT INTO apparaits(personnage_id, album_id) values(6,5);
INSERT INTO apparaits(personnage_id, album_id) values(6,7);
INSERT INTO apparaits(personnage_id, album_id) values(6,8);
INSERT INTO apparaits(personnage_id, album_id) values(6,9);
INSERT INTO apparaits(personnage_id, album_id) values(6,11);
INSERT INTO apparaits(personnage_id, album_id) values(6,12);
INSERT INTO apparaits(personnage_id, album_id) values(6,13);
INSERT INTO apparaits(personnage_id, album_id) values(6,14);
INSERT INTO apparaits(personnage_id, album_id) values(6,15);
INSERT INTO apparaits(personnage_id, album_id) values(6,16);
INSERT INTO apparaits(personnage_id, album_id) values(6,17);
INSERT INTO apparaits(personnage_id, album_id) values(6,18);
INSERT INTO apparaits(personnage_id, album_id) values(6,19);
INSERT INTO apparaits(personnage_id, album_id) values(6,21);
INSERT INTO apparaits(personnage_id, album_id) values(6,22);
INSERT INTO apparaits(personnage_id, album_id) values(6,23);

INSERT INTO apparaits(personnage_id, album_id) values(7,8);
INSERT INTO apparaits(personnage_id, album_id) values(7,13);
INSERT INTO apparaits(personnage_id, album_id) values(7,18);
INSERT INTO apparaits(personnage_id, album_id) values(7,21);
INSERT INTO apparaits(personnage_id, album_id) values(7,23);

INSERT INTO apparaits(personnage_id, album_id) values(8,11);
INSERT INTO apparaits(personnage_id, album_id) values(8,19);
INSERT INTO apparaits(personnage_id, album_id) values(8,21);

INSERT INTO apparaits(personnage_id, album_id) values(9,18);
INSERT INTO apparaits(personnage_id, album_id) values(9,19);
INSERT INTO apparaits(personnage_id, album_id) values(9,22);
INSERT INTO apparaits(personnage_id, album_id) values(9,23);

INSERT INTO apparaits(personnage_id, album_id) values(10,5);
INSERT INTO apparaits(personnage_id, album_id) values(10,20);

INSERT INTO apparaits(personnage_id, album_id) values(11,6);
INSERT INTO apparaits(personnage_id, album_id) values(11,13);
INSERT INTO apparaits(personnage_id, album_id) values(11,19);
INSERT INTO apparaits(personnage_id, album_id) values(11,23);

INSERT INTO apparaits(personnage_id, album_id) values(12,16);
INSERT INTO apparaits(personnage_id, album_id) values(12,19);

INSERT INTO apparaits(personnage_id, album_id) values(13,4);
INSERT INTO apparaits(personnage_id, album_id) values(13,15);
INSERT INTO apparaits(personnage_id, album_id) values(13,19);

INSERT INTO apparaits(personnage_id, album_id) values(14,19);
INSERT INTO apparaits(personnage_id, album_id) values(14,22);

INSERT INTO apparaits(personnage_id, album_id) values(15,4);
INSERT INTO apparaits(personnage_id, album_id) values(15,5);
INSERT INTO apparaits(personnage_id, album_id) values(15,19);
INSERT INTO apparaits(personnage_id, album_id) values(15,22);

INSERT INTO apparaits(personnage_id, album_id) values(16,4);
INSERT INTO apparaits(personnage_id, album_id) values(16,9);
INSERT INTO apparaits(personnage_id, album_id) values(16,19);
INSERT INTO apparaits(personnage_id, album_id) values(16,22);

INSERT INTO apparaits(personnage_id, album_id) values(17,7);
INSERT INTO apparaits(personnage_id, album_id) values(17,15);
INSERT INTO apparaits(personnage_id, album_id) values(17,19);

INSERT INTO apparaits(personnage_id, album_id) values(18,18);
INSERT INTO apparaits(personnage_id, album_id) values(18,23);

INSERT INTO apparaits(personnage_id, album_id) values(19,2);

INSERT INTO apparaits(personnage_id, album_id) values(20,3);

INSERT INTO apparaits(personnage_id, album_id) values(21,3);

INSERT INTO apparaits(personnage_id, album_id) values(22,3);

INSERT INTO apparaits(personnage_id, album_id) values(23,4);

INSERT INTO apparaits(personnage_id, album_id) values(24,5);

INSERT INTO apparaits(personnage_id, album_id) values(25,5);

INSERT INTO apparaits(personnage_id, album_id) values(26,5);

INSERT INTO apparaits(personnage_id, album_id) values(27,5);

INSERT INTO apparaits(personnage_id, album_id) values(28,6);

INSERT INTO apparaits(personnage_id, album_id) values(29,6);

INSERT INTO apparaits(personnage_id, album_id) values(30,6);
INSERT INTO apparaits(personnage_id, album_id) values(30,23);

INSERT INTO apparaits(personnage_id, album_id) values(31,7);

INSERT INTO apparaits(personnage_id, album_id) values(32,7);

INSERT INTO apparaits(personnage_id, album_id) values(33,8);

INSERT INTO apparaits(personnage_id, album_id) values(34,8);

INSERT INTO apparaits(personnage_id, album_id) values(35,8);

INSERT INTO apparaits(personnage_id, album_id) values(36,8);
INSERT INTO apparaits(personnage_id, album_id) values(36,16);
INSERT INTO apparaits(personnage_id, album_id) values(36,17);

INSERT INTO apparaits(personnage_id, album_id) values(37,9);

INSERT INTO apparaits(personnage_id, album_id) values(38,10);

INSERT INTO apparaits(personnage_id, album_id) values(39,10);

INSERT INTO apparaits(personnage_id, album_id) values(40,10);

INSERT INTO apparaits(personnage_id, album_id) values(41,10);

INSERT INTO apparaits(personnage_id, album_id) values(42,10);
INSERT INTO apparaits(personnage_id, album_id) values(42,13);

INSERT INTO apparaits(personnage_id, album_id) values(43,10);

INSERT INTO apparaits(personnage_id, album_id) values(44,10);

INSERT INTO apparaits(personnage_id, album_id) values(45,10);

INSERT INTO apparaits(personnage_id, album_id) values(46,11);

INSERT INTO apparaits(personnage_id, album_id) values(47,11);

INSERT INTO apparaits(personnage_id, album_id) values(48,13);

INSERT INTO apparaits(personnage_id, album_id) values(49,13);

INSERT INTO apparaits(personnage_id, album_id) values(50,13);

INSERT INTO apparaits(personnage_id, album_id) values(51,13);

INSERT INTO apparaits(personnage_id, album_id) values(52,13);

INSERT INTO apparaits(personnage_id, album_id) values(53,13);

INSERT INTO apparaits(personnage_id, album_id) values(54,14);

INSERT INTO apparaits(personnage_id, album_id) values(55,13);
INSERT INTO apparaits(personnage_id, album_id) values(55,14);

INSERT INTO apparaits(personnage_id, album_id) values(56,16);
INSERT INTO apparaits(personnage_id, album_id) values(56,17);

INSERT INTO apparaits(personnage_id, album_id) values(57,16);
INSERT INTO apparaits(personnage_id, album_id) values(57,17);

INSERT INTO apparaits(personnage_id, album_id) values(58,16);
INSERT INTO apparaits(personnage_id, album_id) values(58,17);

INSERT INTO apparaits(personnage_id, album_id) values(59,18);

INSERT INTO apparaits(personnage_id, album_id) values(60,20);

INSERT INTO apparaits(personnage_id, album_id) values(61,15);
INSERT INTO apparaits(personnage_id, album_id) values(61,19);

INSERT INTO apparaits(personnage_id, album_id) values(62,22);

INSERT INTO apparaits(personnage_id, album_id) values(63,22);

INSERT INTO apparaits(personnage_id, album_id) values(64,22);

INSERT INTO apparaits(personnage_id, album_id) values(65,22);

INSERT INTO apparaits(personnage_id, album_id) values(66,22);

INSERT INTO apparaits(personnage_id, album_id) values(67,22);

INSERT INTO apparaits(personnage_id, album_id) values(68,21);

INSERT INTO apparaits(personnage_id, album_id) values(69,21);

INSERT INTO apparaits(personnage_id, album_id) values(70,21);

INSERT INTO apparaits(personnage_id, album_id) values(71,23);

INSERT INTO apparaits(personnage_id, album_id) values(72,23);
