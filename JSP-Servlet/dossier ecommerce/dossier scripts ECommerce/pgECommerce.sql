drop sequence COMMANDE_SEQ cascade;
drop sequence DETAIL_CDE_SEQ cascade;
drop sequence ITEM_SEQ cascade;
drop sequence CLIENT_SEQ cascade;
drop sequence PAYMENT_SEQ cascade;

drop table   COMMANDE CASCADE ;
drop table   DETAIL_CDE CASCADE ;
drop table    ITEM CASCADE ;
drop table    CLIENT CASCADE ;
drop table   PAYMENT CASCADE ;


create sequence COMMANDE_SEQ;
create sequence DETAIL_CDE_SEQ;
create sequence ITEM_SEQ;
create sequence CLIENT_SEQ;
create sequence PAYMENT_SEQ;

create table COMMANDE(
 ID integer,
 DATECDE date,
 CLIENT_ID integer ,
 PAYMENT_ID integer,
 constraint PK_COMMANDE primary key (ID)		
);

create table DETAIL_CDE(
  ID integer ,
  QTE integer not null,
  CDE_ID integer ,
  ITEM_ID integer   ,
  constraint PK_DETAIL_CDE primary key(ID)	
);

create table ITEM(
  ID integer ,
  CATEGORIE varchar(10) not null,
  TITRE varchar(40) not null,
  PRIX numeric(6,2) not null,
  CODE_BARRE integer not null,
  constraint UK_ITEM_CODE_BARRE
     UNIQUE(CODE_BARRE),
  constraint PK_ITEM primary key(ID)	
	);
	
create table CLIENT(
  ID integer ,
  NOM varchar(30) not null,
  PRENOM varchar(30) not null,
  constraint UK_CLIENT_NOM_PRENOM
     unique (NOM, PRENOM),
  constraint PK_CLIENT primary key(ID)
	);
	
create table PAYMENT(
  ID integer ,
  MONTANT numeric(7,2) not null,
  DATE_PAIEMENT date,
  CLIENT_ID integer ,
  TYPE_PAYMENT varchar(5),
  CREDIT_CARD varchar(20),
  CREDIT_CARD_EXPIRATION date,
  constraint PK_PAYMENT primary key(ID)	
	);
	
alter table COMMANDE add constraint FK_COMMANDE_CLIENT
   foreign key (CLIENT_ID) references CLIENT(ID) ;
alter table COMMANDE add constraint  FK_COMMANDE_PAYMENT
   foreign key (PAYMENT_ID) references PAYMENT(ID) ;

alter table DETAIL_CDE add constraint FK_DETAIL_CDE
   foreign key (CDE_ID) references COMMANDE(ID);
alter table DETAIL_CDE add constraint FK_DETAIL_ITEM
   foreign key (ITEM_ID) references ITEM(ID);

alter table PAYMENT add constraint FK_PAYMENT_CLIENT
   foreign key (CLIENT_ID) references CLIENT(ID);
