drop sequence ECOMM_COMMANDE_SEQ;
drop sequence ECOMM_DETAIL_CDE_SEQ;
drop sequence ECOMM_ITEM_SEQ;
drop sequence ECOMM_CLIENT_SEQ;
drop sequence ECOMM_PAYMENT_SEQ;

drop table   ECOMM_COMMANDE CASCADE CONSTRAINTS;
drop table   ECOMM_DETAIL_CDE CASCADE CONSTRAINTS;
drop table   ECOMM_ITEM CASCADE CONSTRAINTS;
drop table   ECOMM_CLIENT CASCADE CONSTRAINTS;
drop table   ECOMM_PAYMENT CASCADE CONSTRAINTS;


create sequence ECOMM_COMMANDE_SEQ;
create sequence ECOMM_DETAIL_CDE_SEQ;
create sequence ECOMM_ITEM_SEQ;
create sequence ECOMM_CLIENT_SEQ;
create sequence ECOMM_PAYMENT_SEQ;

create table ECOMM_COMMANDE(
 ID integer,
 DATECDE date,
 CLIENT_ID integer ,
 PAYMENT_ID integer,
 constraint PK_EC_COMMANDE primary key (ID)		
);

create table ECOMM_DETAIL_CDE(
  ID integer ,
  QTE integer not null,
  CDE_ID integer ,
  ITEM_ID integer   ,
  constraint PK_EC_DETAIL_CDE primary key(ID)	
);

create table ECOMM_ITEM(
  ID integer ,
  CATEGORIE varchar(10) not null,
  TITRE varchar(40) not null,
  PRIX numeric(6,2) not null,
  CODE_BARRE integer not null,
  constraint UK_EC_ITEM_CODE_BARRE
     UNIQUE(CODE_BARRE),
  constraint PK_EC_ITEM primary key(ID)	
	);
	
create table ECOMM_CLIENT(
  ID integer ,
  NOM varchar(30) not null,
  PRENOM varchar(30) not null,
  constraint UK_EC_CLIENT_NOM_PRENOM
     unique (NOM, PRENOM),
  constraint PK_EC_CLIENT primary key(ID)
	);
	
create table ECOMM_PAYMENT(
  ID integer ,
  MONTANT numeric(7,2) not null,
  DATE_PAIEMENT date,
  CLIENT_ID integer ,
  TYPE_PAYMENT varchar(5),
  CREDIT_CARD varchar(20),
  CREDIT_CARD_EXPIRATION date,
  constraint PK_EC_PAYMENT primary key(ID)	
	);
	
alter table ECOMM_COMMANDE add constraint FK_EC_COMMANDE_CLIENT
   foreign key (CLIENT_ID) references ECOMM_CLIENT(ID) ;
alter table ECOMM_COMMANDE add constraint  FK_EC_COMMANDE_PAYMENT
   foreign key (PAYMENT_ID) references ECOMM_PAYMENT(ID) ;

alter table ECOMM_DETAIL_CDE add constraint FK_EC_DETAIL_CDE
   foreign key (CDE_ID) references ECOMM_COMMANDE(ID);
alter table ECOMM_DETAIL_CDE add constraint FK_EC_DETAIL_ITEM
   foreign key (ITEM_ID) references ECOMM_ITEM(ID);

alter table ECOMM_PAYMENT add constraint FK_EC_PAYMENT_CLIENT
   foreign key (CLIENT_ID) references ECOMM_CLIENT(ID);
