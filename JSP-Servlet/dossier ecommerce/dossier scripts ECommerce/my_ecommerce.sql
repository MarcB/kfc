alter table commande drop foreign key fk_commande_client,
                     drop foreign key fk_commande_payment;

alter table detail_cde drop foreign key fk_detail_cde,
                       drop foreign key fk_detail_item;

alter table payment drop foreign key fk_payment_client;

drop table  if exists commande;
drop table  if exists detail_cde;
drop table   if exists item;
drop table   if exists client;
drop table  if exists payment;

create table commande(
 id integer(11) not null auto_increment,
 datecde date,
 client_id integer(11) ,
 payment_id integer(11),
 constraint pk_commande primary key (id)		
)engine=innodb;

create table detail_cde(
  id integer(11) not null auto_increment,
  qte integer(11) not null,
  cde_id integer(11) ,
  item_id integer(11) ,
  constraint pk_detail_cde primary key(id)	
)engine=innodb;

create table item(
  id integer(11) not null auto_increment,
  categorie varchar(10) not null,
  titre varchar(40) not null,
  prix numeric(6,2) not null,
  code_barre integer(11) not null,
  constraint uk_item_code_barre
     unique(code_barre),
  constraint pk_item primary key(id)	
	)engine=innodb;
	
create table client(
  id integer(11) not null auto_increment,
  nom varchar(30) not null,
  prenom varchar(30) not null,
  constraint uk_client_nom_prenom
     unique (nom, prenom),
  constraint pk_client primary key(id)
	)engine=innodb;
	
create table payment(
  id integer(11) not null auto_increment,
  montant numeric(7,2) not null,
  date_paiement date,
  client_id integer(11) ,
  type_payment varchar(10),
  credit_card varchar(20),
  credit_card_expiration date,
  constraint pk_payment primary key(id)	
	)engine=innodb;
	
alter table commande add constraint fk_commande_client
   foreign key (client_id) references client(id) ;
alter table commande add constraint  fk_commande_payment
   foreign key (payment_id) references payment(id) ;

alter table detail_cde add constraint fk_detail_cde
   foreign key (cde_id) references commande(id);
alter table detail_cde add constraint fk_detail_item
   foreign key (item_id) references item(id);

alter table payment add constraint fk_payment_client
   foreign key (client_id) references client(id);
